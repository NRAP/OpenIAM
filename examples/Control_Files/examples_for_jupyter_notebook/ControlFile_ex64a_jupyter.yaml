# OpenIAM Control File Example 64a Jupyter Notebook
#
# This example demonstrates how the SalsaContourPlot can be used with pressure outputs 
# to perform an area of review (AoR) analysis. The AoR plot type and workflow in NRAP-Open-IAM 
# works with separate reservoir, wellbore, and aquifer components, and the SALSA component 
# does not fit in this framework. The SalsaContourPlot can be used to display gridded 
# pressure outputs in map-view figures, however, and specifying the 'AoRAquiferNumber' 
# entry will make the SalsaContourPlot evaluate if and where the pressures exceed a critical 
# pressure. This approach of evaluating if a critical pressure is exceeded is consistent with 
# EPA regulation ("regulatory approach"). In contrast, the approach of evaluating aquifer plume 
# volumes (as performed with aquifer component outputs in the AoR workflow) is a "risk-based approach."
# The use of a critical pressure approach does not work well with initially overpressured units, 
# however (e.g., the critical pressure might be exceeded before injection), and a risk-based approach 
# might be used for an overpressured unit.
#
# The SalsaContourPlot will display the pressures within one of the aquifers included in the 
# 'aquiferNamesContourPlots' keyword argument (entered under the 'ContourPlot' section). Separate 
# figures will be made for each aquifer included, but the 'AquiferNameList' entry can be used to 
# make the SalsaContourPlot only focus on specific aqufiers. For example, 'aquiferNamesContourPlots' 
# is given here as '[1, 2, 3]' - therefore, gridded pressure outputs are saved for aquifers 1, 2, 
# and 3. To reduce the number of figures created and improve the model run time, 'AquiferNameList' 
# for the SalsaContourPlot 'Pressure_Contour_Plot' is given as '[1]'. Therefore, that plot entry will 
# only display pressure results for aquifer 1.
#
# While the pressures evaluated are in aquifer 1 (which is given injection rates, making it a reservoir), 
# the area of review analysis considers the critical pressure required to drive fluid from the reservoir 
# and into an overlying aquifer. The overlying aquifer considered is generally the lowest underground 
# source of drinking water (i.e., a unit that must be protected from leakage). The critical pressure is 
# calculated based on the depth of the reservoir, the depth of the overlying aquifer, and the brine density 
# in the reservoir (the aquifer#FluidDensity parameter); see the 'Equations' section of the user guide. 
# The aquifer number for the overlying unit is given with the 'AoRAquiferNumber' entry. Here, 'AoRAquiferNumber' 
# is given as 3, so the SalsaContourPlot examines the critical pressure for driving fluid from aquifer 1 
# and into aquifer 3 along an open conduit. If the 'AoRAquiferNumber' entry is not given, the SalsaContourPlot 
# will not evaluate critical pressures. 'AoRAquiferNumber' cannot be less than or equal to the aquifer for 
# which pressure results are shown. For example, the SalsaContourPlot examines the pressures in aquifer 1 
# ('AquiferNameList: [1]'), so 'AoRAquiferNumber' cannot be given as aquifer 1.
#
# The 'CriticalPressureMPa' entry controls how the critical pressure is handled. If the entry is given as 'Calculated' 
# or it is not given at all, then critical pressure is calculated with the equation shown in the 'Equations' section 
# of the user guide. Otherwise, if the user wants to manually handle the calculation of critical pressure then a 
# specific critical pressure can be given in MPa. For example, 'CriticalPressureMPa: 12.1' would enforce a critical 
# pressure of 12.1 MPa. The figures made over time would then specify if and where the pressure outputs exceeded 
# 12.1 MPa. Even if a specific critical pressure is given, the user must provide the 'AoRAquiferNumber' to allow for 
# the consideration of critical pressures. The 'AoRAquiferNumber' is displayed on the figure as the aquifer considered 
# for the critical pressure. If multiple aquifers are displayed with the SalsaContourPlot (e.g., if 'AquiferNameList' 
# was not used to limit the selection to only aquifer 1), only one specified critical pressure value will be considered. 
# If 'CriticalPressureMPa' is 'Calculated' or not provided, then different critical pressures will be calculated for 
# each combination of aquifers (deeper unit with pressure output and an overlying, protected aquifer).
#
# To run this file, use the command (\ for Windows / for Mac or Linux):
#  python ../../src/openiam/components/openiam_cf.py --file ControlFile_ex64a_jupyter.yaml
#
# Last Modified: June, 2024
#-------------------------------------------------
ModelParams:
    # Time is in years
    EndTime: 150
    TimeStep: 10.0
    Analysis: forward
    Components: [SalsaComp]
    OutputDirectory: output/output_ex64a_jupyter
    Logging: Info
#-------------------------------------------------
Stratigraphy:
    # The SALSA component does not connect with a stratigraphy component, so 
    # any information entered here does not impact the results from SALSA.
    numberOfShaleLayers: 3
    reservoirThickness: 50
    shale1Thickness: 250
    aquifer1Thickness: 100
    shale2Thickness: 250
    aquifer2Thickness: 100
    shale3Thickness: 250
    #-------------------------------------------------
SalsaComp:
    Type: SALSA
    InjectionWell:
        #---- keyword arguments related to active wells ----#
        periodEndTimes: [75, 150]
        # The first well lcoation is an extraction well, the other four are injection wells
        activeWellCoordx: [0.0, 1000, -1000, -1000, 1000]
        activeWellCoordy: [0.0, 1000, 1000, -1000, -1000]
    LeakingWell:
        #---- keyword arguments related to leaking wells ----#
        leakingWellCoordx: [100, 1100, 5000]
        leakingWellCoordy: [0, 1000, 1000]
    OutputLocations:
        #---- keyword arguments related to hydraulic head output ----#
        aquiferCoordx: [100, 1100, 5000]
        aquiferCoordy: [0, 1000, 1000]
        shaleCoordx: [100, 1100, 5000]
        shaleCoordy: [0, 1000, 1000]
    ContourPlot:
        aquiferNamesContourPlots: [1, 2, 3]
    Parameters:
        #---- Stratigraphy parameters ----#
        bottomLayerType: 0     # 1 for aquifer, 0 for shale (aquitard)
        topLayerType: 0
        # Shale parameters
        numberOfShaleLayers: 4
        shale1Thickness: 43.0
        shale2Thickness: 465.0
        shale3Thickness: 67.0
        shale4Thickness: 210.0
        shale1LogVertK: -10.5
        shale2LogVertK: -10.0
        shale3LogVertK: -11.0
        shale4LogVertK: -12.0
        shale1LogSS: -5.5
        shale2LogSS: -5.0
        shale3LogSS: -6.0
        shale4LogSS: -7.0
        shaleAllInitialGamma: 0
        shaleAllGamma: 0
        # Aquifer parameters
        aquifer1Thickness: 72.0
        aquifer2Thickness: 136.0
        aquifer3Thickness: 48.0
        aquifer1LogHorizK: -4.5
        aquifer2LogHorizK: -4.0
        aquifer3LogHorizK: -5.0
        aquiferAllANSR: 1
        aquifer1LogSS: -5.5
        aquifer2LogSS: -5.0
        aquifer3LogSS: -6.0
        aquifer1FluidDensity: 1100
        aquifer2FluidDensity: 1025
        aquifer3FluidDensity: 1000
        aquifer1Head: 10
        aquifer2Head: 25
        aquifer3Head: 0
        aquiferAllGamma: 0.0
        #---- Boundary conditions -------#
        bottomBoundaryCond: 1
        bottomBoundaryHead: 0.0
        topBoundaryCond: 0
        topBoundaryHead: 0.0
        topBoundaryPressure: 101325
        #---- Active well parameters ----#
        # Injection period 1
        activeWell1QAquifer1Period1: -0.025
        activeWell2QAquifer1Period1: 0.1
        activeWell3QAquifer1Period1: 0.1
        activeWell4QAquifer1Period1: 0.1
        activeWell5QAquifer1Period1: 0.1
        activeWell1QAquifer2Period1: 0.0
        activeWell2QAquifer2Period1: 0.0
        activeWell3QAquifer2Period1: 0.0
        activeWell4QAquifer2Period1: 0.0
        activeWell5QAquifer2Period1: 0.0
        activeWell1QAquifer3Period1: 0.0
        activeWell2QAquifer3Period1: 0.0
        activeWell3QAquifer3Period1: 0.0
        activeWell4QAquifer3Period1: 0.0
        activeWell5QAquifer3Period1: 0.0
        # Injection period 2
        activeWell1QAquifer1Period2: 0.0
        activeWell2QAquifer1Period2: 0.0
        activeWell3QAquifer1Period2: 0.0
        activeWell4QAquifer1Period2: 0.0
        activeWell5QAquifer1Period2: 0.0
        activeWell1QAquifer2Period2: 0.0
        activeWell2QAquifer2Period2: 0.0
        activeWell3QAquifer2Period2: 0.0
        activeWell4QAquifer2Period2: 0.0
        activeWell5QAquifer2Period2: 0.0
        activeWell1QAquifer3Period2: 0.0
        activeWell2QAquifer3Period2: 0.0
        activeWell3QAquifer3Period2: 0.0
        activeWell4QAquifer3Period2: 0.0
        activeWell5QAquifer3Period2: 0.0
        # Injection Well Radius
        activeWellAllRadiusAquiferAll: 0.1
        #---- Leaking well parameters ----#
        leakingWellAllLogKAquifer1: -4.5
        leakingWellAllLogKAquifer2: -4.0
        leakingWellAllLogKAquifer3: -5.0
        leakingWellAllRadiusAquiferAll: 0.15
        leakingWellAllStatAquiferAll: 1
        leakingWellAllLogKShale1: -4.5
        leakingWellAllLogKShale2: -4.0
        leakingWellAllLogKShale3: -5.0
        leakingWellAllLogKShale4: -6.0
        leakingWellAllRadiusShaleAll: 0.15
        leakingWellAllStatShale1: 1
        leakingWellAllStatShale2: 1
        leakingWellAllStatShale3: 0
        #---- Miscellaneous parameters ----#
        numberOfLaplaceTerms: 8
        flagMesh: 1
        xCent: 0.0
        yCent: 0.0
        xMax: 10000.0
        yMax: 10000.0
        gridExp: 1.0
        numberOfNodesXDir: 50
        numberOfNodesYDir: 50
        numberOfRadialGrids: 6
        radialZoneRadius: 100.0
        radialGridExp: 1.2
        numberOfVerticalPoints: 10
    Outputs: [headLocAllAquiferAll, pressureLocAllAquiferAll, pressureLocAllMidAquiferAll, pressureLocAllTopAquiferAll, 
              headProfileAllVertPointAllShaleAll, pressureProfileAllVertPointAllShaleAll, 
              profileDepths, allHeadProfilesShales, allPressureProfilesShales, 
              diffuseLeakageRateBottomAquiferAll, diffuseLeakageVolumeBottomAquiferAll, 
              diffuseLeakageRateTopAquiferAll, diffuseLeakageVolumeTopAquiferAll, 
              wellLeakageRateAquiferAll, wellLeakageVolumeAquiferAll, 
              wellAllLeakageRateAquiferAll, wellAllLeakageVolumeAquiferAll, 
              contourPlotAquiferHead, contourPlotTopAquiferPressure, contourPlotCoordx, contourPlotCoordy]
#-------------------------------------------------
Plots:
    Leaking_Well_Leakage_Volumes:
        TimeSeries: [well1LeakageVolumeAquifer2, well1LeakageVolumeAquifer3, 
                     well2LeakageVolumeAquifer2, well2LeakageVolumeAquifer3, 
                     well3LeakageVolumeAquifer2, well3LeakageVolumeAquifer3]
    Diffuse_Leakage_Rates:
        TimeSeries: [diffuseLeakageRateTopAquifer1, diffuseLeakageRateTopAquifer2, diffuseLeakageRateTopAquifer3]
        Subplot:
            Use: False                       # 'Use: False' specifies that subplots will not be used
    Pressure_Profile:
        SalsaProfile:                        # This plot requires the profileDepths and allPressureProfilesShales outputs
            MetricType: pressure
    Pressure_Time_Series:
        SalsaTimeSeries:                     # This plot requires the profileDepths, pressureLoc#Aquifer#, pressureLoc#MidAquifer#, 
            MetricType: pressure             # pressureLoc#TopAquifer#, and pressureProfile#VertPoint#Shale# outputs.
    Pressure_Contour_Plot:
        SalsaContourPlot:                    # Requires the contourPlotTopAquiferPressure, contourPlotCoordx, and contourPlotCoordy outputs
            MetricType: pressure
            AquiferLocation: Top             # Options are Bottom, Middle, or Top - default is Bottom.
            TimeList: All                    # All times, or just specific times (e.g., [10, 20, 30], in years)
            AoRAquiferNumber: 3              # Specifies that the critical pressure will be calculated for aquifer 3
            CriticalPressureMPa: Calculated
            AquiferNameList: [1]             # Specifies that this SalsaContourPlot will only show pressures for aquifer 1



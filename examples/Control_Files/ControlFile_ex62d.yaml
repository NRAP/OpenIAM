# OpenIAM Control File Example 62d
#
# This example is similar to control file examples 62b and 62c, but this example 
# uses lower hydraulic conductivities for the leaking wells and higher vertical 
# hydraulic conductivities for the shales. These conditions allow for less well 
# leakage but more diffuse leakage across aquifer-aquitard interfaces. There is 
# also only one leaking well that is situated farther from the injection sites.
#
# To run this file, use the command (\ for Windows / for Mac or Linux):
#  python ../../src/openiam/components/openiam_cf.py --file ControlFile_ex62d.yaml
#
# Last Modified: June, 2024
#-------------------------------------------------
ModelParams:
    # Time is in years
    EndTime: 150
    TimeStep: 10.0
    Analysis: forward
    Components: [SALSA]
    OutputDirectory: output/output_ex62d_{datetime}
    Logging: Warning
#-------------------------------------------------
Stratigraphy:
    # The SALSA component does not connect with a stratigraphy component, so 
    # any information entered here does not impact the results from SALSA.
    numberOfShaleLayers:
        vary: False
        value: 3
    shale1Thickness:
        value: 250.0
        vary: False
    shale2Thickness:
        value: 250.0
        vary: False
    shale3Thickness:
        value: 250.0
        vary: False
    aquifer1Thickness:
        value: 100.0
        vary: False
    aquifer2Thickness:
        value: 100.0
        vary: False
    reservoirThickness:
        value: 50.0
        vary: False
#-------------------------------------------------
SALSA:
    Type: SALSA
    InjectionWell:
        #---- keyword arguments related to active wells ----#
        periodEndTimes: [10, 20, 30, 75, 150]
        # The first well location is an extraction well, the other four are injection wells
        activeWellCoordx: [0.0, 3535.53, -3535.53, -3535.53, 3535.53]
        activeWellCoordy: [0.0, 3535.53, 3535.53, -3535.53, -3535.53]
    LeakingWell:
        #---- keyword arguments related to leaking wells ----#
        leakingWellCoordx: [10606.60]
        leakingWellCoordy: [10606.60]
    OutputLocations:
        #---- keyword arguments related to hydraulic head and pressure outputs ----#
        aquiferCoordx: [10606.60]
        aquiferCoordy: [10606.60]
        shaleCoordx: [10606.60]
        shaleCoordy: [10606.60]
    ContourPlot:
        aquiferNamesContourPlots: [1, 2, 3]
    Parameters:
        #---- Stratigraphy parameters ----#
        numberOfShaleLayers: 4
        bottomLayerType: 0
        topLayerType: 0
        shale1Thickness: 56.0
        shale2Thickness: 157.0
        shale3Thickness: 430.0
        shale4Thickness: 52.0
        shale1LogVertK: -7
        shale2LogVertK: -6
        shale3LogVertK: -7
        shale4LogVertK: -7
        shale1LogSS: -7.0
        shale2LogSS: -6.0
        shale3LogSS: -7.0
        shale4LogSS: -7.0
        shaleAllInitialGamma: 0
        shaleAllGamma: 0
        aquifer1Thickness: 145
        aquifer2Thickness: 27
        aquifer3Thickness: 62
        aquifer1LogHorizK: -4.5
        aquifer2LogHorizK: -4.0
        aquifer3LogHorizK: -5.0
        aquiferAllANSR: 1
        aquifer1LogSS: -5.5
        aquifer2LogSS: -5.0
        aquifer3LogSS: -6.0
        aquifer1FluidDensity: 1100
        aquifer2FluidDensity: 1010
        aquifer3FluidDensity: 1000
        aquifer1Head: 10
        aquifer2Head: 25
        aquifer3Head: 5
        aquiferAllGamma: 0.0
        bottomBoundaryCond: 1
        bottomBoundaryHead: 0.0
        topBoundaryCond: 0
        topBoundaryHead: 0.0
        #---- Active well parameters ----#
        # Injection period 1 ----------#
        activeWell1QAquifer1Period1: -0.25
        activeWell2QAquifer1Period1: 0.5
        activeWell3QAquifer1Period1: 0.5
        activeWell4QAquifer1Period1: 0.5
        activeWell5QAquifer1Period1: 0.5
        activeWell1QAquifer2Period1: 0.0
        activeWell2QAquifer2Period1: 0.0
        activeWell3QAquifer2Period1: 0.0
        activeWell4QAquifer2Period1: 0.0
        activeWell5QAquifer2Period1: 0.0
        activeWell1QAquifer3Period1: 0.0
        activeWell2QAquifer3Period1: 0.0
        activeWell3QAquifer3Period1: 0.0
        activeWell4QAquifer3Period1: 0.0
        activeWell5QAquifer3Period1: 0.0
        # Injection period 2 ----------#
        activeWell1QAquifer1Period2: -0.5
        activeWell2QAquifer1Period2: 1.0
        activeWell3QAquifer1Period2: 1.0
        activeWell4QAquifer1Period2: 1.0
        activeWell5QAquifer1Period2: 1.0
        activeWell1QAquifer2Period2: 0.0
        activeWell2QAquifer2Period2: 0.0
        activeWell3QAquifer2Period2: 0.0
        activeWell4QAquifer2Period2: 0.0
        activeWell5QAquifer2Period2: 0.0
        activeWell1QAquifer3Period2: 0.0
        activeWell2QAquifer3Period2: 0.0
        activeWell3QAquifer3Period2: 0.0
        activeWell4QAquifer3Period2: 0.0
        activeWell5QAquifer3Period2: 0.0
        # Injection period 3 ----------#
        activeWell1QAquifer1Period3: -1.0
        activeWell2QAquifer1Period3: 2.0
        activeWell3QAquifer1Period3: 2.0
        activeWell4QAquifer1Period3: 2.0
        activeWell5QAquifer1Period3: 2.0
        activeWell1QAquifer2Period3: 0.0
        activeWell2QAquifer2Period3: 0.0
        activeWell3QAquifer2Period3: 0.0
        activeWell4QAquifer2Period3: 0.0
        activeWell5QAquifer2Period3: 0.0
        activeWell1QAquifer3Period3: 0.0
        activeWell2QAquifer3Period3: 0.0
        activeWell3QAquifer3Period3: 0.0
        activeWell4QAquifer3Period3: 0.0
        activeWell5QAquifer3Period3: 0.0
        # Injection period 4 ----------#
        activeWell1QAquifer1Period4: -2.0
        activeWell2QAquifer1Period4: 4.0
        activeWell3QAquifer1Period4: 4.0
        activeWell4QAquifer1Period4: 4.0
        activeWell5QAquifer1Period4: 4.0
        activeWell1QAquifer2Period4: 0.0
        activeWell2QAquifer2Period4: 0.0
        activeWell3QAquifer2Period4: 0.0
        activeWell4QAquifer2Period4: 0.0
        activeWell5QAquifer2Period4: 0.0
        activeWell1QAquifer3Period4: 0.0
        activeWell2QAquifer3Period4: 0.0
        activeWell3QAquifer3Period4: 0.0
        activeWell4QAquifer3Period4: 0.0
        activeWell5QAquifer3Period4: 0.0
        # Injection period 5 ----------#
        activeWell1QAquifer1Period5: 0.0
        activeWell2QAquifer1Period5: 0.0
        activeWell3QAquifer1Period5: 0.0
        activeWell4QAquifer1Period5: 0.0
        activeWell5QAquifer1Period5: 0.0
        activeWell1QAquifer2Period5: 0.0
        activeWell2QAquifer2Period5: 0.0
        activeWell3QAquifer2Period5: 0.0
        activeWell4QAquifer2Period5: 0.0
        activeWell5QAquifer2Period5: 0.0
        activeWell1QAquifer3Period5: 0.0
        activeWell2QAquifer3Period5: 0.0
        activeWell3QAquifer3Period5: 0.0
        activeWell4QAquifer3Period5: 0.0
        activeWell5QAquifer3Period5: 0.0
        activeWellAllRadiusAquiferAll: 0.1
        #---- Leaking well parameters ----#
        leakingWell1LogKAquifer1: -7
        leakingWell1LogKAquifer2: -7
        leakingWell1LogKAquifer3: -7
        leakingWellAllRadiusAquiferAll: 0.15
        leakingWellAllStatAquiferAll: 1
        leakingWellAllLogKShale1: -7
        leakingWellAllLogKShale2: -7
        leakingWellAllLogKShale3: -7
        leakingWellAllLogKShale4: -7
        leakingWellAllRadiusShaleAll: 0.15
        leakingWell1StatShale1: 1
        leakingWell1StatShale2: 1
        leakingWell1StatShale3: 0
        # no leakingWell1StatShale4 - that parameter only applies when a shale 
        # has aquifers above and below it, but shale 4 does not here. If you entered 
        # that parameter here, a warning statement would print to say that the input 
        # would not be used.
        #---- Miscellaneous parameters ----#
        numberOfLaplaceTerms: 8
        xCent: 0.0
        yCent: 0.0
        xMax: 15000.0
        yMax: 15000.0
        gridExp: 1.0
        numberOfNodesXDir: 75
        numberOfNodesYDir: 75
        numberOfRadialGrids: 6
        radialZoneRadius: 100.0
        radialGridExp: 1.2
        numberOfVerticalPoints: 10
    Outputs: [headLocAllAquiferAll, pressureLocAllAquiferAll, pressureLocAllMidAquiferAll, pressureLocAllTopAquiferAll, 
              headProfileAllVertPointAllShaleAll, pressureProfileAllVertPointAllShaleAll, 
              profileDepths, allHeadProfilesShales, allPressureProfilesShales, 
              diffuseLeakageRateBottomAquiferAll, diffuseLeakageVolumeBottomAquiferAll, 
              diffuseLeakageRateTopAquiferAll, diffuseLeakageVolumeTopAquiferAll, 
              wellLeakageRateAquiferAll, wellLeakageVolumeAquiferAll, 
              wellAllLeakageRateAquiferAll, wellAllLeakageVolumeAquiferAll, 
              contourPlotAquiferHead, contourPlotAquiferPressure, 
              contourPlotMidAquiferPressure, contourPlotTopAquiferPressure, 
              contourPlotCoordx, contourPlotCoordy]
#-------------------------------------------------
Plots:
    Diffuse_Leakage_Rate:
        TimeSeries: [diffuseLeakageRateBottomAquifer1, diffuseLeakageRateTopAquifer1, 
                     diffuseLeakageRateBottomAquifer2, diffuseLeakageRateTopAquifer2, 
                     diffuseLeakageRateBottomAquifer3, diffuseLeakageRateTopAquifer3]
        Subplot:
            Use: False
    Diffuse_Leakage_Vol:
        TimeSeries: [diffuseLeakageVolumeBottomAquifer1, diffuseLeakageVolumeTopAquifer1, 
                     diffuseLeakageVolumeBottomAquifer2, diffuseLeakageVolumeTopAquifer2, 
                     diffuseLeakageVolumeBottomAquifer3, diffuseLeakageVolumeTopAquifer3]
        Subplot:
            Use: False
    Total_Well_Leakage_Vol:
        TimeSeries: [wellLeakageVolumeAquifer1, wellLeakageVolumeAquifer2, wellLeakageVolumeAquifer3]
        Subplot:
            Use: False
    Total_Well_Leakage_Rate:
        TimeSeries: [wellLeakageRateAquifer1, wellLeakageRateAquifer2, wellLeakageRateAquifer3]
        Subplot:
            Use: False
    Indiv_Well_Leakage_Vol:
        TimeSeries: [well1LeakageVolumeAquifer1, well1LeakageVolumeAquifer2, well1LeakageVolumeAquifer3]
        Subplot:
            Use: False
    Indiv_Well_Leakage_Rate:
        TimeSeries: [well1LeakageRateAquifer1, well1LeakageRateAquifer2, well1LeakageRateAquifer2]
        Subplot:
            Use: False
    Head_Profile.tiff:                 # default MetricType of head is used
        SalsaProfile:
            FigureDPI: 300
    Head_Time_Series.tiff:
        SalsaTimeSeries:
            FigureDPI: 300
    Head_Contour_Plot.tiff:
        SalsaContourPlot:
            FigureDPI: 300
    Pressure_Profile.tiff:
        SalsaProfile:
            FigureDPI: 300
            MetricType: pressure
    Pressure_Time_Series.tiff:
        SalsaTimeSeries:               # This plot requires the profileDepths, pressureLoc#Aquifer#, pressureLoc#MidAquifer#, 
            FigureDPI: 300             # pressureLoc#TopAquifer#, and pressureProfile#VertPoint#Shale# outputs.
            MetricType: pressure
    Pressure_Contour_Plot_Bottom.tiff:
        SalsaContourPlot:
            AquiferLocation: Middle    # Options are Bottom, Middle, or Top - default is Bottom.
            MetricType: pressure       # Because the AquiferLocation is set to Bottom, the 
            FigureDPI: 300             # contourPlotAquiferPressure output must be included.
    Pressure_Contour_Plot_Top.tiff:
        SalsaContourPlot:
            AquiferLocation: Top       # Options are Bottom, Middle, or Top - default is Bottom.
            MetricType: pressure       # Because the AquiferLocation is set to Top, the 
            FigureDPI: 300             # contourPlotTopAquiferPressure output must be included.


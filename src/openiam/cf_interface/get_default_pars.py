
import logging
import numpy as np

try:
    import openiam.base as iam_base
    import openiam.cf_interface.commons as iam_commons
except ImportError as err:
    print('Unable to load IAM class module: {}'.format(err))

def get_default_pars(comp_type, par_name):
    """
    Function is used to determine a default parameter value for a given component 
    type. This function uses the get_parameter_val() function and a placeholder 
    ('dummy') system model and a placeholder component to extract the default 
    parameter value. These steps are necessary for situations where default 
    parameter values need to be checked prior to the creation of the actual 
    components. For example, this function is used during the setup of a simulation 
    using a workflow. If two components have the same parameter (e.g., brineDensity)
    and that parameter was left as the default value (i.e., no input was given 
    for that parameter), then this function is used to compare the default values. 
    Default values might be changed over time, so this function allows the 
    current default value to be determined.
    """
    debug_msg = ''.join([
        'Using the get_default_pars() function to determine the default value ', 
        'for the parameter {} of the component type {}. This function is used ', 
        'during the setup of a simulation using a workflow to compare the ', 
        'default parameters of different component types.']).format(
            par_name, comp_type)
    logging.debug(debug_msg)
    
    comp_name = comp_type + '1'
    comp_type_attr = getattr(iam_base, comp_type)
    
    # Set up a fake system model just to extract the default parameter value 
    # for the parameter type used
    num_years = 1
    time_array = 365.25 * np.arange(0.0, num_years + 1)
    sm_model_kwargs = {'time_array': time_array} # time is given in days

    # Create dummy system model
    dummy_sm = iam_base.SystemModel(model_kwargs=sm_model_kwargs)
    
    dummy_comp = dummy_sm.add_component_model_object(
        comp_type_attr(name=comp_name, parent=dummy_sm))
    
    default_par_val = iam_commons.get_parameter_val(dummy_comp, par_name,
                                                    sm=dummy_sm)

    return default_par_val

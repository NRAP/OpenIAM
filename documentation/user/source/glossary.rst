.. _glossary:

Glossary
========

A
-

* **Active Well** - Refers to wells used for injection or extraction of |CO2|, brine, or water into 
  or out of an aquifer. The targeted aquifer is called a reservoir when it receives |CO2| injection.

* **AoR (Area of Review)** - The area surrounding a geologic carbon storage site that may be affected 
  by the operation. Because it may be affected, the area is monitored for leakage during the site's 
  operation and the post-injection site care period (PISC). AoR delineation is required in a permit 
  application, and the extent must be justified based on the potential impacts of the operation.

* **AZMI (Above Zone Monitoring Interval)** - An aquifer layer above the reservoir, typically closer 
  to the reservoir than other overlying aquifers. Because the aquifer is closer to the reservoir, 
  any leakage that occurs may enter that unit first. The AZMI is therefore monitored for leakage.

L
-

* **Latin Hypercube Sampling (LHS)** - A statistical method used to explore a parameter space. While a 
  purely random sampling approach could exclude sections of the parameter space, the LHS approach divides 
  the parameter space into multiple sections and randomly samples within each section. The approach 
  therefore helps to evaluate different combinations of values across the range of the parameter space.

* **Legacy Well** - A wellbore that is no longer in active use. Such wells are often abandoned oil and gas 
  wells. If the legacy well penetrates the reservoir or units that receive leakage, the well can serve as 
  a leakage pathway for fluids such as |CO2| or brine.

* **Leaking Well** - A wellbore that is actively serving as a leakage pathway for fluids such as |CO2| or 
  brine.

P
-

* **Parstudy (Parameter Study)** - An analysis method within NRAP-Open-IAM used for sensitivity analysis, 
  where multiple parameter values are varied to observe their impact on outputs.

* **PISC (Post-Injection Site Care)** - A period after injection stops during which operators must continue 
  to monitor for leakage.

R
-

* **Reservoir** - An aquifer layer that receives |CO2| injection.

T
-

* **TDS (Total Dissolved Solids)** - Concentration of dissolved substances in water, expressed in units 
  of mass per unit volume (e.g., |mg/L|).

* **Thief Zone** - An aquifer layer between the reservoir and an overlying aquifer of interest. The overlying 
  aquifer may be of interest because it contains potable groundwater. The thief zone aquifer can act to 
  intercept leakage from the reservoir before it reaches the overlying aquifer.

* **TTFD (Time to First Detection)** - The time period between the onset of leakage and when the leakage is 
  first detected through monitoring efforts.

U
-

* **USDW (Underground Source of Drinking Water)** - An aquifer layer that contains potable groundwater. 
  The unit can be currently used as a source of drinking water, or it has the potential to be used as one. 
  Such aquifers must be protected from leakage.

X
-

* **x** - Easting distance relative to a reference location. Positive values are to the east of the reference 
  location, while negative values are to the west.
  
Y
-

* **y** - Northing distance relative to a reference location. Positive values are to the north of the reference 
  location, while negative values are to the south.

Z
-

* **z** - Depth beneath the surface of the landscape. Some applications define depths beneath the surface as being 
  negative, while others define them as being positive.

"""
The module contains the solution class for the multisegmented_wellbore_component.

The solution is based on the Celia et al., 2011 paper
"Field-scale application of a semi-analytical model
for estimation of CO2 and brine leakage along old wells".

Author: Seunghwan Baek, PNNL and Veronika S. Vasylkivska, NETL and Nate Michelle, NETL
Date: 06/25/2015
"""
import logging
import os
import sys
import numpy as np
import scipy.special as scm
from scipy.interpolate import interp1d
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from multisegmented import units

# Placeholder value for initial pressures (aquiferInitialPressure and reservoirInitialPressure) 
# that is ignored if left unchanged.
PLACEHOLDER_PRESSURE_INPUT = 0

# As pressure values are passed between components, the process can result in 
# very, very slight rounding. Because of this rounding, the pressure given may 
# be less than the pressure calculated for the top of the reservoir (even if 
# they were meant to be equal). In that case, the variables deltaPave and deltaP
# might still be calculated as very small values, resulting in small but constant
# leakage rates. These thresholds are used to check if the pressure is approximately
# equal to the initial pressure at the top of the reservoir. If so, the deltaPave
# and deltaP variables are manually scaled down (scaled down further as the pressure
# gets closer to the exact initial pressure). Otherwise, the small deltaPave and
# deltaP values calculated can lead to constant, background leakage rates.
INTERVAL = 0.001
APPROX_INITIAL_LOWER_BOUND = 1 - INTERVAL
APPROX_INITIAL_UPPER_BOUND = 1 + INTERVAL

class Parameters():
    """ Parameters class for multisegmented wellbore ROM."""
    def __init__(self):
        """ Create an instance of the Parameters class. """
        self.set_neg_lr_to_zero = None

        self.numberOfShaleLayers = None

        self.shaleThickness = None
        self.aquiferThickness = None
        self.reservoirThickness = None
        self.pressureGrads = None # array of pressure gradients (Pa/m) for each unit

        # Initial pressures for the bottom of each shale and aquifer - used if provided.
        self.shaleInitialPressure = None
        self.aquiferInitialPressure = None

        # Initial pressure at the top of the reservoir - used if provided.
        self.reservoirInitialPressure = None

        # Permeability
        self.shalePermeability = None
        self.aquiferPermeability = None

        # Pressure and fluid density at the top of the system (above the highest shale)
        self.datumPressure = None

        # Input pressure, CO2 saturation and time points
        self.pressure = None
        self.CO2saturation = None
        self.timePoint = None

        # Amount of CO2 present in aquifers before simulation
        self.prevCO2Volume = None

        # Time step in days
        self.timeStep = None

        # Density
        self.brineDensity = None
        self.brineDensityForm = "Constant"     # constant or functional
        self.CO2Density = None
        self.CO2DensityForm = "Constant"       # constant or functional

        # Viscosity
        self.brineViscosity = None
        self.brineViscosityForm = "Constant"     # constant or functional
        self.CO2Viscosity = None
        self.CO2ViscosityForm = "Constant"       # constant or functional

        # Residual saturation and compressibility
        self.brineResidualSaturation = None
        self.compressibility = None

        # Well radius
        self.wellRadius = None
        self.flowArea = None

class Solution():
    """ Solution class for multisegmented wellbore ROM."""
    def __init__(self, parameters):
        """ Create an instance of the Solution class."""
        # Setup the parameters that define a solution
        self.parameters = Parameters()
        self.parameters = parameters

        # Reserve a space for the solution pieces
        self.CO2LeakageRates = None
        self.brineLeakageRates = None
        self.g = 9.81  # acceleration due to gravity, in m/s^2

        # Reserve space for instance attributes
        self.initialPressure = None
        self.initialPressureTopReservoir = None
        self.nSL = None
        self.reservoirBottom = None
        self.depth = None

        self.interface = None
        self.thicknessH = None

        self.CO2Volume = None
        self.CO2SaturationAq = None

        self.CO2MobilityAq = None
        self.brineMobilityAq = None

        self.CO2MobilityShale = None
        self.brineMobilityShale = None
        self.effectiveMobility = None

        self.CO2Q = None
        self.brineQ = None
        self.Lambda = None

    def get_depth_for_pressure(self):
        """ Calculate depth of all layers for the pressure calculations."""
        # The deepest shale has the first thickness in the array
        shaleThickness = self.parameters.shaleThickness
        aquiferThickness = self.parameters.aquiferThickness
        reservoirThickness = self.parameters.reservoirThickness

        # Get the sum of shale layers thicknesses
        shaleThicknessSum = sum(shaleThickness)

        # Get the sum of aquifer layers thicknesses
        aquiferThicknessSum = sum(aquiferThickness)

        self.layerIntervals = []
        for i in range(len(shaleThickness)-1,-1,-1): # from top (surface) to bottom (reservoir)
            if i < len(aquiferThickness):
                self.layerIntervals.append(aquiferThickness[i])

            self.layerIntervals.append(shaleThickness[i])

        self.layerIntervals.append(reservoirThickness)

        self.allbottomdepth = []
        cumulative_sum = 0
        for interval in self.layerIntervals: # from top (surface) to down (reservoir)
            cumulative_sum += interval
            self.allbottomdepth.append(cumulative_sum)

        # Get the depth of the bottom of the reservoir
        self.reservoirBottom = (shaleThicknessSum + aquiferThicknessSum +
                                reservoirThickness)

        self.reservoirTop = self.reservoirBottom - reservoirThickness

    def setup_initial_conditions(self):
        """ Setup initial conditions at the abandoned well."""
        # Determine depths of the aquifers
        self.get_depth_for_pressure()

        # Thickness of reservoir
        self.reservoirThickness = self.parameters.reservoirThickness

        self.nSL = self.parameters.numberOfShaleLayers

        # Initial pressure at the bottom of each unit. First element is for the
        # highest shale, last is for the reservoir.
        self.InitialBottomPressureAll = np.zeros((len(self.allbottomdepth)))

        # Array of initial pressures for just the aquifers and reservoir (not shales)
        self.initialPressure = np.zeros((1 + len(self.parameters.aquiferThickness)))

        shaleInd = self.nSL - 1
        aquiferInd = self.nSL - 2
        for ii in range(len(self.InitialBottomPressureAll)):
            if ii == 0: # top shale unit
                if self.parameters.shaleInitialPressure[shaleInd] == PLACEHOLDER_PRESSURE_INPUT:
                    self.InitialBottomPressureAll[ii] = self.parameters.datumPressure + (
                        self.layerIntervals[ii] * self.parameters.pressureGrads[ii])
                else:
                    # Use the enforced initial pressure
                    self.InitialBottomPressureAll[ii] = self.parameters.shaleInitialPressure[shaleInd]
                shaleInd -= 1

            elif ii == (len(self.InitialBottomPressureAll) - 1): # storage reservoir
                self.InitialBottomPressureAll[ii] = self.InitialBottomPressureAll[ii-1] + (
                    self.layerIntervals[ii] * self.parameters.pressureGrads[ii])

            elif ii % 2 == 1: # aquifer
                if self.parameters.aquiferInitialPressure[aquiferInd] == PLACEHOLDER_PRESSURE_INPUT:
                    self.InitialBottomPressureAll[ii] = self.InitialBottomPressureAll[ii-1] + (
                        self.layerIntervals[ii] * self.parameters.pressureGrads[ii])
                else:
                    # Use the enforced initial pressure
                    self.InitialBottomPressureAll[ii] = self.parameters.aquiferInitialPressure[aquiferInd]
                aquiferInd -= 1

            else: # shale
                if self.parameters.shaleInitialPressure[shaleInd] == PLACEHOLDER_PRESSURE_INPUT:
                    self.InitialBottomPressureAll[ii] = self.InitialBottomPressureAll[ii-1] + (
                        self.layerIntervals[ii] * self.parameters.pressureGrads[ii])
                else:
                    # Use the enforced initial pressure
                    self.InitialBottomPressureAll[ii] = self.parameters.shaleInitialPressure[shaleInd]
                shaleInd -= 1

        # Set up initial pressure at the top of the reservoir
        if (self.parameters.reservoirInitialPressure == PLACEHOLDER_PRESSURE_INPUT):
            self.initialPressureTopReservoir = self.InitialBottomPressureAll[-1] - (
                self.layerIntervals[-1] * self.parameters.pressureGrads[-1])

            warn_msg_addition = 'calculated pressure at'
        else:
            # Use specified reservoir pressure
            self.initialPressureTopReservoir = self.parameters.reservoirInitialPressure

            # set the pressure at bottom of shale 1 equal to the input
            self.InitialBottomPressureAll[-2] = self.initialPressureTopReservoir

            self.InitialBottomPressureAll[-1] = self.initialPressureTopReservoir + (
                self.layerIntervals[-1] * self.parameters.pressureGrads[-1])

            warn_msg_addition = 'initial pressure given for'

        index_reservor_aquifer = 0
        for ii in range(len(self.InitialBottomPressureAll) - 1, -1, -1):
            if (ii % 2 == 1) or (ii == len(self.InitialBottomPressureAll) - 1):
                self.initialPressure[index_reservor_aquifer] = self.InitialBottomPressureAll[ii]
                index_reservor_aquifer += 1

        # Scaling the initial pressure here because it would otherwise print this 
        # message even when attempting to give the component was given the exact 
        # same pressure. The floating point number seems to be slightly rounded 
        # in a way that prevents the two values from being exactly equal.
        if (self.initialPressureTopReservoir * 0.9999) > self.parameters.pressure:
            warn_msg = ''.join([
                f'For a Multisegmented Wellbore component at time {self.parameters.timePoint / 365.25: .2e} ', 
                f'years, the {warn_msg_addition} the top of reservoir ({self.initialPressureTopReservoir: .2e} Pa) ', 
                f'exceeds the supplied pressure ({self.parameters.pressure: .2e} Pa). ', 
                '\nThis discrepancy could explain negative leakage rates.'])
            logging.warning(warn_msg)

        # Setup initial masses of CO2 in each aquifer and reservoir
        self.CO2Volume = self.parameters.prevCO2Volume

        # Setup array for the thickness of aquifers and reservoir
        self.thicknessH = np.ones(self.nSL)
        self.thicknessH[0] = self.parameters.reservoirThickness
        self.thicknessH[1:self.nSL] = self.parameters.aquiferThickness

        # Setup initial interface height h at the leaking well
        self.interface = np.zeros(self.nSL)
        for j in range(self.nSL):
            self.interface[j] = self.parameters.CO2saturation[j]*self.thicknessH[j]

        # Setup saturation in aquifers
        self.CO2SaturationAq = np.zeros(self.nSL-1)

    def get_mobility_for_aquifers(self):
        """ Calculate mobility parameter for brine and CO2 in the aquifers."""

        CO2RelPermAq = np.zeros(self.nSL)
        brineRelPermAq = np.ones(self.nSL)

        Sres = self.parameters.aquBrineResSaturation

        for ind in range(self.nSL):
            CO2RelPermAq[ind] = np.minimum((
                1-Sres[ind]), self.interface[ind]/self.thicknessH[ind])
            brineRelPermAq[ind] = 1/(1-Sres[ind])*(
                1-Sres[ind]-CO2RelPermAq[ind])

        self.CO2MobilityAq = CO2RelPermAq/self.parameters.CO2Viscosity
        self.brineMobilityAq = brineRelPermAq/self.parameters.brineViscosity

        self.effectiveMobility = (self.interface*self.CO2MobilityAq+(
            self.thicknessH-self.interface)*self.brineMobilityAq)/self.thicknessH

        # For use in well functions
        for ind in range(0, self.nSL):
            if self.interface[ind] == 0:
                self.effectiveMobility[ind] = 1/self.parameters.brineViscosity

    def find(self):
        """ Find solution of the ROM corresponding to the provided parameters."""
        timeStep = self.parameters.timeStep*units.day()

        # Setup initial pressure, saturations, masses and interface height
        self.setup_initial_conditions()

        # Setup lambda and mobility
        self.find_lambda()

        self.get_mobility_for_aquifers()

        self.get_mobility_for_shales()

        # Initialize matrix for system of equations. AA*Pressure(Aquifers) = BB
        # LUT coupled: the pressure at the bottom of reservoir (or aquifer 1)
        # is known so it's not a variable
        StartingIdx = 1

        # tridiagonal (N-1)*(N-1) (N: # of aquifers including the bottom reservoir =  self.nSL)
        AA = np.zeros((self.nSL-StartingIdx, self.nSL-StartingIdx))
        BB = np.zeros(self.nSL-StartingIdx) # vector (N-1)*1

        # Calculate coefficients of Darcy equation for each aquifer for the matrix build-up
        # Refer to the document for the detailed information
        CC = self.CC_shale()
        GG = self.GG_shale()
        FF = self.FF_aquifer()
        WV = self.WV_aquifer()

        for ind in range(StartingIdx, self.nSL):

            F_below = FF[ind-1]
            if ind == StartingIdx:
                F_below = 0     # nothing below 1st shale layer

            F_zero = FF[ind]

            if ind < self.nSL-1:
                F_above = FF[ind+1]
            else:
                F_above = 0 # nothing above top shale layer

            C_below = CC[ind-1]
            C_zero = CC[ind]

            G_below = GG[ind-1]
            G_zero = GG[ind]

            # aquifer2 (bottom). Do not calculate the bottom reservoir due to LUT coupled case
            if ind == StartingIdx:
                # Top reservoir, not averaged, pressure from LUT
                ReservoirTopPressure = self.parameters.pressure

                AA[ind-StartingIdx, ind-StartingIdx] = 1 + WV[
                    ind] * C_below - WV[ind] * C_zero
                AA[ind-StartingIdx, ind + 1 - StartingIdx] = WV[ind] * C_zero

                BB[ind-StartingIdx] = WV[ind]*(
                    -C_below * F_zero - G_below + C_zero * F_zero + C_zero 
                    * F_above - G_zero - C_below * ReservoirTopPressure) # + delta_pressure[ind-1]

            elif ind == (self.nSL-1): # aquifer N (top)
                DatumPressure = self.parameters.datumPressure

                AA[ind-StartingIdx, ind-1-StartingIdx] = -WV[ind] * C_below
                AA[ind-StartingIdx, ind-StartingIdx] = 1 + WV[
                    ind] * C_below - WV[ind-1] * C_zero

                BB[ind-StartingIdx] = WV[ind-1] * (
                    -C_below * F_below - C_below * F_zero - G_below + C_zero 
                    * F_zero - G_zero - C_zero * DatumPressure) #+ delta_pressure[ind-1]

            else: # aquifer3 to aquifer N-1 (intermediate)

                AA[ind-StartingIdx, ind-1-StartingIdx] = -WV[ind] * C_below
                AA[ind-StartingIdx, ind-StartingIdx] = (
                    1 + WV[ind] * C_below - WV[ind] * C_zero)
                AA[ind-StartingIdx, ind+1-StartingIdx] = WV[ind] * C_zero

                BB[ind-StartingIdx] = WV[ind] * (
                    -C_below * F_below - C_below * F_zero - G_below + C_zero 
                    * F_zero + C_zero * F_above - G_zero) #+ delta_pressure[ind-1]

        # Calculate coupled pressure of each aquifer as solution of linear system
        # Vertically averaged delta pressure (compared to the initial values) at each aquifer
        deltaPave = np.linalg.solve(AA, BB)

        # The approach above can estimate very small changes in pressure, even 
        # when there is no change in pressure (e.g., an estimated pressure change 
        # that is, when normalized by the corresponding initial pressure, on 
        # the order of 0.1 % or less). If the pressure given is approximately 
        # equal to the initial pressure at the top of the reservoir, these lines 
        # statement manually scale down the deltaPave values.
        pressure_norm = self.parameters.pressure / self.initialPressureTopReservoir

        if APPROX_INITIAL_LOWER_BOUND <= pressure_norm <= APPROX_INITIAL_UPPER_BOUND:
            # As the pressure given gets closer to the exact initial pressure, 
            # scale the deltaPave further down. This approach is meant to allow for 
            # a gradual reduction in leakage rates as the pressure gets closer 
            # to the initial pressure at the top of the reservoir.
            deltaPave *= (np.abs(1 - pressure_norm) / INTERVAL)

        if StartingIdx == 1: # LUT coupled
            # Pressure in aquifers
            Pave = self.initialPressure[StartingIdx:] + deltaPave - FF[StartingIdx:]

            Pbot = Pave + FF[StartingIdx:]
            # Inclusion of the bottom reservoir pressure
            Pbot = np.append(self.initialPressure[0], Pbot)

            Ptop = Pave - FF[StartingIdx:]
            # Inclusion of the top reservoir pressure from LUT
            Ptop = np.append(ReservoirTopPressure, Ptop)

        # Find change in pressure to calculate flow rate
        deltaP = np.zeros(self.nSL)
        deltaP[0:self.nSL-1] = Ptop[0:self.nSL-1] - Pbot[1:self.nSL]
        deltaP[self.nSL-1] = Ptop[self.nSL-1] - self.parameters.datumPressure

        # Very small changes in pressure can be calculated even when the initial 
        # pressure is given, potentially due to rounding / losing significant digits.
        if APPROX_INITIAL_LOWER_BOUND <= pressure_norm <= APPROX_INITIAL_UPPER_BOUND:
            # As the pressure given gets closer to the exact initial pressure, 
            # scale the deltaP further down.
            deltaP *= (np.abs(1 - pressure_norm) / INTERVAL)

        # Coefficients in Darcy equation
        DarcyCoefCO2 = self.parameters.flowArea*self.parameters.shalePermeability*\
            self.CO2MobilityShale
        DarcyCoefCO2Shale = DarcyCoefCO2*(1/self.parameters.shaleThickness)
        GravityCO2Shale = DarcyCoefCO2*self.parameters.CO2Density*self.g

        DarcyCoefBrine = self.parameters.flowArea*self.parameters.shalePermeability*\
            self.brineMobilityShale
        DarcyCoefBrineShale = DarcyCoefBrine*(1/self.parameters.shaleThickness)
        GravityBrineShale = DarcyCoefBrine*self.parameters.brineDensity*self.g

        # Inflow volumetric rate along shale layers = inflow volumetric rate
        # into the aquifer above the shale layer
        self.CO2Q = np.zeros(self.nSL)
        self.brineQ = np.zeros(self.nSL)

        self.CO2Q[:] = (DarcyCoefCO2Shale*deltaP - GravityCO2Shale)
        self.brineQ[:] = (DarcyCoefBrineShale*deltaP - GravityBrineShale)

        # Set negative volumetric leakage rates to zero, if enabled
        if self.parameters.set_neg_lr_to_zero:
            self.CO2Q[self.CO2Q < 0] = 0
            self.brineQ[self.brineQ < 0] = 0

        # Inflow mass rate along shale layers = inflow mass rate
        # into the aquifer above the shale layer
        self.CO2LeakageRates = np.zeros(self.nSL)
        self.brineLeakageRates = np.zeros(self.nSL)

        self.CO2LeakageRates = self.CO2Q*self.parameters.CO2Density
        self.brineLeakageRates = self.brineQ*self.parameters.brineDensity

        # Update cumulative CO2 "volume" in each aquifer (not including the bottom reservoir)
        self.CO2Volume = self.CO2Volume + (
            self.CO2Q[0:self.nSL-1]-self.CO2Q[1:self.nSL])*timeStep

        self.CO2Volume = np.maximum(self.CO2Volume, np.zeros(self.nSL-1))

        self.get_interface()
        for j in range(self.nSL-1):
            self.CO2SaturationAq[j] = self.interface[j+1]/self.thicknessH[j+1]

    def get_mobility_for_shales(self):
        """ Calculate mobility parameter for brine and CO2 in the shale layers."""
        CO2RelPermShale = np.zeros(self.nSL)
        brineRelPermShale = np.ones(self.nSL)

        Sres = self.parameters.aquBrineResSaturation

        for ind in range(0, self.nSL):
            if self.interface[ind] > 0:
                CO2RelPermShale[ind] = min(1-Sres[ind],
                                           self.interface[ind]/self.thicknessH[ind])
                brineRelPermShale[ind] = 1/(1-Sres[ind])*(1-Sres[ind]-CO2RelPermShale[ind])
            else:
                CO2RelPermShale[ind] = 0.
                brineRelPermShale[ind] = 1.

        self.CO2MobilityShale = CO2RelPermShale/self.parameters.CO2Viscosity
        self.brineMobilityShale = brineRelPermShale/self.parameters.brineViscosity

    def find_lambda(self):
        """ Calculate lambda constant."""
        # Lambda is a constant in the formula for the outer egde of the plume
        # It is constant for each aquifer in the system unless
        # brine and CO2 have different relative permeabilities, residual
        # saturations or viscosities
        # For simple calculations, the mobility ratio Lambda is calculated at
        # the endpoint relative permeability values for the two phases.

        Sres = self.parameters.aquBrineResSaturation

        # Maximum value for brine relative permeability
        brinePermeability = np.ones(self.nSL)

        # Maximum value for CO2 relative permeability
        CO2Permeability = np.zeros(self.nSL)
        for ind in range(self.nSL):
            CO2Permeability[ind] = 1 - Sres[ind]

        brineViscosity = self.parameters.brineViscosity
        CO2Viscosity = self.parameters.CO2Viscosity

        self.Lambda = np.ones(self.nSL)*CO2Permeability*brineViscosity/(
            brinePermeability*CO2Viscosity)

    def get_interface(self):
        """ Calculate height of the interface between brine and CO2. """
        Sres = self.parameters.aquBrineResSaturation

        # Set only for aquifers (not including the reservoir layer)
        for j in range(1, self.nSL):

            if self.CO2Volume[j-1] > 0:
                x = 2*np.pi*(self.thicknessH[j])*(1-Sres[j])*0.15*(
                    self.parameters.wellRadius**2)/self.CO2Volume[j-1]

                if x < 2/self.Lambda[j]:
                    tempInterface = (1-Sres[j])*self.thicknessH[j]

                elif x >= 2*self.Lambda[j]:
                    tempInterface = 0.
                else:
                    tempInterface = 1/(self.Lambda[j]-1)*(
                        np.sqrt(2*self.Lambda[j]/x)-1)*self.thicknessH[j]

                self.interface[j] = min([tempInterface,
                                         (1-Sres[j])*self.thicknessH[j],
                                         self.interface[j-1]])

    def CC_shale(self):

        PermWellShale = self.parameters.shalePermeability
        ShaleThickness = self.parameters.shaleThickness

        CO2MobilityShale = self.CO2MobilityShale
        brineMobilityShale = self.brineMobilityShale

        CC = PermWellShale/ShaleThickness*(CO2MobilityShale+brineMobilityShale)

        return CC

    def GG_shale(self):

        PermWellShale = self.parameters.shalePermeability
        gg = self.g

        CO2Density = self.parameters.CO2Density
        brineDensity = self.parameters.brineDensity

        CO2MobilityShale = self.CO2MobilityShale
        brineMobilityShale = self.brineMobilityShale

        GG = PermWellShale*gg*(CO2MobilityShale*CO2Density+brineMobilityShale*brineDensity)

        return GG

    def FF_aquifer(self):

        SCO2 = self.interface*(1/self.thicknessH)
        gg = self.g

        CO2Density = self.parameters.CO2Density
        brineDensity = self.parameters.brineDensity

        AquiferThickness = self.thicknessH

        FF = AquiferThickness/2*gg*(SCO2*CO2Density+(1-SCO2)*brineDensity)

        return FF

    def WV_aquifer(self):

        SCO2 = self.interface*(1/self.thicknessH)

        CO2MobilityAq = self.CO2MobilityAq
        brineMobilityAq = self.brineMobilityAq

        EffMobility = SCO2*CO2MobilityAq+(1-SCO2)*brineMobilityAq

        cf_ave = self.parameters.compressibility

        tt = self.parameters.timePoint*units.day()
        EffTime = 0.92*tt

        rw = self.parameters.wellRadius
        # Reservoir perm not defined separately
        PermAquiferHor = np.append(self.parameters.aquiferPermeability[0],
                                   self.parameters.aquiferPermeability)

        uLeak = rw**2*cf_ave/(4*EffMobility*PermAquiferHor*EffTime)
        GLeak = well_function(uLeak)

        AquiferThickness = self.thicknessH
        WV = GLeak/(4*np.pi*EffMobility*AquiferThickness*PermAquiferHor)

        return WV


def read_data(filename):
    """ Routine used for reading pressure and saturation data files."""
    # Check whether the file with given name exists
    if os.path.isfile(filename):
        data = np.genfromtxt(filename)
        return data
    return None


def well_function(x):
    """
    Return the approximation of the well function with even number
    of terms in expansion. Expansions with an odd number of terms
    diverge to plus infinity without crossing zero.
    """
    W = np.zeros(len(x))
    for i, u in list(enumerate(x)):
        if u <= 1.0:
            W[i] = (-0.577216-np.log(u)+u-u**2/(2*scm.factorial(2))+
                    u**3/(3*scm.factorial(3))-u**4/(4*scm.factorial(4))+
                    u**5/(5*scm.factorial(5))-u**6/(6*scm.factorial(6))+
                    u**7/(7*scm.factorial(7))-u**8/(8*scm.factorial(8)))
        elif u <= 9:
            uu = np.linspace(1.0, 9.0, num=9)
            Wu = np.array([0.219, 0.049, 0.013, 0.0038, 0.0011,
                           0.00036, 0.00012, 0.000038, 0.000012])
            Wfun = interp1d(uu, Wu, kind='linear')
            W[i] = Wfun(u)
        else:
            W[i] = 0.000001
    return W

if __name__ == "__main__":
    xx = [5.0, 6, 4]
    w_val = well_function(xx)
    print(w_val)

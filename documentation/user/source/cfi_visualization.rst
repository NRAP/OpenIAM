.. _cfi_visualization: 
 
Visualization Options in the CFI 
================================ 
 
The plot types available within NRAP-Open-IAM are: ``TimeSeries``, ``TimeSeriesStats``, 
``TimeSeriesAndStats``, ``StratigraphicColumn``, ``Stratigraphy``, ``AoR``, ``TTFD``, 
``GriddedMetric``, ``GriddedRadialMetric``, ``AtmPlumeSingle``, ``AtmPlumeEnsemble``, 
``Bowtie``, ``SalsaProfile``, ``SalsaTimeSeries``, ``SalsaContourPlot``, and ``SalsaLeakageAoR``. 
This section reviews the process of creating plots in the control file interface 
(CFI) and then discusses each plot type separately. 
 
To create a figure using a simulation run with a *.yaml* control file, the 
the figure must be set up in the *Plots* section. Within the *Plots* section, the 
user can have multiple entries that each represent a different plot to be created. 
The names of these entries are defined by the user, and the names are generally used 
as the file name for the corresponding figure. An exception occurs with some 
plots. For example, with the ``TTFD`` plot type generates a large number of 
plots, and the final file names will depend on the input used 
(e.g., *TDS_Plume_Timings_Realization10.png*). The plot name provided can have 
extensions appended that specify the file type of the resulting figure file 
(e.g., *.png*, *.tiff*, or *.eps*). Below, we show two examples of ``TimeSeries`` 
plot entries in a *.yaml* control file. 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        Pressure_Figure:
            TimeSeries: [pressure]
        CO2_Sat_Figure.tiff:
            TimeSeries: [CO2saturation]

Since the first plot entry (*Pressure_Figure*) does not have an extension 
(e.g., *.png* or *.tiff*) appended at the end (e.g., *Pressure_Figure.tiff*), 
the produced figure will be of the default *.png* type. The second plot entry 
(*CO2_Sat_Figure.tiff*), however, includes *.tiff* at the end of the name; 
the resulting figure file will be of *.tiff* type. Note that if an extension 
is provided when using a plot type that generates names (e.g., the ``TTFD`` plot type), 
the generated names will still use the extension provided. 
 
When we refer to an entry as being indented beneath another entry, we 
mean that in a *.yaml* file the indented entry is on a lower line and preceeded 
by additional four spaces. The indentation of one entry carries over to all entries 
contained within that entry. In the example above, for example, *Pressure_Figure:*  
is indented beneath ``Plots:``, and ``TimeSeries: [pressure]`` is indented beneath 
*Pressure_Figure:*. ``TimeSeries`` is preceeded by eight spaces, while 
*Pressure_Figure* is preceeded by four spaces. Additionally, the name of each entry 
is followed by a colon (:). When entries have other entries indented beneath 
them, the colon will be the last character on that line (e.g., ``Plots:`` or 
``Pressure_Figure:``). When entries do not have other entries indented beneath 
them and instead take an input value or list, then the colon is followed by that 
input (e.g., ``TimeSeries: [pressure]``). Note that for the rest of this section, 
we will not include the colon when discussing an entry; it is assumed 
that each entry is followed by a colon. 
 
All plot types have certain entries that are either required or 
optional. Some of these optional entries are used by multiple plot types. To 
avoid repeating the definitions of these entries, we first present the optional 
entries used by multiple plot types. Then, we review each plot type. 
 
Each plot type has the optional entries ``FigureDPI`` and ``FigureSize``.
 
* ``FigureDPI`` - the dots-per-inch (DPI) of the resulting figure(s) (default is 
  100). Most figure types produce only one figure file, but plot types like 
  ``Stratigraphy`` and ``TTFD`` can produce multiple figures from one entry. 
  Larger DPIs will create high-resolution, high-quality figures, but the file 
  sizes are also larger. File size and quality are also influenced by the extension 
  used (e.g., *.png* or *.tiff*). Recommended ``FigureDPI`` values are between 
  100 and 300. 
 
* ``FigureSize`` - the width and height of the figure in inches. The width and 
  height must be entered as a list of length two (e.g., ``FigureSize: [15, 8]``), 
  with the width as the first number and the height as the second. The default 
  ``FigureSize`` for each plot type is given within the plot type's section further 
  below. This entry can also be provided as ``figsize`` instead of ``FigureSize``. 
 
The following entries are font-related options that apply to most plot types. There 
are some exceptions, however. For example, a plot type must have a colorbar for the 
``ColorbarFontSize`` option to apply. The default font sizes used for each plot type 
are listed in the plot type's section further below.

* ``GenFontSize`` - the font size for tick labels on the figure. 
* ``XAxisLabelFontSize`` - the font size for the ``x``-axis labels on the figure.
 
* ``YAxisLabelFontSize`` - the font size for the ``y``-axis labels on the figure.
 
* ``ZAxisLabelFontSize`` - the font size for the ``z``-axis labels on the figure. This entry 
  only works for a plot type that has a ``z`` axis (e.g., the ``Stratigraphy`` plot type).
 
* ``ColorbarFontSize`` - the font size for the colorbar label. This entry only works for 
  a plot type that has a colorbar.
 
* ``TitleFontSize`` - the font size for figure titles.
 
* ``LegendFontSize`` the font size for figure legends.
 
* ``FontName`` the font type to use for the figure. The default value is Arial. Choosing certain 
  font types might result in errors. For example, the formatting of certain symbols might not work 
  properly for some font types. If the input provided is not recognized as a font type, the simulation 
  will log a warning message and use the default font type of Arial.
 
The ``TimeSeries``, ``TimeSeriesStats``, ``TimeSeriesAndStats``, ``AoR``, 
``StratigraphicColumn``, and ``Stratigraphy`` plot types have the optional 
entry ``Title``. 
 
* ``Title`` - the text placed in bold at the top of the figure. If the ``Title`` 
  entry is given for ``StratigraphicColumn`` or ``Stratigraphy`` plots, 
  it replaces the default titles. For the remaining plot types, by default, there is 
  no title for the overall figure, only titles for each individual subplot generated 
  based on the output shown (with ``AoR`` plots having one subplot). Note that 
  if one wants to have |CO2| in a title, one should enter it as 'CO$_2$' 
  (the $$ symbols aid in the proper formatting). 
 
Below , we show examples of ``FigureDPI``, ``FigureSize``, and ``Title`` entries 
used in a *.yaml* control file. 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        Strat_Col:
            StratigraphicColumn:
                Title: Stratigraphy
                FigureDPI: 200
                FigureSize: [8, 12]
        Stratigraphy_Figure.tiff:
            Stratigraphy:
                Title: Stratigraphy
                FigureDPI: 300
                FigureSize: [13, 11]
        Pressure_Figure:
            TimeSeriesStats: [pressure]
            FigureDPI: 100
            FigureSize: [12, 8]
            Title: Pressure Results
        TDS_Volume_AoR.tiff:
            AoR: [Dissolved_salt_volume]
            FigureDPI: 200
            FigureSize: [12, 12]
            Title: AoR Results, Dissolved Salt Impact
        TTFD_Figures:
            TTFD:
                PlumeType: Dissolved_salt
                ComponentNameList: [GenericAquifer1, GenericAquifer2]
                FigureDPI: 200
                FigureSize: [12, 11]
        Gridded_Brine_Aquifer:
            GriddedMetric:
                ComponentNameList: [FaultFlow1]
                MetricName: brine_aquifer
                FigureDPI: 200
                FigureSize: [9, 9]
        Gridded_Salt_Mass_Frac:
            GriddedRadialMetric:
                ComponentNameList: [GenericAquifer1]
                MetricName: Dissolved_salt_mass_fraction
                FigureDPI: 200
                FigureSize: [11, 9]
        Atmospheric_Plume_Single:
            AtmPlumeSingle:
                FigureDPI: 200
                FigureSize: [16, 12]
        Atmospheric_Plume_Probability.tiff:
            AtmPlumeEnsemble:
                FigureDPI: 300
                FigureSize: [14, 11]
 
Notice that the ``FigureDPI`` and ``FigureSize`` entries for the ``StratigraphicColumn``, 
``Stratigraphy``, ``TTFD``, ``GriddedMetric``, ``GriddedRadialMetric``, ``AtmPlumeSingle``, 
and ``AtmPlumeEnsemble`` plots are indented under the plot type. In contrast, the ``FigureDPI``, 
``FigureSize``, and ``Title`` entries for the ``TimeSeriesStats`` and ``AoR`` plots are not 
indented beneath the plot type. This discrepancy occurs because the ``TimeSeriesStats`` and 
``AoR`` entries are followed by a metric (e.g., [**pressure**]), while the other plot type 
entries are not. 
 
The ``StratigraphicColumn`` and ``Stratigraphy`` plot types both have the optional entries 
``ReservoirColor``, ``ShaleColor``, ``AquiferColor``, ``ReservoirAlpha``, ``ShaleAlpha``, 
``AquiferAlpha``, ``ReservoirLabel``, ``Shale#Label``, and ``Aquifer#Label`` (where ``#`` 
is a particular unit number). Note that the color and alpha entries containing ``Shale`` 
and ``Aquifer`` can be used with a number specifying a certain shale or aquifer (e.g., 
``Shale2Color`` or ``Aquifer1Alpha``). Without a specific number, these entries will apply 
to all shales or aquifers (excluding units that have their own, separate entry of the same 
type). Note that the color entries can be a string (e.g., ``ReservoirColor: orange`` or 
``Aquifer2Color: g``) or a list of length three representing fractions of red, green, 
and blue (``Aquifer2Color: [0.25, 0.25, 1]``). The entries containing ``Alpha`` set the 
alpha values used in the plot. Alpha values range from 0 to 1 and control transparency, 
with 1 being fully opaque and values approaching 0 becoming more transparent. For examples 
showing the use of these entries, see *ControlFile_ex33b*. To prevent a label from being 
shown, a label entry can be given as '' (e.g., ``Shale2Label: ''``). One might not want a 
label if the setup causes overlap between different labels. 
 
* ``ReservoirColor`` - the color used when plotting the reservoir. The default is [0.33, 0.33, 0.33]. 
 
* ``ShaleColor`` - the color used when plotting all shales (or a specific shale, if given as 
  ``Shale#Color``, where ``#`` is an appropriate unit number). The default is red. 
 
* ``AquiferColor`` - the color used when plotting all aquifers (or a specific aquifer, if given as 
  ``Aquifer#Color``, where ``#`` is an appropriate unit number). The default is blue. 
 
* ``ReservoirAlpha`` - the alpha value used when plotting the reservoir. The default value is 
  0.5. 
 
* ``ShaleAlpha`` - the alpha value used when plotting all shales (or a specific shale, if given as 
  ``Shale#Alpha``, where ``#`` is an appropriate unit number). The default value is 0.25. 
 
* ``AquiferAlpha`` - the alpha value used when plotting all aquifers (or a specific aquifer, if given 
  as ``Aquifer#Alpha``, where ``#`` is an appropriate unit number). The default value is 0.25. 
 
* ``ReservoirLabel`` - the label displayed for the reservoir. In ``StratigraphicColumn`` plots, the 
  default label is "Reservoir Thickness: H m," where H is the unit thickness. In ``Stratigraphy`` 
  plots, the default label is "Reservoir." 
 
* ``Shale#Label`` - the label displayed a specific shale, where ``#`` is an appropriate unit number. 
  In ``StratigraphicColumn`` plots, the default label is "Shale # Thickness: H m," where H is the 
  unit thickness. In ``Stratigraphy`` plots, the default label is "Shale #." 
 
* ``Aquifer#Label`` - the label displayed a specific aquifer, where ``#`` is an appropriate unit 
  number. In ``StratigraphicColumn`` plots, the default label is "Aquifer # Thickness: H m," where 
  H is the unit thickness. In ``Stratigraphy`` plots, the default label is "Aquifer #." 

The ``Stratigraphy``, ``TTFD``, ``GriddedMetric``, ``GriddedRadialMetric``, ``AtmPlumeSingle``, 
and ``AtmPlumeEnsemble`` plot types all have the optional entries ``PlotInjectionSites``, 
``InjectionCoordx``, ``InjectionCoordy``, ``SpecifyXandYLims``, and ``SaveCSVFiles``. The 
``SalsaContourPlot`` and ``SalsaLeakageAoR`` plot types can also be given the optional entries 
``PlotInjectionSites``, ``PlotWellbores``, and ``SpecifyXandYLims``. ``Stratigraphy`` and 
``SalsaContourPlot`` plots can be given the optional entry ``PlotWellbores``. The ``Bowtie``, 
``SalsaProfile``, ``SalsaTimeSeries``, and ``SalsaContourPlot`` plot types can also be given 
``SaveCSVFiles``.
 
* ``PlotInjectionSites`` - an option to plot injection sites. The default value is ``False`` for 
  all plot types except for ``SalsaContourPlot`` and ``SalsaLeakageAoR``, which have default values 
  of ``True``. The only acceptable values are ``True`` or ``False``. 
 
* ``InjectionCoordx`` - value or list of values for the x coordinate(s) of 
  injection site(s) (default is None). The values are given in meters. This entry must 
  be provided when using a ``LookupTableReservoir``, as that component type does 
  not have an *.injX* attribute. Other reservoir types like ``AnalyticalReservoir`` 
  can be displayed without an ``InjectionCoordx`` entry. 
 
* ``InjectionCoordy`` - value or list of values for the y coordinate(s) of 
  injection site(s) (default is None). The values are given in meters. This entry must 
  be provided when using a ``LookupTableReservoir``, as that component type does 
  not have an *.injY* attribute. Other reservoir types like ``AnalyticalReservoir``  
  can be displayed without an ``InjectionCoordy`` entry. 
 
* ``PlotWellbores`` - an option to plot wellbore locations (default is ``True``). In 
  ``Stratigraphy`` plots, the wellbores are shown as vertical lines. The only acceptable 
  values are ``True`` or ``False``. 
 
* ``SaveCSVFiles`` - an option to save results in *.csv* files. The only acceptable 
  values are ``True`` or ``False``. The default value for all plot types is ``True`` 
  except for ``Stratigraphy`` plots, which have a default value of ``False``. For 
  ``Stratigraphy`` plots, the *.csv* files contain unit thicknesses and depths across 
  the domain. 
 
If set up, ``SpecifyXandYLims`` is a dictionary containing two entries: ``xLims`` 
and ``yLims`` (i.e., ``xLims`` and ``yLims`` are indented beneath ``SpecifyXandYLims`` 
in a *.yaml* file). 
 
* ``SpecifyXandYLims`` - a dictionary containing two optional entries related 
  to the limits of the figure's x and y axes (default is None). Within 
  this dictionary are the entries ``xLims`` and ``yLims``. 
 
* ``xLims`` - an entry under ``SpecifyXandYLims`` containing a list of length two 
  that represents the ``x``-axis limits (e.g., ``xLims: [0, 1000]``; default is None). 
  The values are given in meters. The first and second values in the list are the 
  lower and upper limits, respectively. If ``xLims`` is not provided or provided 
  incorrectly, the figure will use the default approach for setting the 
  ``x``-axis limits. 
 
* ``yLims`` - an entry under ``SpecifyXandYLims`` containing a list of length two 
  that represents the ``y``-axis limits (e.g., ``yLims: [0, 1000]``; default is None). 
  The values are given in meters. The first and second values in the list are the 
  lower and upper limits, respectively. If ``yLims`` is not provided or provided 
  incorrectly, the figure will use the default approach for setting the 
  ``y``-axis limits. 
 
The ``Stratigraphy``, ``TTFD``, and ``AtmPlumeEnsemble`` plots also have the optional 
entry ``SpecifyXandYGridLims``, which is a dictionary containing the ``gridXLims`` and 
``gridYLims`` entries. ``AoR`` plots do not have grid entries because the x and y values 
used are those of the wellbore components. ``GriddedRadialMetric`` plots use the 
radial grids produced by a component (e.g., a ``GenericAquifer`` component), while 
``GriddedMetric`` plots use only the locations corresponding with the output. 
 
* ``SpecifyXandYGridLims`` - a dictionary containing two optional entries 
  related to the x and y limits for the gridded data evaluated (default is None). 
  In ``Stratigraphy`` plots, the gridded data are the three-dimensional planes 
  depicting the the top of each unit. For ``TTFD`` and ``AtmPlumeEnsemble`` plots, the 
  gridded data are the color-labelled values. Within this dictionary are the 
  entries ``gridXLims`` and ``gridYLims``. 
 
* ``gridXLims`` - an entry under ``SpecifyXandYGridLims`` containing a list of 
  length two that represents the ``x``-axis limits for the grid used to evaluate results 
  (e.g., ``gridXLims: [100, 900]``; default is None). The values for ``gridXLims`` are 
  in meters. The first and second values in the list are the lower and upper 
  limits, respectively. If ``gridXLims`` is not provided or provided incorrectly, 
  the figure will use the default approach for creating the gridded values. 
 
* ``gridYLims`` - n entry under ``SpecifyXandYGridLims`` containing a list of 
  length two that represents the ``y``-axis limits for the grid used to evaluate results 
  (e.g., ``gridYLims: [100, 900]``; default is None). The values for ``gridYLims`` are 
  in meters. The first and second values in the list are the lower and upper 
  limits, respectively. If ``gridYLims`` is not provided or provided incorrectly, 
  the figure will use the default approach for creating the gridded values. 
 
The ``Stratigraphy``, ``TTFD``, and ``AtmPlumeEnsemble`` plot types can all use 
the optional entries ``xGridSpacing`` and ``yGridSpacing``: 
 
* ``xGridSpacing`` - a horizontal distance (|m|) used as the interval between the 
  grid points in the x-direction (default is None). If this entry is not provided, 
  the x-coordinates of the grid points are defined using a default approach 
  (1/100th of the range in x-values). 
 
* ``yGridSpacing`` - a horizontal distance (|m|) used as the interval between the 
  grid points in the y-direction (default is None). If this entry is not provided, 
  the y-coordinates of the grid points are defined using a default approach 
  (1/100th of the range in y-values). 
 
The ``AoR``, ``GriddedMetric``, ``GriddedRadialMetric``, ``Bowtie``, ``SalsaContourPlot``, 
and ``SalsaLeakageAoR`` plot types have the optional entry ``TimeList``: 
 
* ``TimeList`` - a list specifying the times (in years) for which to create separate figures 
  (e.g., ``TimeList: [1, 5, 10]`` for 1, 5, and 10 years). Otherwise, one figure can be 
  created for each timestep by having ``TimeList: All``. If ``TimeList`` is not entered for 
  ``AoR`` or ``SalsaLeakageAoR`` plots, the figures created will show the maximum values for 
  all locations across all model times. If ``TimeList`` is not entered for a ``Bowtie`` plot, 
  the figure will relfect model results across all model times. If ``TimeList`` is not entered 
  for a ``GriddedMetric``, ``GriddedRadialMetric``, or ``SalsaContourPlot`` plot, the default 
  setting is ``TimeList: All``. 
 
The ``TTFD``, ``GriddedMetric``, and ``GriddedRadialMetric`` plot types all have the 
required entry ``ComponentNameList``: 
 
* ``ComponentNameList`` - a list containing the names provided for each of the 
  components producing output to be used for the creation of the figures (e.g., 
  ``ComponentNameList: [FutureGen2AZMI1, FutureGen2AZMI2]`` in 
  *ControlFile_ex40.yaml*). Below, we show a section of the *.yaml* file for 
  *ControlFile_ex40.yaml*. This section demonstrates where the name is provided 
  for the *FutureGen2AZMI2* component. Below the excerpt is an example of how 
  component names are set when using NRAP-Open-IAM in a script application. 
 
Excerpt from *ControlFile_ex40* demonstrating how an aquifer component is given 
the name FutureGen2AZMI2: 
 
.. code-block:: python
   :lineno-start: 1

    FutureGen2AZMI2:
        Type: FutureGen2AZMI
        Connection: MultisegmentedWellbore1
        AquiferName: aquifer3
        Parameters:
            por: 0.132
            log_permh: -12.48
            log_aniso: 0.3
            rel_vol_frac_calcite: 0.1
        Outputs: [pH_volume, TDS_volume, Dissolved_CO2_volume,
                  Dissolved_CO2_dx, Dissolved_CO2_dy, Dissolved_CO2_dz]

Example of setting the component name (*FutureGen2AZMI2*) in a script application: 
 
.. code-block:: python
   :lineno-start: 1

    fga = sm.add_component_model_object(FutureGen2AZMI(name='FutureGen2AZMI2', parent=sm))

The ``GriddedMetric`` and ``GriddedRadialMetric`` plot types both have the required entry 
``MetricName``: 

* ``MetricName`` - the name of the metric to plot. For a ``GriddedMetric`` plot, the 
  ``SealHorizon`` and ``FaultFlow`` outputs **CO2_aquifer**, **brine_aquifer**, 
  **mass_CO2_aquifer**, or **mass_brine_aquifer** can be provided for ``MetricName``. For a 
  ``GriddedRadialMetric`` plot, the ``GenericAquifer`` outputs **Dissolved_CO2_mass_fraction** 
  or **Dissolved_salt_mass_fraction** can be provided for ``MetricName``. When plotting these 
  ``GenericAquifer`` metrics, the component used must also produce **r_coordinate** and 
  **z_coordinate** outputs. 
 
The ``GriddedMetric``, ``GriddedRadialMetric``, ``AtmPlumeSingle``, `SalsaProfile``, 
``SalsaTimeSeries``, and ``SalsaContourPlot`` plot types have the optional entry ``Realization``: 
 
* ``Realization`` - the realization number for which to display results (default is 0). 
  Note that this optional input is only used in simulations using Latin Hypercube Sampling 
  (``lhs``) and Parameter Study (``parstudy``) analysis types. This input uses the indexing 
  rules in Python, where 0 represents the first realization and (N - 1) represents the last 
  (where N is the number of realizations). 
 
The ``GriddedMetric`` and ``GriddedRadialMetric`` plot types both have the optional entry 
``EqualAxes``: 
 
* ``EqualAxes`` - the option to force the x and y axes to cover the same distances (for an equal 
  aspect ratio). The acceptable values are ``True`` or ``False``, and the default value is ``True``. 
  If set to ``True``, the axes limits given with ``xLims`` and ``ylims`` will not be used. 

The ``SalsaProfile``, ``SalsaTimeSeries``, and ``SalsaContourPlot`` plot types all have the 
optional entry ``MetricType``:

* ``MetricType`` - the type of output shown in the figure. This entry can be given as ``head`` or 
  ``pressure``, and the default setting is ``head``. When set to ``head``, hydraulic head outputs 
  will be required from the ``SALSA`` component used (e.g., **headLoc#Aquifer#**, **headProfile#VertPoint#Shale#**, 
  **allHeadProfilesShales**, and **contourPlotAquiferHead**). When set to ``pressure``, pressure 
  outputs will be required from the ``SALSA`` component used (e.g., **pressureLoc#Aquifer#**, 
  **pressureProfile#VertPoint#Shale#**, **allPressureProfilesShales**, and **contourPlotAquiferPressure**).
  
Examples of setting up each plot type in a *.yaml* file are shown in the sections below. 
 
TimeSeries, TimeSeriesStats, and TimeSeriesAndStats 
--------------------------------------------------- 
 
The ``TimeSeries``, ``TimeSeriesStats``, and ``TimeSeriesAndStats`` plot types 
are used to display results varying over time. Although this section 
covers three plot types, these plot types are different variations of 
the same type of plot. 
 
``TimeSeries`` plots are line plots of results varying over time. The number 
of lines in the resulting figure depends on the setup of the scenario. For example, 
components and associated locations entered in the *.yaml* file can define the 
number of curves shown in the figure but only the components that produce the metric 
being plotted (e.g., **pressure** or **brine_aquifer1**) influence the number 
of lines created for that particular metric. 
 
``TimeSeriesStats`` and ``TimeSeriesAndStats`` plots can only be produced for simulations 
using the Latin Hypercube Sampling (LHS, ``lhs`` in the CFI) or Parameter Study (``parstudy`` 
in the CFI) analysis types (not the ``forward`` analysis type). Simulations using the ``lhs`` 
and ``parstudy`` analysis types create separate simulations (i.e., different realizations) that 
explore the parameter space. The parameters varied are those entered with minimum and maximum 
values, which are meant to model a uniform distribution. Consider, for example, a 
``TimeSeriesStats`` plot set up for an LHS run with 30 realizations. The ``ModelParams`` 
section of the *.yaml* file would be similar to this excerpt from *ControlFile_ex4a.yaml*: 
 
.. code-block:: python
   :lineno-start: 1

    ModelParams:
        EndTime: 10
        TimeStep: 1.0
        Analysis:
            Type: lhs
            siz: 30
        Components: [AnalyticalReservoir1,
                     OpenWellbore1,
                     CarbonateAquifer1]
        OutputDirectory: output/output_ex4a_{datetime}
        Logging: Debug
 
The entries ``Type: lhs`` and ``siz: 30`` under ``Analysis`` specify the run as an 
LHS simulation with 30 realizations. Each realization will use different values 
for the parameters that are set up to vary. In a ``TimeSeries`` plot, the outputs for 
each realization will be represented by separate lines. 
 
If an ``lhs`` or ``parstudy`` simulation uses many realizations and many component locations, 
a ``TimeSeries`` plot could become visually unclear. To avoid a lack of visual 
clarity, ``TimeSeriesStats`` plots show basic information about the distribution 
of results over time. The plot produces lines representing mean and median values as well 
as shaded regions showing the four quartiles of the distribution varying over time 
(0th to 25th, 25th to 50th, 50th to 75th and 75th to 100th percentiles). 
 
``TimeSeriesAndStats`` plots combine the approaches of ``TimeSeries`` 
and ``TimeSeriesStats`` plots. The mean, median, and quartiles are shown along 
with line graphs for each realization. 
 
The default figure size and font sizes for these plot types are:
 
* ``FigureSize`` - [13, 8] ([width, height], in inches)
 
* ``GenFontSize`` - 11
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, and ``TitleFontSize`` - 12
 
* ``LegendFontSize`` - 8
 
The ``TimeSeries``, ``TimeSeriesStats``, and ``TimeSeriesAndStats`` plot types are unique 
in that the font sizes are automatically adjusted based on the figure size and the number 
of subplots (more subplots results in smaller font sizes). If a font size entry is provided 
by the user, however, that font size will not be automatically adjusted.

``TimeSeries``, ``TimeSeriesStats``, and ``TimeSeriesAndStats`` plots can have the following 
optional entries: ``UseMarkers``, ``VaryLineStyles``, ``UseLines``, ``Subplot``, ``ShowLegend``, 
``Title``, ``FigureDPI``, and ``FigureSize`` (the latter three are described above). Note that 
``Subplot`` is a dictionary containing the optional entries ``Use``, ``NumCols``, 
``EnforceYLims``, ``SaveSpace``, and ``ShowRange`` (i.e., these entries are on a line beneath 
``Subplot`` and preceeded by four more spaces). 
 
* ``UseMarkers`` - an option to show results with values annotated with markers 
  like circles and squares (default is ``False``). The only acceptable values 
  are ``True`` or ``False``. If markers are used, the colors of markers and lines 
  will vary in the normal manner (i.e., a rotation through the default matplotlib 
  color order of 10 colors, then 10 darker versions of those colors, and then 
  10 lighter versions of those colors). 
 
* ``VaryLineStyles`` - an option to vary the line styles used (default is ``False``). 
  The only acceptable values are ``True`` or ``False``. The matplotlib line styles 
  used are 'solid', 'dotted', 'dashed', 'dashdot', 'dotted with greater spacing', 
  'dash-dot-dot', and 'dashed with longer dashes'. Line colors will still vary in 
  the normal manner. 
 
* ``UseLines`` - an option to show results with lines (default is ``True``). The only 
  acceptable values are ``True`` or ``False``. If neither markers nor lines are used, 
  the plot will not show any results. One should only set ``UseLines`` to ``False`` 
  if ``UseMarkers`` is set to ``True``. If ``UseLines`` is set to ``False``, 
  ``VaryLineStyles``  will automatically be set to ``False``, regardless 
  of the entry provided in the *.yaml* file. 
 
* ``ShowLegend`` - an option to show a legend within each subplot (default is ``True``). The only 
  The legend will indicate the name of the component that produced the output plotted, as well 
  as the location of the output. For example, a pressure output produced by a component named 
  "Reservoir1" would have "Reservoir1 at location 0" (location indices begin at 0). If multiple 
  metrics are plotted within one figure (with ``Use`` set to ``False``), each legend entry will 
  also specify the metric name. When plotting many observations in one figure, the legend may become 
  too large, so the user might want to disable the legend (with ``ShowLegend`` set to ``False``).
 
* ``Subplot`` - a dictionary containing the optional entries ``Use``, ``NumCols``, 
  ``EnforceYLims``, and ``ShowRange``. This entry can also be provided as ``subplot``. 
 
* ``Use`` - the option to use multiple subplots (``True``) or not (``False``). The defalt 
  value is ``True``. The different subplots can show the results for different locations 
  and/or the results for different metrics (e.g., **pressure** in one subplot, 
  **CO2saturation** in another). If different types of output (e.g., **pressure** and 
  **CO2saturation**) are included in a ``TimeSeries`` plot but ``Use`` is set to ``False``, 
  the ``y``-axis label will only reflect one of the two output types. This entry can also be 
  provided as ``use``. 
 
* ``NumCols`` - the number of columns used to set up the subplots, if ``Use`` is set to 
  ``True``. If the plot includes 3 or fewer metrics (influenced by the output type(s) and 
  locations used), the default value is 1. If the plot includes 4 or more metrics, the 
  default value is 2. This entry can also be given as ``ncols``. The number of rows 
  is taken as the number required to plot the metrics used by the plot (given the number 
  of columns). The number of metrics is set by the number of output types given (e.g., 
  two for ``TimeSeries: [pressure, CO2saturation]``) and the number of locations for 
  those output types. For example, if ``NumCols`` is two, then the number of rows will 
  be half the number of metrics (rounding up to the next integer). Note that ``TimeSeries``, 
  ``TimeSeriesStats``, and ``TimeSeriesAndStats`` plots will automatically adjust the font 
  sizes used to seek to avoid text overlapping with other features. Accomplishing a specific 
  subplot configuration without the text becoming too small, for example, can be aided by 
  also changing the ``FigureSize`` entry (see above). 
 
* ``EnforceYLims`` - the option to set the ``y`` axis limits of each subplot fixed to the range 
  of values for that output type. For example, subplots with different kinds of leakage rates 
  (to different aquifers or to the atmosphere) will have the same ``y`` axis limits. This approach 
  is meant to allow the data to be compared more easily. This entry must be set to ``True`` or 
  ``False``, and the default value is ``True``. This option does not work when ``Use`` is set 
  to ``False`` (so there is one plot in the figure) and different output types are included in 
  the plot. |CO2| and brine leakage rates to different aquifers are treated as the same output 
  type, for example, while pressures and brine leakage rates are not. 
 
* ``SaveSpace`` - option to save space within the figure by only displaying ``x``-axis labels ("Time (years)") 
  for the lowest subplot in each column of subplots. This entry must be ``True`` or ``False``, and the 
  default setting is ``True``. By only displaying the ``x``-axis label for the lowest subplots, there is less 
  potential for text on the figure to become crowded or overlapping.
 
* ``ShowRange`` - the option for each subplot title to show the range of values for the 
  corresponding output. This entry must be set to ``True`` or ``False``, and the default value 
  is ``True``. This option does not work when ``Use`` is set to ``False`` (so there is one plot 
  in the figure) and different output types are included in the plot. Showing the ranges for many 
  different output types in the same title could cause the title to become too long, potentially 
  extending beyond the edges of the figure. 
 
These optional entries are not indented under ``TimeSeries`` or ``TimeSeriesAndStats`` in a 
*.yaml* file, but are instead indented under the figure name. If ``UseMarkers``, 
``VaryLineStyles``, or ``UseLines`` are provided for a ``TimeSeriesStats`` plot, the 
entries will have no effect (i.e., they do not influence the mean and median lines or the 
shaded quartiles). 
 
The titles for individual subplots are generated based on the metric shown in the subplot 
(i.e., output type and location the output was produced for). One can specify the subplot 
title that will correspond to a specific output, however, by entering the output name 
under ``Subplot``. The output name depends on the corresponding component, the output type, 
and the location index. For example, an ``AnalyticalReservoir`` component named AnalyticalReservoir1 
producing pressure at location 0 will result in an output name of ``AnalyticalReservoir1_000.pressure``. 
The component name is followed by an underscore, then the location index (starting at 0 and 
expressed with three digits), then a period, and finally the output name (e.g., **pressure**). 
The text used for the title is given after the output name as 
``ComponentName_000.OutputName: Text Used for Title``. For an example of this approach, see 
*ControlFile_ex1a*. 
 
Below, we show examples of ``TimeSeries`` and ``TimeSeriesAndStats`` plots in a *.yaml* 
control file. 

.. code-block:: python
   :lineno-start: 1

    Plots:
        Pressure_and_Sat:
            TimeSeries: [pressure, CO2saturation]
            UseMarkers: False
            UseLines: True
            VaryLineStyles: True
            FigureDPI: 150
            Subplot:
                Use: True
                NumCols: 2
                AnalyticalReservoir1_000.pressure: 'Pressure at Well 0'
                AnalyticalReservoir1_001.pressure: 'Pressure at Well 1'
                AnalyticalReservoir1_000.CO2saturation: 'CO$_2$ Saturation at Well 0'
                AnalyticalReservoir1_001.CO2saturation: 'CO$_2$ Saturation at Well 1'
        Pressure_Stats:
            TimeSeriesAndStats: [pressure]
            UseMarkers: True
            UseLines: False
            VaryLineStyles: False
            FigureDPI: 400
 
For examples of ``TimeSeries`` plots, see control file examples 1a, 1b, 1c, 2, 3, 7a, 
7b, and 14. For examples of ``TimeSeriesStats`` plots, see control file examples 
4a, 4b, 6, 8, 15, and 39. For examples of ``TimeSeriesAndStats`` plots, see control 
file examples 4a, 14, and 40. For script examples using the ``TimeSeries`` plot, see 
*iam_sys_reservoir_mswell_4aquifers_timeseries.py* and 
*iam_sys_reservoir_mswell_4aquifers_timeseries_2locs.py*. 
 
StratigraphicColumn 
------------------- 
 
``StratigraphicColumn`` plots show unit thicknesses and depths at one location. If the simulation 
does not use spatially variable stratigraphy (e.g., the ``Stratigraphy`` component), 
then the unit thicknesses at the one location used are representative of the entire domain. 
If the simulation does use spatially variable stratigraphy (e.g., the ``DippingStratigraphy`` 
or ``LookupTableStratigraphy`` components), then the plot shows the calculated unit thicknesses 
for the location used. 
 
The default figure size and font sizes for the ``StratigraphicColumn`` plot type are:
 
* ``FigureSize`` - [6, 10] ([width, height], in inches)
 
* ``GenFontSize`` - 12
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, and ``TitleFontSize`` - 14
 
The ``StratigraphicColumn`` plot type has the following optional entries: ``XValue``, 
``YValue``, ``DepthText``, ``ReservoirColor``, ``ShaleColor``, ``AquiferColor``, 
``ReservoirAlpha``, ``ShaleAlpha``, ``AquiferAlpha``, ``ReservoirLabel``, ``ShaleLabel``, 
``AquiferLabel``, ``FigureDPI``, ``FigureSize``, and ``Title``. All of these entries but 
``XValue``, ``YValue``, and ``DepthText`` are described above. 
 
* ``XValue`` - the x-coordinate (|m|) of the location used for the plot. 
  The default value is is 0 m. 
 
* ``YValue`` - the y-coordinate (|m|) of the location used for the plot. 
  The default value is is 0 m. 
 
* ``DepthText`` - option specifying whether to show depths at each unit interface 
  (``True``) or not (``False``). The default value is ``True``. One may want 
  to disable the depth text if, for example, certain units are so thin that 
  the text for different units plot on top of each other. 
 
Two examples of ``StratigraphicColumn`` plots in a *.yaml* control file are 
shown below. One plot does not include any optional entries and, therefore, uses 
the default options. The other plot includes a variety of optional entries. 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        Strat_Col_Default:
            StratigraphicColumn:
        Strat_Col_With_Options.tiff:
            StratigraphicColumn:
                Title: Stratigraphy
                Shale1Label: Lower Aquitard
                Shale2Label: Middle Aquitard
                Shale3Label: Upper Aquitard
                Aquifer1Label: AZMI
                Aquifer2Label: Freshwater Aquifer
                ReservoirLabel: Storage Reservoir
                ReservoirColor: darkmagenta
                Shale1Color: [0.33, 0.33, 0.33]
                Shale2Color: olive
                Shale3Color: rosybrown
                Aquifer1Color: deeppink
                Aquifer2Color: mediumturquoise
                Shale1Alpha: 0.3
                Shale2Alpha: 0.3
                Shale3Alpha: 0.45
                Aquifer1Alpha: 0.3
                Aquifer2Alpha: 0.45
                ReservoirAlpha: 0.45
                FigureDPI: 300
                XValue: 2000
                YValue: 2000
                DepthText: False
 
For more examples of ``StratigraphicColumn`` plots, see control file examples 
*ControlFile_ex33a*-*ControlFile_ex38c*. 
 
Stratigraphy 
------------ 
 
``Stratigraphy`` plots are three-dimensional plots showing the specified 
stratigraphy as well as features like wellbores and injection sites. These plots 
work with all stratigraphy component types (``Stratigraphy``, ``DippingStratigraphy``, 
and ``LookupTableStratigraphy``). 
 
The default figure size and font sizes for the ``Stratigraphy`` plot type are:
 
* ``FigureSize`` - [12, 10] ([width, height], in inches)
 
* ``GenFontSize`` - 12
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, ``ZAxisLabelFontSize``, and ``TitleFontSize`` - 14
 
``Stratigraphy`` plots can have the following optional entries: ``PlotWellbores``, 
``PlotWellLabels``, ``WellLabel``, ``PlotInjectionSites``, ``PlotInjectionSiteLabels``, 
``InjectionCoordx``, ``InjectionCoordy``, ``PlotStratComponents``, 
``StrikeAndDipSymbol``, ``SpecifyXandYLims``, ``SpecifyXandYGridLims``, 
``xGridSpacing``, ``yGridSpacing``, ``View``, ``SaveCSVFiles``, ``ReservoirColor``, 
``ShaleColor``, ``AquiferColor``, ``ReservoirAlpha``, ``ShaleAlpha``, ``AquiferAlpha``, 
``ReservoirLabel``, ``Shale#Label``, ``Aquifer#Label``, ``FigureDPI``, 
``FigureSize``, and ``Title``. Four of these entries (``StrikeAndDipSymbol``, 
``SpecifyXandYLims``, ``SpecifyXandYGridLims``, and ``View``) are dictionaries 
containing additional entries (i.e., more entries indented beneath them in a 
*.yaml* file). The entries ``PlotWellbores``, ``SpecifyXandYLims``, ``SpecifyXandYGridLims``, 
``xGridSpacing``, ``yGridSpacing``, ``SaveCSVFiles``, ``PlotInjectionSites``, 
``InjectionCoordx``, ``InjectionCoordy``, ``ReservoirColor``, ``ShaleColor``, 
``AquiferColor``, ``ReservoirAlpha``, ``ShaleAlpha``, ``AquiferAlpha``, ``ReservoirLabel``, 
``Shale#Label``, ``Aquifer#Label``, ``FigureDPI``, and ``FigureSize`` are 
described above. 
 
* ``PlotWellLabels`` - an option to show text labels specifying wellbore types 
  and numbers (default is ``True``). If ``WellLabel`` is not entered, labels will 
  be set according to the wellbore component type. For example, the labels could be 
  "Open Wellbore 1" for an Open Wellbore, "M.S. Wellbore 1" for a MultiSegmented Wellbore, 
  or "Cemented Wellbore 1" for a Cemented Wellbore. If ``WellLabel`` is entered, the text 
  provided will be used. The only acceptable values are ``True`` or ``False``. 
 
* ``WellLabel`` - the label used for wellbores if ``PlotWellLabels`` is set to ``True``. 
  If the text given includes empty brackets (*{}*), then the location index will be inserted 
  in that position. If this entry was given as ``WellLabel: Legacy Well {}``, for example, 
  then the labels would range from "Legacy Well 0" to "Legacy Well (N - 1)," where N is the 
  maximum location index for the wellbore components (location indices use the python indexing). 
  If ``WellLabel`` is given without brackets, then the same text will be displayed for each 
  wellbore component (e.g., ``WellLabel: Well``). if ``PlotWellLabels`` is set to ``True`` 
  but ``WellLabel`` is not entered, labels will be set using the default approach. 
 
* ``PlotInjectionSiteLabels`` - an option to show a text label for the injection 
  site(s) (default is ``False``). 
 
* ``PlotStratComponents`` - the option to plot squares along each wellbore at 
  the depths at which the wellbore intersects the top of a unit (default is ``False``). 
  The tops of shales are shown with red squares, while the tops of aquifers 
  are shown with blue squares. The only acceptable values are ``True`` or ``False``. 
 
* ``StrikeAndDipSymbol`` - a dictionary containing four optional entries related 
  to the strike and dip symbol shown in the figure (default is None). Within 
  this dictionary are the entries ``PlotSymbol``, ``coordx``, ``coordy``, 
  and ``length``. 
 
* ``PlotSymbol`` - an entry under ``StrikeAndDipSymbol`` that specifies whether to 
  show the strike and dip symbol (default is ``True``). The only acceptable values 
  are ``True`` or ``False``. 
 
* ``coordx`` - an entry under ``StrikeAndDipSymbol`` that specifies the x-coordinate 
  at which to plot the strike and dip symbol (default is None). If ``coordx`` is 
  not provided, the graph will use a default location (which depends on the domain). 
 
* ``coordy`` - an entry under ``StrikeAndDipSymbol`` that specifies the y-coordinate 
  at which to plot the strike and dip symbol (default is None). If ``coordy`` is 
  not provided, the graph will use a default location (which depends on the domain). 
 
* ``length`` - an entry under ``StrikeAndDipSymbol`` that specifies the length scale 
  (|m|) of the strike and dip symbol (default is None). For flat-lying units, the 
  length is the diameter of the circular symbol used. For dipping units, the 
  length applies to the line going in direction of strike (not the line in 
  the dip direction). If ``length`` is not provided, the graph will use a 
  calculated length (which depends on the domain). 
 
* ``View`` - a dictionary containing two optional entries related to the 
  perspective of the three-dimensional graph (default is None). Within this 
  dictionary are the entries ``ViewAngleElevation`` and ``ViewAngleAzimuth``. 
  A separate version of the figure is created for each combination of 
  the ``ViewAngleElevation`` and ``ViewAngleElevation`` entries, where 
  the first values in the keywords list are used for the same graph and so on. 
 
* ``ViewAngleElevation`` - an entry under ``View`` containing a list of the 
  elevation angles (in degrees) to use in the ``Stratigraphy`` plot(s) (default is 
  [10, 30]). Values must be between -90 and 90. See the matplotlib 
  documentation regarding view angles. This list must have the same length as 
  the ``ViewAngleAzimuth`` list. 
 
* ``ViewAngleAzimuth`` - an entry under ``View`` containing a list of the 
  azimuth angles (in degrees) to use in the ``Stratigraphy`` plot(s) (default is 
  [10, 30]). Values must be between 0 and 360. See the matplotlib 
  documentation regarding view angles. This list must have the same length as 
  the ``ViewAngleElevation`` list. 
 
Two examples of *.yaml* entries for ``Stratigraphy`` plots are shown below. The 
first entry uses the default settings, while the second entry specifies each 
option. Since the simulation uses a ``LookupTableReservoir``, the entry has to 
include ``InjectionCoordx`` and ``InjectionCoordy``. ``InjectionCoordx`` and 
``InjectionCoordy`` are not required when using another type of reservoir 
component with option ``PlotInjectionSites: True``. 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        Strat_Plot_Default_Settings:
            Stratigraphy:
        Strat_Plot.tiff:
            Stratigraphy:
                Title: Proposed GCS Site
                FigureDPI: 500
                PlotInjectionSites: True
                PlotInjectionSiteLabels: True
                InjectionCoordx: 200
                InjectionCoordy: 200
                PlotWellbores: True
                PlotWellLabels: True
                PlotStratComponents: True
                SaveCSVFiles: False
                SpecifyXandYLims:
                    xLims: [0, 400]
                    yLims: [0, 400]
                SpecifyXandYGridLims:
                    gridXLims: [25, 375]
                    gridYLims: [25, 375]
                StrikeAndDipSymbol:
                    PlotSymbol: True
                    coordx: 100
                    coordy: 300
                    length: 75
                View:
                    ViewAngleElevation: [5, 10, 5, 10]
                    ViewAngleAzimuth: [300, 300, 310, 310]
 
For examples of ``Stratigraphy`` plots, see examples *ControlFile_ex33a.yaml*-*ControlFile_ex38c.yaml*. 
For examples of using ``Stratigraphy`` plots in a script application, see the files 
*iam_sys_reservoir_mswell_stratplot_dipping_strata.py* and *iam_sys_reservoir_mswell_stratplot_no_dip.py*. 
 
AoR 
--- 
 
Area of Review (``AoR``) plots are developed to estimate the ``AoR`` needed for a geologic 
carbon storage project based on the spatial extent of reservoir impacts (pressure 
and |CO2| saturation) and potential aquifer impacts (dissolved salt and dissolved 
|CO2| plume volumes). The potential extent is found by distributing wellbore 
components across the domain. We recommend setting wellbore locations using the 
grid placement option (see examples *ControlFile_ex31a.yaml*  to *ControlFile_ex31d.yaml*). 
The wellbores are hypothetical and used to consider the aquifer impacts that could 
occur if a leakage pathway (extending from the reservoir to the aquifer being considered) 
was present at each wellbore location considered. The approach used for ``AoR`` plots 
is based on the work :cite:`BACON2020`. 
 
An ``AoR`` analysis should be conducted for the lowest underground source of drinking water 
(USDW), the freshwater aquifer that is closest to the reservoir. An aquifer is considered to 
be a USDW (or potentially used as a USDW) if it has total dissolved solids (TDS) of less than 
10,000 |mg/L| (:cite:`NRAP2022`). NRAP-Open-IAM allows the user to conduct an ``AoR`` analysis for 
any aquifer layer, however, and the user must decide which aquifer layer should be considered 
in the analysis.
 
``AoR`` plots can be created for four types of outputs: reservoir pressures 
(**pressure**), reservoir |CO2| saturations (**CO2saturation**), aquifer contaminant plume 
volumes from |CO2| leakage (**pH_volume** or **Dissolved_CO2_volume**), and aquifer 
contaminant plume volumes from brine leakage (**TDS_volume** or **Dissolved_salt_volume**). 
The type of plume volume output depends on the aquifer component used (e.g., ``GenericAquifer`` 
vs. ``FutureGen2Aquifer``). 
 
The ``AoR`` plot type examines these metrics at each location in the domain (i.e., each 
hypothetical wellbore location) and displays the maximum value over time (across all times 
or at specific times, depending on the ``TimeList`` entry provided; this entry is discussed 
below). For ``LHS`` simulations, the ``AoR`` plot displays the maximum values over time at 
each location from all ``LHS`` realizations. This approach is meant to depict how 
severe the reservoir and aquifer impacts could become. 
 
The four types of output displayed by ``AoR`` plots (pressure, |CO2| saturations, and contaminant 
plume sizes from brine leakage and |CO2| leakage) apply to different approaches for ``AoR`` delineation: a 
regulatory approach and a risk-based approach (:cite:`NRAP2022`). A regulatory approach determines the 
area of review based on the combination of two extents: (1) the areas where reservoir pressure 
exceeds a critical pressure (see section :ref:`equations`) and (2) the extent of the |CO2| plume in 
the reservoir. The critical pressure is calculated as the pressure required to lift brine through an 
open conduit, from the reservoir and into the aquifer in consideration (:ref:`equations`). The term 
"regulatory approach" is used here because these considerations are consistent with EPA regulations 
for an under-pressurized reservoir (:cite:`USEPA2013`). This approach may not be suitable for a reservoir 
at or above hydrostatic pressure (i.e., "at pressure" or over-pressurized), however, because the extent 
of reservoir pressures exceeding the critical pressure might be unlimited.

While a regulatory ``AoR`` approach considers pressures and |CO2| saturations, a risk-based approach 
for ``AoR`` delineation considers contaminant plume volumes in an aquifer due to brine and |CO2| leakage. 
The concept behind the risk-based approach is to use a probabilistic representation (i.e., stochastic 
simulation results) of reservoir response to |CO2| injection, leakage pathway response, and groundwater 
impact (:cite:`NRAP2022`). Determining the potential groundwater impact requires the selection of an 
impact threshold. With some aquifer components, the impact thresholds for each plume type cannot be 
changed (e.g., ``FutureGen2Aquifer`` and ``FutureGen2AZMI`` components determine TDS plume volumes as 
ares with a relative changes in TDS of > 10 %). Other aquifer components allow the user to specify the 
impact threshold. For example, the ``GenericAquifer`` component has parameters specifying thresholds for 
dissolved salt and dissolved |CO2| plume definition (**dissolved_salt_threshold** and **dissolved_co2_threshold**). 
When using a risk-based approach, the leakage pathways considered (wellbores) can be hypothetical, representing 
the potential impacts caused by an unknown legacy well near the geologic carbon storage site.

The ``AoR`` plot type is meant to support both the regulatory and risk-based approaches for ``AoR`` delineation, 
and the user must consider the advantages and disadvantages of each approach in the context of the geologic 
carbon storage site being evaluated.
 
Note that the ``AoR`` plot type is meant to be used only for one aquifer overlying the reservoir 
at a time, with that aquifer being represented by only one type of aquifer component 
(e.g., representing contaminant spread in aquifer 2 with a ``FutureGen2Aquifer`` 
component). For example, file *ControlFile_ex31a.yaml* has ``AnalyticalReservoir`` 
components that provide the input for ``OpenWellbore`` components, and the ``OpenWellbore`` 
components provide input to ``FutureGen2Aquifer`` components. The ``FutureGen2Aquifer`` 
components are set up to represent aquifer 2. If the user added an entry to the *.yaml* 
file for a ``FutureGen2AZMI`` aquifer component representing aquifer 1, the ``AoR`` plot 
could not make plots representing the impacts on both aquifers 1 and 2. In this 
case, one would need to create a separate *.yaml* file that creates ``AoR`` plots just 
for aquifer 1. 
 
Note that model run times can increase dramatically with the number of wellbore locations. A 
reservoir, wellbore, and aquifer component will be created for each location specified, which 
increases model run times. Additionally, some aquifer components generally require longer model 
run times (e.g., ``GenericAquifer``) in comparison with other aquifer components (e.g., 
``FutureGen2Aquifer``). Also note that ``FutureGen2Aquifer`` is meant to be set up for aquifers 
with bottom depths less than or equal to 700 m, while ``FutureGen2AZMI`` is meant to be set up 
for aquifers with bottom depths greater than or equal to 700 m. 
 
``AoR`` plots can be made while using any type of wellbore component, but we recommend using 
the ``OpenWellbore`` component. The ``OpenWellbore`` component represents a worst-case 
scenario, where the wellbore is uncemented. Additionally, this component can use a critical 
pressure in leakage calculations. One might want to use another wellbore type, like the 
``MultisegmentedWellboreAI`` component, if there are reliable constraints on the effective 
permeabilities of legacy wells in the study area. Without such constraints, it can be more 
responsible to evaluate the implications of a worst-case scenario with uncemented legacy wells. 
 
The ``AoR`` plot type can be used with ``AnalyticalReservoir``, ``LookupTableReservoir``, and 
``TheisReseroir`` components. When using a ``TheisReservoir`` component, however, the system 
model should not include wellbore or aquifer compoments (see *ControlFile_ex47.yaml*). The 
``TheisReservoir`` can represent multiple injection and/or extraction wells, but the component 
only produces the **pressure** output (if **CO2saturation** is requested from the component, the 
values are always zero). When using a ``TheisReservoir``, the ``AoR`` plot type should only 
be used on the **pressure** output. While an ``AoR`` plot usually focuses on the locations 
used for the wellbore component, when using a ``TheisReservoir`` component the locations 
used are those entered directly for the ``TheisReservoir`` (*ControlFile_ex47.yaml*). 
 
Using the ``AoR`` plot type leads to the creation of *.csv* files containing the values 
shown in the ``AoR`` plots. When using the ``AoR`` plot type, we recommend setting 
``GenerateOutputFiles`` and ``GenerateCombOutputFile`` to ``False`` in the ``ModelParams`` 
section of the *.yaml* file. The large number of wellbore locations commonly used for ``AoR`` 
plots causes a large number of output files. A reservoir and aquifer component is created for 
each wellbore location, and every component will have its output saved if those options are set 
to ``True``. The *.csv* files created for the ``AoR`` plots contain all of the necessary 
information and these files are much smaller in size. 
 
The default figure size and font sizes for the ``AoR`` plot type are:
 
* ``FigureSize`` - [10, 8] ([width, height], in inches)
 
* ``GenFontSize`` - 12
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, ``ColorbarFontSize``, and ``TitleFontSize`` - 14
 
* ``LegendFontSize`` - 10
 
``AoR`` plots can have nine optional entries: ``PlotInjectionSites``, ``InjectionCoordx``, 
``InjectionCoordy``, ``SaveCSVFiles``, ``FigureDPI``, ``FigureSize``, ``TimeList``, 
``CriticalPressureMPa``, and ``BrineDensity``. All of these entries except for 
``CriticalPressureMPa`` and ``BrineDensity`` are described above. 
 
* ``CriticalPressureMPa`` - this entry controls how critical pressure is handled in an ``AoR`` plot  
  evaluating the **pressure** metric. The entry can either be given as ``Calculated`` or as a number 
  representing a critical pressure in MPa (e.g., ``CriticalPressureMPa: 20.5`` for 20.5 MPa). Note 
  that this plot entry only controls how reservoir pressures are evaluated in an ``AoR`` plot; this plot 
  entry will not impact the behavior of an ``OpenWellbore`` component using a critical pressure. The 
  crtical pressure for an ``OpenWellbore`` component must be handled in the set up of the component 
  itself. 
 
* ``BrineDensity`` - When the critical pressure is calculated and the wellbore component has a brine 
  density parameter (e.g., the ``brineDensity`` parameter of ``OpenWellbore`` and ``MultisegmentedWellbore`` 
  components), that brine density parameter will be used when calculating the critical pressure. When 
  the wellbore component does not have a brine density parameter (e.g., ``CementedWellbore`` components) 
  but critical pressure is calculated, then a brine density in |kg/m^3| can be provided with the 
  ``BrineDensity`` entry (e.g., ``BrineDensity: 1100`` for a density of 1100 |kg/m^3|). If needed but 
  not provided, the default value is 1045 |kg/m^3|. If this entry would not be used, given the wellbore 
  component type and ``CriticalPressureMPa`` entry, then any input provided for ``BrineDensity`` 
  will be ignored. 
 
If the ``CriticalPressureMPa`` entry is given for an ``AoR`` plot evaluating the **pressure** metric, the 
the figure will highlight all locations with pressures exceeding the critical pressure used. Without 
the inclusion of the ``CriticalPressureMPa`` entry, an ``AoR`` plot examining **pressure** will only display 
the reserovoir pressures across the domain (it will not highlight a particular area of the domain). 
 
For ``AoR`` plots that evaluate the other metrics (**CO2saturation**, **pH_volume**, **TDS_volume**, 
**Dissolved_CO2_volume**, and **Dissolved_salt_volume**), the figure will highlight any wellbore location 
that has a nonzero result (i.e., a nonzero |CO2| saturation or a nonzero plume volume). 
 
The ``AoR`` should, however, extend to the grid points closest to the injection site(s) that do not satisfy 
these criteria (nonzero **CO2saturation** values, nonzero plume volumes, or **pressure** values exceeding 
the critical pressure). In other words, consider a situation where there is a point with nonzero plume volumes 
is next to a point that never had plume volumes. If another point was placed between these two points, this 
new point may also have nonzero plume volumes. Therefore, excluding this area from the ``AoR`` may prevent 
the detection of leakage events. No such exclusion will occur if the boundaries of the ``AoR`` include points 
that never met the criteria discussed above. 
 
If the ``TimeList`` entry is not provided for an ``AoR`` plot, the figure will show the 
maximum values at each location across all model times. If ``TimeList`` is provided 
as a list of times in years (e.g., ``TimeList: [1, 5, 10]`` or ``TimeList: [10]``), 
then the figures created will represent the maximum values at each location at the 
specified time(s). Otherwise, an ``AoR`` figure can be made for every model time by providing 
``TimeList: All``. Evaluating how the potential impacts of a project change over time 
can inform, for example, how the required extents of surveying efforts change 
over time (i.e., discovering and effectively plugging legacy wells at larger distances 
from the injection site). 
 
Below is an example of two ``AoR`` plot entries in a *.yaml* file. The first entry 
uses the default settings, while the second specifies all available options. 
Since the simulation uses a ``LookupTableReservoir`` this example includes 
``InjectionCoordx`` and ``InjectionCoordy``. These inputs are not required 
for other reservoir component types. 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        AoR_pH_Default_Settings:
            AoR: [pH_volume]
        AoR_TDS.tiff:
            AoR: [TDS_volume]
            PlotInjectionSites: True
            InjectionCoordx: 2.37e5
            InjectionCoordy: 4.41e6
            FigureDPI: 300
            SaveCSVFiles: False
            TimeList: All
 
For examples of ``AoR`` plots, see *ControlFile_ex31a.yaml* to *ControlFile_ex32c.yaml* 
and *ControlFile_ex47.yaml*. 
 
TTFD 
---- 
 
The time to first detection (``TTFD``) plot type uses contaminant plume output from 
aquifer components to simulate when a monitoring well would be able to detect the 
plume in the aquifer(s) considered. If the ``TTFD`` plot type is run without monitoring 
locations provided, it still produces maps showing the spread of contaminant plumes across 
the domain. These figures (and the *.csv* files that can be saved) could then be used to 
decide where to place monitoring sensors. 
 
The ``TTFD`` plot type can produce three types of figures: maps of earliest plume 
timings across the domain (i.e., the earliest time at which the plume type occurs in 
each part of the aquifer(s) considered), maps showing the ``TTFD`` provided by the 
entered monitoring locations, and maps of the probability of plume occurrence in the 
aquifer(s) considered. The figures with the ``TTFD`` from monitoring locations are only 
created if monitoring locations are entered. The maps of plume probabilities are only 
created if the analysis type is Latin Hypercube Sampling (``lhs``) or Parameter Study 
(``parstudy``). Note that plume probabilities are calculated as the number of realizations 
in which a plume occurred at each location divided by the total number of realizations. 
 
The ``TTFD`` plot type requires the use of at least one of the following aquifer 
component types (with the component(s) set up to represent the aquifer(s) 
considered): ``CarbonateAquifer``, ``FutureGen2Aquifer``, ``FutureGen2AZMI``, ``GenericAquifer``, 
``DeepAlluviumAquifer``, or ``DeepAlluviumAquiferML``. Note that the ``FutureGen2Aquifer`` 
component is used for aquifers with bottom depths less than or equal to 700 m, while the ``FutureGen2AZMI`` 
component is used for aquifers with bottom depths greater than or equal to 700 m. The aquifer component(s) 
must also produce the plume dimension metrics associated with the plume type 
considered (e.g., **TDS_dx**, **TDS_dy**, and **TDS_dz** for TDS plumes). Note that 
``CarbonateAquifer`` components do not produce plume dimension outputs for different 
plume types, so the required outputs when using ``CarbonateAquifer`` are **dx** and **dy** 
(which represent the lengths of the impacted aquifer volume in the x- and y-directions, 
respectively). 
 
The plume timing and plume probability figures made with the ``TTFD`` plot type show 
four subplots. Each subplot contains a quarter of the depth range from the 
top of the reservoir to the surface. Each subplot contains the results for 
sections of aquifers within the corresponding depth range. If monitoring sensor 
locations are provided, each subplot will also show any sensors with depth (z) values 
in the subplot's depth range as black triangles. Because there are multiple z grid 
points within each subplot, there can be different layers of results displayed. 
The code is set up to make the top layer shown be the layer with the lowest 
plume timing or highest plume probability (for the corresponding figure types). 
The matplotlib function used to display results by color (contourf) can fail to 
display results when there are very few points with results in a layer. To 
address such situations, if there are fewer than 25 points with results we 
display each value as a color-labelled circle. 
 
While the plume timing plots show the earliest plume timings at each grid location 
across the domain, the monitoring ``TTFD`` plots only display plume timings that are 
sufficiently close to the sensor location(s) provided. The purpose of such graphs 
is to show when the sensors used could warn site operators that an aquifer has 
been impacted. If the chosen sensor ``x``, ``y``, and ``z`` values do not provide any 
warning of plumes in an aquifer, and there are plumes in that aquifer, then the monitoring 
locations should be changed. The distance over which sensors can detect a plume 
are controlled by the ``VerticalWindow`` and ``HorizontalWindow`` entries, which are 
discussed below. Note that the ``TTFD`` plot type can produce output for the DREAM 
tool (Design for Risk Evaluation And Management) if ``WriteDreamOutput`` is set to 
``True`` (see below). DREAM is designed to optimize the placement of monitoring 
sensors. 
 
The default figure size and font sizes for the ``TTFD`` plot type are:
 
* ``FigureSize`` - [10, 8] ([width, height], in inches)
 
* ``GenFontSize`` - 12
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, ``ColorbarFontSize``, and ``TitleFontSize`` - 14
 
* ``LegendFontSize`` - 8
 
Unlike most other plot types, the ``TTFD`` plot type has two required entries: 
``PlumeType`` and ``ComponentNameList``. ``TTFD`` plots will not be produced 
without appropriate input for these entries. ``ComponentNameList`` is discussed 
above. 
 
* ``PlumeType`` - the type of plume metric being considered. Acceptable values 
  are *Pressure*, *pH*, *TDS*, *Dissolved_CO2*, *Dissolved_salt*, and *CarbonateAquifer*. 
  The dx, dy, and dz metrics (e.g., **Dissolved_CO2_dz**) for the PlumeType used 
  must be produced by the aquifer components listed in ``ComponentNameList``. The 
  dz metrics are not required when using ``CarbonateAquifer`` components, however, 
  as these components do not produce a dz plume metric. Additionally, when 
  using ``PlumeType: CarbonateAquifer`` the plume timing and plume probability 
  figures do not have different subplots for different depth ranges. 
 
The ``TTFD`` plot type can have the following optional entries: ``MonitoringLocations``, 
``SaveCSVFiles``, ``WriteDreamOutput``, ``SpecifyXandYLims``, ``NumZPointsWithinAquifers``, 
``NumZPointsWithinShales``, ``xGridSpacing``, ``yGridSpacing``, ``SpecifyXandYGridLims``, 
``PlotInjectionSites``, ``InjectionCoordx``, ``InjectionCoordy``, ``FigureDPI``, and 
``FigureSize``. Three of these entries (``MonitoringLocations``, ``SpecifyXandYLims``, and 
``SpecifyXandYGridLims``) are dictionaries containing additional entries 
(i.e., entries indented beneath mentioned keywords in a *.yaml* file). 
All of these entries except for ``MonitoringLocations``, ``WriteDreamOutput``, 
``NumZPointsWithinAquifers``, and ``NumZPointsWithinShales`` are described above. 
 
The ``NumZPointsWithinAquifers``, ``NumZPointsWithinShales``, ``xGridSpacing``, 
``yGridSpacing``, and ``SpecifyXandYGridLims`` entries all relate to the x-, y-, 
and z-coordinates of the grids used to evaluate plume extents and timings. 
The dx, dy, and dz plume dimension metrics (e.g., *pH_dy* or *TDS_dz*) are used 
to evaluate whether each (x, y, z) of a grid is within a plume area for 
each model timestep. Note that ``NumZPointsWithinAquifers`` and 
``NumZPointsWithinShales`` do not have an effect when ``PlumeType`` is given 
as ``CarbonateAquifer`` because a ``CarbonateAquifer`` component does not produce 
a dz plume metric. 
 
* ``MonitoringLocations`` - a dictionary containing five optional entries related 
  to the sensors used to detect aquifer impacts. The five optional entries are 
  ``coordx``, ``coordy``, ``coordz``, ``HorizontalWindow``, and ``VerticalWindow``. 
  Note that the lists provided for ``coordx``, ``coordy``, and ``coordz`` must all 
  have the same length (although ``coordz`` is not used with option 
  ``PlumeType: CarbonateAquifer``). 
 
* ``coordx`` - an entry under ``MonitoringLocations`` that specifies the 
  x-coordinate(s) (|m|) of monitoring sensor(s), if any sensors are used. This entry 
  must be provided as a list, even if only one location is used (e.g., [100] 
  or [100, 200]). 
 
* ``coordy`` - an entry under ``MonitoringLocations`` that specifies the 
  y-coordinate(s) (|m|) of monitoring sensor(s), if any sensors are used. This entry 
  must be provided as a list, even if only one location is used (e.g., [100] 
  or [100, 200]). 
 
* ``coordz`` - an entry under ``MonitoringLocations`` that specifies the depth(s) 
  (z-coordinate(s), (|m|)) of monitoring sensor(s), if any sensors are used. Note that 
  for this entry, depths beneath the surface are taken as negative values. 
  This entry must be provided as a list, even if only one location is used 
  (e.g., [-500] or [-500, -400]). The ``coordz`` entry is not required when using 
  an option ``plumeType: CarbonateAquifer``, as the ``CarbonateAquifer`` 
  component does not produce a dz plume metric. 
 
* ``HorizontalWindow`` - a (maximum) horizontal distance (|m|) from which monitoring 
  sensor(s) will detect plumes (default is 1). For example, if the HorizontalWindow 
  is 5 m, then the sensor will detect any plume at grid locations within 5 m 
  of the sensor's ``coordx`` and ``coordy`` values (if the plume is also within 
  ``VerticalWindow`` of the sensor's ``coordz`` value). This entry is meant to represent 
  the sensitivity of a sensor, but that consideration must also involve the 
  threshold used for the plume type considered (if the aquifer component has 
  a user-defined threshold for plume detection). For example, **Dissolved_salt** 
  plumes from the ``GenericAquifer`` are influenced by the **dissolved_salt_threshold** 
  parameter. In contrast, the ``FutureGen2Aquifer`` component defines TDS plumes 
  where the relative change in TDS is > 10% (i.e., no user-defined threshold). 
  The inclusion of plumes at nearby grid points is also dependent on the spacing 
  of grid points; the x- and y-spacings are controlled by ``xGridSpacing`` and 
  ``yGridSpacing``, while the z-spacing is controlled by ``NumZPointsWithinAquifers`` 
  and ``NumZPointsWithinShales``. Note that the grid is made to include the x-, y-, 
  and z-coordinates for monitoring locations, so there will always be a grid point 
  for each monitoring sensor. 
 
* ``VerticalWindow`` - a (maximum) vertical distance (|m|) from which monitoring 
  sensor(s) will detect plumes (default is 1). For example, if the ``VerticalWindow`` 
  is 5 m, then the sensor will detect any plume within 5 |m| of the sensor's 
  ``coordz`` values (if the plume is also within ``HorizontalWindow`` of the 
  sensor's ``coordx`` and ``coordy`` value). This entry is meant to represent the 
  sensitivity of a sensor, but that consideration must also involve the threshold 
  used for the plume type considered (if the aquifer component has a user-defined 
  threshold for plume detection). For example, **Dissolved_CO2** plumes from the 
  ``GenericAquifer`` are influenced by the **dissolved_co2_threshold** parameter. In 
  contrast, the ``FutureGen2Aquifer`` component defines pH plumes where the 
  absolute change in pH is > 0.2 (i.e., no user-defined threshold). The 
  inclusion of plumes at nearby grid points is dependent on the spacing of 
  grid points; the x- and y-spacings are controlled by ``xGridSpacing`` and 
  ``yGridSpacing``, while the z-spacing is controlled by ``NumZPointsWithinAquifers`` 
  and ``NumZPointsWithinShales``. Note that the grid is made to include the x-, y-, 
  and z-coordinates for monitoring locations, so there will always be a grid point 
  for each monitoring sensor. 
 
* ``WriteDreamOutput`` - the option to create *.iam* files containing plume timing 
  results (default is ``False``). These *.iam* files are the input for the DREAM 
  program. DREAM is the Design for Risk Evaluation And Management tool, which 
  was also developed by NRAP. The only acceptable values are ``True`` or ``False``. 
 
* ``NumZPointsWithinAquifers`` - the number of z-grid points extending from the 
  bottom to the top of each aquifer (default is 10). The points are equally 
  spaced. 
 
* ``NumZPointsWithinShales`` - the number of z-grid points extending from the 
  bottom to the top of each shale (default is 3). The points are equally 
  spaced. Note that the top of an aquifer is also the bottom of a shale, and 
  the same location is not entered twice. In other words, with the default 
  values for ``NumZPointsWithinAquifers`` (10) and ``NumZPointsWithinShales`` (3) 
  a z-grid will have ten points from the bottom to the top of an aquifer, then a 
  point in the middle of the overlying shale (point 2 of 3 across the shale), 
  and then ten points from the bottom to the top of the overlying aquifer 
  (etc.). In this example, including points 1 and 3 for the shale would be 
  redundant because those points are included for the aquifers below and above 
  the shale. 
 
Below, we show two examples of ``TTFD`` plot entries in the ``Plots`` section of a 
*.yaml* file. The first plot (*pH_Minimum_Input*) has only the entries required to 
set up the ``TTFD`` plot type: ``PlumeType`` and ``ComponentNameList``. The second 
plot (*TDS_All_Options_Specified.tiff*) includes all optional entries for the ``TTFD`` 
plot type. Although there are only two plot entries included, each entry can result 
in the creation of multiple figures (e.g., earliest plume timings, ``TTFD`` from 
monitoring locations, and plume probabilities for each model realization). Note that 
all entries for the ``TTFD`` plot type are indented under ``TTFD`` which is indented 
under the figure name. 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        pH_Minimum_Input:
            TTFD:
                PlumeType: pH
                ComponentNameList: [FutureGen2AZMI1, FutureGen2Aquifer1]
        TDS_All_Options_Specified.tiff:
            TTFD:
                PlumeType: TDS
                ComponentNameList: [FutureGen2AZMI1, FutureGen2Aquifer1]
                FigureDPI: 300
                MonitoringLocations:
                    coordx: [100, 200]
                    coordy: [100, 200]
                    coordz: [-407.5, -407.5]
                    HorizontalWindow: 1
                    VerticalWindow: 5
                PlotInjectionSites: True
                InjectionCoordx: 50
                InjectionCoordy: 50
                SpecifyXandYLims:
                    xLims: [-25, 700]
                    yLims: [-25, 700]
                NumZPointsWithinAquifers: 10
                NumZPointsWithinShales: 3
                xGridSpacing: 5
                yGridSpacing: 5
                SpecifyXandYGridLims:
                    gridXLims: [25, 650]
                    gridYLims: [25, 650]
                WriteDreamOutput: False
                SaveCSVFiles: True
 
For examples of ``TTFD`` plots, see *ControlFile_ex39a.yaml* to *ControlFile_ex43.yaml*. 
For script examples, see *iam_sys_reservoir_mswell_futuregen_ttfdplot_no_dip.py*, 
*iam_sys_reservoir_mswell_futuregen_ttfdplot_no_dip_lhs.py*, and 
*iam_sys_reservoir_mswell_futuregen_ttfdplot_dipping_strata.py*. 
 
GriddedMetric 
------------- 
 
The ``GriddedMetric`` plot type produces map view images of a gridded metric. While 
the radial metrics shown by the ``GriddedRadialMetric`` plot type are defined in 
relation to radius and depth values, the metrics shown by the ``GriddedMetric`` plot 
type are defined relative to x-coordinates and y-coordinates. For example, the 
``GriddedMetric`` plot type can display the gridded output produced by ``SealHorizon`` 
and ``FaultFlow`` components. 
 
The ``GriddedMetric`` plot type has two required entries: ``ComponentNameList`` and 
``MetricName``. Both are described above. 
 
The default figure size and font sizes for the ``GriddedMetric`` plot type are:
 
* ``FigureSize`` - [12, 10] ([width, height] in inches)
 
* ``GenFontSize`` - 10
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, ``ColorbarFontSize``, and ``TitleFontSize`` - 14
 
* ``LegendFontSize`` - 10
 
The ``GriddedMetric`` plot type has the following optional entries: ``Realization``, 
``TimeList``, ``PlotInjectionSites``, ``InjectionCoordx``, ``InjectionCoordy``, 
``SpecifyXandYLims``, ``SaveCSVFiles``, ``EqualAxes``, ``FigureDPI``, and ``FigureSize``. 
All of these entries are discussed above. 
 
Below, we show two examples of setting up ``GriddedMetric`` plots in a *.yaml* control 
file. The first plot (*Plot_Default_Settings*) includes only the required entries, 
while the second (*Plot_With_Options*) includes all optional entries. 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        Plot_Default_Settings:
            GriddedMetric:
                ComponentNameList: [Fault1]
                MetricName: mass_brine_aquifer
        Plot_With_Options:
            GriddedMetric:
                ComponentNameList: [Fault1]
                MetricName: CO2_aquifer
                Realization: 0
                FigureDPI: 300
                TimeList: [1, 5, 10, 25, 50]
                SaveCSVFiles: False
                PlotInjectionSites: False
                InjectionCoordx: 4.68e+04
                InjectionCoordy: 5.11e+04
                SpecifyXandYLims:
                    xLims: [38750, 40500]
                    yLims: [48266, 48400]
                EqualAxes: False
 
For examples of ``GriddedMetric`` plots, see *ControlFile_ex18.yaml*, *ControlFile_ex19.yaml*, 
and *ControlFile_ex23.yaml*. 
 
GriddedRadialMetric 
------------------- 
 
The ``GriddedRadialMetric`` plot type produces map view images of a gridded 
radial metric. The ``GenericAquifer`` produces four kinds of gridded 
radial metrics: **r_coordinate**, **z_coordinate**, **Dissolved_CO2_mass_fraction**, 
and **Dissolved_salt_mass_fraction**. Regions of an aquifer with dissolved |CO2| and 
dissolved salt mass fractions exceeding the corresponding mass fraction threshold 
are included in the plume volumes for the corresponding plume type. Those plume 
volumes can be visualized with the ``TTFD`` plot type. The ``GriddedRadialMetric`` 
plot type, however, can show more general changes in dissolved |CO2| and salt 
mass fractions (e.g., seeing changes in mass fractions below the plume definition 
thresholds). 
 
The default figure size and font sizes for the ``GriddedRadialMetric`` plot type are:
 
* ``FigureSize`` - [10, 8] ([width, height] in inches)
 
* ``GenFontSize`` - 10
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, ``ColorbarFontSize``, and ``TitleFontSize`` - 14
 
* ``LegendFontSize`` - 10
 
The ``GriddedRadialMetric`` plot type has three required entries: ``ComponentNameList``, 
``ZList``, and ``MetricName``. ``ComponentNameList`` and ``MetricName`` are discussed above. 
 
* ``ZList`` - the depths (|m|) at which to evaluate the radial metric output. Depths beneath 
  the surface are taken as being negative. An entry of ``ZList: -500`` would correspond with one 
  depth of 500 m while ``ZList: [-800, -860]`` would correspond with two depths, 800 m and 860 m. 
  The depth in the radial grid (e.g., **z_coordinate** from ``GenericAquifer``) that is closest 
  to each value entered will be used. String inputs representing the bottom, middle, or top of 
  a unit can also be provided. Using this approach, a depth can be specified with a unit type 
  ('shale' or 'aquifer'), a unit number, and 'Depth', 'MidDepth', or 'TopDepth' for the bottom, 
  middle, or top of the unit, respectively. For example, if the ``GriddedRadialMetric`` plot is 
  examining the output of a ``GenericAquifer`` component representing aquifer 3, this entry could 
  be given as ``ZList: [aquifer3Depth, aquifer3MidDepth, aquifer3TopDepth]``, so that the bottom, 
  middle, and top of aquifer 3 would be evaluated.

When the ``GriddedRadialMetric`` plot type is used with a ``GenericAquifer`` component, the stratigraphy 
component can be one that produces unit depths as composite parameters (``Stratigraphy`` component) or 
one that produces depths as observations (``DippingStratigraphy`` or ``LookupTableStratigraphy``). When 
the latter type of stratigraphy component is used, however, the depths in the ``GenericAquifer`` component's 
output (**z_coordinate**) will only reflect the depths at the component's location (i.e., ``x`` and ``y`` 
values for the wellbore component connected to the ``GenericAquifer``). In other words, the depths will not bend 
over space to match spatial changes in stratigraphy; the depth values will remain fixed at the depths for 
the location of the ``GenericAquifer`` component. If the dip of the unit is not significant over the area 
of interest around a ``GenericAquifer`` component, however, the impact of this consideration may not 
be significant.

The ``GriddedRadialMetric`` plot type has 13 optional entries: ``MinValue``, ``DegreeInterval``, ``Realization``, 
``TimeList``, ``PlotInjectionSites``, ``InjectionCoordx``, ``InjectionCoordy``, ``SpecifyXandYLims``, 
``SaveCSVFiles``, ``SaveAllValues``, ``EqualAxes``, ``FigureDPI``, and ``FigureSize``. All of these entries 
except for ``MinValue``, ``DegreeInterval``, and ``SaveAllValues`` are discussed above. 
 
* ``MinValue`` - the minimum value used for the colorbar on the figures. Any values beneath 
  this minimum will not be displayed graphically, but the entire range of values is still 
  displayed in the title of each figure. This parameter has a significant impact on 
  ``GriddedRadialMetric`` figures. For example, the **Dissolved_CO2_mass_fraction** and 
  **Dissolved_salt_mass_fraction** outputs saved by a ``GenericAquifer`` for a time of 0 years 
  will all have values of zero. The outputs saved at other times, however, can have very low but 
  nonzero values. The **Dissolved_CO2_mass_fraction** values can be as low as 5.0e-3 at the highest 
  radii, while the **Dissolved_salt_mass_fraction** values can be as low as 1.0e-30. If the 
  ``MinValue`` provided is zero, then the figures created will be zoomed out to encompass such 
  low values at the highest radii evaluated (about 77.5 km). These large extents will make the 
  figures visually unclear. For these figures to be useful, one should specify a ``MinValue`` 
  that is high enough to enable the figure to focus on the area of interest (i.e., near the 
  component's location) but low enough to not exclude too much of the output data. We recommend 
  using a ``MinValue`` of 0.002 when evaluating **Dissolved_salt_mass_fraction** (10 times lower than 
  the default **dissolved_salt_threshold** of 0.02) and 0.01 when evaluating **Dissolved_CO2_mass_fraction** 
  (half of the default **dissolved_salt_threshold** of 0.02). If ``MinValue`` is not entered, these 
  values will be used as the defaults for the corresponding output type (dissolved salt or 
  dissolved |CO2|). If all of the times evaluated only have values less than or equal to ``MinValue``, 
  then one figure will be made. This figure has a title that includes 'All Model Times.' Note 
  that the *.csv* files saved when ``SaveCSVFiles`` is set to ``True`` will only include 
  values above ``MinValue`` (unless ``SaveAllValues`` is ``True``). 
 
* ``DegreeInterval`` - the interval (degrees) used to create a map-view image from the radial 
  output. The accepted values are 1, 5, 10, 15, 30, and 45. If ``DegreeInterval`` is not entered, 
  the default values is 15 degrees. 
 
 * ``SaveAllValues`` - an option to save all results in *.csv* files (``True``) or only values exceeding 
  the ``MinValue`` (``False``). The only acceptable 
  values are ``True`` or ``False``. If set to ``False``, only values above the ``MinValue`` 
  entry will be saved. Saving all values will result in *.csv* files with larger file sizes. 
  The default setting is ``False``.
 
 
Note that although the ``Realization`` entry for the ``GriddedRadialMetric`` plot type 
follows the indexing conventions of Python (i.e., ``Realization: 0`` for the first realization), 
the figure files and *.csv* files saved by the ``GriddedRadialMetric`` plot type will present the 
simulation number as ranging from one to the total number of realizations (e.g., Simulation 1 
instead of Simulation 0). 
 
Below, we show two examples of ``GriddedRadialMetric`` plot entries in a control file. 
The first entry (*Min_Input_Dissolved_Salt*) uses the minimum input required for the 
``GriddedRadialMetric`` plot type. The second entry (*All_Input_Dissolved_Salt*) 
uses all entries available for the ``GriddedRadialMetric`` plot type. 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        Min_Input_Dissolved_Salt:
            GriddedRadialMetric:
                ComponentNameList: [GenericAquifer1]
                MetricName: Dissolved_salt_mass_fraction
                ZList: [aquifer2Depth]
        All_Input_Dissolved_Salt:
            GriddedRadialMetric:
                ComponentNameList: [GenericAquifer1]
                MetricName: Dissolved_salt_mass_fraction
                ZList: [aquifer2Depth, shale3Depth]
                TimeList: [1, 5, 10, 15, 20]
                MinValue: 0.002
                FigureDPI: 300
                PlotInjectionSites: True
                InjectionCoordx: 100
                InjectionCoordy: 100
                DegreeInterval: 1
                Realization: 0
                EqualAxes: True
                SaveCSVFiles: True
                SpecifyXandYLims:
                    xLims: [-200, 400]
                    yLims: [-200, 400]
 
For examples of ``GriddedRadialMetric`` plots, see *ControlFile_ex54a.yaml* to 
*ControlFile_ex54d.yaml* 
 
AtmPlumeSingle 
-------------- 
 
The ``AtmPlumeSingle`` plot type produces map view images depicting how |CO2| leakage 
at the surface creates atmospheric |CO2| plumes. These images are created for each 
time step during one realization of a simulation. Note that simulations using the 
Latin Hypercube Sampling (``lhs``) or Parameter Study (``parstudy``) analysis types have 
many realizations, while a simulation using a ``forward`` analysis type only has one 
realization. For the ``AtmPlumeSingle`` plot type with ``lhs`` or ``parstudy`` 
simulations, the visualization corresponding to the realization of interest 
can be specified with the ``Realization`` entry in the *.yaml* file (discussed above). 
Note that using the ``AmtPlumeSingle`` plot type requires the use of an AtmosphericROM 
component. 
 
Here is an example of the ModelParams section from *ControlFile_ex40.yaml*, where the 
number of LHS realizations is set as ``siz: 30``. 
 
.. code-block:: python
   :lineno-start: 1

    ModelParams:
        EndTime: 15.
        TimeStep: 1
        Analysis:
            type: lhs
            siz: 30
        Components: [LookupTableReservoir1, MultisegmentedWellbore1,
                     FutureGen2AZMI1, FutureGen2AZMI2]
        OutputDirectory: output/output_ex40_{datetime}
        Logging: Info
 
The produced figures show the source of the |CO2| leak as a red circle and the plume 
as a blue circle. The source location(s) are set by the x and y coordinate(s) of 
the component that the ``AtmosphericROM`` is connected to. For example, in 
*ControlFile_ex9a.yaml*, the ``AtmosphericROM`` component is connected 
to an ``OpenWellbore`` component and the ``OpenWellbore`` component has 
its locations entered with ``coordx`` and ``coordy``. The ``coordx`` and 
``coordy`` values serve as the coordinates of sources for the ``AtmosphericROM`` 
component. In the ``AtmPlumeSingle`` figures, the ``coordx`` and ``coordy`` 
values are shown as the |CO2| sources. In the final figures the plumes are labeled 
as *Critical Areas* because the area is defined as being within the **critical_distance** 
output (from an ``AtmosphericROM``) from the corresponding source. The critical areas are, 
therefore, the areas in which the |CO2| concentrations exceed the value defined 
by the parameter **CO_critical**. The **critical_distance** is the radius of each plume 
circle shown in ``AtmPlumeSingle`` plots, and this **critical_distance** is also 
displayed on the figure with text. 
 
Note that when multiple atmospheric plumes overlap enough, they will be displayed 
as one plume. The source shown will be between the sources of each individual 
plume. 
 
``AtmosphericROM`` components can be provided with receptor locations, which are meant to 
represent home or business locations where people will be present. If receptors 
are provided and the *.yaml* input for the ``AtmPlumeSingle`` includes the entry 
``PlotReceptors: True``, then receptor locations will be shown. 
 
The default figure size and font sizes for the ``AtmPlumeSingle`` plot type are:
 
* ``FigureSize`` - [15, 10] ([width, height] in inches)

* ``GenFontSize`` - 18
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize`` - 24
 
* ``TitleFontSize`` - 20
 
* ``LegendFontSize`` - 12
 
The ``AtmPlumeSingle`` plot type can have the following optional entries ``Realization``, 
``PlotReceptors``, ``PlotInjectionSites``, ``InjectionCoordx``, ``InjectionCoordy``, 
``SpecifyXandYLims``, ``FigureDPI``, and ``FigureSize``. All of these entries except 
for ``PlotReceptors`` are described above. 
 
* ``PlotReceptors`` - option to plot receptor locations (default is ``False``). The 
  only acceptable values are ``True`` or ``False``. If the receptors are far away from 
  the source location(s) and/or the injection site, plotting the receptors may 
  cause the x and y limits to be spread too far. The plumes may then be 
  difficult to see. 
 
Below is an example of the ``AtmPlumeSingle`` plot input in a *.yaml* control file. 
Note that ``InjectionCoordx`` and ``InjectionCoordy`` only have to be provided when 
using a ``LookupTableReservoir`` and setting ``PlotInjectionSites: True``. 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        ATM_single:
            AtmPlumeSingle:
                Realization: 10
                FigureDPI: 300
                PlotInjectionSites: True
                InjectionCoordx: 3.68e4
                InjectionCoordy: 4.83e4
                PlotReceptors: True
                SpecifyXandYLims:
                    xLims: [3.58e4, 3.78e4]
                    yLims: [4.73e4, 4.93e4]
 
For examples of ``AmtPlumeSingle`` plots, see *ControlFile_ex9a.yaml* to 
*ControlFile_ex9c.yaml*. 
 
AtmPlumeEnsemble 
---------------- 
 
The ``AtmPlumeEnsemble`` plot type can only be used in simulations with Latin 
Hypercube Sampling (``lhs``) or Parameter Study (``parstudy``) analysis types. This 
plot type involves concepts similar to those as those of the ``AtmPlumeSingle`` 
plot type. While the ``AtmPlumeSingle`` plot type dislays the critical areas for 
one realization, the ``AtmPlumeEnsemble`` plot type displays the probability of 
critical areas occuring in the domain. These probabilities are calculated with 
the results from all realizations of the ``lhs`` or ``parstudy`` simulation. The 
probabilities specifically represent the likelihood of |CO2| plume concentrations 
exceeding the threshold set with the **CO_critical** parameter for ``AtmosphericROM`` 
components. The probabilities are shown as gridded data. The ``AtmPlumeEnsemble`` 
plot type is available only when the simulation includes an ``AtmosphericROM`` component. 
 
The default figure size and font sizes for the ``AtmPlumeEnsemble`` plot type are:
 
* ``FigureSize`` - [15, 10] ([width, height] in inches)

* ``GenFontSize`` - 18
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, and ``ColorbarFontSize`` - 24
 
* ``TitleFontSize`` - 20
 
* ``LegendFontSize`` - 12
 
The ``AtmPlumeEnsemble`` plot type has the optional entries ``PlotReceptors``, 
``PlotInjectionSites``, ``InjectionCoordx``, ``InjectionCoordy``, ``SpecifyXandYGridLims``, 
``xGridSpacing``, ``yGridSpacing``, ``SpecifyXandYLims``, ``FigureDPI``, and ``FigureSize``. 
All of these entries are described above. 
 
Below is an example of a ``AtmPlumeEnsemble`` plot entry in a *.yaml* file: 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        ATM_Ensemble.tiff:
            AtmPlumeEnsemble:
                FigureDPI: 300
                PlotInjectionSites: True
                InjectionCoordx: 200
                InjectionCoordy: 200
                PlotReceptors: False
                xGridSpacing: 1
                yGridSpacing: 1
                SpecifyXandYGridLims:
                    gridXLims: [-100, 300]
                    gridYLims: [-100, 300]
                SpecifyXandYLims:
                    xLims: [-125, 325]
                    yLims: [-125, 325]
 
For examples of ``AmtPlumeEnsemble`` plots, see *ControlFile_ex9a.yaml* and 
*ControlFile_ex9c.yaml*. 
 
Bowtie 
------ 
 
The ``Bowtie`` plot is a risk assessment framework designed to highlight the connections between 
the causes of an adverse event or condition (termed as the top event) and its consequences. The 
central feature of the ``Bowtie`` diagram is the top event. Flanking the top event are two sets 
of branches: one set extends to the left, showing contributing factors (or threats) that could lead 
to the top event, and another set extends to the right, describing the potential consequences of the 
top event.
 
The format of the ``Bowtie`` plot implemented in NRAP-Open-IAM is different from the format typically 
used for a bowtie plot. This distinction exists because the format implemented in NRAP-Open-IAM 
was made to display a mixture of quantitative and qualitative results.  The setup of the ``Bowtie`` 
plot type is meant to help summarize a wide range of considerations related to a particular risk. 
 
Quantitative results are shown with the value at the time shown (by default, the last model time 
step) as well as the percentage of realizations in which the metric exceeded the threshold specified. 
For example, one could examine aquifer plume volumes, and examine whether a plume volume of 1 |m^3| 
or greater formed. A deterministic simulation has only one realization, so the probabilities would 
be either 0 % or 100 %. A stochastic simulation has multiple realizations. Additionally, quantitative 
results can be read from pre-made *.csv* files, allowing the user to incorporate results from another 
tool. 
 
Qualitative results are represented as an assessment from a panel of experts. If a qualitative assessment 
is used in a ``Bowtie`` plot, it is expected that the assessment provided by an expert panel would be 
obtained after presenting the panel with sufficient information regarding the consideration. For example, 
if an expert panel was asked to judge whether the injection well itself represents a potential leakage 
pathway, the panel should be given documentation regarding the site and the injection well's design and 
planned operation. If this information leads the panel to conclude that the injection well is not a potential 
leakage pathway, the panel might return a judgement of "Extremely Unlikely" for the consideration. 
 
The default figure size and font sizes for the ``Bowtie`` plot types are:
 
* ``FigureSize`` - [12, 8] ([width, height] in inches)
 
* ``GenFontSize`` - 12
 
* ``TitleFontSize`` - 14
 
The ``Bowtie`` plot type has two required entries, ``Contributors`` and ``Consequences``:
 
* ``Contributors`` - path to a *.csv* file providing the information for the factors that contribute 
  to the likelihood of the top event occurring. The path given must be relative to the installation 
  directory of NRAP-Open-IAM. 
 
* ``Consequences`` - path to a *.csv* file providing the information for the consequences of the top 
  event. The path given must be relative to the installation directory of NRAP-Open-IAM. 
 
The setup of the ``Contributors`` and ``Consequences`` *.csv* files is described below. 
 
The ``Bowtie`` plot has the following optional entries: ``TopEventLabel``, ``FigureDPI``, ``FigureSize``, 
``SaveCSVFiles``, and ``TimeList``. All of these entries except for ``TopEventLabel`` are described above. 
 
* ``TopEventLabel`` - text that will be displayed as the label for the top event in the figure. If this 
  entry is not given, no label will be shown for the top event. 
 
The *.csv* files for ``Contributors`` and ``Consequences`` include required columns for ``ComponentNameList``, 
``Metrics``, ``Threshold``, and ``Label``. Each row in the *.csv* file represents a contributor or consequence 
in the ``Bowtie`` plot. 
 
* ``ComponentNameList`` - a component name or list of component names for the component(s) that produce the 
  output used for the contributor or consequence represented by the corresponding row of the *.csv* file. 
  Multiple component names can be given if they are separated by a comma, but each component must produce the 
  output(s) in the ``Metrics`` column of the same row. If the contributor or consequence is a qualitative 
  assessment, instead of quantitative results, then the entry can be given as "Expert Panel." 

* ``Metrics`` - the output(s) used to quantify the contributor or consequence represented by the corresponding 
  row of the *.csv* file. Multiple output names can be given if they are separated by commas, but each output 
  must be produced by the component(s) in the ``ComponentNameList`` column of the same row. If the contributor 
  or consequence is a qualitative assessment, then the ``Metrics`` field can be given as "Extremely Unlikely," 
  "Unlikely," "Inconclusive," "Likely," or "Extremely Likely." When quantitative metrics are produced for different 
  locations (e.g., multiple wellbores producing brine leakage rates), the figure will show a range of values for 
  that metric (i.e., minimum to maximum leakage rates across all wellbores). When the simulation is stochastic, 
  the figure will show the mean value across all realizations, plus/minus ("+/-") the standard deviation of 
  values across all realizations. When the simulation is stochastic and multiple the results represent multiple 
  locations, the figure will show a range of values with standard deviations shown for the minimum and maximum 
  values (minimum plus/minus the standard deviation of minimum values to the maximum plus/minus the standard 
  deviation of maximum values).
 
* ``ThresholdValues`` - the threshold used to assess the metrics given in the ``Metrics`` field of the same 
  row. The probability for that contributor or consequence is determined by the number of realizations in 
  which the output values exceeded the threshold value. If the simulation is deterministic (``forward`` 
  analysis type), then there is only one realization and the probabilities will be 0 % or 100 %. If the 
  contributor or consequence is a qualitative assessment, then this entry can be given as "None." When results 
  are taken from different locations (e.g., multiple wellbores producing brine leakage rates), having the metric 
  threshold surpassed at one location means that the threshold is surpassed; the number of locations does not 
  impact or "dilute" the probability. For example, if the simulation was deterministic (``forward``) and results 
  are shown for five locations, is the threshold was surpassed at one location then the probability shown would 
  be 100 %. If the simulation was stochastic so that the probability is calculated as the percentage of realizations 
  in which the threshold was surpassed, having the threshold surpassed at one location or all locations in a 
  realization would have the same impact (i.e., either way, that realization would be counted as one in which 
  the threshold was surpassed). The threshold is meant to be the point at which a risk is unacceptable, so 
  surpassing the threshold at any location is meant to be unacceptable.
  
* ``Label`` - the text used to describe the contributor or consequence. To include subscripts in the text, 
  such as the "2" in |CO2| for example, use the format "CO$_2$."
 
The *.csv* files can also include the optional columns ``ReadResults``, ``FileDirectory``, ``FileName``, 
``Analysis``, ``Realizations``, and ``Units``.
 
* ``ReadResults`` - indicates whether the data for the corresponding contributor or consequence should be 
  read from pre-made *.csv* files. This column accepts values of 1 (``yes``) or 0 (``no``). If a value of 1 
  is provided, the user should also provide the required information under ``FileDirectory``, ``FileName``, 
  ``Analysis``, and ``Realizations`` (``Units`` would be optional).
 
* ``FileDirectory`` - specifies the directory where the pre-made files are stored. This path is relative 
  to the main directory where NRAP-Open-IAM is installed. Note that folders are separated by // characters 
  on Windows and / characters on Mac and Linux. If the results for a contributor or consequence are not 
  being read from a file, then the ``FileDirectory`` can be entered as "None."
 
* ``FileName`` - name of the *.csv* file containing the results to be read, if ``ReadResults`` is given as 1 
  for that contributor or consequence. The file is expected to be formatted as "ComponentName.MetricName.csv", 
  where the "ComponentName" is the input given under ``ComponentNameList`` and "MetricName" is the input 
  given under ``Metrics``. Note that the "ComponentName" and "MetricName" are separated ny a period ("."). 
  The *.csv* file is also expected to have a value for each time step in the system model (including t = 0 
  years). If the results for a contributor or consequence are not being read from a file, then the ``FileName`` 
  can be entered as "None."
 
* ``Analysis`` - analysis type for the simulation that produced the data: ``forward``, ``lhs``, or ``parstudy``. 
  This input specifies how the data in the *.csv* file ``FileName`` should be interpreted, if ``ReadResults`` 
  is 1. If the ``Analysis`` type is stochastic (``lhs`` or ``parstudy``), the code will expect the file used 
  to contain columns "Realization 1" through "Realization N", where N is the total number of realizations. 
  This input is only necessary when ``ReadResults`` is 1. If the results for a contributor or consequence 
  are not being read from a file, then the ``Analysis`` can be entered as "None" (the analysis type for the 
  system model will be used).
 
* ``Realizations`` - the number of realizations contained in a file containing results to be read into the 
  ``Bowtie`` plot. This input is only necessary when ``ReadResults`` is 1. If the ``Analysis`` type is given 
  as ``forward``, then the ``Realizations`` input should be 1. If the results for a contributor or consequence 
  are not being read from a file, then the ``Realizations`` can be entered as "None."
 
* ``Units`` - units of measurement for the results contained in the *.csv* files. If the metric type is 
  recognized by NRAP-Open-IAM, ``None`` can be entered, and the code will automatically recognize and use the 
  correct units. If ``ReadResults`` is set to 1, no ``Units`` input is given, and the metric is not recognized 
  by NRAP-Open-IAM, the values will be shown without units.
 
For examples that incorporates results from pre-made *.csv* files, see *ControlFile_ex59c.yaml* and 
*ControlFile_ex60b.yaml*.
 
When the ``TimeList`` entry is used with the ``Bowtie`` plot type, one figure will be made for each time 
(given in years) in the ``TimeList`` entry (e.g., ``TimeList: [5, 10, 15]`` for 5, 10, and 15 years). The 
probabilities shown in each figure will reflect whether the metric thresholds were exceeded between 0 years 
and the corresponding time. Although the probabilities reflect all model times between 0 years and the time 
specified, the figure will also show the metric values at the time specified. For example, consider if the 
brine leakage rate is below the threshold at 15 years but it was above the threshold at 10 years. In a 
``Bowtie`` plot made for a ``TimeList`` entry of 15 years, the probability will indicate that the threshold 
was surpassed even though the figure will also display the value at 15 years (which is beneath the threshold). 
If ``TimeList`` is not provided for a ``Bowtie`` plot, one figure will be made that reflects all results 
between the minimum and maximum simulation times.
 
Below is an example of a ``Bowtie`` plot entry in a *.yaml* control file. 
 
.. code-block:: python
   :lineno-start: 1

    Plots:
        Bowtie_Figure:
            Bowtie:
                Contributors: examples/Control_Files/input_data/ex60a/Contributors.csv
                Consequences: examples/Control_Files/input_data/ex60a/Consequences.csv
                TopEventLabel: Unwanted Fluid Migration from the Reservoir
                FigureDPI: 300
                FigureSize: [16, 14]
                SaveCSVFiles: True
                TimeList: [5, 10, 15, 20, 25]
                Title: Risk Assessment through Quantitative System Modeling and Expert Elicitation
 
For examples of ``Bowtie`` plots, see *ControlFile_ex59a.yaml* to *ControlFile_ex61b.yaml*. 
 
SalsaProfile 
------------ 
 
The ``SalsaProfile`` plot type shows how the hydraulic head or pressure values of shale layers vary over time. 
The stratigraphy used by the ``SALSA`` is shown along with profiles of hydraulic head or pressure across the shales 
colored by time. The head values of shales at t = 0 years depend on the values given for the **aquifer#Head**, 
**bottomBoundaryCond**, **bottomBoundaryHead**, **topBoundaryCond**, and **topBoundaryHead** parameters. The 
pressure values at t = 0 years depend on those head parameters as well as the **topBoundaryPressure** and 
**waterTableDepth** parameter.
 
The ``SalsaProfile`` plot type must be used in a simulation containing a ``SALSA`` component. The output type shown 
in the plot is controlled by the ``MetricType`` entry, which is discussed above. The ``MetricType`` entry is given as 
``head`` for hydraulic head output, and ``pressure`` for pressure output. If not entered, the default setting is ``head``.

This plot type always requires the **profileDepths** output. When assessing hydraulic head or pressure output, the 
**headProfile#VertPoint#Shale#** or **pressureProfile#VertPoint#Shale#** outputs, respectively, are required for all 
shale layers, all vertical points, and all locations entered with the ``shaleHeadCoordx`` and ``shaleHeadCoordy`` keyword 
arguments. In the control file interface, the required outputs can be added for all units, locations, and aquifers by using 
``All`` instead of specific indices (e.g., **headProfileAllVertPointAllShaleAll** or **pressureProfileAllVertPointAllShaleAll**).

When evaluting pressure outluts, the vertical profile will show hydrostatic pressures as a dashed black line. These hydrostatic 
pressures (P) are calculated with the following approach:

    P = |rho_w| |times| g |times| d

where |rho_w| is the density of water (taken as 1000 |kg/m^3|), g is gravitational acceleration (9.81 |m/s^2|), and d is depth 
(taken from the **profileDepths** output. If the **waterTableDepth** parameter is used (requires a **topLayerType** value of 1), 
the hydrostatic pressure values will be the **topBoundaryPressure** above the water table (e.g., atmospheric pressure) and then 
increase with depth beneath the water table. If the plot is set up to save *.csv* files (``SaveCSVFiles`` setting of ``True``), 
the hydrostatic pressures will also be saved to the *.csv* file.
 
The default figure size and font sizes for the ``SalsaProfile`` plot type are:
 
* ``FigureSize`` - [10, 12] ([width, height], in inches)
 
* ``GenFontSize`` - 10
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, ``ColorbarFontSize``, and ``TitleFontSize`` - 12
 
* ``LegendFontSize`` - 9
 
The ``SalsaProfile`` plot type can also be given the following optional entries: ``FigureDPI``, ``FigureSize``, 
``Title``, ``SaveCSVFiles``, and ``Realization``. All of these entries are described above. 
 
Below are two examples of ``SalsaProfile`` plot entries in a *.yaml* control file. 
 
.. code-block:: python
   :lineno-start: 1
 
    Plots:
        Salsa_Head_Profile.tiff:
            SalsaProfile:
                MetricType: head
                FigureDPI: 300
                FigureSize: [11, 13]
                Title: SALSA Results
                SaveCSVFiles: True
                Realization: 0
        Salsa_Pressure_Profile.tiff:
            SalsaProfile:
                MetricType: pressure
                FigureDPI: 300
                FigureSize: [11, 13]
                Title: SALSA Results
                SaveCSVFiles: True
                Realization: 0
 
For examples using the ``SalsaProfile`` plot type, see *ControlFile_ex62a.yaml* to *ControlFile_ex63b.yaml*. 
 
SalsaTimeSeries 
------------------- 
 
The ``SalsaTimeSeries`` plot type shows how the hydraulic head or pressure values of shales and aquifers vary over time. 
One figure is made showing the results for aquifers, while another figure is made showing the results for shales. 
Each unit is given its own subplot, and the depth is shown with color. 

The ``SalsaTimeSeries`` plot type must be used in a simulation containing a ``SALSA`` component. The output type shown 
in the plot is controlled by the ``MetricType`` entry, which is discussed above. The ``MetricType`` entry is given as 
``head`` for hydraulic head output, and ``pressure`` for pressure output. If not entered, the default setting is ``head``.

The ``SalsaTimeSeries`` plot type always requires the **profileDepths** output.

When showing hydraulic head or pressure outputs, the ``SALSA`` component must produce the **headProfile#VertPoint#Shale#** 
or **pressureProfile#VertPoint#Shale#** outputs types, respectively, for all shale layers, all vertical points, and all 
locations entered with the ``shaleHeadCoordx`` and ``shaleHeadCoordy`` keyword arguments.

When showing hydraulic head or pressure outputs, the ``SALSA`` component must also produce the **headLoc#Aquifer#** 
or **pressureLoc#Aquifer#**, **pressureLoc#MidAquifer#**, and **pressureLoc#TopAquifer#** outputs, respectively, for all 
aquifers and all locations entered with the ``aquiferHeadCoordx`` and ``aquiferHeadCoordy`` keyword arguments.

In the control file interface, the required outputs can be added for all units, locations, and aquifers by using 
``All`` instead of specific indices (e.g., **headProfileAllVertPointAllShaleAll** or **pressureLocAllTopAquiferAll**).

Hydraulic head and pressure outputs in shales are quantified with multiple points across the thickness of each shale; the 
number of points is set with the **numberOfVerticalPoints** parameter. Pressure outputs for aquifers are given for the bottom, 
middle, and top of the unit. By representing depth through line color, this plot type is meant to show how these results
vary across each unit in more effective manner than showing those data with the ``TimeSeries`` plot type. 
 
The default figure size and font sizes for the ``SalsaTimeSeries`` plot type are:
 
* ``FigureSize`` - [10, *height*] ([width, height], in inches), where *height* is calcualted as by the number 
  of layers assessed. When plotting aquifer results, the *height* (in inches) is calculated as (8 |times| (``N``/2)), 
  where ``N`` is the number of aquifer layers. When plotting shale results, the *height* is calculated as (12 * (``N``/3)), 
  where ``N`` is the number of shale layers.
 
* ``GenFontSize`` - 10
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, ``ColorbarFontSize``, and ``TitleFontSize`` - 12
 
* ``LegendFontSize`` - 9
 
The ``SalsaTimeSeries`` plot type can also be given the following optional entries: ``FigureDPI``, ``FigureSize``, 
``Title``, ``SaveCSVFiles``, and ``Realization``. All of these entries are described above.

Examples of ``SalsaTimeSeries`` plot entries in a *.yaml* control file are shown below. 
 
.. code-block:: python
   :lineno-start: 1
 
    Plots:
        Salsa_Head_Time_Series.tiff:
            SalsaTimeSeries:
                MetricType: head
                FigureDPI: 300
                FigureSize: [11, 14]
                Title: SALSA Results
                SaveCSVFiles: True
                Realization: 0
        Salsa_Pressure_Time_Series.tiff:
            SalsaTimeSeries:
                MetricType: pressure
                FigureDPI: 300
                FigureSize: [11, 14]
                Title: SALSA Results
                SaveCSVFiles: True
                Realization: 0
 
For examples using the ``SalsaTimeSeries`` plot type, see *ControlFile_ex62a.yaml* to *ControlFile_ex63b.yaml*. 
 
SalsaContourPlot 
---------------- 
 
The ``SalsaContourPlot`` plot type makes map-view images of hydraulic head or pressure outputs produced for aquifers 
by the ``SALSA`` component.

The ``SalsaContourPlot`` plot type must be used in a simulation containing a ``SALSA`` component. This plot type 
always requires the **contourPlotCoordx**, and **contourPlotCoordy** outputs. When included, these outputs will be 
saved as *.npz* files. Using the ``SalsaContourPlot`` plot type with ``SaveCSVFiles`` set to ``True`` (the default 
setting) will allow the data to be saved in *.csv* files, however.

The number of ``x`` and ``y`` values stored in the **contourPlotCoordx** and **contourPlotCoordy** outputs is determined 
by the parameters **numberOfNodesXDir**, **numberOfNodesYDir**, **numberOfRadialGrids**, and the number of active and 
leaking wells. Specifically, ``SALSA`` produces a rectangular grid with a number of nodes calculated as **numberOfNodesXDir**
times **numberOfNodesYDir**. There are also more nodes placed in a radial grid around each well. Each ring of points 
around a well has 16 nodes, and the number of rings is set by the **numberOfRadialGrids** parameter. The farthest set 
of rings is at a distance of **radialZoneRadius** from the corresponding well.

The output type shown in the plot is controlled by the ``MetricType`` entry, which is discussed above. The ``MetricType`` 
entry is given as ``head`` for hydraulic head output, and ``pressure`` for pressure output. If not entered, the default 
setting is ``head``. When showing hydraulic head output, the **contourPlotAquiferHead** gridded output is required. When showing 
pressure output, the required output depends on the ``AquiferLocation`` entry (see below).

The number of aquifers with gridded hydraulic head or pressure results is set by the ``aquiferNamesContourPlots`` keyword 
argument. For example, the results for aquifers 1, 2, and 3 will be included if the ``SALSA`` component is given 
``aquiferNamesContourPlots: [1, 2, 3]``. If that argument is not given, then the default approach is to only include 
results for aquifer 1. Separate contour plots will be made for each aquifer specified with the ``aquiferNamesContourPlots`` 
keyword argument.
 
The default figure size and font sizes for the ``SalsaContourPlot`` plot type are:
 
* ``FigureSize`` - [10, 8] ([width, height], in inches)
 
* ``GenFontSize`` - 10
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, ``ColorbarFontSize``, and ``TitleFontSize`` - 12
 
* ``LegendFontSize`` - 9
 
When showing pressure results, the plot type can also be given the optional entries ``AquiferLocation``, ``AoRAquiferNumber``, 
``CriticalPressureMPa``, and ``AquiferNameList``:

* ``AquiferLocation`` - specifies whether pressure outputs at the bottom, middle, or top of each aquifer will be shown. 
  This entry can be given as ``Bottom``, ``Middle``, or ``Top``. If not entered, the default setting is ``Bottom``. This 
  entry impacts which pressure output is required (**contourPlotAquiferPressure**, **contourPlotMidAquiferPressure**, or 
  **contourPlotTopAquiferPressure** for ``Bottom``, ``Middle``, and ``Top``, respectively).

* ``AoRAquiferNumber`` - specifies the aquifer number to use in an area of review analysis in the ``SalsaContourPlot``. If 
  this entry is not provided, then no area of review analysis is conducted for the plot. If an aquifer number is provided, 
  the ``SalsaContourPlot`` figure will calculate a critical pressure and highlight if and where the critical pressure was 
  exceeded. ``SalsaContourPlot`` figures are made for each aquifer number entered with the ``aquiferNamesContourPlots`` 
  keyword argument of a ``SALSA`` component (e.g., ``aquiferNamesContourPlots: [1, 2, 3]`` for aquifers 1, 2, and 3). The 
  ``SalsaContourPlot`` figure will show pressures within the aquifer(s) specified, and then providing the ``AoRAquiferNumber`` 
  input will then have the figure highlight whether the pressures shown could lift fluid along an open conduit and into the 
  ``AoRAquiferNumber`` aquifer. For example, the ``SalsaContourPlot`` figure might show the pressures at the top of aquifer 1 
  (which can be a reservoir, if given injection rates), but then highlight whether the pressures at the top of aquifer 1 could 
  exceed the critical pressure for aquifer 5. The ``AoRAquiferNumber`` entry is meant to be an aquifer that is above the 
  ``aquiferNamesContourPlots`` aquifers because the critical pressure is calculated as the pressure required to lift fluid 
  from one aquifer and into an overlying aquifer along an open conduit. The overlying aquifer is meant to be the lowest 
  underground source of drinking water (i.e., a unit that must be protected from leakage). If the ``AoRAquiferNumber`` 
  entry is given as an aquifer number less than or equal to an aquifer specified with the ``aquiferNamesContourPlots`` keyword 
  argument, then the ``AoRAquiferNumber`` input will not be used in that case (i.e., the plot will not consider critical pressure). 
  Note that the critical pressure is calculated in the manner shown in section :ref:`equations`. The brine density used in that 
  equation is taken as the **aquifer#FluidDensity** parameter of the aquifer for which pressure results are shown in the 
  ``SalsaContourPlot``. Because ``SalsaContourPlot`` figures can show the pressure at the bottom, middle, or top of an aquifer, 
  the corresponding depth will be used as the reservoir depth in the critical pressure equation. The pressure of any aquifer entered 
  with the ``aquiferNamesContourPlots`` keyword argument will be shown with the ``SalsaContourPlot``, but whether each aquifer is 
  considered a reservoir depends on if the ``SALSA`` component is given **activeWell#QAquifer#Period#** values greater than 0 for 
  that aquifer. Instead of calculating a critical pressure, the user can also specify a critical pressure with the ``CriticalPressureMPa`` 
  entry described below.

* ``CriticalPressureMPa`` - if the ``SalsaContourPlot`` is showing pressure results and the ``AoRAqufierNumber entry is provided, this 
  entry can specify how critical pressure will be handled in the ``SalsaContourPlot``. The default value is ``Calculated``, which 
  makes the plot use the critical pressure equation (:ref:`equations`). Otherwise, this entry can be given as a critical pressure in 
  |MPa| (e.g., ``CriticalPressureMPa: 12.5`` for a critical pressure of 12.5 |MPa|). Only one critical pressure can be given with this 
  input, and that critical pressure will be used for the pressure outputs from all aquifers included in the ``aquiferNamesContourPlots`` 
  keyword argument. The user could set up a ``SalsaContourPlot`` plot entry with a critical pressure specified, and then use the 
  ``AquiferNameList`` entry to specify one aquifer for which pressures will be shown. Additionally, the user would need to use the 
  ``AoRAquiferNumber`` entry to specify the higher aquifer that the provided critical pressure is representative of.

* ``AquiferNameList`` - this entry can be used to specify which aquifers will have their results plotted by the ``SalsaContourPlot``. If not 
  entered, plots will be made for all aquifers entered with the ``aquiferNamesContourPlots`` keyword argument of the ``SALSA`` component. For 
  example, consider if ``aquiferNamesContourPlot`` is given to the ``SALSA`` component as ``[1, 2, 3, 4]``. If the user specifically wants 
  to evaluate hydraulic head values or pressures in aquifer 2 (to reduce the number of plots, the number of *.csv* files, and the model 
  run time), then ``AquiferNameList`` can be given as ``[2]`` or ``2``. If the user wants to evaluate the hydraulic head values or pressures 
  in aquifers 2 and 3, then the ``AquiferNameList`` should be given as ``[2, 3]`` (multiple aquifer numbers must be entered in a list 
  using brackets, "[]").

The ``SalsaContourPlot`` plot type can be given the following optional entries: ``FigureDPI``, ``FigureSize``, 
``Title``, ``SaveCSVFiles``, ``Realization``, ``PlotInjectionSites``, ``PlotWellbores``, ``SpecifyXandYLims``, and 
``TimeList``. All of these entries are described above. 
 
Examples of ``SalsaContourPlot`` plot entries in a *.yaml* control file are shown below. 
 
.. code-block:: python
   :lineno-start: 1
 
    Plots:
        Head_Contour_Plot.tiff:
            SalsaContourPlot:
                MetricType: head
                FigureDPI: 300
                FigureSize: [12, 12]
                Title: SALSA Results
                SaveCSVFiles: False
                Realization: 0
        Pressure_Contour_Plot.png:
            SalsaContourPlot:
                MetricType: pressure
                FigureDPI: 300
                FigureSize: [12, 12]
                Title: SALSA Results
                SaveCSVFiles: True
                Realization: 0
				PlotInjectionSites: True
                PlotWellbores: True
                TimeList: [10, 40, 70]
                SpecifyXandYLims:
                    xLims: [-12000, 12000]
                    yLims: [-12000, 12000]
 
For examples using the ``SalsaContourPlot`` plot type, see *ControlFile_ex62a.yaml* to *ControlFile_ex62e.yaml* 
and *ControlFile_ex64a.yaml* to *ControlFile_ex64b.yaml*.
 
SalsaLeakageAoR
---------------
 
The ``SalsaLeakageAoR`` plot type makes map-view images of well leakage volumes produced for aquifers by the 
``SALSA`` component. This plot type is meant to be used to inform the delineation of a risk-based area of review. 
 
The plots will display the minimum and maximum leakage volumes, with the maximum volumes displayed on top of the 
minimum volumes. This approach is used because ``SALSA`` can produce positive and negative well leakage volumes. 
A negative well leakage volume indicates that fluid is flowing from the aquifer and into the leaking well, while 
a positive well leakage volume indicates that fluid is entering the aquifer from the leaking well. The minimum 
and maximum leakage volumes are displayed because certain wells may have positive leakage volumes, while others 
have negative leakage volumes. For example, if an aquifer has a lower initial hydraulic head than the other aquifers, 
then fluid can natually flow from the other aquifers and into the aquifer with lower hydraulic head.
 
The default figure size and font sizes for the ``SalsaLeakageAoR`` plot type are:
 
* ``FigureSize`` - [10, 8] ([width, height], in inches)
 
* ``GenFontSize`` - 10
 
* ``XAxisLabelFontSize``, ``YAxisLabelFontSize``, ``ColorbarFontSize``, and ``TitleFontSize`` - 12
 
* ``LegendFontSize`` - 9
 
The ``SalsaLeakageAoR`` plot type must be used in a simulation containing a ``SALSA`` component. This plot type 
always requires the **well#LeakageVolumeAquifer#** outputs for all leaking wells and any of the aquifers being assessed. 
``SalsaLeakageAoR`` plots accept the following entries:
 
* ``AquiferNameList`` - this entry can be used to specify which aquifers will have their results plotted by the ``SalsaLeakageAoR`` 
  plot. The aquifer names should be given in a list. For example an ``AquiferNameList`` entry of "[1, 2, 3]" specifies that 
  aquifers 1, 2, and 3 will be included in the analysis. If ``AquiferNameList`` is not provided, the default approach is to 
  only include the highest aquifer.
 
* ``ThresholdVolume`` - the leakage volume (|m^3|) used as a threshold in the plots. Any volumes beneath the threshold will not be shown. 
  If this input is not provided, the default threshold is 0.1 |m^3|. Using a threshold can be important, for example, because the 
  numerical methods used in ``SALSA`` can cause a value that is zero to be rounded to a very small number (e.g., on the order of 
  1.0e-9). This rounding is only a numerical artifact, and even if such volumes leaked they could be practically impossible to detect.
 
The ``SalsaLeakageAoR`` plot type can be given the following optional entries: ``FigureDPI``, ``FigureSize``, ``Title``, 
``SaveCSVFiles``, ``PlotInjectionSites``, ``PlotWellbores``, ``SpecifyXandYLims``, and ``TimeList``. All of these entries are 
described above. 
 
Examples of ``SalsaLeakageAoR`` plot entries in a *.yaml* control file are shown below. 
 
.. code-block:: python
   :lineno-start: 1
 
    Plots:
        Well_Leakage_AoR_Plot.tiff:
            SalsaLeakageAoR:
                ThresholdVolume: 1.0
                AquiferNameList: [2, 3]
                FigureDPI: 300
                SaveCSVFiles: True
                PlotInjectionSites: True
                PlotWellbores: True
                TimeList: [10, 40, 80, 90, 100]
                Title: SALSA AoR Analysis
                FigureSize: [11, 9]
                SpecifyXandYLims:
                    xLims: [-40000, 40000]
                    yLims: [-40000, 40000]
 
For examples using the ``SalsaLeakageAoR`` plot type, see *ControlFile_ex65a.yaml* to *ControlFile_ex65c.yaml*.

.. generic_reservoir:
.. include:: ../../common/replace_math.rst

Generic Reservoir Model
=======================

Class documentation
-------------------
.. autoclass:: openiam.base.GenericReservoir

Unittests
----------
.. autoclass:: iam_test.Tests
   :members: test_generic_reservoir_forward
   :noindex:

.. bibliography:: ../../bibliography/project.bib
    :filter: docname in docnames

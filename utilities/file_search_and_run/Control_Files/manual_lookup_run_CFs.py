# -*- coding: utf-8 -*-
"""
This script scans through all .yaml files in the IAM_DIR/examples/Control_Files/ 
directory, where IAM_DIR is the main directory of the NRAP-Open-IAM installation. 
It then finds all .yaml control files that meet the criteria specified with the 
component_type, plot_type, and analysis_type variables. If the run_files variables 
is set to True, it will run the files meeting the search criteria. If run_selection 
is set to 'All', the code runs only files that meet all search criteria. If 
run_selection is 'Any', the code runs files that meet any of the criteria 
given.
----------------------------------------------------------------------------
Instructions:
- Change the search criteria with the following variables: component_type, 
  plot_type, and analysis_type.
- The run_files variable controls whether the identified .yaml files are run.
- The run_selection variable controls which files are run. This variable can be given 
  as 'All' to run only files that meet all criteria or as 'Any' to run the files 
  that meet any of the search criteria (not recommended).
- Make sure the .yaml files to process are in the directory IAM_DIR/examples/Control_Files, 
  where IAM_DIR is the main directory of the NRAP-Open-IAM installation.
- Run the script by running "python manual_lookup_run_CFs.py" from your command 
  line or IDE.
"""

import os
import yaml
import csv
import subprocess
import logging
from datetime import datetime
import re 

try:
    from openiam.components.iam_base_classes import IAM_DIR
except:
    err_msg = ('Could not import from NRAP-Open-IAM classes, ensure the ' 
               + 'NRAP-Open-IAM environment has been set up and activated. ' 
               + 'See the NRAP-Open-IAM user guide.')
    
    raise ImportError(err_msg)


#----- Inputs ----------------------------------------------------------------#
output_file_name = 'manual_lookup_csv_criteria.csv'

# Desired search criteria - if one is left as '', it is not considered. For 
# example, you could identify all files using the 'OpenWellbore' component and 
# the 'TimeSeries' plot type, excluding the consideration of analysis type ('').
# Analysis type options are 'forward', 'lhs', or 'parstudy'.
component_type = 'MultisegmentedWellbore'
plot_type = 'AoR'
analysis_type = 'forward'

# Option to run the files, 'True' for yes and 'False' for no
run_files = True

# Enter 'Any' to run any files that meeting any of the search criteria. Enter 
# 'All' to run only files that meet all of the search criteria (all not given as '')
run_selection = 'All'

# Define the directory containing the .yaml files
control_files_directory = os.path.join(IAM_DIR, 'examples', 'Control_Files')

# Directory containing the scripts for searching for control files
file_search_dir = os.path.join(IAM_DIR, 'utilities', 'file_search_and_run', 'Control_Files')

logging_folder = os.path.join(file_search_dir, 'logging_files')

if not os.path.exists(logging_folder):
    os.mkdir(logging_folder)

start_time = datetime.now()
now = start_time.strftime('%Y-%m-%d_%H.%M.%S')

# Define the output folder. If changed, update the .gitignore file.
output_dir = os.path.abspath(os.path.join(
    logging_folder, 'manual_run_CFs_{}'.format(now)))


#----- Processing ------------------------------------------------------------#
full_file_path = os.path.join(output_dir, output_file_name)

# Get a description for the search criteria
criteria = ''
if component_type != '':
    criteria = f'component type: {component_type}'

if plot_type != '':
    if criteria == '':
        criteria = f'plot: {plot_type}'
    else:
        criteria += f', plot type: {plot_type}'

if analysis_type != '':
    if criteria == '':
        criteria = f'analysis type: {analysis_type}'
    else:
        criteria += f', analysis type: {analysis_type}'

os.makedirs(output_dir, exist_ok=True)

# .log file containing messages
log_file_name = 'results_log.log'

log_file_path = os.path.join(output_dir, log_file_name)

log_level = 'DEBUG'

logger = logging.getLogger('')
# Remove default existing handlers
logger.handlers.clear()
logger.setLevel(log_level)
# logging formatter for log files with more details
log_formatter1 = logging.Formatter(
    fmt='%(levelname)s %(module)s.%(funcName)s: %(message)s',
    datefmt='%m-%d %H:%M')
# logging formatter for console output
log_formatter2 = logging.Formatter(
    fmt='%(levelname)-8s %(message)s',
    datefmt='%m-%d %H:%M')

# Setup logging to log file
file_handler = logging.FileHandler(filename=log_file_path, mode='w')
file_handler.setLevel(log_level)
file_handler.setFormatter(log_formatter1)
logger.addHandler(file_handler)

# Setup logging to console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(log_formatter2)
logger.addHandler(console)

if run_selection not in ['Any', 'All']:
    err_msg = f'\nThe run_selection variable can only be set to "Any" or "All", but it was given as {run_selection}. Using default setting of "All".\n'
    logging.error(err_msg)

    run_selection = 'All'

# Log the specified criteria
msg = '\nSpecified criteria: \n' + criteria + '\n'
logging.info(msg)


def sort_selected_files(selected_files): 
    """ 
    Sorts a list of files.
    """ 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)] 
    
    return sorted(selected_files, key=alphanum_key)


def get_files_that_meet_all_criteria(component_type, plot_type, analysis_type, 
                                     files_with_component_type, files_with_plot_type, 
                                     files_with_analysis_type):
    """
    Returns a list of all control files that meet the defined criteria. If one 
    of the criteria (component_type, plot_type, and analysis_type) was left as 
    '', then it is not considered.
    """
    files_meeting_all_criteria = []
    
    if component_type != '' and plot_type != '':
        files_meeting_comp_and_plot = list(
            set(files_with_component_type).intersection(files_with_plot_type))
        
        if analysis_type != '':
            files_meeting_all_criteria = list(
                set(files_meeting_comp_and_plot).intersection(files_with_analysis_type))
        else:
            files_meeting_all_criteria = files_meeting_comp_and_plot.copy()
    
    elif component_type == '' and plot_type != '':
        if analysis_type != '':
            files_meeting_all_criteria = list(
                set(files_with_plot_type).intersection(files_with_analysis_type))
        else:
            files_meeting_all_criteria = files_with_plot_type.copy()
        
    elif component_type == '' and plot_type == '':
        if analysis_type != '':
            files_meeting_all_criteria = files_with_analysis_type.copy()
        else:
            files_meeting_all_criteria = []
        
    elif component_type != '' and plot_type == '':
        if analysis_type != '':
            files_meeting_all_criteria = list(
                set(files_with_component_type).intersection(files_with_analysis_type))
        else:
            files_meeting_all_criteria = files_with_component_type.copy()
    
    try:
        files_meeting_all_criteria = sort_selected_files(files_meeting_all_criteria)
    except:
        pass
    
    return files_meeting_all_criteria


def run_yaml_example(file_path):
    """
    This function runs a specified Python script with the given .yaml file as an argument.
    
    :param file_path: The path to the .yaml file to be processed.
    """
    # Create the path to the Python script that runs the control files
    run_file = os.path.join(IAM_DIR, 'src', 'openiam', 'components', 'openiam_cf.py')
    command = ['python', run_file, '--file', file_path]
    
    msg = f"\nRunning {os.path.basename(file_path)}.\n" 
    logging.info(msg)
    
    try:
        result = subprocess.run(command, check=True, stdout=subprocess.PIPE, 
                                stderr=subprocess.PIPE, text=True)
        msg = f"\nProcessed {os.path.basename(file_path)} successfully. Output: \n{result.stdout}\n"
        logging.info(msg)
    except subprocess.CalledProcessError as e:
        # Log any errors encountered during the subprocess execution
        err_msg = f"\nFailed to process {os.path.basename(file_path)}. Error: \n{e.stderr}\n"
        logging.error(err_msg)


# List all .yaml files in the directory
yaml_files = [file for file in os.listdir(control_files_directory) if file.endswith('.yaml')]

files_with_component_type = []
files_with_plot_type = []
files_with_analysis_type = []

# Check each .yaml file for specified criteria
for yaml_filename in yaml_files:
    yaml_file_path = os.path.join(control_files_directory, yaml_filename)
    
    with open(yaml_file_path, 'r') as yaml_file:
        try:
            yaml_data = yaml.load(yaml_file, Loader=yaml.SafeLoader)

            # Check for specified component type
            if any('Type' in data and data['Type'] == component_type for data in yaml_data.values()):
                files_with_component_type.append(yaml_filename)

            # Check for specified plot type
            if 'Plots' in yaml_data:
                for plot_name, plot_details in yaml_data['Plots'].items():
                    if plot_type in plot_details:  # Checking if plot_type is directly a key
                        files_with_plot_type.append(yaml_filename)
                        
            # Check for specified analysis type
            if 'ModelParams' in yaml_data:
                model_params = yaml_data.get('ModelParams', {})
                analysis_info = model_params.get('Analysis', {})
                
                analysis_type_found = analysis_info if isinstance(
                    analysis_info, str) else analysis_info.get('type', '')
                
                if analysis_type_found == analysis_type:
                    files_with_analysis_type.append(yaml_filename)
            
        except yaml.YAMLError as exc:
            err_msg = f"\nError reading {yaml_filename}: \n{exc}\n"
            logging.error(err_msg)
    
    yaml_file.close()

try:
    files_with_component_type = sort_selected_files(files_with_component_type)
except:
    pass

try:
    files_with_plot_type = sort_selected_files(files_with_plot_type)
except:
    pass

try:
    files_with_analysis_type = sort_selected_files(files_with_analysis_type)
except:
    pass

# Get list of files that meet all three criteria. If a criterion was left as '', 
# then do not consider it.
# Save the results to a .csv file
csv_file_path = os.path.join(output_dir, 'manual_lookup_csv_criteria.csv')

files_meeting_all_criteria = []

try:
    files_meeting_all_criteria = get_files_that_meet_all_criteria(
        component_type, plot_type, analysis_type, files_with_component_type, 
        files_with_plot_type, files_with_analysis_type)
except:       
    err_msg = ''.join([
        'There was an error while attempting to find the control files that ', 
        f'meet the defined criteria ({criteria}).'])

output_text_all_files = f'\nControl files meeting all specified criteria: \n{files_meeting_all_criteria}\n'
logging.info(output_text_all_files)

with open(csv_file_path, 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow([f'Files with "{component_type}" Component Type', 
                     f'Files with "{plot_type}" Plot Type', 
                     f'Files with "{analysis_type}" Analysis Type', 
                     'Files Meeting All Defined Criteria'])
    
    max_len = max(len(files_with_component_type), len(files_with_plot_type), 
                  len(files_with_analysis_type))
    
    for i in range(max_len):
        row = [
            files_with_component_type[i] if i < len(files_with_component_type) else '',
            files_with_plot_type[i] if i < len(files_with_plot_type) else '',
            files_with_analysis_type[i] if i < len(files_with_analysis_type) else '', 
            files_meeting_all_criteria[i] if i < len(files_meeting_all_criteria) else ''
            ]
        
        writer.writerow(row)
file.close()

msg = f'\nSearch results saved to the file {full_file_path}.\n'
logging.info(msg)

# Process all the files that met the criteria using the external Python script
if run_files:
    if run_selection == 'Any':
        selected_files = list(set(files_with_component_type + files_with_plot_type 
                                  + files_with_analysis_type))
        
    elif run_selection == 'All':
        selected_files = files_meeting_all_criteria.copy()
        
    else:
        err_msg = f'\nThe run_selection variable can only be set to "Any" or "All", but it was given as {run_selection}. Using default setting of "All".\n'
        logging.error(err_msg)
        
        selected_files = files_meeting_all_criteria.copy()
    
    for file_name in selected_files:
        full_path = os.path.join(control_files_directory, file_name)
        run_yaml_example(full_path)
    
    msg = '\nAll matched files have been processed.\n'
    logging.info(msg)

msg = f'\nA .log file has been saved to {output_dir}.\n'
logging.info(msg)

# Shutdown logging so the logging file can be deleted afterwards, if desired
logging.shutdown()


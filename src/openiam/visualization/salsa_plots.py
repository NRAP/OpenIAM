# -*- coding: utf-8 -*-
"""
Code to create plots displaying results produced by the SALSA component. Three 
different plot types are included here: SalsaProfile, SalsaTimeSeries, and 
SalsaContourPlot.

Created: March 7th, 2024
Last Modified: June 4th, 2024

@author: Nate Mitchell (Nathaniel.Mitchell@NETL.DOE.GOV)
LRST (Battelle|Leidos) supporting NETL
"""

import os
import logging
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib import colormaps, cm, ticker
import pandas as pd

from openiam.cf_interface.commons import get_parameter_val

# Assumed water density in kg/(m^3), used to calculate hydrostatic pressure
WATER_DENSITY = 1000

# Gravitational acceleration in m/(s^2)
GRAV_ACCEL = 9.81

# Threshold volume in m^3. Any volumes between -DEFAULT_THRESH_VOL and DEFAULT_THRESH_VOL 
# will not be shown. Due to the numerical approach used, values that should be 
# zero can be rounded to very small values (e.g., 1.0e-9 instead of 0).
DEFAULT_THRESH_VOL = 0.1

BACKGROUND_COLOR = [0.67, 0.67, 0.67]

TIME_COLORMAP = 'RdYlBu'
DEPTH_COLORMAP = 'turbo_r'
HEAD_COLORMAP = 'Spectral_r'
PRESSURE_COLORMAP = 'RdYlBu_r'
WELL_LEAKAGE_COLORMAP = 'viridis'

DEFAULT_CBAR_ASPECT = 50

# Initial placeholder values that are overwritten with minimum and maximum values
INITIAL_MIN_VAL = 9.99e99
INITIAL_MAX_VAL = -9.99e99

# This is the default realization number to use with a stochastic simulation, if 
# the "Realization" entry is not provided.
DEFAULT_REALIZATION = 0

# Initial values used to find the minimum and maximum well leakage volumes
DEFAULT_MIN_VALUE = 9.99e+99
DEFAULT_MAX_VALUE = -9.99e+99

# Used to slightly increase the maximum value for the colorbar. Otherwise, the 
# figure can have an uncolored area where the maximum value is.
MAX_VAL_ADJUST = 1.1

RESERVOIR_LABEL = '(Reservoir)'

METRIC_TYPES = ['head', 'pressure']

AQUIFER_LOCATIONS = ['Bottom', 'Middle', 'Top']

OUTPUT_NOT_AVAILABLE_MSG = ''.join([
    'While making the {} plot {}, an error was encountered ', 
    'while attempting to extract the {} output from the SALSA component {}. ', 
    'This output type is required for the plot, but it might not have ', 
    'been included as an output for the component. Alternatively, the output ', 
    'might not be available because the simulation failed. Check your input.'])

ENTRY_NOT_RECOGNIZED_MSG = ''.join([
    'The {} plot {} was given a {} entry of {}. This input does not match the ', 
    'possible options ({}). This plot will not be made.'])


def salsa_plots(sm, s, output_dir, plot_data, model_data, yaml_data, name='SalsaPlot', 
                analysis='forward', title=None, plot_type='SalsaProfile', 
                figsize=None, colormap=None, cbar_aspect=None, fontname=None, 
                genfontsize=None, xaxisfontsize=None, yaxisfontsize=None, 
                caxisfontsize=None, titlefontsize=None, lgndfontsize=None, 
                linewidth=3, bold_labels=True, dpi_ref=100, save_csv_files=True):
    """
    Creates five plot types made with SALSA component output: SalsaProfile, 
    SalsaProfile, SalsaTimeSeries, SalsaTimeSeries, and SalsaContourPlot.
    """
    
    if genfontsize is None:
        genfontsize = 10
    if xaxisfontsize is None:
        xaxisfontsize = 12
    if yaxisfontsize is None:
        yaxisfontsize = 12
    if caxisfontsize is None:
        caxisfontsize = 12
    if titlefontsize is None:
        titlefontsize = 12
    if lgndfontsize is None:
        lgndfontsize = 9
    if fontname is None:
        fontname = 'Arial'
        
    proceed_check = True
    
    metric_type = plot_data[plot_type].get(
        'MetricType', METRIC_TYPES[0])
    
    if metric_type not in METRIC_TYPES:
        err_msg = ENTRY_NOT_RECOGNIZED_MSG.format(
            plot_type, name, 'MetricType', metric_type, METRIC_TYPES)
        logging.error(err_msg)
        
        proceed_check = False
    
    aquifer_location = ''
    if metric_type == 'pressure' and plot_type == 'SalsaContourPlot':
        aquifer_location = plot_data[plot_type].get(
            'AquiferLocation', AQUIFER_LOCATIONS[0])
        
        if aquifer_location not in AQUIFER_LOCATIONS:
            err_msg = ENTRY_NOT_RECOGNIZED_MSG.format(
                plot_type, name, 'AquiferLocation', 
                aquifer_location, AQUIFER_LOCATIONS)
            logging.error(err_msg)
            
            proceed_check = False
    
    fontweight = 'bold'
    if not bold_labels:
        fontweight = 'normal'
    
    if colormap is None:
        if plot_type == 'SalsaProfile':
            colormap = TIME_COLORMAP
            
        elif plot_type == 'SalsaTimeSeries':
            colormap = DEPTH_COLORMAP
            
        elif plot_type == 'SalsaContourPlot':
            if metric_type == 'head':
                colormap = HEAD_COLORMAP
            elif metric_type == 'pressure':
                colormap = PRESSURE_COLORMAP
    
    colormap = colormaps.get_cmap(colormap)
    
    if cbar_aspect is None:
        cbar_aspect = DEFAULT_CBAR_ASPECT
    
    font = {'family': fontname,
            'weight': 'normal',
            'size': genfontsize}
    plt.rc('font', **font)
    
    time_array = sm.time_array
    
    time_array_years = time_array / 365.25
    
    yaml_input = get_salsa_plot_input(plot_data, name, plot_type=plot_type)
    
    if yaml_input['dpi_input'] is not None:
        dpi_ref = yaml_input['dpi_input']
    
    if yaml_input['fig_size'] is not None:
        figsize = yaml_input['fig_size']
    
    if yaml_input['SaveCSVFiles'] is not None:
        save_csv_files = yaml_input['SaveCSVFiles']
    
    if yaml_input['ComponentNameList'] is not None:
        comp_name_list = yaml_input['ComponentNameList']
        
        salsa_comp_name = comp_name_list[0]
        
        warning_addition = 'given with the ComponentNameList entry'
    else:
        components = list(sm.component_models.values())
        
        comp_name_list = []
        for comp in components:
            if comp.class_type == 'SALSA':
                comp_name_list.append(comp)
                
        warning_addition =  'present in the system model'
        
        salsa_comp_name = comp_name_list[0].name
    
    if len(comp_name_list) > 1:
        warning_msg = ''.join([
            'While making the {} plot {}, {} SALSA components were {}. ', 
            'This plot type is designed to handle the output of only one SALSA ', 
            'component at a time. Only one of the SALSA components present ', 
            'will be used ({}). To specify a specific SALSA component for the ', 
            '{} plot, provide the entry "ComponentNameList: SALSA_COMP_NAME" ', 
            'under the plot type, where SALSA_COMP_NAME is the name of a ', 
            'specific SALSA component.'
            ]).format(plot_type, name, len(comp_name_list), warning_addition, 
                      salsa_comp_name, plot_type)
        
        logging.warning(warning_msg)
    
    salsa_comp = sm.component_models[salsa_comp_name]
    
    if save_csv_files and not os.path.exists(os.path.join(
            output_dir, 'csv_files', 'SALSA_Stratigraphy.csv')):
        # Saves the stratigraphy to a .csv file, if the file has not been made yet
        save_salsa_stratigraphy(salsa_comp, output_dir)
    
    realization = yaml_input['Realization']
    
    extension = '.png'
    if '.' in name:
        extension = name[name.index('.'):]
        name = name[:name.index('.')]
    
    real_addition = ''
    if analysis in ['lhs', 'parstudy'] and realization is not None:
        real_addition = '_Realization{}'.format(realization + 1)
    
    if plot_type == 'SalsaProfile':
       salsa_vert_profile_plot(
           name, metric_type, salsa_comp, sm, s, time_array_years, output_dir, 
           yaml_input, extension=extension, real_addition=real_addition, analysis=analysis, 
           title=title, figsize=figsize, colormap=colormap, cbar_aspect=cbar_aspect, 
           genfontsize=genfontsize, xaxisfontsize=xaxisfontsize, yaxisfontsize=yaxisfontsize, 
           caxisfontsize=caxisfontsize, titlefontsize=titlefontsize, 
           lgndfontsize=lgndfontsize, linewidth=linewidth, fontweight=fontweight, 
           dpi_ref=dpi_ref, save_csv_files=save_csv_files)
        
    elif plot_type == 'SalsaTimeSeries':
        salsa_ouput_vs_time_plots(
            name, metric_type, salsa_comp, sm, s, time_array_years, output_dir, 
            yaml_input, extension=extension, real_addition=real_addition, analysis=analysis, 
            title=title, figsize=figsize, colormap=colormap, 
            cbar_aspect=cbar_aspect, genfontsize=genfontsize, 
            xaxisfontsize=xaxisfontsize, yaxisfontsize=yaxisfontsize, 
            caxisfontsize=caxisfontsize, titlefontsize=titlefontsize, 
            lgndfontsize=lgndfontsize, linewidth=linewidth, 
            fontweight=fontweight, dpi_ref=dpi_ref, save_csv_files=save_csv_files)
        
    elif plot_type == 'SalsaContourPlot' and proceed_check:
        salsa_contour_plot(
            name, metric_type, aquifer_location, salsa_comp, sm, time_array_years, 
            output_dir, yaml_input, analysis=analysis, title=title, 
            extension=extension, real_addition=real_addition, 
            figsize=figsize, colormap=colormap, cbar_aspect=cbar_aspect, 
            genfontsize=genfontsize, xaxisfontsize=xaxisfontsize, 
            yaxisfontsize=yaxisfontsize, caxisfontsize=caxisfontsize, 
            titlefontsize=titlefontsize, lgndfontsize=lgndfontsize, 
            fontweight=fontweight, dpi_ref=dpi_ref, save_csv_files=save_csv_files)
        
    elif plot_type == 'SalsaLeakageAoR':
        salsa_well_leakage_aor_plot(
            name, metric_type, aquifer_location, salsa_comp, sm, s, 
            time_array_years, output_dir, yaml_input, yaml_data, analysis=analysis, 
            title=title, extension=extension, figsize=figsize, colormap=colormap, 
            genfontsize=genfontsize, xaxisfontsize=xaxisfontsize, 
            yaxisfontsize=yaxisfontsize, caxisfontsize=caxisfontsize, 
            titlefontsize=titlefontsize, lgndfontsize=lgndfontsize, 
            fontweight=fontweight, dpi_ref=dpi_ref, cbar_aspect=cbar_aspect, 
            save_csv_files=save_csv_files)


def salsa_vert_profile_plot(name, metric_type, salsa_comp, sm, s, time_array_years, output_dir, 
                            yaml_input, extension='.png', real_addition='', 
                            analysis='forward', title=None, figsize=(10, 12), 
                            colormap=None, cbar_aspect=None, genfontsize=10, 
                            xaxisfontsize=12, yaxisfontsize=12, caxisfontsize=12, 
                            titlefontsize=12, lgndfontsize=9, 
                            linewidth=3, fontweight='bold', dpi_ref=100, 
                            save_csv_files=True):
    """
    Plots vertical profiles of depth vs. shale output (head or pressure), with 
    different time steps color labeled by time. Makes one plot for each vertical 
    profile.
    """
    if colormap is None:
        colormap = colormaps.get_cmap(TIME_COLORMAP)
    elif isinstance(colormap, str):
        colormap = colormaps.get_cmap(colormap)
    
    if cbar_aspect is None:
        cbar_aspect = DEFAULT_CBAR_ASPECT
    
    cbar_vals = np.linspace(
        np.min(time_array_years), np.max(time_array_years), 200)
    
    shaleThicknesses, aquiferThicknesses, bottomLayerType, \
        topLayerType = get_salsa_strata_info(salsa_comp)
    
    shaleDepths, shaleMidDepths, shaleTopDepths, aquiferDepths, \
        aquiferMidDepths, aquiferTopDepths = salsa_comp.get_unit_depths(
            shale_thickness_list=shaleThicknesses, aquifer_thickness_list=aquiferThicknesses, 
            bottom_layer_type=bottomLayerType, top_layer_type=topLayerType)
    
    realization = yaml_input['Realization']
    
    realization = check_realization(
        realization, name, analysis=analysis, plot_type='Salsa{}Profile'.format(
            metric_type.title()))
    
    depths_file_name = get_file_name(salsa_comp.name, 'profileDepths', 
                                     analysis=analysis, realization=realization)
    
    try:
        data = np.load(os.path.join(output_dir, depths_file_name))
        profileDepths = data['data']
        data.close()
    except:
        err_msg = OUTPUT_NOT_AVAILABLE_MSG.format(
            'Salsa{}Profile'.format(metric_type.title()), name, 
            'profileDepths', salsa_comp.name) 
        
        logging.error(err_msg)
        raise KeyError(err_msg)
    
    shale_file_name = get_file_name(salsa_comp.name, 'all{}ProfilesShales'.format(
        metric_type.title()), analysis=analysis, realization=realization)
    
    try:
        data = np.load(os.path.join(output_dir, shale_file_name))
        alloutputProfilesShales = data['data']
        data.close()
    except:
        err_msg = OUTPUT_NOT_AVAILABLE_MSG.format(
            'Salsa{}Profile'.format(metric_type.title()), name, 
            'all{}ProfilesShales'.format(metric_type.title()), salsa_comp.name) 
        
        logging.error(err_msg)
        raise KeyError(err_msg)
    
    shaleCoordx = salsa_comp.shaleCoordx
    shaleCoordy = salsa_comp.shaleCoordy
    
    activeWellQ = salsa_comp.activeWellQ
    
    # Output profiles across the aquitards, colored by time
    for profileInd in range(len(shaleCoordx)):
        fig = plt.figure(1 + profileInd, figsize=figsize, dpi=dpi_ref)
        
        if save_csv_files:
            output_vals_compiled = []
            times_compiled = []
        
        profileX_km = shaleCoordx[profileInd] / 1000
        profileY_km = shaleCoordy[profileInd] / 1000
        
        # If the coordinate is 0 km, do not need to show 0.00 km
        profileX_km_str = '{:.2f} km'.format(profileX_km)
        if profileX_km == 0:
            profileX_km_str = '0 km'
        
        profileY_km_str = '{:.2f} km'.format(profileY_km)
        if profileY_km == 0:
            profileY_km_str = '0 km'
        
        position_title = 'Profile {} at x = {} and y = {}'.format(
            profileInd + 1, profileX_km_str, profileY_km_str)
        
        if realization is not None:
            position_title += ', Realization {}'.format(realization + 1)
        
        ax = plt.gca()
        ax.set_facecolor(BACKGROUND_COLOR)
        
        for timeInd in range(len(time_array_years)):
            rgba = get_color(time_array_years, time_array_years[timeInd], colormap)
            
            vals = alloutputProfilesShales[profileInd][timeInd][:]
            
            ax.plot(vals, profileDepths, color=rgba[0:3], 
                    linewidth=linewidth)
            
            if save_csv_files:
                output_vals_compiled.append(vals)
                times_compiled.append(time_array_years[timeInd])
        
        plot_units(ax, shaleDepths, shaleTopDepths, aquiferDepths, aquiferTopDepths, 
                   shaleThicknesses, aquiferThicknesses, activeWellQ, genfontsize, 
                   fontweight=fontweight)
        
        hydrostatic_pressure = None
        if metric_type == 'pressure':
            # Get the current axis limits, so they can be reassigned after 
            # plotting (in case these data would otherwise change the limits)
            xlim = ax.get_xlim()
            ylim = ax.get_ylim()
            
            topBoundaryPressure = get_parameter_val(salsa_comp, 'topBoundaryPressure')
            
            topBoundaryCond = get_parameter_val(salsa_comp, 'topBoundaryCond')
            
            topLayerType = get_parameter_val(salsa_comp, 'topLayerType')
            
            waterTableDepth = get_parameter_val(salsa_comp, 'waterTableDepth')
            
            # Checks if the water table needs to be considered
            water_table_check = (topLayerType == 1 and topBoundaryCond == 1 
                                 and waterTableDepth != 0)
            
            if water_table_check == 1:
                hydrostatic_pressure = (
                    np.abs(profileDepths) - np.abs(waterTableDepth)) * WATER_DENSITY * GRAV_ACCEL
            else:
                hydrostatic_pressure = np.abs(profileDepths) * WATER_DENSITY * GRAV_ACCEL
            
            hydrostatic_pressure += topBoundaryPressure
            
            if water_table_check:
                hydrostatic_pressure[
                    np.abs(profileDepths) <= np.abs(waterTableDepth)] = topBoundaryPressure
            
            label = 'Hydrostatic Pressure for ' + r'$\rho$' + ' = 1000 kg/m$^3$'
            ax.plot(hydrostatic_pressure, profileDepths, color='k', 
                    linestyle = '--', linewidth=(linewidth / 2), 
                    label=label)
            
            if water_table_check:
                ax.plot([xlim[0], xlim[1]], [waterTableDepth, waterTableDepth], color='k', 
                        linestyle = ':', linewidth=(linewidth / 2), 
                        label='Water Table')
            
            ax.legend(fancybox=False, fontsize=genfontsize - 2, edgecolor=[0, 0, 0], 
                      loc='upper right', framealpha=0.9)
            
            ax.set_xlim(xlim[0], xlim[1])
            ax.set_ylim(ylim[0], ylim[1])
        
        label = 'Time (years)'
        make_colorbar(label, cbar_vals, [ax], colormap, cbar_aspect, 
                      caxisfontsize, fontweight, sci_option=False)
        
        if metric_type == 'head':
            x_label = 'Hydraulic Head (m)'
        elif metric_type == 'pressure':
            x_label = 'Pressure (Pa)'
        
        ax.set_xlabel(x_label, fontsize=xaxisfontsize, 
                      fontweight=fontweight)
        ax.set_ylabel('Depth (m)', fontsize=yaxisfontsize, 
                      fontweight=fontweight)
        
        ax.ticklabel_format(style='sci', axis='x',
                            scilimits=(0, 0), useMathText=True)
        
        ax.set_title(position_title, fontsize=titlefontsize, 
                     fontweight=fontweight)
        
        if title is not None:
            plt.suptitle(title, fontweight=fontweight, fontsize=titlefontsize)
        
        fig_title = 'Shales_Vertical_{}_Profile_{}'.format(
            metric_type.title(), profileInd + 1) + real_addition + extension
        
        plt.savefig(os.path.join(output_dir, fig_title), dpi=dpi_ref)
        plt.close()
        
        if save_csv_files:
            save_vertical_profile_to_csv(
                metric_type, profileInd, profileX_km, profileY_km, profileDepths, 
                output_vals_compiled, times_compiled, hydrostatic_pressure, 
                output_dir, realization=realization)


def salsa_ouput_vs_time_plots(name, metric_type, salsa_comp, sm, s, time_array_years, 
                              output_dir, yaml_input, extension='.png', real_addition='', 
                              analysis='forward', title=None, enforce_y_lims=True, 
                              figsize=(10, 12), colormap=None, cbar_vals=None, 
                              cbar_aspect=None, genfontsize=10, xaxisfontsize=12, 
                              yaxisfontsize=12, caxisfontsize=12, titlefontsize=12, 
                              lgndfontsize=9, linewidth=3, fontweight='bold', 
                              dpi_ref=100, save_csv_files=True):
    """
    Creates time series plots of output in each unit, where results are 
    color labelled by depth. The subplots are arranged in one column, so using 
    a large number of units may require changes to the figsize keyword argument 
    (i.e., to prevent the subplots from being too small / crowded).
    """
    if colormap is None:
        colormap = colormaps.get_cmap(DEPTH_COLORMAP)
    elif isinstance(colormap, str):
        colormap = colormaps.get_cmap(colormap)
    
    if cbar_aspect is None:
        cbar_aspect = DEFAULT_CBAR_ASPECT
        
    realization = yaml_input['Realization']
    
    realization = check_realization(
        realization, name, analysis=analysis, plot_type='Salsa{}TimeSeries'.format(
            metric_type.title()))
    
    numberOfShaleLayers = get_parameter_val(salsa_comp, 'numberOfShaleLayers')
    numberOfAquiferLayers = salsa_comp.calc_num_aquifers()
    
    numberOfVerticalPoints = get_parameter_val(salsa_comp, 'numberOfVerticalPoints')
    
    shaleCoordx = salsa_comp.shaleCoordx
    shaleCoordy = salsa_comp.shaleCoordy
    
    aquiferCoordx = salsa_comp.aquiferCoordx
    aquiferCoordy = salsa_comp.aquiferCoordy
    
    activeWellQ = salsa_comp.activeWellQ
    
    figsize_shales = figsize
    figsize_aquifers = figsize
    # Adjust the figure sizes based on the number of units
    if figsize is not None:
        height_shales = 12 * (numberOfShaleLayers / 3)
        figsize_shales = (10, height_shales)
        
        height_aquifers = 8 * (numberOfAquiferLayers / 2)
        figsize_aquifers = (10, height_aquifers)
    
    # Extract the output
    # Output profiles across the aquitards, correspond with shaleCoordx and shaleCoordy
    outputProfilesShales = {}
    for shaleRef in range(numberOfShaleLayers):
        outputProfilesShales['shale{}'.format(shaleRef + 1)] = {}
        
        for profileRef in range(len(shaleCoordx)):
            outputProfilesShales['shale{}'.format(shaleRef + 1)][
                'profile{}'.format(profileRef + 1)] = {}
            
            for pointRef in range(numberOfVerticalPoints):
                full_obs_nm = '{}Profile{}VertPoint{}Shale{}'.format(
                    metric_type, profileRef + 1, pointRef + 1, shaleRef + 1)
                
                try:
                    values = get_values(salsa_comp, sm, s, full_obs_nm, time_array_years, 
                                        analysis=analysis, realization=realization)
                except:
                    err_msg = OUTPUT_NOT_AVAILABLE_MSG.format(
                        'Salsa{}TimeSeries'.format(metric_type.title()), name, 
                        full_obs_nm, salsa_comp.name) 
                    
                    logging.error(err_msg)
                    raise KeyError(err_msg)

                outputProfilesShales['shale{}'.format(shaleRef + 1)][
                    'profile{}'.format(profileRef + 1)][
                        'point{}'.format(pointRef + 1)] = values
    
    # Output in the aquifers, correspond with aquiferCoordx and aquiferCoordy
    aquifer_locations = ['']
    aquifer_location_keys = ['Entire']
    if metric_type == 'pressure':
        aquifer_locations = ['', 'Mid', 'Top']
        aquifer_location_keys = ['Bottom', 'Mid', 'Top']
    
    outputLocAquifers = {}
    for aquRef in range(numberOfAquiferLayers):
        outputLocAquifers['aquifer{}'.format(aquRef + 1)] = {}
        
        for locRef in range(len(aquiferCoordx)):
            if not 'loc{}'.format(locRef + 1) in outputLocAquifers[
                    'aquifer{}'.format(aquRef + 1)]:
                outputLocAquifers['aquifer{}'.format(aquRef + 1)][
                        'loc{}'.format(locRef + 1)] = {}
            
            for (aq_loc, aq_key) in zip(aquifer_locations, aquifer_location_keys):
                full_obs_nm = '{}Loc{}{}Aquifer{}'.format(
                    metric_type, locRef + 1, aq_loc, aquRef + 1)
                
                try:
                    values = get_values(salsa_comp, sm, s, full_obs_nm, time_array_years, 
                                        analysis=analysis, realization=realization)
                except:
                    err_msg = OUTPUT_NOT_AVAILABLE_MSG.format(
                        'Salsa{}TimeSeries'.format(metric_type.title()), name, 
                        full_obs_nm, salsa_comp.name) 
                    
                    logging.error(err_msg)
                    raise KeyError(err_msg)
                
                outputLocAquifers['aquifer{}'.format(aquRef + 1)][
                        'loc{}'.format(locRef + 1)][aq_key] = values
    
    depths_file_name = get_file_name(salsa_comp.name, 'profileDepths', 
                                     analysis=analysis, realization=realization)
    
    try:
        data = np.load(os.path.join(output_dir, depths_file_name))
        profileDepths = data['data']
        data.close()
    except:
        err_msg = OUTPUT_NOT_AVAILABLE_MSG.format(
            'Salsa{}TimeSeries'.format(metric_type.title()), name, 
            'profileDepths', salsa_comp.name) 
        
        logging.error(err_msg)
        raise KeyError(err_msg)
    
    shaleThicknesses, aquiferThicknesses, bottomLayerType, \
        topLayerType = get_salsa_strata_info(salsa_comp)
    
    shaleDepths, shaleMidDepths, shaleTopDepths, aquiferDepths, \
        aquiferMidDepths, aquiferTopDepths = salsa_comp.get_unit_depths(
            shale_thickness_list=shaleThicknesses, aquifer_thickness_list=aquiferThicknesses, 
            bottom_layer_type=bottomLayerType, top_layer_type=topLayerType)
    
    if cbar_vals is None:
        cbar_vals = np.linspace(
            np.min(profileDepths[~np.isnan(profileDepths)]), 
            np.max(profileDepths[~np.isnan(profileDepths)]), 200)
    
    # For convenience when saving results, recompile the shale depths (one depth 
    # for each vertical point from the numberOfVerticalPoints parameter)
    shaleNames = []
    shaleDepthsCompiled = []
    shaleOutputVals = []
    
    compile_results = save_csv_files
    
    # Output in the aquitards vs. time, colored by depth
    for profileRef in range(len(shaleCoordx)):
        fig = plt.figure(profileRef, figsize=figsize_shales, dpi=dpi_ref)
        
        axes = []
        
        profileX_km = shaleCoordx[profileRef] / 1000
        profileY_km = shaleCoordy[profileRef] / 1000
        
        # If the coordinate is 0 km, do not need to show 0.00 km
        profileX_km_str = '{:.2f} km'.format(profileX_km)
        if profileX_km == 0:
            profileX_km_str = '0 km'
        
        profileY_km_str = '{:.2f} km'.format(profileY_km)
        if profileY_km == 0:
            profileY_km_str = '0 km'
        
        position_title = 'Profile {} at x = {} and y = {}'.format(
            profileRef + 1, profileX_km_str, profileY_km_str)
        
        if realization is not None:
            position_title += ', Realization {}'.format(realization + 1)
        
        # These are overwritten
        min_val = INITIAL_MIN_VAL
        max_val = INITIAL_MAX_VAL
        
        num_aquifers_below = -1
        if bottomLayerType == 1:
            num_aquifers_below += 1
        
        max_subplots = numberOfShaleLayers
        for shaleRef in range(numberOfShaleLayers):
            if (num_aquifers_below + 1) <= numberOfAquiferLayers:
                num_aquifers_below += 1
            
            # The shales should be shown with shale 1 at the bottom
            subplot_num = numberOfShaleLayers - (shaleRef + 1) + 1
            
            ax = plt.subplot(numberOfShaleLayers, 1, subplot_num)
            axes.append(ax)
            
            min_val, max_val, shaleNames, shaleDepthsCompiled, \
                shaleOutputVals = plot_shale_output_vs_time(
                    metric_type, ax, subplot_num, max_subplots, profileRef, 
                    pointRef, shaleRef, num_aquifers_below, numberOfShaleLayers, 
                    numberOfAquiferLayers, numberOfVerticalPoints, profileDepths, 
                    outputProfilesShales, time_array_years, colormap, 
                    xaxisfontsize, yaxisfontsize, fontweight, linewidth, 
                    enforce_y_lims, min_val, max_val, position_title=position_title, 
                    compile_results=compile_results, shaleDepthsCompiled=shaleDepthsCompiled, 
                    shaleNames=shaleNames, shaleOutputVals=shaleOutputVals)
            
            subplot_num -= 1
            
            ax.ticklabel_format(style='sci', axis='y',
                                scilimits=(0, 0), useMathText=True)
        
        plt.subplots_adjust(left=0.1, bottom=0.1, right=0.95,
                            top=0.9, wspace=0.1, hspace=0.25)
        
        label = 'Depth Range in the Model (m)'
        make_colorbar(label, cbar_vals, axes, colormap, cbar_aspect, caxisfontsize, 
                      fontweight, min_cbar_value=np.min(cbar_vals), sci_option=False)
        
        set_axis_limts(max_subplots, enforce_y_lims, min_val, max_val)
        
        if title is not None:
            plt.suptitle(title, fontweight=fontweight, fontsize=titlefontsize)
        
        fig_title = 'Shale_{}_Time_Series_Profile_{}'.format(
            metric_type.title(), profileRef + 1) + real_addition + extension
        
        plt.savefig(os.path.join(output_dir, fig_title), dpi=dpi_ref)
        plt.close()
        
        if save_csv_files:
            save_shale_output_data_to_csv(
                metric_type, profileRef, profileX_km, profileY_km, time_array_years, 
                shaleNames, shaleDepthsCompiled, shaleOutputVals, output_dir, 
                realization=realization)
    
    # aquiferNames is used to keep track of which values correspond to which aquifer
    aquiferNames = []
    aquiferOutputVals = []
    
    # Output values in the aquifers
    for locRef in range(len(shaleCoordx)):
        fig = plt.figure(locRef, figsize=figsize_aquifers, dpi=dpi_ref)
        
        axes = []
        
        locX_km = aquiferCoordx[locRef] / 1000
        locY_km = aquiferCoordy[locRef] / 1000
        
        # If the coordinate is 0 km, do not need to show 0.00 km
        locX_km_str = '{:.2f} km'.format(locX_km)
        if locX_km == 0:
            locX_km_str = '0 km'
        
        locY_km_str = '{:.2f} km'.format(locY_km)
        if locY_km == 0:
            locY_km_str = '0 km'
        
        position_title = 'Location {} at x = {} and y = {}'.format(
            locRef + 1, locX_km_str, locY_km_str)
        
        if realization is not None:
            position_title += ', Realization {}'.format(realization + 1)
        
        min_val = INITIAL_MIN_VAL
        max_val = INITIAL_MAX_VAL
        shaleRef = -1
        max_subplots = numberOfAquiferLayers
        for aquRef in range(numberOfAquiferLayers):
            # The aquifers should be shown with aquifer 1 at the bottom
            subplot_num = numberOfAquiferLayers - (aquRef + 1) + 1
            
            ax = plt.subplot(numberOfAquiferLayers, 1, subplot_num)
            axes.append(ax)
            
            min_val, max_val, aquiferNames, aquiferOutputVals = plot_aquifer_output_vs_time(
                metric_type, ax, subplot_num, max_subplots, locRef, aquRef, 
                numberOfAquiferLayers, profileDepths, outputLocAquifers, 
                aquiferDepths, aquiferMidDepths, aquiferTopDepths, activeWellQ, 
                time_array_years, colormap, xaxisfontsize, yaxisfontsize, 
                fontweight, linewidth, enforce_y_lims, min_val, max_val, 
                position_title=position_title, compile_results=compile_results, 
                aquiferNames=aquiferNames, aquiferOutputVals=aquiferOutputVals)
            
            subplot_num -= 1
            
            ax.ticklabel_format(style='sci', axis='y',
                                scilimits=(0, 0), useMathText=True)
        
        plt.subplots_adjust(left=0.1, bottom=0.1, right=0.95,
                            top=0.9, wspace=0.1, hspace=0.25)
        
        label = 'Depth Range in the Model (m)'
        make_colorbar(label, cbar_vals, axes, colormap, cbar_aspect, caxisfontsize, 
                      fontweight, min_cbar_value=np.min(cbar_vals), sci_option=False)
        
        set_axis_limts(max_subplots, enforce_y_lims, min_val, max_val)
        
        if title is not None:
            plt.suptitle(title, fontweight=fontweight, fontsize=titlefontsize)
        
        fig_title = 'Aquifer_{}_Time_Series_Loc_{}'.format(
            metric_type.title(), locRef + 1) + real_addition + extension
        
        plt.savefig(os.path.join(output_dir, fig_title), dpi=dpi_ref)
        plt.close()
        
        if save_csv_files:
            save_aquifer_output_data_to_csv(
                metric_type, locRef, locX_km, locY_km, time_array_years, aquiferNames, 
                aquiferOutputVals, aquiferDepths, aquiferTopDepths, output_dir, 
                realization=realization)


def salsa_contour_plot(name, metric_type, aquifer_location, salsa_comp, sm, 
                       time_array_years, output_dir, yaml_input, analysis='forward', 
                       title=None, extension='.png', real_addition='', 
                       figsize=(10, 8), colormap=None, cbar_vals=None, 
                       cbar_aspect=None, genfontsize=10, xaxisfontsize=12, 
                       yaxisfontsize=12, caxisfontsize=12, titlefontsize=12, 
                       lgndfontsize=9, fontweight='bold', dpi_ref=100, 
                       save_csv_files=True, enforce_xy_lims=False,
                       plot_injection_sites=True, plot_wellbores=True):
    """
    Creates a contour plot of output within an aquifer (head or pressure). Uses 
    the contour plot data saved to .npz files by the SALSA component.
    """
    xLims = None
    yLims = None
    if yaml_input['EnforceXandYLims'] is not None:
        enforce_xy_lims = yaml_input['EnforceXandYLims']
        xLims = yaml_input['xLims']
        yLims = yaml_input['yLims']
    
    if yaml_input['plot_injection_sites'] is not None:
        plot_injection_sites = yaml_input['plot_injection_sites']
    
    if plot_injection_sites:
        InjectionCoordx = salsa_comp.activeWellCoordx
        InjectionCoordy = salsa_comp.activeWellCoordy
    
    if yaml_input['plot_wellbores'] is not None:
        plot_wellbores = yaml_input['plot_wellbores']
    
    if plot_wellbores:
        leakingWellCoordx = salsa_comp.leakingWellCoordx
        leakingWellCoordy = salsa_comp.leakingWellCoordy
    
    AquiferNameList = yaml_input['AquiferNameList']
    
    # Used to filter which aquifers get their results plotted
    AquiferNameList = check_aq_name_list(AquiferNameList, salsa_comp, name)  
    
    if colormap is None:
        colormap = colormaps.get_cmap(HEAD_COLORMAP)
    elif isinstance(colormap, str):
        colormap = colormaps.get_cmap(colormap)
    
    if cbar_aspect is None:
        cbar_aspect = DEFAULT_CBAR_ASPECT
    
    realization = yaml_input['Realization']
    
    realization = check_realization(
        realization, name, analysis=analysis, plot_type='SalsaContourPlot')
    
    time_index_list = range(len(time_array_years))
    if yaml_input['TimeList'] is not None:
        time_list = yaml_input['TimeList']

        if time_list != 'All':
            time_index_list = get_t_indices(time_list, time_array_years)
    
    if metric_type == 'head':
        output_name = 'contourPlotAquiferHead'
        output_label = 'Hydraulic Head'
        output_units = 'm'
        output_title_addition = 'Head'
    elif metric_type == 'pressure':
        aq_loc = ''
        if aquifer_location == 'Middle':
            aq_loc = 'Mid'
        elif aquifer_location == 'Top':
            aq_loc = 'Top'
        
        output_name = 'contourPlot{}AquiferPressure'.format(aq_loc)
        output_label = 'Pressure at {} of Unit'.format(aquifer_location)
        output_units = 'Pa'
        output_title_addition = '{}_Pressure'.format(aquifer_location)
    
    file_name = get_file_name(salsa_comp.name, output_name,  analysis=analysis, 
                              realization=realization)
    
    try:
        data = np.load(os.path.join(output_dir, file_name))
        contourPlotAquiferOutput = data['data']
        data.close()
    except:
        err_msg = OUTPUT_NOT_AVAILABLE_MSG.format(
            'SalsaContourPlot', name, output_name, salsa_comp.name) 
        
        logging.error(err_msg)
        raise KeyError(err_msg)
    
    file_name = get_file_name(salsa_comp.name, 'contourPlotCoordx', 
                              analysis=analysis, realization=realization)
    
    try:
        data = np.load(os.path.join(output_dir, file_name))
        contourPlotCoordx = data['data']
        data.close()
    except:
        err_msg = OUTPUT_NOT_AVAILABLE_MSG.format(
            'SalsaContourPlot', name, 'contourPlotCoordx', salsa_comp.name) 
        
        logging.error(err_msg)
        raise KeyError(err_msg)
    
    file_name = get_file_name(salsa_comp.name, 'contourPlotCoordy', 
                              analysis=analysis, realization=realization)
    
    try:
        data = np.load(os.path.join(output_dir, file_name))
        contourPlotCoordy = data['data']
        data.close()
    except:
        err_msg = OUTPUT_NOT_AVAILABLE_MSG.format(
            'SalsaContourPlot', name, 'contourPlotCoordy', salsa_comp.name) 
        
        logging.error(err_msg)
        raise KeyError(err_msg)
    
    aquiferNamesContourPlots = salsa_comp.aquiferNamesContourPlots
    
    # One for each aquifer in the contour plot data
    min_output_vals = [None] * contourPlotAquiferOutput.shape[1]
    max_output_vals = [None] * contourPlotAquiferOutput.shape[1]
    output_cbar_vals = [None] * contourPlotAquiferOutput.shape[1]
    for aquPlotInd in range(contourPlotAquiferOutput.shape[1]):
        aquifer_number = aquiferNamesContourPlots[aquPlotInd]
        aquInd = aquifer_number - 1
        
        min_output_vals[aquPlotInd] = np.min(contourPlotAquiferOutput[
            :, aquPlotInd, :][~np.isnan(contourPlotAquiferOutput[:, aquPlotInd, :])])
        
        max_output_vals[aquPlotInd] = np.max(contourPlotAquiferOutput[
            :, aquPlotInd, :][~np.isnan(contourPlotAquiferOutput[:, aquPlotInd, :])])
        
        output_cbar_vals[aquPlotInd] = np.linspace(min_output_vals[aquPlotInd], 
                                                 max_output_vals[aquPlotInd], 200)
    
    activeWellQ = salsa_comp.activeWellQ
    
    cmap = colormaps.get_cmap(colormap)
    
    shaleThicknesses, aquiferThicknesses, bottomLayerType, \
        topLayerType = get_salsa_strata_info(salsa_comp)
    
    shaleDepths, shaleMidDepths, shaleTopDepths, aquiferDepths, \
        aquiferMidDepths, aquiferTopDepths = salsa_comp.get_unit_depths(
            shale_thickness_list=shaleThicknesses, aquifer_thickness_list=aquiferThicknesses, 
            bottom_layer_type=bottomLayerType, top_layer_type=topLayerType)
    
    # The aquPlotInd refers to 0:numberOfAquiferNames, where 
    # numberOfAquiferNames is set with the length of aquiferNamesContourPlots
    for aquPlotInd in range(contourPlotAquiferOutput.shape[1]):
        aquifer_number = aquiferNamesContourPlots[aquPlotInd]
        aquInd = aquifer_number - 1
        
        aquifer_bottom_depth = aquiferDepths[aquInd]
        aquifer_top_depth = aquiferTopDepths[aquInd]
        
        for timeInd in time_index_list:
            
            time = time_array_years[timeInd]
            
            make_plot = False
            if AquiferNameList == 'All':
                make_plot = True
            elif isinstance(AquiferNameList, list):
                if aquifer_number in AquiferNameList:
                    make_plot = True
            
            # SALSA does not produce results for t = 0 years. Only make the plot 
            # if AquiferNameList is 'All' or if this aquifer number is included 
            # in AquiferNameList.
            if not time == 0 and make_plot:
                output_vals = contourPlotAquiferOutput[
                    timeInd, aquPlotInd, :]
                
                fig = plt.figure(timeInd, figsize=figsize, dpi=dpi_ref)
                
                ax = plt.gca()
                ax.set_facecolor(BACKGROUND_COLOR)
                
                successful_plot = True
                try:
                    plt.tricontourf(
                        contourPlotCoordx / 1000, contourPlotCoordy / 1000, 
                        output_vals, cmap=colormap, levels=output_cbar_vals[aquPlotInd], 
                        locator=ticker.MaxNLocator(nbins=100, prune='lower'))
                except:
                    err_msg = ''.join([
                        'While creating the SalsaContourPlot {} showing {} output ', 
                        'at t = {} years, there was an error plotting the data. ', 
                        'This error can occur if the spatial distribution of the ', 
                        'output is not compatible with the plotting function used, ', 
                        'matplotlib.pyplot.tricontourf(). The plot will not ', 
                        'be saved.']).format(
                            name, metric_type, time)
                    logging.error(err_msg)
                    
                    successful_plot = False
                
                pcrit_evaluated = False
                pcrit_shown = False
                # Evaluate whether the critical pressure was exceeded
                if successful_plot and metric_type == 'pressure':
                    crit_pressure, usdw_aquifer_num = get_salsa_crit_pressure(
                        yaml_input, salsa_comp, aquifer_number, aquifer_location, 
                        name)
                    
                    if crit_pressure is not None:
                        pcrit_evaluated = True
                        pressure_levels = np.array([crit_pressure])
                        
                        a, b = '{:.2e}'.format(crit_pressure).split('e')
                        b = int(b)
                        crit_pressure_str = r'${}\times10^{{{}}}$'.format(a, b)
                        
                        if np.max(output_vals) < crit_pressure:
                            title_crit_pressure = ',\nDid Not Exceed the P$_{crit}$ of ' + \
                                '{} Pa'.format(crit_pressure_str)
                            
                        elif np.min(output_vals) > crit_pressure:
                            title_crit_pressure = ',\nAll Pressures Exceeded the P$_{crit}$ of ' + \
                                '{} Pa'.format(crit_pressure_str)
                            
                        elif np.min(output_vals) < crit_pressure <= np.max(output_vals):
                            title_crit_pressure = ',\nCertain Pressures Exceeded the P$_{crit}$ of ' + \
                                '{} Pa'.format(crit_pressure_str)
                                
                            # Specifies that the critical pressure is within the 
                            # domain and shown with a magenta line
                            pcrit_shown = True
                            
                            # The handle and label for this are created manually below
                            plt.tricontour(
                                contourPlotCoordx / 1000, contourPlotCoordy / 1000,
                                output_vals, pressure_levels, colors = 'm')
                        
                        if usdw_aquifer_num is not None:
                            title_crit_pressure += f' for aquifer {usdw_aquifer_num}'
                
                if not successful_plot:
                    plt.close()
                else:
                    ax.set_xlabel('Easting (km)', fontsize=xaxisfontsize, 
                                  fontweight=fontweight)
                    ax.set_ylabel('Northing (km)', fontsize=yaxisfontsize, 
                                  fontweight=fontweight)
                    
                    label = 'Aquifer {}'.format(aquifer_number)
                    
                    # Specify if the unit is a targeted reservoir. In stochastic 
                    # simulations, this property will be None, however.
                    if activeWellQ is not None:
                        if np.max(activeWellQ[aquInd][:][:]) > 0:
                            label += ' {}'.format(RESERVOIR_LABEL)
                    
                    label += ' at t = {:.2f} years,'.format(time_array_years[timeInd])
                    
                    if realization is not None:
                        label += ' Realization {},'.format(realization + 1)
                    
                    label += '\n'
                    
                    # It is unnecessary to show 0.00 m for 0 m
                    if np.min(output_vals) == 0:
                        min_val_str = r'$0$'
                    else:
                        a, b = '{:.2e}'.format(np.min(output_vals)).split('e')
                        b = int(b)
                        min_val_str = r'${}\times10^{{{}}}$'.format(a, b)
                    
                    if np.max(output_vals) == 0:
                        max_val_str = r'$0$'
                    else:
                        a, b = '{:.2e}'.format(np.max(output_vals)).split('e')
                        b = int(b)
                        max_val_str = r'${}\times10^{{{}}}$'.format(a, b)
                    
                    if min_val_str == max_val_str:
                        range_str = '{}, Value: {} {}'.format(
                            output_label, min_val_str, output_units)
                    else:
                        range_str = '{}, Range: {} {} to {} {}'.format(
                            output_label, min_val_str, output_units, 
                            max_val_str, output_units)
                    
                    label += range_str
                    
                    if pcrit_evaluated:
                        label += title_crit_pressure
                    
                    ax.set_title(label, fontsize=titlefontsize, 
                                 fontweight=fontweight)
                    
                    label = '{} ({})'.format(output_label, output_units)
                    make_colorbar(label, output_cbar_vals[aquPlotInd], [ax], cmap, 
                                  cbar_aspect, caxisfontsize, fontweight, 
                                  min_cbar_value=min_output_vals[aquPlotInd], 
                                  max_cbar_value=max_output_vals[aquPlotInd])
                    
                    if plot_injection_sites:
                        plot_injection_sites_func(ax, InjectionCoordx, InjectionCoordy)
                    
                    if plot_wellbores:
                        plot_wellbores_func(ax, leakingWellCoordx, leakingWellCoordy)
                    
                    if (plot_injection_sites or plot_wellbores or pcrit_shown):
                        # Create legend
                        handle_list = []
                        label_list = []
                        handles, labels = ax.get_legend_handles_labels()

                        for handle, label in zip(handles, labels):
                            if label not in label_list:
                                handle_list.append(handle)
                                label_list.append(label)

                        # If the metric is pressure and a critical pressure was given, include it in the legend
                        if pcrit_shown:
                            critPressureLabel = 'P$_{crit}$'
                            
                            legend_element_critPressure = Line2D([0], [0], color='m',
                                                                 lw=2, label=critPressureLabel)

                            handle_list.append(legend_element_critPressure)
                            label_list.append(critPressureLabel)
                        
                        ax.legend(handle_list, label_list, fancybox=False, 
                                  fontsize=genfontsize, edgecolor=[0, 0, 0], 
                                  loc='upper left', framealpha=0.67)
                    
                    if enforce_xy_lims:
                        ax.set_xlim([xLims[0] / 1000, xLims[1] / 1000])
                        ax.set_ylim([yLims[0] / 1000, yLims[1] / 1000])
                    
                    if title is not None:
                        plt.suptitle(title, fontweight=fontweight, fontsize=titlefontsize)
                    
                    fig_title = 'SalsaContourPlot_{}_Aquifer_{}_tIndex_{}'.format(
                        output_title_addition, aquifer_number, 
                        timeInd) + real_addition + extension
                    
                    plt.savefig(os.path.join(output_dir, fig_title), dpi=dpi_ref)
                    plt.close()
                
                if save_csv_files:
                    save_contour_plot_data_to_csv(
                        metric_type, time, timeInd, contourPlotCoordx, contourPlotCoordy, 
                        output_vals, aquifer_number, aquifer_bottom_depth, 
                        aquifer_top_depth, output_dir, realization=realization)


def salsa_well_leakage_aor_plot(name, metric_type, aquifer_location, salsa_comp, 
                                sm, s, time_array_years, output_dir, yaml_input, 
                                yaml_data, analysis='forward', title=None, 
                                extension='.png', figsize=(10, 8), colormap=None, 
                                genfontsize=10, xaxisfontsize=12, yaxisfontsize=12, 
                                caxisfontsize=12,titlefontsize=12, lgndfontsize=9, 
                                fontweight='bold', dpi_ref=100, cbar_aspect=None, 
                                save_csv_files=True, enforce_xy_lims=False, 
                                plot_injection_sites=True, plot_wellbores=True):
    """
    Creates SalsaLeakageAoR plots.
    """
    if colormap is None:
        colormap = colormaps.get_cmap(WELL_LEAKAGE_COLORMAP)
    elif isinstance(colormap, str):
        colormap = colormaps.get_cmap(colormap)
    
    if cbar_aspect is None:
        cbar_aspect = DEFAULT_CBAR_ASPECT
    
    xLims = None
    yLims = None
    if yaml_input['EnforceXandYLims'] is not None:
        enforce_xy_lims = yaml_input['EnforceXandYLims']
        xLims = yaml_input['xLims']
        yLims = yaml_input['yLims']
    
    grid_option = False
    try:
        if 'grid' in yaml_data[salsa_comp.name]['LeakingWell']:
            grid_option = True
    except:
        pass
    
    if analysis in ['lhs', 'parstudy']:
        err_msg = ''.join([
            'While making the SalsaLeakageAoR plot {}, the analysis type ', 
            'was {} but the number of realizations could not be extracted. ', 
            'The number of realizations is entered in the .yaml file under ', 
            'ModelParams: Analysis: siz, but this input could not be ', 
            'retrieved. Check your input.']).format(name, analysis)
        
        try:
            num_realizations = yaml_data['ModelParams']['Analysis']['siz']
        except:
            logging.error(err_msg)
            raise KeyError(err_msg)
        
        if num_realizations is None:
            logging.error(err_msg)
            raise KeyError(err_msg)
        
    elif analysis == 'forward':
        num_realizations = 1
    
    if plot_injection_sites:
        InjectionCoordx = salsa_comp.activeWellCoordx
        InjectionCoordy = salsa_comp.activeWellCoordy
    
    activeWellQ = salsa_comp.activeWellQ
    
    leakingWellCoordx = salsa_comp.leakingWellCoordx
    leakingWellCoordy = salsa_comp.leakingWellCoordy
    
    if not isinstance(leakingWellCoordx, (list, np.ndarray)):
        leakingWellCoordx = [leakingWellCoordx]
    
    if not isinstance(leakingWellCoordy, (list, np.ndarray)):
        leakingWellCoordy = [leakingWellCoordy]
    
    if not isinstance(leakingWellCoordx, np.ndarray):
        leakingWellCoordx = np.array(leakingWellCoordx)
    
    if not isinstance(leakingWellCoordy, np.ndarray):
        leakingWellCoordy = np.array(leakingWellCoordy)
    
    singular_vs_plural = ''
    missing_coords = None
    if leakingWellCoordx is None:
        missing_coords = 'leakingWellCoordx'
    
    if leakingWellCoordy is None:
        if isinstance(missing_coords, str):
            missing_coords += ' and leakingWellCoordy'
            singular_vs_plural = 's'
        else:
            missing_coords = ['coordy']
    
    if missing_coords is not None:
        err_msg = ''.join([
            'While making the SalsaLeakageAoR plot {}, the {} keyword ', 
            'argument{} {} could not be retrieved from the SALSA component {}. ', 
            'These keyword arguments are required; check your input.']).format(
                name, missing_coords, singular_vs_plural, missing_coords, 
                salsa_comp.name)
        
        logging.error(err_msg)
        raise KeyError(err_msg)
    
    try:
        # Make sure it the same length as leakingWellCoordy
        numberOfLeakingWells = len(salsa_comp.leakingWellCoordx)
        
        numberOfLeakingWells_v2 = len(salsa_comp.leakingWellCoordy)
        
        if numberOfLeakingWells != numberOfLeakingWells_v2:
            err_msg = ''.join([
                'While making the SalsaLeakageAoR plot {} for the SALSA ', 
                'component {}, the number of leakingWellCoordx values ({}) ', 
                'was not equal to the number of leakingWellCoordy values ({}). ', 
                'Check your inputs.']).format(
                    name, salsa_comp.name, numberOfLeakingWells, 
                    numberOfLeakingWells_v2)
            
            logging.error(err_msg)
            raise KeyError(err_msg)
    except:
        err_msg = ''.join([
            'While making the SalsaLeakageAoR plot {} for the SALSA component ', 
            '{}, there was an error while attempting to determine the number of ', 
            'leakingWellCoordx and/or leakingWellCoordy values. Check your ', 
            'input.']).format(name, salsa_comp.name)
        
        logging.error(err_msg)
        raise KeyError(err_msg)
    
    numberOfAquiferLayers = salsa_comp.calc_num_aquifers()
    
    # Default approach is to focus on the top aquifer
    selected_aquifers = [numberOfAquiferLayers]
    
    # If the AquiferNameList input is given, focus on those aquifers
    if yaml_input['AquiferNameList'] is not None:
        selected_aquifers = yaml_input['AquiferNameList']
        
        if not isinstance(selected_aquifers, list):
            selected_aquifers = [selected_aquifers]
    
    # Specifies whether the plot should show the maximum values across all model 
    # times (False) or the maximum values at specific model times (True)
    time_option = False
    
    time_index_list = [len(time_array_years) - 1]
    
    if yaml_input['TimeList'] is not None:
        time_list = yaml_input['TimeList']
        
        time_option = True
        
        if time_list == 'All':
            time_index_list = range(len(time_array_years))
        else:
            time_index_list = get_t_indices(time_list, time_array_years)
    
    min_val = [DEFAULT_MIN_VALUE] * len(selected_aquifers)
    max_val = [DEFAULT_MAX_VALUE] * len(selected_aquifers)
    
    # Threshold volume in m^3
    vol_thresh = DEFAULT_THRESH_VOL
    
    if yaml_input['ThresholdVolume'] is not None:
        vol_thresh = yaml_input['ThresholdVolume']
    
    min_well_vols = np.ones((numberOfLeakingWells, len(selected_aquifers), 
                             len(time_index_list))) * DEFAULT_MIN_VALUE
    
    max_well_vols = np.ones((numberOfLeakingWells, len(selected_aquifers), 
                             len(time_index_list))) * DEFAULT_MAX_VALUE
    
    for leakingWellRef in range(numberOfLeakingWells):
        for aquInd, aquName in enumerate(selected_aquifers):
            full_obs_nm = 'well{}LeakageVolumeAquifer{}'.format(
                leakingWellRef + 1, aquName)
            
            try:
                # If the analysis is stochastic, having realizations set to None 
                # will return an array containing results for all realizations.
                values = get_values(salsa_comp, sm, s, full_obs_nm, time_array_years, 
                                    analysis=analysis, realization=None)
            except:
                err_msg = OUTPUT_NOT_AVAILABLE_MSG.format(
                    'SalsaLeakageAoR', name, full_obs_nm, salsa_comp.name) 
                
                logging.error(err_msg)
                raise KeyError(err_msg)
            
            if not time_option:
                if (min_well_vols[leakingWellRef, aquInd, 0] > np.min(values) 
                    and ((np.min(values) > vol_thresh) or (np.min(values) < -vol_thresh))):
                    min_well_vols[leakingWellRef, aquInd, 0] = np.min(values)
                    
                    # These are used when making the colorbar
                    if min_val[aquInd] > np.min(values):
                        min_val[aquInd] = np.min(values)
                    
                    if max_val[aquInd] < np.min(values):
                        max_val[aquInd] = np.min(values)
                
                if (max_well_vols[leakingWellRef, aquInd, 0] < np.max(values) 
                    and ((np.max(values) > vol_thresh) or (np.max(values) < -vol_thresh))):
                    max_well_vols[leakingWellRef, aquInd, 0] = np.max(values)
                    
                    # These are used when making the colorbar
                    if min_val[aquInd] > np.max(values):
                        min_val[aquInd] = np.max(values)
                    
                    if max_val[aquInd] < np.max(values):
                        max_val[aquInd] = np.max(values)
                
            else:
                for tInd, timeInd in enumerate(time_index_list):
                    if (min_well_vols[leakingWellRef, aquInd, tInd] > np.min(values[timeInd]) 
                        and ((np.min(values[timeInd]) > vol_thresh) 
                             or (np.min(values[timeInd]) < -vol_thresh))):
                        min_well_vols[leakingWellRef, aquInd, tInd] = np.min(
                            values[timeInd])
                        
                        # These are used when making the colorbar
                        if min_val[aquInd] > np.min(values[timeInd]):
                            min_val[aquInd] = np.min(values[timeInd])
                        
                        if max_val[aquInd] < np.min(values[timeInd]):
                            max_val[aquInd] = np.min(values[timeInd])
                    
                    if (max_well_vols[leakingWellRef, aquInd, tInd] < np.max(values[timeInd]) 
                        and ((np.max(values[timeInd]) > vol_thresh) 
                             or (np.max(values[timeInd]) < -vol_thresh))):
                        max_well_vols[leakingWellRef, aquInd, tInd] = np.max(
                            values[timeInd])
                        
                        # These are used when making the colorbar
                        if min_val[aquInd] > np.max(values[timeInd]):
                            min_val[aquInd] = np.max(values[timeInd])
                        
                        if max_val[aquInd] < np.max(values[timeInd]):
                            max_val[aquInd] = np.max(values[timeInd])
    
    if np.inf in max_well_vols:
        warning_msg = ''.join([
            'While making the SalsaLeakageAoR plot {}, the well leakage volumes ', 
            'included an infinite value. This situation can occur if the user ', 
            'places a leaking well on top of an injection or extraction well. ', 
            'This infinite value will not be displayed in the plot. Check ', 
            'your input.']).format(name)
        
        logging.warning(warning_msg)
    
    output_units = 'm$^3$'
    colorbar_label = f'Well Leakage Volume ({output_units})'
    
    plot_num = 0
    for aquInd, aquName in enumerate(selected_aquifers):
        max_val_to_use = max_val[aquInd]
        
        # If both the min and max are zero, increase the max by one. Otherwise, 
        # the lines below will cause an error.
        if max_val_to_use == min_val[aquInd]:
            max_val_to_use = min_val[aquInd] + 1
        
        if (max_val_to_use * MAX_VAL_ADJUST) > min_val[aquInd]:
            interval = (
                (max_val_to_use * MAX_VAL_ADJUST) - min_val[aquInd]) / 100
            levels = np.arange(
                min_val[aquInd], (max_val_to_use * MAX_VAL_ADJUST) + interval, 
                interval)
        else:
            interval = (
                (max_val_to_use + (np.abs(max_val_to_use) * 0.01)) - min_val[aquInd]) / 100
            levels = np.arange(
                min_val[aquInd], (max_val_to_use  + (np.abs(max_val_to_use) * 0.01)) + interval, 
                interval)
        
        plot_vals = True
        if (min_val[aquInd] in [DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE] or 
            max_val[aquInd] in [DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE]):
                plot_vals = False
        
        for tInd, timeInd in enumerate(time_index_list):
            # This is the number of columns in the legend
            ncol_number = 0
            
            plot_num += 1
            
            fig = plt.figure(plot_num, figsize=figsize, dpi=dpi_ref)
            
            ax = plt.gca()
            ax.set_facecolor(BACKGROUND_COLOR)
            
            if plot_injection_sites:
                plot_injection_sites_func(ax, InjectionCoordx, InjectionCoordy)
                ncol_number += 1
            
            if plot_wellbores:
                plot_wellbores_func(ax, leakingWellCoordx, leakingWellCoordy, 
                                    markersize=4, markeredgewidth=1)
                ncol_number += 1
            
            # First, plot minimum well leakage volumes
            min_well_leakage_volumes = min_well_vols[:, aquInd, tInd]
            
            min_well_leakage_volumes_orig = min_well_leakage_volumes.copy()
            wellX = leakingWellCoordx.copy()
            wellY = leakingWellCoordy.copy()
            
            if plot_vals:
                try:
                    # Remove values left at the default min
                    wellX = wellX[min_well_leakage_volumes != DEFAULT_MIN_VALUE]
                    wellY = wellY[min_well_leakage_volumes != DEFAULT_MIN_VALUE]
                    min_well_leakage_volumes = min_well_leakage_volumes[
                        min_well_leakage_volumes != DEFAULT_MIN_VALUE]
                    
                    # Remove values of zero, so the plot better highlights areas with leakage
                    wellX = wellX[min_well_leakage_volumes != 0]
                    wellY = wellY[min_well_leakage_volumes != 0]
                    min_well_leakage_volumes = min_well_leakage_volumes[
                        min_well_leakage_volumes != 0]
                    
                    # Remove any infinite values, which can happen if a wellbore is 
                    # placed on an injection site
                    wellX = wellX[min_well_leakage_volumes < np.inf]
                    wellY = wellY[min_well_leakage_volumes < np.inf]
                    min_well_leakage_volumes = min_well_leakage_volumes[
                        min_well_leakage_volumes < np.inf]
                    
                    plt.tricontourf(
                        wellX / 1000.0, wellY / 1000.0, min_well_leakage_volumes, levels, 
                        cmap=colormap, locator=ticker.MaxNLocator(nbins=100, prune='lower'), 
                        zorder=1)
                except:
                    # tricontourf() can fail if all values are zero
                    pass
            
            # Plot colors for individual points so there is less ambiguity. Here, 
            # only plotting the minimum well leakage volumes that are nonzero. 
            # Could include positive or negative values.
            lgnd_check = False
            if plot_vals:
                for (xval, yval, leakvol) in zip(wellX, wellY, min_well_leakage_volumes):
                    if leakvol != 0 and leakvol < np.Inf:
                        markersize = 12
                        if leakvol < 0:
                            markersize = 6
                        
                        if leakvol < 0 and not lgnd_check:
                            rgba = colormap(0)
                            # This one has the lowest color. It's just used for the 
                            # legend, and will be plotted over by the lines below.
                            plt.plot(xval / 1000, yval / 1000,
                                     marker='o', markerfacecolor=rgba[0:3],
                                     markeredgecolor='k', markeredgewidth=1.5,
                                     markersize=markersize, linestyle='none',
                                     label='Well with Negative Leakage', zorder=1)
                            lgnd_check = True
                            ncol_number += 1
                        
                        # Get the color by the value's proximity to the upper and lower
                        # limits used for levels (which set the colorbar limits above)
                        rgba = colormap((leakvol - np.min(levels)) 
                                        / (np.max(levels) - np.min(levels)))
                        
                        plt.plot(xval / 1000, yval / 1000,
                                 marker='o', markerfacecolor=rgba[0:3],
                                 markeredgecolor='k', markeredgewidth=1.5,
                                 markersize=markersize, linestyle='none', zorder=1e6)
            
            # Now plot maximum well leakage volumes
            well_leakage_volumes = max_well_vols[:, aquInd, tInd]
            
            well_leakage_volumes_orig = well_leakage_volumes.copy()
            wellX = leakingWellCoordx.copy()
            wellY = leakingWellCoordy.copy()
            
            if plot_vals:
                try:
                    # Remove values left at the default max
                    wellX = wellX[well_leakage_volumes != DEFAULT_MAX_VALUE]
                    wellY = wellY[well_leakage_volumes != DEFAULT_MAX_VALUE]
                    well_leakage_volumes = well_leakage_volumes[
                        well_leakage_volumes != DEFAULT_MAX_VALUE]
                    
                    # Remove values of zero, so the plot better highlights areas with leakage
                    wellX = wellX[well_leakage_volumes != 0]
                    wellY = wellY[well_leakage_volumes != 0]
                    well_leakage_volumes = well_leakage_volumes[
                        well_leakage_volumes != 0]
                    
                    # Remove any infinite values, which can happen if a wellbore is 
                    # placed on an injection site
                    wellX = wellX[well_leakage_volumes < np.inf]
                    wellY = wellY[well_leakage_volumes < np.inf]
                    well_leakage_volumes = well_leakage_volumes[
                        well_leakage_volumes < np.inf]
                    
                    plt.tricontourf(
                        wellX / 1000.0, wellY / 1000.0, well_leakage_volumes, levels, 
                        cmap=colormap, locator=ticker.MaxNLocator(nbins=100, prune='lower'), 
                        zorder = 2)
                except:
                    # tricontourf() can fail if all values are zero
                    pass
                
                try:
                    make_colorbar(colorbar_label, levels, [ax], colormap, 
                                  cbar_aspect, caxisfontsize, fontweight, 
                                  min_cbar_value=np.min(levels), 
                                  max_cbar_value=np.max(levels))
                except:
                    # make_colorbar() can fail if the min value is equal to the max value.
                    pass
            
            # Now, plot colors for each maximum well leakage volume. Could include 
            # positive or negative values.
            lgnd_check = False
            if plot_vals:
                for (xval, yval, leakvol) in zip(wellX, wellY, well_leakage_volumes):
                    if leakvol != 0 and leakvol < np.Inf:
                        markersize = 12
                        if leakvol < 0:
                            markersize = 6
                        
                        if leakvol > 0 and not lgnd_check:
                            rgba = colormap(0.99)
                            # This one has the highest color. It's just used for the 
                            # legend, and will be plotted over by the lines below.
                            plt.plot(xval / 1000, yval / 1000,
                                     marker='o', markerfacecolor=rgba[0:3],
                                     markeredgecolor='k', markeredgewidth=1.5,
                                     markersize=markersize, linestyle='none',
                                     label='Well with Positive Leakage', zorder=1)
                            lgnd_check = True
                            ncol_number += 1
                        
                        # Get the color by the value's proximity to the upper and lower
                        # limits used for levels (which set the colorbar limits above)
                        rgba = colormap((leakvol - np.min(levels)) 
                                            / (np.max(levels) - np.min(levels)))
                        
                        plt.plot(xval / 1000, yval / 1000,
                                 marker='o', markerfacecolor=rgba[0:3],
                                 markeredgecolor='k', markeredgewidth=1.5,
                                 markersize=markersize, linestyle='none', zorder=1e6)
                    
            ax.set_xlabel('Easting (km)', fontsize=xaxisfontsize, 
                          fontweight=fontweight)
            ax.set_ylabel('Northing (km)', fontsize=yaxisfontsize, 
                          fontweight=fontweight)
            
            if analysis in ['lhs', 'parstudy']:
                maximum_str = 'Maximum '
                analysis_str = f' Across {num_realizations} Realizations '
            else:
                maximum_str = ''
                analysis_str = ''
            
            # Default description applies to well leakage volumes that are 
            # all positive
            well_leakage_str = 'Well Leakage Into '
            
            # If either well_leakage_volumes or min_well_leakage_volumes were 
            # trimmed down to having no values, these statements could fail.
            try:
                if np.max(well_leakage_volumes) > 0 and np.min(min_well_leakage_volumes) < 0:
                    well_leakage_str = 'Well Leakage Into (Positive) or Out of (Negative) '
            except:
                pass
            
            try:
                if np.max(well_leakage_volumes) <= 0 and np.min(min_well_leakage_volumes) < 0:
                    well_leakage_str = 'Well Leakage Out of '
            except:
                pass
            
            try:
                if len(well_leakage_volumes) == 0 and np.min(min_well_leakage_volumes) < 0:
                    well_leakage_str = 'Well Leakage Out of '
            except:
                pass
            
            label = maximum_str + well_leakage_str + f'Aquifer {aquName}'
            
            # Specify if the unit is a targeted reservoir. In stochastic 
            # simulations, this property will be None, however.
            if activeWellQ is not None:
                if np.max(activeWellQ[aquName - 1][:][:]) > 0:
                    label += ' {}'.format(RESERVOIR_LABEL)
            
            label += analysis_str
            
            if time_option:
                label += ' at {} years'.format(time_array_years[timeInd])
            
            label += ',\n'
            
            all_well_leakage_volumes = well_leakage_volumes.tolist(
                ) + min_well_leakage_volumes.tolist()
            
            if len(all_well_leakage_volumes) == 0:
                range_str = 'Value: ' + r'$0$' + ' ' + output_units
            else:
                # If the value is zero, just show zero (but with the same font style)
                if np.min(all_well_leakage_volumes) == 0:
                    min_val_str = r'$0$'
                else:
                    a, b = '{:.2e}'.format(np.min(all_well_leakage_volumes)).split('e')
                    b = int(b)
                    min_val_str = r'${}\times10^{{{}}}$'.format(a, b)
                
                if np.max(all_well_leakage_volumes) == 0:
                    max_val_str = r'$0$'
                else:
                    a, b = '{:.2e}'.format(np.max(all_well_leakage_volumes)).split('e')
                    b = int(b)
                    max_val_str = r'${}\times10^{{{}}}$'.format(a, b)
                
                if min_val_str == max_val_str:
                    range_str = 'Value: {} {}'.format(min_val_str, output_units)
                else:
                    range_str = 'Range: {} {} to {} {}'.format(
                        min_val_str, output_units, max_val_str, output_units)
            
            if plot_vals:
                label += range_str + ' '
            
            label += '(Gray: No Leakage)'
            
            ax.set_title(label, fontsize=titlefontsize, fontweight=fontweight)
            
            # Create legend
            handle_list = []
            label_list = []
            handles, labels = ax.get_legend_handles_labels()

            for handle, label in zip(handles, labels):
                if label not in label_list:
                    handle_list.append(handle)
                    label_list.append(label)
            
            if ncol_number <= 2:
                bbox_val = (0.475, -0.075)
            elif ncol_number == 3:
                bbox_val = (0.5, -0.075)
            elif ncol_number >= 4:
                bbox_val = (0.535, -0.075)

            ax.legend(handle_list, label_list, fancybox=False, fontsize=lgndfontsize,
                      ncols=ncol_number, edgecolor=[0, 0, 0], loc='upper center',
                      bbox_to_anchor=bbox_val, framealpha=0.67)
            
            if enforce_xy_lims:
                ax.set_xlim([xLims[0] / 1000, xLims[1] / 1000])
                ax.set_ylim([yLims[0] / 1000, yLims[1] / 1000])
            else:
                # Give some buffer room around the leaking wells, otherwise they 
                # can be cut off at the edges of the figure.
                if grid_option:
                    x_vals_temp = np.unique(leakingWellCoordx)
                    y_vals_temp = np.unique(leakingWellCoordy)

                    ax.set_xlim((np.min(leakingWellCoordx) 
                                - ((x_vals_temp[1] - x_vals_temp[0]))) / 1000,
                                (np.max(leakingWellCoordx) 
                                + ((x_vals_temp[1] - x_vals_temp[0]))) / 1000)

                    ax.set_ylim((np.min(leakingWellCoordy) 
                                - ((y_vals_temp[1] - y_vals_temp[0]))) / 1000,
                                (np.max(leakingWellCoordy) 
                                + ((y_vals_temp[1] - y_vals_temp[0]))) / 1000)
                else:
                    xlim_adjust_val = (np.max(leakingWellCoordx) 
                                       - np.min(leakingWellCoordx)) / 20
                    ylim_adjust_val = (np.max(leakingWellCoordy) 
                                       - np.min(leakingWellCoordy)) / 20

                    ax.set_xlim(
                        (np.min(leakingWellCoordx) - xlim_adjust_val) / 1000,
                        (np.max(leakingWellCoordx) + xlim_adjust_val) / 1000)
                    ax.set_ylim(
                        (np.min(leakingWellCoordy) - ylim_adjust_val) / 1000,
                        (np.max(leakingWellCoordy) + ylim_adjust_val) / 1000)
            
            if title is not None:
                plt.suptitle(title, fontweight=fontweight, fontsize=titlefontsize)
            
            if not time_option:
                fig_title = f'SalsaLeakageAoR_Aquifer_{aquName}' + extension
            elif time_option:
                fig_title = 'SalsaLeakageAoR_Aquifer_{}_tIndex_{}'.format(
                    aquName, timeInd) + extension
            
            plt.savefig(os.path.join(output_dir, fig_title), dpi=dpi_ref)
            plt.close()
            
            if save_csv_files:
                if not plot_vals:
                    min_well_leakage_volumes_orig *= 0
                    well_leakage_volumes_orig *= 0
                
                save_well_leakage_to_csv(
                    leakingWellCoordx, leakingWellCoordy, min_well_leakage_volumes_orig, 
                    well_leakage_volumes_orig, aquName, timeInd, 
                    time_array_years, time_option, output_dir)


def plot_injection_sites_func(ax, InjectionCoordx, InjectionCoordy):
    """
    Plots the locaitons of active wells.
    """
    if not isinstance(InjectionCoordx, (list, np.ndarray)):
        InjectionCoordx = [InjectionCoordx]
    
    if not isinstance(InjectionCoordy, (list, np.ndarray)):
        InjectionCoordy = [InjectionCoordy]
    
    for injRef, (xcoord_val, ycoord_val) in enumerate(
            zip(InjectionCoordx, InjectionCoordy)):
        if injRef == 0:
            ax.plot(xcoord_val / 1000, ycoord_val / 1000, marker='s', 
                    color='k', linestyle='none', markeredgewidth=2, 
                    markersize=6, markerfacecolor='none', zorder=1e7, 
                    label='Injection or Extraction Sites')
        else:
            ax.plot(xcoord_val / 1000, ycoord_val / 1000, marker='s', 
                    color='k', linestyle='none', markeredgewidth=2, markersize=6,
                    markerfacecolor='none', zorder=1e7)


def plot_wellbores_func(ax, leakingWellCoordx, leakingWellCoordy, markersize=6, 
                        markeredgewidth=1.5):
    """
    Plots the locations of leaking wells.
    """
    if not isinstance(leakingWellCoordx, (list, np.ndarray)):
        leakingWellCoordx = [leakingWellCoordx]
    
    if not isinstance(leakingWellCoordy, (list, np.ndarray)):
        leakingWellCoordy = [leakingWellCoordy]
    
    if not isinstance(leakingWellCoordx, np.ndarray):
        leakingWellCoordx = np.array(leakingWellCoordx)
    
    if not isinstance(leakingWellCoordy, np.ndarray):
        leakingWellCoordy = np.array(leakingWellCoordy)
    
    ax.plot(leakingWellCoordx / 1000, leakingWellCoordy / 1000,
            linestyle='none', marker='o', color='k', markeredgewidth=markeredgewidth, 
            markersize=markersize, markerfacecolor='none', zorder=1e5, 
            label='Leaking Well')


def get_values(salsa_comp, sm, s, full_obs_nm, time_array_years, 
               analysis='forward', realization=None):
    """
    Obtains and returns simulation results.
    """
    if analysis == 'forward':
        values = sm.collect_observations_as_time_series(
            salsa_comp, full_obs_nm)
        
    elif analysis in ['lhs', 'parstudy']:
        ind_list = list(range(len(time_array_years)))
        
        values = np.array(
            [s.recarray[salsa_comp.name + '.' + full_obs_nm + '_' 
                        + str(indd)] for indd in ind_list])
        
        if realization is not None:
            values = values[:, realization]
    
    return values


def get_file_name(comp_name, output_type, analysis='forward', realization=None):
    """
    Depending on the analysis type, returns the file name for .npz files saved 
    by the SALSA component.
    """
    if analysis == 'forward':
        file_name = '{}_{}_sim_0_time_0.npz'.format(comp_name, output_type)
        
    elif analysis in ['lhs', 'parstudy']:
        file_name = '{}_{}_sim_{}_time_0.npz'.format(
            comp_name, output_type, realization + 1)
    
    return file_name


def plot_units(ax, shaleDepths, shaleTopDepths, aquiferDepths, aquiferTopDepths, 
               shaleThicknesses, aquiferThicknesses, activeWellQ, genfontsize, 
               fontweight='bold'):
    """
    Plots and labels each unit.
    """
    shaleColor = [0.4, 0.4, 0.4]
    shaleAlpha = 0.8
    
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    
    xvals = [xmin, xmax]
    
    x_text = xmin + ((xmax - xmin) * 0.01)
    
    if isinstance(aquiferThicknesses, np.ndarray):
        aquiferThicknesses = aquiferThicknesses.tolist()
    
    if isinstance(shaleThicknesses, np.ndarray):
        shaleThicknesses = shaleThicknesses.tolist()
    
    y_buffer_text = np.min(aquiferThicknesses + shaleThicknesses) * 0.075
    
    aquInd = 0
    
    for shaleInd in range(len(shaleDepths)):
        ax.plot()
        
        ax.plot(xvals, [shaleTopDepths[shaleInd], shaleTopDepths[shaleInd]],
                color=shaleColor, alpha=shaleAlpha, zorder=1)
        
        ax.plot(xvals, [shaleDepths[shaleInd], shaleDepths[shaleInd]],
                color=shaleColor, alpha=shaleAlpha, zorder=1)
        
        ax.fill_between(
            xvals, [shaleTopDepths[shaleInd], shaleTopDepths[shaleInd]],
            [shaleDepths[shaleInd], shaleDepths[shaleInd]],
            color=shaleColor, alpha=shaleAlpha)
        
        # Text for unit label
        y_text = shaleDepths[shaleInd] + y_buffer_text
        
        label = 'Shale {}'.format(shaleInd + 1)
        
        ax.text(x_text, y_text, label, color='k', alpha=shaleAlpha, 
                fontsize=genfontsize, fontweight=fontweight, 
                zorder=2)
        
        if aquInd <= (len(aquiferDepths) - 1):
            # Text for unit label
            y_text = aquiferDepths[aquInd] + y_buffer_text
            
            label = 'Aquifer {}'.format(aquInd + 1)
            
            # Specify if the unit is a targeted reservoir. In stochastic 
            # simulations, this property will be None, however.
            if activeWellQ is not None:
                if np.max(activeWellQ[aquInd][:][:]) > 0:
                    label += ' {}'.format(RESERVOIR_LABEL)
            
            ax.text(x_text, y_text, label,color='k', alpha=shaleAlpha, 
                    fontsize=genfontsize, fontweight=fontweight, 
                    zorder=2)
            
            aquInd += 1
    
    # This is here for the case where the top unit is an aquifer
    if aquInd <= (len(aquiferDepths) - 1):
        # Text for unit label
        y_text = aquiferDepths[aquInd] + y_buffer_text
        
        label = 'Aquifer {}'.format(aquInd + 1)
        
        # Specify if the unit is a targeted reservoir. In stochastic 
        # simulations, this property will be None, however.
        if activeWellQ is not None:
            if np.max(activeWellQ[aquInd][:][:]) > 0:
                label += ' {}'.format(RESERVOIR_LABEL)
        
        ax.text(x_text, y_text, label,color='k', alpha=shaleAlpha, 
                fontsize=genfontsize, fontweight=fontweight, 
                zorder=2)
        
        aquInd += 1
    
    # reset the limits
    deepest_depth = np.min(shaleDepths)
    if np.min(aquiferDepths) < deepest_depth:
        deepest_depth = np.min(aquiferDepths)
    
    # should be 0
    shallowest_depth = np.max(shaleTopDepths)
    if np.max(aquiferTopDepths) > shallowest_depth:
        shallowest_depth = np.max(aquiferTopDepths)
    
    ax.set_xlim([xmin, xmax])
    ax.set_ylim([deepest_depth, shallowest_depth])


def plot_shale_output_vs_time(metric_type, ax, subplot_num, max_subplots, 
                              profileRef, pointRef, shaleRef, num_aquifers_below, 
                              numberOfShaleLayers, numberOfAquiferLayers, 
                              numberOfVerticalPoints, profileDepths, outputProfilesShales, 
                              time_array_years, cmap, xaxisfontsize, yaxisfontsize, 
                              fontweight, linewidth, enforce_y_lims, min_val, 
                              max_val, titlefontsize=12, position_title=None, 
                              compile_results=False, shaleDepthsCompiled=None, 
                              shaleNames=None, shaleOutputVals=None):
    """
    Plots shale (aquitard) output vs. time, colored by depth.
    """
    # Used to keep track of how many aquifers are beneath this shale layer
    if num_aquifers_below < 0:
        num_aquifers_below = 0
    
    min_val_label = INITIAL_MIN_VAL
    max_val_label = INITIAL_MAX_VAL
    for pointRef in range(numberOfVerticalPoints):
        # numberOfVerticalPoints - 1 across each shale, then one at the 
        # bottom of each aquifer. This is grouped as 
        # (points across deeper units) + points within current shale
        depthInd = ((shaleRef * (numberOfVerticalPoints - 1)) 
                    + num_aquifers_below) + pointRef
        
        rgba = get_color(profileDepths, profileDepths[depthInd], cmap)
        
        vals = outputProfilesShales['shale{}'.format(shaleRef + 1)][
            'profile{}'.format(profileRef + 1)]['point{}'.format(pointRef + 1)]
        
        ax.plot(time_array_years, vals, linewidth=linewidth, 
                color=rgba[0:3])
        
        if enforce_y_lims:
            # These are used to adjust the y axis limits, if enforce_y_lims is True
            if np.min(vals[~np.isnan(vals)]) < min_val:
                min_val = np.min(vals[~np.isnan(vals)])
            
            if np.max(vals[~np.isnan(vals)]) > max_val:
                max_val = np.max(vals[~np.isnan(vals)])
        
        # These are used to set the label below
        if np.min(vals[~np.isnan(vals)]) < min_val_label:
            min_val_label = np.min(vals[~np.isnan(vals)])
        
        if np.max(vals[~np.isnan(vals)]) > max_val_label:
            max_val_label = np.max(vals[~np.isnan(vals)])
        
        if compile_results:
            shaleNames.append(shaleRef + 1)
            shaleDepthsCompiled.append(profileDepths[depthInd])
            shaleOutputVals.append(vals)
    
    if metric_type == 'head':
        units = 'm'
    elif metric_type == 'pressure':
        units = 'Pa'
    
    min_val_str = ''
    if min_val_label != INITIAL_MIN_VAL:
        if min_val_label == 0:
            min_val_str = r'$0$'
        else:
            a, b = '{:.2e}'.format(min_val_label).split('e')
            b = int(b)
            min_val_str = r'${}\times10^{{{}}}$'.format(a, b)
    
    max_val_str = ''
    if max_val_label != INITIAL_MAX_VAL:
        if max_val_label == 0:
            max_val_str = r'$0$'
        else:
            a, b = '{:.2e}'.format(max_val_label).split('e')
            b = int(b)
            max_val_str = r'${}\times10^{{{}}}$'.format(a, b)
    
    if min_val_str == max_val_str:
        label = 'Shale {}, {} Value: {} {}'.format(
            shaleRef + 1, metric_type.title(), min_val_str, units)
    else:
        label = 'Shale {}, {} Range: {} {} to {} {}'.format(
            shaleRef + 1, metric_type.title(), min_val_str, units, 
            max_val_str, units)
    
    if subplot_num == 1 and (position_title is not None):
        label = position_title + '\n' + label
    
    ax.set_title(label, fontsize=titlefontsize, fontweight=fontweight)
    
    y_label = '{} ({})'.format(metric_type.title(), units)
    
    ax.set_ylabel(y_label, fontsize=yaxisfontsize, 
                  fontweight=fontweight)
    
    if subplot_num == max_subplots:
        ax.set_xlabel('Time (years)', fontsize=xaxisfontsize, 
                      fontweight=fontweight)
    
    return min_val, max_val, shaleNames, shaleDepthsCompiled, shaleOutputVals
          

def plot_aquifer_output_vs_time(metric_type, ax, subplot_num, max_subplots, 
                                locRef, aquRef, numberOfAquiferLayers, profileDepths, 
                                outputLocAquifers, aquiferDepths, aquiferMidDepths, 
                                aquiferTopDepths, activeWellQ, time_array_years, 
                                cmap, xaxisfontsize, yaxisfontsize, fontweight, 
                                linewidth, enforce_y_lims, min_val, max_val, 
                                titlefontsize=12, position_title=None, 
                                compile_results=False, aquiferNames=None, 
                                aquiferOutputVals=None):
    """
    Plots aquifer values vs. time, colored by depth.
    """
    aquifer_location_keys = ['Entire']
    aquifer_depths = [aquiferMidDepths[aquRef]]
    if metric_type == 'pressure':
        aquifer_location_keys = ['Bottom', 'Mid', 'Top']
        aquifer_depths = [aquiferDepths[aquRef], aquiferMidDepths[aquRef], 
                          aquiferTopDepths[aquRef]]
    
    if subplot_num == max_subplots:
        ax.set_xlabel('Time (years)', fontsize=xaxisfontsize, 
                      fontweight=fontweight)
    
    min_val_label = INITIAL_MIN_VAL
    max_val_label = INITIAL_MAX_VAL
    for (key, depth) in zip(aquifer_location_keys, aquifer_depths):
        rgba = get_color(profileDepths, depth, cmap)
        
        vals = outputLocAquifers['aquifer{}'.format(aquRef + 1)][
            'loc{}'.format(locRef + 1)][key]
        
        ax.plot(time_array_years, vals, linewidth=linewidth, 
                color=rgba[0:3])
        
        if np.min(vals[~np.isnan(vals)]) < min_val_label:
            min_val_label = np.min(vals[~np.isnan(vals)])
        
        if np.max(vals[~np.isnan(vals)]) > max_val_label:
            max_val_label = np.max(vals[~np.isnan(vals)])
        
        if enforce_y_lims:
            if np.min(vals[~np.isnan(vals)]) < min_val:
                min_val = np.min(vals[~np.isnan(vals)])
            
            if np.max(vals[~np.isnan(vals)]) > max_val:
                max_val = np.max(vals[~np.isnan(vals)])
    
    reservoir_label = ''
    # Specify if the unit is a targeted reservoir. In stochastic simulations, 
    # this property will be None, however.
    if activeWellQ is not None:
        if np.max(activeWellQ[aquRef][:][:]) > 0:
            reservoir_label = ' {}'.format(RESERVOIR_LABEL)
    
    if metric_type == 'head':
        units = 'm'
    elif metric_type == 'pressure':
        units = 'Pa'
    
    min_val_str = ''
    if min_val_label != INITIAL_MIN_VAL:
        if min_val_label == 0:
            min_val_str = r'$0$'
        else:
            a, b = '{:.2e}'.format(min_val_label).split('e')
            b = int(b)
            min_val_str = r'${}\times10^{{{}}}$'.format(a, b)
    
    max_val_str = ''
    if max_val_label != INITIAL_MAX_VAL:
        if max_val_label == 0:
            max_val_str = r'$0$'
        else:
            a, b = '{:.2e}'.format(max_val_label).split('e')
            b = int(b)
            max_val_str = r'${}\times10^{{{}}}$'.format(a, b)
    
    if min_val_str == max_val_str:
        label = 'Aquifer {}{}, {} Value: {} {}'.format(
            aquRef + 1, reservoir_label, metric_type.title(), 
            min_val_str, units)
    else:
        label = 'Aquifer {}{}, {} Range: {} {} to {} {}'.format(
            aquRef + 1, reservoir_label, metric_type.title(), 
            min_val_str, units, max_val_str, units)
    
    if subplot_num == 1 and (position_title is not None):
        label = position_title + '\n' + label
    
    ax.set_title(label, fontsize=titlefontsize, 
                 fontweight=fontweight)
    
    if metric_type == 'head':
        y_label = 'Head (m)'
    elif metric_type == 'pressure':
        y_label = 'Pressure (Pa)'
    
    ax.set_ylabel(y_label, fontsize=yaxisfontsize, 
                  fontweight=fontweight)
    
    if compile_results:
        aquiferNames.append(aquRef + 1)
        aquiferOutputVals.append(vals)
    
    return min_val, max_val, aquiferNames, aquiferOutputVals


def set_axis_limts(max_subplots, enforce_y_lims, min_val, max_val):
    """
    Sets the x and y limits of each subplot.
    """
    if enforce_y_lims:
        subplot_num = max_subplots
        for subplotRef in range(max_subplots):
            ax = plt.subplot(max_subplots, 1, subplot_num)
            
            # Add some buffer room at the top and bottom of the figure, otherwise 
            # if a line runs along the top or bottom it is not shown well.
            buffer = (max_val - min_val) / 100
            
            ax.set_ylim([min_val - buffer, max_val + buffer])
            
            subplot_num -= 1


def get_color(array, val, cmap):
    """
    Get the color by the value's proximity to the highest and lower values 
    in the array.
    """
    rgba = cmap((val - np.min(array)) / (np.max(array) - np.min(array)))
    
    return rgba


def make_colorbar(label, values, axes, cmap, cbar_aspect, axisfontsize, 
                  fontweight, min_cbar_value=None, max_cbar_value=None, 
                  sci_option=True):
    """
    Makes a colorbar that can extend vertically over multiple axes.
    """
    def fmt(x, pos):
        a, b = '{:.2e}'.format(x).split('e')
        b = int(b)
        return r'${} \times 10^{{{}}}$'.format(a, b)
    
    if sci_option:
        cbar = plt.colorbar(cm.ScalarMappable(cmap=cmap), 
                            values=values, ax=axes, aspect=cbar_aspect, 
                            format=ticker.FuncFormatter(fmt))
    else:
        cbar = plt.colorbar(cm.ScalarMappable(cmap=cmap), 
                            values=values, ax=axes, aspect=cbar_aspect)
    
    cbar.set_label(label, rotation=90, fontsize=axisfontsize, 
                    fontweight=fontweight)
    tick_locator = ticker.MaxNLocator(nbins=5)
    cbar.locator = tick_locator
    cbar.update_ticks()


def not_of_length_2_message(input_name, plot_type, name, mult_entries=False):
    """
    Returns string delivering warning message regarding a list not being
    of length 2.

    input_name: string. Name of input not satisfying the right format.
    name: string. Name of plot setup incorrectly.
    """
    if mult_entries:
        phrase = 'are not lists'
    else:
        phrase = 'is not a list'
    
    msg = ''.join([
        'The {} provided for the {} plot {} {} ',
        'of length 2. Check your inputs in the .yaml file.']).format(
            input_name, plot_type, name, phrase)
    
    return msg


def get_salsa_plot_input(plot_data, name, plot_type='SalsaProfile'):
    """
    Checks for and returns the plot options entered under the plot's section in 
    the .yaml file.
    """
    
    # Default values
    yaml_input_keys = [
        'dpi_input', 'fig_size', 'SaveCSVFiles', 'Realization', 'EnforceXandYLims', 
        'xLims', 'yLims', 'plot_injection_sites', 'plot_wellbores', 'TimeList', 
        'ComponentNameList', 'CriticalPressureMPa', 'AoRAquiferNumber', 
        'AquiferNameList', 'ThresholdVolume']
    
    # Initialize output
    yaml_input = {key: None for key in yaml_input_keys}
    
    # Defaults are True for these options
    yaml_input['plot_injection_sites'] = True
    yaml_input['plot_wellbores'] = True
    
    if plot_data[plot_type] is not None:
        if 'ComponentNameList' in plot_data[plot_type]:
            yaml_input['ComponentNameList'] = plot_data[plot_type]['ComponentNameList']
            
            if not isinstance(yaml_input['ComponentNameList'], list):
                yaml_input['ComponentNameList'] = [yaml_input['ComponentNameList']]
        
        if 'FigureDPI' in plot_data[plot_type]:
            yaml_input['dpi_input'] = plot_data[plot_type]['FigureDPI']
        
        if 'FigureSize' in plot_data[plot_type]:
            yaml_input['fig_size'] = plot_data[plot_type]['FigureSize']
        
        if 'SaveCSVFiles' in plot_data[plot_type]:
            yaml_input['SaveCSVFiles'] = plot_data[plot_type]['SaveCSVFiles']
            
            if not isinstance(yaml_input['SaveCSVFiles'], bool):
                debug_msg = not_boolean_debug_message(
                    'SaveCSVFiles', plot_type, name, True)
                logging.debug(debug_msg)
                
                yaml_input['SaveCSVFiles'] = True
            
        if 'Realization' in plot_data[plot_type]:
            try:
                yaml_input['Realization'] = int(plot_data[plot_type]['Realization'])
            except:
                warning_msg = ''.join([
                    'The "Realization" input ({}) provided for the {} plot {} ', 
                    'could not be turned into an integer. Realization is expected ', 
                    'to be an integer value ranging from 0 to N - 1, where N is ', 
                    'the total number of realizations. The input provided will ', 
                    'not be used.']).format(
                        plot_data[plot_type]['Realization'], plot_type, name)
                
                logging.warning(warning_msg)
        
        if 'SpecifyXandYLims' in plot_data[plot_type]:
            yaml_input['EnforceXandYLims'] = True
            
            yaml_input['xLims'] = plot_data[plot_type]['SpecifyXandYLims']['xLims']
            yaml_input['yLims'] = plot_data[plot_type]['SpecifyXandYLims']['yLims']
            
            disable_lims = False
            mult_entries = False
            inputs_disabled = ''
            if isinstance(yaml_input['xLims'], list) and isinstance(yaml_input['yLims'], list):
                for input_key in ['xLims', 'yLims']:
                    if len(yaml_input[input_key]) != 2:
                        disable_lims = True
                        if inputs_disabled == '':
                            inputs_disabled = input_key
                        else:
                            inputs_disabled += ' and {}'.format(input_key)
                            mult_entries = True
            else:
                disable_lims = True
                
                if not isinstance(yaml_input['xLims'], list):
                    inputs_disabled = 'xLims'
                
                if not isinstance(yaml_input['yLims'], list):
                    if inputs_disabled == '':
                        inputs_disabled = 'yLims'
                    else:
                        inputs_disabled += ' and yLims'
                        mult_entries = True
            
            if disable_lims:
                warning_msg = not_of_length_2_message(
                    inputs_disabled, 'SalsaContourPlot', name, mult_entries=mult_entries)
                logging.warning(warning_msg)
                
                yaml_input['EnforceXandYLims'] = False
        
        if 'PlotInjectionSites' in plot_data[plot_type]:
            yaml_input['plot_injection_sites'] = plot_data[plot_type][
                'PlotInjectionSites']
            
            if not isinstance(yaml_input['plot_injection_sites'], bool):
                debug_msg = not_boolean_debug_message(
                    'PlotInjectionSites', plot_type, name, True)
                logging.debug(debug_msg)
                
                yaml_input['plot_injection_sites'] = True
        
        if 'PlotWellbores' in plot_data[plot_type]:
            yaml_input['plot_wellbores'] = plot_data[plot_type]['PlotWellbores']
            
            if not isinstance(yaml_input['plot_wellbores'], bool):
                debug_msg = not_boolean_debug_message(
                    'PlotWellbores', plot_type, name, True)
                logging.debug(debug_msg)
                
                yaml_input['plot_wellbores'] = True
        
        if 'TimeList' in plot_data[plot_type]:
            yaml_input['TimeList'] = plot_data[plot_type]['TimeList']
        
        if 'CriticalPressureMPa' in plot_data[plot_type]:
            yaml_input['CriticalPressureMPa'] = plot_data[plot_type]['CriticalPressureMPa']
        
        if 'AoRAquiferNumber' in plot_data[plot_type]:
            yaml_input['AoRAquiferNumber'] = plot_data[plot_type]['AoRAquiferNumber']
        
        if 'AquiferNameList' in plot_data[plot_type]:
            yaml_input['AquiferNameList'] = plot_data[plot_type]['AquiferNameList']
        
        if 'ThresholdVolume' in plot_data[plot_type]:
            yaml_input['ThresholdVolume'] = plot_data[plot_type]['ThresholdVolume']
            
            try:
                yaml_input['ThresholdVolume'] = float(yaml_input['ThresholdVolume'])
            except:
                warning_msg = ''.join([
                    f'The "ThresholdVolume" input provided for the {plot_type} ', 
                    f'plot {name} could not be converted to a float value. ', 
                    'The input was therefore not of the appropriate type, and ', 
                    'it will not be used. Check your input.'])
                logging.warning(warning_msg)
                yaml_input['ThresholdVolume'] = None
    
    return yaml_input
    

def not_boolean_debug_message(input_name, plot_type, name, default_value):
    """
    Returns string delivering debug message regarding a variable not being
    of boolean type and setting it to the default value (True or False).

    input_name: string
    default_value: True or False
    """
    msg = ''.join(['Input provided for {} within the {} plot {} was not of ',
                   'boolean type. Using the default value of {}.']).format(
                       input_name, plot_type, name, default_value)
    return msg


def get_t_indices(time_list, time_array_years):
    """
    Returns the time index corresponding to the time_array value closest to the 
    times in time_list. Both time_list and time_array_years have units of years.
    """
    time_index_list = []

    corr_t_index = np.arange(0, len(time_array_years))

    if not isinstance(time_list, list):
        time_list = [time_list]

    for time in time_list:
        abs_diff = np.zeros(len(time_array_years))

        for t_ref, t_val in enumerate(time_array_years):
            abs_diff[t_ref] = np.abs(time - (t_val))

        closest_t_index = corr_t_index[abs_diff == np.min(abs_diff)]

        # If more than one time_array value had the same distance to the time
        # in question (e.g., one value is 0.5 yrs before and another is 0.5 yrs
        # after), then just pick one.
        if len(closest_t_index) > 1:
            closest_t_index = closest_t_index[-1]

        if isinstance(closest_t_index, list):
            closest_t_index = closest_t_index[0]

        time_index_list.append(int(closest_t_index))

    return time_index_list


def save_vertical_profile_to_csv(metric_type, profileRef, profileX_km, profileY_km, 
                                 profileDepths, output_vals_compiled, 
                                 times_compiled, hydrostatic_pressure, 
                                 output_dir, realization=None):
    """
    Saves the profile data used in salsa_vert_profile_plot() to a .csv file.
    """
    output_label = ''
    if metric_type == 'head':
        output_label = 'Hydraulic Head (m)'
    elif metric_type == 'pressure':
        output_label = 'Pressure (Pa)'
    
    data = {'Depth (m)': profileDepths}
    
    if hydrostatic_pressure is not None:
        data['Hydrostatic Pressure (Pa) for a water density of 1000 kg/m^3'] = hydrostatic_pressure
    
    for (time, output_vals) in zip(times_compiled, output_vals_compiled):
        output_label_for_t = 'Shale {} for {} years'.format(output_label, time)
        data[output_label_for_t] = output_vals
    
    file_name = 'Shales_Vertical_{}_Profile_{}'.format(
        metric_type.title(), profileRef + 1)
    
    real_addition = ''
    if realization is not None:
        real_addition = '_Realization_{}'.format(str(realization))
    
    file_name += real_addition + '.csv'
    
    file_path = os.path.join(output_dir, 'csv_files', file_name)

    data = pd.DataFrame(data=data)
    
    data.to_csv(file_path, index=False)


def save_shale_output_data_to_csv(metric_type, profileRef, profileX_km, profileY_km, 
                                  time_array_years, shaleNames, shaleDepthsCompiled, 
                                  shaleOutputVals, output_dir, realization=None):
    """
    Saves the shale output used in salsa_ouput_vs_time_plots() to a .csv file.
    """
    real_addition = ''
    if realization is not None:
        real_addition = '_Realization_{}'.format(str(realization))
    
    output_label = ''
    if metric_type == 'head':
        output_label = 'Hydraulic Head (m)'
    elif metric_type == 'pressure':
        output_label = 'Pressure (Pa)'
    
    data_shales = {'Profile x (km)': profileX_km * np.ones((len(time_array_years))), 
                   'Profile y (km)': profileY_km * np.ones((len(time_array_years))), 
                   'Time (years)': time_array_years
                   }
    
    for shale_nm in shaleNames:
        for depthInd, shaleDepth in enumerate(shaleDepthsCompiled):
            depth_label = 'Shale {}, Point {} Depth (m)'.format(
                shale_nm, depthInd + 1)
            data_shales[depth_label] = shaleDepth * np.ones(len(time_array_years))
            
            output_label_for_depth = 'Shale {}, Point {} {}'.format(
                shale_nm, depthInd + 1, output_label)
            data_shales[output_label_for_depth] = np.array(shaleOutputVals[depthInd])
        
    file_name = 'Shale_{}_Time_Series_{}'.format(
        metric_type.title(), profileRef + 1)
    
    file_name += real_addition + '.csv'
    
    file_path = os.path.join(output_dir, 'csv_files', file_name)

    data = pd.DataFrame(data=data_shales)
    
    data.to_csv(file_path, index=False)


def save_aquifer_output_data_to_csv(metric_type, locRef, locX_km, locY_km, time_array_years, 
                                    aquiferNames, aquiferOutputVals, aquiferDepths, 
                                    aquiferTopDepths, output_dir, realization=None):
    """
    Saves the aquifer output used in salsa_ouput_vs_time_plots() to a .csv file.
    """
    real_addition = ''
    if realization is not None:
        real_addition = '_Realization_{}'.format(str(realization))
    
    output_label = ''
    if metric_type == 'head':
        output_label = 'Hydraulic Head (m)'
    elif metric_type == 'pressure':
        output_label = 'Pressure (Pa)'
    
    data_aquifers = {'Location x (km)': locX_km * np.ones((len(time_array_years))), 
                     'Location y (km)': locY_km * np.ones((len(time_array_years))), 
                     'Time (years)': time_array_years
                     }
    
    for aquInd, aquifer_nm in enumerate(aquiferNames):
        depth_label = 'Aquifer {} Bottom Depth (m)'.format(aquifer_nm)
        data_aquifers[depth_label] = aquiferDepths[aquifer_nm - 1] * np.ones(
            len(time_array_years))
        
        depth_label = 'Aquifer {} Top Depth (m)'.format(aquifer_nm)
        data_aquifers[depth_label] = aquiferTopDepths[aquifer_nm - 1] * np.ones(
            len(time_array_years))
        
        output_label_for_aq = 'Aquifer {} {}'.format(aquifer_nm, output_label)
        data_aquifers[output_label_for_aq] = aquiferOutputVals[
            aquInd] * np.ones((len(time_array_years)))
    
    file_name = 'Aquifer_{}_Time_Series_Location_{}'.format(
        metric_type.title(), locRef + 1)
    
    file_name += real_addition + '.csv'
    
    file_path = os.path.join(output_dir, 'csv_files', file_name)

    data = pd.DataFrame(data=data_aquifers)
    
    data.to_csv(file_path, index=False)


def save_contour_plot_data_to_csv(metric_type, time, timeInd, contourPlotCoordx, 
                                  contourPlotCoordy, vals, aquifer_num, aquifer_bottom_depth, 
                                  aquifer_top_depth, output_dir, realization=None):
    """
    Saves the contour plot data for one aquifer at one time to a .csv file.
    """
    metric_name = None
    if metric_type == 'head':
        metric_name = 'Head'
        output_label = 'Aquifer {} Head (m)'.format(aquifer_num)
    elif metric_type == 'pressure':
        metric_name = 'Pressure'
        output_label = 'Aquifer {} Pressure (Pa)'.format(aquifer_num)
    
    if metric_name is not None:
        file_name = 'Contour_Plot_{}_Aquifer_{:.0f}_tIndex_{:.0f}'.format(
            metric_name, aquifer_num, timeInd)
        
        real_addition = ''
        if realization is not None:
            real_addition = '_Realization_{}'.format(str(realization))
        
        file_name += real_addition + '.csv'
        
        file_path = os.path.join(output_dir, 'csv_files', file_name)
        
        aquifer_bottom_depth_label = 'Aquifer {} Bottom Depth (m)'.format(aquifer_num)
        aquifer_top_depth_label = 'Aquifer {} Top Depth (m)'.format(aquifer_num)
        
        data = {'Time (years)': time * np.ones((len(contourPlotCoordx))), 
                aquifer_bottom_depth_label: aquifer_bottom_depth * np.ones((len(contourPlotCoordx))), 
                aquifer_top_depth_label: aquifer_top_depth * np.ones((len(contourPlotCoordx))), 
                'x (km)': contourPlotCoordx / 1000, 
                'y (km)': contourPlotCoordy / 1000, 
                output_label: vals, 
                }

        data = pd.DataFrame(data=data)
        
        data.to_csv(file_path, index=False)


def save_well_leakage_to_csv(leakingWellCoordx, leakingWellCoordy, min_well_leakage_volumes, 
                             well_leakage_volumes, aquName, timeInd, 
                             time_array_years, time_option, output_dir):
    """
    Saves the well leakage AoR data to a .csv file, either for all model times 
    or for a specific model time
    """
    if not isinstance(leakingWellCoordx, (list, np.ndarray)):
        leakingWellCoordx = [leakingWellCoordx]
    
    if not isinstance(leakingWellCoordy, (list, np.ndarray)):
        leakingWellCoordy = [leakingWellCoordy]
    
    if not isinstance(leakingWellCoordx, np.ndarray):
        leakingWellCoordx = np.array(leakingWellCoordx)
    
    if not isinstance(leakingWellCoordy, np.ndarray):
        leakingWellCoordy = np.array(leakingWellCoordy)
    
    file_name = 'SalsaLeakageAoR_Aquifer_{:.0f}'.format(aquName)
    
    if time_option:
        file_name += '_tIndex_{}'.format(timeInd)
    
    file_name += '.csv'
    
    file_path = os.path.join(output_dir, 'csv_files', file_name)
    
    data = {'Leaking Well X (km)': leakingWellCoordx / 1000, 
            'Leaking Well Y (km)': leakingWellCoordy / 1000, 
            'Minimum Well Leakage Volumes (m^3)': min_well_leakage_volumes, 
            'Maximum Well Leakage Volumes (m^3)': well_leakage_volumes, 
            }
    
    if time_option:
        data['Time (years)'] = np.ones(
            (len(leakingWellCoordx))) * time_array_years[timeInd]
    
    data = pd.DataFrame(data=data)
    
    data.to_csv(file_path, index=False)


def save_salsa_stratigraphy(salsa_comp, output_dir):
    """
    Saves the SALSA component's unit thicknesses and depths to a .csv file.
    """
    shaleThicknesses, aquiferThicknesses, bottomLayerType, \
        topLayerType = get_salsa_strata_info(salsa_comp)
    
    shaleDepths, shaleMidDepths, shaleTopDepths, aquiferDepths, \
        aquiferMidDepths, aquiferTopDepths = salsa_comp.get_unit_depths(
            shale_thickness_list=shaleThicknesses, aquifer_thickness_list=aquiferThicknesses, 
            bottom_layer_type=bottomLayerType, top_layer_type=topLayerType)
    
    data = {}
    # Save the depths from deeper to shallower. To do that, use bottomLayerType to
    # determine whether the bottom layer is a shale or an aquifer.
    aquRef = 0
    if salsa_comp.bottomLayerType == 1:
        data['Aquifer {} Thickness (m)'.format(aquRef + 1)] = [aquiferThicknesses[aquRef]]
        
        data['Aquifer {} Bottom Depth (m)'.format(aquRef + 1)] = [aquiferDepths[aquRef]]
        
        data['Aquifer {} Middle Depth (m)'.format(aquRef + 1)] = [aquiferMidDepths[aquRef]]
        
        data['Aquifer {} Top Depth (m)'.format(aquRef + 1)] = [aquiferTopDepths[aquRef]]
        
        aquRef += 1
    
    for shaleRef in range(len(shaleDepths)):
        data['Shale {} Thickness (m)'.format(shaleRef + 1)] = [shaleThicknesses[shaleRef]]
        
        data['Shale {} Bottom Depth (m)'.format(shaleRef + 1)] = [shaleDepths[shaleRef]]
        
        data['Shale {} Middle Depth (m)'.format(shaleRef + 1)] = [shaleMidDepths[shaleRef]]
        
        data['Shale {} Top Depth (m)'.format(shaleRef + 1)] = [shaleTopDepths[shaleRef]]
        
        if (aquRef + 1) < len(aquiferDepths):
            data['Aquifer {} Thickness (m)'.format(aquRef + 1)] = [aquiferThicknesses[aquRef]]
            
            data['Aquifer {} Bottom Depth (m)'.format(aquRef + 1)] = [aquiferDepths[aquRef]]
            
            data['Aquifer {} Middle Depth (m)'.format(aquRef + 1)] = [aquiferMidDepths[aquRef]]
            
            data['Aquifer {} Top Depth (m)'.format(aquRef + 1)] = [aquiferTopDepths[aquRef]]
            
            aquRef += 1
    
    # This is here for the case where the top unit is an aquifer
    if (aquRef + 1) < len(aquiferDepths):
        data['Aquifer {} Thickness (m)'.format(aquRef + 1)] = [aquiferThicknesses[aquRef]]
        
        data['Aquifer {} Bottom Depth (m)'.format(aquRef + 1)] = [aquiferDepths[aquRef]]
        
        data['Aquifer {} Middle Depth (m)'.format(aquRef + 1)] = [aquiferMidDepths[aquRef]]
        
        data['Aquifer {} Top Depth (m)'.format(aquRef + 1)] = [aquiferTopDepths[aquRef]]
        
        aquRef += 1
    
    file_name = 'SALSA_Stratigraphy.csv'
    
    file_path = os.path.join(output_dir, 'csv_files', file_name)
    
    data = pd.DataFrame(data=data)
    
    data.to_csv(file_path, index=False)


def get_salsa_strata_info(salsa_comp):
    """
    Function that returns lists of the shale and aquifer unit thicknesses, with 
    the first element of each list being unit 1 for that unit type. Unit numbers 
    and proximity to the surface increase with index in each list. The function 
    also returns the values for the bottomLayerType and topLayerType parameters.
    """
    numberOfShaleLayers = get_parameter_val(salsa_comp, 'numberOfShaleLayers')
    numberOfAquiferLayers = salsa_comp.calc_num_aquifers()
    
    shaleThicknesses = []
    for shaleRef in range(numberOfShaleLayers):
        shaleThicknesses.append(
            get_parameter_val(salsa_comp, 'shale{}Thickness'.format(shaleRef + 1)))
    
    aquiferThicknesses = []
    for aquRef in range(numberOfAquiferLayers):
        aquiferThicknesses.append(
            get_parameter_val(salsa_comp, 'aquifer{}Thickness'.format(aquRef + 1)))
    
    bottomLayerType = get_parameter_val(salsa_comp, 'bottomLayerType')
    topLayerType = get_parameter_val(salsa_comp, 'topLayerType')
    
    return shaleThicknesses, aquiferThicknesses, bottomLayerType, topLayerType


def check_realization(realization, name, analysis='forward', plot_type='SalsaProfile'):
    """
    Checks if the realization input is None. If it is, and if the simulation is 
    stochastic (lhs or parstudy), then the realization is set to the default 
    and a warning message is logged
    """
    if realization is None and analysis in ['lhs', 'parstudy']:
        warning_msg = ''.join([
            'The "Realization" entry was not given for the {} ', 
            'plot {}. The default Realization of {} will be used (using an ', 
            'indexing approach where Realization ranges from 1 to N, where N ', 
            'is the total number of realizations).'
            ]).format(plot_type, name, DEFAULT_REALIZATION + 1)
        
        logging.warning(warning_msg)
        realization = DEFAULT_REALIZATION
    elif realization is not None and analysis == 'forward':
        realization = None
    
    return realization


def get_salsa_crit_pressure(yaml_input, salsa_comp, aquifer_number, 
                            aquifer_location, name):
    """
    Returns the critical pressure entered or calculates a critical pressure 
    based on aquifer bottom depth and current depth.
    """
    critPressureInput = yaml_input['CriticalPressureMPa']
    usdw_aquifer_num = yaml_input['AoRAquiferNumber']
    
    numberOfAquiferLayers = salsa_comp.calc_num_aquifers()
    
    # Catch issues with the AoRAquiferNumber input
    if usdw_aquifer_num is not None:
        try:
            # Needs to be an integer
            usdw_aquifer_num = int(usdw_aquifer_num)
        except:
            err_msg = ''.join([
                'The AoRAquiferNumber input ({}) provided for the SalsaContourPlot ', 
                '{} could not be converted into an integer. This entry must be ', 
                'provided as an integer, so it will not be used and critical ', 
                'pressures will not be evaluated for this plot.']).format(
                    usdw_aquifer_num, name)
            logging.error(err_msg)
            
            usdw_aquifer_num = None
    
    if usdw_aquifer_num is not None:
        if (usdw_aquifer_num < 1) or (usdw_aquifer_num > numberOfAquiferLayers):
            err_msg = ''.join([
                'The AoRAquiferNumber input ({}) provided for the SalsaContourPlot ', 
                '{} was not a suitable value. This input must be between 1 and ', 
                'the number of aquifer layers ({}). This AoRAquiferNumber ', 
                'input will not be used, and critical pressures will not be ', 
                'evaluated in the figure.']).format(
                    usdw_aquifer_num, name, numberOfAquiferLayers)
            logging.error(err_msg)
            
            usdw_aquifer_num = None
    
    # If the aquifer for the AoR analysis is below the current aquifer (the aquifer 
    # represented by the current set of gridded SALSA outputs), then do not 
    # proceed with the analysis. The critical pressure calculation assumes that 
    # the pressures examined are in a unit beneath the aquifer for the AoR analysis.
    if usdw_aquifer_num is not None:
        if usdw_aquifer_num <= aquifer_number:
            err_msg = ''.join([
                'While making the SalsaContourPlot {}, the AoRAquiferNumber input ', 
                'was given as {}. This aquifer number is less than or equal to the ', 
                'aquifer number {}, which has gridded pressure results that were ', 
                'used in the SalsaContourPlot. The critical pressures will not be ', 
                'evaluated in this figure.']).format(
                    name, usdw_aquifer_num, aquifer_number)
            logging.err(err_msg)
            
            usdw_aquifer_num = None
    
    crit_pressure = None
    # Only assess critical pressures if the AoRAquiferNumber (usdw_aquifer_num) 
    # was specified, not left as None
    if usdw_aquifer_num is not None:
        shaleThicknesses, aquiferThicknesses, bottomLayerType, \
            topLayerType = get_salsa_strata_info(salsa_comp)
        
        _, _, _, aquiferDepths, aquiferMidDepths, aquiferTopDepths = salsa_comp.get_unit_depths(
            shale_thickness_list=shaleThicknesses, aquifer_thickness_list=aquiferThicknesses, 
            bottom_layer_type=bottomLayerType, top_layer_type=topLayerType)
        
        if critPressureInput == 'Calculated' or critPressureInput is None:
            if aquifer_location == 'Bottom':
                res_depth = np.abs(aquiferDepths[aquifer_number - 1])
            elif aquifer_location == 'Middle':
                res_depth = np.abs(aquiferMidDepths[aquifer_number - 1])
            elif aquifer_location == 'Top':
                res_depth = np.abs(aquiferTopDepths[aquifer_number - 1])
            
            usdw_depth = np.abs(aquiferDepths[usdw_aquifer_num - 1])
            
            brineDensity = get_parameter_val(
                salsa_comp, 'aquifer{}FluidDensity'.format(aquifer_number))
            
            crit_pressure = (usdw_depth * GRAV_ACCEL * WATER_DENSITY) + (
                brineDensity * GRAV_ACCEL * (res_depth - usdw_depth))
            
        elif critPressureInput is not None:
            try:
                crit_pressure = float(critPressureInput)
                
                # Convert from MPa to Pa
                crit_pressure *= 1.0e+6
            except:
                err_msg = ''.join([
                    'The CriticalPressureMPa entry provided for the SalsaContourPlot ', 
                    '{} was {}. This input could not be converted into a float, so ', 
                    'it will not be used. Critical pressures will not be displayed ', 
                    'on the SalsaContourPlot figure.']).format(name, critPressureInput)
                logging.error(err_msg)
                
                crit_pressure = None
    
    return crit_pressure, usdw_aquifer_num


def check_aq_name_list(AquiferNameList, salsa_comp, name):
    """
    Checks if the AquiferNameList entry for the SalsaContourPlot is valid. This 
    entry controls which aquifers are plotted by the SalsaContourPlot. If the 
    input is not appropriate, it is not used.
    """
    if AquiferNameList is None:
        # Default approach is to allow all aquifers to be shown
        AquiferNameList = 'All'
    else:
        warning_msg = ''.join([
            'The AquiferNameList entry ({}) provided for the SalsaContourPlot ', 
            '{} did not match the expected format, and the input will not be ', 
            'used.']).format(AquiferNameList, name)
        
        if not isinstance(AquiferNameList, list):
            try:
                AquiferNameList = [int(AquiferNameList)]
            except:
                logging.warning(warning_msg)
                AquiferNameList = 'All'
        else:
            numberOfAquiferLayers = salsa_comp.calc_num_aquifers()
            
            AquiferNameList_redo = []
            for aqNum in AquiferNameList:
                warning_msg = ''.join([
                    'The value {} in the AquiferNameList entry for the ', 
                    'SalsaContourPlot {} is not an appropriate aquifer number, ', 
                    'given the number of aquifer units for the SALSA component ', 
                    '{} ({}).']).format(
                        aqNum, name, salsa_comp.name, numberOfAquiferLayers)
                
                continue_check = True
                if not isinstance(aqNum, int):
                    try:
                        aqNum = int(aqNum)
                    except:
                        logging.warning(warning_msg)
                        continue_check = False
                
                if continue_check:
                    if (aqNum < 1) or (aqNum > numberOfAquiferLayers):
                        logging.warning(warning_msg)
                    else:
                        AquiferNameList_redo.append(aqNum)
            
            if len(AquiferNameList_redo) == 0:
                warning_msg = ''.join([
                    'The aquifer numbers ({}) provided in the AquiferNameList entry ', 
                    'for the SalsaContourPlot {} were not appropriate aquifer numbers, ', 
                    'given the number of aquifer units for the SALSA component ', 
                    '{} ({}).']).format(
                        AquiferNameList, name, salsa_comp.name, numberOfAquiferLayers)
                logging.warning(warning_msg)
                
                AquiferNameList_redo = 'All'
            
            AquiferNameList = AquiferNameList_redo
    
    return AquiferNameList

.. _equations:

Equations
=========

In this section, we present equations related to geologic carbon storage. While all component 
models have their own governing equations, information about each component can be found in chapter 
:ref:`components_description`. This section is only meant to address equations that are frequently 
referenced throughout this document.

Critical Pressure
-----------------

Critical pressure is the pressure required to lift brine from a storage reservoir and into an overlying 
aquifer along an open conduit (e.g., an open wellbore). The overlying aquifer is generally intended to 
be an underground source of drinking water (USDW) that must be protected from leakage. Therefore, reservoir 
pressures can be evaluated to find the areas where the critical pressure is exceeded; leakage risks would 
be higher in these areas, so the area should be thoroughly surveyed for wellbores and monitored for 
leakage. Indeed, this is part of the concept for the area of review (``AoR``), the area that operators must 
monitor during both site operation and the post-injection site care (``PISC``) period.

If given critical pressure input, an ``AoR`` plot evaluating reservoir pressures will highlight any points 
where the critical pressure is exceeded. In this case, the ``AoR`` plot is only considering the output of 
a reservoir component (not a wellbore or aquifer component).

When displaying pressure outputs, the ``SalsaContourPlot`` plot type can also take critical pressure input 
and highlight areas where the critical pressure is exceeded. This analysis is also meant to inform ``AoR`` 
delineation, although with output from the ``SALSA`` component (which does not work with the ``AoR`` plot type).

Critical pressure can be used in the leakage calculations of the ``OpenWellbore`` component. When using a 
critical pressure, the ``OpenWellbore`` component will calculate leakage rates based on the difference between 
the current pressure and the critical pressure. Pressures beneath the critical pressure would generally result 
in no leakage, but leakage can still occur if |CO2| is present at the base of the wellbore (i.e., due to 
buoyancy effects; :cite:`BrownEtAl2023`).

Critical pressure (|Pcrit|) can be calculated as :cite:`USEPA2013`:

    |Pcrit| = |P_aq| + (|rho_br| |times| g |times| (|d_res| - |d_aq|)),

where |P_aq| is the initial fluid pressure in the overlying aquifer (USDW), |rho_br| is the brine density 
in the reservoir, g is gravitational acceleration (9.81 |m/s^2|), |d_aq| is the depth to the bottom of the 
aquifer impacted by leakage (|m|), and |d_res| is the depth to the top of the reservoir (|m|). Here, depths 
are taken as positive values, starting from 0 |m| at the surface and increasing into the subsurface. This 
calculated critical pressure is the pressure required to drive fluid up an open conduit, from the reservoir 
and into the aquifer being considered.

If the overlying aquifer is assumed to have a hydrostatic pressure, then |Pcrit| can be calculated as:

    |Pcrit| = (|rho_w| |times| g |times| |d_aq|) + (|rho_br| |times| g |times| (|d_res| - |d_aq|)),

where |rho_w| is the density of water (1000 |kg/m^3|). If a critical pressure is calculated automatically in 
an ``AoR`` plot, a ``SalsaContourPlot``, or for an ``OpenWellbore`` component, then this approach is used.

In those cases, the user can instead choose to provide a specific critical pressure value. For example, |Pcrit| 
could be calculated for a different value of |P_aq|. The aquifer may be under-pressured or over-pressured 
relative to a hydrostatic curve, due to factors like variations in fluid density within and above the aquifer. 
A higher pressure in the aquifer would cause a higher critical pressure, potentially reducing the size of the 
``AoR``.

In an ``AoR`` plot evaluating reservoir pressure, |d_aq| reflects the aquifer being  considered in the ``AoR`` 
analysis. For an ``OpenWellbore`` component using a critical pressure, |d_aq| should reflect the aquifer receiving 
leakage from the wellbore (i.e., |d_aq| is equal to the **wellTop** parameter) and |rho_br| is set by the 
**brineDensity** parameter.

If an ``AoR`` plot is made for a simulation using a wellbore component that has a brine density parameter 
(e.g., **brineDensity**  parameter of the ``OpenWellbore``, ``MultisegmentedWellbore``, or ``MultisegmentedWellboreAI`` 
components), then that parameter will be used for the |rho_br| value. If the wellbore component does not have a brine 
density parameter (e.g., ``CementedWellbore``), then a brine density can be specified with the ``BrineDensity`` 
entry for the ``AoR`` plot type. This entry can also be used with the ``AoR`` workflow. Otherwise, a default 
brine density value of 1045 |kg/m^3| will be used.

Hydraulic Head
--------------

Hydraulic head is a metric used to represent the energy state of groundwater. This metric is used as both an input and 
output of the ``SALSA`` component. Hydraulic head can be calculated as :cite:`PostVonAsmuth2013`:

    h = |h_e| + |h_p|

where h is hydraulic head (|m|), |h_e| is elevation head (|m|), and |h_p| is pressure head (|m|). Elevation head at a point 
is the height of that point measured relative to a datum (a reference depth). Pressure head can be calculated from the pressure 
of the water, allowing hydraulic head to be calculated as:

    h = |h_e| + (P / (|rho_w| |times| g))

where P is pressure (|Pa|), |rho_w| is fluid density (|kg/m^3|), and g is gravitational acceleration (9.81 |m/s^2|). Elevation 
head and pressure head can trade off, with higher points having higher |h_e| but lower |h_p| and lower points having lower 
|h_e| but higher |h_p|. For example, if all aquifers have equal hydraulic head values, then pressure is increasing hydrostatically 
across the units. If one aquifer has a higher hydraulic head value than the other aquifers, then its pressure is greater than the 
hydrostatic pressure expected for that depth.

With the ``SALSA`` component, the user can specify the initial hydraulic head of each aquifer. Because hydraulic head reflects 
pressure, this input also reflects the initial pressure of the aquifer. When calculating pressure head at a specific point, the 
``SALSA`` component does not use the fluid density at just that point. Instead, it uses the depth-averaged fluid density above that point, 
from that specific depth to the top boundary of the system (or to the water table depth, if the **waterTableDepth** parameter is 
used). This approach is used because the pressure at a specific point reflects the weight of the column of fluids above the point; 
using just the local fluid density would not reflect the entire column of fluid. It is important to take a depth-averaged density; 
for example, one aquifer may have a very high fluid density (suggesting higher salinity), but the impact of that aquifer on the 
pressures of aquifers beneath it would be lessened if the aquifer is very thin. In ``SALSA``, the user can specify the density 
within each aquifer. The density within each shale is then taken as the distance-weighted average of the aquifer fluid densities around 
the shale. If a shale is just beneath the top boundary of the stratigraphy, so there is no aquifer above it, the top boundary is treated 
as having a fluid density of 1000 |kg/m^3|. Each point within the shale is then given a density calculated with this fixed value, the 
fluid density of the aquifer beneath the shale, and the point's depth within the shale. If a shale is just above the bottom boundary of 
the stratigraphy, so there is no aquifer beneath it, then points within the shale are assigned the fluid density of the aquifer above it.

When considering the initial hydraulic head of each aquifer, the user should also consider the **topBoundaryPressure** parameter, which 
specifies the pressure at the top boundary of the system. The pressures used to calculate the initial hydraulic head values for the aquifers 
should be consistent with the pressure given for the top boundary of the system.

By default, ``SALSA`` calculates elevation head as the height of each point above the bottom boundary. The user can, however, manually set 
the depth of the datum with the **specifyDatum** and **datumDepth** parameters.

In addition to the initial hydraulic head of each aquifer, the user can also set the hydraulic head at the top and lower boundaries of the 
system. ``SALSA`` will only use these inputs, however, if the boundaries are fixed-head boundaries rather than no-flow boundaries (specified 
with the **topBoundaryCond** and **bottomBoundaryCond** parameters).

# -*- coding: utf-8 -*-
import os
import logging
import numpy as np
import matplotlib.pyplot as plt

RC_FONT = {'family': 'Arial', 'weight': 'normal', 'size': None}

MARKER_STYLES = {'exclude_xy_edges': 'o', 'thin_point_density': 's'}


def file_locations_plot(cmpnt_name, loc_data, x_vals_orig, y_vals_orig, z_vals_orig, 
                        x_vals_clip, y_vals_clip, z_vals_clip, out_dir, 
                        clip_type='thin_point_density', x_vals_clip_xy_edges=None, 
                        y_vals_clip_xy_edges=None, z_vals_clip_xy_edges=None, 
                        x_vals_thin=None, y_vals_thin=None, z_vals_thin=None, 
                        genfontsize=10, axisfontsize=12, titlefontsize=12, 
                        lgndfontsize=8, fontweight='bold', figsize=(10, 8), 
                        dpi=100):
    """
    Creates and saves figures showing how locations from the file_locations() function 
    (in src/openiam/cf_interface/locations.py) have been manipulated. The possible 
    manipulation types are removing all of the points along the x-y edges of the 
    domain(clip_type='exclude_xy_edges') or thinning the point density with minimum 
    x and y spacings (clip_type='thin_point_density'). The different approaches 
    for clipping the points from a file are meant to help prevent errors related to 
    domain restrictions. For example, using locations at the edge of the domain 
    (along the mapview, x-y edges or along the xtop and bottom z edges) might 
    result in errors during the interpolation of results by a LookupTableReservoir 
    or LookupTableStratigraphy component. Otherwise, some applications do not require 
    extremely high point densities (e.g., the area_of_review() plot) and using an 
    unnecessarily high point density can dramatically increase simulation times. 
    Currently, 'exclude_xy_edges' and 'thin_point_density' are the only accepted 
    clipping types. If more clipping types are added in the future, the x, y, 
    and z inputs for the other clipping types (x_vals_clip_xy_edges, y_vals_clip_xy_edges, 
    z_vals_clip_xy_edges, x_vals_thin, y_vals_thin, and z_vals_thin) are meant 
    to be used to plot the impact of previous clipping steps in addition to the 
    last performed clipping (x_vals_clip, y_vals_clip, z_vals_clip).
    """
    read_z_values = loc_data.get('read_z_values', False)
    
    RC_FONT['size'] = genfontsize
    font = RC_FONT
    plt.rc('font', **font)
    
    tile_partial = '{} Points Before and After\n'.format(cmpnt_name)
    
    continue_check = True
    if clip_type == 'exclude_xy_edges':
        current_label = '{} After x-y Edge Clipping'
        title = tile_partial + ' x-y Edge Clipping'
        fig_name_addition = 'xy_Edge_Clipping'
        
    elif clip_type == 'thin_point_density':
        current_label = '{} After Point Density Thinning'
        title = tile_partial + ' Point Density Thinning'
        fig_name_addition = 'Point_Density_Thinning'
        
    else:
        err_msg = ''.join([
            'The function file_locations_plot() was given an invalid clip_type ', 
            'value. This function is used when wellbore locations are given ', 
            'with a file and a clipping option (clip_type) is specified. The ', 
            'excepted clip_type values are ''exclude_xy_edges'' or ''thin_point_density''. ', 
            'The clip_type value given was {}. The file_locations_plot() /', 
            'figure for the {} clipping will not be made']).format(
                clip_type, clip_type)
        
        logging.error(err_msg)
        continue_check = False
    
    try:
        # These inputs need to be arrays
        if not isinstance(x_vals_orig, np.ndarray):
            x_vals_orig = np.array(x_vals_orig)
        
        if not isinstance(y_vals_orig, np.ndarray):
            y_vals_orig = np.array(y_vals_orig)
        
        if not isinstance(x_vals_clip, np.ndarray):
            x_vals_clip = np.array(x_vals_clip)
        
        if not isinstance(y_vals_clip, np.ndarray):
            y_vals_clip = np.array(y_vals_clip)
        
        if (x_vals_clip_xy_edges is not None and y_vals_clip_xy_edges is not None):
            if not isinstance(x_vals_clip_xy_edges, np.ndarray):
                x_vals_clip_xy_edges = np.array(x_vals_clip_xy_edges)
            
            if not isinstance(y_vals_clip_xy_edges, np.ndarray):
                y_vals_clip_xy_edges = np.array(y_vals_clip_xy_edges)
        
        if (x_vals_thin is not None and y_vals_thin is not None):
            if not isinstance(x_vals_thin, np.ndarray):
                x_vals_thin = np.array(x_vals_thin)
            
            if not isinstance(y_vals_thin, np.ndarray):
                y_vals_thin = np.array(y_vals_thin)
        
        if read_z_values:
            if not isinstance(z_vals_orig, np.ndarray):
                z_vals_orig = np.array(z_vals_orig)
            
            if not isinstance(z_vals_clip, np.ndarray):
                z_vals_clip = np.array(z_vals_clip)
            
            if z_vals_clip_xy_edges is not None:
                if not isinstance(z_vals_clip_xy_edges, np.ndarray):
                    z_vals_clip_xy_edges = np.array(z_vals_clip_xy_edges)
            
            if z_vals_thin is not None:
                if not isinstance(z_vals_thin, np.ndarray):
                    z_vals_thin = np.array(z_vals_thin)
    except:
        err_msg = ''.join([
            'The x, y, and z values provided to the function file_locations_plot() ', 
            'could not be converted into numpy arrays. This function is used ', 
            'when wellbore locations are given with a file and a clipping ', 
            'option (clip_type) is specified. For this figure, the clipping ', 
            'type was given as {}. Because the x, y, and z inputs ', 
            'could not be converted into arrays, these inputs were likely provided ', 
            'as invalid values and/or inappropriate formats. The file_locations_plot() ', 
            'figure using {} clipping will not be made.']).format(
                clip_type, clip_type)
        
        logging.error(err_msg)
        continue_check = False
    
    if continue_check:
        current_marker = MARKER_STYLES[clip_type]
        current_label = current_label.format(len(x_vals_clip))
        
        orig_color = [0.75, 0.75, 0.75]
        orig_linewidth = 0.25
        
        # These is used for the x-y edge clipped points - but only if the 
        # x_vals_thin and y_vals_thin are given (instead of the default None values).
        clip_color1 = [0.5, 0.5, 0.5]
        clip_color2 = [0.25, 0.25, 0.25]
        
        linewidth1 = 1
        linewidth2 = 5
        current_clip_linewidth = 10
        
        # These are adjusted if the x_vals_clip_xy_edges and x_vals_thin are 
        # not None, so that those data sets are more easily distinguished.
        thin_color = clip_color1
        thin_linewidth = linewidth1
        
        plt.figure(1, figsize=figsize, dpi=dpi)
        
        plt.plot(x_vals_orig / 1000, y_vals_orig / 1000, marker='^', linestyle='none', 
                 color=orig_color, markerfacecolor='none', linewidth=orig_linewidth, 
                 zorder=1, label='{} Original'.format(len(x_vals_orig)))
        
        if (x_vals_clip_xy_edges is not None and y_vals_clip_xy_edges is not None 
            and clip_type != 'exclude_xy_edges'):
            if (x_vals_thin is not None and y_vals_thin is not None):
                xy_clip_color = clip_color1
                xy_clip_linewidth = linewidth1
                thin_color = clip_color2
                thin_linewidth = linewidth2
            else:
                xy_clip_color = clip_color2
                xy_clip_linewidth = linewidth2
            
            plt.plot(x_vals_clip_xy_edges / 1000, y_vals_clip_xy_edges / 1000, 
                     marker='o', linestyle='none', color=xy_clip_color, markerfacecolor='none', 
                     linewidth=xy_clip_linewidth, zorder=2, label='{} After x-y Edge Clipping'.format(
                         len(x_vals_clip_xy_edges)))
        
        if (x_vals_thin is not None and y_vals_thin is not None 
            and clip_type != 'thin_point_density'):
            plt.plot(x_vals_thin / 1000, y_vals_thin / 1000, marker='s', 
                     linestyle='none', color=thin_color, markerfacecolor='none', 
                     linewidth=thin_linewidth, zorder=3, label='{} After Thinning Point Density'.format(
                         len(x_vals_thin)))
        
        plt.plot(x_vals_clip / 1000, y_vals_clip / 1000, marker=current_marker, 
                 linestyle='none', markerfacecolor='none', color='r', zorder=4, 
                 label=current_label, linewidth=current_clip_linewidth)

        plt.xlabel('Easting (km)', fontsize=axisfontsize, fontweight='bold')
        plt.ylabel('Northing (km)', fontsize=axisfontsize, fontweight='bold')
        plt.title(title, fontsize=titlefontsize, fontweight='bold')
        plt.legend(fancybox=False, fontsize=lgndfontsize, edgecolor=[0, 0, 0], 
                   loc='best', framealpha=0.75)
        
        fig_name = '{}_{}_Mapview.png'.format(cmpnt_name, fig_name_addition)
        
        plt.savefig(os.path.join(out_dir, fig_name), dpi=dpi)
        plt.close()
        
        # Reset these, in case some of the z values were not provided
        thin_color = clip_color1
        linewidth2 = 1
        
        if read_z_values:
            plt.figure(2, figsize=figsize, dpi=dpi)
            
            plt.plot(x_vals_orig / 1000, z_vals_orig, marker='^', linestyle='none', 
                     color=orig_color, markerfacecolor='none', linewidth=orig_linewidth, 
                     zorder=1, label='{} Original'.format(len(x_vals_orig)))
            
            if (x_vals_clip_xy_edges is not None and z_vals_clip_xy_edges is not None 
                and clip_type != 'exclude_xy_edges'):
                if (x_vals_thin is not None and z_vals_thin is not None):
                    xy_clip_color = clip_color1
                    xy_clip_linewidth = linewidth1
                    thin_color = clip_color2
                    thin_linewidth = linewidth2
                else:
                    xy_clip_color = clip_color2
                    xy_clip_linewidth = linewidth2
                
                plt.plot(x_vals_clip_xy_edges / 1000, z_vals_clip_xy_edges, 
                         marker='o', linestyle='none', color=xy_clip_color, markerfacecolor='none', 
                         linewidth=xy_clip_linewidth, zorder=2, label='{} After x-y Edge Clipping'.format(
                             len(x_vals_clip_xy_edges)))
            
            if (x_vals_thin is not None and z_vals_thin is not None 
                and clip_type != 'thin_point_density'):
                plt.plot(x_vals_thin / 1000, z_vals_thin, 
                         marker='s', linestyle='none', color=thin_color, markerfacecolor='none', 
                         linewidth=thin_linewidth, zorder=3, label='{} After Thinning Point Density'.format(
                             len(x_vals_thin)))
            
            plt.plot(x_vals_clip / 1000, z_vals_clip, marker=current_marker, 
                     linestyle='none', markerfacecolor='none', color='r', zorder=4, 
                     label=current_label, linewidth=current_clip_linewidth)

            plt.xlabel('Easting (km)', fontsize=axisfontsize, fontweight='bold')
            plt.ylabel('Depth (m)', fontsize=axisfontsize, fontweight='bold')
            plt.title(title, fontsize=titlefontsize, fontweight='bold')
            plt.legend(fancybox=False, fontsize=lgndfontsize, edgecolor=[0, 0, 0], 
                       loc='best', framealpha=0.75)
            
            fig_name = '{}_{}_Easting_vs_Depth.png'.format(cmpnt_name, fig_name_addition)
            
            plt.savefig(os.path.join(out_dir, fig_name), dpi=dpi)
            plt.close()
            
            plt.figure(3, figsize=figsize, dpi=dpi)
            
            plt.plot(y_vals_orig / 1000, z_vals_orig, marker='^', linestyle='none', 
                     color=orig_color, markerfacecolor='none', linewidth=orig_linewidth, 
                     zorder=1, label='{} Original'.format(len(x_vals_orig)))
            
            if (y_vals_clip_xy_edges is not None and z_vals_clip_xy_edges is not None 
                and clip_type != 'exclude_xy_edges'):
                if (y_vals_thin is not None and z_vals_thin is not None):
                    xy_clip_color = clip_color1
                    xy_clip_linewidth = linewidth1
                    thin_color = clip_color2
                    thin_linewidth = linewidth2
                else:
                    xy_clip_color = clip_color2
                    xy_clip_linewidth = linewidth2
                
                plt.plot(y_vals_clip_xy_edges / 1000, z_vals_clip_xy_edges, 
                         marker='o', linestyle='none', color=xy_clip_color, markerfacecolor='none', 
                         linewidth=xy_clip_linewidth, zorder=2, label='{} After x-y Edge Clipping'.format(
                             len(x_vals_clip_xy_edges)))
            
            if (y_vals_thin is not None and z_vals_thin is not None 
                and clip_type != 'thin_point_density'):
                plt.plot(y_vals_thin / 1000, z_vals_thin, 
                         marker='s', linestyle='none', color=thin_color, markerfacecolor='none', 
                         linewidth=linewidth2, zorder=3, label='{} After Thinning Point Density'.format(
                             len(y_vals_thin)))
            
            plt.plot(y_vals_clip / 1000, z_vals_clip, marker=current_marker, 
                     linestyle='none', markerfacecolor='none', color='r', zorder=4, 
                     label=current_label, linewidth=current_clip_linewidth)

            plt.xlabel('Northing (km)', fontsize=axisfontsize, fontweight='bold')
            plt.ylabel('Depth (m)', fontsize=axisfontsize, fontweight='bold')
            plt.title(title, fontsize=titlefontsize, fontweight='bold')
            plt.legend(fancybox=False, fontsize=lgndfontsize, edgecolor=[0, 0, 0], 
                       loc='best', framealpha=0.75)
            
            fig_name = '{}_{}_Northing_vs_Depth.png'.format(cmpnt_name, fig_name_addition)
            
            plt.savefig(os.path.join(out_dir, fig_name), dpi=dpi)
            plt.close()

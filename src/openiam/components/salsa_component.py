# -*- coding: utf-8 -*-
import logging
import sys
import os
import numpy as np

try:
    import openiam.components.iam_base_classes as iam_bc
except ImportError as err:
    print('Unable to load NRAP-Open-IAM base classes module: {}'.format(err))

try:
    from openiam.components.models.wellbore.salsa import salsa_ROM
except ImportError:
    print('\nERROR: Unable to load ROM for SALSA component\n')
    sys.exit()

from openiam.cf_interface.commons import get_parameter_val

DEFAULT_PAR_WARNING = ''.join([
    'The parameter {} of the component {} was not assigned. It will be assigned ',
    'the default value of {}.'])

REQUIRED_PAR_ERROR = ''.join([
    'The parameter {} of the component {} was not assigned. This parameter is ',
    'required{}, so the simulation cannot proceed. Check your input.'])

KW_ARG_CALC_LEN_MSG = ''.join([
    'The number of locations given with the {} keyword argument for the SALSA ',
    'component {} could not be calculated. This keyword argument was likely ',
    'given with an incorrect format or value(s). The given input was {}. This ',
    'setup may result in errors.'])

KW_ARG_TYPE_WARNING = ''.join([
    'The keyword argument {} of the SALSA component {} was given as {}. It {}. ',
    'This keyword argument {}. Check your input.'])

KW_ARG_LEN_MSG = ''.join([
    'For the component {}, the keyword arguments {} and {} were not given as ',
    'lists or arrays of the same length. The argument {} was given as {}, while ',
    '{} was given as {}. The simulation cannot proceed. Check your input.'])

FILE_WARNING_MSG = ''.join([
    'During the connect_with_system() method of the SALSA component {}, the ',
    'keyword argument {} was found in the component data provided for the ',
    'component. The input ({}) was a string, but it was not a valid file path. ',
    'Check your input.'])

KW_ARG_NOT_FOUND_ERR_MSG = ''.join([
    'During the connect_with_system() method of the SALSA component {}, the ',
    '{}{} was not found in the component data provided for the ',
    'component. The lack of this input may result in errors. Check your input.'])

KW_ARG_NOT_FOUND_WARNING_MSG = ''.join([
    'During the connect_with_system() method of the SALSA component {}, the ',
    '{}{} was not found in the component data provided for the ',
    'component. The default approach will be used for the corresponding input.'])

KW_ARG_NONE_MSG = ''.join([
    'During the connect_with_system() method of the SALSA component {}, the ',
    '{} was provided as None. The lack of valid input may result in errors. ',
    'Check your input.'])

KW_ARG_ARRAY_MSG = ''.join([
    '{} the SALSA component {}, the keyword argument {} was given as {}. This ',
    'input could not be converted into a numpy array, which is the required ',
    'format for the keyword argument. This situation may result in errors. ',
    'Check your input.'])

KW_ARG_NOT_GIVEN_COORDS_MSG = ''.join([
    'The SALSA component {} was not given {} coordinates, {}. These ',
    'keyword arguments are required, and given under the {} entry in the ',
    'control file interface. Check your input.'])

PAR_NOT_RECOGNIZED_MSG = ''.join([
    'The parameter {} was given to the SALSA component {} with a value of {}, ',
    'but it was not recognized as a valid parameter type. Parameter names are ',
    'case sensitive, so check the exact spelling used. The input will not be used.'])

STRAT_PAR_WARNING_MSG = "".join([
    "The parameter {} was given as a stochastic parameter for the SALSA ",
    "component {}. This parameter is related to the domain's stratigraphy, ",
    "and it cannot be stochastic in the control file interface. Only ",
    "the value key will be used, if it was provided (not the min or max keys)."])

PAR_UNRECOGNIZED_MSG = ''.join([
    'There was an error while attempting to assign the parameter {} provided ',
    'to the SALSA component {}. The value given for this parameter was {}, ',
    'but this input will not be used. The parameter name may have been ',
    'misspelled or formatted incorrectly.'])

BOUNDARY_COND_CHANGE_MSG = ''.join([
    'For the SALSA component {}, the {}BoundaryCond parameter was given as 0 ', 
    '(fixed-head boundary), but the {}LayerType parameter was given as 1 ', 
    '(aquifer). When the {}LayerType is 1, the {} boundary automatically becomes ', 
    'a no-flow boundary. With the SALSA component, only horizontal flow occurs ', 
    'in aquifers (vertical flow occurs in aquitards). The {}BoundaryCond ', 
    'parameter will be set to 1 (no-flow boundary).'])

# List of all of the parameters with unchanging names (i.e., not formatted for
# specific units, wells, or injection periods)
FIXED_PAR_NAMES = [
    'numberOfShaleLayers', 'bottomLayerType', 'topLayerType', 'numberOfAquiferLayers',
    'bottomBoundaryCond', 'bottomBoundaryHead', 'topBoundaryCond', 'topBoundaryHead',
    'numberOfLaplaceTerms', 'flagMesh', 'xCent', 'yCent', 'xMax', 'yMax',
    'gridExp', 'numberOfNodesXDir', 'numberOfNodesYDir', 'numberOfRadialGrids',
    'radialZoneRadius', 'radialGridExp', 'numberOfVerticalPoints', 
    'topBoundaryPressure', 'topBoundaryFluidDensity', 'waterTableDepth', 
    'specifyDatum', 'datumDepth']

# List of all keyword arguments except for output_dir, used to process the arguments
KW_ARGS = [
    'periodEndTimes', 'activeWellCoordx', 'activeWellCoordy',
    'leakingWellCoordx', 'leakingWellCoordy', 'aquiferCoordx', 'aquiferCoordy',
    'shaleCoordx', 'shaleCoordy', 'aquiferNamesContourPlots']

REQUIRED_KW_ARGS = ['leakingWellCoordx', 'leakingWellCoordy',
                    'aquiferCoordx', 'aquiferCoordy',
                    'shaleCoordx', 'shaleCoordy']

DEFAULT_KW_ARGS = {'activeWellCoordx': 0, 'activeWellCoordy': 0}

# These are keyword arguments that are not currently included for the SALSA
# component. Future versions may introduce these arguments as user input.
NOT_INCLUDED_KW_ARGS = ['numberOfContourPlotNodes', 'contourPlotCoordx',
                        'contourPlotCoordy']

# Seconds per day
SEC_PER_DAY = 60 * 60 * 24

# Assumed gravitational acceleration for pressure calculation, m/(s^2)
GRAV_ACCEL = 9.81

SALSA_GRID_OBSERVATIONS = [
    'profileDepths', 'allHeadProfilesShales', 'allPressureProfilesShales', 
    'contourPlotAquiferHead', 'contourPlotCoordx', 'contourPlotCoordy', 
    'contourPlotAquiferPressure', 'contourPlotMidAquiferPressure', 
    'contourPlotTopAquiferPressure']

# In the current version of the SALSA component, aquifer#ANSR must be the same
# in all aquifers because the aquitards are permeable.
ENFORCE_UNIFORM_AQUIFER_ANSR = True


class SALSA(iam_bc.ComponentModel):
    """
    The SALSA (Semi-Analytical Leakage Solutions for Aquifers) component calculates
    transient hydraulic head changes, volumetric leakage rates, and cumulative
    leakage volumes through aquitards and leaking wells due to injection or
    extraction operations. Multiple sites can be used for injection and extraction,
    and injection and extraction rates can vary over time. Each unit and leaking
    well can be assigned a hydraulic conductivity value. This component uses
    the SALSA model developed by :cite:`Cihan2023`. For more details regarding
    the theoretical framework of the SALSA model, see :cite:`CihanEtAl2011`,
    :cite:`CihanEtAl2014`, and :cite:`CihanEtAl2022`.

    SALSA deals with two types of flow: "focused flow" and "diffuse flow".
    Focused flow portrays horizontal flow from leaking wells and into aquifers,
    while diffuse flow portrays vertical flow through aquifer-aquitard interfaces.
    Diffuse leakage results are calculated for the bottom and top aquifer-aquitard
    interface of each aquifer. Because the domain has an infinite horizontal
    extent in SALSA, there is no limit for the areas through which diffuse
    leakage can occur along aquifer-aquitard interfaces. As a result, diffuse
    leakage volumes can be quite high. To examine the potential impacts of
    injection operations, however, the user is encouraged to focus on well leakage.

    If a particular aquifer does not have an aquitard above or below it (for
    top and bottom interfaces, respectively), then it cannot have diffuse
    leakage outputs produced for that side. For example, an aquifer can be the
    bottom unit (if **bottomLayerType** is 1), and that aquifer would not have
    a bottom aquifer-aquitard interface. An aquifer can be the top unit (if
    **topLayerType** is 1), and that aquifer would not have a top aquifer-aquitard
    interface.

    Negative well leakage rates mean that the leakage is going from the aquifer
    and into the well - this situation can occur for an aquifer that is receiving
    injection. Negative diffuse leakage rates mean that the flow is exiting the
    corresponding aquifer rather than entering it across that particular
    aquifer-aquitard interface (bottom or top).

    SALSA can handle different initial conditions for the units in the system.
    Each aquifer can be assigned an initial hydraulic head; because the initial 
    hydraulic head reflects the initial pressure of the aquifer, SALSA allows 
    the user to evaluate injection and leakage across initially overpressured or 
    underpressured units. For more information about hydraulic head, see section 
    :ref:`equations`. Furthermore, both aquifers and aquitards can be assigned 
    a pressurization rate. For more details regarding the parameters for 
    pressurization rates (**shale#InitialGamma**, **shale#Gamma**, and **aquifer#Gamma**), 
    see :cite:`CihanEtAl2022`. Recommended pressurization rates for overpressured
    units range from -1.0e-13 |1/s| to -1.0e-15 |1/s| (negative values).
    Recommended pressurization rates for underpressured units range from
    1.0e-15 |1/s| to 1.0e-13 |1/s| (positive values). If pressurization rates 
    are zero and the simulation runs for a long time after injection stops 
    (potentially thousands of years), then pressures will return to hydrostatic 
    conditions. Otherwise, positive and negative pressurization rates will cause 
    pressures to gradually decrease or increase over time, respectively.
    
    Because the pressure at the top boundary can be set with the parameter 
    **topBoundaryPressure**, the SALSA component may be suitable for offshore 
    geologic carbon storage site applications (i.e., elevating the pressure at 
    the top boundary to account for the column of seawater).

    SALSA uses a single-phase treatment of injection, extraction, and leakage.
    Therefore, in applications for geologic carbon storage, SALSA can only be
    used to simulate hydraulic head changes and brine leakage in regions
    horizontally beyond the |CO2| plume zone (:cite:`CihanEtAl2013`). The
    representation of |CO2| injection (rather than brine injection) requires
    the user to provide volumetric injection rates as equivalent to the volumes
    of |CO2| being injected. For example, if the mass rate of |CO2| is Q_CO2
    (in |kg/s|) and the density of |CO2| is RHO_CO2 (in |kg/m^3|), then the
    equivalent volumetric injection rate for SALSA can be calculated as
    Q_CO2 / RHO_CO2. |CO2| fluid properties, such as density, can change with 
    depth and as pressure increases due to injection (:cite:`CihanEtAl2013`). 
    The user needs to externally calculate or estimate the average density of 
    |CO2| at a given injection depth, expected pressure, and expected temperature 
    during injection. For more details about such aspects of using a single-phase 
    treatment of injection, see :cite:`Nicot2008` and :cite:`CihanEtAl2011`.

    The SALSA component does not connect with a separate stratigraphy component
    in NRAP-Open-IAM. For SALSA, the stratigraphy used is dictated by the parameters
    **bottomLayerType**, **topLayerType**, and **numberOfShaleLayers**,
    **shale#Thickness**, and **aquifer#Thickness**. The number of aquifer layers
    is calculated from the parameters *bottomLayerType**, **topLayerType**, and
    **numberOfShaleLayers**. The unit numbers begin from the bottom and increase
    towards the surface, with aquifer 1 and shale 1 being the deepest units of
    their respective types. Additionally, the reservoirs receiving injection are
    included in the aquifer layers; the reservoirs are not a separate unit type.

    To conform with the practices of other components in NRAP-Open-IAM, the term
    "shale" is used to refer to the aquitards portrayed in SALSA. Although the
    term shale is used, the aquitards do not have to be shale units, specifically.
    In SALSA, injection and extraction wells are referred to as "active wells."
    The inputs provided for these active wells dictate whether an aquifer is
    considered a storage reservoir. Specifically, each aquifer is assigned
    injection or extraction rates over time with the parameter
    **activeWell#QAquifer#Period#**; if an aquifer is given injection rates
    (a positive **activeWell#QAquifer#Period#** value) for any injection period,
    then the aquifer is considered a reservoir. Wells that serve as leakage
    pathways are called "leaking wells".

    Several parameters of the SALSA component can be formatted to match a specific
    active well, leaking well, aquifer, shale, or injection period (e.g.,
    **shale3LogVertK** for shale 3 or **aquifer1LogHorizK** for aquifer 1). Some
    parameter names include multiple indices that can be formatted to match a
    specific active well, leaking well, aquifer, shale, or injection period. For
    example, the parameter **activeWell3QAquifer4Period12** applies to active
    well 3, aquifer 4, and injection period 12. Alternatively, the word 'All'
    can be used instead of a specific well, shale, or aquifer number. In that
    case, the parameter value provided will apply to all features of that type.
    This approach works in both the control file interface and script applications.
    For example, the parameter name **aquiferAllFluidDensity** applies to all
    aquifers while the parameter name **leakingWell10RadiusAquiferAll** applies
    to leaking well 10 across all aquifers. Any parameter value that is given
    for a specific well, aquifer, shale, or period will overwrite a value given
    for all features of that type. For example, if the user provides both
    **aquiferAllFluidDensity** and **aquifer1FluidDensity**, the value given
    with **aquifer1FluidDensity** will apply to aquifer 1 while the value given
    with **aquiferAllFluidDensity** will apply to all other aquifers. The user
    user should be careful about using the 'All' option in multiple parameter
    names that could lead to overlap. For example, the parameter names
    **leakingWellAllStatAquifer2** applies to all leaking wells across aquifer 2,
    while the parameter name **leakingWell10StatAquiferAll** applies to leaking
    well 10 across all aquifers. This overlap in parameter applicability must be
    avoided, as the parameter assignment may not work as the user intends. The
    option to use 'All' for parameter names is meant to allow the user to treat
    all features of one type, and then use a more specific parameter name if a
    particular feature is meant to receive a different value. For example, one
    could use **leakingWellAllRadiusAquiferAll** to set the radii for all leaking
    wells across all aquifers, and then use **leakingWell2RadiusAquifer2** to
    set the well radius specifically for leaking well 2 across aquifer 2.
    If a parameter for a particular active well, leaking well, aquifer, shale,
    or injection period is not assigned a value, it will be assigned the default
    value for that parameter type.

    In the NRAP-Open-IAM control file interface, the type name for the SALSA
    component is ``SALSA``. The parameters of the SALSA component are described
    below.

    * **bottomLayerType** [-] (0 or 1) - specifies whether the bottom layer is
      an aquifer (1) or a shale (0). The default setting is 1.

    * **topLayerType** [-] (0 or 1) - specifies whether the top layer is
      an aquifer (1) or a shale (0). The default setting is 0.

    * **numberOfShaleLayers** [-] (3 to 30) - number of shale layers (aquitards)
      in the system (default: 3). The shale units must be separated by aquifer
      layers. The number of aquifer layers is calculated as
      (**numberOfShaleLayers** + **bottomLayerType** + **topLayerType** - 1).

    * **shale#Thickness** [|m|] (1 to 2000) - thickness of shale ``#`` (default: 250).
      Here, ``#`` varies from 1 to **numberOfShaleLayers**.

    * **aquifer#Thickness** [|m|] (1 to 2000) - thickness of the aquifer with the index
      ``#`` (default: 100). Here, ``#`` varies from 1 to the number of aquifer layers.

    * **aquifer#Head** [-] (-1000 to 1000) - initial hydraulic head in aquifer
      ``#`` (default: 0) Here, ``#`` varies from 1 to the number of aquifer layers. 
      Because hydraulic head reflects the combination of elevation head and 
      pressure head, the initial hydraulic head reflects the initial pressure 
      of the aquifer. For more information about hydraulic head, refer to section 
      :ref:`equations`.

    * **shale#LogVertK** [|log10| |m/s|] (-16 to -4) - vertical hydraulic conductivity
      of shale ``#``.  Here, ``#`` varies from 1 to **numberOfShaleLayers**.
      The default value is -11 |log10| |m/s| (equivalent to 8.64e-7 m/day).

    * **aquifer#LogHorizK** [|log10| |m/s|] (-13 to -3) - horizontal conductivity
      of aquifer ``#``. Here, ``#`` varies from 1 to the number of aquifer layers.
      The default value is -5 |log10| |m/s| (equivalent to 0.864 m/day).

    * **aquifer#ANSR** [-] (1.0e-3, 1.0e+3) - anisotropy ratio for aquifer ``#``.
      Here, ``#`` varies from 1 to the number of aquifer layers. The ratio is the
      hydraulic conductivity in the x-direction divided by the hydraulic
      conductivity in the y direction (default: 1.0). In the current version of
      the SALSA component, all aquifers must have the same **aquifer#ANSR**
      value. If aquifers are given different values, they will all be assigned
      the default value of 1.

    * **shale#LogSS** [|log10| |1/m|] (-12.0 to 0.0) - storativity of shale ``#``.
      (default: -6). Here, ``#`` varies from 1 to **numberOfShaleLayers**.

    * **aquifer#LogSS** [|log10| |1/m|] (-12.0 to 0.0) - storativity of aquifer
      ``#`` (default: -6). Here, ``#`` varies from 1 to the number of aquifer layers.

    * **aquifer#FluidDensity** [|kg/m^3|] (900 to 1500) - initial density of the
      fluid in aquifer ``#`` (default: 1000). Here, ``#`` varies from 1 to
      the number of aquifer layers.

    * **shale#InitialGamma** [|1/s|] (-1.0e-10 to 1.0e-10) - initial pressurization
      rate for shale ``#`` (default: 0). This pressurization rate is meant to
      be representative for the shale over geologic time. Values less than 0
      correspond to overpressurization, while values greater than 0 correspond
      to underpressurization. Here, ``#`` varies from 1 to the number of
      shale layers.

    * **shale#Gamma** [|1/s|] (-1.0e-10 to 1.0e-10) - ongoing pressurization
      rate for shale ``#`` (default: 0). Values less than 0 correspond to
      overpressurization, while values greater than 0 correspond to
      underpressurization. Here, ``#`` varies from 1 to the number of shale 
      layers.

    * **aquifer#Gamma** [|1/s|] (-1.0e-10 to 1.0e-10) - pressurization rate for
      aquifer ``#`` (default: 0). Values less than 0 correspond to overpressurization,
      while values greater than 0 correspond to underpressurization. Here,
      ``#`` varies from 1 to the number of aquifer layers.

    * **bottomBoundaryCond** [-] (0 or 1) - boundary conditions at the bottom
      of the stratigraphic system. If set to 0, the bottom boundary is a fixed
      head boundary. If set to 1, the bottom boundary is a no-flow boundary. The
      default value is 1. If the bottom unit is an aquifer, the bottom automatically
      becomes a no-flow boundary because SALSA only considers horizontal flow
      in aquifers.

    * **bottomBoundaryHead** [|m|] (-1000 to 1000) - if **bottomBoundaryCond** is set
      to 0 (fixed head boundary), this parameter sets the hydraulic head at the
      bottom boundary. The default value is 0 m. If **bottomBoundaryCond** is 1,
      this parameter has no effect.

    * **topBoundaryCond** [-] (0 or 1) - boundary conditions at the top of the
      stratigraphic system. If set to 0, the top boundary is a fixed head boundary.
      If set to 1, the top boundary is a no-flow boundary. The default value is
      1. If the top unit is an aquifer, the top automatically becomes a no-flow
      boundary because SALSA only considers horizontal flow in aquifers.

    * **topBoundaryHead** [|m|] (-1000 to 1000) - if **topBoundaryCond** is set
      to 0 (fixed head boundary), this parameter sets the hydraulic head at the
      top boundary. The default value is 0 m. If **topBoundaryCond** is 1, this
      parameter has no effect.

    * **activeWell#QAquifer#Period#** [|m^3/s|] (-5 to 5) - volumetric injection
      (positive) or extraction (negative) rate from active well ``#`` into aquifer
      ``#`` during injection period ``#``. The active well index (activeWell#)
      ranges from 1 to the number of coordinates provided for the ``activeWellCoordx``
      and ``activeWellCoordy`` keyword arguments. The aquifer index (Aquifer#)
      ranges from 1 to the number of aquifer layers. The period index (Period#)
      ranges from 1 to the number of injection periods. The injection periods
      are controlled with the ``periodEndTimes`` keyword argument. The default
      approach for all active wells is for the lowest aquifer (aquifer 1) to
      have an injection rate of 0.01 |m^3/s| for the first injection period
      (corresponding with parameter names of **activeWell#Aquifer1Period1**,
      where the # varies across all active well indices) and rates of 0 |m^3/s|
      for all other periods. By default, all other aquifers have rates of 0 |m^3/s|
      for across all injection periods. By providing more active well locations
      (``activeWellCoordx`` and ``activeWellCoordy``) and more injection periods
      (``periodEndTimes``), aquifers can have injection or extraction rates that
      vary over space and time. If the ``periodEndTimes`` keyword argument is
      not provided, then there is only one period that extends to the end of the
      simulation. If the volumetric extraction rates used are too high (negative 
      values with high absolute values), the pressure outputs might become negative. 
      Such results are not valid, and the simulation will log an an error messge.

    * **activeWell#RadiusAquifer#** [|m|] (0.01 to 0.5) - radius of active well
      ``#`` across aquifer ``#`` (default: 0.05). Here, the active well index
      (activeWell#) should range from 1 to the number of coordinates provided
      for the ``activeWellCoordx`` and ``activeWellCoordy`` keyword arguments.
      The aquifer index (Aquifer#) ranges from 1 to the number of aquifer
      layers.

    * **leakingWell#RadiusAquifer#** [|m|] (0.01 to 0.5) - radius of leaking well
      ``#`` across aquifer ``#`` (default: 0.05). Here, the well index should
      range from 1 to the number of leaking wells, which is set with the number
      of values provided for ``leakingWellCoordx`` and ``leakingWellCoordy``.
      The aquifer index should range from 1 to the number of aquifer layers.

    * **leakingWell#LogKAquifer#** [|log10| |m/s|] (-13 to -3) - hydraulic conductivity
      of leaking well ``#`` across aquifer ``#`` (default: -5 |log10| |m/s|,
      equivalent to 0.864 m/day). The leaking well index (Well#) should range
      from 1 to the number of leaking wells. The aquifer index (Aquifer#)
      should range from 1 to the number of aquifer layers.

    * **leakingWell#RadiusShale#** [|m|] (0.01 to 0.5) - radius of leaking well
      ``#`` across shale ``#`` (default: 0.05). Here, the well index should range
      from 1 to the number of leaking wells, which is set with the number of values
      provided for ``leakingWellCoordx`` and ``leakingWellCoordy``. The shale
      index (Shale#) should range from 1 to **numberOfShaleLayers**.

    * **leakingWell#LogKShale#** [|log10| |m/s|] (-13 to -3) - hydraulic conductivity
      of leaking well ``#`` across shale ``#`` (default: -5 |log10| |m/s|, equivalent
      to 0.864 m/day). The leaking well index (Well#) should range from 1 to
      the number of leaking wells. The shale index should range from 1 to
      **numberOfShaleLayers**.

    * **leakingWell#StatAquifer#** [-] (0 or 1) - status of leaking well ``#``
      across aquifer ``#``. The only acceptable values are 0 or 1, which correspond
      to the well being cased (0) or screened (1) across the aquifer. If the
      leaking well is screened across an aquifer, leakage can enter the aquifer.
      Here, the well index should range from 1 to the number of leaking wells,
      which is set with the number of values provided for ``leakingWellCoordx``
      and ``leakingWellCoordy``. The aquifer index should range from 1 to the
      number of aquifer layers. The default value is 1 (screened).

    * **leakingWell#StatShale#** [-] (0 or 1) - status of leaking well ``#``
      across shale ``#``. The only acceptable values are 0 or 1, which correspond
      to the well being plugged (0) or unplugged (1) across the shale. If the
      shale is plugged along a shale layer, then leakage will not pass through
      that section of the well. Here, the well index should range from 1 to the
      number of leaking wells, which is set with the number of values provided
      for ``leakingWellCoordx`` and ``leakingWellCoordy``. The shale index should
      range from 1 to **numberOfShaleLayers**, with the exception that this
      parameter cannot be given for a shale that does not have an aquifer
      situated above it. The default value is 1 (unplugged).

    * **leakingWell#QPeriod#** [|m^3/s|] (-5 to 5) - volumetric injection
      (positive) or extraction (negative) rate from leaking well ``#`` during
      injection period ``#`` (default: 0). If this parameter is set to a nonzero
      value for a leaking well, then that leaking well becomes an active well
      that also serves as a leakage pathway. If this parameter is set to zero,
      then the well behaves as a normal leaking well. The leakage from the well
      and into each aquifer is controlled by the leaking well's status (plugged
      or unplugged) and hydraulic conductivity across different units
      (**leakingWell#StatAquifer#**, **leakingWell#StatShale#**, **leakingWell#LogKAquifer#**,
      and **leakingWell#LogKShale#**). The leaking well index (leakingWell#)
      ranges from 1 to the number of coordinates provided for the ``leakingWellCoordx``
      and ``leakingWellCoordy`` keyword arguments. The period index (Period#)
      ranges from 1 to the number of injection periods. The injection periods
      are controlled with the ``periodEndTimes`` keyword argument.

    * **numberOfLaplaceTerms** [-] (5 to 100) - number of terms used in the
      Stehfest Laplace Inversion method (default: 10).

    * **xCent** [|m|] (-10,000 to 10,000) - central x-coordinate of the rectangular
      domain within which the contour plot output will be generated (default: 0 |m|).

    * **yCent** [|m|] (-10,000 to 10,000) - central y-coordinate of the rectangular
      domain within which the contour plot output will be generated (default: 0 |m|).

    * **xMax** [|m|] (500 to 100,000) - maximum extent of the contour plot's
      rectangular domain in the x-direction (default: 5000 |m|). This parameter
      should be large enough to encompass the positions of leaking wells, otherwise
      the contour plot data will have an irregular shape.

    * **yMax** [|m|] (500 to 100,000) - maximum extent of the contour plot's
      rectangular domain in the y-direction (default: 5000 |m|). This parameter
      should be large enough to encompass the positions of leaking wells, otherwise
      the contour plot data will have an irregular shape.

    * **gridExp** [-] (0.1 to 10) - grid size change ratio for the rectangular
      grid (default: 1.0). If set to 1, there will be no size change and the
      grid will be uniform.

    * **numberOfNodesXDir** [-] (3 to 500) - number of nodes in the x-direction
      for the rectangular grid, which is used for contour plot output (default: 30).
      More nodes will provide more finely resolved contour plot output, but it 
      will also increase the size of the *.npz* files generated for the contour 
      plot output.

    * **numberOfNodesYDir** [-] (3 to 500) - number of nodes in the y-direction
      for the rectuangular grid, which is used for contour plot output (default: 30).
      More nodes will provide more finely resolved contour plot output but it 
      will also increase the size of the *.npz* files generated for the contour 
      plot output.

    * **numberOfRadialGrids** [-] (2 to 24) - number of radial grids around
      each injection and leaking well (default: 6). The contour plot uses a
      rectagular grid (controlled with the **xCent**, **yCent**, **xMax**,
      **yMax**, **gridExp**, **numberOfNodesXDir**, and **numberOfNodesYDir**
      parameters), but it also superimposes more radial points around each well.
      Each 'radial grid', as referred to with **numberOfRadialGrids**, is a
      ring of 16 points around each well. There is also one point at the well's
      exact location. More points will lead to more finely resolved contour plot 
      output, but the *.npz* files generated will also be larger.

    * **radialZoneRadius** [|m|] (10 to 10,000) - radius of the radial zone
      around each injection and leaking well (default: 200). The radial zone
      for each well includes includes all of the radial grid points, where the
      points are arranged in a number of rings set by the **numberOfRadialGrids**
      parameter. The ring of points farthest from each well will be separated
      from the well by a distance of **radialZoneRadius**.

    * **radialGridExp** [|m|] (1.0 to 10.0) - rate of radial grid size increase
      away from each injection and leaking well (default: 1.2).

    * **numberOfVerticalPoints** [-] (3 to 100) - number of points used in the
      vertical head profiles across the thickness of each shale layer (default: 10).
      The x- and y-coordinates of the vertical profiles are set with the
      ``shaleCoordx`` and ``shaleCoordy`` keyword arguments. The number
      of points used influences the **headProfile#VertPoint#Shale#**,
      **allHeadProfilesShales**, and **profileDepths** outputs.
      
    * **topBoundaryPressure** [|Pa|] (8.0e+4 to 3.0e+7) - pressure at the top 
      of the system, just above the top stratigraphic layer (default: 101,325). 
      This parameter is used in pressure calculations. If the top stratigraphic 
      layer is meant to be just beneath the atmosphere, then this parameter 
      represents atmospheric pressure. If this pressure is higher, the top boundary 
      could represent a particular depth in the subsurface, with the stratigraphy 
      above that point not being represented in the model. In that case, the 
      depths used by the SALSA component would be relative to the top boundary 
      (as opposed to depth beneath the surface). Furthermore, the top boundary 
      pressure could be elevated to represent the bottom of the seafloor (i.e., 
      atmospheric pressure plus the pressure from the column of seawater). This 
      approach would allow the SALSA component to be used for offshore geologic 
      carbon storage applications. The **topBoundaryPressure** value cannot 
      change over space or time.
      
    * **topBoundaryFluidDensity** [|kg/m^3|] (900 to 1500) - density of the 
      fluid at the top boundary of the system (default: 1000). The top boundary 
      may be the land surface, the seafloor, or a particular depth in the 
      subsurface. If the top layer is a shale (**topLayerType** value of 0), 
      the fluid density in that shale is calculated with this density and that 
      of the aquifer beneath the shale.
      
    * **waterTableDepth** [|m|] (-300 to 0) - depth to the water table (default: 0), 
      relative to the top boundary. Depths beneath the top boundary are taken 
      as being negative. This parameter is used used in pressure calculations, 
      but only if the top layer is an aquifer (**topLayerType** is 1). 
      Additionally, if the top layer is an aquifer then the water table cannot 
      be deeper than the bottom of that aquifer (the value will be set to bottom 
      depth of the aquifer). If the top stratigraphic layer is not meant to be 
      just beneath the atmosphere (so that the **topBoundaryPressure** is not 
      meant to be atmospheric pressure), then this parameter should be set to 
      0 |m|.
      
    * **specifyDatum** [-] (0 or 1) - specifies whether to use the **datumDepth** 
      parameter to calculate elevation head for pressure calculations (1) or to 
      set the datum as the bottom depth of the deepest unit in the model's 
      stratigraphy (0). The default value is 0. Elevation head is calculated as 
      the height of a point above the datum, and pressure head is calculated as 
      overall hydraulic head minus the elevation head. Pressure outputs are then 
      calculated from the pressure head. Therefore, the datum used impacts how 
      the hydraulic head parameters (**aquifer#Head**, **topBoundaryHead**, and 
      **bottomBoundaryHead**) and hydraulc head outputs will convert into pressures.
    
    * **datumDepth** [|m|] (-30000 to 0) - depth of the datum used to calculate 
      elevation head (height of a point above the datum). The default value is 
      -1050 |m|. The datum depth is relative to the top boundary. Depth beneath 
      the top boundary is taken as being negative, so a value of 0 |m| would mean 
      the datum is at the top boundary while a value of -500 |m| would mean the 
      datum is 500 |m| beneath the top boundary. Elevation head is then used to 
      calculate pressure head (overall hydraulic head minus elevation head), and 
      pressure head is used to calculate pressures. This parameter therefore 
      impacts how the hydraulic head parameters and outputs will convert into 
      pressures. This parameter is only used if **specifyDatum** is 1. If 
      **specifyDatum** is 0, the datum is set as the bottom depth of the lowest 
      stratigraphic unit represented in the model.

    The possible outputs from the SALSA component are hydraulic head values,
    leakage across aquifer-aquitard interfaces (volumetric rates and volumes),
    and leakage from wells into aquifers (volumetric rates and volumes). In the
    control file interface, the user can add all observations for an output type
    by using the word 'All' instead of a specific index. For example, using
    **headProfile2VertPointAllShale5** would add the observations for all
    vertical points within shale 5 for profile 2. This approach prevents the user
    from needing to add each vertical point (which depends on the
    **numberOfVerticalPoints** parameter). Conversely, using **headProfileAllVertPointAllShaleAll**
    would add all of the **headProfile#VertPoint#Shale#** observations for all
    profiles and points within all shales. Additionally, using **wellAllLeakageRateAquiferAll**
    would add the **well#LeakageRateAquifer#** observations for all wells and
    all aquifers.

    The outputs of the SALSA component are shown below. The ``x`` and ``y`` 
    locations at which aquifer and shale outputs are calculated are specified 
    separately. The aquifer output coordinates are given with the ``aquiferCoordx`` 
    and ``aquiferCoordy`` keyword arguments, while the shale output coordinates 
    are given with the ``shaleCoordx`` and ``shaleCoordy`` keyword arguments. 
    The location index for aquifer output (Loc#) ranges from 1 to the number of 
    locations provided with the ``aquiferCoordx`` and ``aquiferCoordy`` keyword 
    arguments. The location index for shale output (Profile#) ranges from 1 to 
    the number of locations provided with the ``shaleCoordx`` and ``shaleCoordy`` 
    keyword arguments. Some shale outputs are given for different depths within 
    a shale, where each depth is one of the points specified with the 
    **numberOfVerticalPoints** parameter. For different points within a shale, 
    the point index (VertPoint#) ranges from 1 to the **numberOfVerticalPoints** 
    value. The vertical point index starts at 1 for the deepest point in a shale 
    and increases moving upwards across the shale layer.

    * **headLoc#Aquifer#** [|m|] - hydraulic head in a specific aquifer
      (Aquifer#) at a specific location (headLoc#). At each location,
      hydraulic head is uniform across the thickness of each aquifer.
      
    * **pressureLoc#Aquifer#**, **pressureLoc#MidAquifer#**, 
      **pressureLoc#TopAquifer#** [|Pa|] - pressure at the bottom, middle, or 
      top of a specific aquifer (Aquifer#) at a specific location (Loc#). The 
      pressures at the bottom will always be the highest, while the pressures 
      at the top will be the lowest.
      
    * **headProfile#VertPoint#Shale#** [|m|] - hydraulic head values at a particular
      output location (Profile#), within a certain shale (Shale#), and at a particular 
      point within the shale (VertPoint#).

    * **pressureProfile#VertPoint#Shale#** [|Pa|] - pressure at a particular
      output location (Profile#), within a certain shale (Shale#), and
      at a particular point within the shale (VertPoint#).
    
    * **allHeadProfilesShales** [|m|] - formatted array of hydraulic head values
      across all shale layers and all shale output locations (the ``shaleCoordx`` 
      and ``shaleCoordy`` keyword arguments). If included in the component's 
      output, the simulation will save an *.npz* file containing this array. If 
      included as an output and the ``SalsaProfile`` or ``SalsaTimeSeries`` 
      plot types are used with the ``SaveCSVFiles`` option set to ``True``, the 
      **allHeadProfilesShales** results will be saved to a *.csv* file. If the 
      array is loaded in Python (see below), a particular shale hydraulic head 
      value can be accessed with the indexing approach of 
      array[location index, time index, depth index]. Here, the location index
      corresponds with the number (N) of locations given with the ``shaleCoordx``
      and ``shaleCoordy`` keyword arguments (ranges from 0 to N - 1). The
      time index corresponds with the number of timesteps (ranges 0 to N - 1).
      And the depth index corresponds the number of depths in the **profileDepths**
      array (ranges from 0 to N - 1).
    
    * **allPressureProfilesShales** [|Pa|] - formatted array of pressure values
      across all shale layers and all shale output locations (the ``shaleCoordx`` 
      and ``shaleCoordy`` keyword arguments). If included in the component's 
      output, the simulation will save an *.npz* file containing this array. If 
      included as an output and the ``SalsaProfile`` or ``SalsaTimeSeries`` 
      plot types are used with the ``SaveCSVFiles`` option set to ``True``, the 
      **allPressureProfilesShales** results will be saved to a *.csv* file. If the 
      array is loaded in Python (see below), a particular shale pressure 
      value can be accessed with the indexing approach of 
      array[location index, time index, depth index]. Here, the location index
      corresponds with the number (N) of locations given with the ``shaleCoordx``
      and ``shaleCoordy`` keyword arguments (ranges from 0 to N - 1). The
      time index corresponds with the number of timesteps (ranges 0 to N - 1).
      And the depth index corresponds the number of depths in the **profileDepths**
      array (ranges from 0 to N - 1).

    * **profileDepths** [|m|] - depths used for the hydraulic head profiles across
      the shales. Depths beneath the surface are taken as being negative. If
      included in the component's output, the simulation will save an *.npz* file
      containing the depth array. If **profileDepths** is included as an output
      and the ``SalsaProfile`` or ``SalsaTimeSeries`` plot types is used
      with the ``SaveCSVFiles`` option set to ``True``, the **profileDepths**
      will be saved to a *.csv* file. If the array is loaded in Python (see below),
      then each depth can be accessed in Python with the indexing approach of
      array[depth index] (the depth index ranges from 0 to N - 1, where N is the
      number of depths).

    * **wellLeakageRateAquifer#** [|m^3/s|] - total volumetric leakage rate from
      all leaking leaking wells into aquifer ``#``, where ``#`` ranges from 1
      to the number of aquifer layers.

    * **wellLeakageVolumeAquifer#** [|m^3|] - cumulative volume of leakage from
      all leaking wells into aquifer ``#``, where ``#`` ranges from 1 to the
      number of aquifer layers.

    * **well#LeakageRateAquifer#** [|m^3/s|] - leakage volumetric rate from leaking
      well ``#`` into aquifer ``#``. The aquifer index (Aquifer#) ranges from
      1 to the number of aquifer layers. The leaking well index (well#) ranges
      from 1 to the number of leaking well coordinates provided (``leakingWellCoordx``
      and ``leakingWellCoordy`` keyword arguments).

    * **well#LeakageVolumeAquifer#** [|m^3|] - cumulative volume of leakage from
      leaking well ``#`` into aquifer ``#``. The aquifer index (Aquifer#)
      ranges from 1 to the number of aquifer layers. The leaking well index
      (well#) ranges from 1 to the number of leaking well coordinates provided
      (``leakingWellCoordx`` and ``leakingWellCoordy`` keyword arguments).

    * **diffuseLeakageRateBottomAquifer#**, **diffuseLeakageRateTopAquifer#**
      [|m^3/s|] - volumetric diffuse leakage rates through the bottom or top
      aquifer-aquitard interface of aquifer ``#``, where ``#`` ranges from 1 to
      the number of aquifer layers.

    * **diffuseLeakageVolumeBottomAquifer#**, **diffuseLeakageVolumeTopAquifer#**
      [|m^3|] - cumulative volume from diffuse leakage across the bottom or top
      aquifer-aquitard interface of aquifer ``#``, where ``#`` ranges from 1 to
      the number of aquifer layers.
      
    * **contourPlotAquiferHead** [|m|] - formatted array of hydraulic head values
      in each aquifer specified in the ``aquiferNamesContourPlots`` keyword
      argument. If included in the component's output, the simulation will save
      an *.npz* file containing the array. If included as an output and the
      ``SalsaContourPlot`` plot type is used with the ``SaveCSVFiles`` option
      set to ``True``, the **contourPlotAquiferHead** results will be saved to
      a *.csv* file. If the array is loaded in Python (see below), then a particular
      head value can be accessed with the indexing approach of 
      array[time index, aquifer index, location index]. Here, the time index
      corresponds to the number (N) of time steps used (ranges from 0 to N - 1).
      The aquifer index refers to the number of aquifers included in the
      ``aquiferNamesContourPlots`` keyword argument. For example, if there are
      three aquifers then setting ``aquiferNamesContourPlots`` to "[2, 3]" for
      aquifers 2 and 3 would exclude aquifer 1 results from the array. If
      ``aquiferNamesContourPlots`` is not included, the default approach is to
      only include aquifer 1. The location index here corresponds to each ``x``
      and ``y`` value in the **contourPlotCoordx** and **contourPlotCoordy**
      arrays.

    * **contourPlotAquiferPressure**, **contourPlotMidAquiferPressure**, 
      **contourPlotTopAquiferPressure** [|Pa|] - formatted array of pressures 
      at the bottom, middle, or top of each aquifer specified in the 
      ``aquiferNamesContourPlots`` keyword argument. The pressures at the bottom 
      will always be the highest, while the pressures at the top will be the 
      lowest. If included in the component's output, the simulation will save 
      an *.npz* file containing the array. If included as an output and the 
      ``SalsaContourPlot`` plot type is used with the ``SaveCSVFiles`` option 
      set to ``True``, the results can be saved to a *.csv* file. If the array 
      is loaded in Python (see below), then a particular pressure can be accessed 
      with the indexing approach of array[time index, aquifer index, location index]. 
      Here, the time index corresponds to the number (N) of time steps used 
      (ranges from 0 to N - 1). The aquifer index refers to the number of aquifers 
      included in the ``aquiferNamesContourPlots`` keyword argument. For example, 
      if there are three aquifers then setting ``aquiferNamesContourPlots`` to 
      "[2, 3]" for aquifers 2 and 3 would exclude aquifer 1 results from the 
      array. If ``aquiferNamesContourPlots`` is not included, the default approach 
      is to only include aquifer 1. The location index here corresponds to each 
      ``x`` and ``y`` value in the **contourPlotCoordx** and **contourPlotCoordy**
      arrays.

    * **contourPlotCoordx** [|m|] - array of the easting ``x`` coordinates
      corresponding with each **contourPlotAquiferHead** value. The coordinates
      are impacted by the parameters related to the rectangular and radial grid
      points. If included in the component's output, the simulation will save
      an *.npz* file containing the array. If included as an output and the
      ``SalsaContourPlot`` plot type is used with the ``SaveCSVFiles`` option
      set to ``True``, the **contourPlotCoordx** results will be saved to a
      *.csv* file. If the array is loaded in Python (see below), then each ``x``
      value can be accessed in Python with the indexing approach of
      array[location index] (the location index ranges from 0 to N - 1, where
      N is the number of locations).

    * **contourPlotCoordy** [|m|] - formatted array of the northing ``y`` coordinates
      corresponding with each **contourPlotAquiferHead** value. The coordinates
      are impacted by the parameters related to the rectangular and radial grid
      points. If included in the component's output, the simulation will save
      an *.npz* file containing the array. If included as an output and the
      ``SalsaContourPlot`` plot type is used with the ``SaveCSVFiles`` option
      set to ``True``, the **contourPlotCoordy** results will be saved to a
      *.csv* file. If the array is loaded in Python (see below), then each ``y``
      value can be accessed in Python with the indexing approach of
      array[location index] (the location index ranges from 0 to N - 1, where
      N is the number of locations).
    
    The following outputs are contained in NumPy arrays: **allHeadProfilesShales**, 
    **profileDepths**, **contourPlotAquiferHead**, **contourPlotCoordx**, **contourPlotCoordy**, 
    **contourPlotAquiferPressure**, **contourPlotMidAquiferPressure**, and 
    **contourPlotTopAquiferPressure**. When one of these outputs is included, 
    the simulation will save an *.npz* file containing the output array. These
    output arrays can be loaded in Python with NumPy (use the statement
    "import numpy") and then the statement "array = numpy.load(File_NAME.npz)['data']"
    (where FILE_NAME is the name of the *.npz* file). The data from these arrays
    will also be saved to .csv files, however, when using certain plot types
    in NRAP-Open-IAM.
    
    Keyword arguments are used to set inputs including the times at which injection
    rates change over time (``periodEndTimes``), the locations of all active wells
    (``activeWellCoordx`` and ``activewellCoordy``) and leaking wells
    (``leakingWellCoordx`` and ``leakingWellCoordy``), the locations at which
    the **headLoc#Aquifer#** outputs are calculated (``aquiferCoordx`` and
    ``aquiferCoordy``), the locations at which the **headProfile#VertPoint#Shale#**
    and **allHeadProfilesShales** outputs are calculated (``shaleCoordx``
    and ``shaleCoordy``), and the aquifers for which hydraulic head results
    are included in the **contourPlotAquiferHead** output (``aquiferNamesContourPlots``).
    In the control file interface, the keyword arguments are provided under the
    entries ``InjectionWell``, ``LeakingWell``, ``OutputLocations``, and
    ``ContourPlot``.

    The creation of the contour plot output (**contourPlotAquiferHead**,
    **contourPlotCoordx**, and **contourPlotCoordy**) is the most demanding
    part of a simulation using SALSA. To reduce model run times, the sizes of
    these outputs can be reduced. Specifically, the number of nodes used in the
    contour plot mesh is controlled by the parameters **numberOfNodesXDir**,
    **numberOfNodesYDir**, and **numberOfRadialGrids**. Decreasing these
    parameters to the minimum values allowed will improve model run times.
    Additionally, the aquifers included in the **contourPlotAquiferHead** array
    are specified with the ``aquiferNamesContourPlots`` keyword argument. If
    this keyword argument is not provided, the default approach is to only include
    aquifer 1. Model run times will be improved if ``aquiferNamesContourPlots``
    includes only one aquifer number (e.g., ``aquiferNamesContourPlots: [1]``
    for only aquifer 1, as shown in control file example 63a).

    The SALSA component will only work with Python versions 3.9.X, 3.10.X, 
    3.11.X, and 3.12.X. Using the SALSA component on a Linux machine requires 
    the installation of the gfortran compiler; see the installation instructions 
    for NRAP-Open-IAM.

    For control file examples using the SALSA component, see control file examples 
    62a to 65c. This component is not currently available through the graphical
    user interface (GUI).
    """
    def __init__(self, name, parent, periodEndTimes=None, activeWellCoordx=None,
                 activeWellCoordy=None, leakingWellCoordx=None,
                 leakingWellCoordy=None, aquiferCoordx=None,
                 aquiferCoordy=None, shaleCoordx=None, shaleCoordy=None,
                 aquiferNamesContourPlots=None, output_dir=None, 
                 stochastic=False, realizations=None):
        """

        Constructor method of SALSA class.

        :param name: name of component model
        :type name: str

        :param parent: the SystemModel object that the component model
            belongs to
        :type parent: SystemModel object

        :returns: SALSA class object
        """
        # Set up keyword arguments of the 'model' method provided by the system model
        model_kwargs = {'time_point': 365.25, 'time_step': 365.25}   # default value of 365.25 days

        super().__init__(name, parent, model=self.simulation_model,
                         model_kwargs=model_kwargs)

        # Add type attribute
        self.class_type = 'SALSA'

        # Set input keyword arguments
        self.periodEndTimes = periodEndTimes
        self.activeWellCoordx = activeWellCoordx
        self.activeWellCoordy = activeWellCoordy
        self.leakingWellCoordx = leakingWellCoordx
        self.leakingWellCoordy = leakingWellCoordy
        self.aquiferCoordx = aquiferCoordx
        self.aquiferCoordy = aquiferCoordy
        self.shaleCoordx = shaleCoordx
        self.shaleCoordy = shaleCoordy
        self.aquiferNamesContourPlots = aquiferNamesContourPlots
        self.output_dir = output_dir

        # In the original version of SALSA, the user can provide x and y values
        # for the contour plot data. In the current version of salsa_wrap
        # (used in salsa_ROM.py), x and y values cannot be provided by the
        # user - they can only be generated automatically. Therefore, the parameter
        # flagMesh is fixed at a value of 1 (specifying automatic generation).
        # These keyword arguments are made to match the number of points generated
        # by SALSA, but there are kept here in case future versions allow the
        # user to specify x and y values for contour plots. Once they are enabled,
        # they will be included in the __init__() method.
        self.numberOfContourPlotNodes = None
        self.contourPlotCoordx = None
        self.contourPlotCoordy = None

        kw_args_dict = {
            'periodEndTimes': self.periodEndTimes,
            'activeWellCoordx': self.activeWellCoordx,
            'activeWellCoordy': self.activeWellCoordy,
            'leakingWellCoordx': self.leakingWellCoordx,
            'leakingWellCoordy': self.leakingWellCoordy,
            'aquiferCoordx': self.aquiferCoordx,
            'aquiferCoordy': self.aquiferCoordy,
            'shaleCoordx': self.shaleCoordx,
            'shaleCoordy': self.shaleCoordy,
            'aquiferNamesContourPlots': self.aquiferNamesContourPlots,
            'numberOfContourPlotNodes': self.numberOfContourPlotNodes,
            'contourPlotCoordx': self.contourPlotCoordx,
            'contourPlotCoordy': self.contourPlotCoordy}

        # Check if certain keyword arguments are provided as numpy arrays
        for kw_arg in KW_ARGS:
            if not isinstance(kw_args_dict[kw_arg], np.ndarray):
                try:
                    kw_args_dict[kw_arg] = np.array(kw_args_dict[kw_arg])
                except:
                    msg_pt1 = 'During the __init__() method for'

                    err_msg = KW_ARG_ARRAY_MSG.format(
                        msg_pt1, self.name, kw_arg, kw_args_dict[kw_arg])

                    logging.error(err_msg)

        # Set default parameters of the component model
        self.add_default_par('bottomLayerType', value=1)
        self.add_default_par('topLayerType', value=0)
        self.add_default_par('numberOfShaleLayers', value=3)
        self.add_default_par('numberOfAquiferLayers', value=3)
        self.add_default_par('shaleThickness', value=250)
        self.add_default_par('aquiferThickness', value=100)
        self.add_default_par('shaleLogVertK', value=-11)
        self.add_default_par('aquiferLogHorizK', value=-5)
        self.add_default_par('aquiferANSR', value=1.0)
        self.add_default_par('shaleLogSS', value=-6.0)
        self.add_default_par('aquiferLogSS', value=-6.0)
        self.add_default_par('aquiferFluidDensity', value=1000)
        self.add_default_par('aquiferHead', value=0.0)
        self.add_default_par('shaleInitialGamma', value=0)
        self.add_default_par('shaleGamma', value=0)
        self.add_default_par('aquiferGamma', value=0)
        self.add_default_par('bottomBoundaryCond', value=1)
        self.add_default_par('bottomBoundaryHead', value=0.0)
        self.add_default_par('topBoundaryCond', value=1)
        self.add_default_par('topBoundaryHead', value=0.0)
        # For all active wells, aquifer 1 is given a default value for the first injection period
        self.add_default_par('activeWellQ', value=0.01)
        self.add_default_par('activeWellRadiusAquifer', value=0.1)
        self.add_default_par('leakingWellRadiusAquifer', value=0.1)
        self.add_default_par('leakingWellLogKAquifer', value=-5)
        self.add_default_par('leakingWellRadiusShale', value=0.1)
        self.add_default_par('leakingWellLogKShale', value=-5)
        self.add_default_par('leakingWellStatAquifer', value=1)
        self.add_default_par('leakingWellStatShale', value=1)
        self.add_default_par('leakingWellQ', value=0)
        self.add_default_par('numberOfLaplaceTerms', value=10)
        self.add_default_par('flagMesh', value=1)
        self.add_default_par('xCent', value=0.0)
        self.add_default_par('yCent', value=0.0)
        self.add_default_par('xMax', value=5000.0)
        self.add_default_par('yMax', value=5000.0)
        self.add_default_par('gridExp', value=1.0)
        self.add_default_par('numberOfNodesXDir', value=30)
        self.add_default_par('numberOfNodesYDir', value=30)
        self.add_default_par('numberOfRadialGrids', value=6)
        self.add_default_par('radialZoneRadius', value=200.0)
        self.add_default_par('radialGridExp', value=1.2)
        self.add_default_par('numberOfVerticalPoints', value=10)
        self.add_default_par('topBoundaryPressure', value=101325)
        self.add_default_par('topBoundaryFluidDensity', value=1000)
        self.add_default_par('waterTableDepth', value=0)
        self.add_default_par('specifyDatum', value=0)
        self.add_default_par('datumDepth', value=-1050)

        # Define dictionary of boundaries
        self.pars_bounds = dict()
        self.pars_bounds['bottomLayerType'] = [0, 1]
        self.pars_bounds['topLayerType'] = [0, 1]
        self.pars_bounds['numberOfShaleLayers'] = [1, 30]
        self.pars_bounds['numberOfAquiferLayers'] = [1, 29]
        self.pars_bounds['shaleThickness'] = [1, 2000]
        self.pars_bounds['aquiferThickness'] = [1, 2000]
        self.pars_bounds['shaleLogVertK'] = [-16, -4]
        self.pars_bounds['aquiferLogHorizK'] = [-13, -3]
        self.pars_bounds['aquiferANSR'] = [1.0e-3, 1.0e+3]
        self.pars_bounds['shaleLogSS'] = [-12.0, 0]
        self.pars_bounds['aquiferLogSS'] = [-12.0, 0]
        self.pars_bounds['aquiferFluidDensity'] = [900, 1500]
        self.pars_bounds['aquiferHead'] = [-1000, 1000]
        self.pars_bounds['shaleInitialGamma'] = [-1.0e-10, 1.0e-10]
        self.pars_bounds['shaleGamma'] = [-1.0e-10, 1.0e-10]
        self.pars_bounds['aquiferGamma'] = [-1.0e-10, 1.0e-10]
        self.pars_bounds['bottomBoundaryCond'] = [0, 1]
        self.pars_bounds['bottomBoundaryHead'] = [-1000, 1000]
        self.pars_bounds['topBoundaryCond'] = [0, 1]
        self.pars_bounds['topBoundaryHead'] = [-1000, 1000]
        self.pars_bounds['activeWellQ'] = [-5, 5]
        self.pars_bounds['activeWellRadiusAquifer'] = [0.01, 0.5]
        self.pars_bounds['leakingWellRadiusAquifer'] = [0.01, 0.5]
        self.pars_bounds['leakingWellLogKAquifer'] = [-13, -3]
        self.pars_bounds['leakingWellRadiusShale'] = [0.01, 0.5]
        self.pars_bounds['leakingWellLogKShale'] = [-13, -3]
        self.pars_bounds['leakingWellStatAquifer'] = [0, 1]
        self.pars_bounds['leakingWellStatShale'] = [0, 1]
        self.pars_bounds['leakingWellQ'] = [-5, 5]
        self.pars_bounds['numberOfLaplaceTerms'] = [5, 1000]
        self.pars_bounds['flagMesh'] = [0, 1]
        self.pars_bounds['xCent'] = [-1.0e+5, 1.0e+5]
        self.pars_bounds['yCent'] = [-1.0e+5, 1.0e+5]
        self.pars_bounds['xMax'] = [500, 1.0e+5]
        self.pars_bounds['yMax'] = [500, 1.0e+5]
        self.pars_bounds['gridExp'] = [0.1, 10]
        self.pars_bounds['numberOfNodesXDir'] = [3, 500]
        self.pars_bounds['numberOfNodesYDir'] = [3, 500]
        self.pars_bounds['numberOfRadialGrids'] = [2, 24]
        self.pars_bounds['radialZoneRadius'] = [10.0, 10000.0]
        self.pars_bounds['radialGridExp'] = [1.0, 10.0]
        self.pars_bounds['numberOfVerticalPoints'] = [3, 100]
        self.pars_bounds['topBoundaryPressure'] = [8.0e+4, 3.0e+7]
        self.pars_bounds['topBoundaryFluidDensity'] = [900, 1500]
        self.pars_bounds['waterTableDepth'] = [-300, 0]
        self.pars_bounds['specifyDatum'] = [0, 1]
        self.pars_bounds['datumDepth'] = [-30000, 0]

        # Define dictionary of parameters that are restricted to specific values
        self.acceptable_vals = dict()
        self.acceptable_vals['bottomLayerType'] = [0, 1]
        self.acceptable_vals['topLayerType'] = [0, 1]
        self.acceptable_vals['bottomBoundaryCond'] = [0, 1]
        self.acceptable_vals['topBoundaryCond'] = [0, 1]
        self.acceptable_vals['leakingWellStatAquifer'] = [0, 1]
        self.acceptable_vals['leakingWellStatShale'] = [0, 1]
        self.acceptable_vals['specifyDatum'] = [0, 1]

        # Define dictionary of keyword arguments that can be given as None,
        # depending on other parameter values. The entry for each keyword argument
        # contains a dictionary. The dictionary contains the parameter name and
        # value that controls if the keyword argument can be None.
        self.kwargs_none_vals = dict()
        self.kwargs_none_vals['contourPlotCoordx'] = {'par': 'flagMesh', 'val': 1}
        self.kwargs_none_vals['contourPlotCoordy'] = {'par': 'flagMesh', 'val': 1}

        self.output_labels = []

        # Setup default observations of the component
        self.default_obs = {obs_nm: 0.0 for obs_nm in self.output_labels}

        # SALSA does not include t = 0 years when running, so this value is assigned
        # for that time.
        self.t0_val = 0

        # The model only needs to be run once, as output for all times can be
        # obtained at once.
        self.run_frequency = 1
        self.default_run_frequency = 1

        # Define gridded observations names
        self.grid_obs_keys = SALSA_GRID_OBSERVATIONS

        # Used to track whether time input has already been converted from 
        # years to days
        self.converted_from_years_to_days = False

        # Used to track whether the user has been warned about the bottomBoundaryCond 
        # or topBoundaryCond parameters being changed
        self.warning_for_bottom_bound_cond = False
        self.warning_for_top_bound_cond = False

        # Keeps track of whether a warning statement was provided regarding the 
        # water table depth (to prevent repeating the message)
        self.warning_for_water_table = False

        # Specifies whether the water table depth input was too low and had to 
        # be clipped to the bottom depth of the top aquifer
        self.water_table_clipped = False

        # Specifies whether the leakingWellCoordx and leakingWellCoordy keyword 
        # arguments have been set with the grid_locations() function (in locations.py) 
        # in the control file interface.
        self.leaking_well_grid = False

        # These are updated during the compile_pars() method, which is run during
        # the simulation_model() method.
        self.shaleThicknesses = None
        self.aquiferThicknesses = None
        self.bottomLayerType = None
        self.topLayerType = None
        self.activeWellQ = None
        self.activeWellRadii = None
        self.leakingWellAqRadii = None
        self.leakingWellAqK = None
        self.leakingWellShaleRadii = None
        self.leakingWellShaleK = None
        self.leakingWellAqStat = None
        self.leakingWellShaleStat = None
        self.leakingWellQ = None
        self.leakingWellQAq = None

        # Used to set up the output folders correctly for stochastic simulations
        self.stochastic = stochastic
        self.realizations = realizations

        debug_msg = 'SALSA component created with name {}'.format(name)
        logging.debug(debug_msg)

    def set_up_extra_output(self, comp_data, output_dir, analysis, realizations):
        """
        Used in the control file interface to handle the stochastic and realizations 
        attributes, which are used in the ROM file to correctly create separate 
        folders for the extra output files created when the .dll files are used.
        """
        if analysis in ['lhs', 'parstudy']:
            self.stochastic = True

        self.realizations = realizations

    def connect_with_system(self, component_data, name2obj_dict, *args, **kwargs):
        """
        Code to add the SALSA component to the system model for the control file
        interface.

        :param component_data: Dictionary of component data
        :type component_data: dict

        :param name2obj_dict: Dictionary to map component names to objects
        :type name2obj: dict

        :returns: None
        """
        # Store the output directory so that SALSA files can be saved there
        self.output_dir = kwargs['output_dir']

        # Add the keyword arguments first, as some parameter names depend on them
        kw_args_section_dict = {
            'periodEndTimes': 'InjectionWell',
            'activeWellCoordx': 'InjectionWell',
            'activeWellCoordy': 'InjectionWell',
            'leakingWellCoordx': 'LeakingWell',
            'leakingWellCoordy': 'LeakingWell',
            'aquiferCoordx': 'OutputLocations',
            'aquiferCoordy': 'OutputLocations',
            'shaleCoordx': 'OutputLocations',
            'shaleCoordy': 'OutputLocations',
            'aquiferNamesContourPlots': 'ContourPlot',
            'numberOfContourPlotNodes': 'ContourPlot',
            'contourPlotCoordx': 'ContourPlot',
            'contourPlotCoordy': 'ContourPlot'}

        kw_args_to_check = KW_ARGS.copy()
        
        # If the grid locations were already set in openiam_cf.py, do not check  
        # the leakingWellCoordx and leakingWellCoordy 
        if self.leaking_well_grid:
            # If the grid placement option was successfully used, get rid of
            # the check for leakingWellCoordx and leakingWellCoordy below
            kw_args_to_check.remove('leakingWellCoordx')
            kw_args_to_check.remove('leakingWellCoordy')

        # Get the keyword arguments from component_data
        for kw_arg in kw_args_to_check:
            section_key = kw_args_section_dict[kw_arg]

            if section_key in component_data:
                component_input = component_data[section_key]

                if component_input is None and kw_arg not in [
                        'numberOfContourPlotNodes', 'contourPlotCoordx',
                        'contourPlotCoordy']:
                    warning_msg = KW_ARG_NONE_MSG.format(
                        self.name, '{} entry'.format(section_key))

                    logging.warning(warning_msg)

                else:
                    if kw_arg in component_input:
                        kw_arg_input = component_input[kw_arg]

                        # Check if the input was provided through a file
                        if isinstance(kw_arg_input, str):
                            file_path = os.path.join(iam_bc.IAM_DIR, kw_arg_input)

                            if os.path.isfile(file_path):
                                kw_arg_input = np.genfromtxt(
                                    file_path, delimiter=",", dtype='f8')
                            else:
                                warning_msg = FILE_WARNING_MSG.format(
                                    self.name, kw_arg, kw_arg_input)

                                logging.warning(warning_msg)

                        else:
                            try:
                                kw_arg_input = np.array(kw_arg_input)
                            except:
                                if kw_arg not in [
                                        'numberOfContourPlotNodes',
                                        'contourPlotCoordx', 'contourPlotCoordy']:
                                    msg_pt1 = 'During the connect_with_system() method for'

                                    err_msg = KW_ARG_ARRAY_MSG.format(
                                        msg_pt1, self.name, kw_arg, kw_arg_input)

                                    logging.error(err_msg)

                        # Set the keyword argument
                        if kw_arg == 'periodEndTimes':
                            self.periodEndTimes = kw_arg_input
                        elif kw_arg == 'activeWellCoordx':
                            self.activeWellCoordx = kw_arg_input
                        elif kw_arg == 'activeWellCoordy':
                            self.activeWellCoordy = kw_arg_input
                        elif kw_arg == 'leakingWellCoordx':
                            self.leakingWellCoordx = kw_arg_input
                        elif kw_arg == 'leakingWellCoordy':
                            self.leakingWellCoordy = kw_arg_input
                        elif kw_arg == 'aquiferCoordx':
                            self.aquiferCoordx = kw_arg_input
                        elif kw_arg == 'aquiferCoordy':
                            self.aquiferCoordy = kw_arg_input
                        elif kw_arg == 'shaleCoordx':
                            self.shaleCoordx = kw_arg_input
                        elif kw_arg == 'shaleCoordy':
                            self.shaleCoordy = kw_arg_input
                        elif kw_arg == 'aquiferNamesContourPlots':
                            self.aquiferNamesContourPlots = kw_arg_input
                        elif kw_arg == 'numberOfContourPlotNodes':
                            self.numberOfContourPlotNodes = kw_arg_input
                        elif kw_arg == 'contourPlotCoordx':
                            self.contourPlotCoordx = kw_arg_input
                        elif kw_arg == 'contourPlotCoordy':
                            self.contourPlotCoordy = kw_arg_input

                    elif kw_arg not in NOT_INCLUDED_KW_ARGS:
                        if kw_arg in REQUIRED_KW_ARGS:
                            err_msg = KW_ARG_NOT_FOUND_ERR_MSG.format(
                                self.name, '{} entry'.format(section_key), '')

                            logging.error(err_msg)
                        else:
                            warning_msg = KW_ARG_NOT_FOUND_WARNING_MSG.format(
                                self.name, '{} entry'.format(section_key), '')

                            if kw_arg in DEFAULT_KW_ARGS.keys():
                                default_kw_arg_input = DEFAULT_KW_ARGS[kw_arg]

                                warning_msg += 'The default value of {} will be used.'.format(
                                    default_kw_arg_input)

                                # Set the keyword argument
                                if kw_arg == 'periodEndTimes':
                                    self.periodEndTimes = default_kw_arg_input
                                elif kw_arg == 'activeWellCoordx':
                                    self.activeWellCoordx = default_kw_arg_input
                                elif kw_arg == 'activeWellCoordy':
                                    self.activeWellCoordy = default_kw_arg_input
                                elif kw_arg == 'leakingWellCoordx':
                                    self.leakingWellCoordx = default_kw_arg_input
                                elif kw_arg == 'leakingWellCoordy':
                                    self.leakingWellCoordy = default_kw_arg_input
                                elif kw_arg == 'aquiferCoordx':
                                    self.aquiferCoordx = default_kw_arg_input
                                elif kw_arg == 'aquiferCoordy':
                                    self.aquiferCoordy = default_kw_arg_input
                                elif kw_arg == 'shaleCoordx':
                                    self.shaleCoordx = default_kw_arg_input
                                elif kw_arg == 'shaleCoordy':
                                    self.shaleCoordy = default_kw_arg_input
                                elif kw_arg == 'aquiferNamesContourPlots':
                                    self.aquiferNamesContourPlots = default_kw_arg_input
                                elif kw_arg == 'numberOfContourPlotNodes':
                                    self.numberOfContourPlotNodes = default_kw_arg_input
                                elif kw_arg == 'contourPlotCoordx':
                                    self.contourPlotCoordx = default_kw_arg_input
                                elif kw_arg == 'contourPlotCoordy':
                                    self.contourPlotCoordy = default_kw_arg_input

                            logging.warning(warning_msg)

            elif kw_arg not in NOT_INCLUDED_KW_ARGS:
                if kw_arg in REQUIRED_KW_ARGS:
                    err_msg = KW_ARG_NOT_FOUND_ERR_MSG.format(
                        self.name, '{} entry'.format(section_key), '')

                    logging.error(err_msg)
                else:
                    warning_msg = KW_ARG_NOT_FOUND_WARNING_MSG.format(
                        self.name, '{} entry'.format(section_key), '')

                    if kw_arg in DEFAULT_KW_ARGS.keys():
                        default_kw_arg_input = DEFAULT_KW_ARGS[kw_arg]

                        warning_msg += 'The default value of {} will be used.'.format(
                            default_kw_arg_input)

                        # Set the keyword argument
                        if kw_arg == 'periodEndTimes':
                            self.periodEndTimes = default_kw_arg_input
                        elif kw_arg == 'activeWellCoordx':
                            self.activeWellCoordx = default_kw_arg_input
                        elif kw_arg == 'activeWellCoordy':
                            self.activeWellCoordy = default_kw_arg_input
                        elif kw_arg == 'leakingWellCoordx':
                            self.leakingWellCoordx = default_kw_arg_input
                        elif kw_arg == 'leakingWellCoordy':
                            self.leakingWellCoordy = default_kw_arg_input
                        elif kw_arg == 'aquiferCoordx':
                            self.aquiferCoordx = default_kw_arg_input
                        elif kw_arg == 'aquiferCoordy':
                            self.aquiferCoordy = default_kw_arg_input
                        elif kw_arg == 'shaleCoordx':
                            self.shaleCoordx = default_kw_arg_input
                        elif kw_arg == 'shaleCoordy':
                            self.shaleCoordy = default_kw_arg_input
                        elif kw_arg == 'aquiferNamesContourPlots':
                            self.aquiferNamesContourPlots = default_kw_arg_input
                        elif kw_arg == 'numberOfContourPlotNodes':
                            self.numberOfContourPlotNodes = default_kw_arg_input
                        elif kw_arg == 'contourPlotCoordx':
                            self.contourPlotCoordx = default_kw_arg_input
                        elif kw_arg == 'contourPlotCoordy':
                            self.contourPlotCoordy = default_kw_arg_input

                    logging.warning(warning_msg)

        # Process parameters data. Do not use the process_parameters function
        # here because the SALSA component requires a unique handling of
        # parameter input.
        if 'Parameters' in component_data:
            parameter_input_dict = component_data['Parameters']

            if isinstance(parameter_input_dict, dict):
                strat_related_pars = ['numberOfShaleLayers', 'bottomLayerType',
                                      'topLayerType']

                # These stratigraphy parameters are handled separately because
                # they cannot vary in the control file interface.
                for strat_par in strat_related_pars:
                    if strat_par in parameter_input_dict:
                        par_input = parameter_input_dict[strat_par]

                        if isinstance(par_input, dict):
                            stochastic_check = ('min' in par_input or 'max' in par_input)

                            if 'vary' in par_input:
                                if par_input['vary']:
                                    stochastic_check = True

                            if stochastic_check:
                                warning_msg = STRAT_PAR_WARNING_MSG.format(strat_par, self.name)
                                logging.warning(warning_msg)

                                if 'value' in par_input:
                                    par_input = {'value': par_input['value'],
                                                 'vary': False}
                                else:
                                    par_input = None

                        elif not isinstance(par_input, dict):
                            par_input = {'value': par_input, 'vary': False}

                        if par_input is not None:
                            self.add_par(strat_par, **par_input)

                numberOfShaleLayers = get_parameter_val(self, 'numberOfShaleLayers')
                numberOfAquiferLayers = self.calc_num_aquifers()

                self.add_par('numberOfAquiferLayers', value=numberOfAquiferLayers,
                             vary=False)

                strat_related_pars.append('numberOfAquiferLayers')

                # Go through the parameters twice. The first time, only add
                # parameters that contian the word 'All'. The second time, add
                # parameters that do not contain the word 'All'. With this approach,
                # parameters containing 'All' can be overwritten by more specific
                # parameter names.
                for key, val in parameter_input_dict.items():
                    if 'All' in key:
                        if not isinstance(val, dict):
                            par_input = {'value': val, 'vary': False}
                        else:
                            par_input = val

                        try:
                            if not isinstance(val, dict):
                                par_input = {'value': val, 'vary': False}
                            else:
                                par_input = val

                            par_names, par_input = self.check_and_assign_parameter_input(
                                key, par_input, numberOfShaleLayers, numberOfAquiferLayers,
                                return_par_input=True)

                            # Returned values of 0 indicate the parameter was
                            # not recognized.
                            if par_names != 0 and par_input != 0:
                                for par_name in par_names:
                                    self.add_par(par_name, **par_input)
                            else:
                                warning_msg = PAR_UNRECOGNIZED_MSG.format(
                                    key, self.name, val)

                                logging.warning(warning_msg)
                        except:
                            warning_msg = PAR_UNRECOGNIZED_MSG.format(
                                key, self.name, val)

                            logging.warning(warning_msg)

                # Now assign more specific parameter names without 'All'
                for key, val in parameter_input_dict.items():
                    if not isinstance(val, dict):
                        par_input = {'value': val, 'vary': False}
                    else:
                        par_input = val

                    if not key in strat_related_pars and 'All' not in key:
                        if key in FIXED_PAR_NAMES:
                            par_input = self.check_par_key_values(
                                key, par_input, key)

                            self.add_par(key, **par_input)

                        else:
                            try:
                                if not isinstance(val, dict):
                                    par_input = {'value': val, 'vary': False}
                                else:
                                    par_input = val

                                par_names, par_input = self.check_and_assign_parameter_input(
                                    key, par_input, numberOfShaleLayers, numberOfAquiferLayers,
                                    return_par_input=True)

                                # Returned values of 0 indicate the parameter
                                # was not recognized.
                                if par_names != 0 and par_input != 0:
                                    for par_name in par_names:
                                        self.add_par(par_name, **par_input)
                                else:
                                    warning_msg = PAR_UNRECOGNIZED_MSG.format(
                                        key, self.name, val)

                                    logging.warning(warning_msg)
                            except:
                                warning_msg = PAR_UNRECOGNIZED_MSG.format(
                                    key, self.name, val)

                                logging.warning(warning_msg)

        # These are used while setting outputs further below
        numberOfVerticalPoints = get_parameter_val(self, 'numberOfVerticalPoints')
        numberOfShaleLayers = get_parameter_val(self, 'numberOfShaleLayers')

        # Used below while setting outputs
        try:
            number_of_profiles = len(self.shaleCoordx)
        except:
            err_msg = KW_ARG_CALC_LEN_MSG.format(
                    self.name, 'shaleCoordx', self.shaleCoordx)
            logging.error(err_msg)

        try:
            number_of_wells = len(self.leakingWellCoordx)
        except:
            err_msg = KW_ARG_CALC_LEN_MSG.format(
                    self.name, 'leakingWellCoordx', self.leakingWellCoordx)
            logging.error(err_msg)
        
        def add_salsa_output_cfi(obs_nm, new_comp_outputs, numberOfShaleLayers, 
                                 numberOfAquiferLayers, number_of_wells, 
                                 number_of_profiles, numberOfVerticalPoints):
            """
            Checks the observation name - if the name applies to the SALSA 
            omponent, adds the corresponding observation(s). This functionality 
            is contained in this function to better catch errors with try / except.
            """
            warn_msg = ''.join([
                '{} is not recognised as an observation name ',
                'of the SALSA component {}.']).format(obs_nm, self.name)

            if obs_nm in SALSA_GRID_OBSERVATIONS:
                self.add_grid_obs(
                    obs_nm, constr_type='matrix', output_dir=self.output_dir)

            elif (('headProfile' in obs_nm or 'pressureProfile' in obs_nm) 
                  and 'VertPoint' in obs_nm and 'Shale' in obs_nm):
                # For outputs 'headProfile#VertPoint#Shale#' or 
                # 'pressureProfile#VertPoint#Shale#', where # may be a 
                # number or 'All'.
                if 'VertPointAll' in obs_nm:
                    point_inds = range(numberOfVerticalPoints)
                else:
                    string_partial = 'VertPoint'
                    point_number = int(
                        obs_nm[obs_nm.index(string_partial) + len(string_partial):
                               obs_nm.index('Shale')])
                    point_inds = [point_number - 1]

                if 'ShaleAll' in obs_nm:
                    shale_inds = range(numberOfShaleLayers)
                else:
                    # For specific shales, like 'headProfile2VertPointAllShale3'
                    string_partial = 'Shale'
                    shale_number = int(
                        obs_nm[(obs_nm.index(string_partial) + len(string_partial)):])
                    shale_inds = [shale_number - 1]
                
                if 'headProfile' in obs_nm:
                    string_partial = 'headProfile'
                elif 'pressureProfile' in obs_nm:
                    string_partial = 'pressureProfile'
                
                if string_partial + 'All' in obs_nm:
                    profile_inds = range(number_of_profiles)
                else:
                    profile_number = int(
                        obs_nm[len(string_partial):obs_nm.index('VertPoint')])
                    profile_inds = [profile_number - 1]

                # Add the observations
                for profileInd in profile_inds:
                    for vertPointInd in point_inds:
                        for shaleInd in shale_inds:
                            obs_nm_formatted = '{}{}VertPoint{}Shale{}'.format(
                                string_partial, profileInd + 1, vertPointInd + 1, shaleInd + 1)

                            new_comp_outputs.append(obs_nm_formatted)
                            self.add_obs(obs_nm_formatted)

            elif 'Aquifer' in obs_nm:
                if 'AquiferAll' in obs_nm:
                    aquifer_inds = range(numberOfAquiferLayers)
                else:
                    # For specific aquifers, like 'headLocAllAquifer5'
                    string_partial = 'Aquifer'
                    aquifer_number = int(
                        obs_nm[(obs_nm.index(string_partial) + len(string_partial)):])
                    aquifer_inds = [aquifer_number - 1]

                if ('headLoc' in obs_nm or 'pressureLoc' in obs_nm):
                    # For outputs 'headLoc#Aquifer#' or 'pressureLoc#Aquifer#', 
                    # where # may be a number or 'All'.
                    pressure_loc_addition = ''
                    if 'headLoc' in obs_nm:
                        obs_type = 'head'
                    elif 'pressureLoc' in obs_nm:
                        obs_type = 'pressure'
                        
                        if 'MidAquifer' in obs_nm:
                            pressure_loc_addition = 'Mid'
                        elif 'TopAquifer' in obs_nm:
                            pressure_loc_addition = 'Top'

                    if ('headLocAll' in obs_nm or 'pressureLocAll' in obs_nm):
                        profile_inds = range(number_of_profiles)
                    else:
                        string_partial = obs_type + 'Loc'
                        profile_number = int(
                            obs_nm[len(string_partial):obs_nm.index(
                                '{}Aquifer'.format(pressure_loc_addition))])
                        profile_inds = [profile_number - 1]

                    # Add the observations
                    for profileInd in profile_inds:
                        for aquiferInd in aquifer_inds:
                            obs_nm_formatted = '{}Loc{}{}Aquifer{}'.format(
                                obs_type, profileInd + 1, 
                                pressure_loc_addition, aquiferInd + 1)

                            new_comp_outputs.append(obs_nm_formatted)
                            self.add_obs(obs_nm_formatted)

                elif 'well' in obs_nm:
                    # For outputs like 'wellLeakageRateAquifer#',
                    # 'wellLeakageVolumeAquifer#', 'well#LeakageRateAquifer#',
                    # or 'well#LeakageVolumeAquifer#'
                    found_check = False
                    if 'LeakageRate' in obs_nm:
                        metric_type = 'Rate'
                        found_check = True
                    elif 'LeakageVolume' in obs_nm:
                        metric_type = 'Volume'
                        found_check = True

                    if found_check:
                        if 'wellLeakage' in obs_nm:
                            # Add the observations
                            for aquiferInd in aquifer_inds:
                                obs_nm_formatted = 'wellLeakage{}Aquifer{}'.format(
                                    metric_type, aquiferInd + 1)

                                new_comp_outputs.append(obs_nm_formatted)
                                self.add_obs(obs_nm_formatted)
                        else:
                            if 'wellAll' in obs_nm:
                                well_inds = range(number_of_wells)
                            else:
                                string_partial = 'well'
                                well_number = int(
                                    obs_nm[len(string_partial):obs_nm.index('Leakage')])
                                well_inds = [well_number - 1]

                            # Add the observations
                            for aquiferInd in aquifer_inds:
                                for wellInd in well_inds:
                                    obs_nm_formatted = 'well{}Leakage{}Aquifer{}'.format(
                                        wellInd + 1, metric_type, aquiferInd + 1)

                                    new_comp_outputs.append(obs_nm_formatted)
                                    self.add_obs(obs_nm_formatted)
                    else:
                        logging.warning(warn_msg)

                elif 'diffuseLeakage' in obs_nm:
                    found_check = False
                    if 'diffuseLeakageRate' in obs_nm:
                        metric_type = 'Rate'
                        found_check = True
                    elif 'diffuseLeakageVolume' in obs_nm:
                        metric_type = 'Volume'
                        found_check = True

                    if 'TopAquifer' in obs_nm:
                        interface_type = 'Top'
                    elif 'BottomAquifer' in obs_nm:
                        interface_type = 'Bottom'
                    else:
                        found_check = False

                    if found_check:
                        for aquiferInd in aquifer_inds:
                            obs_nm_formatted = 'diffuseLeakage{}{}Aquifer{}'.format(
                                metric_type, interface_type, aquiferInd + 1)

                            new_comp_outputs.append(obs_nm_formatted)
                            self.add_obs(obs_nm_formatted)
                    else:
                        logging.warning(warn_msg)
                else:
                    logging.warning(warn_msg)
            else:
                logging.warning(warn_msg)

        # Add the outputs
        if 'Outputs' in component_data:
            comp_outputs = component_data['Outputs']
            new_comp_outputs = []

            for obs_nm in comp_outputs:
                try:
                    add_salsa_output_cfi(
                        obs_nm, new_comp_outputs, numberOfShaleLayers, 
                        numberOfAquiferLayers, number_of_wells, 
                        number_of_profiles, numberOfVerticalPoints)
                except:
                    err_msg = ''.join([
                        'There was an error while attempting the set the ', 
                        'output {} for the SALSA component {}. The output name ', 
                        'may not have been set up correctly. Check your input.'
                        ]).format(obs_nm, self.name)
                    
                    logging.error(err_msg)

            component_data['Outputs'] = new_comp_outputs

    def check_input_parameters(self, p):
        """
        Check whether input parameters fall within specified boundaries.

        :param p: input parameters of component model
        :type p: dict
        """
        debug_msg = 'Input parameters of component {} are {}'.format(self.name, p)
        logging.debug(debug_msg)


        for key, val in p.items():
            warn_msg = ''.join([
                'Parameter {} of SALSA component {} ',
                'is out of boundaries.']).format(key, self.name)

            if key.startswith('shale') and key.endswith('Thickness'):
                if (val < self.pars_bounds['shaleThickness'][0]) or (
                        val > self.pars_bounds['shaleThickness'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('aquifer') and key.endswith('Thickness'):
                if (val < self.pars_bounds['aquiferThickness'][0]) or (
                        val > self.pars_bounds['aquiferThickness'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('shale') and key.endswith('LogVertK'):
                if (val < self.pars_bounds['shaleLogVertK'][0]) or (
                        val > self.pars_bounds['shaleLogVertK'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('aquifer') and key.endswith('LogHorizK'):
                if (val < self.pars_bounds['aquiferLogHorizK'][0]) or (
                        val > self.pars_bounds['aquiferLogHorizK'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('aquifer') and key.endswith('ANSR'):
                if (val < self.pars_bounds['aquiferANSR'][0]) or (
                        val > self.pars_bounds['aquiferANSR'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('shale') and key.endswith('LogSS'):
                if (val < self.pars_bounds['shaleLogSS'][0]) or (
                        val > self.pars_bounds['shaleLogSS'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('aquifer') and key.endswith('LogSS'):
                if (val < self.pars_bounds['aquiferLogSS'][0]) or (
                        val > self.pars_bounds['aquiferLogSS'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('aquifer') and key.endswith('FluidDensity'):
                if (val < self.pars_bounds['aquiferFluidDensity'][0]) or (
                        val > self.pars_bounds['aquiferFluidDensity'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('shale') and key.endswith('InitialGamma'):
                if (val < self.pars_bounds['shaleInitialGamma'][0]) or (
                        val > self.pars_bounds['shaleInitialGamma'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('shale') and key.endswith('Gamma'):
                if (val < self.pars_bounds['shaleGamma'][0]) or (
                        val > self.pars_bounds['shaleGamma'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('aquifer') and key.endswith('Gamma'):
                if (val < self.pars_bounds['aquiferGamma'][0]) or (
                        val > self.pars_bounds['aquiferGamma'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('activeWell') and 'QPeriod' in key:
                if (val < self.pars_bounds['activeWellQ'][0]) or (
                        val > self.pars_bounds['activeWellQ'][1]):
                    logging.warning(warn_msg)
                continue

            if key.startswith('leakingWell') and 'QPeriod' in key:
                if (val < self.pars_bounds['leakingWellQ'][0]) or (
                        val > self.pars_bounds['leakingWellQ'][1]):
                    logging.warning(warn_msg)
                continue

            if key in self.pars_bounds:
                if ((val < self.pars_bounds[key][0]) or
                        (val > self.pars_bounds[key][1])):
                    logging.warning(warn_msg)

            if key in self.acceptable_vals:
                warn_msg_2 = ''.join([
                    'Parameter {} of SALSA component {} was given as {}. ',
                    'The value provided is not one of the acceptable values ',
                    'for that parameter ({}).']).format(
                        key, self.name, val, self.acceptable_vals[key])

                if not val in self.acceptable_vals[key]:
                    logging.warning(warn_msg_2)

    def get_unit_thicknesses(self):
        """
        Returns lists of the shale and aquifer unit thicknesses. The first entry
        (index 0) is the deepest unit of that type (shale 1 and aquifer 1), while
        the last entry (index -1) is the shallowest. This method should only be
        used after the parameter values have been set.
        """
        numberOfShaleLayers = get_parameter_val(self, 'numberOfShaleLayers')
        numberOfAquiferLayers = self.calc_num_aquifers()

        shaleThicknesses = [None] * numberOfShaleLayers
        aquiferThicknesses = [None] * numberOfAquiferLayers

        for shaleRef in range(numberOfShaleLayers):
            shaleThicknesses[shaleRef] = get_parameter_val(
                self, 'shale{}Thickness'.format(shaleRef + 1))

        for aquRef in range(numberOfAquiferLayers):
            aquiferThicknesses[aquRef] = get_parameter_val(
                self, 'aquifer{}Thickness'.format(aquRef + 1))

        return shaleThicknesses, aquiferThicknesses


    def get_unit_depths(self, shale_thickness_list=None, aquifer_thickness_list=None,
                        bottom_layer_type=None, top_layer_type=None):
        """
        Returns lists of the depths to the bottom, middle, and top of each unit.
        The first entry (index 0) is the deepest unit of that type (shale 1 and
        aquifer 1), while the last entry (index -1) is the shallowest. If the
        keyword arguments are all None, this method should only be used after
        the component has been run. During the simulation_model() method, the
        compile_pars() method assembles lists of shale and aquifer unit
        thicknesses and stores them in the component. Otherwise, this method can
        be used before the model run if each of the keyword arguments are provided.
        """
        kw_arg_none_check = (
            shale_thickness_list is None and aquifer_thickness_list is None
            and bottom_layer_type is None and top_layer_type is None)

        if kw_arg_none_check:
            availability_check = (self.shaleThicknesses is not None
                                  and self.aquiferThicknesses is not None
                                  and self.bottomLayerType is not None
                                  and self.topLayerType is not None)

            if availability_check:
                shaleThicknesses = self.shaleThicknesses
                aquiferThicknesses = self.aquiferThicknesses
                bottomLayerType = self.bottomLayerType
                topLayerType = self.topLayerType

            else:
                err_msg = ''.join([
                    'The get_unit_depths() method of the SALSA component could ',
                    'not run correctly. The keyword arguments shale_thicknesses_list, ',
                    'aquifer_thicknesses_list, bottomLayerType, and topLayerType were ',
                    'all provided as None values. Although these arguments can ',
                    'be obtained from the component after the simulation has ',
                    'been run, the simulation has not been run yet so the required ',
                    'arguments could not be obtained.'])

                logging.error(err_msg)
                raise KeyError(err_msg)
        else:
            shaleThicknesses = shale_thickness_list
            aquiferThicknesses = aquifer_thickness_list
            bottomLayerType = bottom_layer_type
            topLayerType = top_layer_type

        shaleTopDepths = [0] * len(shaleThicknesses)
        aquiferTopDepths = [0] * len(aquiferThicknesses)

        shaleDepths = [0] * len(shaleThicknesses)
        aquiferDepths = [0] * len(aquiferThicknesses)

        shaleMidDepths = [0] * len(shaleThicknesses)
        aquiferMidDepths = [0] * len(aquiferThicknesses)

        if topLayerType == 0:
            shaleTopDepths[-1] = 0
            shaleDepths[-1] = -shaleThicknesses[-1]
            shaleMidDepths[-1] = (shaleDepths[-1] + shaleTopDepths[-1]) / 2
            shaleInd = len(shaleThicknesses) - 1

            for aquInd in range(len(aquiferThicknesses) - 1, -1, -1):
                aquiferTopDepths[aquInd] = shaleDepths[shaleInd]
                aquiferDepths[aquInd] = shaleDepths[shaleInd] - aquiferThicknesses[aquInd]
                aquiferMidDepths[aquInd] = (
                    aquiferDepths[aquInd] + aquiferTopDepths[aquInd]) / 2

                shaleInd -= 1

                if shaleInd >= 0:
                    shaleTopDepths[shaleInd] = aquiferDepths[aquInd]
                    shaleDepths[shaleInd] = shaleTopDepths[shaleInd] - shaleThicknesses[shaleInd]
                    shaleMidDepths[shaleInd] = (
                        shaleDepths[shaleInd] + shaleTopDepths[shaleInd]) / 2

        elif topLayerType == 1:
            aquiferTopDepths[-1] = 0
            aquiferDepths[-1] = -aquiferThicknesses[-1]
            aquiferMidDepths[-1] = (
                aquiferDepths[-1] + aquiferTopDepths[-1]) / 2
            aquInd = len(aquiferThicknesses) - 1

            for shaleInd in range(len(shaleThicknesses) - 1, -1, -1):
                shaleTopDepths[shaleInd] = aquiferDepths[aquInd]
                shaleDepths[shaleInd] = aquiferDepths[aquInd] - shaleThicknesses[shaleInd]
                shaleMidDepths[shaleInd] = (
                    shaleDepths[shaleInd] + shaleTopDepths[shaleInd]) / 2

                aquInd -= 1

                if aquInd >= 0:
                    aquiferTopDepths[aquInd] = shaleDepths[shaleInd]
                    aquiferDepths[aquInd] = aquiferTopDepths[
                        aquInd] - aquiferThicknesses[aquInd]
                    aquiferMidDepths[aquInd] = (
                        aquiferDepths[aquInd] + aquiferTopDepths[aquInd]) / 2

        return shaleDepths, shaleMidDepths, shaleTopDepths, aquiferDepths, \
            aquiferMidDepths, aquiferTopDepths

    def calc_num_aquifers(self):
        """
        Calculates the number of aquifer layers, given the parameters
        numberOfShaleLayers, bottomLayerType, and topLayerType. The set_num_aquifers()
        method is meant to be used with the actual_p dictionary, while this
        method is not. Additionally, set_num_aquifers() assigns the parameter
        value, while this method only returns the calculated value.
        """
        numberOfShaleLayers = get_parameter_val(self, 'numberOfShaleLayers')
        bottomLayerType = get_parameter_val(self, 'bottomLayerType')
        topLayerType = get_parameter_val(self, 'topLayerType')

        numberOfAquiferLayers = (
            numberOfShaleLayers + bottomLayerType + topLayerType - 1)

        return numberOfAquiferLayers

    def set_num_aquifers(self, actual_p):
        """
        The numberOfAquiferLayers parameter is calculated as a function of
        the numberOfShaleLayers, bottomLayerType, and topLayerType parameters.
        If any other value is given to SALSA (called in salsa_ROM.py), an error
        will occur.
        """
        numberOfShaleLayers = actual_p['numberOfShaleLayers']
        bottomLayerType = actual_p['bottomLayerType']
        topLayerType = actual_p['topLayerType']

        numberOfAquiferLayers = (
            numberOfShaleLayers + bottomLayerType + topLayerType - 1)

        # This parameter cannot vary in stochastic simulations
        self.add_par('numberOfAquiferLayers', value=numberOfAquiferLayers, vary=False)

        actual_p['numberOfAquiferLayers'] = numberOfAquiferLayers

        return actual_p

    def compile_pars(self, actual_p):
        """
        Some parameters are given for specific units (e.g., shale3Thickness or
        aquifer2Head). Salsa requires these values to be arraned in lists, so
        this function compiles those lists from the inputs provided.
        """
        numberOfShaleLayers = actual_p['numberOfShaleLayers']
        numberOfAquiferLayers = actual_p['numberOfAquiferLayers']

        self.shaleThickness = np.array([None] * numberOfShaleLayers)
        self.aquiferThickness = np.array([None] * numberOfAquiferLayers)
        self.shaleLogVertK = np.array([None] * numberOfShaleLayers)
        self.aquiferLogHorizK = np.array([None] * numberOfAquiferLayers)
        self.aquiferANSR = np.array([None] * numberOfAquiferLayers)
        self.shaleLogSS = np.array([None] * numberOfShaleLayers)
        self.aquiferLogSS = np.array([None] * numberOfAquiferLayers)
        self.aquiferFluidDensity = np.array([None] * numberOfAquiferLayers)
        self.aquiferHead = np.array([None] * numberOfAquiferLayers)
        self.shaleInitialGamma = np.array([None] * numberOfShaleLayers)
        self.shaleGamma = np.array([None] * numberOfShaleLayers)
        self.aquiferGamma = np.array([None] * numberOfAquiferLayers)

        time_array = self._parent.time_array
        time_array_years = time_array / 365.25

        if self.periodEndTimes is None:
            warning_msg = ''.join([
                'The periodEndTimes keyword argument of the SALSA component ',
                '{} was given as None (not provided). The default approach will ',
                'be used, where there is only one injection period that extends ',
                'to the end of the simulation. This approach may not work, if ',
                'inputs like activeWell#QAquifer#Period# were provided for ',
                'more than one injection period. Check your input.']).format(self.name)

            logging.warning(warning_msg)

            # periodEndTimes is converted to days during the give_props_to_pars()
            # method, before the simulation runs
            self.periodEndTimes = [np.max(time_array_years)]
        else:
            if np.max(self.periodEndTimes) < np.max(time_array_years):
                warning_msg = ''.join([
                    'The periodEndTimes keyword argument was provided to the ',
                    'SALSA component {} as {}. The maximum time included in ',
                    'this input ({} years) was less than the maximum time ',
                    'in the simulation ({} years). This setup may result in ',
                    'errors. Check your input.']).format(
                        self.name, self.periodEndTimes, np.max(self.periodEndTimes),
                        np.max(time_array_years))

                logging.warning(warning_msg)

        if self.activeWellCoordx is None or self.activeWellCoordy is None:
            err_msg = KW_ARG_NOT_GIVEN_COORDS_MSG .format(
                self.name, 'active well', 'activeWellCoordx and activeWellCoordy',
                'InjectionWell')

            logging.error(err_msg)
            raise KeyError(err_msg)

        if self.leakingWellCoordx is None or self.leakingWellCoordy is None:
            err_msg = KW_ARG_NOT_GIVEN_COORDS_MSG .format(
                self.name, 'leaking well', 'leakingWellCoordx and leakingWellCoordy',
                'LeakingWell')

            logging.error(err_msg)
            raise KeyError(err_msg)

        self.activeWellQ = np.zeros((numberOfAquiferLayers,
                                     len(self.activeWellCoordx),
                                     len(self.periodEndTimes)))

        # For all active wells, aquifer 1 has a default injection rate for the
        # first injection period.
        self.activeWellQ[0, :, 0] = self.default_pars['activeWellQ'].value

        self.activeWellRadii = np.ones(
            (numberOfAquiferLayers, len(self.activeWellCoordx))) * self.default_pars[
                'activeWellRadiusAquifer'].value

        self.leakingWellAqRadii = np.ones(
            (len(self.leakingWellCoordx), numberOfAquiferLayers)) * self.default_pars[
                'leakingWellRadiusAquifer'].value

        self.leakingWellAqK = np.ones((
            len(self.leakingWellCoordx), numberOfAquiferLayers)) * (
                10 ** self.default_pars['leakingWellLogKAquifer'].value)

        self.leakingWellShaleRadii = np.ones((
            len(self.leakingWellCoordx), numberOfShaleLayers)) * self.default_pars[
                'leakingWellRadiusShale'].value

        self.leakingWellShaleK = np.ones((
            len(self.leakingWellCoordx), numberOfShaleLayers)) * (
                10 ** self.default_pars['leakingWellLogKShale'].value)

        self.leakingWellAqStat = np.ones((
            len(self.leakingWellCoordx), numberOfAquiferLayers + 1)) * self.default_pars[
                'leakingWellStatAquifer'].value

        # The last column is only 1 if the leaking well is also an active well
        self.leakingWellAqStat[:, -1] *= 0

        self.leakingWellShaleStat = np.ones((
            len(self.leakingWellCoordx), numberOfAquiferLayers)) * self.default_pars[
                'leakingWellStatShale'].value

        self.leakingWellQ = np.ones((
            len(self.leakingWellCoordx), len(self.periodEndTimes))) * self.default_pars[
                'leakingWellQ'].value

        # If a particular well has a nonzero leakingWellQ, then leakingWellQAq
        # is the first screened aquifer layer from the top.
        self.leakingWellQAq = np.ones((len(self.leakingWellCoordx)))

        # These store the values for specific wells / aquifers / shales / periods
        # so the values can be assign after the loop below. Otherwise, the values
        # given for specific cases could be overwritten by values given for
        # multiple cases.
        self.activeWellQ_specific_inds = []
        self.activeWellQ_specific_vals = []
        self.activeWellQ_specific_par_names = []

        self.activeWellRadiusAquifer_specific_inds = []
        self.activeWellRadiusAquifer_specific_vals = []
        self.activeWellRadiusAquifer_specific_par_names = []

        self.leakingWellRadiusAquifer_specific_inds = []
        self.leakingWellRadiusAquifer_specific_vals = []
        self.leakingWellRadiusAquifer_specific_par_names = []

        self.leakingWellKAquifer_specific_inds = []
        self.leakingWellKAquifer_specific_vals = []
        self.leakingWellKAquifer_specific_par_names = []

        self.leakingWellRadiusShale_specific_inds = []
        self.leakingWellRadiusShale_specific_vals = []
        self.leakingWellRadiusShale_specific_par_names = []

        self.leakingWellKShale_specific_inds = []
        self.leakingWellKShale_specific_vals = []
        self.leakingWellKShale_specific_par_names = []

        self.leakingWellStatAquifer_specific_inds = []
        self.leakingWellStatAquifer_specific_vals = []
        self.leakingWellStatAquifer_specific_par_names = []

        self.leakingWellStatShale_specific_inds = []
        self.leakingWellStatShale_specific_vals = []
        self.leakingWellStatShale_specific_par_names = []

        self.leakingWellQ_specific_inds = []
        self.leakingWellQ_specific_vals = []
        self.leakingWellQ_specific_par_names = []

        # These store the specific values for aquifer or shale properties, so they
        # can be assigned further below. This way, they are not overwritten by
        # parameters that apply to all layers.
        self.shaleThickness_specific_inds = []
        self.shaleThickness_specific_vals = []
        self.shaleThickness_specific_par_names = []

        self.aquiferThickness_specific_inds = []
        self.aquiferThickness_specific_vals = []
        self.aquiferThickness_specific_par_names = []

        self.shaleLogVertK_specific_inds = []
        self.shaleLogVertK_specific_vals = []
        self.shaleLogVertK_specific_par_names = []

        self.aquiferLogHorizK_specific_inds = []
        self.aquiferLogHorizK_specific_vals = []
        self.aquiferLogHorizK_specific_par_names = []

        self.aquiferANSR_specific_inds = []
        self.aquiferANSR_specific_vals = []
        self.aquiferANSR_specific_par_names = []

        self.shaleLogSS_specific_inds = []
        self.shaleLogSS_specific_vals = []
        self.shaleLogSS_specific_par_names = []

        self.aquiferLogSS_specific_inds = []
        self.aquiferLogSS_specific_vals = []
        self.aquiferLogSS_specific_par_names = []

        self.aquiferFluidDensity_specific_inds = []
        self.aquiferFluidDensity_specific_vals = []
        self.aquiferFluidDensity_specific_par_names = []

        self.aquiferHead_specific_inds = []
        self.aquiferHead_specific_vals = []
        self.aquiferHead_specific_par_names = []

        self.shaleInitialGamma_specific_inds = []
        self.shaleInitialGamma_specific_vals = []
        self.shaleInitialGamma_specific_par_names = []

        self.shaleGamma_specific_inds = []
        self.shaleGamma_specific_vals = []
        self.shaleGamma_specific_par_names = []

        self.aquiferGamma_specific_inds = []
        self.aquiferGamma_specific_vals = []
        self.aquiferGamma_specific_par_names = []

        # These are the generalized forms of the parameters, instead of ones
        # made for specific units (e.g., shale5Thickness or aquifer3Head). If
        # provided, the value cannot be assigned so the key will be deleted.
        generalized_parameters = [
            'shaleThickness', 'aquiferThickness', 'shaleLogVertK',
            'aquiferLogHorizK', 'aquiferANSR', 'shaleLogSS', 'aquiferLogSS',
            'aquiferFluidDensity', 'aquiferHead', 'shaleInitialGamma',
            'shaleGamma', 'aquiferGamma', 'activeWellQ', 'activeWellRadiusAquifer',
            'leakingWellRadiusAquifer', 'leakingWellLogKAquifer',
            'leakingWellRadiusShale', 'leakingWellLogKShale',
            'leakingWellStatAquifer', 'leakingWellStatShale', 'leakingWellQ']

        parameters_to_skip = ['bottomBoundaryHead', 'topBoundaryHead']

        self.keys_to_delete = []

        # The assignment of parameter values is done in two steps. The first
        # set of parameter values assigned includes parameter values that apply
        # to all features of a type (e.g., all active wells, leaking wells,
        # aquifers, or shales). The next set of parameter values assigned includes
        # parameter values that are given for specific features of a type. The
        # assignment of features that apply to multiple features is done first
        # so that the more specific parameter assignments will overwrite those
        # values. For example, all aquifer#LogHorizK values could set to a value,
        # but then the aquifer#LogHorizK values for a specific aquifer could be
        # set to a different value. For arrays that have multiple dimensions,
        # if one index is given for a specific feature, then that parameter
        # assignment counts as a specific assignment (even if another index
        # applies to all features of a different type).
        for key, val in actual_p.items():
            if key in generalized_parameters:
                self.keys_to_delete.append(key)
                continue

            if key in parameters_to_skip:
                continue

            if key in FIXED_PAR_NAMES:
                # Do not need to do any manipulation for these parameters
                continue

            try:
                _, _ = self.check_and_assign_parameter_input(
                    key, val, numberOfShaleLayers, numberOfAquiferLayers)

            except:
                warning_msg = PAR_UNRECOGNIZED_MSG.format(key, self.name, val)

                logging.warning(warning_msg)

        for key in self.keys_to_delete:
            del actual_p[key]

        def assign_specific_vals(ind_list, val_list, par_name_list,
                                 list_or_array):
            """
            For parameters related to active and leaking wells, used for the
            assignment of parameters given for specific wells / aquifers /
            shales / periods.
            """
            SET_VAL_ERR_MESSAGE = "".join([
                "An error occured while setting the parameter {} of the SALSA ",
                "component {}. The parameter value was given as {}. This error ",
                "can occur if the parameter name includes well numbers, aquifer ",
                "numbers, shale numbers, or injection period numbers that do ",
                "not match the numbers expected given the component's setup. ",
                "The value given will not be used. Check your input."])

            for (ind, val, par_name) in zip(ind_list, val_list, par_name_list):
                err_msg = SET_VAL_ERR_MESSAGE.format(par_name, self.name, val)

                input_len = None
                if isinstance(list_or_array, np.ndarray):
                    input_len = len(list_or_array.shape)

                if isinstance(ind, int) or input_len == 1:
                    index = ind
                    if isinstance(ind, list):
                        index = ind[0]

                    try:
                        list_or_array[index] = val
                    except:
                        logging.error(err_msg)

                elif len(ind) == 2 or input_len == 2:
                    row = ind[0]
                    col = ind[1]

                    # Done to avoid an array being accessed as array[[0, 1]]
                    # when it should be array[0, 1]
                    try:
                        list_or_array[row, col] = val
                    except:
                        logging.error(err_msg)

                elif input_len == 3:
                    row = ind[0]
                    col = ind[1]
                    layer = ind[2]
                    try:
                        list_or_array[row, col, layer] = val
                    except:
                        logging.error(err_msg)

            return list_or_array

        # For the parameters related to the active and leaking wells, assign the
        # values given for specific wells / aquifers / shales / periods. This
        # assignment is done here, so it overwrites any values given for all
        # wells / all aquifers / all shales / all periods. These inputs are
        # assigned during the give_props_to_pars() method.
        self.activeWellQ = assign_specific_vals(
            self.activeWellQ_specific_inds, self.activeWellQ_specific_vals,
            self.activeWellQ_specific_par_names, self.activeWellQ)

        self.activeWellRadii = assign_specific_vals(
            self.activeWellRadiusAquifer_specific_inds,
            self.activeWellRadiusAquifer_specific_vals,
            self.activeWellRadiusAquifer_specific_par_names,
            self.activeWellRadii)

        self.leakingWellAqRadii = assign_specific_vals(
            self.leakingWellRadiusAquifer_specific_inds,
            self.leakingWellRadiusAquifer_specific_vals,
            self.leakingWellRadiusAquifer_specific_par_names,
            self.leakingWellAqRadii)

        self.leakingWellAqK = assign_specific_vals(
            self.leakingWellKAquifer_specific_inds,
            self.leakingWellKAquifer_specific_vals,
            self.leakingWellKAquifer_specific_par_names,
            self.leakingWellAqK)

        self.leakingWellShaleRadii = assign_specific_vals(
            self.leakingWellRadiusShale_specific_inds,
            self.leakingWellRadiusShale_specific_vals,
            self.leakingWellRadiusShale_specific_par_names,
            self.leakingWellShaleRadii)

        self.leakingWellShaleK = assign_specific_vals(
            self.leakingWellKShale_specific_inds,
            self.leakingWellKShale_specific_vals,
            self.leakingWellKShale_specific_par_names,
            self.leakingWellShaleK)

        self.leakingWellAqStat = assign_specific_vals(
            self.leakingWellStatAquifer_specific_inds,
            self.leakingWellStatAquifer_specific_vals,
            self.leakingWellStatAquifer_specific_par_names,
            self.leakingWellAqStat)

        self.leakingWellShaleStat = assign_specific_vals(
            self.leakingWellStatShale_specific_inds,
            self.leakingWellStatShale_specific_vals,
            self.leakingWellStatShale_specific_par_names,
            self.leakingWellShaleStat)

        self.leakingWellQ = assign_specific_vals(
            self.leakingWellQ_specific_inds, self.leakingWellQ_specific_vals,
            self.leakingWellQ_specific_par_names, self.leakingWellQ)

        self.shaleThickness = assign_specific_vals(
            self.shaleThickness_specific_inds, self.shaleThickness_specific_vals,
            self.shaleThickness_specific_par_names, self.shaleThickness)

        self.aquiferThickness = assign_specific_vals(
            self.aquiferThickness_specific_inds, self.aquiferThickness_specific_vals,
            self.aquiferThickness_specific_par_names, self.aquiferThickness)

        self.shaleLogVertK = assign_specific_vals(
            self.shaleLogVertK_specific_inds, self.shaleLogVertK_specific_vals,
            self.shaleLogVertK_specific_par_names, self.shaleLogVertK)

        self.aquiferLogHorizK = assign_specific_vals(
            self.aquiferLogHorizK_specific_inds, self.aquiferLogHorizK_specific_vals,
            self.aquiferLogHorizK_specific_par_names, self.aquiferLogHorizK)

        self.aquiferANSR = assign_specific_vals(
            self.aquiferANSR_specific_inds, self.aquiferANSR_specific_vals,
            self.aquiferANSR_specific_par_names, self.aquiferANSR)

        self.shaleLogSS = assign_specific_vals(
            self.shaleLogSS_specific_inds, self.shaleLogSS_specific_vals,
            self.shaleLogSS_specific_par_names, self.shaleLogSS)

        self.aquiferLogSS = assign_specific_vals(
            self.aquiferLogSS_specific_inds, self.aquiferLogSS_specific_vals,
            self.aquiferLogSS_specific_par_names, self.aquiferLogSS)

        self.aquiferFluidDensity = assign_specific_vals(
            self.aquiferFluidDensity_specific_inds, self.aquiferFluidDensity_specific_vals,
            self.aquiferFluidDensity_specific_par_names, self.aquiferFluidDensity)

        self.aquiferHead = assign_specific_vals(
            self.aquiferHead_specific_inds, self.aquiferHead_specific_vals,
            self.aquiferHead_specific_par_names, self.aquiferHead)

        self.shaleInitialGamma = assign_specific_vals(
            self.shaleInitialGamma_specific_inds, self.shaleInitialGamma_specific_vals,
            self.shaleInitialGamma_specific_par_names, self.shaleInitialGamma)

        self.shaleGamma = assign_specific_vals(
            self.shaleGamma_specific_inds, self.shaleGamma_specific_vals,
            self.shaleGamma_specific_par_names, self.shaleGamma)

        self.aquiferGamma = assign_specific_vals(
            self.aquiferGamma_specific_inds, self.aquiferGamma_specific_vals,
            self.aquiferGamma_specific_par_names, self.aquiferGamma)

        # Assign default values to any undefined parameters
        self.shaleThickness = self.assign_default_pars(
            self.shaleThickness, 'shaleThickness', 'shale{}Thickness')

        self.aquiferThickness = self.assign_default_pars(
            self.aquiferThickness, 'aquiferThickness', 'aquifer{}Thickness')

        self.shaleLogVertK = self.assign_default_pars(
            self.shaleLogVertK, 'shaleLogVertK', 'shale{}LogVertK')

        self.aquiferLogHorizK = self.assign_default_pars(
            self.aquiferLogHorizK, 'aquiferLogHorizK', 'aquifer{}LogHorizK')

        self.aquiferANSR = self.assign_default_pars(
            self.aquiferANSR, 'aquiferANSR', 'aquifer{}ANSR')

        self.shaleLogSS = self.assign_default_pars(
            self.shaleLogSS, 'shaleLogSS', 'shale{}LogSS')

        self.aquiferLogSS = self.assign_default_pars(
            self.aquiferLogSS, 'aquiferLogSS', 'aquifer{}LogSS')

        self.aquiferFluidDensity = self.assign_default_pars(
            self.aquiferFluidDensity, 'aquiferFluidDensity', 'aquifer{}FluidDensity')

        self.aquiferHead = self.assign_default_pars(
            self.aquiferHead, 'aquiferHead', 'aquifer{}Head')

        self.shaleInitialGamma = self.assign_default_pars(
            self.shaleInitialGamma, 'shaleInitialGamma', 'shale{}InitialGamma')

        self.shaleGamma = self.assign_default_pars(
            self.shaleGamma, 'shaleGamma', 'shale{}Gamma')

        self.aquiferGamma = self.assign_default_pars(
            self.aquiferGamma, 'aquiferGamma', 'aquifer{}Gamma')

        # This property is set according to self.leakingWellQ and self.leakingWellAqStat
        for leakingWellRef in range(len(self.leakingWellCoordx)):
            zero_check = (np.min(self.leakingWellQ[leakingWellRef, :]) == 0
                          and np.max(self.leakingWellQ[leakingWellRef, :]) == 0)

            if not zero_check:
                # If the leaking well is also an active well, the last column
                # of self.leakingWellAqStat must be set to 1.
                self.leakingWellAqStat[leakingWellRef, -1] = 1

                highest_screened_aq = None

                # Find the highest screened aquifer from self.leakingWellAqStat
                for aquRef in range(numberOfAquiferLayers):
                    if self.leakingWellAqStat[leakingWellRef, aquRef] == 1:
                        highest_screened_aq = aquRef

                if not highest_screened_aq is None:
                    self.leakingWellQAq[leakingWellRef] = highest_screened_aq

        # For the current version, the aquiferANSR values must be the same in
        # each aquifer. If a future version allows the model type to vary, this
        # requirement will be removed for certain model types. Specifically, if
        # the aquitards are completely impermeable, then aquiferANSR can vary.
        # In the current version, the aquitards always have a permeability.
        if ENFORCE_UNIFORM_AQUIFER_ANSR:
            if np.min(self.aquiferANSR) != np.max(self.aquiferANSR):
                warning_msg = ''.join([
                    'For the SALSA component {}, the aquifers had different ',
                    'aquifer#ANSR values ({}, for aquifers 1 through {}). In ',
                    'the current version of this component, aquifer#ANSR values ',
                    'must always be the same in all aquifers. These values can ',
                    'only vary if the aquitards are completely impermeable, ',
                    'which is not allowed in the current version. ALl ',
                    'aquifer#ANSR values will be set to the default value of {}.'
                    ]).format(self.name, self.aquiferANSR, numberOfAquiferLayers,
                              self.default_pars['aquiferANSR'].value)
                logging.warning(warning_msg)

                self.aquiferANSR = np.ones(
                    numberOfAquiferLayers) * self.default_pars['aquiferANSR'].value

        # Store the parameter lists
        actual_p['shaleThickness'] = self.shaleThickness
        actual_p['aquiferThickness'] = self.aquiferThickness
        actual_p['shaleLogVertK'] = self.shaleLogVertK
        actual_p['aquiferLogHorizK'] = self.aquiferLogHorizK
        actual_p['aquiferANSR'] = self.aquiferANSR
        actual_p['shaleLogSS'] = self.shaleLogSS
        actual_p['aquiferLogSS'] = self.aquiferLogSS
        actual_p['aquiferFluidDensity'] = self.aquiferFluidDensity
        actual_p['aquiferHead'] = self.aquiferHead
        actual_p['shaleInitialGamma'] = self.shaleInitialGamma
        actual_p['shaleGamma'] = self.shaleGamma
        actual_p['aquiferGamma'] = self.aquiferGamma

        # Store the stratigraphy information in the component so depths can be
        # calculated conveniently
        self.shaleThicknesses = self.shaleThickness.copy()
        self.aquiferThicknesses = self.aquiferThickness.copy()
        self.bottomLayerType = actual_p['bottomLayerType']
        self.topLayerType = actual_p['topLayerType']

        # Remove the '_specific_inds', '_specific_vals', and '_specific_par_names'
        # properties.
        self.clean_properties()

        return actual_p

    def check_parameter_bounds(self, par_name, val, par_key, acceptable_vals=False):
        """
        Checks if parameter input exceeds the range for the parameter type. If
        so, set the value to the nearest limit. This method is necessary because
        the parameter naming convention of the SALSA component can prevent a
        parameter name from being recognized (indices for different wells,
        aquifers, shales, and periods). Here, par_name is the parameter name as
        given by the user, val is the value provided by the user, and par_key
        is the key for the parameter type's limits in the pars_bounds dictionary.
        If acceptable_vals is True, the method checks if the value is acceptable
        for that parameter type (generally limited to values of 0 or 1).
        """
        val_acceptable_warning = ''.join([
            'The parameter {} of the SALSA component {} was given with a ',
            'value of {}, which is not an acceptable value for that parameter ',
            'type ({}). The default value of {} will be used.'])

        val_limit_warning = ''.join([
            'The parameter {} of the SALSA component {} was given with a ',
            'value of {}, which is {} than the {} limit for that parameter ',
            'type ({}). The value will be set to that limit.'])

        par_recognized_warning = ''.join([
            'The parameter {} was given for the SALSA component {} with a ',
            'value of {}, but the parameter type ({}) was not recognized ',
            'as a parameter type of the SALSA component. Therefore, the ',
            'value cannot be compared with the parameter limits of SALSA.'])

        if acceptable_vals:
            # In this case, the parameter is only allowed to have specific
            # values, generally 0 or 1.
            if par_key in self.acceptable_vals:
                if par_key in self.acceptable_vals and par_key in self.default_pars:
                    if not val in self.acceptable_vals[par_key]:
                        warning_msg = val_acceptable_warning.format(
                            par_name, self.name, val, par_key, self.default_pars[par_key])

                        logging.warning(warning_msg)

                        val = self.default_pars[par_key].value
                else:
                    warning_msg = par_recognized_warning.format(
                        par_name, self.name, val, par_key)
            else:
                warning_msg = par_recognized_warning.format(
                    par_name, self.name, val, par_key)

        else:
            if par_key in self.pars_bounds:
                if val < self.pars_bounds[par_key][0]:
                    warning_msg = val_limit_warning.format(
                        par_name, self.name, val, 'less', 'lower',
                        self.pars_bounds[par_key][0])

                    logging.warning(warning_msg)

                    val = self.pars_bounds[par_key][0]

                elif val > self.pars_bounds[par_key][1]:
                    warning_msg = val_limit_warning.format(
                        par_name, self.name, val, 'greater', 'upper',
                        self.pars_bounds[par_key][1])

                    logging.warning(warning_msg)

                    val = self.pars_bounds[par_key][1]

            else:
                warning_msg = par_recognized_warning.format(
                    par_name, self.name, val, par_key)

                logging.warning(warning_msg)

        return val

    def check_par_key_values(self, par_name, par_input_dict, par_base_name,
                             acceptable_vals=False):
        """
        Goes through the parameter input dictionary (val) and compares the
        inputs to the parameter limits. IN check_and_assign_parameter_input,
        this method is only meant to be used when return_par_input is True.
        """
        keys_to_check = ['value', 'min', 'max']
        for key in par_input_dict.keys():
            if key == 'discrete_vals':
                # The discrete_vals key is given a tuple where the first
                # element contains values and the second element contains
                # probabilities
                discrete_vals = par_input_dict[key][0]

                for ind, disc_val in enumerate(discrete_vals):
                    disc_val = self.check_parameter_bounds(
                        par_name, disc_val, par_base_name,
                        acceptable_vals=acceptable_vals)

                    par_input_dict[key][0][ind] = disc_val

            elif key in keys_to_check:
                par_input_dict[key] = self.check_parameter_bounds(
                    par_name, par_input_dict[key], par_base_name,
                    acceptable_vals=acceptable_vals)

        return par_input_dict

    def check_and_assign_parameter_input(self, key, val, numberOfShaleLayers,
                                         numberOfAquiferLayers, return_par_input=False):
        """
        Checks the parameter name provided in actual_p during compile_pars().
        Attempts to find a matching parameter name and assign the value given
        (val). This step is handled with a separate method so that any error
        that occurs can be handled more easily (e.g., a parameter name given
        with incorrect formatting and/or spelling). This method is also used in
        the connect_with_system() method with return_par_input set to True.
        If return_par_input is True, this method returns a list of formatted
        parameter names as well as the input provided (val). When return_par_input
        is True, val is expected to be a dictionary containing the key 'value'
        and potentially the keys 'min' and 'max'. If return_par_input is False,
        this method returns two 1 values if the parameter assignment
        (to the input arrays, not to the component itself with .add_par()) is
        successfull. In either case, the function returns two 0 values if the
        parameter name is unrecognized.
        """
        if return_par_input:
            if not isinstance(val, dict):
                val = {'value': val, 'vary': False}

        par_names = []
        par_input = None

        endings = ['Thickness', 'LogVertK', 'LogHorizK', 'ANSR', 'LogSS',
                   'FluidDensity', 'Head', 'InitialGamma', 'Gamma']

        # partial names of parameters related to active and leaking wells
        well_partial_parameters = [
            'QAquifer', 'RadiusAquifer', 'RadiusShale', 'LogKAquifer', 'LogKShale',
            'StatAquifer', 'StatShale', 'QPeriod']

        def get_par_names(par_name_form, indices):
            """
            Given a parameter name format (par_name_form) and a list of indices,
            returns a list of formatted parameter names.
            """
            par_names = []

            array_len = len(indices)

            first_indices = ['']
            second_indices = ['']
            third_indices = ['']
            if array_len >= 1:
                first_indices = indices[0]

                if not isinstance(first_indices, (range, list)):
                    first_indices = [first_indices]

            if array_len >= 2:
                second_indices = indices[1]

                if not isinstance(second_indices, (range, list)):
                    second_indices = [second_indices]

            if array_len >= 3:
                third_indices = indices[2]

                if not isinstance(third_indices, (range, list)):
                    third_indices = [third_indices]

            if array_len == 1:
                for ind1 in first_indices:
                    par_name = par_name_form.format(ind1 + 1)
                    par_names.append(par_name)

            elif array_len == 2:
                for ind1 in first_indices:
                    for ind2 in second_indices:
                        par_name = par_name_form.format(ind1 + 1, ind2 + 1)
                        par_names.append(par_name)

            elif array_len == 3:
                for ind1 in first_indices:
                    for ind2 in second_indices:
                        for ind3 in third_indices:
                            par_name = par_name_form.format(
                                ind1 + 1, ind2 + 1, ind3 + 1)
                            par_names.append(par_name)

            return par_names

        def set_array_values(indices, val, array):
            """
            Used to set values into an array and then return that array. This
            approach is necessary because there can be problems when setting
            values for the arrays with 2 or more dimensions. Specifically, if
            multiple indices are set as ranges, then setting a value with those
            ranges can cause an error (e.g., array[range(3), range(4)]  = val).
            To avoid that problem, this function uses for loops.
            """
            ind1 = None
            ind2 = None
            ind3 = None

            array_len = len(indices)

            if array_len  >= 1:
                ind1 = indices[0]

                if not isinstance(ind1, (range, list)):
                    ind1 = [ind1]

            if array_len  >= 2:
                ind2 = indices[1]

                if not isinstance(ind2, (range, list)):
                    ind2 = [ind2]

            if array_len  >= 3:
                ind3 = indices[2]

                if not isinstance(ind3, (range, list)):
                    ind3 = [ind3]

            if array_len == 1:
                for Index1 in ind1:
                    array[Index1] = val

            elif array_len == 2:
                for Index1 in ind1:
                    for Index2 in ind2:
                        array[Index1, Index2] = val

            elif array_len == 3:
                for Index1 in ind1:
                    for Index2 in ind2:
                        for Index3 in ind3:
                            array[Index1, Index2, Index3] = val

            return array

        # Used to check if the parameter was identified as one for the active
        # and leaking wells (well_partial_parameters). If it is not, check
        # if it applies to the aquifers and shales.
        continue_test = True
        for partial_par in well_partial_parameters:
            if partial_par in key and continue_test:
                specific_check = False

                if 'activeWell' in key:
                    string_partial = 'activeWell'

                    if 'QAquifer' in key and 'Period' in key:
                        # For parameter names activeWell#QAquifer#Period#
                        if return_par_input:
                            val = self.check_par_key_values(key, val, 'activeWellQ')
                        else:
                            val = self.check_parameter_bounds(key, val, 'activeWellQ')

                        qaquifer_index = key.index('QAquifer')
                        well_str = key[len(string_partial):qaquifer_index]

                        if well_str == 'All':
                            well_ind = range(len(self.activeWellCoordx))
                        else:
                            well_ind = int(well_str) - 1
                            specific_check = True

                        string_partial = 'Period'
                        period_index = key.index(string_partial)
                        period_str = key[(period_index + len(string_partial)):]

                        if period_str == 'All':
                            period_ind = range(len(self.periodEndTimes))
                        else:
                            period_ind = int(period_str) - 1
                            specific_check = True

                        string_partial = 'QAquifer'
                        aquifer_str = key[
                            (qaquifer_index + len(string_partial)):period_index]

                        if aquifer_str == 'All':
                            aquifer_ind = range(numberOfAquiferLayers)
                        else:
                            aquifer_ind = int(aquifer_str) - 1
                            specific_check = True

                        if return_par_input:
                            indices = [well_ind, aquifer_ind, period_ind]

                            par_input = val
                            par_name_form = 'activeWell{}QAquifer{}Period{}'
                            par_names = get_par_names(par_name_form, indices)

                        else:
                            indices = [aquifer_ind, well_ind, period_ind]

                            if not specific_check:
                                self.activeWellQ = set_array_values(
                                    indices, val, self.activeWellQ)

                            elif specific_check:
                                # Set the value further below, so that it is not
                                # overwritten by a parameter that applies to
                                # multiple cases
                                self.activeWellQ_specific_inds.append(indices)
                                self.activeWellQ_specific_vals.append(val)
                                self.activeWellQ_specific_par_names.append(key)

                        continue_test = False

                    elif 'RadiusAquifer' in key:
                        # For parameter names activeWell#RadiusAquifer#
                        if return_par_input:
                            val = self.check_par_key_values(key, val, 'activeWellRadiusAquifer')
                        else:
                            val = self.check_parameter_bounds(key, val, 'activeWellRadiusAquifer')

                        radiusaquifer_index = key.index('RadiusAquifer')
                        well_str = key[len(string_partial):radiusaquifer_index]

                        if well_str == 'All':
                            well_ind = range(len(self.activeWellCoordx))
                        else:
                            well_ind = int(well_str) - 1
                            specific_check = True

                        string_partial = 'RadiusAquifer'
                        aquifer_str = key[(radiusaquifer_index + len(string_partial)):]

                        if aquifer_str == 'All':
                            aquifer_ind = range(numberOfAquiferLayers)
                        else:
                            aquifer_ind = int(aquifer_str) - 1
                            specific_check = True

                        if return_par_input:
                            indices = [well_ind, aquifer_ind]

                            par_input = val
                            par_name_form = 'activeWell{}RadiusAquifer{}'
                            par_names = get_par_names(par_name_form, indices)

                        else:
                            indices = [aquifer_ind, well_ind]

                            if not specific_check:
                                self.activeWellRadii = set_array_values(
                                    indices, val, self.activeWellRadii)

                            elif specific_check:
                                self.activeWellRadiusAquifer_specific_inds.append(
                                    indices)
                                self.activeWellRadiusAquifer_specific_vals.append(
                                    val)
                                self.activeWellRadiusAquifer_specific_par_names.append(
                                    key)

                        continue_test = False

                elif 'leakingWell' in key:
                    string_partial = 'leakingWell'

                    if 'LogK' in key:
                        # Separate treatments here, in case the limits are changed
                        if 'Aquifer' in key:
                            if return_par_input:
                                val = self.check_par_key_values(
                                    key, val, 'leakingWellLogKAquifer')
                            else:
                                val = self.check_parameter_bounds(
                                    key, val, 'leakingWellLogKAquifer')

                        elif 'Shale' in key:
                            if return_par_input:
                                val = self.check_par_key_values(
                                    key, val, 'leakingWellLogKShale')
                            else:
                                val = self.check_parameter_bounds(
                                    key, val, 'leakingWellLogKShale')

                        # For the parameters leakingWell#LogKAquifer# and
                        # leakingWell#LogKShale#, handle the conversion from
                        # log10 m/s to m/day. Only do this when setting values
                        # in the arrays, not when returning parameter inputs.
                        if not return_par_input:
                            val = (10 ** val) * SEC_PER_DAY

                    if 'RadiusAquifer' in key:
                        # For parameter names leakingWell#RadiusAquifer#
                        if return_par_input:
                            val = self.check_par_key_values(
                                key, val, 'leakingWellRadiusAquifer')
                        else:
                            val = self.check_parameter_bounds(
                                key, val, 'leakingWellRadiusAquifer')

                        radiusaquifer_index = key.index('RadiusAquifer')
                        well_str = key[len(string_partial):radiusaquifer_index]

                        if well_str == 'All':
                            well_ind = range(len(self.leakingWellCoordx))
                        else:
                            well_ind = int(well_str) - 1
                            specific_check = True

                        string_partial = 'RadiusAquifer'
                        aquifer_str = key[(radiusaquifer_index + len(string_partial)):]

                        if aquifer_str == 'All':
                            aquifer_ind = range(numberOfAquiferLayers)
                        else:
                            aquifer_ind = int(aquifer_str) - 1
                            specific_check = True

                        indices = [well_ind, aquifer_ind]

                        if return_par_input:
                            par_input = val
                            par_name_form = 'leakingWell{}RadiusAquifer{}'
                            par_names = get_par_names(par_name_form, indices)

                        else:
                            if not specific_check:
                                self.leakingWellAqRadii = set_array_values(
                                    indices, val, self.leakingWellAqRadii)

                            elif specific_check:
                                self.leakingWellRadiusAquifer_specific_inds.append(
                                    indices)
                                self.leakingWellRadiusAquifer_specific_vals.append(
                                    val)
                                self.leakingWellRadiusAquifer_specific_par_names.append(
                                    key)

                        continue_test = False

                    elif 'LogKAquifer' in key:
                        # For parameter names leakingWell#LogKAquifer#
                        string_partial = 'LogKAquifer'
                        kaquifer_index = key.index(string_partial)
                        string_partial = 'leakingWell'
                        well_str = key[len(string_partial):kaquifer_index]

                        if well_str == 'All':
                            well_ind = range(len(self.leakingWellCoordx))
                        else:
                            well_ind = int(well_str) - 1
                            specific_check = True

                        string_partial = 'LogKAquifer'
                        aquifer_str = key[(kaquifer_index + len(string_partial)):]

                        if aquifer_str == 'All':
                            aquifer_ind = range(numberOfAquiferLayers)
                        else:
                            aquifer_ind = int(aquifer_str) - 1
                            specific_check = True

                        indices = [well_ind, aquifer_ind]

                        if return_par_input:
                            par_input = val
                            par_name_form = 'leakingWell{}LogKAquifer{}'
                            par_names = get_par_names(par_name_form, indices)

                        else:
                            if not specific_check:
                                self.leakingWellAqK = set_array_values(
                                    indices, val, self.leakingWellAqK)

                            elif specific_check:
                                self.leakingWellKAquifer_specific_inds.append(
                                    indices)
                                self.leakingWellKAquifer_specific_vals.append(
                                    val)
                                self.leakingWellKAquifer_specific_par_names.append(
                                    key)

                        continue_test = False

                    elif 'RadiusShale' in key:
                        # For parameter names leakingWell#RadiusShale#
                        if return_par_input:
                            val = self.check_par_key_values(
                                key, val, 'leakingWellRadiusShale')
                        else:
                            val = self.check_parameter_bounds(
                                key, val, 'leakingWellRadiusShale')

                        radiusshale_index = key.index('RadiusShale')
                        well_str = key[len(string_partial):radiusshale_index]

                        if well_str == 'All':
                            well_ind = range(len(self.leakingWellCoordx))
                        else:
                            well_ind = int(well_str) - 1
                            specific_check = True

                        string_partial = 'RadiusShale'
                        shale_str = key[(radiusshale_index + len(string_partial)):]

                        if shale_str == 'All':
                            shale_ind = range(numberOfShaleLayers)
                        else:
                            shale_ind = int(shale_str) - 1
                            specific_check = True

                        indices = [well_ind, shale_ind]

                        if return_par_input:
                            par_input = val
                            par_name_form = 'leakingWell{}RadiusShale{}'
                            par_names = get_par_names(par_name_form, indices)

                        else:
                            if not specific_check:
                                self.leakingWellShaleRadii = set_array_values(
                                    indices, val, self.leakingWellShaleRadii)

                            elif specific_check:
                                self.leakingWellRadiusShale_specific_inds.append(
                                    indices)
                                self.leakingWellRadiusShale_specific_vals.append(
                                    val)
                                self.leakingWellRadiusShale_specific_par_names.append(
                                    key)

                        continue_test = False

                    elif 'LogKShale' in key:
                        # For parameter names leakingWell#LogKShale#
                        string_partial = 'LogKShale'
                        kshale_index = key.index(string_partial)
                        string_partial = 'leakingWell'
                        well_str = key[len(string_partial):kshale_index]

                        if well_str == 'All':
                            well_ind = range(len(self.leakingWellCoordx))
                        else:
                            well_ind = int(well_str) - 1
                            specific_check = True

                        string_partial = 'LogKShale'
                        shale_str = key[(kshale_index + len(string_partial)):]

                        if shale_str == 'All':
                            shale_ind = range(numberOfShaleLayers)
                        else:
                            shale_ind = int(shale_str) - 1
                            specific_check = True

                        indices = [well_ind, shale_ind]

                        if return_par_input:
                            par_input = val
                            par_name_form = 'leakingWell{}LogKShale{}'
                            par_names = get_par_names(par_name_form, indices)

                        else:
                            if not specific_check:
                                self.leakingWellShaleK = set_array_values(
                                    indices, val, self.leakingWellShaleK)

                            elif specific_check:
                                self.leakingWellKShale_specific_inds.append(
                                    indices)
                                self.leakingWellKShale_specific_vals.append(
                                    val)
                                self.leakingWellKShale_specific_par_names.append(
                                    key)

                        continue_test = False

                    elif 'StatAquifer' in key:
                        # For parameter names leakingWell#StatAquifer#
                        if return_par_input:
                            val = self.check_par_key_values(
                                key, val, 'leakingWellStatAquifer', acceptable_vals=True)
                        else:
                            val = self.check_parameter_bounds(
                                key, val, 'leakingWellStatAquifer', acceptable_vals=True)

                        stataquifer_index = key.index('StatAquifer')
                        well_str = key[len(string_partial):stataquifer_index]

                        if well_str == 'All':
                            well_ind = range(len(self.leakingWellCoordx))
                        else:
                            well_ind = int(well_str) - 1
                            specific_check = True

                        string_partial = 'StatAquifer'
                        aquifer_str = key[(stataquifer_index + len(string_partial)):]

                        if aquifer_str == 'All':
                            aquifer_ind = range(numberOfAquiferLayers)
                        else:
                            aquifer_ind = int(aquifer_str) - 1
                            specific_check = True

                        indices = [well_ind, aquifer_ind]

                        if return_par_input:
                            par_input = val
                            par_name_form = 'leakingWell{}StatAquifer{}'
                            par_names = get_par_names(par_name_form, indices)

                        else:
                            if not specific_check:
                                self.leakingWellAqStat = set_array_values(
                                    indices, val, self.leakingWellAqStat)

                            elif specific_check:
                                self.leakingWellStatAquifer_specific_inds.append(
                                    indices)
                                self.leakingWellStatAquifer_specific_vals.append(
                                    val)
                                self.leakingWellStatAquifer_specific_par_names.append(
                                    key)

                        continue_test = False

                    elif 'StatShale' in key:
                        # For parameter names leakingWell#StatShale#
                        if return_par_input:
                            val = self.check_par_key_values(
                                key, val, 'leakingWellStatShale', acceptable_vals=True)
                        else:
                            val = self.check_parameter_bounds(
                                key, val, 'leakingWellStatShale', acceptable_vals=True)

                        statshale_index = key.index('StatShale')
                        well_str = key[len(string_partial):statshale_index]

                        if well_str == 'All':
                            well_ind = range(len(self.leakingWellCoordx))
                        else:
                            well_ind = int(well_str) - 1
                            specific_check = True

                        string_partial = 'StatShale'
                        shale_str = key[(statshale_index + len(string_partial)):]

                        use_value = True
                        if shale_str == 'All':
                            # Even though this parameter applies to the
                            # shale layers, the number of parameters of this
                            # type scales with the number of aquifer layers
                            # because it can only be used for a shale with
                            # an aquifer above it.
                            shale_ind = range(numberOfAquiferLayers)
                            
                        else:
                            shale_ind = int(shale_str) - 1
                            
                            specific_check = True

                            if (shale_ind + 1) > numberOfAquiferLayers:
                                warning_msg = ''.join([
                                    'The parameter {} was given for the SALSA ',
                                    'component {}. Parameters of the type ',
                                    'leakingWell#StatShale# can only be given ',
                                    'for a shale that has an aquifer situated ',
                                    'above it. The unit index provided with ',
                                    'the parameter name (shale {}) does ',
                                    'not match this requirement, so the ',
                                    'value provided ({}) will not be used.'
                                    ]).format(key, self.name, shale_ind + 1,
                                              val)

                                logging.warning(warning_msg)
                                use_value = False

                        if use_value:
                            indices = [well_ind, shale_ind]

                            if return_par_input:
                                par_input = val
                                par_name_form = 'leakingWell{}StatShale{}'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.leakingWellShaleStat = set_array_values(
                                        indices, val, self.leakingWellShaleStat)

                                elif specific_check:
                                    self.leakingWellStatShale_specific_inds.append(
                                        indices)
                                    self.leakingWellStatShale_specific_vals.append(
                                        val)
                                    self.leakingWellStatShale_specific_par_names.append(
                                        key)

                        continue_test = False

                    elif 'QPeriod' in key:
                        # For parameter names leakingWell#QPeriod#
                        if return_par_input:
                            val = self.check_par_key_values(
                                key, val, 'leakingWellQ')
                        else:
                            val = self.check_parameter_bounds(
                                key, val, 'leakingWellQ')

                        qperiod_index = key.index('QPeriod')
                        well_str = key[len(string_partial):qperiod_index]

                        if well_str == 'All':
                            well_ind = range(len(self.leakingWellCoordx))
                        else:
                            well_ind = int(well_str) - 1
                            specific_check = True

                        string_partial = 'QPeriod'
                        period_str = key[(qperiod_index + len(string_partial)):]

                        if period_str == 'All':
                            period_ind = range(len(self.periodEndTimes))
                        else:
                            period_ind = int(period_str) - 1
                            specific_check = True

                        indices = [well_ind, period_ind]

                        if return_par_input:
                            par_input = val
                            par_name_form = 'leakingWell{}QPeriod{}'
                            par_names = get_par_names(par_name_form, indices)

                        else:
                            if not specific_check:
                                self.leakingWellQ = set_array_values(
                                    indices, val, self.leakingWellQ)

                            elif specific_check:
                                self.leakingWellQ_specific_inds.append(
                                    indices)
                                self.leakingWellQ_specific_vals.append(
                                    val)
                                self.leakingWellQ_specific_par_names.append(
                                    key)

                        continue_test = False

            # If continue_test has been set to False, the correct parameter name
            # has already been found.
            if not continue_test:
                continue

        # If the parameter was not identified as one for the active and leaking
        # wells, check if it matches one expected for an aquifer or shale.
        if continue_test:
            if key.startswith('shale'):
                start = 'shale'
            elif key.startswith('aquifer'):
                start = 'aquifer'
            else:
                start = None

            if start is not None:
                # Used to check if the parameter applies to all shales or aquifers
                # (False) or to one specific shale or aquifer (True). If True,
                # the value is assigned further below so it is not overwritten
                # by a parameter that applies to all shales or aquifers.
                specific_check = False
                for end in endings:
                    if key.endswith(end) and continue_test:
                        # Without this check, the Gamma and InitialGamma parameters
                        # might be repeated The 'Gamma' ending is handled after the
                        # 'InitialGamma' ending, so this case can be skipped.
                        if end == 'Gamma' and key.endswith('InitialGamma'):
                            continue

                        if 'Gamma' in key:
                            if 'shale' in key and 'InitialGamma' in key:
                                if return_par_input:
                                    val = self.check_par_key_values(
                                        key, val, 'shaleInitialGamma')
                                else:
                                    val = self.check_parameter_bounds(
                                        key, val, 'shaleInitialGamma')

                            elif 'shale' in key and 'Gamma' in key:
                                if return_par_input:
                                    val = self.check_par_key_values(
                                        key, val, 'shaleGamma')
                                else:
                                    val = self.check_parameter_bounds(
                                        key, val, 'shaleGamma')

                            elif 'aquifer' in key and 'Gamma' in key:
                                if return_par_input:
                                    val = self.check_par_key_values(
                                        key, val, 'aquiferGamma')
                                else:
                                    val = self.check_parameter_bounds(
                                        key, val, 'aquiferGamma')

                            # For the shale#InitialGamma, shale#Gamma, and
                            # aquifer#Gamma parameters, convert from 1/s to 1/day.
                            # Only do this when setting values in the arrays, not
                            # when returning parameter input.
                            if not return_par_input:
                                val *= SEC_PER_DAY

                        lower_bound_1dig = -(len(start) + 1 + len(end))
                        upper_bound_1dig = -(1 + len(end))

                        val_index_1dig = upper_bound_1dig

                        lower_bound_2dig = -(len(start) + 2 + len(end))
                        upper_bound_2dig = -(2 + len(end))

                        val_indices_2dig = [upper_bound_2dig, -((-upper_bound_2dig) - 2)]

                        lower_bound_3dig = -(len(start) + 3 + len(end))
                        upper_bound_3dig = -(3 + len(end))

                        val_indices_3dig = [upper_bound_3dig, -((-upper_bound_3dig) - 3)]

                        if key[lower_bound_1dig:upper_bound_1dig] == start:
                            # For one digit, like 'shale3Thickness'
                            index = int(key[val_index_1dig]) - 1
                            specific_check = True
                        elif key[lower_bound_2dig:upper_bound_2dig] == start:
                            # For two digits, like 'shale15Thickness'
                            index = int(key[val_indices_2dig[
                                0]:val_indices_2dig[1]]) - 1
                            specific_check = True
                        elif key[lower_bound_3dig:upper_bound_3dig] == start:
                            # For three digits like 'aquifer112Thickness'
                            # (that case should not happen) OR 'All'
                            index_str = key[val_indices_3dig[0]:val_indices_3dig[1]]

                            if index_str == 'All':
                                if start == 'shale':
                                    index = range(numberOfShaleLayers)
                                elif start == 'aquifer':
                                    index = range(numberOfAquiferLayers)
                            else:
                                index = int(index_str) - 1
                                specific_check = True

                        indices = [index]

                        if start == 'shale' and end == 'Thickness':
                            if return_par_input:
                                val = self.check_par_key_values(key, val, 'shaleThickness')
                            else:
                                val = self.check_parameter_bounds(key, val, 'shaleThickness')

                            if return_par_input:
                                par_input = val
                                par_name_form = 'shale{}Thickness'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.shaleThickness = set_array_values(
                                        indices, val, self.shaleThickness)
                                elif specific_check:
                                    self.shaleThickness_specific_inds.append(index)
                                    self.shaleThickness_specific_vals.append(val)
                                    self.shaleThickness_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'aquifer' and end == 'Thickness':
                            if return_par_input:
                                val = self.check_par_key_values(key, val, 'aquiferThickness')
                            else:
                                val = self.check_parameter_bounds(key, val, 'aquiferThickness')

                            if return_par_input:
                                par_input = val
                                par_name_form = 'aquifer{}Thickness'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.aquiferThickness = set_array_values(
                                        indices, val, self.aquiferThickness)
                                elif specific_check:
                                    self.aquiferThickness_specific_inds.append(index)
                                    self.aquiferThickness_specific_vals.append(val)
                                    self.aquiferThickness_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'shale' and end == 'LogVertK':
                            if return_par_input:
                                val = self.check_par_key_values(key, val, 'shaleLogVertK')
                            else:
                                val = self.check_parameter_bounds(key, val, 'shaleLogVertK')

                            if return_par_input:
                                par_input = val
                                par_name_form = 'shale{}LogVertK'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.shaleLogVertK = set_array_values(
                                        indices, val, self.shaleLogVertK)
                                elif specific_check:
                                    self.shaleLogVertK_specific_inds.append(index)
                                    self.shaleLogVertK_specific_vals.append(val)
                                    self.shaleLogVertK_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'aquifer' and end == 'LogHorizK':
                            if return_par_input:
                                val = self.check_par_key_values(key, val, 'aquiferLogHorizK')
                            else:
                                val = self.check_parameter_bounds(key, val, 'aquiferLogHorizK')

                            if return_par_input:
                                par_input = val
                                par_name_form = 'aquifer{}LogHorizK'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.aquiferLogHorizK = set_array_values(
                                        indices, val, self.aquiferLogHorizK)
                                elif specific_check:
                                    self.aquiferLogHorizK_specific_inds.append(index)
                                    self.aquiferLogHorizK_specific_vals.append(val)
                                    self.aquiferLogHorizK_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'aquifer' and end == 'ANSR':
                            if return_par_input:
                                val = self.check_par_key_values(key, val, 'aquiferANSR')
                            else:
                                val = self.check_parameter_bounds(key, val, 'aquiferANSR')

                            if return_par_input:
                                par_input = val
                                par_name_form = 'aquifer{}ANSR'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.aquiferANSR = set_array_values(
                                        indices, val, self.aquiferANSR)
                                elif specific_check:
                                    self.aquiferANSR_specific_inds.append(index)
                                    self.aquiferANSR_specific_vals.append(val)
                                    self.aquiferANSR_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'shale' and end == 'LogSS':
                            if return_par_input:
                                val = self.check_par_key_values(key, val, 'shaleLogSS')
                            else:
                                val = self.check_parameter_bounds(key, val, 'shaleLogSS')

                            if return_par_input:
                                par_input = val
                                par_name_form = 'shale{}LogSS'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.shaleLogSS = set_array_values(
                                        indices, val, self.shaleLogSS)
                                elif specific_check:
                                    self.shaleLogSS_specific_inds.append(index)
                                    self.shaleLogSS_specific_vals.append(val)
                                    self.shaleLogSS_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'aquifer' and end == 'LogSS':
                            if return_par_input:
                                val = self.check_par_key_values(key, val, 'aquiferLogSS')
                            else:
                                val = self.check_parameter_bounds(key, val, 'aquiferLogSS')

                            if return_par_input:
                                par_input = val
                                par_name_form = 'aquifer{}LogSS'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.aquiferLogSS = set_array_values(
                                        indices, val, self.aquiferLogSS)
                                elif specific_check:
                                    self.aquiferLogSS_specific_inds.append(index)
                                    self.aquiferLogSS_specific_vals.append(val)
                                    self.aquiferLogSS_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'aquifer' and end == 'FluidDensity':
                            if return_par_input:
                                val = self.check_par_key_values(key, val, 'aquiferFluidDensity')
                            else:
                                val = self.check_parameter_bounds(key, val, 'aquiferFluidDensity')

                            if return_par_input:
                                par_input = val
                                par_name_form = 'aquifer{}FluidDensity'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.aquiferFluidDensity = set_array_values(
                                        indices, val, self.aquiferFluidDensity)
                                elif specific_check:
                                    self.aquiferFluidDensity_specific_inds.append(index)
                                    self.aquiferFluidDensity_specific_vals.append(val)
                                    self.aquiferFluidDensity_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'aquifer' and end == 'Head':
                            if return_par_input:
                                val = self.check_par_key_values(key, val, 'aquiferHead')
                            else:
                                val = self.check_parameter_bounds(key, val, 'aquiferHead')

                            if return_par_input:
                                par_input = val
                                par_name_form = 'aquifer{}Head'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.aquiferHead = set_array_values(
                                        indices, val, self.aquiferHead)
                                elif specific_check:
                                    self.aquiferHead_specific_inds.append(index)
                                    self.aquiferHead_specific_vals.append(val)
                                    self.aquiferHead_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'shale' and end == 'InitialGamma':

                            if return_par_input:
                                par_input = val
                                par_name_form = 'shale{}InitialGamma'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.shaleInitialGamma = set_array_values(
                                        indices, val, self.shaleInitialGamma)
                                elif specific_check:
                                    self.shaleInitialGamma_specific_inds.append(index)
                                    self.shaleInitialGamma_specific_vals.append(val)
                                    self.shaleInitialGamma_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'shale' and end == 'Gamma':

                            if return_par_input:
                                par_input = val
                                par_name_form = 'shale{}Gamma'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.shaleGamma = set_array_values(
                                        indices, val, self.shaleGamma)
                                elif specific_check:
                                    self.shaleGamma_specific_inds.append(index)
                                    self.shaleGamma_specific_vals.append(val)
                                    self.shaleGamma_specific_par_names.append(key)

                            continue_test = False

                        elif start == 'aquifer' and end == 'Gamma':

                            if return_par_input:
                                par_input = val
                                par_name_form = 'aquifer{}Gamma'
                                par_names = get_par_names(par_name_form, indices)

                            else:
                                if not specific_check:
                                    self.aquiferGamma = set_array_values(
                                        indices, val, self.aquiferGamma)
                                elif specific_check:
                                    self.aquiferGamma_specific_inds.append(index)
                                    self.aquiferGamma_specific_vals.append(val)
                                    self.aquiferGamma_specific_par_names.append(key)

                            continue_test = False

                        del start

        # If the parameter has not been recognized, warn the user
        if continue_test:
            warning_msg = PAR_NOT_RECOGNIZED_MSG.format(key, self.name, val)

            logging.warning(warning_msg)

            # In this case, the parameter identification was unsuccessful
            return 0, 0

        # In the case when continue_test is False, the parameter identification was successful
        if not return_par_input:
            return 1, 1

        # If return_par_input is True, return a list of formatted parameter names as
        # well as the input value.
        return par_names, par_input

    def clean_properties(self):
        """
        Removes many properties used in the methods compile_pars() and
        check_and_assign_parameter_input(). The properties are not needed
        afterwards.
        """
        del self.keys_to_delete

        # These store the values for specific active wells
        del self.activeWellQ_specific_inds
        del self.activeWellQ_specific_vals
        del self.activeWellQ_specific_par_names

        del self.activeWellRadiusAquifer_specific_inds
        del self.activeWellRadiusAquifer_specific_vals
        del self.activeWellRadiusAquifer_specific_par_names

        # These store the values for specific leaking wells
        del self.leakingWellRadiusAquifer_specific_inds
        del self.leakingWellRadiusAquifer_specific_vals
        del self.leakingWellRadiusAquifer_specific_par_names

        del self.leakingWellKAquifer_specific_inds
        del self.leakingWellKAquifer_specific_vals
        del self.leakingWellKAquifer_specific_par_names

        del self.leakingWellRadiusShale_specific_inds
        del self.leakingWellRadiusShale_specific_vals
        del self.leakingWellRadiusShale_specific_par_names

        del self.leakingWellKShale_specific_inds
        del self.leakingWellKShale_specific_vals
        del self.leakingWellKShale_specific_par_names

        del self.leakingWellStatAquifer_specific_inds
        del self.leakingWellStatAquifer_specific_vals
        del self.leakingWellStatAquifer_specific_par_names

        del self.leakingWellStatShale_specific_inds
        del self.leakingWellStatShale_specific_vals
        del self.leakingWellStatShale_specific_par_names

        # These store the specific values for aquifer or shale properties
        del self.shaleThickness
        del self.aquiferThickness
        del self.shaleLogVertK
        del self.aquiferLogHorizK
        del self.aquiferANSR
        del self.shaleLogSS
        del self.aquiferLogSS
        del self.aquiferFluidDensity
        del self.aquiferHead
        del self.shaleInitialGamma
        del self.shaleGamma
        del self.aquiferGamma

        del self.shaleThickness_specific_inds
        del self.shaleThickness_specific_vals
        del self.shaleThickness_specific_par_names

        del self.aquiferThickness_specific_inds
        del self.aquiferThickness_specific_vals
        del self.aquiferThickness_specific_par_names

        del self.shaleLogVertK_specific_inds
        del self.shaleLogVertK_specific_vals
        del self.shaleLogVertK_specific_par_names

        del self.aquiferLogHorizK_specific_inds
        del self.aquiferLogHorizK_specific_vals
        del self.aquiferLogHorizK_specific_par_names

        del self.aquiferANSR_specific_inds
        del self.aquiferANSR_specific_vals
        del self.aquiferANSR_specific_par_names

        del self.shaleLogSS_specific_inds
        del self.shaleLogSS_specific_vals
        del self.shaleLogSS_specific_par_names

        del self.aquiferLogSS_specific_inds
        del self.aquiferLogSS_specific_vals
        del self.aquiferLogSS_specific_par_names

        del self.aquiferFluidDensity_specific_inds
        del self.aquiferFluidDensity_specific_vals
        del self.aquiferFluidDensity_specific_par_names

        del self.aquiferHead_specific_inds
        del self.aquiferHead_specific_vals
        del self.aquiferHead_specific_par_names

        del self.shaleInitialGamma_specific_inds
        del self.shaleInitialGamma_specific_vals
        del self.shaleInitialGamma_specific_par_names

        del self.shaleGamma_specific_inds
        del self.shaleGamma_specific_vals
        del self.shaleGamma_specific_par_names

        del self.aquiferGamma_specific_inds
        del self.aquiferGamma_specific_vals
        del self.aquiferGamma_specific_par_names

    def assign_default_pars(self, par_list, par_name, par_name_formatting,
                            required_def=False):
        """
        Checks if a parameter list in compile_pars() has a value of None. If so,
        that None value is replaced with the corresponding default value and a
        warning is logged. If the parameter requires input, an error message is
        raised.
        """
        for ind, val in enumerate(par_list):
            if val is None:
                if required_def:
                    err_msg = REQUIRED_PAR_ERROR.format(
                        par_name_formatting.format(ind + 1), self.name, '')

                    logging.error(err_msg)
                    raise KeyError(err_msg)

                par_list[ind] = self.default_pars[par_name].value

                warning_msg = DEFAULT_PAR_WARNING.format(
                    par_name_formatting.format(ind + 1), self.name,
                    self.default_pars[par_name].value)

                logging.warning(warning_msg)

        return par_list

    def check_kw_args(self, kw_arg_list, kw_arg_name, all_pars=None, array_req=True):
        """
        Checks the keyword arguments of the SALSA component. If the argument is
        expected to be an array but it was not given as an array, the method
        attempts to convert it to an array. If that attempt fails, an error is
        logged.
        """
        if kw_arg_list is None and (all_pars is not None):
            # If the argument is None, check if that value is acceptable given
            # other parameter values.
            if kw_arg_name in self.kwargs_none_vals:
                check_req_cond = (
                    all_pars[self.kwargs_none_vals[kw_arg_name]['par']]
                    == self.kwargs_none_vals[kw_arg_name]['val'])

                # if the required conditions are not met, raise a warning
                if not check_req_cond:
                    err_msg = ''.join([
                        'For the SALSA component {}, the keyword argument {} ',
                        'was provided as {}. This setting is only acceptable ',
                        'if the parameter {} is given as {}. Instead, that ',
                        'parameter was given as {}. These conditions ',
                        'may result in an error.']).format(
                            self.name, kw_arg_name, kw_arg_list,
                            self.kwargs_none_vals[kw_arg_name]['par'],
                            self.kwargs_none_vals[kw_arg_name]['val'],
                            all_pars[self.kwargs_none_vals[kw_arg_name]['par']])

                    logging.error(err_msg)
            else:
                warning_msg = ''.join([
                    'The keyword argument {} of the SALSA component {} was given ',
                    'as {}. This setup may result in errors, check your input.'
                    ]).format(kw_arg_name, self.name, kw_arg_list)

                logging.warning(warning_msg)
        else:
            if not isinstance(kw_arg_list, np.ndarray) and array_req:
                kw_arg_list = np.array(kw_arg_list)

        return kw_arg_list

    def give_props_to_pars(self, actual_p):
        """
        Places the properties from keyword arguments and compiled parameter values
        (e.g., activeWellQ) into the parameter dictionary. For certain inputs,
        calls the method check_kw_args() to see if the keyword arguments have the
        expected format.
        """
        # Active well input (injection / extraction wells)
        self.periodEndTimes = self.check_kw_args(
            self.periodEndTimes, 'periodEndTimes')

        # Convert from years to days, if that conversion has not yet been done
        if not self.converted_from_years_to_days:
            self.periodEndTimes = self.periodEndTimes * 365.25
            self.converted_from_years_to_days = True

        actual_p['periodEndTimes'] = self.periodEndTimes

        actual_p['numberOfPeriods'] = len(self.periodEndTimes)

        self.activeWellCoordx = self.check_kw_args(
            self.activeWellCoordx, 'activeWellCoordx')
        actual_p['activeWellCoordx'] = self.activeWellCoordx

        actual_p['numberOfActiveWells'] = len(self.activeWellCoordx)

        self.activeWellCoordy = self.check_kw_args(
            self.activeWellCoordy, 'activeWellCoordy')
        actual_p['activeWellCoordy'] = self.activeWellCoordy

        if len(self.activeWellCoordx) != len(self.activeWellCoordy):
            err_msg = KW_ARG_LEN_MSG.format(
                self.name, 'activeWellCoordx', 'activeWellCoordy', 'activeWellCoordx',
                self.activeWellCoordx, 'activeWellCoordy', self.activeWellCoordy)

            logging.error(err_msg)
            raise KeyError(err_msg)

        actual_p['activeWellRadii'] = self.activeWellRadii

        # Convert from m^3/s to m^3/day
        for ind1 in range(self.activeWellQ.shape[0]):
            for ind2 in range(self.activeWellQ.shape[1]):
                for ind3 in range(self.activeWellQ.shape[2]):
                    self.activeWellQ[ind1, ind2, ind3] *= SEC_PER_DAY

        actual_p['activeWellQ'] = self.activeWellQ

        # Leaking well input
        self.leakingWellCoordx = self.check_kw_args(
            self.leakingWellCoordx, 'leakingWellCoordx')
        actual_p['leakingWellCoordx'] = self.leakingWellCoordx

        actual_p['numberOfLeakingWells'] = len(self.leakingWellCoordx)

        self.leakingWellCoordy = self.check_kw_args(
            self.leakingWellCoordy, 'leakingWellCoordy')
        actual_p['leakingWellCoordy'] = self.leakingWellCoordy

        if len(self.leakingWellCoordx) != len(self.leakingWellCoordy):
            err_msg = KW_ARG_LEN_MSG.format(
                self.name, 'leakingWellCoordx', 'leakingWellCoordy', 'leakingWellCoordx',
                self.leakingWellCoordx, 'leakingWellCoordy', self.leakingWellCoordy)

            logging.error(err_msg)
            raise KeyError(err_msg)

        actual_p['leakingWellAqRadii'] = self.leakingWellAqRadii

        actual_p['leakingWellAqK'] = self.leakingWellAqK

        actual_p['leakingWellShaleRadii'] = self.leakingWellShaleRadii

        actual_p['leakingWellShaleK'] = self.leakingWellShaleK

        actual_p['leakingWellAqStat'] = self.leakingWellAqStat

        actual_p['leakingWellShaleStat'] = self.leakingWellShaleStat

        # Convert from m^3/s to m^3/day
        for ind1 in range(self.leakingWellQ.shape[0]):
            for ind2 in range(self.leakingWellQ.shape[1]):
                self.leakingWellQ[ind1, ind2] *= SEC_PER_DAY

        actual_p['leakingWellQ'] = self.leakingWellQ

        actual_p['leakingWellQAq'] = self.leakingWellQAq

        # Head output locations
        if self.aquiferCoordx is None or self.aquiferCoordy is None:
            err_msg = KW_ARG_NOT_GIVEN_COORDS_MSG.format(
                self.name, 'aquifer head output', 'aquiferCoordx and aquiferCoordy',
                'OutputLocations')

            logging.warning(err_msg)
            raise KeyError(err_msg)

        self.aquiferCoordx = self.check_kw_args(self.aquiferCoordx, 'aquiferCoordx')
        actual_p['aquiferCoordx'] = self.aquiferCoordx

        actual_p['numberOfHeadLocs'] = len(self.aquiferCoordx)

        self.aquiferCoordy = self.check_kw_args(self.aquiferCoordy, 'aquiferCoordy')
        actual_p['aquiferCoordy'] = self.aquiferCoordy

        if len(self.aquiferCoordx) != len(self.aquiferCoordy):
            err_msg = KW_ARG_LEN_MSG.format(
                self.name, 'aquiferCoordx', 'aquiferCoordy', 'aquiferCoordx',
                self.aquiferCoordx, 'aquiferCoordy', self.aquiferCoordy)

            logging.error(err_msg)
            raise KeyError(err_msg)

        if self.shaleCoordx is None or self.shaleCoordy is None:
            err_msg = KW_ARG_NOT_GIVEN_COORDS_MSG.format(
                self.name, 'shale head output', 'shaleCoordx and shaleCoordy',
                'OutputLocations')

            logging.error(err_msg)
            raise KeyError(err_msg)

        self.shaleCoordx = self.check_kw_args(
            self.shaleCoordx, 'shaleCoordx')
        actual_p['shaleCoordx'] = self.shaleCoordx

        actual_p['numberOfHeadProfiles'] = len(self.shaleCoordx)

        self.shaleCoordy = self.check_kw_args(
            self.shaleCoordy, 'shaleCoordy')
        actual_p['shaleCoordy'] = self.shaleCoordy

        if len(self.shaleCoordx) != len(self.shaleCoordy):
            err_msg = KW_ARG_LEN_MSG.format(
                self.name, 'shaleCoordx', 'shaleCoordy', 'shaleCoordx',
                self.shaleCoordx, 'shaleCoordy', self.shaleCoordy)

            logging.error(err_msg)
            raise KeyError(err_msg)

        # Contour plot
        if self.aquiferNamesContourPlots is None:
            warning_msg = ''.join([
                'The aquiferNamesContourPlots keyword argument was not given ',
                'for the SALSA component {}. It will be set to include only ',
                'aquifer 1.']).format(self.name)

            logging.warning(warning_msg)

            self.aquiferNamesContourPlots = np.array([1])
        else:
            self.aquiferNamesContourPlots = self.check_kw_args(
                self.aquiferNamesContourPlots, 'aquiferNamesContourPlots')

        actual_p['aquiferNamesContourPlots'] = self.aquiferNamesContourPlots

        actual_p['numberOfAquiferNames'] = len(self.aquiferNamesContourPlots)

        if self.contourPlotCoordx is not None and self.contourPlotCoordy is not None:
            # In the current version of salsa_wrap() (used in salsa_ROM.py),
            # contour plot x and y values cannot be provided manually. They can
            # only be generated by SALSA (flagMesh value of 1).
            self.contourPlotCoordx = self.check_kw_args(
                self.contourPlotCoordx, 'contourPlotCoordx', all_pars=actual_p)

            self.numberOfContourPlotNodes = len(self.contourPlotCoordx)

            self.contourPlotCoordy = self.check_kw_args(
                self.contourPlotCoordy, 'contourPlotCoordy', all_pars=actual_p)

        kw_arg_none_check = (
            self.numberOfContourPlotNodes is None
            or self.contourPlotCoordx is None or self.contourPlotCoordy is None)

        if kw_arg_none_check:
            self.numberOfContourPlotNodes = self.get_number_of_nodes(actual_p)

            self.contourPlotCoordx = np.zeros(self.numberOfContourPlotNodes)

            self.contourPlotCoordy = np.zeros(self.numberOfContourPlotNodes)

        actual_p['numberOfContourPlotNodes'] = self.numberOfContourPlotNodes

        actual_p['contourPlotCoordx'] = self.contourPlotCoordx

        actual_p['contourPlotCoordy'] = self.contourPlotCoordy

        if self.contourPlotCoordx is not None:
            if len(self.contourPlotCoordx) != len(self.contourPlotCoordy):
                err_msg = KW_ARG_LEN_MSG.format(
                    self.name, 'contourPlotCoordx', 'contourPlotCoordy', 'contourPlotCoordx',
                    self.contourPlotCoordx, 'contourPlotCoordy', self.contourPlotCoordy)

                logging.error(err_msg)
                raise KeyError(err_msg)

        return actual_p

    def get_number_of_nodes(self, actual_p):
        """
        Because contour plot x and y values cannot be provided in the current
        version of salsa_wrap(), they can only be generated. The salsa_wrap()
        input nxyz (a key in the input dictionary, which is set with the
        keyword argument numberOfContourPlotNodes) has to be greater than or
        equal to the number of nodes in the generated mesh. Otherwise, the
        salsa_wrap() function will return an error. The number of generated
        nodes depends on the parameters for the rectangular and radial grids.
        """
        # For the rectangular grid, SALSA actually uses double the number of
        # x rows and y columns given by the parameters numberOfNodesXDir and
        # numberOfNodesYDir, respectively.
        nodes_rect_grid = 4 * actual_p['numberOfNodesXDir'] * actual_p['numberOfNodesYDir']

        num_active_wells = 0
        if self.activeWellCoordx is not None:
            num_active_wells = len(self.activeWellCoordx)

        num_leaking_wells = 0
        if self.leakingWellCoordx is not None:
            num_leaking_wells = len(self.leakingWellCoordx)

        num_of_wells = num_active_wells + num_leaking_wells

        points_per_radial_grid_ring = 16

        num_radial_grid_rings = actual_p['numberOfRadialGrids']

        points_per_well = 1 + (points_per_radial_grid_ring * num_radial_grid_rings)

        nodes_radial_grid = points_per_well * num_of_wells

        total_number_of_nodes = nodes_rect_grid + nodes_radial_grid

        # Give extra nodes - if too few are given, salsa_wrap() will encounter
        # an error. If too many are given, any extra nodes are trimmed from the
        # array, so using too many is not a significant problem. Even when
        # salsa_wrap is given as many nodes as it will generate, or even 50 %
        # more, it still encounters an error.
        total_number_of_nodes = int(total_number_of_nodes * 10)

        return total_number_of_nodes

    @staticmethod
    def convert_par_units(actual_p):
        """
        Converts the units of certain parameters in the actual_p dictionary.
        The conversions for the leaking well parameters leakingWell#LogKAquifer#
        and leakingWell#LogKShale# (given to self.leakingWellAqK and
        self.leakingWellShaleK) and the Gamma parameters (shale#InitialGamma,
        shale#Gamma, and aquifer#Gamma parameters) are handled in the
        compile_pars() method.
        """
        # Convert from log10 m/s to m/s
        actual_p['shaleVertK'] = [
            10 ** logK for logK in actual_p['shaleLogVertK']]
        del actual_p['shaleLogVertK']

        actual_p['aquiferHorizK'] = [
            10 ** logK for logK in actual_p['aquiferLogHorizK']]
        del actual_p['aquiferLogHorizK']

        actual_p['shaleSS'] = [
            10 ** logSS for logSS in actual_p['shaleLogSS']]
        del actual_p['shaleLogSS']

        actual_p['aquiferSS'] = [
            10 ** logSS for logSS in actual_p['aquiferLogSS']]
        del actual_p['aquiferLogSS']

        # Convert from m/s to m/day (needs to align with the time_array in days)
        actual_p['shaleVertK'] = [
            val * SEC_PER_DAY for val in actual_p['shaleVertK']]

        actual_p['aquiferHorizK'] = [
            val * SEC_PER_DAY for val in actual_p['aquiferHorizK']]

        actual_p['leakingWellAqK'] = [
            val * SEC_PER_DAY for val in actual_p['leakingWellAqK']]

        actual_p['leakingWellShaleK'] = [
            val * SEC_PER_DAY for val in actual_p['leakingWellShaleK']]

        return actual_p

    def check_certain_pars(self, actual_p):
        """
        Checks whether certain parameter value combinations are valid.
        """
        if actual_p['bottomBoundaryCond'] == 0:
            if actual_p['bottomBoundaryHead'] is None:
                err_msg = REQUIRED_PAR_ERROR.format(
                    'bottomBoundaryHead', self.name,
                    'because bottomBoundaryCond was set to 0')

                logging.error(err_msg)
                raise KeyError(err_msg)

        if actual_p['topBoundaryCond'] == 0:
            if actual_p['topBoundaryHead'] is None:
                err_msg = REQUIRED_PAR_ERROR.format(
                    'topBoundaryHead', self.name,
                    'because topBoundaryCond was set to 0')

                logging.error(err_msg)
                raise KeyError(err_msg)

        return actual_p

    @staticmethod
    def set_par_values(inputParameters, actual_p):
        """
        Sets the parameter values for the Parameter object.
        """
        # Inputs initially given as keyword arguments (or derived from them)
        # Active well input (injection / extraction wells)
        inputParameters.periodEndTimes = actual_p['periodEndTimes']

        inputParameters.numberOfPeriods = actual_p['numberOfPeriods']

        inputParameters.activeWellCoordx = actual_p['activeWellCoordx']

        inputParameters.activeWellCoordy = actual_p['activeWellCoordy']

        inputParameters.numberOfActiveWells = actual_p['numberOfActiveWells']

        inputParameters.activeWellRadii = actual_p['activeWellRadii']

        inputParameters.activeWellQ = actual_p['activeWellQ']

        # Leaking well input
        inputParameters.leakingWellCoordx = actual_p['leakingWellCoordx']

        inputParameters.leakingWellCoordy = actual_p['leakingWellCoordy']

        inputParameters.numberOfLeakingWells = actual_p['numberOfLeakingWells']

        inputParameters.leakingWellAqRadii = actual_p['leakingWellAqRadii']

        inputParameters.leakingWellAqK = actual_p['leakingWellAqK']

        inputParameters.leakingWellShaleRadii = actual_p['leakingWellShaleRadii']

        inputParameters.leakingWellShaleK = actual_p['leakingWellShaleK']

        inputParameters.leakingWellAqStat = actual_p['leakingWellAqStat']

        inputParameters.leakingWellShaleStat = actual_p['leakingWellShaleStat']

        inputParameters.leakingWellQ = actual_p['leakingWellQ']

        inputParameters.leakingWellQAq = actual_p['leakingWellQAq']

        # Head output locations
        inputParameters.aquiferCoordx = actual_p['aquiferCoordx']

        inputParameters.aquiferCoordy = actual_p['aquiferCoordy']

        inputParameters.shaleCoordx = actual_p['shaleCoordx']

        inputParameters.shaleCoordy = actual_p['shaleCoordy']

        inputParameters.numberOfHeadProfiles = actual_p['numberOfHeadProfiles']
        
        inputParameters.numberOfHeadLocs = actual_p['numberOfHeadLocs']

        # Contour plot
        inputParameters.aquiferNamesContourPlots = actual_p['aquiferNamesContourPlots']

        inputParameters.numberOfAquiferNames = actual_p['numberOfAquiferNames']

        inputParameters.contourPlotCoordx = actual_p['contourPlotCoordx']

        inputParameters.contourPlotCoordy = actual_p['contourPlotCoordy']

        inputParameters.numberOfContourPlotNodes = actual_p['numberOfContourPlotNodes']

        # Inputs initially given as parameters (or derived from them)
        inputParameters.bottomLayerType = actual_p['bottomLayerType']

        inputParameters.topLayerType = actual_p['topLayerType']

        inputParameters.numberOfShaleLayers = actual_p['numberOfShaleLayers']

        inputParameters.numberOfAquiferLayers = actual_p['numberOfAquiferLayers']

        inputParameters.shaleThickness = actual_p['shaleThickness']

        inputParameters.aquiferThickness = actual_p['aquiferThickness']

        inputParameters.shaleVertK = actual_p['shaleVertK']

        inputParameters.aquiferHorizK = actual_p['aquiferHorizK']

        inputParameters.aquiferANSR = actual_p['aquiferANSR']

        inputParameters.shaleSS = actual_p['shaleSS']

        inputParameters.aquiferSS = actual_p['aquiferSS']

        inputParameters.aquiferFluidDensity = actual_p['aquiferFluidDensity']

        inputParameters.aquiferHead = actual_p['aquiferHead']

        inputParameters.shaleInitialGamma = actual_p['shaleInitialGamma']

        inputParameters.shaleGamma = actual_p['shaleGamma']

        inputParameters.aquiferGamma = actual_p['aquiferGamma']

        inputParameters.bottomBoundaryCond = actual_p['bottomBoundaryCond']

        inputParameters.bottomBoundaryHead = actual_p['bottomBoundaryHead']

        inputParameters.topBoundaryCond = actual_p['topBoundaryCond']

        inputParameters.topBoundaryHead = actual_p['topBoundaryHead']

        inputParameters.numberOfLaplaceTerms = actual_p['numberOfLaplaceTerms']

        inputParameters.flagMesh = actual_p['flagMesh']

        inputParameters.xCent = actual_p['xCent']

        inputParameters.yCent = actual_p['yCent']

        inputParameters.xMax = actual_p['xMax']

        inputParameters.yMax = actual_p['yMax']

        inputParameters.gridExp = actual_p['gridExp']

        inputParameters.numberOfNodesXDir = actual_p['numberOfNodesXDir']

        inputParameters.numberOfNodesYDir = actual_p['numberOfNodesYDir']

        inputParameters.numberOfRadialGrids = actual_p['numberOfRadialGrids']

        inputParameters.radialZoneRadius = actual_p['radialZoneRadius']

        inputParameters.radialGridExp = actual_p['radialGridExp']

        inputParameters.numberOfVerticalPoints = actual_p['numberOfVerticalPoints']

        inputParameters.topBoundaryPressure = actual_p['topBoundaryPressure']
        
        inputParameters.topBoundaryFluidDensity = actual_p['topBoundaryFluidDensity']

        inputParameters.waterTableDepth = actual_p['waterTableDepth']

        inputParameters.specifyDatum = actual_p['specifyDatum']

        inputParameters.datumDepth = actual_p['datumDepth']

        return inputParameters

    @staticmethod
    def get_current_shale_point_depth(pointRef, shaleRef, num_aquifers_below,
                                      numberOfVerticalPoints, profileDepths):
        """
        This method is used to calculate the depth for each point (pointRef,
        ranging from 0 to numberOfVerticalPoints - 1) in a shale
        (shaleRef, ranging from 0 to numberOfShaleLayers - 1). The depth is also
        influenced by the number of aquifer layers beneath the point. Here,
        aquRef ranges from 0 to numberOfAquiferLayers - 1, and it is the index
        for the next aquifer layer beneath the pointin consideration. The
        profileDepths used here is the output array created during the
        simulation_model() method.
        """
        # numberOfVerticalPoints - 1 across each shale, then one at the
        # bottom of each aquifer. This is grouped as
        # (points across deeper units) + points within current shale
        # Because this is used in the simulation_model() method, the 
        # numberOfVerticalPoints has already been decreased by one, so no 
        # adjustment is needed here.
        depthInd = ((shaleRef * numberOfVerticalPoints)
                    + num_aquifers_below) + pointRef

        # depth in m
        current_depth = profileDepths[depthInd]

        return current_depth

    @staticmethod
    def calculate_shale_point_head(current_depth, num_aquifers_below, aquiferDepths,
                                   aquiferTopDepths, shaleDepths, actual_p):
        """
        Calculates the initial hydraulic head for a particular point within a
        shale unit, based on the point's depth, aquifer depths, and the initial
        hydraulic heads for the aquifers.
        """
        if num_aquifers_below == 0:
            # In this case, the shale unit is the lowest unit in the domain.
            distance_to_head_below = np.abs(np.min(shaleDepths)) - np.abs(current_depth)

            if actual_p['bottomBoundaryCond'] == 1:
                # If the current shale layer is the lowest but the bottom boundary
                # is closed, use the head of the aquifer above it.
                head_below = actual_p['aquiferHead'][0]
            elif actual_p['bottomBoundaryCond'] == 0:
                head_below = actual_p['bottomBoundaryHead']

            distance_to_head_above = np.abs(aquiferDepths[0]) - np.abs(current_depth)

            head_above = actual_p['aquiferHead'][0]

        elif num_aquifers_below >= len(aquiferDepths):
            # In this case, there is no aquifer above the shale layer.
            distance_to_head_above = np.abs(current_depth)

            if actual_p['topBoundaryCond'] == 1:
                # If the current shale layer is the highest but the top boundary
                # is closed, use the head of the aquifer below it.
                head_above = actual_p['aquiferHead'][-1]
            elif actual_p['topBoundaryCond'] == 0:
                head_above = actual_p['topBoundaryHead']

            distance_to_head_below = np.abs(aquiferTopDepths[-1]) - np.abs(current_depth)

            head_below = actual_p['aquiferHead'][-1]

        else:
            distance_to_head_below = np.abs(
                aquiferTopDepths[num_aquifers_below - 1]) - np.abs(current_depth)

            head_below = actual_p['aquiferHead'][num_aquifers_below - 1]

            distance_to_head_above = np.abs(current_depth) - np.abs(
                aquiferDepths[num_aquifers_below])

            head_above = actual_p['aquiferHead'][num_aquifers_below]

        if head_below == head_above:
            initial_head = head_below
        elif distance_to_head_below == 0:
            initial_head = head_below
        elif distance_to_head_above == 0:
            initial_head = head_above
        elif np.abs(distance_to_head_below) == np.abs(distance_to_head_above):
            initial_head = (head_below + head_above) / 2
        else:
            weight_head_below_i = 1 / np.abs(distance_to_head_below)
            weight_head_above_i = 1 / np.abs(distance_to_head_above)

            weight_head_below = weight_head_below_i / (
                weight_head_below_i + weight_head_above_i)

            weight_head_above = weight_head_above_i / (
                weight_head_below_i + weight_head_above_i)

            initial_head = (head_below * weight_head_below) + (
                head_above * weight_head_above)

        return initial_head
    
    @staticmethod
    def convert_head_to_pressure(head, current_depth, aquiferDepths, aquiferTopDepths, 
                                 shaleDepths, shaleTopDepths, actual_p, 
                                 after_pressure_adjustment=True, pressure_adjustment=0):
        """
        Converts a hydraulic head value to a pressure. First, a depth-averaged 
        water density is calculated based on the densities given for each aquifer, 
        an assumed water density at the surface, and a current depth within an 
        aquifer or shale. After calculating the depth-averaged density, uses the 
        elevation head (current_depth - lowest_depth), gravitational acceleration, 
        and the hydraulic head to calculate a pressure. This approach assumes 
        that the velocity head is approximately zero meters.
        """
        aquifers_above = []
        aquifers_above_water_table_check = []
        aquifer_within = None
        aquifer_within_water_table_check = None
        
        topBoundaryCond = actual_p['topBoundaryCond']
        topLayerType = actual_p['topLayerType']
        waterTableDepth = actual_p['waterTableDepth']
        
        # Checks if the water table needs to be considered
        water_table_check = (topLayerType == 1 and topBoundaryCond == 1 
                             and waterTableDepth != 0)
        
        for aqInd, (aqDepth, aqTopDepth) in enumerate(
                zip(aquiferDepths, aquiferTopDepths)):
            if np.abs(current_depth) >= np.abs(aqDepth):
                # Case where the current depth is >= the bottom depth of this aquifer
                aquifers_above.append(aqInd + 1)
                
            elif np.abs(current_depth) < np.abs(aqDepth) and np.abs(
                    current_depth) > np.abs(aqTopDepth):
                # Case where the current depth is somewhere in the middle of 
                # this aquifer.
                aquifer_within = aqInd + 1
                
                if water_table_check and (aqInd == (len(aquiferDepths) - 1)):
                    aquifer_within_water_table_check = True
            
            # Check if the current aquifer is the top aquifer and if the water 
            # table needs to be considered
            if water_table_check and (aqInd == (len(aquiferDepths) - 1)):
                aquifers_above_water_table_check.append(True)
            else:
                aquifers_above_water_table_check.append(False)
        
        shales_above = []
        shale_within = None
        
        for shaleInd, (shaleDepth, shaleTopDepth) in enumerate(
                zip(shaleDepths, shaleTopDepths)):
            if np.abs(current_depth) >= np.abs(shaleDepth):
                shales_above.append(shaleInd + 1)
            
            if np.abs(current_depth) < np.abs(shaleDepth) and np.abs(
                    current_depth) > np.abs(shaleTopDepth):
                shale_within = shaleInd + 1
        
        # Get densities for the aquifer units
        aquifer_densities = [None] * len(aquifers_above)
        aquifer_thicknesses = [None] * len(aquifers_above)
        
        for ind, (aqNum, waterTableCheck) in enumerate(
                zip(aquifers_above, aquifers_above_water_table_check)):
            aquifer_densities[ind] = actual_p['aquiferFluidDensity'][aqNum - 1]
            
            aquiferThickness = actual_p['aquiferThickness'][aqNum - 1]
            if waterTableCheck:
                if aquiferThickness - np.abs(waterTableDepth) > 0:
                    aquifer_thicknesses[ind] = aquiferThickness - np.abs(waterTableDepth)
                else:
                    aquifer_thicknesses[ind] = 0
            else:
                aquifer_thicknesses[ind] = aquiferThickness
        
        if aquifer_within is not None:
            aquifer_within_density = actual_p['aquiferFluidDensity'][
                aquifer_within - 1]
            
            if aquifer_within_water_table_check:
                aquifer_within_thickness = np.abs(
                    current_depth) - np.abs(
                        aquiferTopDepths[aquifer_within - 1]) - np.abs(
                            waterTableDepth)
                
                if aquifer_within_thickness < 0:
                    aquifer_within_thickness = 0
            else:
                aquifer_within_thickness = np.abs(
                    current_depth) - np.abs(aquiferTopDepths[aquifer_within - 1])
        
        def get_densities_around_shale(shale_num, total_num_shales, actual_p):
            """
            Returns the fluid densities above and below a shale layer. The densities 
            are either for an aquifer or the surface.
            """
            if shale_num == 1 and actual_p['bottomLayerType'] == 0:
                # If the shale is the lowest unit, use the density of the aquifer 
                # above it
                density_below = None
                density_above = actual_p['aquiferFluidDensity'][0]
                
            elif shale_num == total_num_shales and actual_p['topLayerType'] == 0:
                # If the shale is the highest unit, take the average between the 
                # highest aquifer's density and an assumed surface water density
                density_below = actual_p['aquiferFluidDensity'][-1]
                density_above = actual_p['topBoundaryFluidDensity']
                
            else:
                # Order of unit numbers depends on bottomLayerType
                if actual_p['bottomLayerType'] == 0:
                    aqNum_below = shale_num - 1
                    aqNum_above = shale_num
                else:
                    aqNum_below = shale_num
                    aqNum_above = shale_num + 1
                
                density_below = actual_p['aquiferFluidDensity'][aqNum_below - 1]
                density_above = actual_p['aquiferFluidDensity'][aqNum_above - 1]
            
            return density_below, density_above
        
        # Get densities for the shale units
        shale_densities = [None] * len(shales_above)
        shale_thicknesses = [None] * len(shales_above)
        
        total_num_shales = len(shaleDepths)
        
        for ind, shaleNum in enumerate(shales_above):
            density_below, density_above = get_densities_around_shale(
                shaleNum, total_num_shales, actual_p)
            
            if density_below is not None:
                shale_avg_density = (density_below + density_above) / 2
            else:
                shale_avg_density = density_above
            
            shale_densities[ind] = shale_avg_density
            shale_thicknesses[ind] = actual_p['shaleThickness'][shaleNum - 1]
        
        if shale_within is not None:
            density_below, density_above = get_densities_around_shale(
                shale_within, total_num_shales, actual_p)
            
            if shale_within == total_num_shales and actual_p['topLayerType'] == 0:
                # If the current depth is within the top shale and there is no 
                # aqufier above that shale, set the distance_above with the 
                # current_depth
                distance_above = np.abs(current_depth)
            else:
                # Order of unit numbers depends on bottomLayerType
                if actual_p['bottomLayerType'] == 0:
                    aqNum_above = shale_within
                else:
                    aqNum_above = shale_within + 1
                
                distance_above = np.abs(current_depth) - np.abs(aquiferDepths[aqNum_above - 1])
            
            # Thickness used to calculate thickness-averaged density
            shale_within_thickness = distance_above
            
            # If there is no aquifer below the shale, there is no fluid density 
            # to use for a unit beneath the shale.
            if density_below is None:
                shale_within_density = density_above
            else:
                # Order of unit numbers depends on bottomLayerType
                if actual_p['bottomLayerType'] == 0:
                    aqNum_above = shale_within
                else:
                    aqNum_above = shale_within + 1
                
                distance_below = np.abs(shaleDepths[shale_within - 1]) - np.abs(current_depth)
                
                # Get the average density for the portion of the shale above the 
                # current depth using a distance-weighted average
                weight_density_below_i = 1 / np.abs(distance_below)
                weight_density_above_i = 1 / np.abs(distance_above)

                weight_density_below = weight_density_below_i / (
                    weight_density_below_i + weight_density_above_i)

                weight_density_above = weight_density_above_i / (
                    weight_density_below_i + weight_density_above_i)
                
                shale_density_at_point = (density_below * weight_density_below) + (
                    density_above * weight_density_above)
                
                # For a point within a shale layer, use the average of the density 
                # in the overlying aquifer and the density at that point within the 
                # shale.
                shale_within_density = (density_above + shale_density_at_point) / 2
        
        # Get the average water density for the column of water extending from 
        # the current depth to the surface.
        total_thickness = 0
        
        sum_of_densities_weighed_by_thickness = 0
        
        for ind, (density, thickness) in enumerate(
                zip(aquifer_densities, aquifer_thicknesses)):
            sum_of_densities_weighed_by_thickness += (density * thickness)
            
            total_thickness += thickness
        
        if aquifer_within is not None:
            sum_of_densities_weighed_by_thickness += (
                aquifer_within_density * aquifer_within_thickness)
            
            total_thickness += aquifer_within_thickness
        
        for ind, (density, thickness) in enumerate(
                zip(shale_densities, shale_thicknesses)):
            sum_of_densities_weighed_by_thickness += (density * thickness)
            
            total_thickness += thickness
        
        if shale_within is not None:
            sum_of_densities_weighed_by_thickness += (
                shale_within_density * shale_within_thickness)
            
            total_thickness += shale_within_thickness
        
        if total_thickness == 0:
            avg_density = actual_p['topBoundaryFluidDensity']
        else:
            avg_density = sum_of_densities_weighed_by_thickness / total_thickness
        
        # Now calculate pressure from the pressure head portion of hydraulic head 
        # (hydraulic head minus elevation head).
        if actual_p['specifyDatum']:
            datum_depth = actual_p['datumDepth']
        else:
            if actual_p['bottomLayerType'] == 0:
                datum_depth = np.min(shaleDepths)
            else:
                datum_depth = np.min(aquiferDepths)
        
        elev_head = np.abs(datum_depth) - np.abs(current_depth)
        
        pressure_head = head - elev_head
        
        pressure = pressure_head * avg_density * GRAV_ACCEL
        
        top_bound_pressure = False
        if actual_p['topLayerType'] == 1 and (
                np.abs(current_depth) <= np.abs(aquiferDepths[-1])):
            if (np.abs(current_depth) <= np.abs(actual_p['waterTableDepth']) 
                and after_pressure_adjustment):
                pressure = actual_p['topBoundaryPressure']
                top_bound_pressure = True
        
        if after_pressure_adjustment and not top_bound_pressure:
            pressure += pressure_adjustment
        
        return pressure

    def simulation_model(self, p, time_point=365.25, time_step=365.25):
        """
        Return observations of the SALSA component.

        :param p: input parameters of SALSA component
        :type p: dict

        :param time_point: time point in days at which the leakage rates are
            to be calculated; by default, its value is 365.25 (1 year in days)
        :type time_point: float

        :param time_step: difference between the current and previous
            time points in days; by default, its value is 365.25 (1 year in days)
        :type time_point: float

        :returns: out - dictionary of observations of SALSA component
            model; keys (here # must be replaced with an appropriate number):
            ['headProfile#VertPoint#Shale#', 'pressureProfile#VertPoint#Shale#', 
             'profileDepths', 'allHeadProfilesShales', 'allPressureProfilesShales', 
             'headLoc#Aquifer#', 'pressureLoc#Aquifer#', 'pressureLoc#MidAquifer#', 
             'pressureLoc#TopAquifer#', 'diffuseLeakageRateBottomAquifer#', 
             'diffuseLeakageRateTopAquifer#', 'diffuseLeakageVolumeBottomAquifer#', 
             'diffuseLeakageVolumeTopAquifer#', 'wellLeakageRateAquifer#', 
             'wellLeakageVolumneAquifer#', 'well#LeakageRateAquifer#', 
             'well#LeakageVolumneAquifer#', 'contourPlotAquiferHead', 
             'contourPlotAquiferPressure', 'contourPlotMidAquiferPressure', 
             'contourPlotTopAquiferPressure', 'contourPlotCoordx', 
             'contourPlotCoordy']
        """

        # Obtain the default values of the parameters from dictionary of default parameters
        actual_p = {k: v.value for k, v in self.default_pars.items()}

        # Update default values of parameters with the provided ones
        actual_p.update(p)

        # The numberOfAquiferLayers parameter is set as a function of other parameters
        actual_p = self.set_num_aquifers(actual_p)

        # Compile aquifer and shale parameters into lists
        actual_p = self.compile_pars(actual_p)

        # Certain parameters need to be checked
        actual_p = self.check_certain_pars(actual_p)

        # In the current version of the salsa_wrap() function used in salsa_ROM.py,
        # flagMesh has to be fixed at one. Therefore, x and y values cannot be
        # provided for the contour plot - you can only use the x and y values
        # generated automatically by SALSA. This issue might change in a future
        # version.
        actual_p['flagMesh'] = 1

        # For the rectangular grid, SALSA actually uses double the number of
        # x rows and y columns given by the parameters numberOfNodesXDir and
        # numberOfNodesYDir, respectively. Changing the user input here so that
        # the parameter has the expected impact (i.e., so that the number of
        # rectangular grid points is numberOfNodesXDir * numberOfNodesXDir, not
        # numberOfNodesXDir * numberOfNodesXDir * 4).
        actual_p['numberOfNodesXDir'] = np.ceil(0.5 * actual_p['numberOfNodesXDir'])

        actual_p['numberOfNodesYDir'] = np.ceil(0.5 * actual_p['numberOfNodesYDir'])
        
        # For the number of points to be as intended, this parameter needs to 
        # be decreased by one - salsa_wrap() will produce results for points 1 
        # through this value plus 1.
        actual_p['numberOfVerticalPoints'] = actual_p[
            'numberOfVerticalPoints'] - 1

        actual_p = self.give_props_to_pars(actual_p)

        # Handle parameters that need adjustment
        actual_p = self.convert_par_units(actual_p)
        
        if actual_p['bottomBoundaryCond'] == 0 and actual_p['bottomLayerType'] == 1:
            if not self.warning_for_bottom_bound_cond:
                boundary = 'bottom'
                warning_msg = BOUNDARY_COND_CHANGE_MSG.format(
                    self.name, boundary, boundary, boundary, boundary, boundary)
                logging.warning(warning_msg)
                
                self.warning_for_bottom_bound_cond = True
            
            actual_p['bottomBoundaryCond'] = 1
        
        if actual_p['topBoundaryCond'] == 0 and actual_p['topLayerType'] == 1:
            if not self.warning_for_top_bound_cond:
                boundary = 'top'
                warning_msg = BOUNDARY_COND_CHANGE_MSG.format(
                    self.name, boundary, boundary, boundary, boundary, boundary)
                logging.warning(warning_msg)
                
                self.warning_for_top_bound_cond = True
            
            actual_p['topBoundaryCond'] = 1

        inputParameters = salsa_ROM.Parameters()

        inputParameters = self.set_par_values(inputParameters, actual_p)

        # Set the time_array (time in days). Cannot include t = 0 years.
        time_array = self._parent.time_array

        # Used to check if 0 was in the time_array
        remove_zero_check = (0 in time_array)

        time_array = time_array[time_array > 0]

        inputParameters.time_array = time_array

        inputParameters.stochastic = self.stochastic

        inputParameters.realizations = self.realizations

        timeInd_adjust = 0
        if remove_zero_check:
            timeInd_adjust = 1

        # Use the shale and aquifer depths to calculate the initial head for
        # each shale point. Provide the thicknesses and layer types directly in
        # the get_unit_depths() method, to avoid any potential complications
        # in stochastic simulations.
        shaleDepths, _, shaleTopDepths, aquiferDepths, aquiferMidDepths, \
            aquiferTopDepths = self.get_unit_depths(
                shale_thickness_list=actual_p['shaleThickness'], 
                aquifer_thickness_list=actual_p['aquiferThickness'], 
                bottom_layer_type=actual_p['bottomLayerType'],
                top_layer_type=actual_p['topLayerType'])

        # Create solution object with defined input parameters
        sol = salsa_ROM.Solution(inputParameters)

        # Find solution corresponding to the inputParameters
        sol.find(self.output_dir)

        # Get the output from the solution object
        # Shale head profiles in (m), numberOfVerticalPoints in each shale
        haqt = sol.haqt

        # Aquifer head profiles in (m) - head is uniform in each aquifer
        saqf = sol.saqf

        # Total diffuse leakage rate for each aquifer-aquitard interface in (m^3/day)
        faqt = sol.faqt

        # Total cumulative diffuse leakage through each aquifer-aquitard interface in (m^3)
        cfaqt = sol.cfaqt

        # Total focused leakage rate through wells into each aquifer in (m^3/day)
        qctvt = sol.qctvt

        # Total cumulative focused leakage to each aquifer in (m^3)
        cfct = sol.cfct

        # Focused well leakage rates  from each well into each aquifer in (m^3/day)
        fcte_ind = sol.fcte_ind

        # Contour plot head values in (m)
        chead = sol.chead

        # Contour plot x values in (m)
        cxout = sol.cxout

        # Contour plot y values in (m)
        cyout = sol.cyout

        # Create output dictionary
        out = dict()

        # The following abbreviations are used to describe output array dimensions:
        # 'time index' refers to 0:len(time_array)
        # 'location index' refers to 0:len(aquiferCoordx)
        # 'head profile index' refers to 0:numberOfHeadProfiles
        # 'shale index' refers to 0:numberOfShaleLayers, from bottom to top
        # 'aquifer index' refers to 0:numberOfAquiferLayers, from bottom to top
        # 'interface index' refers to 0:(numberOfAquiferLayers * 2) - each aquifer-aquitard interface, from bottom to top
        # 'leaking well index' refers to 0:numberOfLeakingWells
        # 'vertical point index' refers to 0:(numberOfVerticalPoints + 1) (used for shale head profiles)
        # 'aquifer name index' refers to 0:numberOfAquiferNames (used for contour plot head values)
        # 'contour plot location index' refers to 0:numberOfContourPlotNodes (used for contour plot head values)      

        # Depths for the head profile across the shales
        sub_ind = 1
        if sol.parameters.bottomLayerType == 1:
            sub_ind = 0

        distFromBottom = np.linspace(0, sol.parameters.shaleThickness[0],
                                     sol.parameters.numberOfVerticalPoints + 1)

        if sol.parameters.bottomLayerType == 1:
            distFromBottom = np.concatenate((
                [0], distFromBottom + sol.parameters.aquiferThickness[0]))

        endz = distFromBottom[np.size(distFromBottom) - 1]

        for j in range(sol.parameters.numberOfShaleLayers):
            if j > 0:
                begz = endz + sol.parameters.aquiferThickness[j - sub_ind]
                endz = begz + sol.parameters.shaleThickness[j]
                distFromBottom = np.concatenate((distFromBottom, np.linspace(
                    begz, endz, sol.parameters.numberOfVerticalPoints + 1)))

        if sol.parameters.topLayerType == 1:
            distFromBottom = np.concatenate((
                distFromBottom, [distFromBottom[np.size(distFromBottom) - 1] + (
                    sol.parameters.aquiferThickness[
                        sol.parameters.numberOfAquiferLayers - 1])]))

        profileDepths = distFromBottom - np.max(distFromBottom)

        out['profileDepths'] = profileDepths

        self.output_labels.append('profileDepths')
        
        # Adjust pressure results so the initial pressure near the surface is 
        # consistent with atmospheric pressure. Hydraulic head is relative to a 
        # datum, but pressure needs to be absolute.
        if actual_p['topBoundaryCond'] == 0:
            # Case for fixed head boundary condition
            initial_head = actual_p['topBoundaryHead']
            
            # Initial pressure before adjustment
            initial_pressure = self.convert_head_to_pressure(
                initial_head, 0, aquiferDepths, 
                aquiferTopDepths, shaleDepths, shaleTopDepths, 
                actual_p, after_pressure_adjustment=False)
            
            # Adjustment required to be consistent with surface pressure
            pressure_adjustment = sol.parameters.topBoundaryPressure - initial_pressure
            
            if actual_p['waterTableDepth'] != 0 and not self.warning_for_water_table:
                warning_msg = ''.join([
                    'For the SALSA component {}, the waterTableDepth parameter ', 
                    'was given a value of {} m, but the topBoundaryCond parameter ', 
                    'was given a value of {}. The waterTableDepth parameter does ', 
                    'not have an effect when topBoundaryCond is {}.']).format(
                        self.name, actual_p['waterTableDepth'], 
                        actual_p['topBoundaryCond'], actual_p['topBoundaryCond'])
                logging.warning(warning_msg)
                
                # Setting this to True, so that the warning will not be provided 
                # repeatedly in a stochastic simulation
                self.warning_for_water_table = True
            
        elif actual_p['topBoundaryCond'] == 1:
            if actual_p['topLayerType'] == 0:
                # The top shale point should be at the surface, but check for internal consistency
                # Also check if the depth is below the water table - if so, do not 
                # use that depth.
                current_depth = self.get_current_shale_point_depth(
                    sol.parameters.numberOfVerticalPoints, 
                    sol.parameters.numberOfShaleLayers - 1, sol.parameters.numberOfAquiferLayers,
                    sol.parameters.numberOfVerticalPoints, profileDepths)
                
                # Get the initial hydraulic head for the top point within the shale
                initial_head = self.calculate_shale_point_head(
                    current_depth, sol.parameters.numberOfAquiferLayers, aquiferDepths,
                    aquiferTopDepths, shaleDepths, actual_p)
                
                # Initial pressure before adjustment
                initial_pressure = self.convert_head_to_pressure(
                    initial_head, current_depth, aquiferDepths, 
                    aquiferTopDepths, shaleDepths, shaleTopDepths, 
                    actual_p, after_pressure_adjustment=False)
                
                # Adjustment required to be consistent with surface pressure
                pressure_adjustment = sol.parameters.topBoundaryPressure - initial_pressure
                
                if actual_p['waterTableDepth'] != 0 and not self.warning_for_water_table:
                    warning_msg = ''.join([
                        'For the SALSA component {}, the waterTableDepth parameter ', 
                        'was given a value of {} m, but the topLayerType parameter ', 
                        'was given a value of {}. The waterTableDepth parameter ', 
                        'does not have an effect when topLayerType is {}.']).format(
                            self.name, actual_p['waterTableDepth'], 
                            actual_p['topLayerType'], actual_p['topLayerType'])
                    logging.warning(warning_msg)
                    
                    self.warning_for_water_table = True
                
            elif actual_p['topLayerType'] == 1:
                # The hydraulic head is uniform across the thickness of the aquifer. 
                # Here, a pressure is calculated from the aquifer's initial 
                # hydraulic head and the middle depth of the aquifer. Then
                initial_head = actual_p['aquiferHead'][-1]
                aquifer_density = actual_p['aquiferFluidDensity'][-1]
                current_depth = -actual_p['aquiferThickness'][-1] / 2
                
                # Initial pressure before adjustment
                initial_pressure = self.convert_head_to_pressure(
                    initial_head, current_depth, aquiferDepths, 
                    aquiferTopDepths, shaleDepths, shaleTopDepths, 
                    actual_p, after_pressure_adjustment=False)
                
                # Hydrostatic increase in pressure to the middle of the aquifer
                if (np.abs(current_depth) - np.abs(sol.parameters.waterTableDepth)) <= 0:
                    # In this case, force the water table to be at the bottom 
                    # of the aquifer. Otherwise, this situation could cause 
                    # problems for the pressure output within the shale layers.
                    current_depth = -actual_p['aquiferThickness'][-1]
                    
                    self.water_table_clipped = True
                    
                    # Initial pressure before adjustment
                    initial_pressure = self.convert_head_to_pressure(
                        initial_head, current_depth, aquiferDepths, 
                        aquiferTopDepths, shaleDepths, shaleTopDepths, 
                        actual_p, after_pressure_adjustment=False)
                    
                    if (np.abs(current_depth) - np.abs(sol.parameters.waterTableDepth)) < 0:
                        if not self.warning_for_water_table:
                            warning_msg = ''.join([
                                'For the SALSA component {}, the waterTableDepth ', 
                                'parameter ({} m) was lower than the bottom depth ', 
                                'of the top aquifer layer ({} m). The waterTableDepth ', 
                                'cannot be beneath this depth, so it will be ', 
                                'set to {} m.']).format(
                                    self.name, sol.parameters.waterTableDepth, 
                                    current_depth, current_depth)
                            logging.warning(warning_msg)
                            
                            self.warning_for_water_table = True
                        
                        # Adjustment required to be consistent with surface pressure
                        pressure_adjustment = sol.parameters.topBoundaryPressure - initial_pressure
                    else:
                        inc_in_pressure_across_aquifer = aquifer_density * GRAV_ACCEL * (
                            np.abs(current_depth) - np.abs(sol.parameters.waterTableDepth))
                        
                        # Adjustment required to be consistent with surface pressure
                        pressure_adjustment = (
                            sol.parameters.topBoundaryPressure + inc_in_pressure_across_aquifer
                            ) - initial_pressure
                    
                else:
                    inc_in_pressure_across_aquifer = aquifer_density * GRAV_ACCEL * (
                        np.abs(current_depth) - np.abs(sol.parameters.waterTableDepth))
                
                    # Adjustment required to be consistent with surface pressure
                    pressure_adjustment = (
                        sol.parameters.topBoundaryPressure + inc_in_pressure_across_aquifer
                        ) - initial_pressure
        
        # This is used to track the minimum pressure in the simulation - if the 
        # minimum pressure is less than 0, an error is logged.
        min_pressure = sol.parameters.topBoundaryPressure
        
        def check_for_min_pressure(pressure, min_pressure):
            """
            Compares the current pressure value with the current minimum pressure. 
            If it is lower than the minimum, the minimum is updated and returned. 
            Otherwise, the previous minimum is returned.
            """
            if pressure < min_pressure:
                min_pressure = pressure
            
            return min_pressure

        # Profiles of hydraulic head (m) and pressure (Pa) across the shales
        # haqt dimensions: [time index, vertical point index, shale index, head profile index]
        num_aquifers_below = -1
        if actual_p['bottomLayerType'] == 1:
            num_aquifers_below += 1

        for shaleInd in range(haqt.shape[2]):
            # Used to keep track of how many aquifer layers are beneath this shale layer
            if (num_aquifers_below + 1) <= actual_p['numberOfAquiferLayers']:
                num_aquifers_below += 1

            for vertPointInd in range(haqt.shape[1]):
                for headProfileInd in range(haqt.shape[3]):
                    out_label = 'headProfile{}VertPoint{}Shale{}'.format(
                        headProfileInd + 1, vertPointInd + 1, shaleInd + 1)

                    out_label_pressure = 'pressureProfile{}VertPoint{}Shale{}'.format(
                        headProfileInd + 1, vertPointInd + 1, shaleInd + 1)

                    self.output_labels.append(out_label)
                    self.output_labels.append(out_label_pressure)

                    current_depth = self.get_current_shale_point_depth(
                        vertPointInd, shaleInd, num_aquifers_below,
                        actual_p['numberOfVerticalPoints'], profileDepths)

                    if remove_zero_check:
                        initial_head = self.calculate_shale_point_head(
                            current_depth, num_aquifers_below, aquiferDepths,
                            aquiferTopDepths, shaleDepths, actual_p)

                        out[out_label + '_0'] = initial_head

                        out[out_label_pressure + '_0'] = self.convert_head_to_pressure(
                            initial_head, current_depth, aquiferDepths, 
                            aquiferTopDepths, shaleDepths, shaleTopDepths, 
                            actual_p, pressure_adjustment=pressure_adjustment)
                        
                        min_pressure = check_for_min_pressure(
                            out[out_label_pressure + '_0'], min_pressure)

                    for timeInd in range(haqt.shape[0]):
                        time_label = '_{}'.format(timeInd + timeInd_adjust)

                        head = haqt[timeInd, vertPointInd, shaleInd, headProfileInd]

                        out[out_label + time_label] = head

                        out[out_label_pressure + time_label] = self.convert_head_to_pressure(
                            head, current_depth, aquiferDepths, 
                            aquiferTopDepths, shaleDepths, shaleTopDepths, 
                            actual_p, pressure_adjustment=pressure_adjustment)
                        
                        min_pressure = check_for_min_pressure(
                            out[out_label_pressure + time_label], min_pressure)

        # Formatted array of head profiles across the aquitards, to be plotted
        # against profileDepths.
        numberOfHeadProfiles = inputParameters.numberOfHeadProfiles

        allHeadProfilesShales = np.zeros((numberOfHeadProfiles,
                                          len(time_array) + timeInd_adjust,
                                          np.size(profileDepths)))

        for profileInd in range(numberOfHeadProfiles):
            har = np.zeros((len(time_array), np.size(profileDepths)))

            for timeInd in range(len(time_array)):
                ct = 0

                if sol.parameters.bottomLayerType == 1:
                    har[timeInd][0] = haqt[timeInd][0][0][profileInd]
                    ct = 1

                for shaleRef in range(sol.parameters.numberOfShaleLayers):
                    for pointInd in range(sol.parameters.numberOfVerticalPoints + 1):
                        har[timeInd][ct] = haqt[timeInd][pointInd][shaleRef][profileInd]
                        ct = ct + 1

                if sol.parameters.topLayerType == 1:
                    har[timeInd][ct] = har[timeInd][ct - 1]

            allHeadProfilesShales[profileInd][timeInd_adjust:] = har

        if remove_zero_check:
            for depthRef in range(len(profileDepths)):
                current_depth = profileDepths[depthRef]

                num_aquifers_below = 0
                for aquInd in range(len(aquiferTopDepths)):
                    if np.abs(current_depth) <= np.abs(aquiferTopDepths[aquInd]):
                        num_aquifers_below += 1

                initial_head = self.calculate_shale_point_head(
                    current_depth, num_aquifers_below, aquiferDepths,
                    aquiferTopDepths, shaleDepths, actual_p)

                allHeadProfilesShales[:, 0, depthRef] = initial_head

        out['allHeadProfilesShales'] = allHeadProfilesShales

        self.output_labels.append('allHeadProfilesShales')

        # Calculate pressure from allPressureProfilesShales
        allPressureProfilesShales = allHeadProfilesShales.copy()
        
        for i in range(allHeadProfilesShales.shape[0]):
            for j in range(allHeadProfilesShales.shape[1]):
                for k in range(allHeadProfilesShales.shape[2]):
                    current_depth = profileDepths[k]

                    head = allPressureProfilesShales[i, j, k]

                    allPressureProfilesShales[i, j, k] = self.convert_head_to_pressure(
                        head, current_depth, aquiferDepths, aquiferTopDepths, 
                        shaleDepths, shaleTopDepths, actual_p, 
                        pressure_adjustment=pressure_adjustment)
                    
                    min_pressure = check_for_min_pressure(
                        allPressureProfilesShales[i, j, k], min_pressure)

        out['allPressureProfilesShales'] = allPressureProfilesShales

        self.output_labels.append('allPressureProfilesShales')

        # Head values (m) and pressure (Pa) in the aquifers
        # saqf dimensions: [time index, location index, aquifer index]
        for locInd in range(saqf.shape[1]):
            for aquiferInd in range(saqf.shape[2]):
                out_label = 'headLoc{}Aquifer{}'.format(
                    locInd + 1, aquiferInd + 1)
                
                out_label_pressure = 'pressureLoc{}Aquifer{}'.format(
                    locInd + 1, aquiferInd + 1)
                out_label_pressure_mid = 'pressureLoc{}MidAquifer{}'.format(
                    locInd + 1, aquiferInd + 1)
                out_label_pressure_top = 'pressureLoc{}TopAquifer{}'.format(
                    locInd + 1, aquiferInd + 1)

                self.output_labels.append(out_label)
                self.output_labels.append(out_label_pressure)
                self.output_labels.append(out_label_pressure_mid)
                self.output_labels.append(out_label_pressure_top)

                if remove_zero_check:
                    # Use the initial hydraulic head for the aquifer
                    head = actual_p['aquiferHead'][aquiferInd]

                    out[out_label + '_0'] = head

                    out[out_label_pressure + '_0'] = self.convert_head_to_pressure(
                        head, aquiferDepths[aquiferInd], aquiferDepths, aquiferTopDepths, 
                        shaleDepths, shaleTopDepths, actual_p, 
                        pressure_adjustment=pressure_adjustment)
                    
                    out[out_label_pressure_mid + '_0'] = self.convert_head_to_pressure(
                        head, aquiferMidDepths[aquiferInd], aquiferDepths, aquiferTopDepths, 
                        shaleDepths, shaleTopDepths, actual_p, pressure_adjustment=pressure_adjustment)
                    
                    out[out_label_pressure_top + '_0'] = self.convert_head_to_pressure(
                        head, aquiferTopDepths[aquiferInd], aquiferDepths, aquiferTopDepths, 
                        shaleDepths, shaleTopDepths, actual_p, pressure_adjustment=pressure_adjustment)
                    
                    min_pressure = check_for_min_pressure(
                        out[out_label_pressure + '_0'], min_pressure)
                    
                    min_pressure = check_for_min_pressure(
                        out[out_label_pressure_mid + '_0'], min_pressure)
                    
                    min_pressure = check_for_min_pressure(
                        out[out_label_pressure_top + '_0'], min_pressure)

                for timeInd in range(saqf.shape[0]):
                    time_label = '_{}'.format(timeInd + timeInd_adjust)

                    head = saqf[timeInd, locInd, aquiferInd]

                    out[out_label + time_label] = head

                    out[out_label_pressure + time_label] = self.convert_head_to_pressure(
                        head, aquiferDepths[aquiferInd], aquiferDepths, aquiferTopDepths, 
                        shaleDepths, shaleTopDepths, actual_p, pressure_adjustment=pressure_adjustment)
                    
                    out[out_label_pressure_mid + time_label] = self.convert_head_to_pressure(
                        head, aquiferMidDepths[aquiferInd], aquiferDepths, aquiferTopDepths, 
                        shaleDepths, shaleTopDepths, actual_p, pressure_adjustment=pressure_adjustment)
                    
                    out[out_label_pressure_top + time_label] = self.convert_head_to_pressure(
                        head, aquiferTopDepths[aquiferInd], aquiferDepths, aquiferTopDepths, 
                        shaleDepths, shaleTopDepths, actual_p, pressure_adjustment=pressure_adjustment)
                    
                    min_pressure = check_for_min_pressure(
                        out[out_label_pressure + time_label], min_pressure)
                    
                    min_pressure = check_for_min_pressure(
                        out[out_label_pressure_mid + time_label], min_pressure)
                    
                    min_pressure = check_for_min_pressure(
                        out[out_label_pressure_top + time_label], min_pressure)

        # Total diffuse leakage rate through each aquifer-aquitard interface,
        # from faqt (m^3/day)
        # faqt dimensions: [time index, interface index]
        # These are used to keep track of the current aquifer name and whether
        # the results apply to the bottom or top aquifer-aquitard interface
        aquiferIndUpdated = 0 # ranges from 0 to (numberOfAquiferLayers - 1)
        interfaceCounter = 0 # 1 for bottom, 2 for top
        for interfaceInd in range(faqt.shape[1]):
            interfaceCounter += 1

            if interfaceCounter == 1:
                interfaceType = 'Bottom'
            elif interfaceCounter == 2:
                interfaceType = 'Top'

            out_label = 'diffuseLeakageRate{}Aquifer{}'.format(
                interfaceType, aquiferIndUpdated + 1)

            self.output_labels.append(out_label)

            if remove_zero_check:
                out[out_label + '_0'] = self.t0_val

            for timeInd in range(faqt.shape[0]):
                time_label = '_{}'.format(timeInd + timeInd_adjust)

                # Convert from (m^3/day) to (m^3/s)
                out[out_label + time_label] = faqt[
                    timeInd, interfaceInd] / SEC_PER_DAY

            if interfaceCounter >= 2:
                interfaceCounter = 0

                aquiferIndUpdated += 1

        # Total cumulative diffuse leakage through each aquifer-aquitard interface (m^3)
        # cfaqt dimensions: [time index, interface index]
        # These are used to keep track of the current aquifer name and whether
        # the results apply to the bottom or top aquifer-aquitard interface
        aquiferIndUpdated = 0 # ranges from 0 to (numberOfAquiferLayers - 1)
        interfaceCounter = 0 # 1 for bottom, 2 for top
        for interfaceInd in range(cfaqt.shape[1]):
            interfaceCounter += 1

            if interfaceCounter == 1:
                interfaceType = 'Bottom'
            elif interfaceCounter == 2:
                interfaceType = 'Top'

            out_label = 'diffuseLeakageVolume{}Aquifer{}'.format(
                interfaceType, aquiferIndUpdated + 1)

            self.output_labels.append(out_label)

            if remove_zero_check:
                out[out_label + '_0'] = self.t0_val

            for timeInd in range(cfaqt.shape[0]):
                time_label = '_{}'.format(timeInd + timeInd_adjust)

                out[out_label + time_label] = cfaqt[timeInd, interfaceInd]

            if interfaceCounter >= 2:
                interfaceCounter = 0

                aquiferIndUpdated += 1

        # Total (from all wells) focused leakage rate through wells and into an
        # aquifer, from qctvt (m^3/day)
        # qctvt dimensions: [time index, aquifer index]
        for aquiferInd in range(qctvt.shape[1]):
            out_label = 'wellLeakageRateAquifer{}'.format(aquiferInd + 1)

            self.output_labels.append(out_label)

            if remove_zero_check:
                out[out_label + '_0'] = self.t0_val

            for timeInd in range(qctvt.shape[0]):
                time_label = '_{}'.format(timeInd + timeInd_adjust)

                # Convert from (m^3/day) to (m^3/s)
                out[out_label + time_label] = qctvt[
                    timeInd, aquiferInd] / SEC_PER_DAY

        # Total (from all wells) cumulative focused aquifer leakage in (m^3)
        # cfct dimensions: [time index, aquifer index]
        for aquiferInd in range(cfct.shape[1]):
            out_label = 'wellLeakageVolumeAquifer{}'.format(aquiferInd + 1)

            self.output_labels.append(out_label)

            if remove_zero_check:
                out[out_label + '_0'] = self.t0_val

            for timeInd in range(cfct.shape[0]):
                time_label = '_{}'.format(timeInd + timeInd_adjust)

                out[out_label + time_label] = cfct[timeInd, aquiferInd]

            if interfaceCounter >= 2:
                interfaceCounter = 0

                aquiferIndUpdated += 1

        # Focused leakage rate from each leaking well into each aquifer, from
        # fcte_ind (m^3/day)
        # fcte_ind dimensions: [time index, leaking well index, aquifer index]
        for wellInd in range(fcte_ind.shape[1]):
            for aquiferInd in range(fcte_ind.shape[2]):
                out_label = 'well{}LeakageRateAquifer{}'.format(
                    wellInd + 1, aquiferInd + 1)

                self.output_labels.append(out_label)

                if remove_zero_check:
                    out[out_label + '_0'] = self.t0_val

                for timeInd in range(fcte_ind.shape[0]):
                    time_label = '_{}'.format(timeInd + timeInd_adjust)

                    # Convert from (m^3/day) to (m^3/s)
                    out[out_label + time_label] = fcte_ind[
                        timeInd, wellInd, aquiferInd] / SEC_PER_DAY

        # Focused leakage volume from each leaking well into each aquifer (m^3)
        # This metric is not produced by salsa_wrap, it is calculated from
        # ftce_ind.
        # fcte_ind dimensions: [time index, leaking well index, aquifer index]
        for wellInd in range(fcte_ind.shape[1]):
            for aquiferInd in range(fcte_ind.shape[2]):
                out_label = 'well{}LeakageVolumeAquifer{}'.format(
                    wellInd + 1, aquiferInd + 1)

                self.output_labels.append(out_label)

                if remove_zero_check:
                    out[out_label + '_0'] = self.t0_val

                cumulative_vol = 0
                for timeInd in range(fcte_ind.shape[0]):
                    time_label = '_{}'.format(timeInd + timeInd_adjust)

                    if timeInd == 0:
                        # Here, time_array does not include t = 0 days
                        time_diff_days = time_array[timeInd]
                    else:
                        time_diff_days = time_array[timeInd] - time_array[timeInd - 1]

                    cumulative_vol += (fcte_ind[
                        timeInd, wellInd, aquiferInd] * time_diff_days)

                    out[out_label + time_label] = cumulative_vol

        # Contour plot head values in (m)
        # chead dimensions: [time index, aquifer name index, contour plot location index]
        # In the salsa_wrap() currently available, the chead array provided in the
        # direc dictionary has to be initialized with enough nodes to receive the
        # values provided (enough rows, columns, and layers for [row, column, layer] dimensions).
        # If it is not provided with enough nodes in any of the three dimensions,
        # salsa_wrap() return an  error 'Windows fatal exception: access violation'
        # because the DLL function is called with invalid arguments. Therefore,
        # it is likely that allowing the user to change the dimensions of the
        # contourPlotAquiferHead output would frequently result in errors. To
        # prevent such errors, the user is not allowed to change the dimensions.
        # More than enough nodes are provided in the function. Then, the unused
        # nodes are removed from chead (values of 0).
        _, _, layers_with_zero = np.where(chead == 0)

        if len(layers_with_zero) > 0:
            chead = chead[:, :, :(np.min(layers_with_zero) - 1)]
            cxout = cxout[:(np.min(layers_with_zero) - 1)]
            cyout = cyout[:(np.min(layers_with_zero) - 1)]

        # If remove_zero_check is True, add nans for t = 0 years
        if remove_zero_check:
            chead_v2 = np.zeros((chead.shape[0] + timeInd_adjust,
                                 chead.shape[1], chead.shape[2]))

            chead_v2[1:, :, :] = chead
            chead_v2[0, :, :] = self.t0_val

            chead = chead_v2.copy()
            del chead_v2

        out['contourPlotAquiferHead'] = chead

        self.output_labels.append('contourPlotAquiferHead')

        # Convert contour plot head to pressure for the bottom, middle, and top 
        # of the aquifer(s)
        cpressure = chead.copy()
        cpressure_mid = chead.copy()
        cpressure_top = chead.copy()
        
        for i in range(cpressure.shape[0]):
            for j in range(cpressure.shape[1]):
                for k in range(cpressure.shape[2]):
                    current_depth = aquiferDepths[
                        actual_p['aquiferNamesContourPlots'][j] - 1]
                    
                    current_depth_mid = aquiferMidDepths[
                        actual_p['aquiferNamesContourPlots'][j] - 1]
                    
                    current_depth_top = aquiferTopDepths[
                        actual_p['aquiferNamesContourPlots'][j] - 1]
                    
                    head = chead[i, j, k]
                    
                    cpressure[i, j, k] = self.convert_head_to_pressure(
                        head, current_depth, aquiferDepths, aquiferTopDepths, 
                        shaleDepths, shaleTopDepths, actual_p, pressure_adjustment=pressure_adjustment)
                    
                    cpressure_mid[i, j, k] = self.convert_head_to_pressure(
                        head, current_depth_mid, aquiferDepths, aquiferTopDepths, 
                        shaleDepths, shaleTopDepths, actual_p, pressure_adjustment=pressure_adjustment)
                    
                    cpressure_top[i, j, k] = self.convert_head_to_pressure(
                        head, current_depth_top, aquiferDepths, aquiferTopDepths, 
                        shaleDepths, shaleTopDepths, actual_p, pressure_adjustment=pressure_adjustment)
                    
                    min_pressure = check_for_min_pressure(
                        cpressure[i, j, k], min_pressure)
                    
                    min_pressure = check_for_min_pressure(
                        cpressure_mid[i, j, k], min_pressure)
                    
                    min_pressure = check_for_min_pressure(
                        cpressure_top[i, j, k], min_pressure)
        
        if min_pressure < 0:
            err_msg = ''.join([
                'The SALSA component {} had negative pressures in its results. ', 
                'These results are not valid and should not be used. SALSA pressures ', 
                'are calculated by using the hydraulic head output as well as ', 
                'the top boundary pressure (topBoundaryPressure). Hydraulic head ', 
                'values are relative to a datum, but pressure values must be ', 
                'absolute. This error can occur if the extraction rates used ', 
                'are too high. In other words, the activeWell#QAquifer#Period# ', 
                'parameter has values that are too low (negative for extraction), ', 
                'given the hydraulic head and pressure parameter values provided ', 
                '(aquifer#Head, bottomBoundaryHead, topBoundaryHead, and topBoundaryPressure). ', 
                'This outcome can also be related to the pressurization rate ', 
                'parameters (shale#InitialGamma, shale#Gamma, and aquifer#Gamma). ', 
                'Check your input.']).format(self.name)
            
            logging.error(err_msg)
            raise KeyError(err_msg)

        out['contourPlotAquiferPressure'] = cpressure
        self.output_labels.append('contourPlotAquiferPressure')
        
        out['contourPlotMidAquiferPressure'] = cpressure_mid
        self.output_labels.append('contourPlotMidAquiferPressure')
        
        out['contourPlotTopAquiferPressure'] = cpressure_top
        self.output_labels.append('contourPlotTopAquiferPressure')

        # Contour plot x values in (m)
        # cxout dimensions: [contour plot location index]
        out['contourPlotCoordx'] = cxout

        self.output_labels.append('contourPlotCoordx')

        # Contour plot y values in (m)
        # cyout dimensions: [contour plot location index]
        out['contourPlotCoordy'] = cyout

        self.output_labels.append('contourPlotCoordy')

        # Return dictionary of outputs
        return out

    def reset(self):
        return


def test_salsa_component():
    from datetime import datetime

    __spec__ = None

    logging.basicConfig(level=logging.WARNING)

    start_time = datetime.now()
    now = start_time.strftime('%Y-%m-%d_%H.%M.%S')
    
    output_dir = os.path.join(iam_bc.IAM_DIR, 'output')
    
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    output_dir = os.path.join(
        iam_bc.IAM_DIR, 'output', 'SALSA_test_{datetime}'.format(datetime=now))

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    length_err_msg = err_msg = 'The length of {} should match the length of {}. Check your input.'

    # Set up the inputs for the SALSA component ------------------------------#
    numberOfShaleLayers = 3

    bottomLayerType = 0

    topLayerType = 0

    # The number of aquifer layers is calculated by the SALSA component in this
    # manner, but here it is calculated before the simulation to help with the
    # setup of the model.
    numberOfAquiferLayers = (numberOfShaleLayers + bottomLayerType +
                             topLayerType - 1)

    numberOfActiveWells = 1

    numberOfPeriods = 2

    # Output parameters ------------------------------------------------------#
    numberOfVerticalPoints = 10

    # Aquifers ---------------------------------------------------------------#
    aquifer1Thickness = 50.00
    aquifer2Thickness = 50.00
    aquiferThicknesses = [aquifer1Thickness, aquifer2Thickness]

    aquifer1LogHorizK = np.log10(0.02983924 / SEC_PER_DAY)
    aquifer2LogHorizK = np.log10(0.02983924 / SEC_PER_DAY)
    aquiferLogHorizK_list = [aquifer1LogHorizK, aquifer2LogHorizK]

    aquifer1ANSR = 1.0
    aquifer2ANSR = 1.0
    aquiferANSR_list = [aquifer1ANSR, aquifer2ANSR]

    aquifer1LogSS = np.log10(2.11258e-6)
    aquifer2LogSS = np.log10(2.11258e-6)
    aquiferLogSS_list = [aquifer1LogSS, aquifer2LogSS]

    aquifer1FluidDensity = 1000.00
    aquifer2FluidDensity = 1000.00
    aquiferFluidDensity_list = [aquifer1FluidDensity, aquifer2FluidDensity]

    aquifer1Head = 70.6334
    aquifer2Head = 36.4788
    aquiferHead_list = [aquifer1Head, aquifer2Head]

    aquifer1Gamma = 0.0
    aquifer2Gamma = 0.0
    aquiferGamma_list = [aquifer1Gamma, aquifer2Gamma]

    # Aquitards --------------------------------------------------------------#
    shale1Thickness = 186.00
    shale2Thickness = 186.00
    shale3Thickness = 186.00
    shaleThicknesses = [shale1Thickness, shale2Thickness, shale3Thickness]

    shale1LogVertK = np.log10(8.75166e-7 / SEC_PER_DAY)
    shale2LogVertK = np.log10(8.75166e-7 / SEC_PER_DAY)
    shale3LogVertK = np.log10(8.75166e-7 / SEC_PER_DAY)
    shaleLogVertK_list = [shale1LogVertK, shale2LogVertK, shale3LogVertK]

    shale1LogSS = np.log10(5.38798e-6)
    shale2LogSS = np.log10(5.38798e-6)
    shale3LogSS = np.log10(5.38798e-6)
    shaleLogSS_list = [shale1LogSS, shale2LogSS, shale3LogSS]

    shale1InitialGamma = 0
    shale2InitialGamma = 0
    shale3InitialGamma = 0
    shaleInitialGamma_list = [shale1InitialGamma, shale2InitialGamma, shale3InitialGamma]

    shale1Gamma = 0
    shale2Gamma = 0
    shale3Gamma = 0
    shaleGamma_list = [shale1Gamma, shale2Gamma, shale3Gamma]

    # Domain BCs -------------------------------------------------------------#
    bottomBoundaryCond = 1
    topBoundaryCond = 0
    bottomBoundaryHead = 0.0
    topBoundaryHead = 0.0

    # Active wells -----------------------------------------------------------#
    periodEndTimes = np.array([18250.00, 36500.00 * 100.00]) / 365.25

    numberOfPeriods = len(periodEndTimes)

    activeWellCoordx = np.array([0.00])
    activeWellCoordy = np.array([0.00])

    numberOfActiveWells = len(activeWellCoordx)
    if len(activeWellCoordy) != numberOfActiveWells:
        err_msg = length_err_msg.format(activeWellCoordx, activeWellCoordy)
        raise KeyError(err_msg)

    activeWellRadii = np.zeros((numberOfAquiferLayers, numberOfActiveWells))
    activeWellRadii[0][0] = 0.1
    activeWellRadii[1][0] = 0.1

    activeWellQ = np.zeros((numberOfAquiferLayers, numberOfActiveWells, numberOfPeriods))
    activeWellQ[0][0][0] = 0.00966
    activeWellQ[0][0][1] = 0.0
    activeWellQ[1][0][0] = 0.0
    activeWellQ[1][0][1] = 0.0

    # Leaking well inputs ----------------------------------------------------#
    leakingWellCoordx = [2000]

    leakingWellCoordy = [0]

    numberOfLeakingWells = len(leakingWellCoordx)
    if len(leakingWellCoordy) != numberOfLeakingWells:
        err_msg = length_err_msg.format(leakingWellCoordx, leakingWellCoordy)
        raise KeyError(err_msg)

    # radius in m
    leakingWell_radius = 0.15

    leakingWellAqRadii = np.ones((
        numberOfLeakingWells, numberOfAquiferLayers)) * leakingWell_radius

    leakingWellK_mperday = 0.25

    # 0.25 m/day converts to about -5.54 log10 m/s
    leakingWellLogAqK = np.ones((
        numberOfLeakingWells, numberOfAquiferLayers)) * np.log10(
            leakingWellK_mperday / SEC_PER_DAY)

    leakingWellShaleRadii = np.ones((
        numberOfLeakingWells, numberOfShaleLayers)) * leakingWell_radius

    leakingWellLogShaleK = np.ones((
        numberOfLeakingWells, numberOfShaleLayers)) * np.log10(
            leakingWellK_mperday / SEC_PER_DAY)

    leakingWellShaleStat = np.zeros((numberOfLeakingWells, numberOfAquiferLayers))
    leakingWellShaleStat[0][0] = 1

    leakingWellAqStat = np.ones((numberOfLeakingWells, numberOfAquiferLayers + 1))
    # The values for the last column of leakingWellAqStat should only be 1 if
    # the leaking well is also an active well (i.e., leakingWellQ for that well
    # is nonzero).
    leakingWellAqStat[:, -1] = 0

    leakingWellQ = np.zeros((numberOfLeakingWells, numberOfPeriods))

    # Solution control & time step inputs ------------------------------------#
    numberOfLaplaceTerms = 8

    aquiferCoordx = np.array([1990.00])

    aquiferCoordy = np.array([0.0])

    numberOfHeadLocs = len(aquiferCoordx)
    if len(aquiferCoordy) != numberOfHeadLocs:
        err_msg = 'The length of aquiferCoordx should match the length of aquiferCoordy.'
        raise KeyError(err_msg)

    shaleCoordx = np.array([1990.00])

    shaleCoordy = np.array([0.0])

    numberOfHeadProfiles = len(shaleCoordx)
    if len(shaleCoordy) != numberOfHeadProfiles:
        err_msg = 'The length of shaleCoordx should match the length of shaleCoordy.'
        raise KeyError(err_msg)

    # Inputs for generating the contour plot mesh ----------------------------#
    xCent = 0.0

    yCent = 0.0

    xMax = 3000.0

    yMax = 3000.0

    gridExp = 1.00

    numberOfNodesXDir = 40

    numberOfNodesYDir = 40

    numberOfRadialGrids = 6

    radialZoneRadius = 50.00

    radialGridExp = 1.20

    aquiferNamesContourPlots = [1, 2]

    # Define keyword arguments of the system model
    num_years = 100
    dt = 10 # timestep in years
    time_array = 365.25 * np.arange(0, num_years + dt, dt)

    sm_model_kwargs = {'time_array': time_array} # time is given in days

    # Create system model
    sm = iam_bc.SystemModel(model_kwargs=sm_model_kwargs)

    # Add SALSA component
    salsa = sm.add_component_model_object(SALSA(
        name='salsa', parent=sm, periodEndTimes=periodEndTimes,
        activeWellCoordx=activeWellCoordx, activeWellCoordy=activeWellCoordy,
        leakingWellCoordx=leakingWellCoordx, leakingWellCoordy=leakingWellCoordy,
        aquiferCoordx=aquiferCoordx, aquiferCoordy=aquiferCoordy,
        shaleCoordx=shaleCoordx, shaleCoordy=shaleCoordy,
        aquiferNamesContourPlots=aquiferNamesContourPlots, output_dir=output_dir))

    # Add parameters of the SALSA component
    salsa.add_par('bottomLayerType', value=bottomLayerType, vary=False)
    salsa.add_par('topLayerType', value=topLayerType, vary=False)
    salsa.add_par('bottomBoundaryCond', value=bottomBoundaryCond, vary=False)
    salsa.add_par('bottomBoundaryHead', value=bottomBoundaryHead, vary=False)
    salsa.add_par('topBoundaryCond', value=topBoundaryCond, vary=False)
    salsa.add_par('topBoundaryHead', value=topBoundaryHead, vary=False)
    salsa.add_par('numberOfLaplaceTerms', value=numberOfLaplaceTerms, vary=False)
    salsa.add_par('xCent', value=xCent, vary=False)
    salsa.add_par('yCent', value=yCent, vary=False)
    salsa.add_par('xMax', value=xMax, vary=False)
    salsa.add_par('yMax', value=yMax, vary=False)
    salsa.add_par('gridExp', value=gridExp, vary=False)
    salsa.add_par('numberOfNodesXDir', value=numberOfNodesXDir, vary=False)
    salsa.add_par('numberOfNodesYDir', value=numberOfNodesYDir, vary=False)
    salsa.add_par('numberOfRadialGrids', value=numberOfRadialGrids, vary=False)
    salsa.add_par('radialZoneRadius', value=radialZoneRadius, vary=False)
    salsa.add_par('radialGridExp', value=radialGridExp, vary=False)
    salsa.add_par('numberOfVerticalPoints', value=numberOfVerticalPoints, vary=False)

    salsa.add_par('numberOfShaleLayers', value=numberOfShaleLayers, vary=False)

    # Add parameters for each shale layer
    for shaleRef in range(numberOfShaleLayers):
        salsa.add_par('shale{}Thickness'.format(shaleRef + 1),
                      value=shaleThicknesses[shaleRef], vary=False)
        salsa.add_par('shale{}LogVertK'.format(shaleRef + 1),
                      value=shaleLogVertK_list[shaleRef], vary=False)
        salsa.add_par('shale{}LogSS'.format(shaleRef + 1),
                      value=shaleLogSS_list[shaleRef], vary=False)
        salsa.add_par('shale{}InitialGamma'.format(shaleRef + 1),
                      value=shaleInitialGamma_list[shaleRef], vary=False)
        salsa.add_par('shale{}Gamma'.format(shaleRef + 1),
                      value=shaleGamma_list[shaleRef], vary=False)

    # Add parameters for each aquifer layer
    for aquRef in range(numberOfAquiferLayers):
        salsa.add_par('aquifer{}Thickness'.format(aquRef + 1),
                      value=aquiferThicknesses[aquRef], vary=False)
        salsa.add_par('aquifer{}LogHorizK'.format(aquRef + 1),
                      value=aquiferLogHorizK_list[aquRef], vary=False)
        salsa.add_par('aquifer{}ANSR'.format(aquRef + 1),
                      value=aquiferANSR_list[aquRef], vary=False)
        salsa.add_par('aquifer{}LogSS'.format(aquRef + 1),
                      value=aquiferLogSS_list[aquRef], vary=False)
        salsa.add_par('aquifer{}FluidDensity'.format(aquRef + 1),
                      value=aquiferFluidDensity_list[aquRef], vary=False)
        salsa.add_par('aquifer{}Head'.format(aquRef + 1),
                      value=aquiferHead_list[aquRef], vary=False)
        salsa.add_par('aquifer{}Gamma'.format(aquRef + 1),
                      value=aquiferGamma_list[aquRef], vary=False)

    # Add properties of the active wells
    for aquRef in range(numberOfAquiferLayers):
        for activeWellRef in range(numberOfActiveWells):
            salsa.add_par('activeWell{}RadiusAquifer{}'.format(
                activeWellRef + 1, aquRef + 1),
                value=activeWellRadii[aquRef, activeWellRef], vary=False)

            for periodRef in range(numberOfPeriods):
                salsa.add_par('activeWell{}QAquifer{}Period{}'.format(
                    activeWellRef + 1, aquRef + 1, periodRef + 1),
                    value=activeWellQ[aquRef, activeWellRef, periodRef], vary=False)

    # Add the leaking well properties related to aquifer layers
    for aquRef in range(numberOfAquiferLayers):
        for leakingWellRef in range(numberOfLeakingWells):
            salsa.add_par('leakingWell{}LogKAquifer{}'.format(
                leakingWellRef + 1, aquRef + 1),
                value=leakingWellLogAqK[leakingWellRef, aquRef], vary=False)

            salsa.add_par('leakingWell{}RadiusAquifer{}'.format(
                leakingWellRef + 1, aquRef + 1),
                value=leakingWellAqRadii[leakingWellRef, aquRef], vary=False)

            salsa.add_par('leakingWell{}StatAquifer{}'.format(
                leakingWellRef + 1, aquRef + 1),
                value=leakingWellAqStat[leakingWellRef, aquRef], vary=False)

            # This parameter actually is limited to the number of aquifer layers,
            # because it can only apply to shales that have aquifers above them.
            salsa.add_par('leakingWell{}StatShale{}'.format(
                leakingWellRef + 1, aquRef + 1),
                value=leakingWellShaleStat[leakingWellRef, aquRef], vary=False)

    # Add the leaking well properties related to shale layers
    for shaleRef in range(numberOfShaleLayers):
        for leakingWellRef in range(numberOfLeakingWells):
            salsa.add_par('leakingWell{}LogKShale{}'.format(
                leakingWellRef + 1, shaleRef + 1),
                value=leakingWellLogShaleK[leakingWellRef, shaleRef], vary=False)

            salsa.add_par('leakingWell{}RadiusShale{}'.format(
                leakingWellRef + 1, shaleRef + 1),
                value=leakingWellShaleRadii[leakingWellRef, shaleRef], vary=False)

    # Add leaking well Q
    for leakingWellRef in range(numberOfLeakingWells):
        for periodRef in range(numberOfPeriods):
            salsa.add_par('leakingWell{}QPeriod{}'.format(
                leakingWellRef + 1, periodRef + 1),
                value=leakingWellQ[leakingWellRef, periodRef], vary=False)

    # Add the grid observations
    for grid_obs in SALSA_GRID_OBSERVATIONS:
        salsa.add_grid_obs(
            grid_obs, constr_type='matrix', output_dir=output_dir)

    # Run the system model deterministically
    sm.forward()

    shale1BottomPointHead = sm.collect_observations_as_time_series(
        salsa, 'headProfile1VertPoint1Shale1')
    shale1TopPointHead = sm.collect_observations_as_time_series(
        salsa, 'headProfile1VertPoint{}Shale1'.format(numberOfVerticalPoints))

    print('Time (years): \n', time_array / 365.25)
    print('')

    print('Head Profile 1 at x = {} m, y = {} m: '.format(
        shaleCoordx[0], shaleCoordy[0]))
    print('Hydraulic Head (m) at the Bottom of Shale 1: \n{}'.format(shale1BottomPointHead))
    print('')
    print('Hydraulic Head (m) at the Top of Shale 1: \n{}'.format(shale1TopPointHead))
    print('')

    aquifer1Head = sm.collect_observations_as_time_series(salsa, 'headLoc1Aquifer1')

    print('Aquifer 1 Hydraulic Head (m): \n{}'.format(aquifer1Head))
    print('')

    diffuseLeakageVolumeTopAquifer1 = sm.collect_observations_as_time_series(
        salsa, 'diffuseLeakageVolumeTopAquifer1')

    print('Leakage Volume (m^3) through the Aquifer-Aquitard Interface at '
          + 'the Top of Aquifer 1: \n{}'.format(diffuseLeakageVolumeTopAquifer1))
    print('')

    well1LeakageRateAquifer2 = sm.collect_observations_as_time_series(
        salsa, 'well1LeakageRateAquifer2')

    print('Leakage Rate (m^3) from Well 1 into Aquifer 2: \n{}'.format(
        well1LeakageRateAquifer2))
    print('')

    wellLeakageVolumeAquifer2 = sm.collect_observations_as_time_series(
        salsa, 'wellLeakageVolumeAquifer2')

    print('Total Leakage Volume (m^3) from All Wells into Aquifer 2: \n{}'.format(
        wellLeakageVolumeAquifer2))
    print('')

    if 0 in time_array:
        print('Note that for t = 0 years, SALSA produces values of {} '.format(
            salsa.t0_val) + 'for all metrics other than hydraulic head.')

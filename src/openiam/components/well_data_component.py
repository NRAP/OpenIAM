import os
import logging
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import pandas as pd

# Potential keys used for hole 1, set up so that the code can recognize if the 
# user has entered variations on 'Hole 1'.
HOLE_1_KEYS = ['Hole 1', 'Hole1']

EXPECTED_FORMAT_MSG = ''.join([
    'While processing the data for the WellData component {}, the {} data ', 
    'included a key called {}. This key did not include the term{}, so the key ', 
    'does not match the required format. This input will likely result in errors.'])

# Used for the figure showing the number of plugs along each hole
LINE_STYLES = ['solid', 'dotted', 'dashed', 'dashdot', (0, (1, 5)), 
               (0, (3, 1, 1, 1, 1, 1)), (0, (8, 1))]


class WellBoreData():
    """
    The WellData component takes an input dictionary that descripts the reservoirs, 
    holes (pipes), and plugs along the well. It then processes the data to determine 
    conditions along each hole and annulus (spacing between adjacent holes).
    
    The input dictionary should have the following keys:
        * 'uwi', for unique well identifier
        * 'tvd': true vertical depth to the bottom of the well, in meters
        * 'md': measured depth to the bottom of the well, in meters
        * 'res_data': dictionary containing keys 'Reservoir 1', 'Reservoir 2', 
            ... 'Reservoir N', where N is the number of reservoirs along the well
        * 'hole_data': dictionary containing keys 'Hole 1', 'Hole 2', 
            ... 'Hole N', where N is the number of holes / pipes along the well
        * 'plug_data': dictionary containing keys 'Plug 1', 'Plug 2', 
            ... 'Plug N', where N is the number of plugs along the well
    
    Below, 'tvd' will be used for true vertical depth while 'md' will be used 
    for measured depth. If a wellbore is perfectly straight, then the tvd and 
    md values will be equal. If a well bends, however, the md values will be 
    greater than the tvd values.
    
    Each reservoir entry (e.g., the key 'Reservoir 1') in 'res_data' should 
    have the following keys:
        * 'seal_top_tvd': tvd at the top of the seal, in meters
        * 'seal_bot_tvd': tvd at the bottom of the seal, in meters
        * 'seal_top_md': md at the top of the seal, in meters
        * 'seal_bot_md': md at the bottom of the seal, in meters
        * 'target_top_tvd': tvd at the top of the reservoir (target), in meters
        * 'target_bot_tvd': tvd at the bottom of the reservoir (target), in meters
        * 'target_top_md': md at the top of the reservoir (target), in meters
        * 'target_bot_md': md at the bottom of the reservoir (target), in meters
    
    Each hole entry (e.g., the key 'Hole 1') in 'hole_data' should have the 
    following keys:
        * 'hole_tvd': tvd to the bottom of the hole, in meters
        * 'hole_md': md to the bottom of the hole, in meters
        * 'csg_pres': casing present, True or False
        * 'csg_top_tvd': tvd to the top of the casing, in meters
        * 'csg_bot_tvd': tvd to the bottom of the casing, in meters
        * 'csg_top_md': md to the top of the casing, in meters
        * 'csg_bot_md': md to the bottom of the casing, in meters
        * 'csg_cmt_top_md': md to the top of the cement in the casing, in meters
    
    Each plug entry (e.g., the key 'Plug 1') in 'plug_data' should have the 
    following keys:
        * 'plug_type': type of the plug, either 'Casing' or 'Annulus'
        * 'plug_loc': name of the hole the plug is in (e.g., 'Hole 1'), must 
            match the exact spelling used for the hole_data keys.
        * 'plug_top_md': md to the top of the plug, True or False
        * 'plug_bot_md': md to the bottom of the plug, in meters
    
    A dictionary of well data can be saved to a .yaml file, and the function 
    process_wellbore_data() can then take that file can create plots showing 
    the condition of holes and annuli along the well. For an example of saving 
    an dictionary to a .yaml file, see the script example_well_data.py in 
    /examples/scripts/example_well_data.py.
    """
    def __init__(self, name='welldata1', well_data=None):
        # Assign base well properties from input data
        if not isinstance(name, str):
            err_msg = ''.join([
                'A WellData component was created and given the name {name}, ', 
                'but this name was not in the format of a string variable. ', 
                'This setup may result in errors. Check your input.'])
            logging.error(err_msg)
        
        self.name = name
        
        if well_data is None:
            err_msg = ''.join([
            f'The WellData component {name} was not given well data when created. ', 
            'Well data are provided with the keyword argument well_data. The ', 
            'absence of data will result in errors. Check your input.'])
            logging.error(err_msg)
            
        elif not isinstance(well_data, dict):
            if isinstance(well_data, str):
                try:
                    well_data = pd.read_csv(well_data)
                except:
                    err_msg = ''.join([
                        f'The WellData component {name} was given well data ', 
                        f'in the format of a string variable ({well_data}). ', 
                        'This string input was expected to be the path to a .csv ', 
                        'file containing the data, but the component could not ', 
                        'read data from the well_data input provided. Check ', 
                        'your input.'])
                    logging.error(err_msg)
            else:
                err_msg = ''.join([
                    f'The WellData component {name} was given well data that did not ', 
                    'match the expected format. Well data are provided with the ', 
                    'keyword argument well_data. The well_data keyword argument ', 
                    'is expected to be given as a dictionary or as the path to ', 
                    'a .csv file containing the data, but it was given as the ', 
                    f'type {type(well_data)}. Check your input.'])
                logging.error(err_msg)
        
        # Store the well data
        self.well_data = well_data

        # Create arrays representing the wellbore
        self.create_well_arrays()

        # Categorize well for ROM selection
        self.categorize_well()

    def get_data_for_larger_hole(self, hole_key):
        """
        Obtains a dictionary containing the data for the larger hole around the 
        current hole (e.g., hole 1 is around hole 2).
        """
        larger_hole_data = None
        if hole_key.title() not in HOLE_1_KEYS:
            current_hole_num = int(hole_key[len('Hole'):])
            
            if not current_hole_num == 1:
                larger_hole_num = current_hole_num - 1
                
                larger_hole_data = self.well_data['hole_data'][f'Hole {larger_hole_num}']
        
        return larger_hole_data

    def create_well_arrays(self):
        """
        Method that create arrays for each hole and annulus. The annulus is the 
        space between two pipes / holes.
        
        All of the arrays have one node for every 1 meter of measured depth, 
        so if a hole array has a length (len(hole_arr)) of 250, then that hole 
        has a measured depth of 250 m.
        
        For hole and annulus arrays, specific values correspond with particular 
        states.
        
        For the hole array (hole_arr):
            0: sections not in reservoir or seal that also are without casing 
                or (a) plug(s)
            2: sections with casing
            3: sections with a plug
            5: sections with casing and plug(s)
            10: section in a reservoir, no casing or plug(s)
            12: section in a reservoir and with casing
            13: section in a reservoir and with (a) plug(s)
            15: section in a reservoir, with casing and (a) plug(s)
            20: section in a seal, no casing or plug(s)
            22: section in a seal and with casing
            23: section in a seal and with (a) plug(s)
            25: section in a seal, with casing and (a) plug(s)
        
        The plug array (plug_arr) keeps track of how many plugs are present at 
        each depth.
        
        For the annulus array (ann_arr):
            0: section with no annulus (e.g., largest pipe without another around it.)
            1: section with cement in the annulus
            2: section with casing from the encompassing hole in the annulus
            3: section with cement and casing from the encompassing hole in the 
                annulus
            10: section in a reservoir with no annulus
            11: section in a reservoir with cement in the annulus
            12: section in a reservoir with casing from the encompassing hole 
                in the annulus
            13: section in a reservoir, with cement and casing from the 
                encompassing hole in the annulus
            20: section in a seal, with no annulus
            21: section in a seal with cement in the annulus
            22: section in a seal with casing from the encompassing hole in the 
                annulus
            23: section in a seal, with cement and casing from the encompassing 
                hole in the annulus
        """
        hole_arrays = []
        plug_arrays = []
        ann_arrays = []
        
        for hole_key in self.well_data['hole_data'].keys():
            if not ('Hole' in hole_key or 'hole' in hole_key):
                err_msg = EXPECTED_FORMAT_MSG.format(
                    self.name, 'hole', hole_key, 's "Hole" or "hole"')
                logging.error(err_msg)
            
            hole_data = self.well_data['hole_data'][hole_key]
            
            # Create array of zeros for entire measured depth of hole
            hole_arr = np.zeros(hole_data['hole_md'])

            # Create an array of zeros for entire measured of the annuli
            ann_arr = np.zeros(hole_data['csg_bot_md'])

            #Loop though GCS target reservoirs
            for res_key in self.well_data['res_data'].keys():
                if not ('Reservoir' in res_key or 'reservoir' in res_key):
                    err_msg = EXPECTED_FORMAT_MSG.format(
                        self.name, 'reservoir', res_key, 's "Reservoir" or "reservoir"')
                    logging.error(err_msg)
                res_data = self.well_data['res_data'][res_key]
                
                # Check to see if the hole is in the target reservoir and assign values 
                # Use true vertical depth (tvd) here, as the reservoir depth is 
                # taken as a true vertical depth from the surface.
                hole_arr = self.locate_reservoir(
                    hole_arr, hole_data['hole_tvd'], res_data)
                
                ann_arr = self.locate_reservoir(
                    ann_arr, hole_data['csg_bot_tvd'], res_data)

            # Add a value of 2 where casing is located in the hole array
            hole_arr[hole_data['csg_top_md']:hole_data['csg_bot_md']] = hole_arr[
                     hole_data['csg_top_md']:hole_data['csg_bot_md']] + 2
            
            # Try to get the data for the hole around this hole
            larger_hole_data = None
            try:
                larger_hole_data = self.get_data_for_larger_hole(hole_key)
            except Exception as e:
                err_msg = ''.join([
                    f'While the WellData component {self.name} was processing ', 
                    f'the hole data, there was an error: \n{e}\nCheck your input.'])
                logging.error(err_msg)
            
            if larger_hole_data is not None:
                # Use the data for the current hole and the larger hole around 
                # it to find where cement is located in the annulus (add 1) and 
                # where the casing is located in the annulus (add 2)
                # Add a value of 1 where cement is located in the annulus
                ann_arr[larger_hole_data['csg_cmt_top_md']:hole_data[
                    'csg_bot_md']] = ann_arr[larger_hole_data[
                        'csg_cmt_top_md']:hole_data['csg_bot_md']] + 1
                
                # Add a value of 2 where casing from the encompassing hole is 
                # located in the annulus.
                ann_arr[larger_hole_data['csg_top_md']:hole_data[
                    'csg_bot_md']] = ann_arr[larger_hole_data[
                        'csg_top_md']:hole_data['csg_bot_md']] + 2
            
            # Used to keep track of whether a plug was already added at each depth
            plug_arr = np.zeros((len(hole_arr)))
            depths = np.arange(1, (len(hole_arr)) + 1, 1)

            # Loop through plugs 
            for plug_key in self.well_data['plug_data'].keys():
                # Grab the plug data
                plug_data = self.well_data['plug_data'][plug_key]

                # If plug is located in the casing or hole and the located in the hole
                if (((plug_data['plug_type']=='Casing') or (plug_data['plug_type']=='Hole')) 
                    and (plug_data['plug_loc']==hole_key)):
                    # plug_added[plug_data['plug_top_md']:plug_data[
                    #     'plug_bot_md']] = np.ones((len(plug_added[plug_data[
                    #         'plug_top_md']:plug_data['plug_bot_md']])))
                    
                    for i in range(len(hole_arr)):
                        if (depths[i] >= plug_data['plug_top_md'] 
                            and depths[i] <= plug_data['plug_bot_md'] 
                            and plug_arr[i] == 0):
                            # Add 3 at plug locations
                            hole_arr[i] += 3
                            plug_arr[i] += 1
                        elif (depths[i] >= plug_data['plug_top_md'] 
                            and depths[i] <= plug_data['plug_bot_md'] 
                            and plug_arr[i] != 0):
                            plug_arr[i] += 1
            
            hole_arrays.append(hole_arr)
            plug_arrays.append(plug_arr)
            ann_arrays.append(ann_arr)
        
        self.hole_arrays = hole_arrays
        self.plug_arrays = plug_arrays
        self.ann_arrays = ann_arrays
    
    @staticmethod
    def locate_reservoir(array, depth, res_data):
        """
        Method that assigns values to the hole or annulus array to represent 
        the target GCS reservoirs and seals
        """
        # If the hole/annulus is deeper than the target reservoir bottom
        if depth > res_data['target_bot_tvd']:
            # Assign a value of 20 where the seal is located
            array[res_data['seal_top_md']:res_data['seal_bot_md']] = 20
            
            # Assign a value of 10 where the reservoir is located
            array[res_data['target_top_md']:res_data['target_bot_md']] = 10
        
        # If the hole/annulus is deeper than the target reservoir top but shallower than the bottom
        elif (depth < res_data['target_bot_tvd']) and (depth > res_data['target_top_tvd']):
            # Assign a value of 20 where the seal is located
            array[res_data['seal_top_md']:res_data['seal_bot_md']] = 20
            
            # Assign a value of 10 where the reservoir is located
            array[res_data['target_top_md']:] = 10

        # If the hole/annulus is deeper than the seal bottom but shallower than the target reservoir top
        elif (depth < res_data['target_top_tvd']) and (depth > res_data['seal_bot_tvd']):
            # Assign a value of 20 where the seal is located
            array[res_data['seal_top_md']:res_data['seal_bot_md']] = 20
        
        # If the hole/annulus is deeper than the seal top but shallower than the seal bottom
        elif (depth < res_data['seal_bot_tvd']) and (depth > res_data['seal_top_tvd']):
            # Assign a value of 20 where the seal is located
            array[res_data['seal_top_md']:] = 20

        return array

    def categorize_well(self):  
        """
        Well categories:  1) Above the GCS target - no need to simulate, 
                          2) Open hole design - use open wellbore model
                          3) Cemented well design - use multisegmented wellbore
        
        NOTE: This function is simpler than what we've implemented in the study 
        with Exxon. It's also simpler than what Meng and Ishti did in their study. 
        At this point it's focused directly on what we can and cannot simulate 
        in NRAP-Open-IAM.
        """
        self.well_category_by_res = {}
        
        for res_key in self.well_data['res_data'].keys():
            res_data = self.well_data['res_data'][res_key]
            
            if self.well_data['tvd'] < res_data['target_top_tvd']:
                # Well is too shallow to simulate
                self.well_category_by_res[res_key] = 1
            else:
                # Search through hole and annulus arrays and 
                # Assign categories appropriately
                for ann in self.ann_arrays:
                    if 20 in ann:
                        # A value of 20 in the annulus array means that the 
                        # section is in a seal, and it has no annulus. Take that 
                        # as meaning that the seal has an uncemented annulus, 
                        # and assign a value of 2 (open hole).
                        self.well_category_by_res[res_key] = 2
                    else:
                        # The seal is cemented in the annulus
                        self.well_category_by_res[res_key] = 3

                for hole in self.hole_arrays:
                    if 20 in hole:
                        # A value of 20 in the hole array means that the section 
                        # is in a seal, but it does not have casing or a plug. 
                        # In this case, assign a value of 2 (open hole).
                        self.well_category_by_res[res_key] = 2
        
        self.well_category = 1
        for res_key in self.well_category_by_res.keys():
            if self.well_category_by_res[res_key] == 2:
                self.well_category = 2
                
            elif (self.well_category_by_res[res_key] == 3 
                  and self.well_category != 2):
                self.well_category = 3


def process_wellbore_data(filepath, well_name, savefigs=False, output_dir=None, 
                          dpi_ref=100, fig_extension='.png'):
    """
    Runs the WellData component on a .yaml file containing the well data and 
    creates plots showing the processed results.
    """
    if savefigs and output_dir is not None:
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
    
    WellData = WellBoreData(name='welldata1', well_data=well_data)
    
    num_holes = len(WellData.hole_arrays)
    
    # Set up how the wellbore will be shown in figure 1 (not to scale, in terms 
    # of the well diameter).
    width_central_hole = 0.5
    spacing_btwn_holes = 0.25
    
    x_adjust_for_holes = [0] * num_holes
    x_adjust_for_holes[-1] = width_central_hole
    for hole_ref in range(num_holes - 2, -1, -1):
        x_adjust_for_holes[hole_ref] = x_adjust_for_holes[hole_ref + 1] + spacing_btwn_holes
    
    xLims = [-(max(x_adjust_for_holes) + 5), max(x_adjust_for_holes) + 5]
    
    # Label fontsize
    genfontsize = 10
    lgndfontsize = 6
    txtfontsize = 8
    labelfontsize = 12
    titlefontsize = 12
    
    font = {'family': 'Arial',
            'weight': 'normal',
            'size': genfontsize}
    plt.rc('font', **font)
    
    hole_clr_no_casing_or_plug = 'dimgray'
    hole_clr_casing = 'firebrick'
    hole_clr_plug = 'darkblue'
    hole_clr_casing_and_plug = 'gold'
    
    ann_clr_no_ann = 'dimgray'
    ann_clr_cmnt = 'darkorange'
    ann_clr_enc = 'darkturquoise'
    ann_clr_cmnt_and_enc = 'deeppink'
    
    linewidth = 2
    linestyle_1 = '-'
    linewidth_1 = 1
    linestyle_2 = '--'
    linewidth_2 = 3
    linestyle_3 = (0, (1, 0.25))
    linewidth_3 = 8
    
    BACKGROUND_COLOR = [0.67, 0.67, 0.67]
    
    # Plot of holes with depths shown
    _ = plt.figure(1, figsize=(10, 6.67), dpi=200)
    
    # Plot of holes with conditions shown
    _ = plt.figure(2, figsize=(10, 6.67), dpi=200)
    
    # Plot of holes with the number of plugs shown
    _ = plt.figure(3, figsize=(10, 6.67), dpi=200)
    
    # Plot of annuli with conditions shown
    _ = plt.figure(4, figsize=(10, 6.67), dpi=200)
    
    def plot_array_depths_by_val(ax, val, array, depths, color, linestyle, 
                                 linewidth, x_adjustment):
        """
        Plots sections of the hole that meet specific criteria. The value (val) 
        specified corresponds with the conditions specified in the 
        create_well_arrays() method.
        """
        depths_clip = depths.copy()
        
        if len(depths_clip[array == val]) != 0:
            depths_clip = depths_clip[array == val]
            
            # Separate depths_clip into continuous sections, otherwise the 
            # line will be plotted along sections of the hole where there is a 
            # gap in the depths_clip data. Sections of depths_clips are continuous 
            # if the spacing between values is 1 m (i.e., if each node is 1 m 
            # lower than the previous node).
            groups = np.zeros((len(depths_clip)))
            current_group = 1
            groups_list = [current_group]
            groups[0] = current_group
            for i in range(1, len(depths_clip)):
                if depths_clip[i] != depths_clip[i - 1] - 1:
                    current_group += 1
                    groups_list.append(current_group)
                
                groups[i] = current_group
            
            for group_num in groups_list:
                ax.plot(
                    np.ones((len(depths_clip[groups == group_num]))) * x_adjustment, 
                    depths_clip[groups == group_num], color=color, 
                    linewidth=linewidth, linestyle=linestyle)
                
                ax.plot(
                    np.ones((len(depths_clip[groups == group_num]))) * -x_adjustment, 
                    depths_clip[groups == group_num], color=color, 
                    linewidth=linewidth, linestyle=linestyle)
    
    # Entries correspond with the hole array values.
    hole_val_list = [0, 2, 3, 5, 10, 12, 13, 15, 20, 22, 23, 25]
    
    hole_clr_list = [
        hole_clr_no_casing_or_plug, hole_clr_casing, hole_clr_plug, hole_clr_casing_and_plug, 
        hole_clr_no_casing_or_plug, hole_clr_casing, hole_clr_plug, hole_clr_casing_and_plug, 
        hole_clr_no_casing_or_plug, hole_clr_casing, hole_clr_plug, hole_clr_casing_and_plug, 
        ]
    
    hole_lb_list = [
        'Sections without casing or a plug', 'Sections with casing', 
        'Sections with a plug','Sections with casing and a plug', 
        'Sections in a reservoir without casing or a plug', 'Sections in a reservoir with casing', 
        'Sections in a reservoir with a plug', 'Sections in a reservoir with casing and a plug', 
        'Sections in a seal without casing or a plug', 'Sections in a seal with casing', 
        'Sections in a seal with a plug', 'Sections in a seal with casing and a plug',
        ]
    
    hole_ls_list = [linestyle_1] * 4 + [linestyle_2] * 4 + [linestyle_3] * 4
    
    hole_lw_list = [linewidth_1] * 4 + [linewidth_2] * 4 + [linewidth_3] * 4
    
    ann_val_list = [0, 1, 2, 3, 10, 11, 12, 13, 20, 21, 22, 23]

    ann_clr_list = [
        ann_clr_no_ann, ann_clr_cmnt, ann_clr_enc, ann_clr_cmnt_and_enc, 
        ann_clr_no_ann, ann_clr_cmnt, ann_clr_enc, ann_clr_cmnt_and_enc, 
        ann_clr_no_ann, ann_clr_cmnt, ann_clr_enc, ann_clr_cmnt_and_enc, 
        ]

    ann_lb_list = [
        'Section without an annulus', 
        'Section with cement in the annulus', 
        'Casing from the encompassing hole\nin the annulus', 
        'Cement and casing from the\nencompassing hole in the annulus', 
        'In a reservoir without an annulus', 
        'In a reservoir with cement in the annulus', 
        'In a reservoir with casing from the\nencompassing hole in the annulus', 
        'In a reservoir, with cement and casing from\nthe encompassing hole in the annulus', 
        'Section in a seal, without an annulus', 
        'Section in a seal with cement in the annulus', 
        'In a seal with casing from the\nencompassing hole in the annulus', 
        'In a seal, with cement and casing from the\nencompassing hole in the annulus',
        ]

    ann_ls_list = hole_ls_list.copy()

    ann_lw_list = hole_lw_list.copy()
    
    for ind, (hole_arr, plug_arr, ann_arr) in enumerate(zip(
            WellData.hole_arrays, WellData.plug_arrays, WellData.ann_arrays)):
        # Hole arrays have lengths equal to the measured depth to the bottom of 
        # the hole, in meters.
        hole_depths = -1 * np.arange(1, (len(hole_arr)) + 1, 1)
        
        # Annulus arrays have lengths equal to the measured depth to the botrtom 
        # of the casing, in meters.
        ann_depths = -1 * np.arange(1, (len(ann_arr)) + 1, 1)
        
        hole_clr = f'C{ind}'
        
        plt.figure(1)
        ax = plt.gca()
        
        hole_bottom_md = WellData.well_data['hole_data'][f'Hole {ind+1}']['hole_md']
        
        a, b = '{:.2e}'.format(np.min(hole_bottom_md)).split('e')
        b = int(b)
        hole_bottom_md_str = r'${}\times10^{{{}}}$'.format(a, b)
        
        
        ax.plot(np.ones((len(hole_depths))) * x_adjust_for_holes[ind], hole_depths, 
                color=hole_clr, label=f'Hole {ind + 1}, Bottom: -{hole_bottom_md_str} m', 
                linewidth=linewidth)
        
        ax.plot(np.ones((len(hole_depths))) * -x_adjust_for_holes[ind], hole_depths, 
                color=hole_clr, linewidth=linewidth)
        
        # Plot conditions along the holes
        plt.figure(2)
        ax = plt.gca()
        
        for hole_ind, (val, clr, lb, lw, ls) in enumerate(zip(
                hole_val_list, hole_clr_list, hole_lb_list, hole_lw_list, hole_ls_list)):
            plot_array_depths_by_val(
                ax, val, hole_arr, hole_depths, clr, ls, lw, x_adjust_for_holes[ind])
        
        # Plot number of plugs along the holes
        plt.figure(3)
        ax = plt.gca()
        
        # Sections not in a reservoir or seal that also lack casing or a plug
        linestyle_ind = 0
        color_count = -1
        for plug_num in range(int(max(plug_arr)) + 1):
            if (color_count + 1) > 10:
                color_count = 0 
                
                if (linestyle_ind + 1) >= len(LINE_STYLES):
                    linestyle_ind = 0
                else:
                    linestyle_ind += 1
            color_count += 1
            
            color = f'C{color_count}'
            linestyle = LINE_STYLES[linestyle_ind]
            
            plot_array_depths_by_val(
                ax, plug_num, plug_arr, hole_depths, color, 
                linestyle, linewidth, x_adjust_for_holes[ind])
        
        # Plot conditions along the annuli
        plt.figure(4)
        ax = plt.gca()
        
        for ann_ind, (val, clr, lb, lw, ls) in enumerate(zip(
                ann_val_list, ann_clr_list, ann_lb_list, ann_lw_list, ann_ls_list)):
            plot_array_depths_by_val(
                ax, val, ann_arr, ann_depths, clr, ls, lw, x_adjust_for_holes[ind])
        
        # Plot hole and annulus array values for each hole
        plt.figure(ind + 5, figsize=(15, 10), dpi=200)
        ax1 = plt.subplot(1, 2, 1)
        
        ax1.plot(hole_arr, hole_depths, linestyle='none', marker='o')
        ax1.set_xticks(range(26))
        ax1.grid(alpha=0.25)
        
        ax1.set_xlabel('Hole Array Value', fontsize=labelfontsize, fontweight='bold')
        ax1.set_ylabel('Measured Depth (m)', fontsize=labelfontsize, fontweight='bold')
        ax1.set_title(f'Hole Data for Hole {ind + 1}', fontsize=titlefontsize, fontweight='bold')
        ax1.set_xlim([-1, 26])
        
        ax1_ylims = ax1.get_ylim()
        
        ax2 = plt.subplot(1, 2, 2)
        
        ax2.plot(ann_arr, ann_depths, linestyle='none', marker='o')
        ax2.set_xticks(range(26))
        ax2.grid(alpha=0.25)
        
        ax2.set_xlabel('Annulus Array Value', fontsize=labelfontsize, fontweight='bold')
        ax2.set_title(f'Annulus Data for Hole {ind + 1}', fontsize=titlefontsize, fontweight='bold')
        ax2.set_xlim([-1, 26])
        
        ax2_ylims = ax2.get_ylim()
        ax1.set_ylim([np.min([ax1_ylims, ax2_ylims]), np.max([ax1_ylims, ax2_ylims])])
        ax2.set_ylim([np.min([ax1_ylims, ax2_ylims]), np.max([ax1_ylims, ax2_ylims])])
        
        if savefigs and output_dir is not None:
            plt.savefig(os.path.join(
                output_dir, f'Array_Values_Hole_{ind + 5}' + fig_extension), 
                dpi=dpi_ref)
    
    def plot_surf_and_res(ax, WellData):
        """
        Plots lines showing the surface and each reservoir, as well as text 
        labels for the reservoirs.
        """
        fig_xLims = ax.get_xlim()
        color = [0.125, 0.125, 0.125]
        
        # Plot a line along the surface
        ax.plot(fig_xLims, [0, 0], color=color, linewidth=0.5)
        ax.text(fig_xLims[0] + ((fig_xLims[1] - fig_xLims[0]) / 20), 
                0, 'Surface', fontsize=txtfontsize, fontweight='bold', 
                color=color)
        
        # Plot horizontal lines along the top and bottom of each reservoir
        for res_key in WellData.well_data['res_data'].keys():
            # TODO - Note: using measured depth here to be consistent with the 
            # well data.
            res_top = WellData.well_data['res_data'][res_key]['target_top_md']
            ax.plot(fig_xLims, [-res_top, -res_top], color=color, linewidth=0.5)
            
            res_bot = WellData.well_data['res_data'][res_key]['target_bot_md']
            ax.plot(fig_xLims, [-res_bot, -res_bot], color=color, linewidth=0.5)
            ax.text(fig_xLims[0] + ((fig_xLims[1] - fig_xLims[0]) / 20), 
                    -res_bot, res_key, fontsize=txtfontsize, fontweight='bold', 
                    color=color)
    
    # Figure showing hole names and depths
    plt.figure(1)
    
    ax = plt.gca()
    ax.set_facecolor(BACKGROUND_COLOR)
    ax.set_ylabel('Measured Depth (m)', fontsize=labelfontsize, fontweight='bold')
    ax.set_xlim(xLims)
    ax.set_xticks([])
    ax.set_title('Depth Data for ' + well_name, fontsize=titlefontsize, fontweight='bold')
    
    plot_surf_and_res(ax, WellData)
    ax.legend(fontsize=lgndfontsize, loc='lower right', framealpha=1, edgecolor=[0, 0, 0])
    
    fig1_ylims = ax.get_ylim()
    
    if savefigs and output_dir is not None:
        plt.savefig(os.path.join(output_dir, 'Wellbore_Hole_Depths' + fig_extension), 
                    dpi=dpi_ref)  
    
    # Figure showing hole conditions
    plt.figure(2)
    
    ax = plt.gca()
    ax.set_facecolor(BACKGROUND_COLOR)
    ax.set_ylabel('Measured Depth (m)', fontsize=labelfontsize, fontweight='bold')
    ax.set_xlim(xLims)
    ax.set_xticks([])
    ax.set_title('Hole Data for ' + well_name, fontsize=titlefontsize, fontweight='bold')
    
    plot_surf_and_res(ax, WellData)
    ax.set_ylim(fig1_ylims)
    
    handle_list = []
    label_list = []
    
    for hole_ind, (clr, lb, lw, ls) in enumerate(zip(
            hole_clr_list, hole_lb_list, hole_lw_list, hole_ls_list)):
        handle_list.append(Line2D([0], [0], color=clr, lw=lw, linestyle=ls, label=lb))
        label_list.append(lb)
    
    ax.legend(handle_list, label_list, fancybox=False, 
              fontsize=lgndfontsize, edgecolor=[0, 0, 0], 
              loc='lower right', framealpha=1, handlelength=5)
    
    if savefigs and output_dir is not None:
        plt.savefig(os.path.join(output_dir, 'Wellbore_Hole_Conditions' + fig_extension), 
                    dpi=dpi_ref)
    
    # Figure showing number of plugs
    plt.figure(3)
    ax = plt.gca()
    ax.set_facecolor(BACKGROUND_COLOR)
    ax.set_ylabel('Measured Depth (m)', fontsize=labelfontsize, fontweight='bold')
    ax.set_xlim(xLims)
    ax.set_xticks([])
    ax.set_title('Plug Data for ' + well_name, fontsize=titlefontsize, fontweight='bold')
    
    plot_surf_and_res(ax, WellData)
    ax.set_ylim(fig1_ylims)
    
    handle_list = []
    label_list = []
    
    linestyle_ind = 0
    color_count = -1
    max_plugs = int(np.max([np.max(plug_arr) for plug_arr in WellData.plug_arrays]))
    for plug_num in range(max_plugs + 1):
        if (color_count + 1) > 10:
            color_count = 0 
            
            if (linestyle_ind + 1) >= len(LINE_STYLES):
                linestyle_ind = 0
            else:
                linestyle_ind += 1
        color_count += 1
        
        label = f'{plug_num} Plugs'
        color = f'C{color_count}'
        linestyle = LINE_STYLES[linestyle_ind]
        legend_element = Line2D(
            [0], [0], color=color, lw=linewidth, linestyle=linestyle, 
            label=label)
        
        handle_list.append(legend_element)
        label_list.append(label)
    
    ax.legend(handle_list, label_list, fancybox=False, 
              fontsize=lgndfontsize, edgecolor=[0, 0, 0], 
              loc='lower right', framealpha=1, handlelength=5)
    
    if savefigs and output_dir is not None:
        plt.savefig(os.path.join(output_dir, 'Wellbore_Plugs' + fig_extension), 
                    dpi=dpi_ref)
    
    # Figure annuli conditions
    plt.figure(4)
    ax = plt.gca()
    
    ax.set_facecolor(BACKGROUND_COLOR)
    ax.set_ylabel('Measured Depth (m)', fontsize=labelfontsize, fontweight='bold')
    ax.set_xlim(xLims)
    ax.set_xticks([])
    ax.set_title('Annulus Data for ' + well_name, fontsize=titlefontsize, fontweight='bold')
    
    plot_surf_and_res(ax, WellData)
    ax.set_ylim(fig1_ylims)
    
    handle_list = []
    label_list = []
    
    for hole_ind, (clr, lb, lw, ls) in enumerate(zip(
            ann_clr_list, ann_lb_list, ann_lw_list, ann_ls_list)):
        handle_list.append(Line2D([0], [0], color=clr, lw=lw, linestyle=ls, label=lb))
        label_list.append(lb)
    
    ax.legend(handle_list, label_list, fancybox=False, 
              fontsize=lgndfontsize, edgecolor=[0, 0, 0], 
              loc='lower right', framealpha=1, handlelength=5)
    
    if savefigs and output_dir is not None:
        plt.savefig(os.path.join(output_dir, 'Wellbore_Annulus_Conditions' + fig_extension), 
                    dpi=dpi_ref)
    
    return WellData


if __name__ == "__main__":
    def load_well_data(file):
        with open(file, 'r') as yaml_cf:
            well_data = yaml.load(yaml_cf, Loader=yaml.SafeLoader)
        
        return well_data
    
    import yaml
    
    try:
        from openiam.components.iam_base_classes import IAM_DIR
    except:
        raise ImportError('Error while attempting to import NRAP-Open-IAM data, check the setup of your environment.')
    
    filename = 'ExampleWellData.yaml'
    try:
        file_dir = os.path.join(IAM_DIR, 'examples', 'scripts', 'example_well_data')
        
        filepath = os.path.join(file_dir, filename)
        
        well_data = load_well_data(filepath)
        
        well_name = 'Example Well'
        
        WellData = process_wellbore_data(well_data, well_name, savefigs=True, 
                                         output_dir=file_dir, dpi_ref=300)
        
    except Exception as e:
        err_msg = ''.join([
            'There was an error while attempting to use the WellData compoenent ', 
            f'to process well data from the file {filename}. Check that the ', 
            f'file is set up properly. Error message: \n{e}'])
        logging.error(err_msg)
        raise KeyError(err_msg)

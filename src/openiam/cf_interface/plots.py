import os
import sys
import logging
import matplotlib.font_manager as fm

try:
    from openiam.components.atmRom_component import AtmosphericROM
    import openiam.visualization as iam_vis
    from openiam.components.iam_base_classes import IAM_DIR
except ImportError as err:
    print('Unable to load NRAP-Open-IAM module: {}'.format(err))


PLOT_TYPES = [
    'TimeSeries', 'TimeSeriesStats', 'TimeSeriesAndStats', 'AtmPlumeSingle', 
    'AtmPlumeEnsemble', 'StratigraphicColumn', 'Stratigraphy', 'AoR', 'TTFD', 
    'GriddedRadialMetric', 'GriddedMetric', 'Bowtie', 'SalsaProfile', 
    'SalsaTimeSeries', 'SalsaContourPlot', 'SalsaLeakageAoR']

FIGURE_SIZES = {
    'TimeSeries': (13, 8), 'TimeSeriesStats': (13, 8), 'TimeSeriesAndStats': (13, 8), 
    'AtmPlumeSingle': (15, 10), 'AtmPlumeEnsemble': (15, 10), 
    'StratigraphicColumn': (6, 10), 'Stratigraphy': (12, 10), 'AoR': (10, 8), 
    'TTFD': (10, 8), 'GriddedRadialMetric': (10, 8), 'GriddedMetric': (10, 8), 
    'Bowtie': (15, 10), 'SalsaProfile': (10, 12), 'SalsaTimeSeries': (10, 12), 
    'SalsaContourPlot': (10, 8), 'SalsaLeakageAoR': (10, 8)}

TIME_SERIES_PLOT_TYPES = {
    'TimeSeries': ['real'], 'TimeSeriesStats': ['stats'], 
    'TimeSeriesAndStats': ['real', 'stats']}

SALSA_PLOTS = ['SalsaProfile', 'SalsaTimeSeries', 'SalsaContourPlot', 
               'SalsaLeakageAoR']

# PLOT_TYPE_ONE plots all have their options indented under the plot name.
PLOT_TYPE_ONE = ['TimeSeries', 'TimeSeriesStats', 'TimeSeriesAndStats', 'AoR']

# PLOT_TYPE_TWO plots all have their options indented under the plot type, which 
# is then indented under the plot name.
PLOT_TYPE_TWO = ['AtmPlumeSingle', 'AtmPlumeEnsemble', 'StratigraphicColumn', 
                 'Stratigraphy', 'TTFD', 'GriddedRadialMetric', 'GriddedMetric', 
                 'Bowtie', 'SalsaProfile', 'SalsaTimeSeries', 'SalsaContourPlot', 
                 'SalsaLeakageAoR']


def process_plots(yaml_data, model_data, sm, s, output_list, analysis,
                  time_array, components, locations):
    """
    Analyze control file setup related to the plots setup to determine
    what kind of plots need to be produced.
    """
    output_dir = os.path.join(IAM_DIR, model_data['OutputDirectory'])

    plots = yaml_data['Plots']
    for p in plots:
        # Figure name
        save_filename = plots[p].get('savefig', p)  # plot name p is default file name
        save_filename = os.path.join(output_dir, save_filename)

        current_plot_type, plot_type_found = get_plot_type(plots[p], p)

        if plot_type_found:
            # Title of plot
            title = get_title(current_plot_type, plots[p], p)

            # Figure size
            figsize, plots[p] = get_fig_size(current_plot_type, plots[p], p)

            if 'Data' in plots[p]:
                # Keep Data keyword for backward compatibility
                plots[p]['TimeSeries'] = plots[p]['Data']

            # Check if any font options were given
            gen_font_size, x_axis_label_font_size, y_axis_label_font_size, \
                z_axis_label_font_size, c_axis_font_size, title_font_size, \
                    legend_font_size, fontname = check_font_options(
                        plots, p, current_plot_type)

            if current_plot_type in ['TimeSeries', 'TimeSeriesStats', 'TimeSeriesAndStats']:
                # Check for options specific to the TimeSeries plot
                subplot, savespace = check_time_series_options(plots, p)

                iam_vis.time_series_plot(
                    plots[p][current_plot_type], sm, s, plots[p], output_list, name=p, 
                    analysis=analysis, savefig=save_filename, title=title, 
                    subplot=subplot, plot_type=TIME_SERIES_PLOT_TYPES[current_plot_type],
                    figsize=figsize, savespace=savespace, title_font_size=title_font_size, 
                    x_axis_label_font_size=x_axis_label_font_size, 
                    y_axis_label_font_size=y_axis_label_font_size, gen_font_size=gen_font_size,
                    legend_font_size=legend_font_size, fontname=fontname)

            if current_plot_type == 'AtmPlumeSingle':
                satm = find_atm_comp(components)

                iam_vis.map_plume_plot_single(
                    plots[p], p, sm, s, satm, time_array, output_dir, 
                    analysis=analysis, figsize=figsize, genfontsize=gen_font_size, 
                    xaxislabelfontsize=x_axis_label_font_size, 
                    yaxislabelfontsize=y_axis_label_font_size, 
                    titlefontsize=title_font_size, suptitle=title,
                    legendfontsize=legend_font_size, fontname=fontname)

            if current_plot_type == 'AtmPlumeEnsemble':
                satm = find_atm_comp(components)

                iam_vis.map_plume_plot_ensemble(
                    plots[p], p, sm, s, satm, time_array, output_dir, 
                    analysis=analysis, figsize=figsize, genfontsize=gen_font_size,
                    xaxislabelfontsize=x_axis_label_font_size, 
                    yaxislabelfontsize=y_axis_label_font_size, 
                    caxislabelfontsize=c_axis_font_size, titlefontsize=title_font_size,
                    legendfontsize=legend_font_size, fontname=fontname, suptitle=title)

            if current_plot_type == 'StratigraphicColumn':
                iam_vis.stratigraphic_column(
                    yaml_data, sm, name=p, savefig=save_filename, title=title,
                    figsize=figsize, genfontsize=gen_font_size,
                    xaxislabelfontsize=x_axis_label_font_size,
                    yaxislabelfontsize=y_axis_label_font_size,
                    titlefontsize=title_font_size, fontname=fontname)

            if current_plot_type == 'Stratigraphy':
                iam_vis.stratigraphy_plot(
                    yaml_data, model_data, sm, name=p, savefig=save_filename,
                    title=title, figsize=figsize, genfontsize=gen_font_size,
                    xaxislabelfontsize=x_axis_label_font_size,
                    yaxislabelfontsize=y_axis_label_font_size,
                    zaxislabelfontsize=z_axis_label_font_size,
                    titlefontsize=title_font_size, fontname=fontname)

            if current_plot_type == 'AoR':
                iam_vis.area_of_review_plot(
                    yaml_data, model_data, plots[p]['AoR'], sm, s, output_list,
                    locations, name=p, analysis=analysis, savefig=save_filename,
                    title=title, figsize=figsize, save_results=True, legend_font_size=legend_font_size,
                    gen_font_size=gen_font_size, x_axis_label_font_size=x_axis_label_font_size,
                    y_axis_label_font_size=y_axis_label_font_size,
                    c_axis_label_font_size=c_axis_font_size,
                    title_font_size=title_font_size, fontname=fontname)

            if current_plot_type == 'TTFD':
                iam_vis.ttfd_plot(
                    yaml_data, model_data, sm, s, output_dir, name=p, analysis=analysis,
                    figsize=figsize, genfontsize=gen_font_size, 
                    xaxislabelfontsize=x_axis_label_font_size, 
                    yaxislabelfontsize=y_axis_label_font_size, 
                    caxislabelfontsize=c_axis_font_size, titlefontsize=title_font_size, 
                    legendfontsize=legend_font_size, fontname=fontname, boldlabels=True)

            if current_plot_type == 'GriddedRadialMetric':
                iam_vis.gridded_radial_metric_plot(
                    yaml_data, sm, output_dir, name=p, analysis=analysis, 
                    savefig=save_filename, figsize=figsize, genfontsize=gen_font_size, 
                    xaxislabelfontsize=x_axis_label_font_size, 
                    yaxislabelfontsize=y_axis_label_font_size, suptitle=title,
                    caxislabelfontsize=c_axis_font_size, titlefontsize=title_font_size, 
                    legendfontsize=legend_font_size, fontname=fontname, boldlabels=True)

            if current_plot_type == 'GriddedMetric':
                iam_vis.gridded_metric_plot(
                    yaml_data, model_data, sm, s, output_dir, name=p, analysis=analysis, 
                    savefig=save_filename, figsize=figsize, genfontsize=gen_font_size, 
                    xaxislabelfontsize=x_axis_label_font_size, 
                    yaxislabelfontsize=y_axis_label_font_size, 
                    caxislabelfontsize=c_axis_font_size,
                    titlefontsize=title_font_size, legendfontsize=legend_font_size, 
                    fontname=fontname, suptitle=title, boldlabels=True)
            
            if current_plot_type == 'Bowtie':
                iam_vis.bowtie_plot(
                    yaml_data, model_data, sm, s, output_list, locations, output_dir, 
                    name=p, analysis=analysis, figsize=figsize, genfontsize=gen_font_size, 
                    titlefontsize=title_font_size, fontname=fontname, boldlabels=True)
            
            if current_plot_type in SALSA_PLOTS:
                iam_vis.salsa_plots(
                    sm, s, output_dir, yaml_data['Plots'][p], model_data, 
                    yaml_data, name=p, analysis=analysis, title=title, genfontsize=gen_font_size, 
                    xaxisfontsize=x_axis_label_font_size, yaxisfontsize=y_axis_label_font_size, 
                    caxisfontsize=c_axis_font_size, titlefontsize=title_font_size, 
                    lgndfontsize=legend_font_size, fontname=fontname, 
                    plot_type=current_plot_type, figsize=figsize)


def check_font_options(plots, p, current_plot_type, default_font='Arial'):
    """
    Checks if a font option has been included in the plot entry. If an option 
    has been given, it also checks if the input is valid. If the input is not 
    valid, it is reset and returned. Font names would be set to the default font 
    type, while font size options are set to None (which causes those inputs to 
    be ignored.)
    """
    def return_val_if_key_present(key, plot_data):
        value = None
        if key == 'FontName':
            value = default_font

        if key in plot_data:
            value = plot_data[key]

            if key == 'FontName':
                value = check_if_font_available(value, key, default_font, p)
            else:
                # The other font options are all font size (numbers)
                value = check_if_number(value, key, p)

        return value

    plot_data = plots[p]
    if current_plot_type in PLOT_TYPE_TWO:
        plot_data = plots[p][current_plot_type]

    gen_font_size = return_val_if_key_present('GenFontSize', plot_data)
    x_axis_label_font_size = return_val_if_key_present('XAxisLabelFontSize', plot_data)
    y_axis_label_font_size = return_val_if_key_present('YAxisLabelFontSize', plot_data)
    z_axis_label_font_size = return_val_if_key_present('ZAxisLabelFontSize', plot_data)
    c_axis_font_size = return_val_if_key_present('ColorbarFontSize', plot_data)
    title_font_size = return_val_if_key_present('TitleFontSize', plot_data)
    legend_font_size = return_val_if_key_present('LegendFontSize', plot_data)
    fontname = return_val_if_key_present('FontName', plot_data)

    return gen_font_size, x_axis_label_font_size, y_axis_label_font_size, \
        z_axis_label_font_size, c_axis_font_size, title_font_size, \
            legend_font_size, fontname


def check_if_number(value, key, p):
    """
    Checks if a value can be converted into a float. If not, the value is 
    returned as None.
    """
    try:
        value = float(value)
    except:
        warning_msg = ''.join([
            f'For the plot {p}, the {key} entry was given as {value} with type ', 
            f'{type(value)}. This input could not be converted into a float value ', 
            '(number), so it will not be used. Check your input.'])
        logging.warning(warning_msg)
        value = None

    return value


def check_if_font_available(font_name, key, default_font, p):
    """
    Checks if a FontName input given is an acceptable font type for matplotlib. 
    If so, returns True. If not, returns False.
    """
    try:
        _ = fm.findfont(font_name, fallback_to_default=False)
    except ValueError:
        warning_msg = ''.join([
            f'For the plot {p}, the {key} input was given as {font_name}. ', 
            'This input did not match any expected font names for Matplotlib, ', 
            f'so the default font of {default_font} will be used. Check your input.'])
        logging.warning(warning_msg)

        font_name = default_font

    return font_name


def check_time_series_options(plots, p, default_use=True, default_save_space=True):
    """
    Checks for additional options related to the time series plot type. Returns 
    any inputs provided
    """
    subplot_check = False
    if 'Subplot' in plots[p]:
        if isinstance(plots[p]['Subplot'], dict):
            subplot = plots[p].get('Subplot', {'use': default_use})
            subplot_check = True
    elif 'subplot' in plots[p]:
        if isinstance(plots[p]['subplot'], dict):
            subplot = plots[p].get('subplot', {'use': default_use})
            subplot_check = True

    if not subplot_check:
        subplot = {'use': default_use}

    savespace = default_save_space
    if ('SaveSpace' in subplot):
        savespace = subplot['SaveSpace']
        savespace = check_if_bool(savespace, 'SaveSpace', 'True', p)

    return subplot, savespace


def check_if_bool(value, key, default_value, p):
    """
    Checks if the value given is boolean. If not, logs a warning message and 
    sets the value to the default value.
    """
    if not isinstance(value, bool):
        warning_msg = ''.join([
            f'For the plot {p}, rhe input {key} was given as {value} with type ', 
            f'{type(value)}. This input is expected to be given as a boolean, ', 
            'however, so the default value of {default_value} will be used. ', 
            'Check your input.'])
        logging.warning(warning_msg)

        value = default_value

    return value


def find_atm_comp(components):
    """ Return AtmosphericROM component if it was added to the system model."""

    for comp in components[::-1]:
        if isinstance(comp, AtmosphericROM):
            atm_comp = comp
            break
    else:
        atm_comp = None
        logging.warning('Unable to find Atmospheric ROM for plume plots')

    return atm_comp


def get_plot_type(plot_yaml_input, name):
    """
    Returns the plot type contained in the current plot entry.
    """
    current_plot_type = None
    continue_check = True
    for plot_type in PLOT_TYPES:
        if plot_type in plot_yaml_input and continue_check:
            current_plot_type = plot_type
            continue_check = False
    
    plot_type_found = True
    if not current_plot_type in PLOT_TYPES:
        err_msg = ''.join([
            'The plot {} was not given one of the expected plot types ({}), ', 
            'and the plot will not be made. Check your input.']).format(
                name, current_plot_type, PLOT_TYPES)
        logging.error(err_msg)
        
        plot_type_found = False
    
    return current_plot_type, plot_type_found


def get_fig_size(current_plot_type, plot_yaml_input, name):
    """
    The position of the FigureSize entry depends on the plot type. TimeSeries,
    TimeSeriesStats, TimeSeriesAndStats, and AoR figures have the FigureSize indented
    under the plot name. The other plot types that accept the FigureSize entry
    have it indented under the plot type (which is in turn
    indented under the plot name).
    """
    figsize_warning_msg = ''.join([
        'The "FigureSize" input ({}) provided for the plot {} could ', 
        'not be converted into a tuple. The input will not be used.'])
    
    figsize = None
    
    if current_plot_type in PLOT_TYPE_ONE:
        if isinstance(plot_yaml_input, dict):
            if 'FigureSize' in plot_yaml_input:
                figsize = plot_yaml_input['FigureSize']
    
    elif current_plot_type in PLOT_TYPE_TWO:
        if isinstance(plot_yaml_input[current_plot_type], dict):
            if 'FigureSize' in plot_yaml_input[current_plot_type]:
                figsize = plot_yaml_input[current_plot_type]['FigureSize']
    
    if figsize is not None:
        try:
            figsize = tuple(figsize)
        except:
            warning_msg = figsize_warning_msg.format(figsize, name)
            logging.warning(warning_msg)
            
            figsize = None
    
    if figsize is None and current_plot_type in FIGURE_SIZES:
        figsize = FIGURE_SIZES[current_plot_type]
    
    # Adding the value for the 'figsize' key
    if current_plot_type in PLOT_TYPE_ONE:
        if isinstance(plot_yaml_input, dict):
            plot_yaml_input['figsize'] = figsize
    elif current_plot_type in PLOT_TYPE_TWO:
        if isinstance(plot_yaml_input[current_plot_type], dict):
            plot_yaml_input[current_plot_type]['figsize'] = figsize
    
    return figsize, plot_yaml_input


def get_title(current_plot_type, plot_yaml_input, name):
    """
    The position of the Title entry depends on the plot type. TimeSeries,
    TimeSeriesStats, TimeSeriesAndStats, and AoR figures have the Title indented
    under the plot name. The other plot types that accept the Title entry
    (not TTFD, GriddedMetric, GriddedRadialMetric, AtmPlumeSIngle, or
    AtmPlumeEnsemble) have it indented under the plot type (which is in turn
    indented under the plot name).
    """
    title = None
    
    if current_plot_type in PLOT_TYPE_ONE:
        if isinstance(plot_yaml_input, dict):
            if 'Title' in plot_yaml_input:
                title = plot_yaml_input['Title']
    
    elif current_plot_type in PLOT_TYPE_TWO:
        if isinstance(plot_yaml_input[current_plot_type], dict):
            if 'Title' in plot_yaml_input[current_plot_type]:
                title = plot_yaml_input[current_plot_type]['Title']
    
    if not isinstance(title, str) and title is not None:
        warning_msg = ''.join([
            'For the {} plot {}, the "Title" entry was provided as {}. ', 
            'This input was not a string, so it will not be used as the ', 
            'figure title.']).format(current_plot_type, name, title)
        logging.warning(warning_msg)
        
        title = None
    
    return title

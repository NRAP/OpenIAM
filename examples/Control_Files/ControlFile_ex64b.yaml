# OpenIAM Control File Example 64b
#
# This example is similar to control file example 64a, in that it demonstrates the evaluation 
# of critical pressures with the SalsaContourPlot plot type. Unlike that example, this example 
# uses multiple aquifers in the SalsaContourPlot. Each aquifer has a different critical pressure 
# calculated based on the aquifer's depth, the aquifer's aquifer#FluidDensity parameter, and 
# the depth of the overlying aquifer specified with the 'AoRAquiferNumber' entry.
#
# In this example, only aquifer 1 is a reservoir because it is given positive activeWell#QAquifer#Period# 
# values. Even though aquifers 2 and 3 are not storage reservoirs, the SalsaConourPlot still 
# assesses whether the pressures of those units can drive leakage into aquifer 4 (i.e., by 
# exceeding the critical pressure). In this scenario, the critical pressure analyses conducted 
# for aquifers 2 and 3 could involve an assumption that pressure increases in aquifers 2 and 3 
# (potentially due to leakage entering those units from aquifer 1) could drive fluid from aquifers 
# 2 and 3 and into aquifer 4 along a leakage pathway that is not connected to the reservoir. 
# The salinity of the brine in aquifers 2 and 3 could negatively impact aquifer 4 if aquifer 4 is 
# an underground source of drinking water. For example, one might consider a hypothetical well 
# that pentrates aquifers 2 through 4, but not aquifer 1 (if it also penetrated aquifer 1, flow 
# along the well might be from aquifer 1 to aquifer 4, rather than from aquifers 2 or 3 to aquifer 
# 4). One would consider hypothetical wells in an area of review analysis to account for the possibility 
# that an unknown well is present in the area.
#
# To run this file, use the command (\ for Windows / for Mac or Linux):
#  python ../../src/openiam/components/openiam_cf.py --file ControlFile_ex64b.yaml
#
# Last Modified: June, 2024
#-------------------------------------------------
ModelParams:
    # Time is in years
    EndTime: 200
    TimeStep: 10.0
    Analysis: forward
    Components: [SALSA]
    OutputDirectory: output/output_ex64b_{datetime}
    Logging: Warning
#-------------------------------------------------
Stratigraphy:
    # The SALSA component does not connect with a stratigraphy component, so 
    # any information entered here does not impact the results from SALSA.
    numberOfShaleLayers:
        vary: False
        value: 3
    shale1Thickness:
        value: 250.0
        vary: False
    shale2Thickness:
        value: 250.0
        vary: False
    shale3Thickness:
        value: 250.0
        vary: False
    aquifer1Thickness:
        value: 100.0
        vary: False
    aquifer2Thickness:
        value: 100.0
        vary: False
    reservoirThickness:
        value: 50.0
        vary: False
#-------------------------------------------------
SALSA:
    Type: SALSA
    InjectionWell:
        #---- keyword arguments related to active wells ----#
        periodEndTimes: [150, 200]
        # The first well lcoation is an extraction well, the other four are injection wells
        activeWellCoordx: [0.0, 1000, -1000, -1000, 1000]
        activeWellCoordy: [0.0, 1000, 1000, -1000, -1000]
    LeakingWell:
        #---- keyword arguments related to leaking wells ----#
        leakingWellCoordx: [100, 1100, 5000]
        leakingWellCoordy: [0, 1000, 1000]
    OutputLocations:
        #---- keyword arguments related to hydraulic head output ----#
        aquiferCoordx: [100, 1100, 5000]
        aquiferCoordy: [0, 1000, 1000]
        shaleCoordx: [100, 1100, 5000]
        shaleCoordy: [0, 1000, 1000]
    ContourPlot:
        aquiferNamesContourPlots: [1, 2, 3]
    Parameters:
        #---- Stratigraphy parameters ----#
        numberOfShaleLayers: 5
        bottomLayerType: 0
        topLayerType: 0
        shale1Thickness: 76.0
        shale2Thickness: 389.0
        shale3Thickness: 127.0
        shale4Thickness: 98.0
        shale5Thickness: 103.0
        shale1LogVertK: -10.5
        shale2LogVertK: -10.0
        shale3LogVertK: -11.0
        shale4LogVertK: -12.0
        shale5LogVertK: -12.5
        shale1LogSS: -5.5
        shale2LogSS: -5.0
        shale3LogSS: -6.0
        shale4LogSS: -7.0
        shale5LogSS: -7.5
        shaleAllInitialGamma: 0
        shaleAllGamma: 0
        aquifer1Thickness: 45.0
        aquifer2Thickness: 113.0
        aquifer3Thickness: 78.0
        aquifer4Thickness: 23.0
        aquifer1LogHorizK: -4.5
        aquifer2LogHorizK: -4.0
        aquifer3LogHorizK: -5.0
        aquifer4LogHorizK: -5.5
        aquiferAllANSR: 1
        aquifer1LogSS: -5.5
        aquifer2LogSS: -5.0
        aquifer3LogSS: -6.0
        aquifer4LogSS: -6.5
        aquifer1FluidDensity: 1150
        aquifer2FluidDensity: 1100
        aquifer3FluidDensity: 1010
        aquifer4FluidDensity: 1000
        aquifer1Head: 10
        aquifer2Head: 25
        aquifer3Head: 5
        aquifer4Head: 0
        aquiferAllGamma: 0.0
        bottomBoundaryCond: 1
        bottomBoundaryHead: 0.0
        topBoundaryCond: 0
        topBoundaryHead: 0.0
        #---- Active well parameters ----#
        # Injection period 1
        activeWell1QAquifer1Period1: -0.12
        activeWell2QAquifer1Period1: 0.16
        activeWell3QAquifer1Period1: 0.16
        activeWell4QAquifer1Period1: 0.16
        activeWell5QAquifer1Period1: 0.16
        activeWell1QAquifer2Period1: 0.0
        activeWell2QAquifer2Period1: 0.0
        activeWell3QAquifer2Period1: 0.0
        activeWell4QAquifer2Period1: 0.0
        activeWell5QAquifer2Period1: 0.0
        activeWell1QAquifer3Period1: 0.0
        activeWell2QAquifer3Period1: 0.0
        activeWell3QAquifer3Period1: 0.0
        activeWell4QAquifer3Period1: 0.0
        activeWell5QAquifer3Period1: 0.0
        activeWell1QAquifer4Period1: 0.0
        activeWell2QAquifer4Period1: 0.0
        activeWell3QAquifer4Period1: 0.0
        activeWell4QAquifer4Period1: 0.0
        activeWell5QAquifer4Period1: 0.0
        # Injection period 2
        activeWell1QAquifer1Period2: 0.0
        activeWell2QAquifer1Period2: 0.0
        activeWell3QAquifer1Period2: 0.0
        activeWell4QAquifer1Period2: 0.0
        activeWell5QAquifer1Period2: 0.0
        activeWell1QAquifer2Period2: 0.0
        activeWell2QAquifer2Period2: 0.0
        activeWell3QAquifer2Period2: 0.0
        activeWell4QAquifer2Period2: 0.0
        activeWell5QAquifer2Period2: 0.0
        activeWell1QAquifer3Period2: 0.0
        activeWell2QAquifer3Period2: 0.0
        activeWell3QAquifer3Period2: 0.0
        activeWell4QAquifer3Period2: 0.0
        activeWell5QAquifer3Period2: 0.0
        activeWell1QAquifer4Period2: 0.0
        activeWell2QAquifer4Period2: 0.0
        activeWell3QAquifer4Period2: 0.0
        activeWell4QAquifer4Period2: 0.0
        activeWell5QAquifer4Period2: 0.0
        activeWellAllRadiusAquiferAll: 0.1
        #---- Leaking well parameters ----#
        leakingWell1LogKAquifer1: -4.5
        leakingWell1LogKAquifer2: -4.0
        leakingWell1LogKAquifer3: -5.0
        leakingWellAllRadiusAquiferAll: 0.15
        leakingWellAllStatAquiferAll: 1
        leakingWellAllLogKShale1: -4.5
        leakingWellAllLogKShale2: -4.0
        leakingWellAllLogKShale3: -5.0
        leakingWellAllLogKShale4: -6.0
        leakingWellAllRadiusShaleAll: 0.15
        leakingWell1StatShale1: 1
        leakingWell1StatShale2: 1
        leakingWell1StatShale3: 0
        # no leakingWell1StatShale4 - that parameter only applies when a shale 
        # has aquifers above and below it, but shale 4 does not here. If you entered 
        # that parameter here, a warning statement would print to say that the input 
        # would not be used.
        #---- Miscellaneous parameters ----#
        numberOfLaplaceTerms: 8
        flagMesh: 1
        xCent: 0.0
        yCent: 0.0
        xMax: 10000.0
        yMax: 10000.0
        gridExp: 1.0
        numberOfNodesXDir: 50
        numberOfNodesYDir: 50
        numberOfRadialGrids: 6
        radialZoneRadius: 100.0
        radialGridExp: 1.2
        numberOfVerticalPoints: 10
    Outputs: [headLocAllAquiferAll, pressureLocAllAquiferAll, pressureLocAllMidAquiferAll, pressureLocAllTopAquiferAll, 
              headProfileAllVertPointAllShaleAll, pressureProfileAllVertPointAllShaleAll, 
              profileDepths, allHeadProfilesShales, allPressureProfilesShales, 
              diffuseLeakageRateBottomAquiferAll, diffuseLeakageVolumeBottomAquiferAll, 
              diffuseLeakageRateTopAquiferAll, diffuseLeakageVolumeTopAquiferAll, 
              wellLeakageRateAquiferAll, wellLeakageVolumeAquiferAll, 
              wellAllLeakageRateAquiferAll, wellAllLeakageVolumeAquiferAll, 
              contourPlotAquiferHead, contourPlotTopAquiferPressure, contourPlotCoordx, contourPlotCoordy]
#-------------------------------------------------
Plots:
    Pressure_Profile:
        SalsaProfile:                        # This plot requires the profileDepths and allPressureProfilesShales outputs
            FigureDPI: 300
            Title: SALSA Simulation Results
            FigureSize: [11, 13]             # The default figure size is [10, 12]
            SaveCSVFiles: True
            MetricType: pressure
    Pressure_Time_Series:
        SalsaTimeSeries:                     # This plot requires the profileDepths, pressureLoc#Aquifer#, pressureLoc#MidAquifer#, 
            FigureDPI: 300                   # pressureLoc#TopAquifer#, and pressureProfile#VertPoint#Shale# outputs.
            Title: SALSA Simulation Results
            FigureSize: [10, 14]             # The default figure size scales with the number of aquifer and shale layers
            SaveCSVFiles: True
            MetricType: pressure
    Pressure_Contour_Plot:
        SalsaContourPlot:                    # This plot requires the contourPlotAquiferPressure, contourPlotCoordx, and contourPlotCoordy outputs
            MetricType: pressure             # If the AquiferLocation entry is set to Middle or Top, the contourPlotMidAquiferPressure or 
            FigureDPI: 300                   # contourPlotTopAquiferPressure outputs would be required, respectively, instead of the contourPlotAquiferPressure output.
            AquiferLocation: Top             # Options are Bottom, Middle, or Top - default is Bottom.
            TimeList: All
            AoRAquiferNumber: 4              # Specifies that the critical pressure will be calculated for aquifer 3 (here, the pressure required to lift fluid from aquifer 1 to aquifer 3)
            CriticalPressureMPa: Calculated
            AquiferNameList: [1, 2, 3]       # Specifies that this SalsaContourPlot will only show pressures for aquifers 1, 2, and 3 - if not provided, the plot will show 
                                             # results for all aquifers in the aquiferNamesContourPlot keyword argument provided for the SALSA component (here, 1, 2, and 3).



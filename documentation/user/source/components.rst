.. toctree::
    :maxdepth: 2

.. _components_description:

**********************
Components Description
**********************

.. toctree::

This section of the document describes the component models available in 
NRAP-Open-IAM. There are sub-sections below for component inputs, component 
outputs, and different types of component models.

Component Inputs
================

Parameters
----------

Components have input parameters that control the modelled behavior. For example, 
reservoir components might have parameters for the permeability of the reservoir as 
well as the |CO2| injection rate. Some parameters have defined units while other 
parameters are dimensionless values (e.g., porosity). Parameters have default values 
that will be used if the user does not provide input for that parameter. Parameters 
also have limited ranges of acceptable values; if the user attempts to provide a value 
outside of the allowed range, then the value will be set to the closest limit. In 
a deterministic simulation (the ``forward`` analysis type), the exact parameter values 
provided will be used. In a stochastic simulation (the ``lhs`` or ``parstudy`` analsis 
types), parameter values can be varied across different realizations. In this case, 
only parameters that are given minimum and maximum values will be varied across the 
realizations.

In the graphical user interface (GUI), parameter values are entered in cells. In the 
control file interface, parameter values for a component are entered under the 
``Parameters`` entry in that component's section of the control file. In a scripting 
application, parameter values are added to a component with the ``add_par()`` method 
of the component. For more details regarding the parameters of a specific component, 
read the component's section in this document and refer to the examples demonstrating 
the component. The examples using a component are listed at the end of the component's 
section in this document.

Keyword Arguments
-----------------

Some components also take inputs as keyword arguments. In Python, a keyword argument 
is a named value that is provided when calling a function or method. For example, 
the easting and northing (``x`` and ``y``) coordinates of the injection well(s) 
for reservoir components are given with the keyword arguments ``injX`` and ``injY``. 
In a script application, the user would provide these keyword arguments when 
creating a reservoir component like an Analytical Reservoir. When using the 
graphical user interface (GUI), however, the location of the injection well is 
entered in cells and the component is given those ``injX`` and ``injY`` keyword arguments 
when created. In the control file interface, the injection well locations are given 
in the component's section of the control file as ``coordx`` and ``coordy`` values 
under the entry ``InjectionWell``. These ``coordx`` and ``coordy`` inputs are then used 
as ``injX`` and ``injY`` keyword arguments when the reservoir component is made during 
the model run. Overall, keyword arguments are entered in a cell in the GUI, provided 
under a specific entry in the control file interface, or given when creating the 
component in a scripting application. For more details regarding the keyword arguments 
of a specific component, read the component's section in this document and refer to the 
examples using the component.

A keyword argument is different from a parameter in several ways. First, keyword arguments 
cannot vary in stochastic simulations while parameters can. Second, parameters must be given 
as numerical values while keyword arguments can take different types of variables. For 
example, a keyword argument might be given as a list containing multiple values. A keyword 
argument might also have formatting requirements, such as a list containing a number of values 
that aligns with a number of features (e.g., one value for each injection well location). 
Alternatively, a component like the Lookup Table Reservoir can have keyword arguments 
representing the file names and directories of the input files used for the component.

Component Connections
---------------------

Components can be connected to each other, so the outputs of one component serves as inputs 
for another component. Wellbores can be connected to reservoir components, calculating leakage 
rates based on the pressures and |CO2| saturations provided. Receptor components, such as 
aquifer or atmosphere components, can be connected to wellbore components, allowing the receptor 
components to calculate contaminant plume volumes based on the leakage rates provided.

Dynamic Parameters
------------------

Component connections are not always required. Some components are not designed to be connected 
to other components (e.g., standalone components like SALSA). Otherwise, a component that 
normally requires connection to another component can be used with dynamic parameter inputs. 
While a normal parameter does not vary over time, dynamic parameters can vary over time. For 
example, if a wellbore component is not connected to a reservoir component, the pressures and 
|CO2| saturations required by the wellbore component can be given as dynamic parameters (with 
one pressure and one |CO2| saturation given for each time step). In the GUI, dynamic parameters 
can be provided when setting up the component. In the control file interface, dynamic parameters 
can be provided under the entry ``DynamicParameters`` (see *ControlFile_ex7a* and *ControlFile_ex7b*). 
While a Lookup Table Reservoir can be used to drive leakage simulations with the output from 
high-fidelity reservoir simulations, the files used by the component have formatting requirements 
that could be challenging. To drive a simulation with reservoir simulation output more easily, 
the user provide pressures and |CO2| saturations as dynamic parameters.

Component Outputs
=================

Each component has certain outputs it can produce. For example, a reservoir component can 
calculate pressures and |CO2| saturations, a wellbore component can calculate brine 
and |CO2| leakage rates, and an aquifer component can calculate contaminant plume volumes. 
Outputs can be saved to *.csv* files and displayed in automatically generated figures, such 
as the ``TimeSeries`` and ``AoR`` plot types (see section :ref:`cfi_visualization`). The 
sections below describe the outputs for each component.

Stratigraphy Components
=======================

NRAP-Open-IAM has several components that represent the stratigraphy of the domain. These 
components have different intended uses. For example, the Stratigraphy component represents 
flat-lying units while the Lookup Table Stratigraphy component allows unit thicknesses to 
vary across the domain. Only one stratigraphy component can be used in a simulation. For 
simulations run through the GUI or control file interface, many other components will connect 
with the stratigraphy component to set parameters related to the stratigraphy. For example, 
an aquifer component can have parameters for the thickness and depth of the aquifer; in the 
GUI or control file interface, these parameters will be automatically assigned based on the 
setup of the stratigraphy component.

.. _stratigraphy_component:

Stratigraphy Component
----------------------

.. automodule:: openiam.base.Stratigraphy

Dipping Stratigraphy Component
------------------------------

.. automodule:: openiam.base.DippingStratigraphy

LookupTableStratigraphy Component
---------------------------------

.. automodule:: openiam.base.LookupTableStratigraphy

Reservoir Components
====================

Reservoir components are used to represent the response of a reservoir to injection and/or 
extraction. Different reservoir components have different intended uses. For example, the 
Analytical Reservoir can only represent one injection well, and it produces pressures and 
|CO2| saturations. In contrast, a Theis Reservoir can represent multiple injection and/or 
extraction wells, but it can only produce pressures. A Lookup Table Reservoir component 
can be used to incorporate results from a separate reservoir simulator into an NRAP-Open-IAM 
simulation.

Analytical Reservoir Component
------------------------------

.. automodule:: openiam.base.AnalyticalReservoir

Lookup Table Reservoir Component
--------------------------------

.. automodule:: openiam.base.LookupTableReservoir

Theis Reservoir Component
-------------------------

.. automodule:: openiam.base.TheisReservoir

Wellbore Components
===================

Wellbore components portray the leakage of fluids along a well. These components are usually 
set up to use the pressures and |CO2| saturations produced by a reservoir component. Alternatively,
they can be given specific values to use (see the discussion of dynamic parameters further above).

Multisegmented Wellbore Component
---------------------------------

.. automodule:: openiam.base.MultisegmentedWellbore

Multisegmented Wellbore AI Component
------------------------------------

.. automodule:: openiam.base.MultisegmentedWellboreAI

Cemented Wellbore Component
---------------------------

.. automodule:: openiam.base.CementedWellbore

Open Wellbore Component
-----------------------

.. automodule:: openiam.base.OpenWellbore

Generalized Flow Rate Component
-------------------------------

.. automodule:: openiam.base.GeneralizedFlowRate

Receptor Components
===================

A receptor component portrays the response of an aquifer or the atmosphere to the input of leakage. With 
the currently available receptor components, the leakage must come from a point-source leakage pathway. 
For example, an aquifer component can be connected to a wellbore component, but not a fault component; a 
fault component produces leakage rates that occur over the length of the fault, while a wellbore can be 
sufficiently represented as a point. Different aquifer components have different requirements. For example, 
the FutureGen2 Aquifer component is meant to represent shallower aquifers (bottom depths of 100 |m| to 700 |m|) 
while the FutureGen2 AZMI component is meant to represent deepter aquifers (bottom depths of 700 |m| to 1600 |m|). 
In contrast, the Generic Aquifer can be applied to aquifers with top depths ranging from 100 |m| to 4100 |m|, 
but it is a machine-learning model that has longer model run times.

Carbonate Aquifer Component
---------------------------

.. automodule:: openiam.base.CarbonateAquifer

Deep Alluvium Aquifer Component
-------------------------------

.. automodule:: openiam.base.DeepAlluviumAquifer

FutureGen2 Aquifer Component
----------------------------

.. automodule:: openiam.base.FutureGen2Aquifer

FutureGen2 AZMI Component
-------------------------

.. automodule:: openiam.base.FutureGen2AZMI

Generic Aquifer Component
-------------------------

.. automodule:: openiam.base.GenericAquifer

Atmospheric Model Component
---------------------------

.. automodule:: openiam.base.AtmosphericROM

Fault Components
================

Fault Flow Component
--------------------

.. automodule:: openiam.base.FaultFlow

Fault Leakage Component
-----------------------

.. automodule:: openiam.base.FaultLeakage

Caprock Leakage Components
==========================

Seal Horizon Component
----------------------

.. automodule:: openiam.base.SealHorizon

Standalone Components
=====================

SALSA Component
---------------

.. automodule:: openiam.base.SALSA

Plume Stability Component
-------------------------

.. automodule:: openiam.base.PlumeStability

Chemical Well Sealing Component
-------------------------------

.. automodule:: openiam.base.ChemicalWellSealing

Hydrocarbon Leakage Component
-----------------------------

.. automodule:: openiam.base.HydrocarbonLeakage

"""
This script contains example well data that can be used with the WellData 
component.

The input dictionary should have the following keys:
    * 'uwi', for unique well identifier
    * 'tvd': true vertical depth to the bottom of the well, in meters
    * 'md': measured depth to the bottom of the well, in meters
    * 'res_data': dictionary containing keys 'Reservoir 1', 'Reservoir 2', 
        ... 'Reservoir N', where N is the number of reservoirs along the well
    * 'hole_data': dictionary containing keys 'Hole 1', 'Hole 2', 
        ... 'Hole N', where N is the number of holes / pipes along the well
    * 'plug_data': dictionary containing keys 'Plug 1', 'Plug 2', 
        ... 'Plug N', where N is the number of plugs along the well

Below, 'tvd' will be used for true vertical depth while 'md' will be used 
for measured depth. If a wellbore is perfectly straight, then the tvd and 
md values will be equal. If a well bends, however, the md values will be 
greater than the tvd values.

Each reservoir entry (e.g., the key 'Reservoir 1') in 'res_data' should 
have the following keys:
    * 'seal_top_tvd': tvd at the top of the seal, in meters
    * 'seal_bot_tvd': tvd at the bottom of the seal, in meters
    * 'seal_top_md': md at the top of the seal, in meters
    * 'seal_bot_md': md at the bottom of the seal, in meters
    * 'target_top_tvd': tvd at the top of the reservoir (target), in meters
    * 'target_bot_tvd': tvd at the bottom of the reservoir (target), in meters
    * 'target_top_md': md at the top of the reservoir (target), in meters
    * 'target_bot_md': md at the bottom of the reservoir (target), in meters

Each hole entry (e.g., the key 'Hole 1') in 'hole_data' should have the 
following keys:
    * 'hole_tvd': tvd to the bottom of the hole, in meters
    * 'hole_md': md to the bottom of the hole, in meters
    * 'csg_pres': casing present, True or False
    * 'csg_top_tvd': tvd to the top of the casing, in meters
    * 'csg_bot_tvd': tvd to the bottom of the casing, in meters
    * 'csg_top_md': md to the top of the casing, in meters
    * 'csg_bot_md': md to the bottom of the casing, in meters
    * 'csg_cmt_top_md': md to the top of the cement in the casing, in meters

Each plug entry (e.g., the key 'Plug 1') in 'plug_data' should have the 
following keys:
    * 'plug_type': type of the plug, either 'Casing' or 'Annulus'
    * 'plug_loc': name of the hole the plug is in (e.g., 'Hole 1'), must 
        match the exact spelling used for the hole_data keys.
    * 'plug_top_md': md to the top of the plug, True or False
    * 'plug_bot_md': md to the bottom of the plug, in meters

The user can modify the well_data dictionary, change the file_name and output_dir 
variables below, and then run the file. The updated well_data dictionary will 
then be saved to a .yaml file, in the directory output_dir with the file name 
file_name. This .yaml file can be used with the process_wellbore_data() function 
in well_data_component.py, which can be used to create plots of the well data. 
For example, the function can create plots showing where the holes have casing 
and plugs and plots showing where the annuli (spaces between holes) have cement.
"""
import os
import yaml

well_data = {
    'uwi': 427050000400,
    'tvd': 4309,
    'md': 4309,
    'res_data': {
        'Reservoir 1': {
            'seal_top_tvd': 991, 
            'seal_bot_tvd': 1041, 
            'seal_top_md': 991,
            'seal_bot_md': 1041,
            'target_top_tvd':1042,
            'target_bot_tvd':1142,
            'target_top_md':1042,
            'target_bot_md':1142,
            },
        'Reservoir 2': {
            'seal_top_tvd': 3184,
            'seal_bot_tvd': 3234,
            'seal_top_md': 3184,
            'seal_bot_md': 3234,
            'target_top_tvd':3235,
            'target_bot_tvd':3335,
            'target_top_md':3235,
            'target_bot_md':3335,
            }
        },
    'hole_data' : {
        'Hole 1': {
            'hole_tvd': 112, 
            'hole_md': 112, 
            'csg_pres':True,
            'csg_top_tvd':0,
            'csg_bot_tvd':112,
            'csg_top_md':0,
            'csg_bot_md':112,
            'csg_cmt_top_md':0,
            },
        'Hole 2': {
            'hole_tvd': 232, 
            'hole_md': 232, 
            'csg_pres':True,
            'csg_top_tvd':0,
            'csg_bot_tvd':232,
            'csg_top_md': 0,
            'csg_bot_md':232,
            'csg_cmt_top_md':44,
            },
        'Hole 3': {
            'hole_tvd': 916, 
            'hole_md': 916, 
            'csg_pres':True,
            'csg_top_tvd':0,
            'csg_bot_tvd':916,
            'csg_top_md': 0,
            'csg_bot_md':916,
            'csg_cmt_top_md':648,
            },
        'Hole 4': {
            'hole_tvd': 3047, 
            'hole_md': 3047, 
            'csg_pres':True,
            'csg_top': 0,
            'csg_top_tvd': 0,
            'csg_bot_tvd': 3047,
            'csg_top_md': 0,
            'csg_bot_md': 3047,
            'csg_cmt_top_md': 2769,
            },
        'Hole 5': {
            'hole_tvd': 4309, 
            'hole_md': 4309, 
            'csg_pres': True,
            'csg_top_tvd': 2967,
            'csg_bot_tvd': 3101,
            'csg_top_md': 2967,
            'csg_bot_md': 3101,
            'csg_cmt_top_md': 3097,
            },
        },
    'plug_data' : {
        'Plug 1': {
            'plug_type': 'Casing',
            'plug_loc': 'Hole 4',
            'plug_top_md':107,
            'plug_bot_md':229,
            },
        'Plug 2': {
            'plug_type': 'Annulus',
            'plug_loc': 'Hole 4',
            'plug_top_md':274,
            'plug_bot_md':549,
            },
        'Plug 3': {
            'plug_type': 'Annulus',
            'plug_loc': 'Hole 4',
            'plug_top_md':621,
            'plug_bot_md':829,
            },
        'Plug 4': {
            'plug_type': 'Casing',
            'plug_loc': 'Hole 5',
            'plug_top_md': 2956,
            'plug_bot_md': 3018,
            },
        'Plug 5': {
            'plug_type': 'Casing',
            'plug_loc': 'Hole 5',
            'plug_top_md': 2957,
            'plug_bot_md': 3039
            },
        'Plug 6': {
            'plug_type': 'Casing',
            'plug_loc': 'Hole 5',
            'plug_top_md': 3627,
            'plug_bot_md': 3718,
            },
        }
    }

if __name__ == "__main__":
    output_dir = os.getcwd()
    file_name = 'ExampleWellData.yaml'
    
    with open(os.path.join(output_dir, file_name), 'w') as f:
        yaml.dump(well_data, f)
    f.close()

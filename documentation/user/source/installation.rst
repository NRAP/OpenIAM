***********************
Obtaining NRAP-Open-IAM
***********************

.. toctree::

.. image:: ../../images/nrap.png
   :align: center
   :scale: 100%
   :alt: Nrap Logo

Introduction
============

NRAP-Open-IAM is an open-source Integrated Assessment Model (IAM) for 
Phases II and III of the National Risk Assessment Partnership (NRAP). The goal 
of this software is to go beyond risk assessment at geologic carbon storage sites 
and into risk management and containment assurance. NRAP-Open-IAM is currently 
in active development and is available for testing and feedback only.

As this is a prototype of software being actively developed, we are seeking 
any feedback or bug reports. Feedback can be emailed to the NRAP-Open-IAM project 
at NRAP@netl.doe.gov, to the current development team lead at 
Nathaniel.Mitchell@netl.doe.gov, or to any other member of the development team.

Issues can also be reported on GitLab issues page for NRAP-Open-IAM:
https://gitlab.com/NRAP/OpenIAM/-/issues?sort=created_date&state=opened

If you have been given access to the code indirectly and would like to be notified 
when updates are available for testing, please contact the development team to be 
added to our email list.

Downloading NRAP-Open-IAM
=========================

NRAP-Open-IAM tool and examples can be downloaded from a public GitLab 
repository located at https://gitlab.com/NRAP/OpenIAM. If the NRAP-Open-IAM 
was downloaded from the GitLab repository, the folder name may have 
the repository's current hash appended to it. To simply the folder name, the 
user can rename the folder *NRAP_Open_IAM*.

In addition to that, a copy of the tool can be obtained through the National Energy 
Tecnhology Laboratory's (NETL) Energy Data eXchange (EDX) website: 

https://edx.netl.doe.gov/dataset/phase-iii-nrap-open-iam

To download the tool from EDX, the user must first requesting access through an 
e-mail addressed to NRAP@netl.doe.gov. The NRAP-Open-IAM is distributed as a *.zip* 
file that can be extracted in the location specified by user.

Installing NRAP-Open-IAM
========================

Obtaining Python
----------------

NRAP-Open-IAM requires Python version 3.9 or greater to operate. If you 
need to install Python, descriptions of the installation process are 
provided separately for Windows, macOS, and Linux (see below). After 
installing a Python distribution, such as Anaconda Navigator, continue 
following the instructions further below.

For Windows: The file *Installation_Instructions_Windows.txt* describes 
steps to install a Python distribution on Windows.

For macOS: The file *Installation_Instructions_macOS.txt* describes the 
steps to install a Python distribution on macOS.

For Linux OS: Linux users are assumed to know how to install a Python 
distribution for their specific version of Linux.

Navigate a Command Prompt to the Main Directory
-----------------------------------------------

Once a Python distribution is installed, open a command prompt. For example, 
if using Anaconda Navigator, open "Anaconda Prompt." Once Anaconda Navigator 
is installed on Windows, Anaconda Prompt can be found by typing "Anaconda Prompt" 
in the Windows search bar.

In the command prompt, navigate to the installation directory of NRAP-Open-IAM. 
The installation directory has the files 'User_Guide.pdf' and 'Developer_Guide.pdf' 
as well as folders such as 'examples' and 'src'. On Windows, the path to the 
directory can be copied from File Explorer (highlighting the text and hitting 
"ctrl" and "c" on Windows, or "command" and "c" on Mac). In the command prompt, 
the user can then navigate to the installation directory by typing "cd," a space, and 
then pasting the installation directory and hitting enter. On Windows, pasting is done by 
pushing the buttons "ctrl" and "v". On Mac, pasting is done by pushing "command" and 
"v." 

Alternatively, the user can navigate by typing "cd", a space, and then manually 
typing the path to the installation directory. For example, if the NRAP-Open-IAM 
installation was located in *C:/Users/USERNAME/Documents/NRAP_Open_IAM* and the 
command prompt was currently in *C:/Users/USERNAME*, the user could type 
"cd Documents/NRAP_Open_IAM" and hit enter. Here, *USERNAME* is a placeholder for 
the user name. Directories are separated with \\ on Windows and / on Mac and 
Linux - adjust as necessary for your operating system.

Typing "cd .." in the command prompt and pushing enter will navigate up one directory 
(e.g., going from *C:/Users/USERNAME/Documents/* to *C:/Users/USERNAME/*). Note that 
hitting the tab button will automatically complete a partially entered directory 
name - this approach can significantly speed up navigation in a command prompt. If 
multiple directories begin with the partially entered text (e.g., ``Do`` for ``Documents`` 
or ``Downloads``), hitting tab repeatedly will cycle through the applicable folder names.
For example, to accomplish the command "cd Documents/NRAP_Open_IAM," the user could type 
"cd", a space, "d" (for "Documents") and then tab, a separator (\\ on Windows and 
/ on Mac and Linux), "n" (for "NRAP_Open_IAM") and then tab, and finally the enter button 
(the tab button may need to be pushed more than once each time).

Again, the commands described below will not work unless the user has navigated a 
command prompt to the installation directory of NRAP-Open-IAM. To test if the command 
prompt is in the correct folder, type ``dir`` and hit enter. This command will display 
information about current directory's contents. If located in the correct 
directory, the information displayed will include files like ``User_Guide.pdf`` 
and ``Developer_Guide.pdf``.

Commands to Install NRAP-Open-IAM
---------------------------------

Once the user has navigated a command prompt to the installation directory of 
NRAP-Open-IAM, type the following command and hit enter::

    conda create -n OpenIAMEnv python=3.10 pip

The command prompt will display "Proceed ([y]/n)?" - type "y" and hit enter. This 
command creates an environment called "OpenIAMEnv" with Python version 3.10 and 
the ``pip`` Python package. The environment can be given a different name by using 
a different name in place of "OpenIAMEnv," but that different name should then be 
used in place of "OpenIAMEnv" in the instructions further below.

Next, activate the "OpenIAMEnv" environment with this command::

    conda activate OpenIAMEnv

The command prompt will show "(OpenIAMEnv)" to the left, indicating that the environment 
is active.

Next, enter the following command::

    python -m pip install -e .

It is important to include the period (".") at the end of the command. This step 
will install all of the required Python libraries within the "OpenIAMEnv" 
environment. Specifically, the file "setup.cfg" in the installation directory of 
NRAP-Open-IAM is used during this step to obtain all of the required libraries.

If you are using Anaconda Navigator and wish to use the integrated development 
environment Spyder, you can then use pip to install that program::

    python -m pip install spyder

The user can then open Spyder with the following command::

    spyder

If you are using Anaconda Navigator and wish to use Jupyter Notebook, you can also 
use pip to install that program::

    python -m pip install notebook

The user can then open Jupyter Notebook by using the following command::

    jupyter notebook

To easily access the Jupyter Notebook examples for NRAP-Open-IAM, use the command above 
while located in the installation directory of NRAP-Open-IAM.

To use NRAP-Open-IAM in the future (control file examples, Jupyter Notebook examples, 
or script examples), the "OpenIAMEnv" environment must first be activated each 
time. The environment only needs to be created once, but after it is created and 
all of the Python libraries are installed within it, it must be activated with the 
"conda activate OpenIAMEnv" command shown above.

To deactivate the environment, use the following command::

    conda deactivate

On macOS and Linux machines, the gfortran compiler needs to be present/installed 
to compile some of the NRAP-Open-IAM code (macOS users can find gfortran here: 
`(https://gcc.gnu.org/wiki/GFortranBinariesMacOS) 
<https://gcc.gnu.org/wiki/GFortranBinariesMacOS>`_).

Testing Installation
--------------------

After the proper version of Python is installed, the NRAP-Open-IAM can be set up 
and tested. **Note: If Python was installed through Anaconda, please use Anaconda 
prompt instead of command prompt for setup and tests.**

To test the installation, open a command prompt and activate the NRAP-Open-IAM environment. 
Then, navigate the command prompt to the *setup* folder in the main directory of the 
NRAP-Open-IAM installation. For more details regarding navigation in a command prompt 
or activating an environment, see the section ``Navigate a Command Prompt to the Main Directory`` 
above.

Once located in the *setup* folder with the NRAP-Open-IAM environment activated, run the 
setup script by typing this command and hitting enter::

    python openiam_setup_tests.py

This script will test the version of Python installed on the system; the script will 
display the statement ``Checking Python version``. Next, the setup script will check 
that certain required files are present and compile the Fortran libraries needed 
for some component models on Mac and Linux. Users of Windows OS will be provided with the 
compiled libraries. During these steps, the script will display the statement 
``Checking internal NRAP-Open-IAM libraries``. Finally, the setup script will run the 
test suite to see if the NRAP-Open-IAM has been installed correctly. During this step, 
the script will display the statement ``Testing NRAP-Open-IAM installation``. If the results 
printed to the console indicate that errors occurred during the testing, the errors 
have to be resolved before the NRAP-Open-IAM can be used.

The most common cause for errors is when one of the Python libraries used, like Numpy, 
Matplotlib, or TensorFlow, has an update that requires changes to the model code. For example, 
a library could have a change where certain commands need to be used in a new manner or are 
not supported anymore. The developers try to address errors and conflicts as quickly as 
possible. When encountering an error, first check if the version of NRAP-Open-IAM installed 
is the latest version, as the developers might have already released a newer version that 
addresses the issue.

If the error still occurs in the newest version of NRAP-Open-IAM, contact the developers (see 
the beginning of this section). Be sure to include the following information: the type of computer 
used (Windows, Mac, or Linux), the setup of the simulation (e.g., which components were used), how 
the simulation was run (graphic user interface, control file interface, or a scripting approach), 
and the exact error messages produced. Error messages can be long, but it is best to include all 
of the error messages in your email.

Testing installation
====================

After setup, the test suite can be run again by entering the NRAP-Open-IAM *test* 
directory in a terminal and typing::

    python iam_test.py

Test results will be printed to the terminal. The setup script run during
the installation process uses the same test suite after testing whether the necessary
Python libraries are installed, and compiling the NRAP-Open-IAM libraries.

Contributors
============

During the Phase II and/or Phase III of the NRAP the following researchers contributed
to the development of NRAP-Open-IAM (listed in alphabetical order with affiliation
at the time of active contribution):

* Diana Bacon (Pacific Northwest National Laboratory)
* Seunghwan Baek (Pacific Northwest National Laboratory)
* Pramod Bhuvankar (Lawrence Berkeley National Laboratory)
* Suzanne (Michelle) Bourret (Los Alamos National Laboratory)
* Julia De Toledo Camargo (Pacific Northwest National Laboratory)
* Bailian Chen (Los Alamos National Laboratory)
* Abdullah Cihan (Lawrence Berkeley National Laboratory)
* Dylan Harp (Los Alamos National Laboratory)
* Paul Holcomb (National Energy Technology Laboratory)
* Jaisree Iyer (Lawrence Livermore National Laboratory)
* Elizabeth Keating (Los Alamos National Laboratory)
* Seth King (National Energy Technology Laboratory)
* Greg Lackey (National Energy Technology Laboratory)
* Ernest Lindner (National Energy Technology Laboratory)
* Kayyum Mansoor (Lawrence Livermore National Laboratory)
* Mohamed Mehana (Los Alamos National Laboratory)
* Saro Meguerdijian (Los Alamos National Laboratory)
* Nathaniel Mitchell (National Energy Technology Laboratory)
* Omotayo Omosebi (Lawrence Berkeley National Laboratory)
* Shaparak Salek (National Energy Technology Laboratory)
* Veronika Vasylkivska (National Energy Technology Laboratory)
* Ya-Mei Yang (National Energy Technology Laboratory)
* Yingqi Zhang (Lawrence Berkeley National Laboratory)

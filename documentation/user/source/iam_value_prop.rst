.. value_of_iam:

Value Offered by NRAP-Open-IAM
==============================

Need for GCS Leakage Risk Assessment Tools
------------------------------------------

Carbon capture and storage (CCS) is the process of capturing carbon dioxide (|CO2|) from 
industrial sources :cite:`BainsEtAl2017` or the air :cite:`BreyerEtAl2019` and storing 
it so that it does not enter the atmosphere :cite:`AminuEtAl2017`. For the storage component 
of CCS, one of the most promising options is geologic carbon storage (GCS) :cite:`Bachu2008`. 
During GCS, carbon dioxide (|CO2|) is injected into rock formations in the deep subsurface 
under high pressure as a supercritical fluid :cite:`VilarrasaEtAl2013`. Formations used to 
store |CO2| are often a deep, high-permeability, saline aquifers :cite:`MichaelEtAl2010` with 
little or no history of oil and gas development. Other types of high-permeability units 
have been considered, however (e.g., depleted hydrocarbon reservoirs :cite:`CallasEtAl2022` 
or deep, unmineable coal beds :cite:`JiangEtAl2022`).

Although GCS is a promising approach, the injection of |CO2| underground is also associated with 
the risk of fluid leakage :cite:`GholamiEtAl2021`. Specifically, injecting |CO2| into a formation 
can increase pressures within the formation, which could drive the upward migration of injected |CO2| 
or brine into a shallower formation or the atmosphere. Migration of brine or |CO2| into a freshwater 
aquifer could degrade water quality :cite:`XiaoEtAl2020` and create an environmental, health, and safety 
hazard. |CO2| leaked into the atmosphere could be unsafe to breathe :cite:`FanEtAl2024` and would reduce 
the utility of the project.

Potential leakage pathways include faults :cite:`MeguerdijianJha2021`, fractures :cite:`NewellEtAl2017`, 
and legacy wells :cite:`SuEtAl2023`. Legacy wells are often abandoned oil and gas wells; while such wells 
should have been plugged with cement upon abandonment, cement plugs do not completely remove leakage 
risks :cite:`LackeyEtAl2019`. For example, the integrity of the wellbore's materials might already be 
poor :cite:`KiranEtAl2017` or the materials could become degraded :cite:`ChoiEtAl2013`, allowing fluid 
to leak through the legacy well. In a worst-case scenario, a legacy well may have been improperly 
abandoned :cite:`CalvertSmith1994`. Well abandonment practices have changed over time :cite:`SminchakEtAl2014`, 
and older wells are more likely to have problematic conditions. Furthermore, older wellbores may also be 
missing from well records :cite:`HammackEtAl2018` and may be difficult to find through surveying due to the 
limitations of surveying techniques :cite:`EksethEtAl2007` or the presence of buildings, roads, parking 
lots, or other features created after the well's abandonment :cite:`VeloskiEtAl2007`.

For these reasons, operators and regulators must evaluate all potential leakage risks at candidate GCS sites. 
In particular, operators should consider (1) uncertainties regarding the characteristics of known leakage 
pathways and (2) the potential existence of unknown leakage pathways. It is important for site operators to 
have an objective and quantitative understanding of the impacts of site characteristics (e.g., site geology, 
legacy well properties) and operational decisions on leakage risks (:cite:`CeliaEtAl2005` and :cite:`PawarEtAl2015`). 
For example, operators might need to understand the impacts of increasing or decreasing injection rates :cite:`MitchellEtAl2023`, 
installing a brine extraction well to reduce pressures :cite:`BuscheckEtAl2012`, or uncertainties regarding the 
permeabilities of the reservoir or legacy wells :cite:`GanEtAl2021`. Such a quantitative understanding requires 
computational tools that can represent the physical dynamics of GCS :cite:`C2009`.

NRAP-Open-IAM is a computational tool for the assessment of leakage risks and containment effectiveness 
at GCS sites :cite:`VasylkivskaEtAl2021`. The tool can be used in the preparation of permit applications 
for GCS sites or for operation decision support at an active GCS site. Below, we discuss the alternatives 
to using NRAP-Open-IAM and highlight how NRAP-Open-IAM offers a unique value to the field of GCS.

Leakage Risk Assessment for GCS
-------------------------------

Qualitative risk assessment involves the use of judgement-based approaches such as expert elicitation and 
risk registers (:cite:`GerstenbergerEtAl2013` and :cite:`HT2013`). These approaches generally assign relative 
scores to risks, such as low, medium, and high. Although qualitative approaches can have a valuable role in 
risk assessment, these approaches are subjective and highly dependent on the judgement of the individuals 
participating. If the individuals fail to recognize and weigh certain considerations appropriately, then risks 
could be significantly underestimated.

In contrast, quantitative risk assessment methods are based on objective, reproducible results from physical 
models; such results can offer significant clarity when evaluating risks :cite:`PawarEtAl2015`. There are a 
variety of software products that are capable of building such models for GCS sites. For example, many 
widely-used reservoir simulation tools can simulate leakage risks at GCS sites. Running these simulations, however, 
could require significant time and computational resources because GCS systems are inherently complex and 
include multiple components such as the reservoir, leakage pathways, and leakage receptors. Exploring the 
full parameter space of a simulation (e.g., to understand the influence of uncertainties regarding reservoir 
or wellbore permeabilities) might require the user to manually set up and run a large number of simulations, 
which could be infeasible with a large reservoir simulator.

NRAP-Open-IAM Approach
----------------------

NRAP-Open-IAM is an open-source integrated assessment model developed by the United States Department 
of Energy's National Risk Assessment Partnership (NRAP) for GCS. NRAP was established in 2010 and includes 
researchers from five National Laboratories: the National Energy Technology Laboratory, Los Alamos National 
Laboratory, Lawrence Berkeley National Laboratory, Lawrence Livermore National Laboratory, and Pacific Northwest 
National Laboratory. 

NRAP-Open-IAM was designed to address the computational challenges associated with stochastic leakage risk 
assessment for GCS. The tool has a modular framework, where separate component models simulate the physical 
processes in different elements of a GCS system. For example, a reservoir component simulates the changes in pressures 
and |CO2| saturations in the storage reservoir. The reservoir component is linked to a wellbore component, which 
uses the pressures and |CO2| saturations to calculate leakage rates. The wellbore component is then linked to an 
aquifer component, which calculates the spread of leaked contaminants within an aquifer.

The modular design offers enhanced flexibility, so that the properties of each model can be varied between simulations. 
This framework enables stochastic simulations in which parameter values are rapidly varied across thousands of model 
realizations. This capability is particularly valuable for GCS risk assessment, because many properties of a GCS system 
are highly uncertain.

NRAP-Open-IAM is available for free and it uses open-source software (i.e., if desired, the user can modify the tool). 
The tool was designed for users with different levels of coding experience. In particular, the graphical user 
interface (GUI) for the tool was made to be approachable for anyone, regardless of coding experience.

Many fit-for-purpose workflows designed to support risk-based decision making are included in NRAP-Open-IAM. These 
workflows automate the creation of figures and results. For example, the area of review (AoR) workflow can inform 
the delineation of an AoR, which is a key requirement in a permit application. The time to first detection (TTFD) 
workflow evaluates the spread of contaminant plumes in an aquifer as well as the times at which monitoring wells 
would first detect the plumes. There are other workflows and functionalities in NRAP-Open-IAM that are designed 
to expedite the creation of analyses in support of GCS site planning, permitting, and operation.

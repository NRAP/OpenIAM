# -*- coding: utf-8 -*-
'''
This example uses the SALSA component to simulate subsurface fluid dynamics with 
five active wells (one extraction and four injection) and two leaking wells. There 
are four shale layers and three aquifers. The bottom layer is a shale 
(bottomLayerType is 0). This layer is shale 1, and the aquifer above it is aquifer 
1. Unit numbers increase approaching the surface. Aquifer 1 is the targeted 
storage reservoir.

Many of the SALSA parameter names can be entered with indices for active wells, 
unit numbers, or injection periods (e.g., activeWell2RadiusAquifer3, for the 
radius of active well 2 across aquifer 3). This example demonstrates using 
the word 'All' instead of specific indices. For example, one parameter name 
can be used to set the radii of all active wells across aquifer 3 
(activeWellAllRadiusAquifer3). This approach can simplify parameter assignments, 
but the user needs to be careful when

The user needs to be careful when using 'All' in situations that could lead 
to overlap, however. For example, the parameter name 'activeWellAllQAquifer1Period1' 
applies to all active wells, aquifer 1, and period 1, while the parameter name 
'activeWell1QAquiferAllPeriod1' applies to active well 1, all aquifers, and 
period 1. There is overlap in the applicability of these parameters, and one 
value would overwrite the other in certain cases. If a parameter name is used 
without the word 'All', then it will always overwrite any parameters that include 
the word 'All'.

While the word 'All' can also be used for outputs of the SALSA component in the 
control file interface, it does not work with outputs in a standard scripting 
approach like the one shown here. Only parameters can be set with 'All' in this 
example.

This script was created to have the same setup as the control file 
`ControlFile_ex62b.yaml`.

Example of run:
$ python iam_sys_salsa_2.py
'''

import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, ticker
from datetime import datetime
from openiam.components.iam_base_classes import SystemModel
from openiam.components.salsa_component import SALSA

# TF_ENABLE_ONEDNN_OPTS=0

# Plotting options
FIGSIZE = (11, 8)
GENFONTSIZE = 10
FONTSIZE = 12
LGNFONTSIZE = 8
LINEWIDTH = 2
LINESTYLES = ['-', '--', ':', '-.', (0, (1, 10)), (5, (10, 3)), (0, (3, 10, 1, 10))]
HANDLELENGTH = 7
DPI = 300

FONT = 'Arial'

font = {'family': FONT,
        'weight': 'normal',
        'size': GENFONTSIZE}
plt.rc('font', **font)

def main():
    # Define output directory
    output_dir = os.path.join(os.getcwd(), '..', '..', 'Output')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    now = datetime.now().strftime('%Y-%m-%d_%H.%M.%S')
    output_dir = os.path.join(output_dir, f'iam_sys_salsa_2_{now}')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    csv_dir = os.path.join(output_dir, 'csv_files')
    if not os.path.exists(csv_dir):
        os.makedirs(csv_dir)

    # Define parameters
    num_years = 150
    dt = 10.0
    time_array = 365.25 * np.arange(0, num_years + dt, dt)

    numberOfShaleLayers = 4
    bottomLayerType = 0
    topLayerType = 0
    
    # Calculated as (numberOfShaleLayers + bottomLayerType + topLayerType - 1)
    numberOfAquiferLayers = 3

    # Aquifer properties
    aquiferThicknesses = [145, 27, 62]
    aquiferLogHorizK_list = [-4.5, -4.0, -5.0]
    aquiferLogSS_list = [-5.5, -5.0, -6.0]
    aquiferFluidDensity_list = [1100.0, 1010.0, 1000.0]
    aquiferHead_list = [10.0, 25.0, 5.0]
    
    # aquifer#Gamma value assigned to all aquifers
    aquiferGamma_val = 0
    
    # aquifer#ANSR assigned to all aquifers
    aquiferANSR_val = 1

    # Shale properties
    shaleThicknesses = [56.0, 157.0, 430.0, 52.0]
    shaleLogVertK_list = [-10.5, -10.0, -11.0, -12.0]
    shaleLogSS_list = [-5.5, -5.0, -6.0, -7.0]
    
    # Gamma values assigned to all shale units
    shaleInitialGamma_val = 0
    shaleGamma_val = 0

    # Well properties
    activeWellCoordx = np.array([0.0, 3535.53, -3535.53, -3535.53, 3535.53])
    activeWellCoordy = np.array([0.0, 3535.53, 3535.53, -3535.53, -3535.53])
    
    # Times (years) at which each injeciton period ends. The user can define an 
    # injection (or extraction) rate for each aquifer during each injection 
    # period. There is a default injection rate for aquifer 1 during period 1. 
    # All other combinations of aquifers and injection periods will have default 
    # rates of 0 m^3/s.
    periodEndTimes = [75, 150]
    
    # The three dimensions are for aquifers, active wells, and injection periods
    activeWellQ = np.zeros((
        numberOfAquiferLayers, len(activeWellCoordx), len(periodEndTimes)))
    
    activeWellQ[0, 0, 0] = -0.1   # Active well at x = 0 m, y = 0 m has extraction
    activeWellQ[0, 1:5, 0] = 0.1  # The other active wells have injection
    
    # Radius (m) assigned to all active wells across all aquifers
    activeWellAllRadius_val = 0.1

    leakingWellCoordx = [2828.43, 7071.06]
    leakingWellCoordy = [2828.43, 7071.06]
    
    # Hydraulic conductivity (log10 m/s) values along the leaking wells across 
    # each aquifer and shale
    leakingWellLogK_aq1_val = -4.5
    leakingWellLogK_aq2_val = -4.0
    leakingWellLogK_aq3_val = -5.0
    
    leakingWellLogK_shale1_val = -4.5
    leakingWellLogK_shale2_val = -4.0
    leakingWellLogK_shale3_val = -5.0
    leakingWellLogK_shale4_val = -6.0
    
    # Radius (m) for all leaking wells, across all units
    leakingWellRadius_val = 0.15
    
    # Number of points spread vertically across each shale
    numberOfVerticalPoints = 10
    
    
    
    # x and y (easting and northing) coordinates at which hydraulic head and 
    # pressure outputs will be produced.
    aquiferCoordx = [2828.43, 7071.06]
    aquiferCoordy = [2828.43, 7071.06]
    
    shaleCoordx = [2828.43, 7071.06]
    shaleCoordy = [2828.43, 7071.06]
    
    # Aquifers to make contour plots for
    aquiferNamesContourPlots = [1, 2, 3]

    # Create system model
    sm = SystemModel(model_kwargs={'time_array': time_array})
    
    comp_name = 'salsa'
    salsa = sm.add_component_model_object(SALSA(
        name=comp_name, parent=sm, periodEndTimes=periodEndTimes,
        activeWellCoordx=activeWellCoordx, activeWellCoordy=activeWellCoordy,
        leakingWellCoordx=leakingWellCoordx, leakingWellCoordy=leakingWellCoordy,
        aquiferCoordx=aquiferCoordx, aquiferCoordy=aquiferCoordy,
        shaleCoordx=shaleCoordx, shaleCoordy=shaleCoordy,
        aquiferNamesContourPlots=aquiferNamesContourPlots, output_dir=output_dir))

    # Add parameters of the SALSA component
    salsa.add_par('bottomLayerType', value=bottomLayerType, vary=False)
    salsa.add_par('topLayerType', value=topLayerType, vary=False)
    salsa.add_par('bottomBoundaryCond', value=1, vary=False)
    salsa.add_par('bottomBoundaryHead', value=0.0, vary=False)
    salsa.add_par('topBoundaryCond', value=0, vary=False)
    salsa.add_par('topBoundaryHead', value=0.0, vary=False)
    salsa.add_par('numberOfLaplaceTerms', value=8, vary=False)
    salsa.add_par('xCent', value=0.0, vary=False)
    salsa.add_par('yCent', value=0.0, vary=False)
    salsa.add_par('xMax', value=10000.0, vary=False)
    salsa.add_par('yMax', value=10000.0, vary=False)
    salsa.add_par('gridExp', value=1.0, vary=False)
    salsa.add_par('numberOfNodesXDir', value=50, vary=False)
    salsa.add_par('numberOfNodesYDir', value=50, vary=False)
    salsa.add_par('numberOfRadialGrids', value=6, vary=False)
    salsa.add_par('radialZoneRadius', value=100.0, vary=False)
    salsa.add_par('radialGridExp', value=1.2, vary=False)
    salsa.add_par('numberOfVerticalPoints', value=numberOfVerticalPoints, vary=False)
    salsa.add_par('numberOfShaleLayers', value=numberOfShaleLayers, vary=False)

    # Add shale parameters
    salsa.add_par('shale1Thickness', value=shaleThicknesses[0], vary=False)
    salsa.add_par('shale1LogVertK', value=shaleLogVertK_list[0], vary=False)
    salsa.add_par('shale1LogSS', value=shaleLogSS_list[0], vary=False)
    
    salsa.add_par('shale2Thickness', value=shaleThicknesses[1], vary=False)
    salsa.add_par('shale2LogVertK', value=shaleLogVertK_list[1], vary=False)
    salsa.add_par('shale2LogSS', value=shaleLogSS_list[1], vary=False)
    
    salsa.add_par('shale3Thickness', value=shaleThicknesses[2], vary=False)
    salsa.add_par('shale3LogVertK', value=shaleLogVertK_list[2], vary=False)
    salsa.add_par('shale3LogSS', value=shaleLogSS_list[2], vary=False)
    
    salsa.add_par('shale4Thickness', value=shaleThicknesses[3], vary=False)
    salsa.add_par('shale4LogVertK', value=shaleLogVertK_list[3], vary=False)
    salsa.add_par('shale4LogSS', value=shaleLogSS_list[3], vary=False)
    
    # Add all shale#Gamma values
    salsa.add_par('shaleAllInitialGamma', value=shaleInitialGamma_val, vary=False)
    salsa.add_par('shaleAllGamma', value=shaleGamma_val, vary=False)

    # Add aquifer parameters
    salsa.add_par('aquifer1Thickness', value=aquiferThicknesses[0], vary=False)
    salsa.add_par('aquifer1LogHorizK', value=aquiferLogHorizK_list[0], vary=False)
    salsa.add_par('aquifer1LogSS', value=aquiferLogSS_list[0], vary=False)
    salsa.add_par('aquifer1FluidDensity', value=aquiferFluidDensity_list[0], vary=False)
    salsa.add_par('aquifer1Head', value=aquiferHead_list[0], vary=False)

    salsa.add_par('aquifer2Thickness', value=aquiferThicknesses[1], vary=False)
    salsa.add_par('aquifer2LogHorizK', value=aquiferLogHorizK_list[1], vary=False)
    salsa.add_par('aquifer2LogSS', value=aquiferLogSS_list[1], vary=False)
    salsa.add_par('aquifer2FluidDensity', value=aquiferFluidDensity_list[1], vary=False)
    salsa.add_par('aquifer2Head', value=aquiferHead_list[1], vary=False)

    salsa.add_par('aquifer3Thickness', value=aquiferThicknesses[2], vary=False)
    salsa.add_par('aquifer3LogHorizK', value=aquiferLogHorizK_list[2], vary=False)
    salsa.add_par('aquifer3LogSS', value=aquiferLogSS_list[2], vary=False)
    salsa.add_par('aquifer3FluidDensity', value=aquiferFluidDensity_list[2], vary=False)
    salsa.add_par('aquifer3Head', value=aquiferHead_list[2], vary=False)
    
    salsa.add_par('aquiferAllANSR', value=aquiferANSR_val, vary=False)
    salsa.add_par('aquiferAllGamma', value=aquiferGamma_val, vary=False)

    # Add active well parameters
    salsa.add_par('activeWell1QAquifer1Period1', value=activeWellQ[0, 0, 0], vary=False)
    salsa.add_par('activeWell2QAquifer1Period1', value=activeWellQ[0, 1, 0], vary=False)
    salsa.add_par('activeWell3QAquifer1Period1', value=activeWellQ[0, 2, 0], vary=False)
    salsa.add_par('activeWell4QAquifer1Period1', value=activeWellQ[0, 3, 0], vary=False)
    salsa.add_par('activeWell5QAquifer1Period1', value=activeWellQ[0, 4, 0], vary=False)
    # By default, there is only an injection rate assigned to aquifer 1 during 
    # injection period 1. Because the injection rates for aquifer 1 during 
    # period 1 have been handled, the rest will be 0 m^3/s.

    # Assign one radius to all active wells across all aquifers
    salsa.add_par('activeWellAllRadiusAquiferAll', value=activeWellAllRadius_val, vary=False)

    # Add leaking well parameters
    # Add the hydraulic conductivities of the leaking wells across each unit, 
    # using the word 'All' instead of specific leaking well numbers. 
    salsa.add_par('leakingWellAllLogKAquifer1', value=leakingWellLogK_aq1_val, vary=False)
    salsa.add_par('leakingWellAllLogKAquifer2', value=leakingWellLogK_aq2_val, vary=False)
    salsa.add_par('leakingWellAllLogKAquifer3', value=leakingWellLogK_aq3_val, vary=False)
    
    salsa.add_par('leakingWellAllLogKShale1', value=leakingWellLogK_shale1_val, vary=False)
    salsa.add_par('leakingWellAllLogKShale2', value=leakingWellLogK_shale2_val, vary=False)
    salsa.add_par('leakingWellAllLogKShale3', value=leakingWellLogK_shale3_val, vary=False)
    salsa.add_par('leakingWellAllLogKShale4', value=leakingWellLogK_shale4_val, vary=False)
    
    # Add the radii for all leaking well across each unit
    salsa.add_par('leakingWellAllRadiusAquiferAll', value=leakingWellRadius_val, vary=False)
    salsa.add_par('leakingWellAllRadiusShaleAll', value=leakingWellRadius_val, vary=False)
    
    # Add the status values (unscreenedL 0, or screened: 1) across each aquifer 
    # for all leaking wells
    salsa.add_par('leakingWellAllStatAquiferAll', value=1, vary=False)
    
    # Add the status values (plugged: 0, or unplugged: 1) across each shale for 
    # all leaking wells
    salsa.add_par('leakingWellAllStatShale1', value=1, vary=False)
    salsa.add_par('leakingWellAllStatShale2', value=1, vary=False)
    salsa.add_par('leakingWellAllStatShale3', value=0, vary=False)

    # Add observations
    observations = [
        'headLoc1Aquifer1', 'headLoc1Aquifer2', 
        'pressureLoc1Aquifer1', 'pressureLoc1Aquifer2',
        'diffuseLeakageRateBottomAquifer1', 'diffuseLeakageRateBottomAquifer2',
        'diffuseLeakageVolumeBottomAquifer1', 'diffuseLeakageVolumeBottomAquifer2',
        'diffuseLeakageRateTopAquifer1', 'diffuseLeakageRateTopAquifer2',
        'diffuseLeakageVolumeTopAquifer1', 'diffuseLeakageVolumeTopAquifer2',
    ]
    
    numberOfProfiles = len(shaleCoordx)
    
    # Get lists for the combinations of leaking wells and aquifer numbers
    well_nums = list(range(1, len(leakingWellCoordx) + 1)) * numberOfAquiferLayers
    aq_nums = list(range(1, numberOfAquiferLayers + 1)) * len(well_nums)
    
    # Leakage rates and volumes from each individual leaking well into each aquifer
    indiv_well_lr_obs = [f'well{wellRef}LeakageRateAquifer{aqNum}' 
                         for (wellRef, aqNum) in zip(well_nums, aq_nums)]
    
    indiv_well_lv_obs = [f'well{wellRef}LeakageVolumeAquifer{aqNum}' 
                         for (wellRef, aqNum) in zip(well_nums, aq_nums)]
    
    observations += indiv_well_lr_obs
    observations += indiv_well_lv_obs
    
    # Leakage rates and volumes from all leaking well into each aquifer
    total_well_lr_obs = [f'wellLeakageRateAquifer{aqNum}' 
                         for aqNum in range(1, numberOfAquiferLayers + 1)]
    
    total_well_lv_obs = [f'wellLeakageVolumeAquifer{aqNum}' 
                         for aqNum in range(1, numberOfAquiferLayers + 1)]
    
    observations += total_well_lr_obs
    observations += total_well_lv_obs
    
    # Get lists for the combinations of aquiferCoordx locations and aquifer numbers
    loc_nums = list(range(1, len(aquiferCoordx) + 1)) * numberOfAquiferLayers
    aq_nums = list(range(1, numberOfAquiferLayers + 1)) * len(aquiferCoordx)
    
    # Add the hydraulic head and pressure observations for each aquifer at each 
    # location (only bottom pressure, not top or middle)
    metrics = ['head', 'pressure']
    for metric_name in metrics:
        observations += [f'{metric_name}Loc{locRef}Aquifer{aqNum}' 
                         for (locRef, aqNum) in zip(loc_nums, aq_nums)]
        
        # Add the head and pressure observations within shales
        for profileRef in range(1, numberOfProfiles + 1):
            for vert_point in range(1, numberOfVerticalPoints + 1):
                for shaleRef in range(1, numberOfShaleLayers + 1):
                    observations.append(f'{metric_name}Profile{profileRef}VertPoint{vert_point}Shale{shaleRef}')

    SALSA_GRID_OBSERVATIONS = [
       'profileDepths', 'allHeadProfilesShales', 'contourPlotAquiferHead', 
       'allPressureProfilesShales', 'contourPlotAquiferPressure',
       'contourPlotCoordx', 'contourPlotCoordy']
    
    observations.extend(SALSA_GRID_OBSERVATIONS)
    
    for obs in observations:
        if obs in SALSA_GRID_OBSERVATIONS:
            salsa.add_grid_obs(
                obs, constr_type='matrix', output_dir=output_dir)
        else:
            salsa.add_obs(obs)

    sm.forward()

    ##############
    # Outputs    #
    ##############
    output_data = {}
    for obs in observations:

        if not obs in SALSA_GRID_OBSERVATIONS:
            try:
                data = sm.collect_observations_as_time_series(salsa, obs)
                output_data[obs] = data
                output_file = os.path.join(csv_dir, f'{obs}.csv')
                np.savetxt(output_file, np.column_stack((time_array, data)), 
                           delimiter=',', header=f'Time,{obs}', comments='')
            except Exception as e:
                print(f"Error fetching or saving data for observation {obs}: {e}")
        else:
            output_data[obs] = 'SAVED_NPZ_FILE'

    ##############
    # Plotting   #
    ##############
    plt.close('all')
    plot_configurations = {
        'Diffuse_Leakage_Rates': [
            'diffuseLeakageRateBottomAquifer1', 'diffuseLeakageRateTopAquifer1', 
            'diffuseLeakageRateBottomAquifer2', 'diffuseLeakageRateTopAquifer2', 
            'diffuseLeakageRateBottomAquifer3', 'diffuseLeakageRateTopAquifer3'
        ],
        'Diffuse_Leakage_Volumes': [
            'diffuseLeakageVolumeBottomAquifer1', 'diffuseLeakageVolumeTopAquifer1',
            'diffuseLeakageVolumeBottomAquifer2', 'diffuseLeakageVolumeTopAquifer2', 
            'diffuseLeakageVolumeBottomAquifer3', 'diffuseLeakageVolumeTopAquifer3'
        ],
        'Total_Well_Leakage_Volumes': total_well_lv_obs,
        'Total_Well_Leakage_Rates': total_well_lr_obs,
        'Individual_Well_Leakage_Volumes': indiv_well_lv_obs,
        'Individual_Well_Leakage_Rates': indiv_well_lr_obs,
    }
    
    for plot_name, obs_list in plot_configurations.items():
        y_label_addition = ''
        if 'Rate' in plot_name:
            y_label_addition = ' (m$^3$/s)'
        elif 'Volume' in plot_name:
            y_label_addition = ' (m$^3$)'
        
        plt.figure(figsize=FIGSIZE, dpi=DPI)
        
        lstyle_ref = 0
        for obs in obs_list:
            if obs in output_data:
                plt.plot(time_array / 365.25, output_data[obs], label=obs, 
                         linewidth=LINEWIDTH, linestyle=LINESTYLES[lstyle_ref])
                
                lstyle_ref += 1 
                if lstyle_ref >= len(LINESTYLES):
                    lstyle_ref = 0
                
        plt.xlabel('Time (years)', fontsize=FONTSIZE, fontweight='bold')
        plt.ylabel(plot_name.replace('_', ' ') + y_label_addition, 
                   fontsize=FONTSIZE, fontweight='bold')
        
        ax = plt.gca()
        ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
        
        plt.title(f'{plot_name.replace("_", " ")} Over Time', 
                  fontsize=FONTSIZE, fontweight='bold')
        plt.legend(fontsize=LGNFONTSIZE, handlelength=HANDLELENGTH)
        
        plt.savefig(os.path.join(output_dir, f'{plot_name}.png'))
        plt.show()

    plot_specific_observations(output_data, time_array, output_dir, comp_name, 
                               aquiferNamesContourPlots, activeWellCoordx, 
                               activeWellCoordy, leakingWellCoordx, 
                               leakingWellCoordy, aquiferCoordx, aquiferCoordy, 
                               numberOfShaleLayers, numberOfAquiferLayers)

def plot_specific_observations(output_data, time_array, output_dir, comp_name, 
                               aquiferNamesContourPlots, activeWellCoordx, 
                               activeWellCoordy, leakingWellCoordx, 
                               leakingWellCoordy, aquiferCoordx, aquiferCoordy, 
                               numberOfShaleLayers, numberOfAquiferLayers,
                               cmap='Spectral_r'):
    def make_plot(plotInd, obs_list, metric_label, metric_units, unit_type, 
                  figsize=FIGSIZE, diff_subplots=False, max_subplots=None):
        plt.figure(plotInd, figsize=figsize, dpi=DPI)
        
        plotInd += 1
        
        # Excludes the 's' at the end
        unit_type_v2 = unit_type[:-1]
        
        if diff_subplots:
            if max_subplots is None:
                diff_subplots = False
                print(
                    'In the function plot_specific_observations(), cannot use ', 
                    + 'different subplots (diff_subplots = True) unless the max_subplots ',  
                    + 'keyword argument is provided.')
        else:
            ax = plt.gca()
        
        min_value = 9.99e+99
        max_value = -9.99e+99
        lstyle_ref = 0
        for obs in obs_list:
            if obs in output_data:
                if diff_subplots:
                    unitNum = int(obs[obs.index(unit_type_v2) + len(unit_type_v2)])
                    
                    ax = plt.subplot(max_subplots, 1, unitNum)
                
                ax.plot(time_array / 365.25, output_data[obs], label=obs, 
                         linewidth=LINEWIDTH, linestyle=LINESTYLES[lstyle_ref])
                
                lstyle_ref += 1 
                if lstyle_ref >= len(LINESTYLES):
                    lstyle_ref = 0
                
                if np.min(output_data[obs]) < min_value:
                    min_value = np.min(output_data[obs])
                
                if np.max(output_data[obs]) > max_value:
                    max_value = np.max(output_data[obs])
        
        if diff_subplots:
            for subplotRef in range(1, max_subplots + 1):
                ax = plt.subplot(max_subplots, 1, subplotRef)
                ax.set_title(f'{unit_type_v2} {subplotRef}', fontsize=FONTSIZE, 
                             fontweight='bold')
                
                if subplotRef == max_subplots:
                    ax.set_xlabel('Time (years)', fontsize=FONTSIZE, fontweight='bold')
                    
                ax.set_ylabel(f'{metric_label} ({metric_units})', fontsize=FONTSIZE, fontweight='bold')
                ax.ticklabel_format(style='sci', axis='y', scilimits=(0, 0), useMathText=True)
                
                ax.set_ylim([min_value, max_value])
                
                # Not using a legend here, space is limited in the subplot
                # ax.legend(fontsize=LGNFONTSIZE, handlelength=HANDLELENGTH)
        else:
            ax.set_xlabel('Time (years)', fontsize=FONTSIZE, fontweight='bold')
            ax.set_ylabel(f'{metric_label} in {unit_type} ({metric_units})', fontsize=FONTSIZE, fontweight='bold')
            
            ax.ticklabel_format(style='sci', axis='y', scilimits=(0, 0), useMathText=True)
            
            ax.set_title(f'Salsa {metric_label} Output in {unit_type}', fontsize=FONTSIZE, fontweight='bold')
            
            ax.legend(fontsize=LGNFONTSIZE, handlelength=HANDLELENGTH)
        
        title_addition = 'Head'
        if metric_label == 'Pressure':
            title_addition = metric_label
        
        plt.savefig(os.path.join(output_dir, f'Salsa{title_addition}TimeSeries_{unit_type}.png'))
                    
        return plotInd
    
    plotInd = 1  # Start plot index at 1
    
    aq_ref_list = list(range(numberOfAquiferLayers)) * len(aquiferCoordx)
    loc_list = list(range(len(aquiferCoordx))) * numberOfAquiferLayers
    
    metrics = [[f'headLoc{locRef + 1}Aquifer{aqRef + 1}' 
                for (locRef, aqRef) in zip(loc_list, aq_ref_list)], 
               [f'pressureLoc{locRef + 1}Aquifer{aqRef + 1}' 
                for (locRef, aqRef) in zip(loc_list, aq_ref_list)], 
               ]
    metric_labels = ['Hydraulic Head', 'Pressure']
    metric_units = ['m', 'Pa']
    for ind, (metric, metric_label, metric_unit) in enumerate(zip(
            metrics, metric_labels, metric_units)):
        try:
            plotInd = make_plot(
                plotInd, metric, metric_label, metric_unit, 'Aquifers')
        except Exception as e:
            print(f"Error plotting Salsa Vertical Profile: {e}")
    
    # Get the head and pressure profile output for aquitards
    aquitard_head_obs_list = []
    aquitard_pressure_obs_list = []
    for obs in output_data.keys():
        if 'headProfile' in obs:
            aquitard_head_obs_list.append(obs)
        
        if 'pressureProfile' in obs:
            aquitard_pressure_obs_list.append(obs)
    
    aquitard_obs_list = [aquitard_head_obs_list, aquitard_pressure_obs_list]
    
    height_shales = 12 * (numberOfShaleLayers / 3)
    figsize_shales = (10, height_shales)
    
    for ind, (metric, metric_label, metric_unit) in enumerate(zip(
            aquitard_obs_list, metric_labels, metric_units)):
        try:
            plotInd = make_plot(
                plotInd, metric, metric_label, metric_unit, 'Shales', 
                figsize=figsize_shales, diff_subplots=True, max_subplots=numberOfShaleLayers)
        except Exception as e:
            print(f"Error plotting Salsa Vertical Profile: {e}")
    
    def fmt(x, pos):
        a, b = '{:.2e}'.format(x).split('e')
        b = int(b)
        return r'${} \times 10^{{{}}}$'.format(a, b)
    
    def get_val_str(val):
        a, b = '{:.2e}'.format(np.min(val)).split('e')
        b = int(b)
        val_str = r'${}\times10^{{{}}}$'.format(a, b)
        
        return val_str

    # Contour plots
    fail_check = False
    try:
        file_name = '{}_{}_sim_0_time_0.npz'.format(comp_name, 'contourPlotCoordx')
        data = np.load(os.path.join(output_dir, file_name))
        contourPlotCoordx = data['data']
        data.close()
        
        file_name = '{}_{}_sim_0_time_0.npz'.format(comp_name, 'contourPlotCoordy')
        data = np.load(os.path.join(output_dir, file_name))
        contourPlotCoordy = data['data']
        data.close()
    except:
        print("Missing contour plot data.")
        fail_check = True
    
    def make_contour_plot(plotInd, metric, metric_label, metric_unit, 
                          contourPlotCoordx, contourPlotCoordy, colormap, 
                          metric_loc='Bottom'):
        # Do not use 'Bottom' when getting the bottom pressure, only 'Mid' or 
        # 'Top' for middle and top.
        metric_loc_str = metric_loc
        if metric_loc == 'Bottom':
            metric_loc_str = ''
        
        metric_name_for_npz = f'contourPlot{metric_loc_str}Aquifer{metric}'
        
        # In a stochastic simulation the index 'sim_0' would be varied for 
        # different realizations (e.g., 'sim_1', 'sim_2', ... 'sim_N' where 
        # N is the number of realizations).
        file_name = '{}_{}_sim_0_time_0.npz'.format(comp_name, metric_name_for_npz)
        file_path = os.path.join(output_dir, file_name)
        
        if os.path.exists(file_path):
            data = np.load(file_path)
            contourPlotAquiferMetric = data['data']
            data.close()
            
            for aquInd, aquName in enumerate(aquiferNamesContourPlots):
                min_val = np.min(contourPlotAquiferMetric[:, aquInd, :])
                max_val = np.max(contourPlotAquiferMetric[:, aquInd, :])
                values_for_cbar = np.linspace(min_val, max_val, 200)
                
                for timeInd, time in enumerate(time_array):
                    plt.figure(plotInd, figsize=FIGSIZE, dpi=DPI)
                    plotInd += 1
                    
                    values = contourPlotAquiferMetric[timeInd, aquInd, :]

                    plt.tricontourf(contourPlotCoordx / 1000, contourPlotCoordy / 1000, 
                                    values, cmap=colormap, levels=values_for_cbar, 
                                    locator=ticker.MaxNLocator(
                                        nbins=100, prune='lower'))
                    
                    for ind, (injX, injY) in enumerate(zip(
                            activeWellCoordx, activeWellCoordy)):
                        if ind == 0:
                            plt.plot(injX / 1000, injY / 1000, marker='s', color='k', 
                                     markerfacecolor='none', linestyle='none', 
                                     linewidth=LINEWIDTH, label='Injection Well')
                        else:
                            plt.plot(injX / 1000, injY / 1000, marker='s', color='k', 
                                     markerfacecolor='none', linestyle='none', 
                                     linewidth=LINEWIDTH)
                    
                    for ind, (leakX, leakY) in enumerate(zip(
                            leakingWellCoordx, leakingWellCoordy)):
                        if ind == 0:
                            plt.plot(leakX / 1000, leakY / 1000, marker='o', color='k', 
                                     markerfacecolor='none', linestyle='none', 
                                     linewidth=LINEWIDTH, label='Leaking Well')
                        else:
                            plt.plot(leakX / 1000, leakY / 1000, marker='o', color='k', 
                                     markerfacecolor='none', linestyle='none', 
                                     linewidth=LINEWIDTH)
                    
                    plt.xlabel('X (km)', fontsize=FONTSIZE, fontweight='bold')
                    plt.ylabel('Y (km)', fontsize=FONTSIZE, fontweight='bold')
                    
                    min_val_str = get_val_str(np.min(values))
                    max_val_str = get_val_str(np.max(values))
                    
                    if np.min(values) == np.max(values):
                        range_str = f', {metric_label} Value: {min_val_str} {metric_unit}'
                    else:
                        range_str = f', {metric_label} Range: {min_val_str} {metric_unit} to {max_val_str} {metric_unit}'
                    
                    # Used to specify if the pressure is taken from the bottom, 
                    # middle, or top of the aquifer.
                    metric_loc_str = ''
                    if metric_loc != '':
                        if metric_loc == 'Mid':
                            metric_loc_str = 'Middle of '
                        else:
                            metric_loc_str = metric_loc + ' of '
                    
                    plt.title(f'{metric_loc_str}Aquifer {aquName}, t = {time / 365.25} years' + range_str, 
                              fontsize=FONTSIZE, fontweight='bold')
                    
                    ax = plt.gca()
                    cbar = plt.colorbar(cm.ScalarMappable(cmap=colormap), ax=ax, 
                                        values=values_for_cbar, 
                                        format=ticker.FuncFormatter(fmt))
                    cbar.set_label(f'{metric_label} ({metric_unit})', rotation=90, 
                                   fontsize=FONTSIZE, fontweight='bold')
                    tick_locator = ticker.MaxNLocator(nbins=5)
                    cbar.locator = tick_locator
                    cbar.update_ticks()
                    
                    plt.legend(fontsize=LGNFONTSIZE)
                    
                    plt.savefig(os.path.join(
                        output_dir, f'SalsaContourPlot_Aquifer{aquName}_{metric_loc}{metric}_timeInd{timeInd}.png'), 
                        dpi=DPI)
        else:
            print(f'File required for a contour plot is not present: {file_path}')
        
        return plotInd
    
    if not fail_check:
        # Used to specify if the pressure should be taken as the top ('Top'), 
        # bottom ('Bottom'), or middle ('Mid') of the aquifer. Set to '' for Head.
        aq_locations = ['', 'Bottom']
        
        metrics = ['Head', 'Pressure']
        metric_labels = ['Hydraulic Head', 'Pressure']
        metric_units = ['m', 'Pa']
        # Have different colormaps for the metrics, so the plots can be distinguished more easily
        colormaps = ['Spectral_r', 'RdYlBu_r']
        
        for ind, (metric, metric_loc, metric_label, metric_unit, colormap) in enumerate(zip(
                metrics, aq_locations, metric_labels, metric_units, colormaps)):
            try:
                plotInd = make_contour_plot(
                    plotInd, metric, metric_label, metric_unit, 
                    contourPlotCoordx, contourPlotCoordy, colormap=colormap, 
                    metric_loc=metric_loc)
            except Exception as e:
                print(f"Error plotting Salsa Contour Plot: {e}")
    
    plt.show()

if __name__ == '__main__':
    main()

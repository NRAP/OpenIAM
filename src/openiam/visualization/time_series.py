# -*- coding: utf-8 -*-
"""
Code to create different time-series visualizations for NRAP-Open-IAM.

Last Modified: June, 2023

@author: Seth King
@author: Nate Mitchell (Nathaniel.Mitchell@NETL.DOE.GOV)
@author: Veronika Vasylkivska (Veronika.Vasylkivska@NETL.DOE.GOV)
LRST (Battelle/Leidos) supporting NETL
"""
import logging
import math
import re

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as clrs

from openiam.matk.sampleset import percentile, mean
from .label_setup import (LEGEND_DICT, Y_LABEL_DICT, Y_LABEL_SPLIT_DICT, 
                          Y_LABEL_2ROWS_DICT, Y_LABEL_2ROWS_SPLIT_DICT,
                          TITLE_DICT, TITLE_SPLIT_DICT, UNIT_DICT, 
                          SIMILAR_PARTIAL_METRICS_DICT, Y_LABEL_DICT_MULT_METRICS, 
                          TITLE_DICT_MULT_METRICS)

# Constants used to adjust figure formatting
DEFAULT_FIG_WIDTH = 13
DEFAULT_FIG_HEIGHT = 8
AXIS_LABEL_PAD_REF = 4
TITLE_PAD_REF = 3
SINGLE_PLOT_FONTSIZE_ADJUST = 1.25

MIN_FONT_SIZE = 3

AX_LINEWIDTH_REF = 1.5
LINEWIDTH_REF = 2.5
XTICK_SIZE_REF = 5
YTICK_SIZE_REF = 5
XTICK_WIDTH_REF = 1.5
YTICK_WIDTH_REF = 1.5

# These are used to check if the x and y labels are too long relative to the subplot.
# These values were found by trial and error.
MAX_YLABEL_HEIGHT_FRAC = 0.85
MAX_YLABEL_WIDTH_FRAC = 0.075

MAX_XLABEL_HEIGHT_FRAC = 0.6
MAX_XLABEL_WIDTH_FRAC = 0.75

MAX_TITLE_HEIGHT_FRAC = 0.25
MAX_TITLE_WIDTH_FRAC = 0.67

# These versions are used if the savespace option is false
MAX_XLABEL_HEIGHT_FRAC_V2 = 0.4
MAX_XLABEL_WIDTH_FRAC_V2 = 0.5

MAX_TITLE_HEIGHT_FRAC_V2 = 0.1
MAX_TITLE_WIDTH_FRAC_V2 = 0.4


LEGEND_ITEM_THRESH1 = 5
LEGEND_ITEM_THRESH2 = 10
LEGEND_ITEM_THRESH3 = 15
LEGEND_ITEM_THRESH4 = 20

LEGEND_FRAME_ALPHA = 0.5
LEGEND_COLUMNS = 1
LEGEND_HANDLE_LENGTH = 2
LEGEND_HANDLE_LENGTH_VARY_LINESTYLES = 9

NUMBER_LINES_THRESH = 30

# If the number of subplots is > MAX_SUBPLOTS, the number of subplots calculated 
# is only used if 'Use: True' is included under the 'Subplots' key in plots_data.
MAX_SUBPLOTS = 12

# When plotting results using iam_post_processor.py for the GUI, the subplot 
# keyword argument (and the subplots_data variable) will not be fully reset 
# between separate graphs. For example, if the 'Use Subplots' button is not 
# checked on the GUI post processor so that separate graphs are made for each 
# output specified, the subplot argument for the second graph will still contain 
# information that was saved for the first graph. This value is used to delete 
# all such information from the subplots_data variable each time a new figure 
# is made.
MAX_SUBPLOT_NUM_FOR_RESET = 100

# These are overwritten with minimum and maximum values for each metric type
PLACEHOLDER_MIN_VAL = 9.9e99
PLACEHOLDER_MAX_VAL = -9.9e99

# By default, subplots with the same metrics are given the same y limits.
DEFAULT_ENFORCE_Y_LIMS = True

# By default, subplot titles include the range for the output in the subplot. 
# If there is one subplot and it includes multiple output types, this is disabled.
DEFAULT_ADD_RANGE_STR = True

# I purposefully included more than 10 marker styles so that the rotation of
# marker styles (when plotting a large number of metrics) is desynchronized from
# the rotation of line colors. The default rotation of 10 line colors is used,
# then two rotations of darker and lighter versions of the same 10 colors are
# used. Having the rotations of marker styles and colors desynchronized may
# help clarify such distinctions. Most simulations wil use many years of data
# with a 1 year timestep, however, in which case markers may be too close together.
defaultMarkerList = ['o', '^', 's', 'd', 'X', 'P', '*', 'p', 'D', 'H',
                     '>', '<', 'v']
# The last three entries are similar to the 'loosely dotted', 
# 'densely dashdotdotted', and 'densely dashed' (spacings changed slightly)
defaultLineStyleList = ['solid', 'dotted', 'dashed', 'dashdot', 
                        (0, (1, 5)), (0, (3, 1, 1, 1, 1, 1)), 
                        (0, (8, 1))]
hatchList = ['|', '-', '+', 'x', 'o', 'O', '.', '*', '/', '\\']


def time_series_plot(output_names, sm, s, plot_data, output_list, name='Figure 1',
                     analysis='forward', savefig=None, title=None, subplot=None,
                     plot_type=None, figsize=None, fontname='Arial',
                     gen_font_size=None, x_axis_label_font_size=None, 
                     y_axis_label_font_size=None, title_font_size=None, 
                     sup_title_font_size=None, legend_font_size=None, 
                     bold_labels=True, useMathTextOption=True, generate_title=True,
                     plot_grid_option=True, grid_alpha_val=0.15, close_figs=True, 
                     savespace=True, remove_tick_labels=False, tight_layout=True):
    """
    Makes a time series plots of statistics of data in output_names.
    Highlights percentiles, mean, and median values.

    :param output_names: List of observation names to match with component models and plot.
    Examples:
        output_names=['pressure']

        output_names=['pressure', 'mass_CO2_aquifer1']

        output_names=['pressure', 'CO2_aquifer1', 'CO2_aquifer2']
    :type output_names: list

    :param sm: OpenIAM System model for which plots are created
    :type sm: openiam.SystemModel object

    :param s: SampleSet with results of analysis performed on the system model.
        None for forward analysis.
    :type s: matk.SampleSet object

    :param plot_data: Dictionary of setup for a given plot
    :type plot_data: dict

    :param output_list: Dictionary mapping component models to observations
    Examples:
        This dictionary only includes pressure from an AnalyticalReservoir named ares.
        output_list={ares: 'pressure'}

        This dictionary includes pressure from an AnalyticalReservior named ares as well
        as well as the CO2 leakage rates to aquifers 1 and 2. The CO2 leakage rates
        come from a MultisegmentedWellbore named ms.
        output_list = {ares: ['pressure', 'CO2saturation'], 
                       ms: ['CO2_aquifer1', 'CO2_aquifer2']}
    :type output_list: dict

    :param name: Figure Name to be used/created.
    :type name: str

    :param analysis: Type of OpenIAM system analysis performed ('forward',
        'lhs', or 'parstudy')
    :type analysis: str

    :param savefig: Filename to save resulting figure to. No file saved if None.
    :type savefig: str

    :param title: Optional Title for figure
    :type title: str

    :param subplot: Dictionary for subplot controls, use=True will use
        subplotting (boolean default=False), ncols=n will use n columns
        of subplots (positive integer default 1); comp.var_name=sub_title
        will title the subplots of comp.var_name subtitle (string default=comp.var_name).
        Examples:
            This dictionary creates only one plot (i.e., no subplots). If you are
            plotting multiple types of metrics (e.g., pressure and CO2 leakage
            rates), all metrics will be displayed together and only the yaxis
            label for the metric plotted last will be shown. Do not use
            this setup in such a case: subplot={'use': False}

            This dictionary enables the creation of subplots and specifies
            the use of only one column: subplot={'use': True, 'ncols': 1}

            This dictionary includes figure titles for sres.pressure, ms.CO2_aquifer1, and
            ms.CO2_aquifer2.
            subplot={'use': True, 'ncols': 3, 'sres.pressure': 'Pressure at location',
                     'ms.CO2_aquifer1': 'CO$_2$ Leakage Rate to Aquifer 1',
                     'ms.CO2_aquifer2': 'CO$_2$ Leakage Rate to Aquifer 2'}
    :type subplot: dict

    :param plot_type: List of 'real' and/or 'stats' plot types to produce.
        'real' plots realization values
        'stats' plots quartiles, mean, and median values
    :type plot_type: list of 'real' and/or 'stats'

    :param figsize: width and height of the figure (width, height), in inches. Default is
        None, in which case the DEFAULT_FIG_WIDTH and DEFAULT_FIG_HEIGHT are used.
    :type figsize: tuple or NoneType

    :param fontname: name of the font type to be used
    :type fontname: str

    :param gen_font_size: fontsize for tick labels, etc.
    :type gen_font_size: float or int

    :param x_axis_label_font_size: fontsize for the x-axis label. These font sizes are
        later updated depending on the figsize and number of subplots.
    :type x_axis_label_font_size: float or int

    :param y_axis_label_font_size: fontsize for the y-axis label. These font sizes are
        later updated depending on the figsize and number of subplots.
    :type y_axis_label_font_size: float or int

    :param title_font_size: fontsize for the title. This font size is later
        updated depending on the figsize and number of subplots.
    :type title_font_size: float or int

    :param sup_title_font_size: fontsize for the supplemental title. This font size is later
        updated depending on the figsize and number of subplots.
    :type sup_title_font_size: float or int

    :param legend_font_size: fontsize for the legend. This font size is later
        updated depending on the figsize.
    :type legend_font_size: float or int

    :param bold_labels: option to use bold x and y labels and bold titles. Set to
        True for bold labels, False for normal labels.
    :type bold_labels: bool

    :param useMathTextOption: option for the useMathText option for the y axis.
    :type useMathTextOption: bool

    :param generate_title: option to enable (True) or disable (False) figure titles.
        If no title is included in the subplot dictionary, a title is created. The
        created title will include location numbers if the variable name includes
        a location (e.g., '001' in 'msw1_001.C02_aquifer1').
    :type generate_title: bool

    :param plot_grid_option: option to display a grid (True) or not (False)
    :type plot_grid_option: bool

    :param grid_alpha_val: alpha value for the grid
    :type grid_alpha_val: float

    :param close_figs: Option to close figures (True) or not (False). Should be 
        set to False when using the post processor in the GUI and True when using 
        the control file interface. Otherwise, successive figures in a control 
        file could plot on top of a figure.
    :type close_figs: bool

    :param savespace: option to save space by only using x labels on the bottom 
        subplot in each column of subplots.
    :type savespace: bool

    :param tight_layout: option to use the tight_layout() function.
    :type tight_layout: bool

    :return: None
    """
    selected_keys = ['gen_font_size', 'x_axis_label_font_size', 'y_axis_label_font_size',
                     'title_font_size', 'sup_title_font_size', 'legend_font_size', 
                     'line_width', 'ax_line_width', 'xtick_size', 'ytick_size',
                     'xtick_width', 'ytick_width']

    # Setup for allowing user to pass in font sizes
    if gen_font_size is None:
        gen_font_size = 11
    else:
        selected_keys.remove('gen_font_size')

    if (x_axis_label_font_size is None):
        x_axis_label_font_size = 12
    else:
        selected_keys.remove('x_axis_label_font_size')

    if (y_axis_label_font_size is None):
        y_axis_label_font_size = 12
    else:
        selected_keys.remove('y_axis_label_font_size')

    if title_font_size is None:
        title_font_size = 12
    else:
        selected_keys.remove('title_font_size')

    if sup_title_font_size is None:
        sup_title_font_size = 14
    else:
        selected_keys.remove('sup_title_font_size')

    if(legend_font_size is None):
        legend_font_size = 8
    else:
        selected_keys.remove('legend_font_size')

    # Dictionary with variables used to adjust figure formatting. There are two 
    # entries for x and y axis font sizes - two of them (ending with '_axis_labeL_font_size')
    # are used as reference values for resetting, while two (ending with 'axis_font_size') 
    # are adjusted during the creation of the figures.
    fig_setup = {'gen_font_size': gen_font_size,
                 'x_axis_label_font_size': x_axis_label_font_size,
                 'y_axis_label_font_size': y_axis_label_font_size,
                 'title_font_size': title_font_size,
                 'sup_title_font_size': sup_title_font_size,
                 'legend_font_size': legend_font_size,
                 'line_width': LINEWIDTH_REF,
                 'ax_line_width': AX_LINEWIDTH_REF,
                 'xtick_size': XTICK_SIZE_REF,
                 'ytick_size': YTICK_SIZE_REF,
                 'xtick_width': XTICK_WIDTH_REF,
                 'ytick_width': YTICK_WIDTH_REF,
                 'axis_label_pad': AXIS_LABEL_PAD_REF,
                 'title_pad': TITLE_PAD_REF,
                 'xaxis_font_size': x_axis_label_font_size,
                 'yaxis_font_size': y_axis_label_font_size}

    if bold_labels:
        fig_setup['label_font_weight'] = 'bold'
    else:
        fig_setup['label_font_weight'] = 'normal'

    if figsize is None:
        figsize = (DEFAULT_FIG_WIDTH, DEFAULT_FIG_HEIGHT)
    else: ##elif 
        # Scale font sizes to the specified figure size. Here, the updated
        # font size scales with the new height or width (relative to the default
        # height or width). This scaling uses the length scale with the largest change
        # relative to the default values (e.g., if height has a larger change, then
        # fontsize is scaled using the specified height value).
        dw_ratio = np.abs(figsize[0] - DEFAULT_FIG_WIDTH) / DEFAULT_FIG_WIDTH
        dh_ratio = np.abs(figsize[1] - DEFAULT_FIG_HEIGHT) / DEFAULT_FIG_HEIGHT

        if figsize != (DEFAULT_FIG_WIDTH, DEFAULT_FIG_HEIGHT):
            if dw_ratio > dh_ratio:
                L1 = DEFAULT_FIG_WIDTH
                L2 = figsize[0]
            elif dw_ratio <= dh_ratio:
                L1 = DEFAULT_FIG_HEIGHT
                L2 = figsize[1]

            # Update the font sizes - some of these are also updated later on, depending
            # on subplot sizes. The fontsize is adjusted depending on the input figsize -
            # sufficiently shrinking the fontsize can be important for small figures, but
            # increasing font sizes in the same way often leads to font that is way too large.
            if (L2 / L1) < 1:
                for key in selected_keys:
                    # The formula is simplified based on initial Nate's idea
                    fig_setup[key] = 0.5 * (1 + L2/L1) * fig_setup[key]
            elif (L2 / L1) > 1:
                for key in selected_keys:
                    # The formula is simplified based on initial Nate's idea
                    fig_setup[key] = 0.25 * (3 + L2/L1) * fig_setup[key]

    # These are updated separately depending on the number of rows and columns
    xaxis_font_size_ref = fig_setup['x_axis_label_font_size']
    yaxis_font_size_ref = fig_setup['y_axis_label_font_size']
    title_font_size_ref = fig_setup['title_font_size']
    sup_title_font_size_ref = fig_setup['sup_title_font_size']
    legend_font_size_ref = fig_setup['legend_font_size']

    # Update matplotlib figure setup
    update_rc_setup(fig_setup, fontname)

    # Find number of subplots
    num_plots, number_lines_single_plot, subplot = get_number_of_subplots(
        output_names, output_list, subplot, plot_data, name)
    
    # Process subplots and their type data
    # subplots_data has the following keys: 'use', 'nrows', 'ncols',
    # 'single_plot', 'type' + (possibly) keys representing titles of subplots
    # if defined by user
    subplots_data = setup_subplots_data(subplot, plot_type, num_plots)

    # Initialize indices
    subplot_ind = 1
    color_ind = 0
    reals_ind = 0
    used_colors = []
    markerRef = 0
    lineStyleRef = 0
    hatchRef = 0

    useMarkers, useLines, varyLineStyles, figdpi, showLegend = get_plot_yaml_input(
        plot_data, name)

    if number_lines_single_plot > NUMBER_LINES_THRESH and subplots_data[
            'single_plot'] and not useMarkers:
        warning_msg = ''.join([
            'The plot {} is set up to show results in a single subplot, but ',
            'the number of outputs shown in the plot is high ({}). When the ', 
            'number of outputs in a single subplot is above {} and markers are ', 
            'not being used, a TimeSeries, TimeSeriesStats, and TimeSeriesAndStats ', 
            'plots are automatically made to use lines and vary the line ', 
            'styles. Otherwise, it can be hard to distinguish the different ', 
            'outputs shown in a single subplot.'
            ]).format(name, number_lines_single_plot, NUMBER_LINES_THRESH)

        logging.warning(warning_msg)

        useLines = True
        varyLineStyles = True

    lgnd_handle_length = LEGEND_HANDLE_LENGTH
    if varyLineStyles:
        lgnd_handle_length = LEGEND_HANDLE_LENGTH_VARY_LINESTYLES

    if not useMarkers:
        markerList = ['None']
    else:
        markerList = defaultMarkerList

    if not useLines:
        lineStyleList = ['None']
        varyLineStyles = False

        if not useMarkers:
            warning_msg = ''.join([
                'The plot {} was set up to exclude lines and markers. No results ', 
                'would have been shown, so the figure will use markers.'
                ]).format(name)

            logging.warning(warning_msg)

            useMarkers = True
            markerList = defaultMarkerList
    else:
        lineStyleList = defaultLineStyleList

    # Transform time points from days to years
    time = sm.time_array / 365.25

    subplot_ind_current = None

    # Create figure
    fig = plt.figure(num=name, figsize=figsize, dpi=figdpi)

    xaxis_fontsizes_used = []
    yaxis_fontsizes_used = []

    # Loop over observation names in outputs
    for obs_to_plot in output_names:
        # If there's more than one metric type in this plot, use the more 
        # generalized form (if there is one)
        metric_type = get_metric_type_key_for_axis_lims(obs_to_plot)

        # Some of the sizes can be adjusted within the loop, so reset them each time
        fig_setup = reset_sizes(fig_setup, xaxis_font_size_ref, yaxis_font_size_ref,
                                title_font_size_ref, sup_title_font_size_ref, 
                                legend_font_size_ref)

        # If this figure has only one plot, slightly increase the font sizes
        # (no risk of overlap with other subplots)
        if subplots_data['single_plot']:
            fig_setup = update_single_plot_setup(fig_setup, fontname)

        # List of components providing given observation
        cmpnts_to_process = process_components(obs_to_plot, output_list)

        for cmpnt_obj in cmpnts_to_process:
            cmpnt_name = cmpnt_obj.name
            full_obs_nm = '.'.join([cmpnt_name, obs_to_plot])

            # Add subplot
            ax = plt.subplot(
                subplots_data['nrows'], subplots_data['ncols'], subplot_ind)

            subplot_ind_current = subplot_ind

            if not 'subplot{}'.format(subplot_ind) in subplots_data:
                subplots_data['subplot{}'.format(subplot_ind)] = {}

                # Used to keep track of what output type is in each subplot
                subplots_data['subplot{}'.format(subplot_ind)]['MetricTypes'] = []

            if not metric_type in subplots_data['subplot{}'.format(
                    subplot_ind)]['MetricTypes']:
                subplots_data['subplot{}'.format(subplot_ind)][
                    'MetricTypes'].append(metric_type)

            if not metric_type in subplots_data['MetricRanges']:
                # The lower and upper limits for each metric, overwritten below
                subplots_data['MetricRanges'][metric_type] = [
                    PLACEHOLDER_MIN_VAL, PLACEHOLDER_MAX_VAL]

            if not subplots_data['single_plot']:
                subplot_ind += 1
                color_ind = 0
                reals_ind = 0
                used_colors = []
                markerRef = 0
                lineStyleRef = 0

            lgnd_label, label, lgnd_label_mult_metrics, label_mult_metrics, \
                loc_ind = generate_legend_setup(obs_to_plot, cmpnt_name, analysis)

            if subplots_data['single_plot'] and (
                    len(output_names) > 1 or obs_to_plot != metric_type):
                if 'stats' in subplots_data['type']:
                    label = label_mult_metrics
                    lgnd_label = ', ' + lgnd_label_mult_metrics
                else:
                    label = lgnd_label_mult_metrics

            colorValReal, colorValStats, used_colors, color_ind, \
                hatch_check = get_colors(reals_ind, color_ind, used_colors,
                                         subplots_data)

            if useMarkers and not colorValReal is None:
                rgbReal = clrs.to_rgba(colorValReal[:])
                markerEdgeColor = np.array(list(rgbReal[:]))
                markerEdgeColor /= 2
                markerEdgeColor[-1] = 1
            else:
                markerEdgeColor = 'None'

            if analysis == 'forward':
                values = sm.collect_observations_as_time_series(
                    cmpnt_obj, obs_to_plot)
                ax.plot(time, values, '-', label=label, color=colorValReal,
                        marker=markerList[markerRef],
                        markeredgecolor = markerEdgeColor,
                        linestyle=lineStyleList[lineStyleRef], alpha=0.8,
                        linewidth=fig_setup['line_width'])
                reals_ind = reals_ind + 1

            elif analysis in ['lhs', 'parstudy']:
                ind_list = list(range(len(time)))
                obs_names = [full_obs_nm + '_{0}'.format(indd)
                             for indd in ind_list]
                obs_percentiles = percentile(s.recarray[obs_names],
                                             [0, 25, 50, 75, 100])
                obs_means = mean(s.recarray[obs_names])
                values = np.array(
                    [s.recarray[full_obs_nm + '_'
                                + str(indd)] for indd in ind_list])

                if 'real' in subplots_data['type']:
                    if 'stats' in subplots_data['type']:
                        ax.plot(time, values, color=colorValReal,
                                marker=markerList[markerRef],
                                markeredgecolor = markerEdgeColor,
                                linestyle=lineStyleList[lineStyleRef],
                                label=label, linewidth=fig_setup['line_width'],
                                alpha=0.33, zorder = 0)
                    else:
                        ax.plot(time, values, color=colorValReal,
                                marker=markerList[markerRef],
                                markeredgecolor = markerEdgeColor,
                                linestyle=lineStyleList[lineStyleRef],
                                label=label, linewidth=fig_setup['line_width'],
                                alpha=0.8)
                    reals_ind = reals_ind + 1

                if 'stats' in subplots_data['type']:
                    if not hatch_check:
                        ax.fill_between(time, obs_percentiles[3, :],
                                        obs_percentiles[4, :],
                                        label='Upper and lower quartiles' + lgnd_label,
                                        color=colorValStats, alpha=0.15)
                        ax.fill_between(time, obs_percentiles[1, :],
                                        obs_percentiles[3, :],
                                        label='Middle quartiles' + lgnd_label,
                                        color=colorValStats, alpha=0.3)
                        ax.fill_between(time, obs_percentiles[0, :],
                                        obs_percentiles[1, :],
                                        color=colorValStats, alpha=0.15)
                        ax.plot(time, obs_percentiles[2, :], color=colorValStats,
                                label='Median value' + lgnd_label,
                                linewidth = fig_setup['line_width'],
                                linestyle = ':', alpha=0.8)
                        ax.plot(time, obs_means, color=colorValStats,
                                label='Mean value' + lgnd_label,
                                linewidth = fig_setup['line_width'], alpha=0.8)

                    elif hatch_check:
                        ax.fill_between(time, obs_percentiles[3, :],
                                        obs_percentiles[4, :],
                                        label='Upper and lower quartiles' + lgnd_label,
                                        color=colorValStats, alpha=0.15,
                                        hatch = hatchList[hatchRef])
                        ax.fill_between(time, obs_percentiles[1, :],
                                        obs_percentiles[3, :],
                                        label='Middle quartiles' + lgnd_label,
                                        color=colorValStats, alpha=0.3,
                                        hatch = hatchList[hatchRef])
                        ax.fill_between(time, obs_percentiles[0, :],
                                        obs_percentiles[1, :],
                                        color=colorValStats, alpha=0.15,
                                        hatch = hatchList[hatchRef])
                        ax.plot(time, obs_percentiles[2, :], color=colorValStats,
                                label='Median value' + lgnd_label,
                                linewidth = fig_setup['line_width'],
                                linestyle = ':', alpha=0.8)
                        ax.plot(time, obs_means, color=colorValStats,
                                label='Mean value' + lgnd_label,
                                linewidth = fig_setup['line_width'], alpha=0.8)
                        hatch_check = False
                        hatchRef += 1
                        if hatchRef > (len(hatchList) - 1):
                            hatchRef = 0

                    color_ind += 1
            
            try:
                min_val = np.min(values[~np.isnan(values)])
                max_val = np.max(values[~np.isnan(values)])
            except:
                min_val = ''
                max_val = ''

            if min_val != '':
                if min_val < subplots_data['MetricRanges'][
                        metric_type][0]:
                    subplots_data['MetricRanges'][metric_type][0] = min_val

            if max_val != '':
                if max_val > subplots_data['MetricRanges'][
                        metric_type][1]:
                    subplots_data['MetricRanges'][metric_type][1] = max_val

            if useMarkers:
                markerRef += 1
                if markerRef > (len(defaultMarkerList) - 1):
                    markerRef = 0

            if varyLineStyles:
                lineStyleRef += 1
                if lineStyleRef > (len(defaultLineStyleList) - 1):
                    lineStyleRef = 0

            if plot_grid_option:
                ax.grid(alpha=grid_alpha_val)

            if not subplots_data['single_plot']:
                # Set up offset so we can have x axis show on uneven subplots
                if (num_plots < subplots_data['nrows'] * subplots_data['ncols']):
                    offset = subplots_data['nrows'] * subplots_data['ncols'] - num_plots
                else:
                    offset = 0

                # When savespace is enabled, only plot x_labels for lowest height subplots
                if (not savespace or subplot_ind_current > (subplots_data['nrows'] - 1) * subplots_data['ncols'] - offset): 
                    # Shows x-axis label
                    fig_setup = adjust_x_label(ax, fig, fig_setup, subplots_data, savespace)
                else:
                    # If enabled, remove tick labels for subplots without x label
                    if remove_tick_labels:
                        ax.set_xticklabels([])
            else:
                fig_setup = adjust_x_label(ax, fig, fig_setup, subplots_data, savespace)

            # Shows y-axis label
            fig_setup = adjust_y_label(obs_to_plot, cmpnt_name, ax, fig,
                                       fig_setup, subplots_data, subplot_ind_current, 
                                       useMathTextOption)

            xaxis_fontsizes_used.append(fig_setup['xaxis_font_size'])
            yaxis_fontsizes_used.append(fig_setup['yaxis_font_size'])

            if generate_title:
                # Obtain title for the subplot
                sub_title = get_title(obs_to_plot, cmpnt_name, subplots_data, 
                                      loc_ind, min_val, max_val, metric_type, 
                                      output_names, subplot_ind_current)

                # Shows subplot title
                fig_setup = adjust_title(sub_title, ax, fig, fig_setup, 
                                         subplots_data, savespace)

    if subplots_data['EnforceYLims'] and subplot_ind_current is not None:
        enforce_y_lims(subplot_ind_current, subplots_data)

    if len(xaxis_fontsizes_used) == 0:
        warning_msg = ''.join([
            'The plot {} was not able to plot any simulation results. '.format(name),
            'This outcome can occur if there is an error in the setup of a ',
            'TimeSeries plot entry. For example, the input is case-sensitive, ',
            'so using "TimeSeries: [Pressure]" will fail to plot pressure results. ',
            'In this hypothetical example, the plot entry should say ',
            '"TimeSeries: [pressure]" because the name of the metric is pressure ',
            '(not Pressure). Alternatively, the component producing the output ',
            'may not have been included in the "Components" list in the "ModelParams" ',
            'section of the .yaml file - in this case, the component would not ',
            'be used during the simulation. Check your input.'])
        logging.warning(warning_msg)

        # Add subplot
        ax = plt.subplot(1, 1, 1)
        ax.text(0.01, 0.55, 'Results for the given output ({}) could '.format(
            output_names) + 'not be displayed for the figure {}. '.format(name)
            + 'Check your input\nfor errors (e.g., typos in metric names, '
            + 'component producing the output failed to run because it was not '
            + 'set up properly, etc.).', fontsize=gen_font_size)

        min_fontsize = None

    else:
        min_fontsize = make_label_fontsizes_uniform(
            subplot_ind - 1, xaxis_fontsizes_used, yaxis_fontsizes_used,
            subplots_data)

        if fig_setup['gen_font_size'] > min_fontsize:
            fig_setup['gen_font_size'] = min_fontsize
            update_rc_setup(fig_setup, fontname)

    # Used to reset the legend font size within the loop through the axes.
    # Otherwise, the font size could shrink from one subplot to the next.
    legend_font_size_ref2 = fig_setup['legend_font_size']

    if analysis == 'forward' and showLegend:
        ax_list = fig.axes
        for ax in ax_list:
            handle_list = []
            label_list = []
            handles, labels = ax.get_legend_handles_labels()

            for handle, label in zip(handles, labels):
                if label not in label_list:
                    handle_list.append(handle)
                    label_list.append(label)

            fig_setup['legend_font_size'] = legend_font_size_ref2

            fig_setup = check_legend(handle_list, fig_setup, min_fontsize,
                                     subplots_data)

            ax.legend(handle_list, label_list, fancybox=False,
                      fontsize=fig_setup['legend_font_size'],
                      framealpha=fig_setup['legend_framealpha'],
                      ncols=fig_setup['legend_columns'], 
                      handlelength=lgnd_handle_length)

    elif analysis in ['lhs', 'parstudy'] and showLegend:
        ax_list = fig.axes
        for ax in ax_list:
            handle_list = []
            label_list = []
            handles, labels = ax.get_legend_handles_labels()
            for handle, label in zip(handles, labels):
                if label not in label_list:
                    handle_list.append(handle)
                    label_list.append(label)

            fig_setup['legend_font_size'] = legend_font_size_ref2

            fig_setup = check_legend(handle_list, fig_setup, min_fontsize,
                                     subplots_data)

            ax.legend(handle_list, label_list, fancybox=False,
                      fontsize=fig_setup['legend_font_size'],
                      framealpha=fig_setup['legend_framealpha'],
                      ncols=fig_setup['legend_columns'])
    else:
        pass

    if title:
        fig.suptitle(title, fontweight=fig_setup['label_font_weight'],
                     fontsize=fig_setup['sup_title_font_size'])

    # With 3 or more rows, the titles and x-axis labels often overlap.
    if subplots_data['single_plot']:
        fig.subplots_adjust(left=0.15, bottom=0.1, right=0.85, top=0.85,
                            wspace=0.1, hspace=0.1)
    elif subplots_data['nrows'] >= 3:
        fig.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9,
                            wspace=0.25, hspace=0.5)
    else:
        fig.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9,
                            wspace=0.25, hspace=0.33)

    if tight_layout:
        plt.tight_layout()

    if savefig:
        try:
            fig.savefig(savefig)
        except ValueError:
            # User has specified plot with a '.' in name but no extension.
            # Add .png as output format.
            savefig += '.png'
            fig.savefig(savefig)
    else:
        fig.show()

    if close_figs:
        plt.close()
    # Restore to default matplotlib settings
    # matplotlib.rcdefaults()


def reset_sizes(fig_setup, xaxis_font_size_ref, yaxis_font_size_ref,
                title_font_size_ref, sup_title_font_size_ref, legend_font_size_ref):
    """
    Reset some of the sizes to original values

    Parameters
    ----------
    fig_setup : dict
        Contains information about sizes of different figure elements.

    Returns updated dictionary sizes
    -------
    """
    fig_setup['xaxis_font_size'] = xaxis_font_size_ref
    fig_setup['yaxis_font_size'] = yaxis_font_size_ref
    fig_setup['title_font_size'] = title_font_size_ref
    fig_setup['sup_title_font_size'] = sup_title_font_size_ref
    fig_setup['legend_font_size'] = legend_font_size_ref
    fig_setup['legend_framealpha'] = LEGEND_FRAME_ALPHA
    fig_setup['legend_columns'] = LEGEND_COLUMNS
    fig_setup['axis_label_pad'] = AXIS_LABEL_PAD_REF
    fig_setup['title_pad'] = TITLE_PAD_REF

    return fig_setup


def update_single_plot_setup(fig_setup, fontname):
    """
    Update some of the relevant fig_setup entries if single plot (without subplots)
    is to be generated.

    Parameters
    ----------
    fig_setup : dict
        Contains information about sizes and properties of different figure elements.

    Returns updated dictionary fig_setup
    """
    fig_setup['gen_font_size'] *= SINGLE_PLOT_FONTSIZE_ADJUST
    fig_setup['xaxis_font_size'] *= SINGLE_PLOT_FONTSIZE_ADJUST
    fig_setup['yaxis_font_size'] *= SINGLE_PLOT_FONTSIZE_ADJUST
    fig_setup['title_font_size'] *= SINGLE_PLOT_FONTSIZE_ADJUST
    fig_setup['sup_title_font_size'] *= SINGLE_PLOT_FONTSIZE_ADJUST
    # The legend font sizes can easily become too big, so the initial
    # adjustment is scaled down
    fig_setup['legend_font_size'] = 0.5 * fig_setup['legend_font_size'] * (
        1 + SINGLE_PLOT_FONTSIZE_ADJUST)
    value = fig_setup['gen_font_size']
    font = {'family': fontname,
            'weight': 'normal',
            'size': value}
    plt.rc('font', **font)
    fig_setup['axis_label_pad'] *= SINGLE_PLOT_FONTSIZE_ADJUST
    fig_setup['title_pad'] *= SINGLE_PLOT_FONTSIZE_ADJUST

    return fig_setup


def update_rc_setup(fig_setup, fontname):
    """
    Update matplotlib.pyplot parameters using information in the fig_setup and fontname
    arguments.

    Parameters
    ----------
    fig_setup : dict
        Contains information about sizes and properties of different figure elements.

    fontname : str
        Name of font for figure elements

    Returns
    -------
    None.
    """
    font = {'family': fontname,
            'weight': 'normal',
            'size': fig_setup['gen_font_size']}
    plt.rc('font', **font)
    plt.rcParams['axes.linewidth'] = fig_setup['ax_line_width']
    plt.rcParams['xtick.major.size'] = fig_setup['xtick_size']
    plt.rcParams['ytick.major.size'] = fig_setup['ytick_size']
    plt.rcParams['xtick.major.width'] = fig_setup['xtick_width']
    plt.rcParams['ytick.major.width'] = fig_setup['ytick_width']


def get_number_of_subplots(output_names, cmpnt_to_output, subplot, plot_data, 
                           name):
    """
    Calculate required number of subplots for the requested outputs.

    :param output_names: List of observation names to match with component models.
    Examples:
        output_names=['pressure']

        output_names=['pressure', 'mass_CO2_aquifer1']

        output_names=['pressure', 'CO2_aquifer1', 'CO2_aquifer2']
    :type output_names: list

    :param cmpnt_to_output: Dictionary mapping component models to observations
    Examples:
        This dictionary only includes pressure from a component saved in variable sres.
        cmpnt_to_output={sres: 'pressure'}

    :param subplot: Dictionary for subplot controls, use=True will use
        subplotting (boolean default=False), ncols=n will use n columns
        of subplots (positive integer default 1); comp.var_name=sub_title
        will title the subplots of comp.var_name subtitle (string default=comp.var_name).
    :type subplot: dict

    :param plot_data: Dictionary of setup for a given plot
    :type plot_data: dict

    Returns number of subplots required, the number of lines there will be if 
    a single plot is used, and the subplot dictionary.
    """
    nplots = 0
    for obs_nm in output_names:
        for cmpnt in cmpnt_to_output:
            if obs_nm in cmpnt_to_output[cmpnt]:
                nplots += 1

    # nplots is adjusted if subplots are diabled, but it also represents the 
    # number of lines there will be in a single plot.
    number_lines_single_plot = nplots

    if subplot is None:
        use_subplots = False
    else:
        use_subplots = subplot.get('use', True)
        if 'Use' in subplot:
            use_subplots = subplot['Use']

    if nplots > MAX_SUBPLOTS and use_subplots:
        warning_msg_pt1 = ''.join([
            'For the plot {}, the default approach for that plot type is to ', 
            'show outputs in separate subplots. Given the outputs provided ', 
            'to the plot, the number of subplots was calculated as {}. '
            ]).format(name, nplots)

        enforce_subplots = False

        if 'Subplot' in plot_data:
            if 'Use' in plot_data['Subplot']:
                if plot_data['Subplot']['Use']:
                    enforce_subplots = True

        if not enforce_subplots:
            warning_msg_pt2 = ''.join([
                'When the number of subplots is greater than {}, the use of ', 
                'subplots is disabled unless "Use: True" is included under', 
                ' "Subplot" in the entry for the plot (see control file ', 
                'example 1a). The figure {} will use not use subplots.'
                ]).format(MAX_SUBPLOTS, name)

            warning_msg = warning_msg_pt1 + warning_msg_pt2

            logging.warning(warning_msg)

            subplot['use'] = False
            if 'Use' in subplot:
                del subplot['Use']

            nplots = 1

        elif enforce_subplots:
            warning_msg_pt2 = ''.join([
                'Normally, if there are more than {} subplots, the use of ', 
                'subplots is disabled. Because "Use: True" is included under', 
                ' "Subplot" in the entry for the plot, however, subplots will ', 
                'still be used.']).format(MAX_SUBPLOTS)

            warning_msg = warning_msg_pt1 + warning_msg_pt2

            logging.warning(warning_msg)

    return nplots, number_lines_single_plot, subplot


def setup_subplots_data(subplot, plot_type, num_plots):
    """
    Setup dictionary containing information about subplots:
        single subplot versus many, number of rows and columns,
        type of data (realizations and/or stats).

    Possible keys: 'use', 'nrows', 'ncols', 'single_plot', 'type' (plus data about
    titles corresponding to different cmpnt_name.obs_name subplots; these data
    might not be necessarily present). Note that this function is also set up to
    read capitalized versions of the in .yaml files (Use, and NumCols instead
    of use and ncols), as this approach matches the conventions of other plot
    types. The capitalized or non-capitaized inputs will have the same effects,
    however.
    """
    # Initialize subplots_data dictionary depending on the input arguments
    if subplot is None:
        subplots_data = {'use': False}
    else:
        subplots_data = subplot

    if 'Use' in subplots_data:
        subplots_data['use'] = subplots_data['Use']
        del subplots_data['Use']

    if 'ncol' in subplots_data:
        subplots_data['ncols'] = subplots_data['ncol']
        del subplots_data['ncol']

    if 'NumCols' in subplots_data:
        subplots_data['ncols'] = subplots_data['NumCols']
        del subplots_data['NumCols']

    if 'NumCol' in subplots_data:
        subplots_data['ncols'] = subplots_data['NumCol']
        del subplots_data['NumCol']

    if 'EnforceYLims' not in subplots_data:
        subplots_data['EnforceYLims'] = DEFAULT_ENFORCE_Y_LIMS
    elif not isinstance(subplots_data['EnforceYLims'], bool):
        subplots_data['EnforceYLims'] = DEFAULT_ENFORCE_Y_LIMS

    if not 'ShowRange' in subplots_data:
        subplots_data['ShowRange'] = DEFAULT_ADD_RANGE_STR
    elif not isinstance(subplots_data['ShowRange'], bool):
        subplots_data['ShowRange'] = DEFAULT_ADD_RANGE_STR

    # Process plot type
    if plot_type is None:
        subplots_data['type'] = ['real']
    else:
        subplots_data['type'] = plot_type

    if not subplots_data['use']:  # if subplots are not to be used
        subplots_data['single_plot'] = True
        subplots_data['ncols'] = 1
        subplots_data['nrows'] = 1
    else:
        subplots_data['single_plot'] = False
        if 'ncols' not in subplots_data:
            if num_plots <= 3:
                subplots_data['ncols'] = 1
            elif num_plots > 3:
                subplots_data['ncols'] = 2

        subplots_data['nrows'] = math.ceil(float(num_plots) / subplots_data['ncols'])

        # If the user entered 'use': True in the subplot dictionary but there is
        # still only one row and one column in this plot, set single_plot to True.
        if subplots_data['ncols'] == 1 and subplots_data['nrows'] == 1:
            subplots_data['single_plot'] = True

    subplots_data['MetricRanges'] = {}

    # When making separate figures in the GUI, the subplot keyword argument can 
    # keep information that was saved for a previous figure (e.g., 'subplot#'
    # keys for each subplot, with a 'MetricTypes' key storing the output types 
    # that were in the subplot). If so, delete that information.
    for subplotRef in range(MAX_SUBPLOT_NUM_FOR_RESET):
        if f'subplot{subplotRef + 1}' in subplots_data:
            del subplots_data[f'subplot{subplotRef + 1}']

    return subplots_data


def get_label(obs_name, labels, split_labels):
    """
    Get label for y-axis for a plot of a given observation

    :param obs_name: name of observation to be shown on a figure
    :type obs_name: str

    :param labels: dictionary containing y-labels corresponding to observation
    names
    :type labels: dict()

    :param split_labels: dictionary containing y-labels corresponding to particular
    parts of observation names
    :type split_labels: dict()

    Returns:
        string to use for y-label
    """
    out_flag = 1
    if obs_name in labels:
        str_label = labels[obs_name]
    else:
        # The observation name is split on numerical characters:
        # it can be 2 in CO2_aquifer or it can be 1 (or similar) in brine_aquifer1
        # This works on preexisting known observation names
        split_name = re.split('\d', obs_name)
        try:
            str_label = split_labels[split_name[0]]
        except KeyError:
            str_label = obs_name
            out_flag = 0

    return str_label, out_flag


def generate_legend_setup(obs_name, cmpnt_name, analysis):
    """
    Generate legend setup.

    Returns lgnd_label, label, lgnd_label_mult_metrics, label_mult_metrics, and line_style
    """
    if '_' in cmpnt_name:
        cmpnt_name_edit = cmpnt_name[0:cmpnt_name.index('_')]
    else:
        cmpnt_name_edit = cmpnt_name

    # Determine whether location is provided in the component name and get index if it is
    loc_ind = is_location_in_name(cmpnt_name)

    # Get initial version of legend label and update it later if location index
    # is in the component name
    lgnd_label = get_legend_label(obs_name)
    
    # These are used if a single plot has multiple metrics
    lgnd_label_mult_metrics = lgnd_label

    # If location index is known
    if loc_ind != -1:
        lgnd_label = '{} at location {}'.format(cmpnt_name_edit, loc_ind)

        if analysis != 'forward':
            lgnd_label = ', ' + lgnd_label

    else:
        lgnd_label = cmpnt_name_edit

    # If multiple metrics are produced from the same component, this label 
    # can help distinguish the metric types. For example, 
    # 'pressure, AnalyticalReservoir1 at location 0' vs 
    # 'CO$_2$ saturation, AnalyticalReservoir1 at location 0'.
    if analysis != 'forward':
        if lgnd_label == cmpnt_name_edit:
            lgnd_label_mult_metrics = lgnd_label_mult_metrics + ', ' + lgnd_label
        else:
            lgnd_label_mult_metrics = lgnd_label_mult_metrics + lgnd_label
    else:
        lgnd_label_mult_metrics = lgnd_label_mult_metrics + ', ' + lgnd_label

    label_mult_metrics = ''
    if analysis == 'forward':
        label = lgnd_label
    elif analysis in ['lhs', 'parstudy']:
        if lgnd_label == cmpnt_name_edit:
            lgnd_label = ', ' + cmpnt_name_edit

        label = 'Simulated values' + lgnd_label

        label_mult_metrics = 'Simulated values, ' + lgnd_label_mult_metrics

    return lgnd_label, label, lgnd_label_mult_metrics, label_mult_metrics, loc_ind


def get_legend_label(obs_name):
    """
    Generate part of the legend based on the provided observation name
    """
    if obs_name in LEGEND_DICT:
        legend_label = LEGEND_DICT[obs_name]
    else:
        # Check if 'aquifer' in the name
        place_ind = obs_name.rfind('aquifer')
        if place_ind != -1:
            # Determine index of aquifer
            try:
                aquifer_ind = int(obs_name[place_ind+7:])
            except ValueError:
                # Possibly observations of Seal Horizon or Fault Flow components
                # TODO update with location being defined by cell or segment ind
                legend_label = ''
            else:
                legend_label = 'aquifer {}'.format(aquifer_ind)
        else:
            legend_label = ''

    return legend_label


def adjust_x_label(ax, fig, fig_setup, subplots_data, savespace):
    """
    Adjust font of x-label based on figure size.
    """
    max_width_frac = MAX_XLABEL_WIDTH_FRAC
    max_height_frac = MAX_XLABEL_HEIGHT_FRAC
    if not savespace:
        max_width_frac = MAX_XLABEL_WIDTH_FRAC_V2
        max_height_frac = MAX_XLABEL_HEIGHT_FRAC_V2

    h_xlabel = ax.set_xlabel(
        'Time (years)', fontsize=fig_setup['xaxis_font_size'],
        fontweight=fig_setup['label_font_weight'],
        labelpad=fig_setup['axis_label_pad'])

    continue_test = True
    while continue_test:
        height_frac, width_frac = width_and_depth_frac(fig, h_xlabel, subplots_data)

        # If the xlabel is too long, shrink the fontsize
        if (width_frac > max_width_frac) or (height_frac > max_height_frac):
            if 0.95 * fig_setup['xaxis_font_size'] >= MIN_FONT_SIZE:
                fig_setup['xaxis_font_size'] = 0.95 * fig_setup['xaxis_font_size']
                h_xlabel = ax.set_xlabel(
                    'Time (years)', fontsize=fig_setup['xaxis_font_size'],
                    fontweight=fig_setup['label_font_weight'],
                    labelpad=fig_setup['axis_label_pad'])
            else:
                continue_test = False
        else:
            continue_test = False

    return fig_setup


def adjust_y_label(obs_name, cmpnt_name, ax, fig, fig_setup, subplots_data, 
                   subplot_ind, useMathTextOption):
    """
    Adjust font of y-label based on figure size.
    """
    subplot_metrics = subplots_data['subplot{}'.format(subplot_ind)]['MetricTypes']

    # Get y-label associated with given observation
    y_label_dict = Y_LABEL_DICT

    y_label, out_flag = get_label(obs_name, y_label_dict, Y_LABEL_SPLIT_DICT)

    if out_flag != 1:
        y_label = '{}.{}'.format(cmpnt_name, obs_name)

    # If there are multiple metrics in one subplot, using one y label will be problematic
    if subplots_data['single_plot']:
        # This is used, for example, if the figure shows multiple types of 
        # leakage rates (to aquifer 1, 2, 3, etc.). Instead of showing all of 
        # those varieties (with one being overwritten by the next), just show 
        # 'Leakage Rates (kg/s)'. Otherwise, the label could say a specific 
        # aquifer number, even if all of the results do not align with that 
        # unit number.
        mult_metrics_check = False
        if len(subplot_metrics) > 1:
            mult_metrics_check = True
        elif len(subplot_metrics) == 1:
            if subplot_metrics[0] in SIMILAR_PARTIAL_METRICS_DICT.keys():
                mult_metrics_check = True    

        if mult_metrics_check:
            y_label = ''

            for ind, metric in enumerate(subplot_metrics):
                y_label_part = ''
                if metric in Y_LABEL_DICT_MULT_METRICS:
                    y_label_part = Y_LABEL_DICT_MULT_METRICS[metric]

                if y_label == '':
                    y_label = y_label_part
                elif (ind + 1) == len(subplot_metrics) and len(subplot_metrics) == 2:
                    y_label += ' or ' + y_label_part
                elif (ind + 1) == len(subplot_metrics) and len(subplot_metrics) != 2:
                    y_label += ', or ' + y_label_part
                else:
                    y_label += ', ' + y_label_part

            if ' ' in y_label:
                y_label = split_at_space(y_label)

    ax.ticklabel_format(style='sci', axis='y',
                        scilimits=(0, 0), useMathText=useMathTextOption)

    h_ylabel = ax.set_ylabel(y_label, fontsize=fig_setup['yaxis_font_size'],
                             fontweight=fig_setup['label_font_weight'],
                             labelpad=fig_setup['axis_label_pad'])

    if out_flag == 1:
        height_frac, _ = width_and_depth_frac(fig, h_ylabel, subplots_data)
        # If the ylabel is too long relative to the figure, use labels with two rows
        if height_frac > MAX_YLABEL_HEIGHT_FRAC:
            y_label_dict = Y_LABEL_2ROWS_DICT

            y_label, _ = get_label(
                obs_name, y_label_dict, Y_LABEL_2ROWS_SPLIT_DICT)

            h_ylabel = ax.set_ylabel(y_label,
                                     fontsize=fig_setup['yaxis_font_size'],
                                     fontweight=fig_setup['label_font_weight'],
                                     labelpad=fig_setup['axis_label_pad'])

    # Check if the fontsize is too large
    continue_test = True
    while continue_test:
        height_frac, width_frac = width_and_depth_frac(fig, h_ylabel, subplots_data)

        # If the ylabels are still too long, shrink the fontsize
        if (height_frac > MAX_YLABEL_HEIGHT_FRAC) or (width_frac > MAX_YLABEL_WIDTH_FRAC):
            if 0.95 * fig_setup['yaxis_font_size'] >= MIN_FONT_SIZE:
                fig_setup['yaxis_font_size'] *= 0.95
                h_ylabel = ax.set_ylabel(y_label,
                                         fontsize=fig_setup['yaxis_font_size'],
                                         fontweight=fig_setup['label_font_weight'],
                                         labelpad=fig_setup['axis_label_pad'])
            else:
                continue_test = False
        else:
            continue_test = False

    return fig_setup


def get_title(obs_name, cmpnt_name, subplots_data, loc_ind, min_val, max_val, 
              metric_type, output_names, subplot_ind):
    """
    Generate figure title based on the provided observation name
    """
    full_obs_name = '{}.{}'.format(cmpnt_name, obs_name)
    if full_obs_name in subplots_data:
        sub_title = subplots_data[full_obs_name]

    # This checks if the name includes a number like "_000," which indicates a location
    else:
        title_dict = TITLE_DICT

        title_label, _ = get_label(
            obs_name, title_dict, TITLE_SPLIT_DICT)

        if loc_ind != -1:  # -1 means no location in component name
            # If it's a single plot, the results plotted could represent multiple locations.
            # In that scenario, you shouldn't have one location displayed in the title.
            if not subplots_data['single_plot']:
                sub_title = '{} at Location {}'.format(title_label, loc_ind)
            else:
                sub_title = title_label
        # If the location number is not in the name, just use the title dictionary
        else:
            sub_title = title_label

    subplot_metrics = subplots_data['subplot{}'.format(subplot_ind)]['MetricTypes']

    if subplots_data['single_plot']:
        min_val = ''
        max_val = ''
        if metric_type in subplots_data['MetricRanges']:
            min_val = subplots_data['MetricRanges'][metric_type][0]
            max_val = subplots_data['MetricRanges'][metric_type][1]

    # Add the range to the title
    if min_val != '' and max_val != '' and subplots_data['ShowRange']:
        units = ''
        if obs_name in UNIT_DICT:
            units = ' ' + UNIT_DICT[obs_name]

        if min_val == 0:
            min_val_str = r'$0$'
        else:
            a, b = '{:.2e}'.format(min_val).split('e')
            b = int(b)
            min_val_str = r'${}\times10^{{{}}}$'.format(a, b)

        if max_val == 0:
            max_val_str =  r'$0$'
        else:
            a, b = '{:.2e}'.format(max_val).split('e')
            b = int(b)
            max_val_str = r'${}\times10^{{{}}}$'.format(a, b)

        if min_val == max_val:
            sub_title += ', Value: {}{}'.format(min_val_str, units)
        else:
            sub_title += ', Range: {}{} to {}{}'.format(
                min_val_str, units, max_val_str, units)

    # If there's more than one metric type, using one title will be problematic
    if subplots_data['single_plot']:
        subplot_metrics = subplots_data['subplot{}'.format(subplot_ind)]['MetricTypes']

        # This is used, for example, if the figure shows multiple types of 
        # leakage rates (to aquifer 1, 2, 3, etc.). Instead of showing all of 
        # those varieties, just show 'Leakage Rates'. Otherwise, the title 
        # will include a specific unit number.
        mult_metrics_check = False
        if len(subplot_metrics) > 1:
            mult_metrics_check = True
        elif len(subplot_metrics) == 1:
            if subplot_metrics[0] in SIMILAR_PARTIAL_METRICS_DICT.keys() and len(
                    output_names) > 1:
                mult_metrics_check = True    

        if mult_metrics_check:
            sub_title = ''

            for ind, metric in enumerate(subplot_metrics):
                sub_title_part = ''
                if metric in TITLE_DICT_MULT_METRICS:
                    sub_title_part = TITLE_DICT_MULT_METRICS[metric]

                if sub_title == '':
                    sub_title = sub_title_part
                elif (ind + 1) == len(subplot_metrics):
                    sub_title += ', and ' + sub_title_part
                else:
                    sub_title += ', ' + sub_title_part

    return sub_title


def adjust_title(sub_title, ax, fig, fig_setup, subplots_data, savespace):
    """
    Adjust font of subplot title based on figure size.
    """
    max_width_frac = MAX_TITLE_WIDTH_FRAC
    max_height_frac = MAX_TITLE_HEIGHT_FRAC
    if not savespace:
        max_width_frac = MAX_TITLE_WIDTH_FRAC_V2
        max_height_frac = MAX_TITLE_HEIGHT_FRAC_V2

    h_title = ax.set_title(sub_title, fontsize=fig_setup['title_font_size'],
                           fontweight=fig_setup['label_font_weight'],
                           pad=fig_setup['title_pad'])

    height_frac, width_frac = width_and_depth_frac(fig, h_title, subplots_data)
    # If the title is too long, it can overlap the axis labels (e.g., 'x 10^6')
    # If there is a space in the title, find it and make a line break
    if (height_frac > max_height_frac) or (width_frac > max_width_frac):
        if ' ' in sub_title:
            sub_title = split_at_space(sub_title)
            h_title = ax.set_title(sub_title, fontsize=fig_setup['title_font_size'],
                                   fontweight=fig_setup['label_font_weight'],
                                   pad=fig_setup['title_pad'])
        else:  # If there's no space in the title shrink the fontsize
            fig_setup['title_font_size'] *= 0.95
            h_title = ax.set_title(sub_title,
                                   fontsize=fig_setup['title_font_size'],
                                   fontweight=fig_setup['label_font_weight'],
                                   pad=fig_setup['title_pad'])

        continue_test = True
        while continue_test:
            height_frac, width_frac = width_and_depth_frac(fig, h_title, subplots_data)

            # If the title is still too long, shrink the fontsize
            if (height_frac > max_height_frac) or (width_frac > max_width_frac):
                if fig_setup['title_font_size'] * 0.95 >= MIN_FONT_SIZE:
                    fig_setup['title_font_size'] *= 0.95
                    h_title = ax.set_title(sub_title,
                                           fontsize=fig_setup['title_font_size'],
                                           fontweight=fig_setup['label_font_weight'],
                                           pad=fig_setup['title_pad'])
                else:
                    continue_test = False
            else:
                continue_test = False

    return fig_setup


def is_location_in_name(cmpnt_name):
    """
    Determine whether name of component contains location: applicable only for
    control file interface created components with names in the format name_###

    Returns location index extracted from name if found and -1 if there is
    no location index in the component name.
    """
    # Determine index of last underscore in the name: if -1 is returned underscore
    # symbol is not present
    underscore_ind = cmpnt_name.rfind('_')

    # Default value of output that can be changed to location specified
    # the component name
    loc_ind = -1

    if underscore_ind != -1: # i.e., underscore is found
        # Check that all symbols after underscore are numeric
        if np.char.isnumeric(cmpnt_name[underscore_ind+1:]):
            # Transform what is after underscore symbol into location index
            loc_ind = int(cmpnt_name[underscore_ind+1:])

    return loc_ind


def split_at_space(label):
    """
    Split string at '_' (space symbol) trying for the two parts
    to be approximately the same.
    """
    # Find a space (' ') near the middle of the title
    center_of_label_index = int(np.ceil(len(label) / 2))

    if label[center_of_label_index] == ' ':
        final_label = '{}\n{}'.format(label[0:center_of_label_index],
                                      label[center_of_label_index + 1:])
    else:
        lower_index = center_of_label_index - 1
        upper_index = center_of_label_index + 1
        continue_test = True
        while continue_test:
            if label[lower_index] == ' ':
                final_label = '{}\n{}'.format(label[0:lower_index],
                                                   label[lower_index + 1:])
                continue_test = False
            elif label[upper_index] == ' ':
                final_label = '{}\n{}'.format(label[0:upper_index],
                                        label[upper_index + 1:])
                continue_test = False
            else:
                lower_index -= 1
                upper_index += 1

    return final_label


def width_and_depth_frac(fig, element, subplots_data):
    """
    Calculate width and height fractions for a given figure.
    """
    fig_renderer = fig.canvas.get_renderer()
    bb = element.get_window_extent(renderer=fig_renderer)
    ywidth = bb.width
    yheight = bb.height

    height_frac = yheight / (fig_renderer.height / subplots_data['nrows'])
    width_frac = ywidth / (fig_renderer.width / subplots_data['ncols'])

    return height_frac, width_frac


def process_components(obs_name, cmpnt_to_output):
    """
    Return list of components from the dictionary returning given observation.
    """
    comp_list = []
    for cmpnt in cmpnt_to_output:
        if obs_name in cmpnt_to_output[cmpnt]:
            comp_list.append(cmpnt)

    return comp_list


def make_label_fontsizes_uniform(num_subplots, xaxis_fontsizes_used,
                                 yaxis_fontsizes_used, subplots_data):
    """
    Function that ensures all x and y axis labels have the same fontsizes
    """
    min_fontsize = np.min(xaxis_fontsizes_used)

    if np.min(yaxis_fontsizes_used) < min_fontsize:
        min_fontsize = np.min(yaxis_fontsizes_used)

    if num_subplots == 0:
        ax = plt.gca()
        ax.xaxis.label.set_fontsize(min_fontsize)
        ax.yaxis.label.set_fontsize(min_fontsize)

    else:
        for subplotRef in range(0, num_subplots):
            ax = plt.subplot(
                subplots_data['nrows'], subplots_data['ncols'], subplotRef + 1)

            ax = plt.gca()
            ax.xaxis.label.set_fontsize(min_fontsize)
            ax.yaxis.label.set_fontsize(min_fontsize)

    return min_fontsize


def get_colors(reals_ind, color_ind, used_colors, subplots_data):
    """
    Function that checks the colors used previously and provides a color that
    has not been used yet.
    """
    hatch_check = False

    if 'real' in subplots_data['type']:
        colorRefReal = 'C' + str((reals_ind) % 10)
        colorValReal = colorRefReal[:]

        rgbReal = clrs.to_rgba('C' + str((reals_ind) % 10))
        darkColorRefReal = 'DC' + str((reals_ind) % 10)
        lightColorRefReal = 'LC' + str((reals_ind) % 10)

        dark_clr = np.array(list(rgbReal[:]))
        dark_clr *= (2 / 3)
        dark_clr[-1] = 1

        light_clr = np.array(list(rgbReal[:]))
        light_clr[0] = (light_clr[0] + 1) * 0.5
        light_clr[1] = (light_clr[1] + 1) * 0.5
        light_clr[2] = (light_clr[2] + 1) * 0.5
        light_clr[-1] = 1

        if not colorRefReal in used_colors:
            used_colors.append(colorRefReal)

        else:
            if not darkColorRefReal in used_colors:
                colorValReal = dark_clr

                used_colors.append(darkColorRefReal)

            elif not lightColorRefReal in used_colors:
                colorValReal = light_clr

                used_colors.append(lightColorRefReal)

            else:
                remainder_num_colors = reals_ind % 30

                if 10 <= remainder_num_colors <= 19:
                    colorValReal = dark_clr
                elif 20 <= remainder_num_colors <= 29:
                    colorValReal = light_clr

    else:
        colorValReal = None

    if 'stats' in subplots_data['type']:
        colorRefStats = 'C' + str((color_ind) % 10)
        colorValStats = colorRefStats[:]

        rgbStats = clrs.to_rgba('C' + str((color_ind) % 10))
        darkColorRefStats = 'DC' + str((color_ind) % 10)
        lightColorRefStats = 'LC' + str((color_ind) % 10)

        if not colorRefStats in used_colors:
            used_colors.append(colorRefStats)

        else:
            color_ind += 1

            colorRefStats = 'C' + str((color_ind) % 10)
            colorValStats = colorRefStats[:]

            rgbStats = clrs.to_rgba('C' + str((color_ind) % 10))
            darkColorRefStats = 'DC' + str((color_ind) % 10)
            lightColorRefStats = 'LC' + str((color_ind) % 10)

            if not colorRefStats in used_colors:
                used_colors.append(colorRefStats)

            else:
                if not darkColorRefStats in used_colors:
                    dark_clr = np.array(list(rgbStats[:]))
                    dark_clr /= 2
                    dark_clr[-1] = 1
                    colorValStats = dark_clr

                    used_colors.append(darkColorRefStats)

                elif not lightColorRefStats in used_colors:
                    light_clr = np.array(list(rgbReal[:]))
                    light_clr[0] = (light_clr[0] + 1) / 2
                    light_clr[1] = (light_clr[1] + 1) / 2
                    light_clr[2] = (light_clr[2] + 1) / 2
                    light_clr[-1] = 1
                    colorValStats = light_clr

                    used_colors.append(lightColorRefStats)

                else:
                    # If there are no more colors to use, the hatches can help
                    # distinguish the filled areas.
                    hatch_check = True
    else:
        colorValStats = None

    return colorValReal, colorValStats, used_colors, color_ind, hatch_check


def check_legend(handle_list, fig_setup, min_fontsize, subplots_data):
    """
    This function checks the number of items in the legend (handle_list) and
    adjusts the legend fontsize and columns if there are too many items. The
    min_fontsize is the minimum fontsize used for x and y axis labels, which is
    adjusted based on the number of subplots. If the legend fontsize is larger
    than min_fontsize, it is set to min_fontsize.
    """
    if min_fontsize:
        if fig_setup['legend_font_size'] > min_fontsize:
            fig_setup['legend_font_size'] = min_fontsize

    scaling_factors = [0.9, 0.75, 0.67, 0.5, 0.33]

    if subplots_data['single_plot']:
        # Not using LEGEND_ITEM_THRESH1 or LEGEND_ITEM_THRESH2 for single plots
        if LEGEND_ITEM_THRESH2 <= len(handle_list) < LEGEND_ITEM_THRESH3:
            fig_setup['legend_font_size'] *= scaling_factors[0]
            fig_setup['legend_framealpha'] *= scaling_factors[0]

        elif LEGEND_ITEM_THRESH3 <= len(handle_list) < LEGEND_ITEM_THRESH4:
            fig_setup['legend_font_size'] *= scaling_factors[1]
            fig_setup['legend_framealpha'] *= scaling_factors[1]
            fig_setup['legend_columns'] = 2

        elif len(handle_list) >= LEGEND_ITEM_THRESH4:
            fig_setup['legend_font_size'] *= scaling_factors[2]
            fig_setup['legend_framealpha'] *= scaling_factors[2]
            fig_setup['legend_columns'] = 2

    else:
        if subplots_data['ncols'] == 2:
            fig_setup['legend_font_size'] *= scaling_factors[0]
        elif subplots_data['ncols'] == 3:
            fig_setup['legend_font_size'] *= scaling_factors[1]
        elif subplots_data['ncols'] >= 4:
            fig_setup['legend_font_size'] *= scaling_factors[2]

        if LEGEND_ITEM_THRESH1 <= len(handle_list) < LEGEND_ITEM_THRESH2:
            fig_setup['legend_font_size'] *= scaling_factors[1]

        elif LEGEND_ITEM_THRESH2 <= len(handle_list) < LEGEND_ITEM_THRESH3:
            fig_setup['legend_font_size'] *= scaling_factors[2]
            fig_setup['legend_framealpha'] *= scaling_factors[1]

        elif LEGEND_ITEM_THRESH3 <= len(handle_list) < LEGEND_ITEM_THRESH4:
            fig_setup['legend_font_size'] *= scaling_factors[3]
            fig_setup['legend_framealpha'] *= scaling_factors[3]
            fig_setup['legend_columns'] = 2

        elif len(handle_list) >= LEGEND_ITEM_THRESH4:
            fig_setup['legend_font_size'] *= scaling_factors[4]
            fig_setup['legend_framealpha'] *= scaling_factors[3]
            fig_setup['legend_columns'] = 2

    return fig_setup


def get_plot_yaml_input(plot_data, name):
    """
    This function checks the plot's section within the .yaml file for any input
    related to markerstyles and linestyles.
    """
    # Each value under a particular key is a list: default value, type, type written as string
    default_values = {'UseMarkers': [False, bool, 'boolean'],
                      'UseLines': [True, bool, 'boolean'],
                      'VaryLineStyles': [False, bool, 'boolean'],
                      'FigureDPI': [100, (int, float), 'integer or float'], 
                      'ShowLegend': [True, bool, 'boolean']}

    out_values = {key: val[0] for key, val in default_values.items()}

    warning_msg = ''.join([
        'In the .yaml file, the input provided for {} under the ', name, ' plot ',
        'was not of type {}. The default value of {} will be used.'])

    for key, val in default_values.items():
        if key in plot_data:
            out_values[key] = plot_data[key]
            if not isinstance(out_values[key], val[1]):
                msg = warning_msg.format(key, val[2], val[0])
                logging.warning(msg)
                out_values[key] = val[0]

    return out_values['UseMarkers'], out_values['UseLines'],\
        out_values['VaryLineStyles'], out_values['FigureDPI'], out_values['ShowLegend']


def get_metric_type_key_for_axis_lims(obs_to_plot):
    """
    If y axis limits are enforced, the metric_type key returned by this function 
    is used to store the axis limits. Instead of having the key exactly match 
    each output metric name, the key can be lumped into more general categories 
    defined in SIMILAR_PARTIAL_METRICS_DICT. For example, leakage rates of 
    brine and CO2 in kg/s are conceptually similar, so using the same axis limits 
    for those subplots can help the user compare the data more quickly and easily.
    """
    metric_type = obs_to_plot
    metric_type_found = False
    for key in SIMILAR_PARTIAL_METRICS_DICT.keys():
        if not metric_type_found:
            obs_for_key = SIMILAR_PARTIAL_METRICS_DICT[key]

            for ob_for_key in obs_for_key:
                # Need an exact match for these to prevent the wrong one from 
                # being selected.
                if key in ['leakage_rates', 'leaked_masses']:
                    if ob_for_key == metric_type:
                        metric_type = key
                        metric_type_found = True
                        continue
                else:
                    if ob_for_key in metric_type:
                        metric_type = key
                        metric_type_found = True
                        continue

    return metric_type


def enforce_y_lims(num_subplots, subplots_data):
    """
    This function is used to set the y axis limits for each subplot, if that 
    approach is enabled (controlled by the 'EnforceYLims' key in subplots_data).
    """
    for subplot_ind in range(num_subplots):
        ax = plt.subplot(
            subplots_data['nrows'], subplots_data['ncols'], subplot_ind + 1)

        subplot_metric_types = subplots_data[
            'subplot{}'.format(subplot_ind + 1)]['MetricTypes']

        # If one metric type is used in the subplot, set the axis limits to 
        # match the range for that metric across other subplots.
        if len(subplot_metric_types) == 1:
            ylim_min = subplots_data['MetricRanges'][subplot_metric_types[0]][0]
            ylim_max = subplots_data['MetricRanges'][subplot_metric_types[0]][1]

            not_updated_check = (ylim_min == PLACEHOLDER_MIN_VAL or ylim_max == PLACEHOLDER_MAX_VAL)
            if not not_updated_check:
                # Used to provide some space at the top and bottom of the figure. 
                # Otherwise a line can be hard to see if it runs along the top 
                # or bottom.
                ylim_adjust = 0

                if ylim_min == ylim_max:
                    ylim_min -= 0.25
                    ylim_max += 1
                else:
                    ylim_adjust = (ylim_max - ylim_min) / 100

                ax.set_ylim([ylim_min - ylim_adjust, ylim_max + ylim_adjust])


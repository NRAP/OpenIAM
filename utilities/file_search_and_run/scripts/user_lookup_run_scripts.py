# -*- coding: utf-8 -*-
"""
This script scans through all Python files in the IAM_DIR/examples/scripts directory, 
where IAM_DIR is the main directory of the NRAP-Open-IAM installation. It then 
finds all .py control files that use the component type(s) specified by the 
user through text entry. If the user specifies to run the files (also through 
text entry), it will run the files meeting the search criteria. The user can 
specify whether to run only files that use all of the components specified or 
files that use any of the components specified (not recommended).
----------------------------------------------------------------------------
Instructions:
- Make sure the script files to process are in the directory IAM_DIR/examples/scripts, 
  where IAM_DIR is the main directory of the NRAP-Open-IAM installation.
- Run the script by executing "python user_lookup_run_scripts.py" from your 
  command line or IDE.
- Follow the prompt to enter the component type(s) to search for. Type 'q' to 
  exit.
- The user can specify whether or not to run files. If running files, the user 
  can specify whether to run only files that use all of the component types or 
  files that use any of the component types (not recommended).
- The script will generate several .csv and .txt files in a subdirectory named 
  'user_script_search_results', containing detailed mappings and usage logs of 
  components found.
- To view errors that occurred during the file processing, check the .log file 
  in the same subdirectory.
"""
import os
import re
import csv
import logging
from datetime import datetime
import subprocess

try:
    from openiam.components.iam_base_classes import IAM_DIR
except:
    err_msg = ('Could not import from NRAP-Open-IAM classes, ensure the ' 
               + 'NRAP-Open-IAM environment has been set up and activated. ' 
               + 'See the NRAP-Open-IAMuser guide.')
    
    raise ImportError(err_msg)


#----- Inputs ----------------------------------------------------------------#
# .txt file listing all components, the component modules, and the files using 
# each component
component_mapping_file_name = 'component_mapping.txt' 

# .csv file listing all components, the component modules, and the files using 
# each component
component_csv_file_name = 'component_mapping.csv'

# .csv file specifying each component type in each .py file
matrix_csv_file_name = 'file_component_matrix.csv'

# .csv specifying the .py files with each component type as well as those with 
# all of the components specified
script_search_results_file_name = 'script_search_results.csv' 


#----- Processing ------------------------------------------------------------#
scripts_dir = os.path.join(IAM_DIR, 'examples', 'scripts')

file_search_dir = os.path.join(IAM_DIR, 'utilities', 'file_search_and_run', 'scripts')

logging_folder = os.path.join(file_search_dir, 'logging_files')

if not os.path.exists(logging_folder):
    os.mkdir(logging_folder)

# Define the output folder for the log file path. If changed, update the 
# .gitignore file.
start_time = datetime.now()
now = start_time.strftime('%Y-%m-%d_%H.%M.%S')

# Define the output folder. If changed, update the .gitignore file.
output_dir = os.path.abspath(os.path.join(
    logging_folder, 'user_lookup_run_scripts_{}'.format(now)))

# Ensure the directory exists
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

entered_comp_file_path = os.path.join(output_dir, script_search_results_file_name)

# .log file containing messages
log_file_name = 'results_log.log'

log_file_path = os.path.join(output_dir, log_file_name)

log_level = 'DEBUG'

logger = logging.getLogger('')
# Remove default existing handlers
logger.handlers.clear()
logger.setLevel(log_level)
# logging formatter for log files with more details
log_formatter1 = logging.Formatter(
    fmt='%(levelname)s %(module)s.%(funcName)s: %(message)s',
    datefmt='%m-%d %H:%M')
# logging formatter for console output
log_formatter2 = logging.Formatter(
    fmt='%(levelname)-8s %(message)s',
    datefmt='%m-%d %H:%M')

# Setup logging to log file
file_handler = logging.FileHandler(filename=log_file_path, mode='w')
file_handler.setLevel(log_level)
file_handler.setFormatter(log_formatter1)
logger.addHandler(file_handler)

# Setup logging to console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(log_formatter2)
logger.addHandler(console)

# Regex pattern to match lines with 'from ... import ...' where 'from ...' includes "_component"
pattern = r'^from\s+([\w\.]+_component[\w\.]*)\s+import\s+(\w+)'

# Dictionary to store the results
component_mapping = {}


def check_file(component_mapping, file_path, local_components, pattern):
    """
    Function used in process_file.
    """
    with open(file_path, 'r', encoding='utf-8') as file:
        for line in file:
            # Use regex to find matches
            match = re.match(pattern, line.strip())
            
            if match:
                module, component = match.groups()
                
                if component not in local_components:
                    if component in component_mapping:
                        component_mapping[component][1].append(file_path)
                    else:
                        component_mapping[component] = (module, [file_path])
                    
                    local_components.add(component)
    
    return component_mapping


def process_file(component_mapping, file_path):
    """
    Function to process each Python file.
    """
    local_components = set()  # To avoid duplicates in the same file
    
    try:
        component_mapping = check_file(component_mapping, file_path, 
                                       local_components, pattern)
    except Exception as e:
        logging.error(f"\nError processing file {file_path}: \n{e}\n")
    
    return component_mapping


def sort_selected_files(selected_files): 
    """
    Sorts a list of files.
    """
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)] 
    
    return sorted(selected_files, key=alphanum_key)


# Move to the script examples directory
os.chdir(scripts_dir)

# List all files in the script examples directory
for filename in os.listdir('.'):
    if filename.endswith('.py'):
        component_mapping = process_file(component_mapping, filename)

# Move to the output directory
os.chdir(output_dir)


# Write the dictionary to a text file
with open(os.path.join(output_dir, component_mapping_file_name), 
          'w', encoding='utf-8') as output_file:
    for component, module in component_mapping.items():
        output_file.write(f'{component}: {module}\n')

# Write the dictionary to a CSV file (component, module, files as rows)
with open(os.path.join(output_dir, component_csv_file_name), 
          'w', newline='', encoding='utf-8') as csvfile:
    csv_writer = csv.writer(csvfile)
    csv_writer.writerow(['Component', 'Module', 'Files'])
    
    for component, (module, files) in component_mapping.items():
        csv_writer.writerow([component, module, ', '.join(files)])

# Write the dictionary to a .csv file (files, components as columns)
file_component_map = {}
for component, (_, files) in component_mapping.items():
    for file in files:
        if file not in file_component_map:
            file_component_map[file] = set()
        
        file_component_map[file].add(component)

all_components = sorted(component_mapping.keys())

# Write a .csv file specifying which components are in each .py file in the 
# scripts folder
with open(os.path.join(output_dir, matrix_csv_file_name), 'w', newline='', encoding='utf-8') as csvfile:
    csv_writer = csv.writer(csvfile)
    csv_writer.writerow(['File'] + all_components)
    
    for file, components in file_component_map.items():
        row = [file] + ['X' if component in components else '' for component in all_components]
        csv_writer.writerow(row)


# Function to look up a desired component and list files where it is used
def files_using_desired_component(component_name):
    output_lines = []
    
    if component_name in component_mapping:
        module, files = component_mapping[component_name]
        output_lines.append(f"Desired_Component: {component_name}, Used in files: \n{', '.join(files)}\n")
    else:
        output_lines.append(f"Component '{component_name}' not found.\n")
        files = []

    # Join the lines for both printing and writing to file
    output_text = "\n".join(output_lines)
    logging.info(output_text)

    # Save the output to a file with listing the files using the desired component
    with open(os.path.join(output_dir, "component_usage_log.txt"), 'a', encoding='utf-8') as file:
        file.write(f"The list of the files using '{component_name}' component:\n{output_text}\n")
    
    return files


def get_run_options():
    msg = '\nThis code asks the user to specify component type(s). It then specifies which script files use that component, and runs the files if that option is enabled.\n'
    logging.info(msg)
    
    msg = "\nEnter 'q' to exit at any point.\n"
    logging.info(msg)
    
    # Get criteria from user input
    msg = "\nFirst, enter 'True' to run files. Enter 'False' to prevent files from running (only identifying files by the components used). Enter 'q' to quit: \n"
    run_files = input(msg).strip()
    if run_files.lower() == 'q':
        return
    
    logging.info(msg + run_files + '\n')
    
    if run_files in ['False', 'false', '0']:
        run_files = False
    elif run_files in ['True', 'true', '1']:
        run_files = True
    else:
        err_msg = f'\nThe run_files variable can only be set to "True" or "False", but it was given as {run_files}. Using default setting of "False".\n'
        logging.error(err_msg)
        
        run_files = False
    
    run_selection = 'All'
    if run_files:
        msg = "\nEnter 'All' to only run files using all of the components specified. Enter 'Any' to run files using any of the components specified. Enter 'q' to quit: \n"
        run_selection = input(msg).strip()
        if run_selection.lower() == 'q':
            return
        
        logging.info(msg + run_selection + '\n')
        
        if run_selection in ['all', 'ALL']:
            run_selection = 'All'
        
        if run_selection in ['any', 'ANY']:
            run_selection = 'Any'
        
        if run_selection not in ['Any', 'All']:
            err_msg = f'\nThe run_selection variable can only be set to "Any" or "All", but it was given as {run_selection}. Using default setting of "All".'
            logging.error(err_msg)

            run_selection = 'All'
    
    return run_files, run_selection

# Prepare to accumulate user-entered components and their files
entered_components = []
entered_files = {}


# User interface for component lookup
run_files, run_selection = get_run_options()

while True:
    msg = "\nEnter a component name to look up (or type 'q' to stop): \n"
    user_input = input(msg)
    
    logging.info(msg + user_input + '\n')
    if user_input.lower() == 'q':
        break
    
    files = files_using_desired_component(user_input)
    if files:
        entered_components.append(user_input)
        for file in files:
            if file not in entered_files:
                entered_files[file] = set()
            
            entered_files[file].add(user_input)

# Create the data structure for CSV output
csv_data = {component: [] for component in entered_components}
max_files = max(len(files) for files in entered_files.values())

all_file_lists = []
# Populate the data structure
for component in entered_components:
    file_list = [file for file, components in entered_files.items() 
                 if component in components]
    
    csv_data[component] = file_list
    all_file_lists.append(file_list)

# Get a list of the files that use all specified components
files_with_all_comps = all_file_lists[0]
for ind in range(1, len(all_file_lists)):
    files_with_all_comps = list(
        set(files_with_all_comps).intersection(all_file_lists[ind]))

label_for_all_files = 'Files with All Specified Components'
csv_data[label_for_all_files] = files_with_all_comps

all_files_clipped = []
for file in files_with_all_comps:
    if file != '':
        all_files_clipped.append(file)

output_text_all_files = f'\nFiles using all specified components: \n {all_files_clipped}\n'
logging.info(output_text_all_files)

column_labels = entered_components.copy()
column_labels.append(label_for_all_files)


# Get the maximum number of files for any component
max_length = max(len(files) for files in csv_data.values())

# Pad the lists to ensure all columns are the same length
for column in column_labels:
    while len(csv_data[column]) < max_length:
        csv_data[column].append('')

# Write the accumulated components and their files to a CSV file
with open(os.path.join(output_dir, script_search_results_file_name), 
          'w', newline='', encoding='utf-8') as csvfile:
    csv_writer = csv.writer(csvfile)
    csv_writer.writerow(column_labels)
    
    for i in range(max_length):
        row = [csv_data[column_label][i] for column_label in column_labels]
        csv_writer.writerow(row)

msg = f"\nThe files using the entered components have been saved to '{script_search_results_file_name}' in the '{output_dir}' directory.\n"
logging.info(msg)


def run_script_example(file_path):
    """
    This function runs an NRAP-Open-IAM script example.
    
    :param file_path: The path to the file to be processed.
    """
    command = ['python', file_path]
    
    msg = f"\nRunning {os.path.basename(file_path)}.\n" 
    logging.info(msg)
    
    try:
        result = subprocess.run(command, check=True, stdout=subprocess.PIPE, 
                                stderr=subprocess.PIPE, text=True)
        msg = f"\nProcessed {os.path.basename(file_path)} successfully. Output: \n{result.stdout}\n"
        logging.info(msg)
        
    except subprocess.CalledProcessError as e:
        # Log any errors encountered during the subprocess execution
        err_msg = f"\nFailed to process {os.path.basename(file_path)}. Error: \n{e.stderr}\n"
        logging.error(err_msg)


# Function to run the files listed in the entered_components_usage.csv file
def run_files_from_csv(csv_filename, run_selection, label_for_all_files, scripts_dir):
    files_already_processed = []
    
    with open(csv_filename, 'r', encoding='utf-8') as csvfile:
        csv_reader = csv.reader(csvfile)
        
        headers = next(csv_reader)  # Skip the header row
        
        for row in csv_reader:
            for ind, file in enumerate(row):
                if file: # Only run non-empty entries
                    if run_selection == 'Any':
                        proceed_check = headers[ind] != label_for_all_files
                    elif run_selection == 'All':
                        proceed_check = headers[ind] == label_for_all_files
                    
                    # If run_selection is Any, do not run a file repeatedly
                    if file in files_already_processed:
                        proceed_check = False
                    
                    if proceed_check:
                        file_path = os.path.join(scripts_dir, file)
                        
                        run_script_example(file_path)
                        files_already_processed.append(file)


if run_files:
    # Run the files from the entered_components_usage.csv file
    run_files_from_csv(entered_comp_file_path, run_selection, label_for_all_files, 
                       scripts_dir)
    
    msg = '\nAll matched files have been processed.\n'
    logging.info(msg)

msg = f'\nA .log file has been saved to {output_dir}.\n'
logging.info(msg)

# Shutdown logging so the logging file can be deleted afterwards, if desired
logging.shutdown()

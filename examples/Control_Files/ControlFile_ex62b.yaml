# OpenIAM Control File Example 62b
#
# This example demonstrates the use of the SALSA component, in a simulation with five active 
# wells. One active well is an extraction well, while the other four are injection wells.
# Additionally, there are two leaking wells. When entering parameters or outputs for the 
# SALSA component, the user can use the word 'All' instead of a specific well, unit, or 
# injection period index. Then, the parameter value will apply to all wells, units of that type, 
# or injection periods. Any parmeter names that include the word 'All' will be overwritten by 
# parameters that do not include the word 'All'. For example, the user can set the hydraulic 
# conductivity of all leaking wells across all aquifers with the parameter name 
# 'leakingWellAllLogKAquiferAll', but then set a different value for the hydraulic conductivity 
# for leaking well 3 across aquifer 1 with 'leakingWell3LogKAquifer1' (the more specific input 
# will be used). The user needs to be careful when using 'All' in situations that could lead 
# to overlap, however. For example, the parameter name 'activeWellAllQAquifer1Period1' applies 
# to all active wells, aquifer 1, and period 1, while the parameter name 'activeWell1QAquiferAllPeriod1' 
# applies to active well 1, all aquifers, and period 1. There is overlap in the applicability of 
# these parameters, and one value would overwrite the other in certain cases. If a parameter name 
# is used without the word 'All', then it will always overwrite any parameters that include the 
# word 'All'.
#
# The word 'All' can also be used set set the output types. For example, including 
# 'headProfileAllVertPointAllShaleAll' in the outputs will automatically add all of the 
# observations of the form 'headProfile#VertPoint#Shale#'.
#
# This example also shows all of the entries (except for 'Realization') that can be given for the 
# SalsaProfile, SalsaTimeSeries, and SalsaContourPlot plot types. The 'Realization' entry only 
# applies in a stochastic simulation (lhs or parstudy, not the forward analysis type).
#
# To run this file, use the command (\ for Windows / for Mac or Linux):
#  python ../../src/openiam/components/openiam_cf.py --file ControlFile_ex62b.yaml
#
# Last Modified: June, 2024
#-------------------------------------------------
ModelParams:
    # Time is in years
    EndTime: 150
    TimeStep: 10.0
    Analysis: forward
    Components: [SALSA]
    OutputDirectory: output/output_ex62b_{datetime}
    Logging: Warning
#-------------------------------------------------
Stratigraphy:
    # The SALSA component does not connect with a stratigraphy component, so 
    # any information entered here does not impact the results from SALSA.
    numberOfShaleLayers:
        vary: False
        value: 3
    shale1Thickness:
        value: 250.0
        vary: False
    shale2Thickness:
        value: 250.0
        vary: False
    shale3Thickness:
        value: 250.0
        vary: False
    aquifer1Thickness:
        value: 100.0
        vary: False
    aquifer2Thickness:
        value: 100.0
        vary: False
    reservoirThickness:
        value: 50.0
        vary: False
#-------------------------------------------------
SALSA:
    Type: SALSA
    InjectionWell:
        #---- keyword arguments related to active wells ----#
        periodEndTimes: [75, 150]
        # The first well location is an extraction well, the other four are injection wells
        activeWellCoordx: [0.0, 3535.53, -3535.53, -3535.53, 3535.53]
        activeWellCoordy: [0.0, 3535.53, 3535.53, -3535.53, -3535.53]
    LeakingWell:
        #---- keyword arguments related to leaking wells ----#
        leakingWellCoordx: [2828.43, 7071.06]
        leakingWellCoordy: [2828.43, 7071.06]
    OutputLocations:
        #---- keyword arguments related to hydraulic head and pressure outputs ----#
        aquiferCoordx: [2828.43, 7071.06]
        aquiferCoordy: [2828.43, 7071.06]
        shaleCoordx: [2828.43, 7071.06]
        shaleCoordy: [2828.43, 7071.06]
    ContourPlot:
        aquiferNamesContourPlots: [1, 2, 3]
    Parameters:
        #---- Stratigraphy parameters ----#
        numberOfShaleLayers: 4
        bottomLayerType: 0
        topLayerType: 0
        shale1Thickness: 56.0
        shale2Thickness: 157.0
        shale3Thickness: 430.0
        shale4Thickness: 52.0
        shale1LogVertK: -10.5
        shale2LogVertK: -10.0
        shale3LogVertK: -11.0
        shale4LogVertK: -12.0
        shale1LogSS: -5.5
        shale2LogSS: -5.0
        shale3LogSS: -6.0
        shale4LogSS: -7.0
        shaleAllInitialGamma: 0
        shaleAllGamma: 0
        aquifer1Thickness: 145
        aquifer2Thickness: 27
        aquifer3Thickness: 62
        aquifer1LogHorizK: -4.5
        aquifer2LogHorizK: -4.0
        aquifer3LogHorizK: -5.0
        aquiferAllANSR: 1
        aquifer1LogSS: -5.5
        aquifer2LogSS: -5.0
        aquifer3LogSS: -6.0
        aquifer1FluidDensity: 1100
        aquifer2FluidDensity: 1010
        aquifer3FluidDensity: 1000
        aquifer1Head: 10
        aquifer2Head: 25
        aquifer3Head: 5
        aquiferAllGamma: 0.0
        bottomBoundaryCond: 1
        bottomBoundaryHead: 0.0
        topBoundaryCond: 0
        topBoundaryHead: 0.0
        #---- Active well parameters ----#
        # Injection period 1
        activeWell1QAquifer1Period1: -0.1
        activeWell2QAquifer1Period1: 0.1
        activeWell3QAquifer1Period1: 0.1
        activeWell4QAquifer1Period1: 0.1
        activeWell5QAquifer1Period1: 0.1
        activeWell1QAquifer2Period1: 0.0
        activeWell2QAquifer2Period1: 0.0
        activeWell3QAquifer2Period1: 0.0
        activeWell4QAquifer2Period1: 0.0
        activeWell5QAquifer2Period1: 0.0
        activeWell1QAquifer3Period1: 0.0
        activeWell2QAquifer3Period1: 0.0
        activeWell3QAquifer3Period1: 0.0
        activeWell4QAquifer3Period1: 0.0
        activeWell5QAquifer3Period1: 0.0
        # Injection period 2
        activeWell1QAquifer1Period2: 0.0
        activeWell2QAquifer1Period2: 0.0
        activeWell3QAquifer1Period2: 0.0
        activeWell4QAquifer1Period2: 0.0
        activeWell5QAquifer1Period2: 0.0
        activeWell1QAquifer2Period2: 0.0
        activeWell2QAquifer2Period2: 0.0
        activeWell3QAquifer2Period2: 0.0
        activeWell4QAquifer2Period2: 0.0
        activeWell5QAquifer2Period2: 0.0
        activeWell1QAquifer3Period2: 0.0
        activeWell2QAquifer3Period2: 0.0
        activeWell3QAquifer3Period2: 0.0
        activeWell4QAquifer3Period2: 0.0
        activeWell5QAquifer3Period2: 0.0
        activeWellAllRadiusAquiferAll: 0.1
        #---- Leaking well parameters ----#
        leakingWellAllLogKAquifer1: -4.5
        leakingWellAllLogKAquifer2: -4.0
        leakingWellAllLogKAquifer3: -5.0
        leakingWellAllRadiusAquiferAll: 0.15
        leakingWellAllStatAquiferAll: 1
        leakingWellAllLogKShale1: -4.5
        leakingWellAllLogKShale2: -4.0
        leakingWellAllLogKShale3: -5.0
        leakingWellAllLogKShale4: -6.0
        leakingWellAllRadiusShaleAll: 0.15
        leakingWellAllStatShale1: 1
        leakingWellAllStatShale2: 1
        leakingWellAllStatShale3: 0
        # no leakingWell1StatShale4 - that parameter only applies when a shale 
        # has aquifers above and below it, but shale 4 does not here. If you entered 
        # that parameter here, a warning statement would print to say that the input 
        # would not be used.
        #---- Miscellaneous parameters ----#
        numberOfLaplaceTerms: 8
        xCent: 0.0
        yCent: 0.0
        xMax: 10000.0
        yMax: 10000.0
        gridExp: 1.0
        numberOfNodesXDir: 50
        numberOfNodesYDir: 50
        numberOfRadialGrids: 6
        radialZoneRadius: 100.0
        radialGridExp: 1.2
        numberOfVerticalPoints: 10
    Outputs: [headLocAllAquiferAll, pressureLocAllAquiferAll, pressureLocAllMidAquiferAll, pressureLocAllTopAquiferAll, 
              headProfileAllVertPointAllShaleAll, pressureProfileAllVertPointAllShaleAll, 
              profileDepths, allHeadProfilesShales, allPressureProfilesShales, 
              diffuseLeakageRateBottomAquiferAll, diffuseLeakageVolumeBottomAquiferAll, 
              diffuseLeakageRateTopAquiferAll, diffuseLeakageVolumeTopAquiferAll, 
              wellLeakageRateAquiferAll, wellLeakageVolumeAquiferAll, 
              wellAllLeakageRateAquiferAll, wellAllLeakageVolumeAquiferAll, 
              contourPlotAquiferHead, contourPlotAquiferPressure, contourPlotCoordx, contourPlotCoordy]
#-------------------------------------------------
Plots:
    Diffuse_Leakage_Rate:
        TimeSeries: [diffuseLeakageRateBottomAquifer1, diffuseLeakageRateTopAquifer1, 
                     diffuseLeakageRateBottomAquifer2, diffuseLeakageRateTopAquifer2, 
                     diffuseLeakageRateBottomAquifer3, diffuseLeakageRateTopAquifer3]
    Diffuse_Leakage_Vol:
        TimeSeries: [diffuseLeakageVolumeBottomAquifer1, diffuseLeakageVolumeTopAquifer1, 
                     diffuseLeakageVolumeBottomAquifer2, diffuseLeakageVolumeTopAquifer2, 
                     diffuseLeakageVolumeBottomAquifer3, diffuseLeakageVolumeTopAquifer3]
    Total_Well_Leakage_Vol:
        TimeSeries: [wellLeakageVolumeAquifer1, wellLeakageVolumeAquifer2, wellLeakageVolumeAquifer3]
    Total_Well_Leakage_Rate:
        TimeSeries: [wellLeakageRateAquifer1, wellLeakageRateAquifer2, wellLeakageRateAquifer3]
    Indiv_Well_Leakage_Vol:
        TimeSeries: [well1LeakageVolumeAquifer1, well1LeakageVolumeAquifer2, well1LeakageVolumeAquifer3]
    Indiv_Well_Leakage_Rate:
        TimeSeries: [well1LeakageRateAquifer1, well1LeakageRateAquifer2, well1LeakageRateAquifer2]
    Head_Profile:
        SalsaProfile:                        # This plot requires the profileDepths and allHeadProfilesShales outputs
            FigureDPI: 300
            Title: SALSA Simulation Results
            FigureSize: [11, 13]             # The default figure size is [10, 12]
            SaveCSVFiles: True
            MetricType: head
    Head_Time_Series:
        SalsaTimeSeries:                     # This plot requires the profileDepths, headLoc#Aquifer#, and headProfile#VertPoint#Shale# outputs
            FigureDPI: 300
            Title: SALSA Simulation Results
            FigureSize: [10, 14]             # The default figure size scales with the number of aquifer and shale layers
            SaveCSVFiles: True
            MetricType: head
    Head_Contour_Plot:
        SalsaContourPlot:                    # This plot requires the contourPlotAquiferHead, contourPlotCoordx, and contourPlotCoordy outputs
            FigureDPI: 300
            MetricType: head                 # MetricType is set to head (the default), so the contourPlotAquiferHead output is required
            Title: SALSA Simulation Results
            FigureSize: [11, 9]              # The default figure size is [10, 8]
            SaveCSVFiles: True
            PlotInjectionSites: True
            PlotWellbores: True
            TimeList: [20, 40, 60, 80, 100]  # times given in years
            SpecifyXandYLims:
                xLims: [-12000, 12000]       # Easting (x) and northing (y) distances in meters. Because these are 
                yLims: [-12000, 12000]       # beyond the xMax and yMax distances, portions of the figure will be blank.
    Pressure_Profile:
        SalsaProfile:                # This plot type requires the profileDepths and allPressureProfilesShales outputs
            FigureDPI: 300
            Title: SALSA Simulation Results
            FigureSize: [11, 13]             # The default figure size is [10, 12]
            SaveCSVFiles: True
            MetricType: pressure
    Pressure_Time_Series:
        SalsaTimeSeries:                     # This plot requires the profileDepths, pressureLoc#Aquifer#, pressureLoc#MidAquifer#, 
            FigureDPI: 300                   # pressureLoc#TopAquifer#, and pressureProfile#VertPoint#Shale# outputs.
            Title: SALSA Simulation Results
            FigureSize: [10, 14]             # The default figure size scales with the number of aquifer and shale layers
            SaveCSVFiles: True
            MetricType: pressure
    Pressure_Contour_Plot:
        SalsaContourPlot:                    # This plot requires the contourPlotAquiferPressure, contourPlotCoordx, and contourPlotCoordy outputs
            MetricType: pressure             # If the AquiferLocation entry is set to Middle or Top, the contourPlotMidAquiferPressure or 
            FigureDPI: 300                   # contourPlotTopAquiferPressure outputs would be required, respectively, instead of the contourPlotAquiferPressure output.
            AquiferLocation: Bottom          # Options are Bottom, Middle, or Top - default is Bottom.
            Title: SALSA Simulation Results
            FigureSize: [11, 9]              # The default figure size is [10, 8]
            SaveCSVFiles: True
            PlotInjectionSites: True
            PlotWellbores: True
            TimeList: [20, 40, 60, 80, 100]     # times given in years
            SpecifyXandYLims:
                xLims: [-12000, 12000]          # Easting (x) and northing (y) distances in meters. Because these are 
                yLims: [-12000, 12000]          # beyond the xMax and yMax distances, portions of the figure will be blank.


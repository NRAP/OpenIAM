# -*- coding: utf-8 -*-
import logging
import sys
import os
import numpy as np
import pandas as pd

try:
    import openiam.components.iam_base_classes as iam_bc
except ImportError as err:
    print('Unable to load NRAP-Open-IAM base classes module: {}'.format(err))

try:
    import openiam.components.models.wellbore.multisegmented.multisegmented_wellbore_ROM as mswrom
    from openiam.components.models.wellbore.multisegmented.multisegmented_wellbore_ROM import (
        PLACEHOLDER_PRESSURE_INPUT)
except ImportError:
    print('\nERROR: Unable to load ROM for Multisegmented Wellbore component\n')
    sys.exit()


class MultisegmentedWellbore(iam_bc.ComponentModel):
    """
    The Multisegmented Wellbore component analytically estimates the leakage rates 
    of brine and |CO2| through leakage pathway along leaky wells from a storage 
    reservoir to overlying aquifers, such as an Underground Sources of Drinking 
    Water (USDW). It can consider multiple intermediate aquifers (or thief zones) 
    that may exist between the storage reservoir and the USDW. The model is based 
    on work of Baek et al., :cite:`BaekEtAl2021b`. Further reading can be found in 
    :cite:`N2009` and :cite:`N2011a`.

    The model is focused on flow across relatively large distances and does not 
    take into account discrete features of the flow paths such as fractures and 
    cracks. It assumes that leakage is occurring in the annulus between the 
    outside of the casing and borehole. This area is assigned an “effective” 
    permeability of the flow path. The permeability is applied over a length 
    along the well that corresponds to the thickness of a shale or aquifer layer. 
    Each well is characterized by effective permeability values assigned to each 
    segment of the well crossing individual formations or layers. For example, 
    if a well crosses ``N`` permeable formations, then it is characterized by 
    ``N`` different permeability values. The model utilizes the one-dimensional 
    multiphase version of Darcy’s law to represent flow along a leaky well.

    The initial pressures at the bottom of each unit are calculated with the 
    pressure gradient, unit thickness, and **datumPressure** parameters. 
    Alternatively, the user can enforce specific initial pressures with the 
    **shaleInitialPressure**, **aquiferInitialPressure**, and **reservoirInitialPressure** 
    parameters. Enforcing specific initial pressures must be done carefully, 
    however, as strange behaviors can occur if the user provides inappropriate 
    inputs. Furthermore, the user should ensure that the pressures used are not 
    unrealistic for the depths and conditions used (e.g., having a pressure so 
    high that the unit would likely be fractured, violating the assumptions of 
    the models used). The component can save the initial pressures used to a 
    *.csv* file, so that the user can ensure the setup is as intended. To enable 
    this functionality, the keyword argument ``saveInitialPressures`` must be 
    set to ``True``. To set this keyword argument in the control file interface, 
    include the entry ``SaveInitialConditions: True`` in the component's section 
    of the control file (see *ControlFile_ex2b*).

    It is possible for negative leakage rates to be calculated by the component. 
    Negative leakage rates indicate that fluid is leaving a unit, rather than 
    entering it. To prevent the component from producing negative leakage 
    rates, the keyword argument ``set_neg_lr_to_zero`` can be set to ``True``. 
    With this approach, any negative leakage rates predicted will be set to zero. 
    To enable this approach in the control file interface, include the entry 
    ``NoNegativeLeakage: True`` in the component's section of the control file 
    (see control file *ControlFile_ex2b*).

    In the NRAP-Open-IAM control file interface, the type name for the Multisegmented
    Wellbore component is ``MultisegmentedWellbore``. Descriptions of the 
    component's parameters are provided below.

    * **logWellPerm** [|log10| |m^2|] (-101 to -9) - logarithm of well permeability
      along shale layer (default: -13). Logarithm of well permeability along shale 3,
      for example, can be defined by **logWell3Perm**. Permeability of the well
      along the shale layers not defined by user will be assigned a default value.

    * **logAquPerm** [|log10| |m^2|] (-17 to -9) - logarithm of aquifer
      permeability (default: -12). Logarithm of aquifer 1 permeability, for example,
      can be defined by **logAqu1Perm**. Aquifer permeability not defined by user will
      be assigned a default value.

    * **brineDensity** [|kg/m^3|] (900 to 1500) - density of the brine phase 
      (default: 1000). This brine can move from the reservoir, up the wellbore, 
      and potentially into overlying aquifers.

    * **CO2Density** [|kg/m^3|] (100 to 1000) - density of |CO2| phase 
      (default: 479). This |CO2| can move from the reservoir, up the wellbore, 
      and potentially into overlying aquifers.

    * **brineViscosity** [|Pa*s|] (1.0e-4 to 5.0e-3) - viscosity of the brine 
      phase (default: 2.535e-3).

    * **CO2Viscosity** [|Pa*s|] (1.0e-6 to 1.0e-4)  - viscosity of the |CO2| 
      phase (default: 3.95e-5).

    * **aquBrineResSaturation** [-] (0 to 0.99) - residual saturation of the brine phase
      in each aquifer (default: 0.0). For example, the residual brine saturation
      of aquifer 2 can be defined by **aqu2BrineResSaturation**; otherwise, aquifer
      layers for which the residual brine saturation is not defined will be
      assigned a default value.

    * **compressibility** [|Pa^-1|] (1.0e-13 to 1.0e-8) - compressibility of the 
      brine and |CO2| phases (assumed to be the same for both phases) (default: 5.1e-11).

    * **wellRadius** [|m|] (0.01 to 0.5) - radius of the leaking well (default: 0.05).

    * **numberOfShaleLayers** [-] (3 to 30) - number of shale layers in the
      system (default: 3); *linked to Stratigraphy*. The shale units must be
      separated by an aquifer, and thus, the number of aquifer is 
      **numberOfShaleLayers** - 1. Users need to set  **numberOfShaleLayers**
      and corresponding layer thicknesses appropriately to acheive the intended 
      reservoir top depth.

    * **shaleThickness** [|m|] (1 to 1600) - thickness of shale layers (default: 
      250); *linked to Stratigraphy*. The thickness of individual shale layers 
      can be defined by including a specific shale number (e.g., **shale14Thickness**). 
      Shale 1 is just above the reservoir, while shale ``N`` is the top layer 
      (where ``N`` is set with **numberOfShaleLayers**). If the parameter is 
      given as **shaleThickness** (without a layer number), the thickness provided 
      will be used for all shale layers not assigned a specific thickness. If 
      the **shaleThickness** parameter is not provided, the default value will be 
      used for any shale unit not assigned a specific thickness.

    * **aquiferThickness** [|m|] (1 to 1600) - thickness of aquifers (default: 100); 
      *linked to Stratigraphy*. The thickness of individual aquifer layers 
      can be defined by including a specific aquifer number (e.g., **aquifer12Thickness**). 
      Aquifer 1 is just above shale 1 (which is above the reservoir), while 
      aquifer ``N - 1`` is just below shale ``N`` (where ``N`` is set with 
      **numberOfShaleLayers**, and shale ``N`` is the top layer). If the parameter 
      is given as **aquiferThickness** (without a layer number), the thickness 
      provided will be used for all aquifer layers not assigned a specific 
      thickness. If the **aquiferThickness** parameter is not provided, the default 
      value will be used for any aquifer unit not assigned a specific thickness.

    * **reservoirThickness** [|m|] (1 to 5000) - thickness of the reservoir 
      (default: 30); *linked to Stratigraphy*.

    * **shaleInitialPressure** [|Pa|] (1.0e+5 to 2.0e+7) - initial pressure at the 
      bottom of shales. For example, the initial pressure of shale 14 can 
      be specified with the parameter name **shale14InitialPressure**. This parameter 
      is not used by default. It is only used for an aquifer if that aquifer is 
      given **shaleInitialPressure** input by the user. Otherwise, the initial 
      pressure for the bottom of each shale is calculated with the pressure 
      gradient, unit thickness, and **datumPressure** parameters. Enforcing 
      specific initial pressures for shales must be done carefully, as strange 
      behaviors can occur if the user provides inappropriate inputs. The parameter 
      **shale1InitialPressure** does not need to be provided - the bottom of 
      shale 1 is the top of the reservoir, so the pressure at that depth can 
      be set manually with the **reservoirInitialPressure** parameter.

    * **aquiferInitialPressure** [|Pa|] (1.0e+5 to 2.0e+7) - initial pressure at the 
      bottom of aquifers. For example, the initial pressure of aquifer 1 can 
      be specified with the parameter name **aquifer1InitialPressure**. This parameter 
      is not used by default. It is only used for an aquifer if that aquifer is 
      given **aquiferInitialPressure** input by the user. Otherwise, the initial 
      pressure for the bottom of each aquifer is calculated with the pressure 
      gradient, unit thickness, and **datumPressure** parameters. Enforcing 
      specific initial pressures for aquifers must be done carefully, as strange 
      behaviors can occur if the user provides inappropriate inputs.

    * **reservoirInitialPressure** [|Pa|] (1.5e+5 to 2.0e+7) - initial pressure at the 
      top of the reservoir. This parameter is not used by default. It is only 
      used if the user provides **reservoirInitialPressure** input. Otherwise, the 
      initial pressure at the top of the reservoir is calculated with the pressure 
      gradient, unit thickness, and **datumPressure** parameters. Enforcing a 
      specific initial pressure for the top of the reservoir must be done carefully, 
      as strange behaviors can occur if the user provides inappropriate inputs.

    * **datumPressure** [|Pa|] (8.0e+4 to 3.0e+7) - pressure at the top of the
      system (default: 101,325); *linked to Stratigraphy*. The top of the system 
      can represent the land surface, with atmospheric pressure. Alternatively, 
      if the site considered is offshore, then the top of the system could be 
      the bottom of the ocean, with an elevated pressure due to the column of 
      seawater. Otherwise, the top of the system could represent a specific depth 
      in the subsurface. In that case, the user would only be considering dynamics 
      between that depth and the reservoir.

    * **shalePressureGrad** [|Pa/m|] (8.83e+3 to 1.47e+4) - rate at which 
      pressure increases across (a) shale(s) (default: 9.792e+3, hydrostatic). 
      If a specific shale number is specified (e.g., **shale7PressureGrad**), the 
      input will apply to only that shale. If no shale number is specified 
      (**shalePressureGrad**), the input will apply to all shales that are not 
      given a specific value.

    * **aquiferPressureGrad** [|Pa/m|] (8.83e+3 to 1.47e+4) - rate at which 
      pressure increases across (an) aquifer(s) (default: 9.792e+3, hydrostatic). 
      If a specific aquifer number is specified (e.g., **aquifer5PressureGrad**), 
      the input will apply to only that aquifer. If no aquifer number is specified 
      (**aquiferPressureGrad**), the input will apply to all aquifers that are
      not given a specific value.

    * **reservoirPressureGrad** [|Pa/m|] (8.83e+3 to 1.47e+4) - rate at which 
      pressure increases across the reservoir (default: 9.792e+3, hydrostatic).

    The possible outputs from the Multisegmented Wellbore component are
    leakage rates of |CO2| and brine to each of the aquifers in the system and
    atmosphere. The names of the observations are of the form:

    * **CO2_aquifer1**, **CO2_aquifer2**,..., **CO2_atm** [|kg/s|] -
      |CO2| leakage rates

    * **brine_aquifer1**, **brine_aquifer2**,..., **brine_atm** [|kg/s|] -
      brine leakage rates

    * **mass_CO2_aquifer1**, **mass_CO2_aquifer2**,..., **mass_CO2_aquiferN** [|kg|]
      - mass of the |CO2| leaked into the aquifer.

    For control file examples using the Multisegmented Wellbore component, see 
    *ControlFile_ex2a* through *ControlFile_ex2c*, *ControlFile_ex3*, 
    *ControlFile_ex8a*, *ControlFile_ex24*, *ControlFile_ex31f*, and *ControlFile_ex39a*. 
    For script examples, see *iam_sys_reservoir_mswell_2aquifers.py*, 
    *iam_sys_lutstrata_reservoir_mswell.py*, and 
    *iam_sys_reservoir_mswell_futuregen_ttfdplot_dipping_strata.py*.
    """
    def __init__(self, name, parent, saveInitialPressures=False, output_dir=None, 
                 stochastic=False, realizations=None, set_neg_lr_to_zero=False):
        """
        Constructor method of MultisegmentedWellbore class

        :param name: name of component model
        :type name: str

        :param parent: the SystemModel object that the component model
            belongs to
        :type parent: SystemModel object

        :param saveInitialPressures: option to save the initial pressures at the 
            datum and the bottoms of units to a .csv file (True for yes, False 
            for no). If output_dir is left as None or given as an inapplicable 
            value, then this option will be set to False.
        :type saveInitialPressures: boolean

        :param output_dir: directory in which to save the initial pressures, if 
            saveInitialPressures is set to True.
        :type output_dir: str or None

        :param stochastic: specifies whether the simulation type is stochastic 
            (True, for lhs or parstudy) or deterministic (False, for forward).
        :type stochastic: boolean

        :param realizations: If the simulation is stochastic, specifies the 
            number of realizations used. If not, this keyword arguments is None.
        :type realizations: int or None

        :param set_neg_lr_to_zero: Specifies if negative leakage rates should 
            be set to zero (True) or not (False).
        :type set_neg_lr_to_zero: boolean

        :returns: MultisegmentedWellbore class object
        """
        # Set up keyword arguments of the 'model' method provided by the system model
        model_kwargs = {'time_point': 365.25, 'time_step': 365.25}   # default value of 365.25 days

        super().__init__(name, parent, model=self.simulation_model,
                         model_kwargs=model_kwargs)

        # Add type attribute
        self.class_type = 'MultisegmentedWellbore'

        # Set default parameters of the component model
        self.add_default_par('numberOfShaleLayers', value=3)
        self.add_default_par('shaleThickness', value=250.0)
        self.add_default_par('aquiferThickness', value=100.0)
        self.add_default_par('reservoirThickness', value=30.0)
        self.add_default_par('logWellPerm', value=-13.0)
        self.add_default_par('logAquPerm', value=-12.0)
        self.add_default_par('datumPressure', value=101325.0)
        self.add_default_par('brineDensity', value=1000.0)
        self.add_default_par('CO2Density', value=479.0)
        self.add_default_par('brineViscosity', value=2.535e-3)
        self.add_default_par('CO2Viscosity', value=3.95e-5)
        self.add_default_par('aquBrineResSaturation', value=0.0)
        self.add_default_par('compressibility', value=5.1e-11)
        self.add_default_par('wellRadius', value=0.05)
        self.add_default_par('shaleInitialPressure', value=PLACEHOLDER_PRESSURE_INPUT)
        self.add_default_par('aquiferInitialPressure', value=PLACEHOLDER_PRESSURE_INPUT)
        self.add_default_par('reservoirInitialPressure', value=PLACEHOLDER_PRESSURE_INPUT)
        self.add_default_par('shalePressureGrad', value=9.792e+3)
        self.add_default_par('aquiferPressureGrad', value=9.792e+3) 
        self.add_default_par('reservoirPressureGrad', value=9.792e+3)

        # Define dictionary of boundaries
        self.pars_bounds = dict()
        self.pars_bounds['numberOfShaleLayers'] = [3, 30]
        self.pars_bounds['shaleThickness'] = [1.0, 5000.0]
        self.pars_bounds['aquiferThickness'] = [1.0, 5000.0]
        self.pars_bounds['reservoirThickness'] = [1.0, 5000.0]
        self.pars_bounds['logWellPerm'] = [-101.0, -9.0]
        self.pars_bounds['logAquPerm'] = [-17.0, -9.0]
        self.pars_bounds['datumPressure'] = [8.0e+4, 3.0e+7]
        self.pars_bounds['brineDensity'] = [900.0, 1500.0]
        self.pars_bounds['CO2Density'] = [100.0, 1000.0]
        self.pars_bounds['brineViscosity'] = [1.0e-4, 5.0e-3]
        self.pars_bounds['CO2Viscosity'] = [1.0e-6, 1.0e-4]
        self.pars_bounds['aquBrineResSaturation'] = [0.0, 0.99]
        self.pars_bounds['compressibility'] = [1.0e-13, 1.0e-8]
        self.pars_bounds['wellRadius'] = [0.01, 0.5]
        self.pars_bounds['shaleInitialPressure'] = [1.0e+5, 5.0e+7]
        self.pars_bounds['aquiferInitialPressure'] = [1.0e+5, 5.0e+7]
        self.pars_bounds['reservoirInitialPressure'] = [1.5e+5, 5.0e+7]

        self.pars_bounds['shalePressureGrad'] = [8.83e+3, 1.47e+4]
        self.pars_bounds['aquiferPressureGrad'] = [8.83e+3, 1.47e+4]
        self.pars_bounds['reservoirPressureGrad'] = [8.83e+3, 1.47e+4]

        self.saveInitialPressures = saveInitialPressures
        self.output_dir = output_dir
        self.set_neg_lr_to_zero = set_neg_lr_to_zero

        # Used to set up the output folders correctly for stochastic simulations
        self.stochastic = stochastic
        self.realizations = realizations

        # By default, the smallest number of aquifers the system can have is 2,
        # so we add two accumulators by default. Extra will be added as needed,
        # once the system knows how many there are
        for i in range(2):
            self.add_accumulator('mass_CO2_aquifer'+str(i+1), sim=0.0)
            self.add_accumulator('volume_CO2_aquifer'+str(i+1), sim=0.0)
            self.add_accumulator('CO2_saturation_aquifer'+str(i+1), sim=0.0)
        self.num_accumulators = 2

        # Define output dictionary labels
        self.output_labels = ['CO2_aquifer1', 'CO2_aquifer2', 'CO2_atm',
                              'brine_aquifer1', 'brine_aquifer2', 'brine_atm',
                              'mass_CO2_aquifer1', 'mass_CO2_aquifer2']

        # Setup default observations of the component
        self.default_obs = {obs_nm: 0.0 for obs_nm in self.output_labels}

        debug_msg = 'MultisegmentedWellbore component created with name {}'.format(name)
        logging.debug(debug_msg)

    def check_input_parameters(self, p):
        """
        Check whether input parameters fall within specified boundaries.

        :param p: input parameters of component model
        :type p: dict
        """
        debug_msg = 'Input parameters of component {} are {}'.format(self.name, p)
        logging.debug(debug_msg)

        for key, val in p.items():
            warn_msg = ''.join([
                'Parameter {} of MultisegmentedWellbore component {} ',
                'is out of boundaries.']).format(key, self.name)

            if key.startswith('shale') and key.endswith('Thickness'):
                if (val < self.pars_bounds['shaleThickness'][0]) or (
                        val > self.pars_bounds['shaleThickness'][1]):
                    logging.warning(warn_msg)
                continue

            elif key.startswith('aquifer') and key.endswith('Thickness'):
                if (val < self.pars_bounds['aquiferThickness'][0]) or (
                        val > self.pars_bounds['aquiferThickness'][1]):
                    logging.warning(warn_msg)
                continue

            elif key.startswith('logWell') and key.endswith('Perm'):
                if (val < self.pars_bounds['logWellPerm'][0]) or (
                        val > self.pars_bounds['logWellPerm'][1]):
                    logging.warning(warn_msg)
                continue

            elif key.startswith('logAqu') and key.endswith('Perm'):
                if ((val < self.pars_bounds['logAquPerm'][0]) or
                        (val > self.pars_bounds['logAquPerm'][1])):
                    logging.warning(warn_msg)
                continue

            elif key.startswith('aqu') and key.endswith('BrineResSaturation'):
                if ((val < self.pars_bounds['aquBrineResSaturation'][0]) or
                        (val > self.pars_bounds['aquBrineResSaturation'][1])):
                    logging.warning(warn_msg)
                continue

            elif key.startswith('shale') and key.endswith('InitialPressure'):
                if (val < self.pars_bounds['shaleInitialPressure'][0]) or (
                        val > self.pars_bounds['shaleInitialPressure'][1]):
                    logging.warning(warn_msg)
                continue

            elif key.startswith('aquifer') and key.endswith('InitialPressure'):
                if (val < self.pars_bounds['aquiferInitialPressure'][0]) or (
                        val > self.pars_bounds['aquiferInitialPressure'][1]):
                    logging.warning(warn_msg)
                continue

            elif key.startswith('shale') and key.endswith('PressureGrad'):
                if (val < self.pars_bounds['shalePressureGrad'][0]) or (
                        val > self.pars_bounds['shalePressureGrad'][1]):
                    logging.warning(warn_msg)
                continue

            elif key.startswith('aquifer') and key.endswith('PressureGrad'):
                if (val < self.pars_bounds['aquiferPressureGrad'][0]) or (
                        val > self.pars_bounds['aquiferPressureGrad'][1]):
                    logging.warning(warn_msg)
                continue

            elif key in self.pars_bounds:
                if ((val < self.pars_bounds[key][0]) or
                        (val > self.pars_bounds[key][1])):
                    logging.warning(warn_msg)
            else:
                warn_msg = ''.join([
                    'Parameter {} is not recognized as an input parameter ',
                    'of MultisegmentedWellbore component {}.']).format(key, self.name)
                logging.warning(warn_msg)

    def check_kw_args_for_saving(self):
        """
        Checks if the keyword arguments for saving initial pressures (saveInitialPressures 
        and output_dir) are valid. If not, those keyword arguments are reset.
        """
        if self.saveInitialPressures:
            if self.output_dir is None:
                warning_msg = ''.join([
                    f'The MultisegmentedWellbore component {self.name} was created ', 
                    'with the saveInitialPressures keyword argument given as True, ', 
                    'but the output_dir keyword argument was given as None. The ', 
                    'saveInitialPressures keyword argument will be set to False.'])
                logging.warning(warning_msg)

                self.saveInitialPressures = False
        elif not self.saveInitialPressures:
            self.output_dir = None

    def set_up_extra_output(self, comp_data, output_dir, analysis, realizations):
        """
        Used in the control file interface to handle the saveInitialPressures 
        and output_dir keyword arguments, which are used in the ROM file to save 
        .csv files containing the initial pressures.
        """
        if analysis in ['lhs', 'parstudy']:
            self.stochastic = True

        self.realizations = realizations

        if 'SaveInitialConditions' in comp_data:
            save_initial_cond = comp_data['SaveInitialConditions']
            if isinstance(save_initial_cond, bool):
                self.saveInitialPressures = save_initial_cond
            else:
                warning_msg = ''.join([
                    f'The SaveInitialConditions entry ({save_initial_cond}) provided ', 
                    f'for the MultisegmentedWellbore component {self.name} was ', 
                    'not boolean (True or False), so it will not be used.'])
                logging.warning(warning_msg)

        if self.saveInitialPressures:
            if not os.path.exists(os.path.join(output_dir, 'csv_files')):
                os.mkdir(os.path.join(output_dir, 'csv_files'))

            self.output_dir = os.path.join(output_dir, 'csv_files', 
                                           'MultisegmentedWellbore_Initial_Pressures')

            if not os.path.exists(self.output_dir):
                os.mkdir(self.output_dir)

        self.check_kw_args_for_saving()

    def set_negative_leakage_behavior(self, comp_data):
        """
        Used in the control file interface to allow the user to disable the 
        prediction of negative leakage rates. If set_neg_lr_to_zero is set to 
        True, all negative leakage rates will be set to 0 kg/s.
        """
        if 'NoNegativeLeakage' in comp_data:
            no_neg_lr_input = comp_data['NoNegativeLeakage']
            if isinstance(no_neg_lr_input, bool):
                self.set_neg_lr_to_zero = no_neg_lr_input
            else:
                warning_msg = ''.join([
                    f'The NoNegativeLeakage entry ({no_neg_lr_input}) provided ', 
                    f'for the MultisegmentedWellbore component {self.name} was ', 
                    'not boolean (True or False), so it will not be used.'])
                logging.warning(warning_msg)

    def save_initial_pressures(self, num_shale_layers, thicknesses, depths, 
                               initial_pressures, datumPressure):
        """
        Saves the initial pressures for each unit to a .csv file. Only used if 
        the saveInitialPressures keyword argument is True.
        """
        stochastic_check = (self.stochastic 
                            and (self.realizations is not None))

        current_realization = None
        if not stochastic_check:
            file_name = f'{self.name}_initial_pressures.csv'
        elif stochastic_check:
            current_realization = None
            for real in range(self.realizations):
                if current_realization is None:
                    file_name = f'{self.name}_initial_pressures_sim_{real + 1}.csv'

                    file_path = os.path.join(self.output_dir, file_name)

                    if not os.path.exists(file_path):
                        # Find the current realization by identifying which 
                        # realizations have already run based on the file saved.
                        current_realization = real + 1

            file_name = f'{self.name}_initial_pressures_sim_{current_realization}.csv'

        file_path = os.path.join(self.output_dir, file_name)

        proceed_check = True
        if self.stochastic:
            if current_realization is None:
                proceed_check = False

        # Only save the file once
        if not os.path.exists(file_path) and proceed_check:
            if not os.path.exists(self.output_dir):
                os.mkdir(self.output_dir)

            def create_unit_names(num_shale_layers):
                numbered_units = ['datumPressure']
                for i in range(num_shale_layers, 0, -1):
                    if i == num_shale_layers:
                        numbered_units.append(f'shale {i}')
                    else:
                        numbered_units.append(f'aquifer {i}')
                        numbered_units.append(f'shale {i}')

                numbered_units.append('reservoir')

                return numbered_units

            initial_pressures_with_datum = np.zeros((len(initial_pressures) + 1))
            initial_pressures_with_datum[0] = datumPressure
            initial_pressures_with_datum[1:] = initial_pressures

            thicknesses_with_datum = np.zeros((len(thicknesses) + 1))
            thicknesses_with_datum[1:] = thicknesses

            depths_with_datum = np.zeros((len(depths)) + 1)
            depths_with_datum[1:] = depths

            # Example usage
            unit_names = create_unit_names(num_shale_layers)
            data = {
                'Datum or Unit': unit_names, 
                'Thickness (m)': thicknesses_with_datum, 
                'Bottom depth (m)': depths_with_datum, 
                'Initial pressure at datum or bottom of unit (Pa)': 
                    initial_pressures_with_datum, 
                }

            df = pd.DataFrame(data=data)
            df.to_csv(file_path, index=False)

    def simulation_model(self, p, time_point=365.25, time_step=365.25,
                         pressure=0.0, CO2saturation=0.0):
        """
        Return |CO2| and brine leakage rates corresponding to the provided input.
        Note that the names of parameters contained in the input dictionary
        coincide with those defined in this module's docstring.

        :param p: input parameters of multisegmented wellbore model
        :type p: dict

        :param pressure: pressure at the bottom of leaking well, in Pa;
            by default, its value is 0.0
        :type pressure: float

        :param CO2saturation: saturation of |CO2| phase at the bottom
            of leaking well; by default, its value is 0.0
        :type CO2saturation: float

        :param time_point: time point in days at which the leakage rates are
            to be calculated; by default, its value is 365.25 (1 year in days)
        :type time_point: float

        :param time_step: difference between the current and previous
            time points in days; by default, its value is 365.25 (1 year in days)
        :type time_point: float

        :returns: out - dictionary of observations of multisegmented wellbore
            model; keys:
            ['CO2_aquifer1','CO2_aquifer2',...,'CO2_atm',
            'brine_aquifer1','brine_aquifer2',...,'brine_atm',
            'mass_CO2_aquifer1','mass_CO2_aquifer2',...]
        """
        # Obtain the default values of the parameters from dictionary of default parameters
        actual_p = {k: v.value for k, v in self.default_pars.items()}
        # Update default values of parameters with the provided ones
        actual_p.update(p)
        inputParameters = mswrom.Parameters()

        inputParameters.set_neg_lr_to_zero = self.set_neg_lr_to_zero

        nSL = int(actual_p['numberOfShaleLayers'])
        inputParameters.numberOfShaleLayers = nSL

        # Add extra accumulators after the number of aquifers became known
        num_extra_accumulators = nSL-1 - self.num_accumulators
        if num_extra_accumulators != 0: # if component does not have enough accumulators
            for i in range(num_extra_accumulators):
                self.add_accumulator('mass_CO2_aquifer{}'.format(i+3), sim=0.0)
                self.add_accumulator('volume_CO2_aquifer{}'.format(i+3), sim=0.0)
                self.add_accumulator('CO2_saturation_aquifer{}'.format(i+3), sim=0.0)

            self.num_accumulators = nSL - 1

        # Create dictionary of leakage rates
        out = dict()

        # Check whether the initial state of the system is requested. 
        # Then if yes, return the state without proceeding further. If initial 
        # pressures are to be saved, however, it continues further so the pressures 
        # can be saved.
        if time_point == 0.0:
            for i in range(nSL-1):
                out['CO2_aquifer{}'.format(i+1)] = 0.0
                out['brine_aquifer{}'.format(i+1)] = 0.0
                out['mass_CO2_aquifer{}'.format(i+1)] = 0.0
                self.accumulators['mass_CO2_aquifer{}'.format(i+1)].sim = 0.0
                self.accumulators['volume_CO2_aquifer{}'.format(i+1)].sim = 0.0
                self.accumulators['CO2_saturation_aquifer{}'.format(i+1)].sim = 0.0

            out['CO2_atm'] = 0.0
            out['brine_atm'] = 0.0

            if not self.saveInitialPressures:
                return out

        inputParameters.shaleThickness = actual_p[
            'shaleThickness'] * np.ones(nSL)

        inputParameters.shalePermeability = 10 ** actual_p[
            'logWellPerm'] * np.ones(nSL)

        inputParameters.aquiferThickness = actual_p[
            'aquiferThickness'] * np.ones((nSL-1))

        inputParameters.aquiferPermeability = 10 ** actual_p[
            'logAquPerm'] * np.ones((nSL-1))

        inputParameters.shaleInitialPressure = [
            PLACEHOLDER_PRESSURE_INPUT] * nSL

        inputParameters.aquiferInitialPressure = [
            PLACEHOLDER_PRESSURE_INPUT] * (nSL-1)

        if 'brineResSaturation' in actual_p:
            inputParameters.aquBrineResSaturation = actual_p[
                'brineResSaturation'] * np.ones((nSL))
        else:
            inputParameters.aquBrineResSaturation = actual_p[
                'aquBrineResSaturation'] * np.ones((nSL))

        # Set up shale, aquifer, and reservoir parameters
        for i in range(nSL):
            nm = 'shale{}Thickness'.format(i+1)
            if nm in p:
                inputParameters.shaleThickness[i] = p[nm]

            nm = 'shale{}InitialPressure'.format(i+1)
            # Do not set a bottom pressure for shale 1, that is set with the 
            # reservoirInitialPressure parameter.
            if nm in p and nm != 'shale1InitialPressure': 
                inputParameters.shaleInitialPressure[i] = p[nm]

        for i in range(nSL-1):
            nm = 'aquifer{}Thickness'.format(i+1)
            if nm in p:
                inputParameters.aquiferThickness[i] = p[nm]

            nm = 'aquifer{}InitialPressure'.format(i+1)
            if nm in p:
                inputParameters.aquiferInitialPressure[i] = p[nm]

        inputParameters.reservoirThickness = actual_p['reservoirThickness']
        inputParameters.reservoirInitialPressure = actual_p['reservoirInitialPressure']

        # Set up permeabilities of the well segments along shales
        for i in range(nSL):
            nm = 'logWell{}Perm'.format(i+1)
            if nm in p:
                inputParameters.shalePermeability[i] = 10**p[nm]

        # Set up permeabilities of the well segments along aquifers
        for i in range(nSL-1):
            nm = 'logAqu{}Perm'.format(i+1)
            if nm in p:
                inputParameters.aquiferPermeability[i] = 10**p[nm]

        # Set up residual saturation of aquifers and the reservoir
        for i in range(nSL):
            nm = 'aqu{}BrineResSaturation'.format(i) # aqu0BrineResSaturation is reservoir
            if nm in p:
                inputParameters.aquBrineResSaturation[i] = p[nm]

        # Set up datum pressure
        inputParameters.datumPressure = actual_p['datumPressure']

        # Set up brine and CO2 density
        inputParameters.brineDensity = actual_p['brineDensity']
        inputParameters.CO2Density = actual_p['CO2Density']

        # Set up brine and CO2 viscosity
        inputParameters.brineViscosity = actual_p['brineViscosity']
        inputParameters.CO2Viscosity = actual_p['CO2Viscosity']

        # Set up residual saturation and compressibility
        inputParameters.compressibility = actual_p['compressibility']

        # Set up well radius parameter
        inputParameters.wellRadius = actual_p['wellRadius']
        inputParameters.flowArea = np.pi*inputParameters.wellRadius**2

        # Get parameters from keyword arguments of the 'model' method
        inputParameters.timeStep = time_step         # in days
        inputParameters.timePoint = time_point       # in days
        inputParameters.pressure = pressure

        inputParameters.prevCO2Volume = np.zeros(nSL-1)
        inputParameters.CO2saturation = np.zeros(nSL)
        inputParameters.CO2saturation[0] = CO2saturation

        for i in range(nSL-1):
            inputParameters.prevCO2Volume[i] = (
                self.accumulators['volume_CO2_aquifer{}'.format(i+1)].sim)
            inputParameters.CO2saturation[i+1] = (
                self.accumulators['CO2_saturation_aquifer{}'.format(i+1)].sim)

        # pressure gradients for each unit. The highest shale is the first element, 
        # and the top of the reservoir is the last.
        inputParameters.pressureGrads = np.ones((2 * nSL))

        # First set the default values for each unit type
        index = 0
        for i in range(nSL, 0, -1):
            if i < nSL:
                inputParameters.pressureGrads[index] = actual_p['aquiferPressureGrad']
                index += 1

            inputParameters.pressureGrads[index] = actual_p['shalePressureGrad']
            index += 1

        # Set the values given for specific units
        index = 0
        for i in range(nSL, 0, -1):
            if i < nSL:
                nm = f'aquifer{i}PressureGrad'
                if nm in p:
                    inputParameters.pressureGrads[index]  = p[nm]
                index += 1

            nm = f'shale{i}PressureGrad'
            if nm in p:
                inputParameters.pressureGrads[index] = p[nm]
            index += 1

        # Reservoir pressure gradient
        inputParameters.pressureGrads[-1] = actual_p['reservoirPressureGrad']

        # Create solution object with defined input parameters
        sol = mswrom.Solution(inputParameters)

        # If time_point is 0 and saveInitialPressures is True, save the initial  
        # pressures and then return out
        if time_point == 0.0 and self.saveInitialPressures:
            sol.setup_initial_conditions()
            try:
                self.save_initial_pressures(
                    nSL, sol.layerIntervals, sol.allbottomdepth,
                    sol.InitialBottomPressureAll, actual_p['datumPressure'])

            except Exception as e:
                err_msg = ''.join([
                    f'For the Multisegmented Wellbore component {self.name}, ', 
                    'an error occured while attempting to save the ', 
                    'initial pressures. The initial pressures will not ', 
                    f'be saved. Error message: \n{e}'])
                logging.error(err_msg)

            # For time_point == 0, out was set up further above
            return out

        # Find solution corresponding to the inputParameters
        sol.find()

        for i in range(nSL-1):
            out['CO2_aquifer{}'.format(i+1)] = sol.CO2LeakageRates[i]
            out['brine_aquifer{}'.format(i+1)] = sol.brineLeakageRates[i]

            # Convert mass in cubic meters to kg
            out['mass_CO2_aquifer{}'.format(i+1)] = (
                sol.CO2Volume[i] * inputParameters.CO2Density)

            # Keep mass in cubic meters in accumulators
            self.accumulators['mass_CO2_aquifer{}'.format(i+1)].sim = (
                sol.CO2Volume[i] * inputParameters.CO2Density)
            self.accumulators['volume_CO2_aquifer{}'.format(i+1)].sim = sol.CO2Volume[i]
            self.accumulators['CO2_saturation_aquifer{}'.format(i+1)].sim = \
                sol.CO2SaturationAq[i]
 
        out['CO2_atm'] = sol.CO2LeakageRates[inputParameters.numberOfShaleLayers-1]
        out['brine_atm'] = sol.brineLeakageRates[inputParameters.numberOfShaleLayers-1]

        # Return dictionary of outputs
        return out

    def reset(self):
        return

    # Attributes for system connections
    system_inputs = ['pressure',
                     'CO2saturation']
    system_params = ['numberOfShaleLayers',
                     'shale1Thickness',
                     'shale2Thickness',
                     'shale3Thickness',
                     'aquifer1Thickness',
                     'aquifer2Thickness',
                     'reservoirThickness',
                     'datumPressure']

def read_data(filename):
    """
    Routine used for reading data files.

    Read data from file and create numpy.array if file exists.
    """
    # Check whether the file with given name exists
    if os.path.isfile(filename):
        data = np.genfromtxt(filename)
        return data

    return None

def test_multisegmented_wellbore_component():
    try:
        from openiam.components.analytical_reservoir_component import AnalyticalReservoir
    except ImportError as err:
        print('Unable to load IAM class module: {}'.format(err))

    import matplotlib.pyplot as plt
    __spec__ = None

    logging.basicConfig(level=logging.WARNING)
    # Define keyword arguments of the system model
    isUQAnalysisOn = False # False: one forward run, True: stochastic runs
    isPlottingOn = 1
    to_save_png = False

    num_years = 50
    time_array = 365.25 * np.arange(0, num_years+1)

    sm_model_kwargs = {'time_array': time_array} # time is given in days

    # Create system model
    sm = iam_bc.SystemModel(model_kwargs=sm_model_kwargs)

    # Add reservoir component
    res = sm.add_component_model_object(AnalyticalReservoir(
        name='res', parent=sm, injX=100., injY=0., locX=0., locY=0.))

    # Add parameters of reservoir component model
    res.add_par('injRate', min=1e-5, max=1e+2, value=0.0185, vary=False)
    res.add_par('reservoirRadius', min=500.0, max=1000000, value=500.0, vary=False)
    res.add_par('reservoirThickness', min=10.0, max=150., value=40.0, vary=False)
    res.add_par('logResPerm', min=-16.0, max=-9.0, value=-13.69897, vary=False)
    res.add_par('reservoirPorosity', min=0.01, max=0.5, value=0.15, vary=False)

    res.add_par('numberOfShaleLayers', value=5, vary=False)

    res.add_par('shale1Thickness', value=101.0, vary=False)
    res.add_par('shale2Thickness', value=102.0, vary=False)
    res.add_par('shale3Thickness', value=103.0, vary=False)
    res.add_par('shale4Thickness', value=104.0, vary=False)
    res.add_par('shale5Thickness', value=2440.0, vary=False)

    res.add_par('aquifer1Thickness', value=31.0, vary=False)
    res.add_par('aquifer2Thickness', value=32.0, vary=False)
    res.add_par('aquifer3Thickness', value=33.0, vary=False)
    res.add_par('aquifer4Thickness', value=34.0, vary=False)

    res.add_par('brineDensity', min=900.0, max=1300., value=1045.0, vary=False)
    res.add_par('CO2Density', min=200.0, max=900., value=479.0, vary=False)
    res.add_par('brineViscosity', min=1.0e-5, max=1.0e-3, value=2.535e-4, vary=False)
    res.add_par('CO2Viscosity', min=1.0e-5, max=1.0e-4, value=3.95e-5, vary=False)
    res.add_par('brineResSaturation', min=0.0, max=0.99, value=0.0, vary=False)
    res.add_par('brineCompressibility', min=1e-9, max=1e-13, value=1.e-11, vary=False)

    # The datumPressure and pressure gradient parameters of the AnalyticalReservoir 
    # and MultisegmentedWellbore need to match.
    res.add_par('datumPressure', value=101325.0, vary=False)

    res.add_par('shale1PressureGrad', value=9799.9, vary=False)
    res.add_par('shale2PressureGrad', value=9797.7, vary=False)
    res.add_par('shale3PressureGrad', value=9795.5, vary=False)
    res.add_par('shale4PressureGrad', value=9793.3, vary=False)
    res.add_par('shale5PressureGrad', value=9791.1, vary=False)

    res.add_par('aquifer1PressureGrad', value=9799.8, vary=False)
    res.add_par('aquifer2PressureGrad', value=9799.6, vary=False)
    res.add_par('aquifer3PressureGrad', value=9799.4, vary=False)
    res.add_par('aquifer4PressureGrad', value=9799.2, vary=False)

    res.add_par('reservoirPressureGrad', value=9790., vary=False)

    # Add observations of reservoir component model
    res.add_obs_to_be_linked('pressure')
    res.add_obs_to_be_linked('CO2saturation')

    res.add_obs('pressure')
    res.add_obs('CO2saturation')
    res.add_obs('mass_CO2_reservoir')

    # Add multisegmented wellbore component
    ms = sm.add_component_model_object(MultisegmentedWellbore(name='ms', parent=sm))

    ms.add_par('wellRadius', min=0.01, max=0.2, value=0.15, vary=isUQAnalysisOn)

    ms.add_par('numberOfShaleLayers', value=5, vary=False)
    ms.add_par('shale1Thickness', value=101.0, vary=False)
    ms.add_par('shale2Thickness', value=102.0, vary=False)
    ms.add_par('shale3Thickness', value=103.0, vary=False)
    ms.add_par('shale4Thickness', value=104.0, vary=False)
    ms.add_par('shale5Thickness', value=2440.0, vary=False)

    ms.add_par('logWell1Perm', min=-16, max=-9, value=-12, vary=isUQAnalysisOn)
    ms.add_par('logWell2Perm', min=-16, max=-9, value=-12, vary=isUQAnalysisOn)
    ms.add_par('logWell3Perm', min=-16, max=-9, value=-12, vary=isUQAnalysisOn)
    ms.add_par('logWell4Perm', min=-16, max=-9, value=-12, vary=isUQAnalysisOn)
    ms.add_par('logWell5Perm', min=-101, max=-9, value=-100, vary=isUQAnalysisOn)

    ms.add_par('aquifer1Thickness', value=31.0, vary=False)
    ms.add_par('aquifer2Thickness', value=32.0, vary=False)
    ms.add_par('aquifer3Thickness', value=33.0, vary=False)
    ms.add_par('aquifer4Thickness', value=34.0, vary=False)

    ms.add_par('logAqu1Perm', min=-16, max=-9, value=-12, vary=isUQAnalysisOn)
    ms.add_par('logAqu2Perm', min=-16, max=-9, value=-12, vary=isUQAnalysisOn)
    ms.add_par('logAqu3Perm', min=-16, max=-9, value=-12, vary=isUQAnalysisOn)
    ms.add_par('logAqu4Perm', min=-16, max=-9, value=-12, vary=isUQAnalysisOn)

    ms.add_par('aqu1BrineResSaturation', min=0.0, max=0.99, value=0.18, vary=isUQAnalysisOn)
    ms.add_par('aqu2BrineResSaturation', min=0.0, max=0.99, value=0.4, vary=isUQAnalysisOn)
    ms.add_par('aqu3BrineResSaturation', min=0.0, max=0.99, value=0.5, vary=isUQAnalysisOn)
    ms.add_par('aqu4BrineResSaturation', min=0.0, max=0.99, value=0.0, vary=isUQAnalysisOn)

    ms.add_par('shale1PressureGrad', value=9799.9, vary=False)
    ms.add_par('shale2PressureGrad', value=9797.7, vary=False)
    ms.add_par('shale3PressureGrad', value=9795.5, vary=False)
    ms.add_par('shale4PressureGrad', value=9793.3, vary=False)
    ms.add_par('shale5PressureGrad', value=9791.1, vary=False)

    ms.add_par('aquifer1PressureGrad', value=9799.8, vary=False)
    ms.add_par('aquifer2PressureGrad', value=9799.6, vary=False)
    ms.add_par('aquifer3PressureGrad', value=9799.4, vary=False)
    ms.add_par('aquifer4PressureGrad', value=9799.2, vary=False)

    ms.add_par('reservoirPressureGrad', value=9790., vary=False)

    # Add linked parameters: common to both components
    ms.add_par_linked_to_par('reservoirThickness',
                             res.default_pars['reservoirThickness'])
    ms.add_par_linked_to_par('aqu0BrineResSaturation',
                             res.default_pars['brineResSaturation']) # reservoir
    ms.add_par_linked_to_par('compressibility',
                             res.default_pars['brineCompressibility'])
    ms.add_par_linked_to_par('datumPressure',
                             res.default_pars['datumPressure'])

    # Add keyword arguments linked to the output provided by reservoir model
    ms.add_kwarg_linked_to_obs('pressure', res.linkobs['pressure'])
    ms.add_kwarg_linked_to_obs('CO2saturation', res.linkobs['CO2saturation'])

    # Add observations of multisegmented wellbore component model
    # When add_obs method is called, system model automatically
    # (if no other options of the method is used) adds a list of observations
    # with names ms.obsnm_0, ms.obsnm_1,.... The final index is determined by the
    # number of time points in system model time_array attribute.
    # For more information, see the docstring of add_obs of ComponentModel class.
    ms.add_obs('CO2_aquifer1')
    ms.add_obs('CO2_aquifer2')
    ms.add_obs('CO2_aquifer3')
    ms.add_obs('CO2_aquifer4')
    ms.add_obs('CO2_atm')

    ms.add_obs('mass_CO2_aquifer1')
    ms.add_obs('mass_CO2_aquifer2')
    ms.add_obs('mass_CO2_aquifer3')
    ms.add_obs('mass_CO2_aquifer4')

    ms.add_obs('brine_aquifer1')
    ms.add_obs('brine_aquifer2')
    ms.add_obs('brine_aquifer3')
    ms.add_obs('brine_aquifer4')

    if isUQAnalysisOn == 0:
        # Run system model using current values of its parameters
        sm.forward(save_output=True)  # system model is run deterministically

        print('------------------------------------------------------------------')
        print('                  Forward method illustration ')
        print('------------------------------------------------------------------')
        # Since the observations at the particular time points are different variables,
        # method collect_observations_as_time_series creates lists of
        # values of observations belonging to a given component (e.g. cw) and having the same
        # common name (e.g. 'CO2_aquifer1', etc) but differing in indices.
        # More details are given in the docstring and documentation to the method
        # collect_observations_as_time_series of SystemModel class.

        pressure = sm.collect_observations_as_time_series(res, 'pressure')
        CO2saturation = sm.collect_observations_as_time_series(res, 'CO2saturation')
        mass_CO2_reservoir = sm.collect_observations_as_time_series(res, 'mass_CO2_reservoir')
        CO2_aquifer1 = sm.collect_observations_as_time_series(ms, 'CO2_aquifer1')
        CO2_aquifer2 = sm.collect_observations_as_time_series(ms, 'CO2_aquifer2')
        CO2_aquifer3 = sm.collect_observations_as_time_series(ms, 'CO2_aquifer3')
        CO2_aquifer4 = sm.collect_observations_as_time_series(ms, 'CO2_aquifer4')
        CO2_atm = sm.collect_observations_as_time_series(ms, 'CO2_atm')

        mass_CO2_aquifer1 = sm.collect_observations_as_time_series(ms, 'mass_CO2_aquifer1')
        mass_CO2_aquifer2 = sm.collect_observations_as_time_series(ms, 'mass_CO2_aquifer2')
        mass_CO2_aquifer3 = sm.collect_observations_as_time_series(ms, 'mass_CO2_aquifer3')
        mass_CO2_aquifer4 = sm.collect_observations_as_time_series(ms, 'mass_CO2_aquifer4')

        brine_aquifer1 = sm.collect_observations_as_time_series(ms, 'brine_aquifer1')
        brine_aquifer2 = sm.collect_observations_as_time_series(ms, 'brine_aquifer2')
        brine_aquifer3 = sm.collect_observations_as_time_series(ms, 'brine_aquifer3')
        brine_aquifer4 = sm.collect_observations_as_time_series(ms, 'brine_aquifer4')

        num_samples = 1
        print('Forward run is done. ')

        if isPlottingOn:
            # Plot results
            label_size = 13
            font_size = 16
            ticks_size = 12
            line_width = 1
            fig = plt.figure(figsize=(13, 12))

            ax = fig.add_subplot(321)
            plt.plot(time_array/365.25, CO2_aquifer1,
                     color='steelblue', linewidth=line_width)
            plt.xlabel('Time, years', fontsize=label_size)
            plt.ylabel('Leakage rates, kg/s', fontsize=label_size)
            plt.title(r'Leakage of CO$_2$: aquifer 1', fontsize=font_size)
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, 50])
            ax.get_yaxis().set_label_coords(-0.13, 0.5)

            ax = fig.add_subplot(322)
            plt.plot(time_array/365.25, CO2_aquifer2,
                     color='steelblue', linewidth=line_width)
            plt.xlabel('Time, years', fontsize=label_size)
            plt.ylabel('Leakage rates, kg/s', fontsize=label_size)
            plt.title(r'Leakage of CO$_2$: aquifer 2', fontsize=font_size)
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, 50])
            ax.get_yaxis().set_label_coords(-0.13, 0.5)

            ax = fig.add_subplot(323)
            plt.plot(time_array/365.25, brine_aquifer1,
                     color='rosybrown', linewidth=line_width)
            plt.xlabel('Time, years', fontsize=label_size)
            plt.ylabel('Leakage rates, kg/s', fontsize=label_size)
            plt.title(r'Leakage of brine: aquifer 1', fontsize=font_size)
            plt.tight_layout()
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, 50])
            ax.get_yaxis().set_label_coords(-0.14, 0.5)

            ax = fig.add_subplot(324)
            plt.plot(time_array/365.25, brine_aquifer2,
                     color='rosybrown', linewidth=line_width)
            plt.xlabel('Time, years', fontsize=label_size)
            plt.ylabel('Leakage rates, kg/s', fontsize=label_size)
            plt.title(r'Leakage of brine: aquifer 2', fontsize=font_size)
            plt.tight_layout()
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, 50])
            ax.get_yaxis().set_label_coords(-0.14, 0.5)

            fig.subplots_adjust(
                left=0.1, top=0.95, right=0.95, bottom=0.05, wspace=0.22)

            # to_save_png = False
            if to_save_png:
                plt.savefig('MSWObservationsCombinedPlotVsTime.png', dpi=300)
                print('"MSWObservationsCombinedPlotVsTime.png" successfully saved.')

    else:
        print('------------------------------------------------------------------')
        print('                          UQ illustration ')
        print('------------------------------------------------------------------')

        import random
        num_samples = 300
        ncpus = 1
        # Draw Latin hypercube samples of parameter values
        seed = random.randint(500, 1100)

        s = sm.lhs(siz=num_samples, seed=seed)

        # Run model using values in samples for parameter values
        results = s.run(cpus=ncpus, verbose=False)

        # Extract results from stochastic simulations
        outputs = s.collect_observations_as_time_series()
        CO2_aquifer1 = outputs['ms.CO2_aquifer1']
        CO2_aquifer2 = outputs['ms.CO2_aquifer2']
        CO2_aquifer3 = outputs['ms.CO2_aquifer3']
        CO2_aquifer4 = outputs['ms.CO2_aquifer4']
        brine_aquifer1 = outputs['ms.brine_aquifer1']
        brine_aquifer2 = outputs['ms.brine_aquifer2']
        brine_aquifer3 = outputs['ms.brine_aquifer3']
        brine_aquifer4 = outputs['ms.brine_aquifer4']
        mass_CO2_aquifer1 = outputs['ms.mass_CO2_aquifer1']
        mass_CO2_aquifer2 = outputs['ms.mass_CO2_aquifer2']
        mass_CO2_aquifer3 = outputs['ms.mass_CO2_aquifer3']
        mass_CO2_aquifer4 = outputs['ms.mass_CO2_aquifer4']

        mass_CO2_reservoir = outputs['res.mass_CO2_reservoir']

        print('UQ run is done. ')

        if isPlottingOn:
            # Plot result 1 : CO2 mass rate, kg/s
            label_size = 8
            font_size = 10
            ticks_size = 6
            line_width = 1
            fig = plt.figure(1, figsize=(13, 3.5))

            ax = fig.add_subplot(141)
            for j in range(num_samples):
                plt.plot(time_array/365.25, CO2_aquifer1[j],
                         color='steelblue', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leakage rates, q (kg/s)', fontsize=label_size)
            plt.title(r'Leakage of CO$_2$: aquifer 1', fontsize=font_size)
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, num_years])
            ax.get_yaxis().set_label_coords(-0.13, 0.5)

            ax = fig.add_subplot(142)
            for j in range(num_samples):
                plt.plot(time_array/365.25, CO2_aquifer2[j],
                         color='steelblue', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leakage rates, q (kg/s)', fontsize=label_size)
            plt.title(r'Leakage of CO$_2$: aquifer 2', fontsize=font_size)
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, num_years])
            ax.get_yaxis().set_label_coords(-0.13, 0.5)

            ax = fig.add_subplot(143)
            for j in range(num_samples):
                plt.plot(time_array/365.25, CO2_aquifer3[j],
                         color='steelblue', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leakage rates, q (kg/s)', fontsize=label_size)
            plt.title(r'Leakage of CO$_2$: aquifer 3', fontsize=font_size)
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, num_years])
            ax.get_yaxis().set_label_coords(-0.13, 0.5)

            ax = fig.add_subplot(144)
            for j in range(num_samples):
                plt.plot(time_array/365.25, CO2_aquifer4[j],
                         color='steelblue', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leakage rates, q (kg/s)', fontsize=label_size)
            plt.title(r'Leakage of CO$_2$: aquifer 4', fontsize=font_size)
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, num_years])
            ax.get_yaxis().set_label_coords(-0.13, 0.5)

            if to_save_png:
                plt.savefig('MSWObservationsCombinedPlot1VsTime.png', dpi=300)
                print('"MSWObservationsCombinedPlot1VsTime.png" successfully saved.')

            # Plot result 2 : CO2 mass, kg
            label_size = 8
            font_size = 10
            ticks_size = 6
            line_width = 1
            fig = plt.figure(2, figsize=(13, 3.5))

            # CO2 mass
            ax = fig.add_subplot(141)
            for j in range(num_samples):
                plt.plot(time_array/365.25, mass_CO2_aquifer1[j],
                         color='steelblue', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leaked mass, kg', fontsize=label_size)
            plt.title(r'Leakage of CO$_2$: aquifer 1', fontsize=font_size)
            plt.tick_params(labelsize=ticks_size)
            ax.yaxis.get_offset_text().set_fontsize(ticks_size)
            plt.xlim([0, num_years])
            ax.get_yaxis().set_label_coords(-0.13, 0.5)

            ax = fig.add_subplot(142)
            for j in range(num_samples):
                plt.plot(time_array/365.25, mass_CO2_aquifer2[j],
                         color='steelblue', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leaked mass, kg', fontsize=label_size)
            plt.title(r'Leakage of CO$_2$: aquifer 2', fontsize=font_size)
            plt.tick_params(labelsize=ticks_size)
            ax.yaxis.get_offset_text().set_fontsize(ticks_size)
            plt.xlim([0, num_years])
            ax.get_yaxis().set_label_coords(-0.13, 0.5)

            ax = fig.add_subplot(143)
            for j in range(num_samples):
                plt.plot(time_array/365.25, mass_CO2_aquifer3[j],
                         color='steelblue', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leaked mass, kg', fontsize=label_size)
            plt.title(r'Leakage of CO$_2$: aquifer 3', fontsize=font_size)
            plt.tick_params(labelsize=ticks_size)
            ax.yaxis.get_offset_text().set_fontsize(ticks_size)
            plt.xlim([0, num_years])
            ax.get_yaxis().set_label_coords(-0.13, 0.5)

            ax = fig.add_subplot(144)
            for j in range(num_samples):
                plt.plot(time_array/365.25, mass_CO2_aquifer4[j],
                         color='steelblue', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leaked mass, kg', fontsize=label_size)
            plt.title(r'Leakage of CO$_2$: aquifer 4', fontsize=font_size)
            plt.tick_params(labelsize=ticks_size)
            ax.yaxis.get_offset_text().set_fontsize(ticks_size)
            plt.xlim([0, num_years])
            ax.get_yaxis().set_label_coords(-0.13, 0.5)

            if to_save_png:
                plt.savefig('MSWObservationsCombinedPlot2VsTime.png', dpi=300)
                print('"MSWObservationsCombinedPlot2VsTime.png" successfully saved.')

            # Plot result 3 : brine mass rate, kg/s
            label_size = 8
            font_size = 10
            ticks_size = 6
            line_width = 1
            fig = plt.figure(3, figsize=(13, 3.5))

            ax = fig.add_subplot(141)
            for j in range(num_samples):
                plt.plot(time_array/365.25, brine_aquifer1[j],
                         color='rosybrown', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leakage rates, q (kg/s)', fontsize=label_size)
            plt.title(r'Leakage of brine: aquifer 1', fontsize=font_size)
            plt.tight_layout()
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, num_years])
            ax.get_yaxis().set_label_coords(-0.14, 0.5)

            ax = fig.add_subplot(142)
            for j in range(num_samples):
                plt.plot(time_array/365.25, brine_aquifer2[j],
                         color='rosybrown', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leakage rates, q (kg/s)', fontsize=label_size)
            plt.title(r'Leakage of brine: aquifer 2', fontsize=font_size)
            plt.tight_layout()
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, num_years])
            ax.get_yaxis().set_label_coords(-0.14, 0.5)

            ax = fig.add_subplot(143)
            for j in range(num_samples):
                plt.plot(time_array/365.25, brine_aquifer3[j],
                         color='rosybrown', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leakage rates, q (kg/s)', fontsize=label_size)
            plt.title(r'Leakage of brine: aquifer 3', fontsize=font_size)
            plt.tight_layout()
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, num_years])
 
            ax.get_yaxis().set_label_coords(-0.14, 0.5)

            ax = fig.add_subplot(144)
            for j in range(num_samples):
                plt.plot(time_array/365.25, brine_aquifer4[j],
                         color='rosybrown', linewidth=line_width)
            plt.xlabel('Time, t (years)', fontsize=label_size)
            plt.ylabel('Leakage rates, q (kg/s)', fontsize=label_size)
            plt.title(r'Leakage of brine: aquifer 4', fontsize=font_size)
            plt.tight_layout()
            plt.tick_params(labelsize=ticks_size)
            plt.xlim([0, num_years])

            ax.get_yaxis().set_label_coords(-0.14, 0.5)

            if to_save_png:
                plt.savefig('MSWObservationsCombinedPlot3VsTime.png', dpi=300)
                print('"MSWObservationsCombinedPlot3VsTime.png" successfully saved.')

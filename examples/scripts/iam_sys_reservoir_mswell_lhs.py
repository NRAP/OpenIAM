'''
Here, the system model contains two linked component models: an Analytical 
Reservoir model and a Multisegmented Wellbore. The reservoir model provides pressures 
and CO2 saturations to the wellbore model, which then calculates the leakage rates 
of brine and CO2 into two aquifers overlying the reservoir.

Here, a stochastic analysis type is used (Latin Hypercube Sampling). There are 
30 realizations where parameter values are varied.

This example was created to have a setup similar to control file example 2a 
(ControlFile_ex2a.yaml).

Examples of run:
$ python iam_sys_reservoir_mswell_lhs.py
'''

import matplotlib.pyplot as plt
import numpy as np

from openiam.components.iam_base_classes import SystemModel
from openiam.components.analytical_reservoir_component import AnalyticalReservoir
from openiam.components.multisegmented_wellbore_component import MultisegmentedWellbore


if __name__=='__main__':
    # Create system model
    num_years = 10.
    time_array = 365.25 * np.arange(0.0, num_years+1)
    sm_model_kwargs = {'time_array': time_array}   # time is given in days
    sm = SystemModel(model_kwargs=sm_model_kwargs)
    # Add reservoir component
    ares = sm.add_component_model_object(AnalyticalReservoir(name='ares', parent=sm, locX=100., locY=100.))

    #stratigraphy parameters
    ares.add_par('numberOfShaleLayers', value=3, vary=False)
    ares.add_par('shale1Thickness', min=30.0, max=50.0, value=40.0)
    ares.add_par('aquifer1Thickness',  min=20.0, max=60.0, value=45.0)
    ares.add_par('shale2Thickness', value=250.0, vary=False)
    ares.add_par('aquifer2Thickness',  min=30.0, max=50.0, value=45.0)
    ares.add_par('shale3Thickness', value=250.0, vary=False)
    
    # Add parameters of reservoir component model
    ares.add_par('injRate', value=3, vary=False)
    ares.add_par('logResPerm', min=-13.0, max=-12.0, value=-12.5)
    ares.add_par('reservoirRadius', value=2500, vary=False)
    ares.add_par('brineResSaturation', value=0.1, vary=False)
    
    # Add observations of reservoir component model
    ares.add_obs('pressure')
    ares.add_obs('CO2saturation')
    ares.add_obs_to_be_linked('pressure')
    ares.add_obs_to_be_linked('CO2saturation')

    # Add multisegmented wellbore component
    msw = sm.add_component_model_object(MultisegmentedWellbore(name='msw', parent=sm))

    # Add parameters of multisegmented wellbore component
    msw.add_par('logWellPerm', min=-14.0,max=-11.0,value=-13.5)
    
    #repeat stratigraphy parameters
    msw.add_par('numberOfShaleLayers', value=3, vary=False)
    msw.add_par('shale1Thickness', min=30.0, max=50.0, value=40.0)
    msw.add_par('aquifer1Thickness',  min=20.0, max=60.0, value=45.0)
    msw.add_par('shale2Thickness', value=250.0, vary=False)
    msw.add_par('aquifer2Thickness',  min=30.0, max=50.0, value=45.0)
    msw.add_par('shale3Thickness', value=250.0, vary=False)

    # Add keyword arguments of the cemented wellbore component model
    msw.add_kwarg_linked_to_obs('pressure', ares.linkobs['pressure'])
    msw.add_kwarg_linked_to_obs('CO2saturation', ares.linkobs['CO2saturation'])

    # Add observations of the multisegmented wellbore component
    msw.add_obs('CO2_aquifer1')
    msw.add_obs('CO2_aquifer2')
    msw.add_obs('CO2_atm')
    msw.add_obs('brine_aquifer1')
    msw.add_obs('brine_aquifer2')

    # Run lhs simulation
    size = 30
    seed = 721
    s = sm.lhs(siz=size,seed=seed)
    results = s.run()

    # The results array will be an array of (X,Y) where X is the # of realizations
    # and Y is # of timesteps * # of observations. Observations are ordered by column, so 
    # this example has 7 observations, and our timeseries is 11 elements long (0 - 10) inclusive
    # so each collected observation array is (30 x 11).
    # Plot our observations in time series arrays
    font = {'family': 'Arial',
            'weight': 'normal',
            'size': 10}
    plt.rc('font', **font)

    plt.figure(1)
    ax = plt.gca()
    ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
    pressure_all = np.array(results[:,0:11])
    for ii in range(0,size):
        ax.plot(sm.time_array/365.25, pressure_all[ii], color='#000066',
                 linewidth=2, label="pressure_res")
    ax.set_xlabel('Time [years]', fontsize=12, fontweight='bold')
    ax.set_ylabel('Reservoir Pressure [Pa]', fontsize=12, fontweight='bold')
    ax.set_title(r'Pressure in the Reservoir', fontsize=12, fontweight='bold')

    plt.figure(2)
    ax = plt.gca()
    ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
    CO2_Sat_all = np.array(results[:,11:22])
    for ii in range(0,size):
        ax.plot(sm.time_array/365.25, CO2_Sat_all[ii], color='#FF0066',
                 linewidth=2, label="CO2_Saturation")
    ax.set_xlabel('Time [years]', fontsize=12, fontweight='bold')
    ax.set_ylabel('CO$_2$_Saturation [-]', fontsize=12, fontweight='bold')
    ax.set_title(r'CO$_2$ Saturation in the Reservoir', fontsize=12, fontweight='bold')

    plt.figure(3)
    ax = plt.gca()
    ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
    Brine_leakrates_aq1_all = np.array(results[:,55:66])
    for ii in range(0,size):
        ax.plot(sm.time_array/365.25, Brine_leakrates_aq1_all[ii], color='#000099',
                 linewidth=2, label="Brine Leakage")
    ax.set_xlabel('Time [years]')
    ax.set_ylabel('Brine Leakage Rate [kg/s]', fontsize=12, fontweight='bold')
    ax.set_title(r'Brine Leakage to Aquifer 1', fontsize=12, fontweight='bold')

    plt.figure(4)
    ax = plt.gca()
    ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
    CO2_leakrates_aq1_all = np.array(results[:,22:33])
    for ii in range(0,size):
        ax.plot(sm.time_array/365.25, CO2_leakrates_aq1_all[ii], color='#FF0066',
                 linewidth=2, label="aquifer 1")
    ax.xlabel('Time [years]', fontsize=12, fontweight='bold')
    ax.ylabel('CO$_2$ Leakage Rates [kg/s]', fontsize=12, fontweight='bold')
    ax.title(r'CO$_2$ Leakage to Aquifer 1', fontsize=12, fontweight='bold')

    plt.figure(5)
    ax = plt.gca()
    ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
    Brine_leakrates_aq2_all = np.array(results[:,66:77])
    for ii in range(0,size):
        ax.plot(sm.time_array/365.25, Brine_leakrates_aq2_all[ii], color='#000099',
                 linewidth=2, label="aquifer 2")
    ax.xlabel('Time [years]')
    ax.ylabel('Brine Leakage Rate [kg/s]', fontsize=12, fontweight='bold')
    ax.title(r'Brine Leakage to Aquifer 2', fontsize=12, fontweight='bold')

    plt.figure(6)
    ax = plt.gca()
    ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
    CO2_leakrates_aq2_all = np.array(results[:,33:44])
    for ii in range(0,size):
        ax.plot(sm.time_array/365.25, CO2_leakrates_aq2_all[ii], color='#FF0066',
                 linewidth=2, label="aquifer 2")
    ax.xlabel('Time [years]')
    ax.ylabel('CO$_2$ Leakage Rates [kg/s]', fontsize=12, fontweight='bold')
    ax.title(r'CO$_2$ Leakage to Aquifer 2', fontsize=12, fontweight='bold')

    plt.show()

"""
This code can be used to obtain dictionaries describing the parameters and 
keyword arguments for NRAP-Open-IAM components. The parameter information 
provided includes parameter names as well as minimum, maximum, and default 
values. The keyword argument information includes the names and default values 
of keyword arguments.
"""
import os
import logging
import numpy as np
import pickle
import inspect

try:
    from openiam.components.iam_base_classes import IAM_DIR, SystemModel
    import openiam.base as iam_base
except ImportError as err:
    print('Unable to load IAM class module: {}'.format(err))

# Items in iam_base.__all__ that do not need to be checked for parameter information
NOT_COMPONENTS_TO_CHECK = [
    'IAM_DIR', 'SystemModel', 'ComponentModel', 'SamplerModel', 
    'ReservoirDataInterpolator', 'RateToMassAdapter', 'LocationGenerator', 
    'DataInterpolator', 'Mesh2D', 'read_Mesh2D_data', 'SHPermeabilitySampler',
    'SHThicknessSampler', 'SHFractureSampler', 'ParameterSetup1', 'ParameterSetup2', 
    'ParameterSetup3', 'ParameterSetup4', 'ParameterSetup5', 'PressureBasedRiskConfigurer',
    'DataBasedRiskConfigurer', 'WellDepthRiskConfigurer', 'MonitoringTool1',
    'MonitoringTool2', 'MonitoringTool3', 'MonitoringScheduler1', 
    'MonitoringScheduler2', 'MonitoringScheduler3', 'AreaEstimate'
    ]

# Components that do not need to be checked for the GUI. Some are not available 
# in the GUI, and some do not have standard parameters (LookupTableReservoir 
# and PlumeStability).
COMPONENTS_CURRENTLY_DISABLED = [
    'SimpleReservoir', 'LookupTableReservoir', 'GenericReservoir', 
    'CementedWellboreWR', 'KimberlinaWellbore', 'DeepAlluviumAquiferML', 
    'AlluviumAquifer', 'AlluviumAquiferLF', 'PlumeStability'
    ]

# Keyword arguments in the __init__() methods of the component objects that can 
# be excluded.
KW_ARGS_TO_EXCLUDE = [
    'self', 'name', 'parent', 
    ]


def get_par_info(comp_types):
    """
    Function that takes a list of component types and returns a dictionary 
    with information about component parameters (minimum, maximum, and default 
    values, where the minimum and maximum are stored with the 'bounds' key).
    """
    # Dictionary of parameter minimum and maximum values
    par_info = {}
    
    # Set up a system model just to obtain the parameter bounds for each 
    # component type in comp_types
    num_years = 1
    time_array = 365.25 * np.arange(0.0, num_years + 1)
    sm_model_kwargs = {'time_array': time_array} # time is given in days

    # Create dummy system model
    dummy_sm = SystemModel(model_kwargs=sm_model_kwargs)
    
    components = []
    
    for comp_type_name in comp_types:
        par_info[comp_type_name] = {}
        
        comp_type = getattr(iam_base, comp_type_name)
        
        success = False
        try:
            components.append(dummy_sm.add_component_model_object(
                comp_type(name=comp_type_name, parent=dummy_sm)))
            success = True
        except Exception as e:
            warning_msg = ''.join([
                f'Error while attempting to create a {comp_type_name} component: ', 
                f'{e}'])
            logging.warning(warning_msg)
        
        if success:
            par_names = components[-1].pars_bounds.keys()
            
            for par in par_names:
                par_info[comp_type_name][par] = {}
                
                try:
                    par_info[comp_type_name][par]['default'] = components[
                        -1].default_pars[par].value
                except Exception as e:
                    warning_msg = ''.join([
                        f'Error while attempting to obtain the default {par} ', 
                        f'parameter value for the {comp_type_name} component ', 
                        f'class: {e}'])
                    logging.warning(warning_msg)
                
                try:
                    par_info[comp_type_name][par]['bounds'] = [
                        components[-1].pars_bounds[par][0], 
                        components[-1].pars_bounds[par][1]]
                except Exception as e:
                    warning_msg = ''.join([
                        'Error while attempting to obtain the range for the ', 
                        f'{par} parameter for the {comp_type_name} component ', 
                        f'class: {e}'])
                    logging.warning(warning_msg)
                    
    
    return par_info


def get_keyword_argument_info(comp_types):
    """
    Function that takes a list of component types and returns a dictionary 
    containing information about the keyword arguments of the components. The 
    information includes the names and default values for each keyword argument. 
    The dictionary has keys for all keyword argument names, all keyword argument 
    default values, and then a key for a dictionary that contains the default 
    value for each keyword argument. This approach is used in case the number of 
    keyword argument names does not align with the number of default values. In 
    that case, the key containing the dictionary of default values by name 
    (kw_arg_info['default_by_name']) will be None, but the names (kw_arg_info['names']) 
    and default values (kw_arg_info['defaults']) will still be stored. The user 
    would have to find out why the disagreement occurred.
    """
    def get_def_by_name(kw_arg_info, comp_type_name, kw_arg_props):
        """
        Function returns an updated kw_arg_info dictionary with the default value 
        for each keyword argument. This dictionary is only updated, however, if 
        the keyword arguments each have a corresponding default value.
        """
        kw_args = kw_arg_props[0]
        
        kw_arg_defs = kw_arg_props[3]
        
        if len(kw_arg_info[comp_type_name]['defaults']) == len(
                kw_arg_info[comp_type_name]['names']):
            kw_args = kw_arg_info[comp_type_name]['names']
            
            kw_arg_defs = kw_arg_info[comp_type_name]['defaults']
            
            kw_arg_info[comp_type_name]['default_by_name'] = {}
            
            for (kw_arg, kw_arg_def) in zip(kw_args, kw_arg_defs):
                kw_arg_info[comp_type_name]['default_by_name'][
                    kw_arg] = kw_arg_def
        
        return kw_arg_info
    
    # Dictionary of keyword arguments for each component type
    kw_arg_info = {}
    
    # Set up a system model just to obtain the parameter bounds for each 
    # component type in comp_types
    num_years = 1
    time_array = 365.25 * np.arange(0.0, num_years + 1)
    sm_model_kwargs = {'time_array': time_array} # time is given in days

    # Create dummy system model
    dummy_sm = SystemModel(model_kwargs=sm_model_kwargs)
    
    components = []
    
    for comp_type_name in comp_types:
        kw_arg_info[comp_type_name] = {}
        
        comp_type = getattr(iam_base, comp_type_name)
        
        success = False
        try:
            components.append(dummy_sm.add_component_model_object(
                comp_type(name=comp_type_name, parent=dummy_sm)))
            success = True
        except Exception as e:
            warning_msg = ''.join([
                f'Error while attempting to create a {comp_type_name} component: ', 
                f'{e}'])
            logging.warning(warning_msg)
        
        if success:
            kw_arg_props = inspect.getfullargspec(components[-1].__init__)
            
            kw_args = kw_arg_props[0]
            
            kw_arg_info[comp_type_name]['names'] = [
                kw_arg for kw_arg in kw_args if kw_arg not in KW_ARGS_TO_EXCLUDE]
            
            kw_arg_defs = kw_arg_props[3]
            
            kw_arg_info[comp_type_name]['defaults'] = kw_arg_defs
            
            kw_arg_info[comp_type_name]['default_by_name'] = None
            
            if kw_arg_defs is not None:
                try:
                    kw_arg_info = get_def_by_name(
                        kw_arg_info, comp_type_name, kw_arg_props)
                except Exception as e:
                    warning_msg = ''.join([
                        'Error while attempting to aquire the default keyword ', 
                        f'arguments for the {comp_type_name} component class: ', 
                        f'{e}'])
                    logging.warning(warning_msg)
    
    return kw_arg_info


def get_par_and_kw_arg_info(comp_list=None, save_option=True, save_path=None, 
                            par_file_name=None, kw_arg_file_name=None, 
                            log_file_name=None):
    """
    Function that obtains information about the the parameter boundaries and 
    keyword arguments for a list of component classes (comp_list). Parameter 
    information is returned in the par_info dictionary, while keyword argument 
    information is returned in the kw_arg_info dictionary.
    
    The parameter information includes minimum, maximum, and default values. 
    The keyword argument information includes the names of all keyword arguments 
    (excluding those in KW_ARGS_TO_EXCLUDE, like name and parent) and the default 
    values for each keyword argument. The kw_arg_info stores all names, all 
    default values, and then the defaults for each keyword argument. This approach 
    is used in case the number of keyword argument names and defaults do not 
    match.
    
    If comp_list is None, the function uses a selection of component classes.
    This selection exlcudes component classes that are not handled directly by 
    the user in the GUI (NOT_COMPONENTS_TO_CHECK), do not have standard parameters 
    (LookupTableReservoir and PlumeStability), or are currently disabled in the 
    GUI (COMPONENTS_CURRENTLY_DISABLED).
    
    If save_option is True, the function saves results to .pkl files in the 
    directory save_path. If save_path is None, results are saved to 
    IAM_DIR/utilities/search_parameters_and_kw_args/. If save_option is True, 
    the code also saves a .log file to the save_path location. The .log file 
    will describe any errors that occurred while attempting to obtain parameter 
    and keyword argument information.
    """
    if save_option:
        make_pickle_dir = False
        
        if save_path is None:
            make_pickle_dir = True
        elif not os.path.exists(save_path):
            warning_msg = ''.join([
                'The save_path keyword argument provided to the function ', 
                'get_par_and_kw_arg_info() did not correspond with a directory ', 
                'that exists. The default directory will be used ', 
                '(IAM_DIR/utilities/search_parameters_and_kw_args/, where ', 
                'IAM_DIR is the main directory of the NRAP-Open-IAM installation).'])
            logging.warning(warning_msg)
            
            # Using os.path.exists() on a None value would result in an error
            make_pickle_dir = True
        
        if make_pickle_dir:
            save_path = os.path.join(IAM_DIR, 'utilities', 'search_parameters_and_kw_args')
            
            if not os.path.exists(save_path):
                os.mkdir(save_path)
    
        # .log file containing messages
        if log_file_name is None:
            log_file_name = 'get_par_and_kw_arg_info.log'

        log_file_path = os.path.join(save_path, log_file_name)

        log_level = 'DEBUG'

        logger = logging.getLogger('')
        # Remove default existing handlers
        logger.handlers.clear()
        logger.setLevel(log_level)
        # logging formatter for log files with more details
        log_formatter1 = logging.Formatter(
            fmt='%(levelname)s %(module)s.%(funcName)s: %(message)s',
            datefmt='%m-%d %H:%M')
        # logging formatter for console output
        log_formatter2 = logging.Formatter(
            fmt='%(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M')

        # Setup logging to log file
        file_handler = logging.FileHandler(filename=log_file_path, mode='w')
        file_handler.setLevel(log_level)
        file_handler.setFormatter(log_formatter1)
        logger.addHandler(file_handler)

        # Setup logging to console
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        console.setFormatter(log_formatter2)
        logger.addHandler(console)
    
    if comp_list is None:
        # If a list of component types is not provided, use a selection of 
        # components
        iam_base_all = iam_base.__all__
        
        excluded_comps = NOT_COMPONENTS_TO_CHECK + COMPONENTS_CURRENTLY_DISABLED
        
        # Remove items that are not components that need to be checked for 
        # parameter boundaries
        comp_list = [comp for comp in iam_base_all if comp not in excluded_comps]
    
    par_info = get_par_info(comp_list)
    
    kw_arg_info = get_keyword_argument_info(comp_list)
    
    if save_option:
        if par_file_name is None:
            par_file_name = 'par_info.pkl'
        
        if kw_arg_file_name is None:
            kw_arg_file_name = 'kw_arg_info.pkl'
        
        pickle_file = open(os.path.join(save_path, par_file_name), 'ab')
        pickle.dump(par_info, pickle_file)
        pickle_file.close()
        
        pickle_file = open(os.path.join(save_path, kw_arg_file_name), 'ab')
        pickle.dump(kw_arg_info, pickle_file)
        pickle_file.close()
        
        # Shutdown logging so the logging file can be deleted afterwards, if desired
        logging.shutdown()
    
    return par_info, kw_arg_info


if __name__ == '__main__':
    
    par_info, kw_arg_info = get_par_and_kw_arg_info()

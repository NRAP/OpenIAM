.. _gui_operation:

GUI Operation
=============

Launcing the GUI
----------------

To launch the graphical user interface (GUI), the user must open a command prompt 
and navigate to the folder *src/openiam/gu_interface*, where the *src/* folder is 
located in the main directory of the NRAP-Open-IAM installation. The NRAP-Open-IAM 
environment must also be activated. For more details regarding navigation in a command 
prompt or activating an environment, see the installation instructions near the 
beginning of this document.

With the command prompt located in the *src/openiam/gu_interface* directory and the environment 
activated, type this command and hit enter to launch the NRAP-Open-IAM GUI::

    python NRAP_OPENIAM.py

This command will not work if the command prompt is not located in the *src/openiam/gu_interface* 
directory. To check if the command prompt is located in that directory, type ``dir`` and hit enter. 
That command will display the contents of the current directory. If the information shown does not 
include files like ``Dashboard.py``, ``PostProcessor_Page.py``, and ``dictionarydata.py``, then the 
command prompt is not located in the correct directory.

Main Dashboard
--------------

When the graphical user interface (GUI) is first opened, a disclaimer screen
will be shown followed by the main interface.

.. figure:: ../../images/GUI_ScreenShots/FrontPage.PNG
   :align: center
   :width: 400
   :alt: Main NRAP-Open-IAM Interface

   Main NRAP-Open-IAM Interface

On the main dashboard of the GUI, there are several buttons. One button is 
**Enter Parameters**, which can be used to build a system model by selecting 
different component models. The **Use Workflow** button is also used to make 
system models, but aspects of the system model are handled automatically by a 
workflow (instead of the user manually selecting all options). We discuss 
workflows further below, but we will first discuss how to build a system model 
manually.

Build a System Model
--------------------

To begin building a system model, click on the **Enter Parameters** button. 
The process of building a model consists of entering basic model parameters, 
defining the stratigraphy of the site, and then adding each component to be 
included in the system model. Therefore, the first tab that a user would see 
after clicking the **Enter Parameters** button is the model parameters view. 

.. figure:: ../../images/GUI_ScreenShots/Model_Params.PNG
   :align: center
   :width: 400
   :alt: Model Parameters View

   Model Parameters View

Start by defining a ``Simulation name`` for the model: the name also will be used as the 
name of the file containing details of the NRAP-Open-IAM simulation. Time should be entered in years. 
The ``End time`` is the total number of years that will be considered in simulation. 
The ``Time step`` entry specifies the uniform time step taken during the simulation 
(typically 1 year time steps are used). NRAP-Open-IAM can run simulations with three 
different analysis types: ``Forward``, ``LHS``, and ``Parstudy``. The ``Forward`` analysis 
type runs a single deterministic scenario, where parameter values are constant and the results 
will be the same each time the simulation is run. ``LHS`` (abbreviation for Latin Hypercube Sampling) 
is the analysis type used for stochastic simulations with random variations in parameter values. 
The ``Parstudy`` analysis type (short for parameter study analysis) evaluates a user-defined number of 
random values for each stochastic parameter. See section :ref:`conceptual_model_overview` for 
more details regarding these analysis types.

NRAP-Open-IAM creates a log file for each simulation: the level of information 
being logged can be set by the ``Logging`` entry. In general, the 
default level of ``Info`` would contain the most useful messages. A ``Debug`` 
(debugging) level of ``Logging`` will contain more information about component 
model connections, setup and calls, but will produce very large files and should be 
avoided for large simulations. ``Warning``, ``Error``, or ``Critical`` levels can be 
used if log file sizes become an issue; only more important messages will be logged, 
with the number of messages decreasing from ``Warning`` to ``Critical``.

NRAP-Open-IAM will save all the simulation results to the specified ``Output directory``. 
In the text field corresponding to ``Output directory``, the user can enter a path to the 
folder where the output will be saved. In the case the entered path does not exist, the 
empty directory will be created if the ``Generate output directory`` box is checked (it is 
checked by default). Additionally, if the provided path is not absolute, it is assumed 
that it starts in the NRAP-Open-IAM root folder. A ``{datetime}`` stamp can be included at 
the end of the output folder name - this stamp will automatically be replaced with the 
start time of the simulation (formatted as year-month-day-hour.minute.second). If the 
``Generate output directory`` box is checked, the ``{datetime}`` will automatically be 
included. If the output folder name does not include the ``{datetime}`` stamp and the 
output folder name was used before, results from the previous run will be overwritten. If 
an output directory is not specified (i.e., the ``Output directory`` input it not changed 
by the user), the default approach is to save results to a folder called 
``GUI_simulation_{datetime}``.

Stratigraphy Tab
----------------

After setting up the model parameters, proceed to the Stratigraphy tab.

.. figure:: ../../images/GUI_ScreenShots/Strat_Params.PNG
   :align: center
   :width: 400
   :alt: Stratigraphy View

   Stratigraphy View

In the Stratigraphy tab, model parameters related to the stratigraphy of the geologic carbon
storage site are defined. All coordinate systems are assumed to have units of meters. Many 
components have parameters that are tied to the stratigraphy component, so the input provided 
here will also impact those components. Model parameters for the stratigraphy component are 
defined by either assigning a fixed value or random distribution to vary over. For the ``LHS`` 
analysis type, parameters defined with a distribution will be sampled from that distribution. 
For the ``Forward`` analysis type, all parameters should be specified with a fixed value.

The number of shale unit is set with the ``Number of shale units`` entry. If the number of shale 
layers is ``N``, the number of aquifer layers is ``N - 1``. The shale and aquifer layers alternate, 
with shale 1 situated above the reservoir, aquifer 1 situated above shale 1, and shale 2 situated 
above aquifer 1. The unit numbers increase towards the surface. This configuration is demonstrated 
when the user clicks the ``Stratigraphy layers`` button on the Stratigraphy tab. When using a 
large number of layers, the GUI may encounter an issue where the scroll bar does not extend all the 
way to the bottom of the list of units. For example, the lowest unit shown may be aquifer 4, so the 
user cannot change the thicknesses of the reservoir and shale 1 through 4 and aquifers 1 through 3. 
When this issue occurs, however, the user can decrease the number of shale layers, set the thicknesses 
for the lower units, increase the number of shale layers, and then set the thicknesses for the higher 
units. Although the lower units may not be visible due to the scroll bar issue, the inputs that the 
user gave before increasing the number of shale layers will still be stored. To check that the 
input is set up correctly, the user can inspect the *.yaml* file that is saved when a GUI simulation 
is saved. The *.yaml* file can be opened with a text editor like Notepad, and the format of the 
*.yaml* file is the same as the control file interface of NRAP-Open-IAM (see section :ref:`control_file`).

See the :ref:`stratigraphy_component` section of this document for a list of all available 
parameters and their definitions. Although there are other types of stratigraphy components 
in NRAP-Open-IAM (e.g., ``LookupTableStratigraphy``), these other component types are not 
currently available in the GUI.

Adding Component Models
-----------------------

NRAP-Open-IAM is designed so that only the components of interest need to be included 
in the system model. Generally, a simulation will be built upwards starting from the deepest 
component (e.g., first reservoir, then wellbore, then aquifer, then atmosphere). 
To add a component, first give it a name (each component must have a unique name). 
Next, select the type of component model to be used. When adding subsequent components, 
a connection to existing components can be specified. For example, a wellbore component 
can be connected to a reservoir component, while an aquifer component can be connected 
to a wellbore component.

.. figure:: ../../images/GUI_ScreenShots/Add_Component1.PNG
   :align: center
   :width: 400
   :alt: Add Component View

   Adding a Component Model

.. figure:: ../../images/GUI_ScreenShots/Add_Component1a.PNG
   :align: center
   :width: 400
   :alt: Setup of Reservoir Component

   Setup of Reservoir Component

While a reservoir component can be given output locations under ``Observation locations``, 
it does not need to be given output locations if a wellbore component is connected to it. 
The wellbore component has its own locations specified under ``Wellbore locations``. Because 
the wellbore component has locations input, the connected reservoir component will also use 
those locations. Similarly, an aquifer component connected to a wellbore component will 
produce output for the locations specified on the wellbore component's tab.

Note that when using a ``LookupTableReservoir`` component, the component requires *.csv* files 
containing reservoir simulation results. For more details, see control file examples 6, 10, 
and 14. Furthermore, if a wellbore component is connected to a ``LookupTableReservoir`` component, 
the wellbore locations must be inside the domain covered by the lookup table files. If the 
``LookupTableReservoir`` is asked to interpolate results for a location outside of the domain 
in the data available to it, the component will raise an error with a message that describes 
this issue.

Each component model has component-specific input parameters and outputs. 
Parameters can be specified to be sampled from different distributions. 
If a parameter value is left unchanged from its initial ``Fixed Value`` 
setting, the default value shown will be used. When running a ``Forward`` 
model, parameters should only be specified as fixed values. When running 
a ``Parstudy`` simulation, the parameters meant to vary should be specified 
as having a uniform distribution and minimum and maximum values. For ``LHS`` 
simulations, any distribution can be specified.

If a component output is not checked, it will not be saved to a *.csv* file 
and it cannot be plotted during the post-processing step described further below.

Parameter and output definitions can be found in the chapter :ref:`components_description`.

.. figure:: ../../images/GUI_ScreenShots/Add_Component2.PNG
   :align: center
   :width: 400
   :alt: Add Second Component View

   Adding Second Component

.. figure:: ../../images/GUI_ScreenShots/Add_Component2a.PNG
   :align: center
   :width: 400
   :alt: Setup of Wellbore Component

   Setup of Wellbore Component

When using a component that generally needs input from another component but that
component is not to be part of the model (i.e., using a wellbore model without a reservoir model),
dynamic parameters can be used for component model input. For dynamic parameters, a value
must be specified for each time step in the simulation. Values can be entered
manually (separated by a comma) or by providing the path to a file containing the data.
Some components require specification of which layer in the stratigraphy they
represent (such as an aquifer model).

.. figure:: ../../images/GUI_ScreenShots/Add_Component3.PNG
   :align: center
   :width: 400
   :alt: Add Component View 3

   Adding a Component Model with Connection and a Stratigraphy Selection

After one component has been added to the system model, more components can 
can be added. When all required components have been included, save the model and 
return to the dashboard. The system model can then be run using 
the **RUN SIMULATION** button on the main dashboard.

GUI Setup Examples
------------------

In the folder *examples*, there is a subfolder called *GUI_Files* with example simulation 
files that can be loaded into the GUI and run by NRAP-Open-IAM. To run one 
of the provided examples, select **Load Simulation** on the main dashboard of the GUI. 
In the file browser that appears, navigate to the *GUI_Files* subfolder of the 
*examples* folder and select the first example file *01_Forward_AR_CW.OpenIAM*. 
This example runs a simple forward model with an ``AnalyticalReservoir`` component 
providing inputs to a ``CementedWellbore`` component.

When the file is loaded into the GUI, the parameters of the simulation 
can be inspected. After the simulation is complete, the user can proceed to the 
post-processing step (by clicking **Post Processing** on the main dashboard of the GUI) 
to visualize and, for some scenarios, analyze the obtained results. The Post Processing 
tab has a folder selection button with which the user can select the folder 
containing simulation results. Note that the selection of the folder (and loading of results) 
might fail if the simulation did not finish successfully. In this case, it is recommended 
to check the file *IAM_log.log* within the output folder containing useful (debug, info, 
warning or error) messages produced during the simulation. A text editor like Notepad can 
be used to open the *IAM_log.log* file. The file name of each GUI example is 
made to show the components and analyses demonstrated by the example (e.g., ``AR`` for 
``AnalyticalReservoir`` and ``OW`` for ``OpenWellbore``).

The second example file *02_LHS_AR_MSW.OpenIAM* is a stochastic simulation for a 
system model containing ``AnalyticalReservoir`` and ``MultisegmentedWellbore`` 
components. The example illustrates the use of the Latin Hypercube Sampling analysis type, 
with parameter values varying across 30 different realizations.

The third example file *03_LHS_LUT_MSW.OpenIAM* illustrates the use of 
``LookupTableReservoir`` and ``MultisegmentedWellbore`` components. The data 
set used for the ``LookupTableReservoir`` component is based on a simulation made 
for the Kimberlina oil field (:cite:`DAA2018`).

The fourth example file *04_LHS_DP_MSW.OpenIAM* illustrates Latin Hypercube 
Sampling analysis type applied to a ``MultisegmentedWellbore`` component. The pressure 
and |CO2| saturation required as inputs of the component are provided in the 
form of arrays. This form of input arguments is called dynamic parameters (``DP``; 
i.e., component inputs that change over time).

The fifth example file *05_LHS_AR_OW_CA.OpenIAM* illustrates the application of
three component models: ``AnalyticalReservoir``, ``OpenWellbore`` and ``CarbonateAquifer``
components. The example uses the ``LHS`` analysis type to estimate the reservoir's
response to |CO2| injection, the leakage of fluids through a wellbore, and the impact
of this leakage on an aquifer overlying the storage reservoir.

GUI Workflows
-------------

The **Use Workflow** button on the main dashboard of the GUI allows the user 
to use a workflow that effectively builds a system model from a blueprint. There 
are different types of workflows for specific analyses, like area of review 
(``AoR``) and time to first detection (``TTFD``). Furthermore, the workflow 
will automatically create figures in the specified output directory, without 
the user using the **Post Processing** functionality on the main dashboard 
of the GUI.

The workflow allows the user to select a specific component type for each 
"slot" in a chain of connected components (i.e., reservoir, wellbore, and 
aquifer components). The user can then change parameter values for each component 
and change factors like wellbore locations. Aspects of the system model are then 
set up automatically by the workflow (e.g., component connections, output types, 
and plot setup). For example, if the user selects an ``AoR`` workflow using 
``LookupTableReservoir``, ``OpenWellbore``, and ``GenericAquifer`` components, 
the user would not have to specify that the reservoir component should produce 
**pressure** and **CO2saturation** outputs. Those outputs are required for 
the wellbore component to produce brine and |CO2| leakage rates, so they are 
added automatically. Similarly, the brine and |CO2| leakage rates are added 
so that the aquifer component can calculate plume volumes, which are also 
added automatically.

The ``AoR`` workflow distributes wells across the study area in a grid pattern 
- these wells are only hypothetical, and meant to assess what the impact would 
be if a well was located at each grid position. Considering the impacts that could 
happen is meant to inform an area of review delineation. The ``AoR`` workflow will 
highlight the pH and TDS plume volumes that could develop in an aquifer, but it 
will also highlight the reservoir pressures and |CO2| saturations across the 
grid used. The user can specify a critical pressure in |MPa|, or allow the 
workflow to calculate the critical pressure for a particular aquifer given the 
selected aquifer's depth, the reservoir's depth, and a brine density (see 
section :ref:`equations`). This critical pressure is the pressure required to 
drive fluid from the reservoir and into the aquifer along an open conduit, and 
basing an area of review on this consideration is consistent with EPA 
regulation for an under-pressurized reservoir (:cite:`USEPA2013`).

The ``TTFD`` workflow takes well locations and examines how the leakage would 
cause aquifer plumes to spread over space and time. By specifying monitoring 
well locations at specific coordinates (``x``, ``y``, and ``z``), the workflow 
will highlight when the aquifer plume first reaches each coordinate and therefore 
when the monitoring well could have provided warning of the plume (i.e., the 
time to first detection).

When using a workflow in the GUI, the Model and Stratigraphy tabs are used the same 
way as in a normal simulation. There is another tab called "Add Workflow." On that 
tab, the user can select which type of workflow to use (``Workflow type``), the 
aquifer to examine in the workflow (``Aquifer Layer``), and the component types 
for the reservoir, wellbore, and aquifer components.

.. figure:: ../../images/GUI_ScreenShots/Add_Workflow.PNG
   :align: center
   :width: 400
   :alt: Add Workflow View

   Adding a Workflow

Once all of the workflow options have been selected, click the ``Add Workflow`` button. 
Do not save a workflow simulation without first adding a workflow. Furthermore, once a 
workflow has been added, the stratigraphy tab is locked; the user should therefore finalize 
the stratigraphy parameters before adding a workflow. Once the ``Add Workflow`` button 
is selected, the workflow will automatically add the tabs for the reservoir, wellbore, 
and aquifer components as well as a tab for the workflow itself. Because the workflow handles 
the component outputs, component connection, and certain parameters, the user will not 
be able to change those options on the component tabs. For example, the ``Aquifer name`` 
on the aquifer component's tab will be set to the ``Aquifer Layer`` that was selected when 
adding the workflow.

On the ``Workflow`` tab, the user can specify wellbore locations other options specific to 
the type of workflow being used. The options that apply to multiple workflows include the 
resolution of the figures produced (``Figure Resolution [DPI]``) and whether the injection 
sites will be displayed in the figures (``Plot Injection Sites``). When using a 
``LookupTableReservoir`` component, the ``Workflow`` tab will also have entries for 
the ``x`` and ``y`` coordinates for the injection site(s) (``Injection well location(s)``). 
The coordinates must be given by the user for the injection sites to be shown when using 
a ``LookupTableReservoir``.

The well placement options on the ``Workflow`` tab depend on the workflow type. 
For example, the ``AoR`` workflow places wellbores across the domain in a rectangular 
grid pattern. On the ``AoR`` workflow tab, the user specifies the range of ``x`` 
and ``y`` values for the grid (``x-min``, ``x-max``, ``y-min``, and ``y-max``) as well 
as the number of rows and columns for the grid (``y size`` and ``x size``, respectively).

.. figure:: ../../images/GUI_ScreenShots/AoR_Workflow.PNG
   :align: center
   :width: 400
   :alt: AoR Workflow View

   Area of Review Workflow

When using the ``TTFD`` workflow, the ``x`` and ``y`` coordinates for wellbores are 
entered as comma separated lists (e.g., "1000, 2000, 3000" for 1 |km|, 2 |km|, and 
3 |km|). A specific type of plume must also be selected on the ``TTFD`` tab 
(``Aquifer Plume Type``). Furthermore, the ``TTFD`` workflow has ``x``, ``y``, and 
``z`` (depth) coordinates (|m|) for monitoring locations. These monitoring locations are 
meant to represent monitoring wells, and multiple well coordinates can be entered as 
comma separated lists. The number of ``x``, ``y``, and ``z`` values should be the 
same. If the aquifer plume type selected reaches one of the monitoring locations, then 
that monitoring well is considered to have detected the plume. The ``TTFD`` workflow 
tab also has fields for inputs called horizontal and vertical windows. If the plume 
comes within a horizontal distance (horizontal window, in |m|) and vertical distance 
(vertical window, in |m|) of the ``x``, ``y``, and ``z`` values of a monitoring well, 
the plume will be detected. Therefore, if the ``z`` value is given in the middle of 
the aquifer being considered and the vertical window is half of the aquifer's thickness, 
the monitoring well will detect any plumes across the thickness of the aquifer at 
that location (the ``x`` and ``y`` values provided).

.. figure:: ../../images/GUI_ScreenShots/TTFD_Workflow.PNG
   :align: center
   :width: 400
   :alt: TTFD Workflow View

   Time to First Detection Workflow

To remove the workflow options entered, click the **Remove Workflow** button on the 
``Workflow`` tab. Doing so will remove the tabs for the components and the workflow, 
allowing the user to restart the selection of workflow options.

When all options have been selected, save the model and return to the dashboard. The 
system model can then be run using the **RUN SIMULATION** button on the main dashboard. 
When the simulation finishes, the workflow will automatically create figures in the 
output directory. For example, the ``AoR`` workflow will create map-view images 
highlighting the potential impacts across the domain (i.e., the well grid locations).

For more information about workflows, see the section :ref:`workflow`.

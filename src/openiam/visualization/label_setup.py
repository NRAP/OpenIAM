# -*- coding: utf-8 -*-
"""
Module containing dictionaries defining default titles, y-axis and legend labels
for time series plots created.
"""
additional_keys = [
    'brine_aquifer{}'.format(unitRef) for unitRef in range(1, 30)
    ] + ['CO2_aquifer{}'.format(unitRef) for unitRef in range(1, 30)
         ] + ['mass_brine_aquifer{}'.format(unitRef) for unitRef in range(1, 30)
              ] + ['mass_CO2_aquifer{}'.format(unitRef) for unitRef in range(1, 30)]
      
additional_keys_leakage_rates = [
    'brine_aquifer{}'.format(unitRef) for unitRef in range(1, 30)
    ] + ['CO2_aquifer{}'.format(unitRef) for unitRef in range(1, 30)]

additional_keys_leaked_masses = [
    'mass_brine_aquifer{}'.format(unitRef) for unitRef in range(1, 30)
    ] + ['mass_CO2_aquifer{}'.format(unitRef) for unitRef in range(1, 30)]

additional_ylabels = [
    'Brine leakage rate to aquifer {} (kg/s)'.format(unitRef) for unitRef in range(1, 30)
    ] + ['CO$_2$ leakage rate to aquifer {} (kg/s)'.format(unitRef) for unitRef in range(1, 30)
         ] + ['Brine mass leaked to aquifer {} (kg)'.format(unitRef) for unitRef in range(1, 30)
              ] + ['CO$_2$ mass leaked to aquifer {} (kg)'.format(unitRef) for unitRef in range(1, 30)]

additional_ylabels_2rows = [
    'Brine leakage rate\nto aquifer {} (kg/s)'.format(unitRef) for unitRef in range(1, 30)
    ] + ['CO$_2$ leakage rate\nto aquifer {} (kg/s)'.format(unitRef) for unitRef in range(1, 30)
         ] + ['Brine mass leaked\nto aquifer {} (kg)'.format(unitRef) for unitRef in range(1, 30)
              ] + ['CO$_2$ mass leaked\nto aquifer {} (kg)'.format(unitRef) for unitRef in range(1, 30)]

additional_titles = [
    'Brine Leakage Rate to Aquifer {}'.format(unitRef) for unitRef in range(1, 30)
    ] + ['CO$_2$ Leakage Rate to Aquifer {}'.format(unitRef) for unitRef in range(1, 30)
         ] + ['Brine Mass Leaked to Aquifer {}'.format(unitRef) for unitRef in range(1, 30)
              ] + ['CO$_2$ Mass Leaked to Aquifer {}'.format(unitRef) for unitRef in range(1, 30)]

additional_legends = [
    'brine leakage rate to aquifer {}'.format(unitRef) for unitRef in range(1, 30)
    ] + ['CO$_2$ leakage rate to aquifer {}'.format(unitRef) for unitRef in range(1, 30)
         ] + ['brine mass leaked to aquifer {}'.format(unitRef) for unitRef in range(1, 30)
              ] + ['CO$_2$ mass leaked to aquifer {}'.format(unitRef) for unitRef in range(1, 30)]

additional_units = [
    'kg/s' for unitRef in range(1, 30)
    ] + ['kg/s' for unitRef in range(1, 30)
         ] + ['kg' for unitRef in range(1, 30)
              ] + ['kg' for unitRef in range(1, 30)]

additional_keys_SALSA = []
additional_ylabels_SALSA = []
additional_ylabels_2rows_SALSA = []
additional_titles_SALSA = []
additional_units_SALSA = []
additional_legends_SALSA = []

interfaces = ['Bottom', 'Top']
metrics = ['Rate', 'Volume']
metric_units = {'Rate': 'm$^3$/s', 'Volume': 'm$^3$'}

for unitRef in range(30):
    
    for metric in metrics:
        # Diffuse leakage rates and vlumes
        for interface in interfaces:
            label = 'diffuseLeakage{}{}Aquifer{}'.format(
                metric, interface, unitRef + 1)
            unit = metric_units[metric]
            ylabel = 'Diffuse Leakage {}, {} of Aquifer {} ({})'.format(
                metric, interface, unitRef + 1, unit)
            ylabel_2rows = 'Diffuse Leakage {},\n{} of Aquifer {} ({})'.format(
                metric, interface, unitRef + 1, unit)
            title = 'Diffuse Leakage {}, {} of Aquifer {}'.format(
                metric, interface, unitRef + 1)
            lgnd = 'diffuse leakage {}, {} of aquifer {}'.format(
                metric.lower(), interface.lower(), unitRef + 1)
            
            additional_keys_SALSA.append(label)
            additional_ylabels_SALSA.append(ylabel)
            additional_ylabels_2rows_SALSA.append(ylabel_2rows)
            additional_titles_SALSA.append(title)
            additional_units_SALSA.append(unit)
            additional_legends_SALSA.append(lgnd)
        
        # Total focused leakage rates and volumes from all wells
        label = 'wellLeakage{}Aquifer{}'.format(
            metric, unitRef + 1)
        unit = metric_units[metric]
        ylabel = 'Leakage {} to Aquifer {} from All Wells ({})'.format(
            metric, unitRef + 1, unit)
        ylabel_2rows = 'Leakage {} to Aquifer {}\nfrom All Wells ({})'.format(
            metric, unitRef + 1, unit)
        title = 'Total Well Leakage {} to Aquifer {}'.format(
            metric, unitRef + 1)
        lgnd = 'total well leakage {} to aquifer {}'.format(
            metric.lower(), unitRef + 1)
        
        additional_keys_SALSA.append(label)
        additional_ylabels_SALSA.append(ylabel)
        additional_ylabels_2rows_SALSA.append(ylabel_2rows)
        additional_titles_SALSA.append(title)
        additional_units_SALSA.append(unit)
        additional_legends_SALSA.append(lgnd)
    
    for locRef in range(1000):
        # Aquifer hydraulic head
        label = 'headLoc{}Aquifer{}'.format(locRef + 1, unitRef + 1)
        ylabel = 'Aquifer {} Hydraulic Head at Location {} (m)'.format(
            unitRef + 1, locRef + 1)
        ylabel_2rows = 'Aquifer {} Hydraulic\nHead at Location {} (m)'.format(
            unitRef + 1, locRef + 1)
        title = 'Aquifer {} Hydraulic Head at Location {}'.format(
            unitRef + 1, locRef + 1)
        lgnd = 'aquifer {} hydraulic head at location {}'.format(
            unitRef + 1, locRef + 1)
        unit = 'm'
        
        additional_keys_SALSA.append(label)
        additional_ylabels_SALSA.append(ylabel)
        additional_ylabels_2rows_SALSA.append(ylabel_2rows)
        additional_titles_SALSA.append(title)
        additional_units_SALSA.append(unit)
        additional_legends_SALSA.append(lgnd)
        
        # Aquifer pressure
        label = 'pressureLoc{}Aquifer{}'.format(locRef + 1, unitRef + 1)
        ylabel = 'Aquifer {} Pressure at Location {}, Bottom of Unit (Pa)'.format(
            unitRef + 1, locRef + 1)
        ylabel_2rows = 'Aquifer {} Pressure at Location {},\nBottom of Unit (Pa)'.format(
            unitRef + 1, locRef + 1)
        title = 'Aquifer {} Pressure at Location {}, Bottom of Unit'.format(
            unitRef + 1, locRef + 1)
        lgnd = 'aquifer {} pressure at location {}, bottom of unit'.format(
            unitRef + 1, locRef + 1)
        unit = 'Pa'
        
        additional_keys_SALSA.append(label)
        additional_ylabels_SALSA.append(ylabel)
        additional_ylabels_2rows_SALSA.append(ylabel_2rows)
        additional_titles_SALSA.append(title)
        additional_units_SALSA.append(unit)
        additional_legends_SALSA.append(lgnd)
        
        label = 'pressureLoc{}MidAquifer{}'.format(locRef + 1, unitRef + 1)
        ylabel = 'Aquifer {} Pressure at Location {}, Middle of Unit (Pa)'.format(
            unitRef + 1, locRef + 1)
        ylabel_2rows = 'Aquifer {} Pressure at Location {},\nMiddle of Unit (Pa)'.format(
            unitRef + 1, locRef + 1)
        title = 'Aquifer {} Pressure at Location {}, Middle of Unit'.format(
            unitRef + 1, locRef + 1)
        lgnd = 'aquifer {} pressure at location {}, middle of unit'.format(
            unitRef + 1, locRef + 1)
        unit = 'Pa'
        
        additional_keys_SALSA.append(label)
        additional_ylabels_SALSA.append(ylabel)
        additional_ylabels_2rows_SALSA.append(ylabel_2rows)
        additional_titles_SALSA.append(title)
        additional_units_SALSA.append(unit)
        additional_legends_SALSA.append(lgnd)
        
        label = 'pressureLoc{}TopAquifer{}'.format(locRef + 1, unitRef + 1)
        ylabel = 'Aquifer {} Pressure at Location {}, Top of Unit (Pa)'.format(
            unitRef + 1, locRef + 1)
        ylabel_2rows = 'Aquifer {} Pressure at Location {},\nTop of Unit (Pa)'.format(
            unitRef + 1, locRef + 1)
        title = 'Aquifer {} Pressure at Location {}, Top of Unit'.format(
            unitRef + 1, locRef + 1)
        lgnd = 'aquifer {} pressure at location {}, top of unit'.format(
            unitRef + 1, locRef + 1)
        unit = 'Pa'
        
        additional_keys_SALSA.append(label)
        additional_ylabels_SALSA.append(ylabel)
        additional_ylabels_2rows_SALSA.append(ylabel_2rows)
        additional_titles_SALSA.append(title)
        additional_units_SALSA.append(unit)
        additional_legends_SALSA.append(lgnd)
        
        for pointRef in range(100):
            # Shale hydraulic head
            label = 'headProfile{}VertPoint{}Shale{}'.format(
                locRef + 1, pointRef + 1, unitRef + 1)
            ylabel = 'Hydraulic Head at Point {} within Shale {}, Profile {} (m)'.format(
                pointRef + 1, unitRef + 1, locRef + 1)
            ylabel_2rows = 'Hydraulic Head at Point {}\nwithin Shale {}, Profile {} (m)'.format(
                pointRef + 1, unitRef + 1, locRef + 1)
            title = 'Hydraulic Head at Point {} within Shale {}, Profile {}'.format(
                pointRef + 1, unitRef + 1, locRef + 1)
            lgnd = 'hydraulic head at point {} within shale {}, profile {}'.format(
                pointRef + 1, unitRef + 1, locRef + 1)
            unit = 'm'
            
            additional_keys_SALSA.append(label)
            additional_ylabels_SALSA.append(ylabel)
            additional_ylabels_2rows_SALSA.append(ylabel_2rows)
            additional_titles_SALSA.append(title)
            additional_units_SALSA.append(unit)
            additional_legends_SALSA.append(lgnd)
            
            # Shale pressure
            label = 'pressureProfile{}VertPoint{}Shale{}'.format(
                locRef + 1, pointRef + 1, unitRef + 1)
            ylabel = 'Pressure at Point {} within Shale {}, Profile {} (Pa)'.format(
                pointRef + 1, unitRef + 1, locRef + 1)
            ylabel_2rows = 'Pressure at Point {}\nwithin Shale {}, Profile {} (Pa)'.format(
                pointRef + 1, unitRef + 1, locRef + 1)
            title = 'Pressure at Point {} within Shale {}, Profile {}'.format(
                pointRef + 1, unitRef + 1, locRef + 1)
            lgnd = 'pressure at point {} within shale {}, profile {}'.format(
                pointRef + 1, unitRef + 1, locRef + 1)
            unit = 'Pa'
            
            additional_keys_SALSA.append(label)
            additional_ylabels_SALSA.append(ylabel)
            additional_ylabels_2rows_SALSA.append(ylabel_2rows)
            additional_titles_SALSA.append(title)
            additional_units_SALSA.append(unit)
            additional_legends_SALSA.append(lgnd)
        
        # Individual well leakage rates and volumes
        for metric in metrics:
            label = 'well{}Leakage{}Aquifer{}'.format(
                locRef + 1, metric, unitRef + 1)
            unit = metric_units[metric]
            ylabel = 'Well {} Leakage {} to Aquifer {} ({})'.format(
                locRef + 1, metric, unitRef + 1, unit)
            ylabel_2rows = 'Well {} Leakage {}\nto Aquifer {} ({})'.format(
                locRef + 1, metric, unitRef + 1, unit)
            title = 'Well {} Leakage {} to Aquifer {}'.format(
                locRef + 1, metric, unitRef + 1)
            lgnd = 'well {} leakage {} to aquifer {}'.format(
                locRef + 1, metric.lower(), unitRef + 1)
            
            additional_keys_SALSA.append(label)
            additional_ylabels_SALSA.append(ylabel)
            additional_ylabels_2rows_SALSA.append(ylabel_2rows)
            additional_titles_SALSA.append(title)
            additional_units_SALSA.append(unit)
            additional_legends_SALSA.append(lgnd)

# Dictionary with the y-label names for different component output. Because single
# plots can display results for multiple aquifers, I have removed the aquifer numbers here.
Y_LABEL_DICT = {
    # Reservoir components
    'pressure': 'Reservoir pressure at location (Pa)',
    'CO2saturation': 'Reservoir CO$_2$ saturation at location [-]',
    'mass_CO2_reservoir': 'Mass of CO$_2$ in the reservoir (kg)',
    # Wellbore and adapter components
    'CO2_aquifer': 'Leakage rate of CO$_2$ to aquifer (kg/s)',
    'CO2_atm': 'Leakage rate of CO$_2$ to atmosphere (kg/s)',
    'brine_aquifer': 'Leakage rate of brine to aquifer (kg/s)',
    'brine_atm': 'Leakage rate of brine to atmosphere (kg/s)',
    'mass_CO2_aquifer': 'Mass of CO$_2$ leaked to aquifer (kg)',
    'mass_brine_aquifer': 'Mass of brine leaked to aquifer (kg)',
    'mass_CO2_atm': 'Mass of CO$_2$ leaked to atmosphere (kg)',
    'mass_brine_atm': 'Mass of brine leaked to atmosphere (kg)',
    'mass_CO2_gas_aquifer': 'Mass of CO$_2$ gas leaked to aquifer (kg)',
    'mass_methane_oil_aquifer': 'Mass of CH$_4$ oil leaked to aquifer (kg)',
    'mass_methane_gas_aquifer': 'Mass of CH$_4$ gas leaked to aquifer (kg)',
    'mass_oil_aquifer': 'Total mass of oil leaked to aquifer (kg)',
    'mass_gas_aquifer': 'Total mass of gas leaked to aquifer (kg)',
    # Fault flow and Seal horizon components
    'CO2_aquifer_total': 'Cumulative leakage rate of CO$_2$ to aquifer (kg/s)',
    'brine_aquifer_total': 'Cumulative leakage rate of brine to aquifer (kg/s)',
    'mass_CO2_aquifer_total': 'Cumulative mass of CO$_2$ leaked to aquifer per timestep (kg)',
    'mass_brine_aquifer_total': 'Cumulative mass of brine leaked to aquifer per timestep (kg)',
    # Aquifer components
    'Benzene_volume': 'Volume of plume above benzene threshold (m$^3$)',
    'Naphthalene_volume': 'Volume of plume above naphthalene threshold (m$^3$)',
    'Phenol_volume': 'Volume of plume above phenol threshold (m$^3$)',
    'As_volume': 'Volume of plume above arsenic threshold (m$^3$)',
    'Pb_volume': 'Volume of plume above lead threshold (m$^3$)',
    'Cd_volume': 'Volume of plume above cadmium threshold (m$^3$)',
    'Ba_volume': 'Volume of plume above barium threshold (m$^3$)',
    'Flux': 'CO$_2$ leakage rate to atmosphere (kg/s)',
    # FutureGen2 aquifer components
    'TDS_volume': 'Volume of plume above TDS threshold (m$^3$)',
    'TDS_dx': 'Length of plume above TDS threshold (m)',
    'TDS_dy': 'Width of plume above TDS threshold (m)',
    'TDS_dz': 'Height of plume above TDS threshold (m)',
    'pH_volume': 'Volume of plume below pH threshold (m$^3$)',
    'pH_dx': 'Length of plume below pH threshold (m)',
    'pH_dy': 'Width of plume below pH threshold (m)',
    'pH_dz': 'Height of plume below pH threshold (m)',
    'Pressure_volume': 'Volume of plume above baseline pressure change (m$^3$)',
    'Pressure_dx': 'Length of plume above baseline pressure change (m)',
    'Pressure_dy': 'Width of plume above baseline pressure change (m)',
    'Pressure_dz': 'Height of plume above baseline pressure change (m)',
    'Dissolved_CO2_volume': 'Volume of plume above baseline dissolved CO$_2$ (m$^3$)',
    'Dissolved_CO2_dx': 'Length of plume above baseline dissolved CO$_2$ (m)',
    'Dissolved_CO2_dy': 'Width of plume above baseline dissolved CO$_2$ (m)',
    'Dissolved_CO2_dz': 'Height of plume above baseline dissolved CO$_2$ (m)',
    'Temperature_volume': 'Volume of plume above baseline temperature change (m$^3$)',
    'Temperature_dx': 'Length of plume above baseline temperature change (m)',
    'Temperature_dy': 'Width of plume above baseline temperature change (m)',
    'Temperature_dz': 'Height of plume above baseline temperature change (m)',
    # Generic aquifer component
    'Dissolved_salt_volume': 'Volume of plume above baseline salt mass fraction (m$^3$)',
    'Dissolved_salt_dr': 'Radius of plume above baseline salt mass fraction (m)',
    'Dissolved_salt_dz': 'Height of plume above baseline salt mass fraction (m)',
    'Dissolved_salt_mass_fraction': 'Mass fraction of salt in aquifer [-]',
    'Dissolved_CO2_dr': 'Radius of plume above baseline dissolved CO$_2$ (m)',
    'Dissolved_CO2_dz': 'Height of plume above baseline dissolved CO$_2$ (m)',
    'Dissolved_CO2_mass_fraction': 'Mass fraction of CO$_2$ in aquifer [-]',
    # Plume stability component
    'pressure_areas': 'Pressure plume area (m$^2$)',
    'pressure_areas_dt': 'Change in pressure plume area (m$^2$/year)',
    'pressure_mobility': 'Velocity of pressure plume centroid (m/year)',
    'pressure_mobility_angles': 'Direction of pressure plume centroid [-]',
    'pressure_spreading': 'Dispersion of pressure plume (m$^2$/year)',
    'pressure_spreading_angles': 'Direction of pressure plume dispersion [-]',
    'CO2saturation_areas': 'CO$_2$ saturation plume area (m$^2$)',
    'CO2saturation_areas_dt': 'Change in CO$_2$ saturation plume area (m$^2$/year)',
    'CO2saturation_mobility': 'Velocity of CO$_2$ saturation plume centroid (m/year)',
    'CO2saturation_mobility_angles': 'Direction of CO$_2$ saturation plume centroid [-]',
    'CO2saturation_spreading': 'Dispersion of CO$_2$ saturation plume (m$^2$/year)',
    'CO2saturation_spreading_angles': 'Direction of CO$_2$ saturation plume dispersion [-]',
    }

for (key, val) in zip(additional_keys, additional_ylabels):
    Y_LABEL_DICT[key] = val

for (key, val) in zip(additional_keys_SALSA, additional_ylabels_SALSA):
    Y_LABEL_DICT[key] = val

Y_LABEL_SPLIT_DICT = {
    'CO': Y_LABEL_DICT['CO2_aquifer'],
    'brine_aquifer': Y_LABEL_DICT['brine_aquifer'],
    'brine_aquifer_cell_': Y_LABEL_DICT['brine_aquifer'],
    'brine_aquifer_segm_': Y_LABEL_DICT['brine_aquifer'],
    'mass_CO': Y_LABEL_DICT['mass_CO2_aquifer'],
    'mass_brine_aquifer': Y_LABEL_DICT['mass_brine_aquifer'],
    'mass_brine_aquifer_cell_': Y_LABEL_DICT['mass_brine_aquifer'],
    'mass_brine_aquifer_segm_': Y_LABEL_DICT['mass_brine_aquifer'],
    'Dissolved_CO': Y_LABEL_DICT['Dissolved_CO2_mass_fraction'],
    'Dissolved_salt_mass_fraction_coord_': Y_LABEL_DICT['Dissolved_salt_mass_fraction']}

# These ylabels are used if the one row case is too long on the figure. Because single
# plots can display results for multiple aquifers, I have removed the aquifer numbers here.
Y_LABEL_2ROWS_DICT = {
    # Reservoir components
    'pressure': 'Reservoir pressure\nat location (Pa)',
    'CO2saturation': 'Reservoir CO$_2$\nsaturation at location [-]',
    'mass_CO2_reservoir': 'Mass of CO$_2$\nin the reservoir (kg)',
    # Wellbore and adapter components
    'CO2_aquifer': 'Leakage rate of\nCO$_2$ to aquifer (kg/s)',
    'CO2_atm': 'Leakage rate of\nCO$_2$ to atmosphere (kg/s)',
    'brine_aquifer': 'Leakage rate of\nbrine to aquifer (kg/s)',
    'brine_atm': 'Leakage rate of\nbrine to atmosphere (kg/s)',
    'mass_CO2_aquifer': 'Mass of CO$_2$\nleaked to aquifer (kg)',
    'mass_brine_aquifer': 'Mass of brine\nleaked to aquifer (kg)',
    'mass_CO2_atm': 'Mass of CO$_2$\nleaked to atmosphere (kg)',
    'mass_brine_atm': 'Mass of brine\nleaked to atmosphere (kg)',
    'mass_CO2_gas_aquifer': 'Mass of CO$_2$ gas\nleaked to aquifer (kg)',
    'mass_methane_oil_aquifer': 'Mass of CH$_4$ oil\nleaked to aquifer (kg)',
    'mass_methane_gas_aquifer': 'Mass of CH$_4$ gas\nleaked to aquifer (kg)',
    'mass_oil_aquifer': 'Total mass of oil\nleaked to aquifer (kg)',
    'mass_gas_aquifer': 'Total mass of gas\nleaked to aquifer (kg)',
    # Fault flow and Seal horizon components
    'CO2_aquifer_total': 'Cumulative leakage rate\nof CO$_2$ to aquifer (kg/s)',
    'brine_aquifer_total': 'Cumulative leakage rate\nof brine to aquifer (kg/s)',
    'mass_CO2_aquifer_total': 'Cumulative mass of\nCO$_2$ leaked to aquifer (kg)',
    'mass_brine_aquifer_total': 'Cumulative mass of\nbrine leaked to aquifer (kg)',
    # Aquifer components
    'Benzene_volume': 'Volume of plume\nabove benzene threshold (m$^3$)',
    'Naphthalene_volume': 'Volume of plume\nabove naphthalene threshold (m$^3$)',
    'Phenol_volume': 'Volume of plume\nabove phenol threshold (m$^3$)',
    'As_volume': 'Volume of plume\nabove arsenic threshold (m$^3$)',
    'Pb_volume': 'Volume of plume\nabove lead threshold (m$^3$)',
    'Cd_volume': 'Volume of plume\nabove cadmium threshold (m$^3$)',
    'Ba_volume': 'Volume of plume\nabove barium threshold (m$^3$)',
    'Flux': 'CO$_2$ leakage rate\nto atmosphere (kg/s)',
    # FutureGen2 aquifer components
    'TDS_volume': 'Volume of plume\nabove TDS threshold (m$^3$)',
    'TDS_dx': 'Length of plume\nabove TDS threshold (m)',
    'TDS_dy': 'Width of plume\nabove TDS threshold (m)',
    'TDS_dz': 'Height of plume\nabove TDS threshold (m)',
    'pH_volume': 'Volume of plume\nnbelow pH threshold (m$^3$)',
    'pH_dx': 'Length of plume\nbelow pH threshold (m)',
    'pH_dy': 'Width of plume\nbelow pH threshold (m)',
    'pH_dz': 'Height of plume\nbelow pH threshold (m)',
    'Pressure_volume': 'Volume of plume above\nbaseline pressure change (m$^3$)',
    'Pressure_dx': 'Length of plume above\nbaseline pressure change (m)',
    'Pressure_dy': 'Width of plume above\nbaseline pressure change (m)',
    'Pressure_dz': 'Height of plume above\nbaseline pressure change (m)',
    'Dissolved_CO2_volume': 'Volume of plume above\nbaseline dissolved CO$_2$ (m$^3$)',
    'Dissolved_CO2_dx': 'Length of plume above\nbaseline dissolved CO$_2$ (m)',
    'Dissolved_CO2_dy': 'Width of plume above\nbaseline dissolved CO$_2$ (m)',
    'Dissolved_CO2_dz': 'Height of plume above\nbaseline dissolved CO$_2$ (m)',
    'Temperature_volume': 'Volume of plume above\nbaseline temperature change (m$^3$)',
    'Temperature_dx': 'Length of plume above\nbaseline temperature change (m)',
    'Temperature_dy': 'Width of plume above\nbaseline temperature change (m)',
    'Temperature_dz': 'Height of plume above\nbaseline temperature change (m)',
    # Generic aquifer component
    'Dissolved_salt_volume': 'Volume of plume above\nbaseline salt mass fraction (m$^3$)',
    'Dissolved_salt_dr': 'Radius of plume above\nbaseline salt mass fraction (m)',
    'Dissolved_salt_dz': 'Height of plume above\nbaseline salt mass fraction (m)',
    'Dissolved_salt_mass_fraction': 'Mass fraction of\nsalt in aquifer [-]',
    'Dissolved_CO2_dr': 'Radius of plume above\nbaseline dissolved CO$_2$ (m)',
    'Dissolved_CO2_dz': 'Height of plume above\nbaseline dissolved CO$_2$ (m)',
    'Dissolved_CO2_mass_fraction': 'Mass fraction of\nCO$_2$ in aquifer [-]',
    # Plume stability component
    'pressure_areas': 'Pressure\nplume area (m$^2$)',
    'pressure_areas_dt': 'Change in pressure\nplume area (m$^2$/year)',
    'pressure_mobility': 'Velocity of pressure\nplume centroid (m/year)',
    'pressure_mobility_angles': 'Direction of pressure\nplume centroid [-]',
    'pressure_spreading': 'Dispersion of\npressure plume (m$^2$/year)',
    'pressure_spreading_angles': 'Direction of pressure\nplume dispersion [-]',
    'CO2saturation_areas': 'CO$_2$ saturation\nplume area (m$^2$)',
    'CO2saturation_areas_dt': 'Change in CO$_2$\nsaturation plume area (m$^2$/year)',
    'CO2saturation_mobility': 'Velocity of CO$_2$\nsaturation plume centroid (m/year)',
    'CO2saturation_mobility_angles': 'Direction of CO$_2$\nsaturation plume centroid [-]',
    'CO2saturation_spreading': 'Dispersion of CO$_2$\nsaturation plume (m$^2$/year)',
    'CO2saturation_spreading_angles': 'Direction of CO$_2$\nsaturation plume dispersion [-]',
    }

for (key, val) in zip(additional_keys, additional_ylabels_2rows):
    Y_LABEL_2ROWS_DICT[key] = val

for (key, val) in zip(additional_keys_SALSA, additional_ylabels_2rows_SALSA):
    Y_LABEL_2ROWS_DICT[key] = val

Y_LABEL_2ROWS_SPLIT_DICT = {
    'CO': Y_LABEL_2ROWS_DICT['CO2_aquifer'],
    'brine_aquifer': Y_LABEL_2ROWS_DICT['brine_aquifer'],
    'brine_aquifer_cell_': Y_LABEL_2ROWS_DICT['brine_aquifer'],
    'brine_aquifer_segm_': Y_LABEL_2ROWS_DICT['brine_aquifer'],
    'mass_CO': Y_LABEL_2ROWS_DICT['mass_CO2_aquifer'],
    'mass_brine_aquifer': Y_LABEL_2ROWS_DICT['mass_brine_aquifer'],
    'mass_brine_aquifer_cell_': Y_LABEL_2ROWS_DICT['mass_brine_aquifer'],
    'mass_brine_aquifer_segm_': Y_LABEL_2ROWS_DICT['mass_brine_aquifer'],
    'Dissolved_CO': Y_LABEL_2ROWS_DICT['Dissolved_CO2_mass_fraction'],
    'Dissolved_salt_mass_fraction_coord_': Y_LABEL_2ROWS_DICT['Dissolved_salt_mass_fraction']}

# Dictionary with the title names for different component output. Because single
# plots can display results for multiple aquifers, I have removed the aquifer numbers here.
TITLE_DICT = {
    # Reservoir components
    'pressure': 'Reservoir Pressure',
    'CO2saturation': 'Reservoir CO$_2$ Saturation',
    'mass_CO2_reservoir': 'Mass of CO$_2$ in the Reservoir',
    # Wellbore and adapter components
    'CO2_aquifer': 'CO$_2$ Leakage Rate to Aquifer',
    'CO2_atm': 'CO$_2$ Leakage Rate to Atmosphere',
    'brine_aquifer': 'Brine Leakage Rate to Aquifer',
    'brine_atm': 'Brine Leakage Rate to Atmosphere',
    'mass_CO2_aquifer': 'CO$_2$ Mass Leaked to Aquifer',
    'mass_brine_aquifer': 'Brine Mass Leaked to Aquifer',
    'mass_CO2_atm': 'CO$_2$ Mass Leaked to Atmosphere',
    'mass_brine_atm': 'Brine Mass Leaked to Atmosphere',
    'mass_CO2_gas_aquifer': 'CO$_2$ Gas Leaked to Aquifer',
    'mass_methane_oil_aquifer': 'CH$_4$ Oil Leaked to Aquifer',
    'mass_methane_gas_aquifer': 'CH$_4$ Gas Leaked to Aquifer',
    'mass_oil_aquifer': 'Oil Leaked to Aquifer',
    'mass_gas_aquifer': 'Gas Leaked to Aquifer',
    # Fault flow and Seal horizon components
    'CO2_aquifer_total': 'Cumulative CO$_2$ Leakage Rate to Aquifer',
    'brine_aquifer_total': 'Cumulative Brine Leakage Rate to Aquifer',
    'mass_CO2_aquifer_total': 'Cumulative CO$_2$ Mass Leaked to Aquifer',
    'mass_brine_aquifer_total': 'Cumulative Brine Mass Leaked to Aquifer',
    # Aquifer components
    'Benzene_volume': 'Benzene Plume Volume',
    'Naphthalene_volume': 'Naphthalene Plume Volume',
    'Phenol_volume': 'Phenol Plume Volume',
    'As_volume': 'Arsenic Plume Volume',
    'Pb_volume': 'Lead Plume Volume',
    'Cd_volume': 'Cadmium Plume Volume',
    'Ba_volume': 'Barium Plume Volume',
    'Flux': 'CO$_2$ Leakage Rate to Atmosphere',
    'Dissolved_salt_volume': 'Dissolved Salt Volume',
    'Dissolved_CO2_mass_fraction': 'Dissolved CO$_2$ Mass Fraction',
    'Dissolved_salt_mass_fraction': 'Dissolved Salt Mass Fraction',
    # FutureGen2 aquifer components
    'TDS_volume': 'TDS Plume Volume',
    'TDS_dx': 'TDS Plume Length',
    'TDS_dy': 'TDS Plume Width',
    'TDS_dz': 'TDS Plume Height',
    'pH_volume': 'Low-pH Plume Volume',
    'pH_dx': 'Low-pH Plume Length',
    'pH_dy': 'Low-pH Plume Width',
    'pH_dz': 'Low-pH Plume Height',
    'Pressure_volume': 'High-Pressure Plume Volume',
    'Pressure_dx': 'High-Pressure Plume Length',
    'Pressure_dy': 'High-Pressure Plume Width',
    'Pressure_dz': 'High-Pressure Plume Height',
    'Dissolved_CO2_volume': 'Dissolved-CO$_2$ Plume Volume',
    'Dissolved_CO2_dx': 'Dissolved-CO$_2$ Plume Length',
    'Dissolved_CO2_dy': 'Dissolved-CO$_2$ Plume Width',
    'Dissolved_CO2_dz': 'Dissolved-CO$_2$ Plume Height',
    'Temperature_volume': 'Temperature-Change Plume Volume',
    'Temperature_dx': 'Temperature-Change Plume Length',
    'Temperature_dy': 'Temperature-Change Plume Width',
    'Temperature_dz': 'Temperature-Change Plume Height',
    # Generic aquifer component
    # key 'Dissolved_salt_volume' is already present for aquifer components above
    'Dissolved_salt_dr': 'Dissolved Salt Plume Radius',
    'Dissolved_salt_dz': 'Dissolved Salt Plume Height',
    # key 'Dissolved_salt_mass_fraction' is already present for aquifer components above
    'Dissolved_CO2_dr': 'Dissolved CO$_2$ Plume Radius',
    # key 'Dissolved_CO2_dz' is already present for aquifer components above
    # key 'Dissolved_CO2_mass_fraction' is already present for aquifer components above
    # Plume stability component
    'pressure_areas': 'Pressure Plume Area',
    'pressure_areas_dt': 'Change in Pressure Plume Area',
    'pressure_mobility': 'Velocity of Pressure Plume',
    'pressure_mobility_angles': 'Direction of Pressure Plume',
    'pressure_spreading': 'Dispersion of Pressure Plume',
    'pressure_spreading_angles': 'Direction of Pressure Plume Dispersion',
    'CO2saturation_areas': 'CO$_2$ Saturation Plume Area',
    'CO2saturation_areas_dt': 'Change in CO$_2$ Saturation Plume Area',
    'CO2saturation_mobility': 'Velocity of CO$_2$ Saturation Plume',
    'CO2saturation_mobility_angles': 'Direction of CO$_2$ Saturation Plume',
    'CO2saturation_spreading': 'Dispersion of CO$_2$ Saturation Plume',
    'CO2saturation_spreading_angles': 'Direction of CO$_2$ Saturation Plume Dispersion',
    }

for ind, (key, val) in enumerate(zip(additional_keys, additional_titles)):
    TITLE_DICT[key] = val

for (key, val) in zip(additional_keys_SALSA, additional_titles_SALSA):
    TITLE_DICT[key] = val

TITLE_SPLIT_DICT = {
    'CO': TITLE_DICT['CO2_aquifer'],
    'brine_aquifer': TITLE_DICT['brine_aquifer'],
    'brine_aquifer_cell_': TITLE_DICT['brine_aquifer'],
    'brine_aquifer_segm_': TITLE_DICT['brine_aquifer'],
    'mass_CO': TITLE_DICT['mass_CO2_aquifer'],
    'mass_brine_aquifer': TITLE_DICT['mass_brine_aquifer'],
    'mass_brine_aquifer_cell_': TITLE_DICT['mass_brine_aquifer'],
    'mass_brine_aquifer_segm_': TITLE_DICT['mass_brine_aquifer'],
    'Dissolved_CO': TITLE_DICT['Dissolved_CO2_mass_fraction'],
    'Dissolved_salt_mass_fraction_coord_': TITLE_DICT['Dissolved_salt_mass_fraction']}

# Dictionary with the partial legend names for different component output.
# Some graphs can display results for multiple aquifers, and this dictionary is
# used to differentiate between such results. Otherwise, the LEGEND_DICT entries are empty.
LEGEND_DICT = {
    # Reservoir components
    'pressure': 'pressure',
    'CO2saturation': 'CO$_2$ saturation',
    'mass_CO2_reservoir': 'CO$_2$ mass in reservoir',
    # Wellbore and adapter components
    'CO2_aquifer': 'CO$_2$ leakage rate to aquifer',
    'CO2_atm': 'CO$_2$ leakage rate to atmosphere',
    'brine_aquifer': 'brine leakage rate to aquifer',
    'brine_atm': 'brine leakage rate to atmosphere',
    'mass_CO2_aquifer': 'CO$_2$ mass leaked to aquifer',
    'mass_brine_aquifer': 'brine mass leaked to aquifer',
    'mass_CO2_atm': 'CO$_2$ mass leaked to atmosphere',
    'mass_brine_atm': 'brine mass leaked to atmosphere',
    'mass_CO2_gas_aquifer': 'mass of CO$_2$ gas leaked to aquifer',
    'mass_methane_oil_aquifer': 'mass of methane oil leaked to aquifer',
    'mass_methane_gas_aquifer': 'mass of methane gas leaked to aquifer',
    'mass_oil_aquifer': 'oil mass leaked to aquifer',
    'mass_gas_aquifer': 'gas mass leaked to aquifer',
    # Fault flow and Seal horizon components
    'CO2_aquifer_total': 'cumulative CO$_2$ leakage rate',
    'brine_aquifer_total': 'cumulative brine leakage rate',
    'mass_CO2_aquifer_total': 'cumulative CO$_2$ mass leaked to aquifer',
    'mass_brine_aquifer_total': 'cumulative brine mass leaked to aquifer',
    # Aquifer components
    'Benzene_volume': 'benzene plume volume',
    'Naphthalene_volume': 'naphthalene plume volume',
    'Phenol_volume': 'phenol plume volume',
    'As_volume': 'arsenic plume volume',
    'Pb_volume': 'lead plume volume',
    'Cd_volume': 'cadmium plume volume',
    'Ba_volume': 'barium plume volume',
    'Flux': 'CO$_2$ leakage rate to atmosphere',
    # FutureGen2 aquifer components
    'TDS_volume': 'TDS plume volume',
    'TDS_dx': 'TDS plume Length',
    'TDS_dy': 'TDS plume width',
    'TDS_dz': 'TDS plume height',
    'pH_volume': 'pH plume volume',
    'pH_dx': 'pH plume length',
    'pH_dy': 'pH plume width',
    'pH_dz': 'pH plume height',
    'Pressure_volume': 'pressure plume volume',
    'Pressure_dx': 'pressure plume length',
    'Pressure_dy': 'pressure plume width',
    'Pressure_dz': 'pressure plume height',
    'Dissolved_CO2_volume': 'CO$_2$ plume volume',
    'Dissolved_CO2_dx': 'CO$_2$ plume length',
    'Dissolved_CO2_dy': 'CO$_2$ plume width',
    'Dissolved_CO2_dz': 'CO$_2$ plume height',
    'Temperature_volume': 'temperature plume volume',
    'Temperature_dx': 'temperature plume length',
    'Temperature_dy': 'temperature plume width',
    'Temperature_dz': 'temperature plume height',
    # Generic aquifer component
    'Dissolved_salt_volume': 'salt plume volume',
    'Dissolved_salt_dr': 'salt plume radius',
    'Dissolved_salt_dz': 'salt plume height',
    'Dissolved_salt_mass_fraction': 'salt mass fraction',
    'Dissolved_CO2_dr': 'CO$_2$ plume radius',
    'Dissolved_CO2_dz': 'CO$_2$ plume height',
    'Dissolved_CO2_mass_fraction': 'CO$_2$ mass fraction',
    # Plume stability component
    'pressure_areas': 'pressure plume areas',
    'pressure_areas_dt': 'change in pressure plume areas',
    'pressure_mobility': 'pressure plume mobility',
    'pressure_mobility_angles': 'direction of pressure plume velocity',
    'pressure_spreading': 'pressure plume spreading',
    'pressure_spreading_angles': 'direction of pressure plume dispersion',
    'CO2saturation_areas': 'CO$_2$ plume areas',
    'CO2saturation_areas_dt': 'change in CO$_2$ plume areas',
    'CO2saturation_mobility': 'CO$_2$ plume velocity',
    'CO2saturation_mobility_angles': 'direction of CO$_2$ plume velocity',
    'CO2saturation_spreading': 'CO$_2$ plume dispersion',
    'CO2saturation_spreading_angles': 'direction of CO$_2$ plume dispersion',
    }

for ind, (key, val) in enumerate(zip(additional_keys, additional_legends)):
    LEGEND_DICT[key] = val

for (key, val) in zip(additional_keys_SALSA, additional_legends_SALSA):
    LEGEND_DICT[key] = val

UNIT_DICT = {
    # Reservoir components
    'pressure': 'Pa',
    'CO2saturation': '',
    'mass_CO2_reservoir': 'kg',
    # Wellbore and adapter components
    'CO2_aquifer': 'kg/s',
    'CO2_atm': 'kg/s',
    'brine_aquifer': 'kg/s',
    'brine_atm': 'kg/s',
    'mass_CO2_aquifer': 'kg',
    'mass_brine_aquifer': 'kg',
    'mass_CO2_atm': 'kg',
    'mass_brine_atm': 'kg',
    'mass_CO2_gas_aquifer': 'kg',
    'mass_methane_oil_aquifer': 'kg',
    'mass_methane_gas_aquifer': 'kg',
    'mass_oil_aquifer': 'kg',
    'mass_gas_aquifer': 'kg',
    # Fault flow and Seal horizon components
    'CO2_aquifer_total': 'kg/s',
    'brine_aquifer_total': 'kg/s',
    'mass_CO2_aquifer_total': 'kg',
    'mass_brine_aquifer_total': 'kg',
    # Aquifer components
    'Benzene_volume': 'm$^3$',
    'Naphthalene_volume': 'm$^3$',
    'Phenol_volume': 'm$^3$',
    'As_volume': 'm$^3$',
    'Pb_volume': 'm$^3$',
    'Cd_volume': 'm$^3$',
    'Ba_volume': 'm$^3$',
    'Flux': 'kg/s',
    # FutureGen2 aquifer components
    'TDS_volume': 'm$^3$',
    'TDS_dx': 'm',
    'TDS_dy': 'm',
    'TDS_dz': 'm',
    'pH_volume': 'm$^3$',
    'pH_dx': 'm',
    'pH_dy': 'm',
    'pH_dz': 'm',
    'Pressure_volume': 'm$^3$',
    'Pressure_dx': 'm',
    'Pressure_dy': 'm',
    'Pressure_dz': 'm',
    'Dissolved_CO2_volume': 'm$^3$',
    'Dissolved_CO2_dx': 'm',
    'Dissolved_CO2_dy': 'm',
    'Dissolved_CO2_dz': 'm',
    'Temperature_volume': 'm$^3$',
    'Temperature_dx': 'm',
    'Temperature_dy': 'm',
    'Temperature_dz': 'm',
    # Generic aquifer component
    'Dissolved_salt_volume': 'm$^3$',
    'Dissolved_salt_dr': 'm',
    'Dissolved_salt_dz': 'm',
    'Dissolved_salt_mass_fraction': '',
    'Dissolved_CO2_dr': 'm',
    'Dissolved_CO2_dz': 'm',
    'Dissolved_CO2_mass_fraction': 'm',
    # Plume stability component
    'pressure_areas': 'm$^2$',
    'pressure_areas_dt': 'm$^2$/year',
    'pressure_mobility': 'm/year',
    'pressure_mobility_angles': '',
    'pressure_spreading': 'm$^2$/year',
    'pressure_spreading_angles': '',
    'CO2saturation_areas': 'm$^2$',
    'CO2saturation_areas_dt': 'm$^2$/year',
    'CO2saturation_mobility': 'm/year',
    'CO2saturation_mobility_angles': '',
    'CO2saturation_spreading': 'm$^2$/year',
    'CO2saturation_spreading_angles': '',
    }

for (key, val) in zip(additional_keys, additional_units):
    UNIT_DICT[key] = val

for (key, val) in zip(additional_keys_SALSA, additional_units_SALSA):
    UNIT_DICT[key] = val

# If y axis limits are enforced, this dictionary is used to check the metrics 
# for different subplots. If the metrics for different subplots are sufficientyly 
# similar (e.g., different kinds of leakage rates in kg/s), then those subplots 
# are assigned the same y axis limits.
SIMILAR_PARTIAL_METRICS_DICT = {
    'leakage_rates': ['brine_aquifer', 'CO2_aquifer', 'brine_atm', 'CO2_atm'], 
    'leaked_masses': ['mass_brine_aquifer', 'mass_CO2_aquifer', 
                      'mass_brine_atm', 'mass_CO2_atm', 
                      'mass_CO2_gas_aquifer', 'mass_methane_oil_aquifer', 
                      'mass_methane_gas_aquifer', 'mass_oil_aquifer', 
                      'mass_gas_aquifer'], 
    'plume_volumes': ['Pressure_volume', 'TDS_volume', 'pH_volume', 
                      'Dissolved_salt_volume', 'Dissolved_CO2_volume', 
                      'As_volume', 'Pb_volume', 'Cd_volume', 'Ba_volume', 
                      'Benzene_volume', 'Temperature_volume'], 
    'plume_dimensions': ['Pressure_dx', 'Pressure_dy', 'Pressure_dz', 
                         'TDS_dx', 'TDS_dy', 'TDS_dz', 'pH_dx', 'pH_dy', 'pH_dz', 
                         'Dissolved_salt_dr', 'Dissolved_salt_dz', 
                         'Dissolved_CO2_dr', 'Dissolved_CO2_dz', 
                         'Temperature_dx', 'Temperature_dy', 'Temperature_dz'], 
    'plume_areas': ['_areas'], 'plume_areas_dt': ['_areas_dt'], 
    'plume_mobility': ['_mobility'], 'plume_mobility_angles': ['_mobility_angles'], 
    'plume_spreading': ['_spreading'], 'plume_spreading_angles': ['_spreading_angles'], 
    'SALSA_well_leakage_rates': ['LeakageRateAquifer'], 
    'SALSA_well_leakage_volumes': ['LeakageVolumeAquifer'], 
    'SALSA_diffuse_leakage_rates': ['LeakageRateBottomAquifer', 'LeakageRateTopAquifer'], 
    'SALSA_diffuse_leakage_volumes': ['LeakageVolumeBottomAquifer', 'LeakageVolumeTopAquifer'], 
    'SALSA_hydraulic_head': ['headLoc', 'headProfile'], 
    'SALSA_pressure': ['pressureLoc', 'pressureProfile']
    }

# For the leakage rates and leaked masses, time_series.py requires an exact 
# match. Otherwise, the wrong key might be selected, resulting in inaccurate 
# y axis labels, titles, and legends.
for key in additional_keys_leakage_rates:
    SIMILAR_PARTIAL_METRICS_DICT['leakage_rates'].append(key)

for key in additional_keys_leaked_masses:
    SIMILAR_PARTIAL_METRICS_DICT['leaked_masses'].append(key)

Y_LABEL_DICT_MULT_METRICS = Y_LABEL_DICT.copy()

Y_LABEL_DICT_MULT_METRICS['leakage_rates'] = 'Leakage Rates (kg/s)'
Y_LABEL_DICT_MULT_METRICS['leaked_masses'] = 'Leaked Masses (kg)'
Y_LABEL_DICT_MULT_METRICS['plume_volumes'] = 'Aquifer Plume Volumes (m$^3$)'
Y_LABEL_DICT_MULT_METRICS['plume_dimensions'] = 'Aquifer Plume Dimensions (m)'
Y_LABEL_DICT_MULT_METRICS['plume_areas'] = 'Reservoir Plume Areas (m$^2$)'
Y_LABEL_DICT_MULT_METRICS['plume_areas_dt'] = 'Change in Reservoir Plume Areas (m$^2$/year)'
Y_LABEL_DICT_MULT_METRICS['plume_mobility'] = 'Reservoir Plume Mobility (m/year)'
Y_LABEL_DICT_MULT_METRICS['plume_mobility_angles'] = 'Direction of Reservoir Plume Mobility'
Y_LABEL_DICT_MULT_METRICS['plume_spreading'] = 'Reservoir Plume Dispersion (m$^2$/year)'
Y_LABEL_DICT_MULT_METRICS['plume_spreading_angles'] = 'Direction of Reservoir Plume Dispersion'
Y_LABEL_DICT_MULT_METRICS['SALSA_well_leakage_rates'] = 'Well Leakage Rates (m$^3$/s)'
Y_LABEL_DICT_MULT_METRICS['SALSA_well_leakage_volumes'] = 'Well Leakage Volumes (m$^3$)'
Y_LABEL_DICT_MULT_METRICS['SALSA_diffuse_leakage_rates'] = 'Diffuse Leakage Rates (m$^3$/s)'
Y_LABEL_DICT_MULT_METRICS['SALSA_diffuse_leakage_volumes'] = 'Diffuse Leakage Volumes (m$^3$)'
Y_LABEL_DICT_MULT_METRICS['SALSA_hydraulic_head'] = 'Hydraulic Head (m)'
Y_LABEL_DICT_MULT_METRICS['SALSA_pressure'] = 'Pressure (Pa)'

TITLE_DICT_MULT_METRICS = TITLE_DICT.copy()

TITLE_DICT_MULT_METRICS['leakage_rates'] = 'Leakage Rates'
TITLE_DICT_MULT_METRICS['leaked_masses'] = 'Leaked Masses'
TITLE_DICT_MULT_METRICS['plume_volumes'] = 'Aquifer Plume Volumes'
TITLE_DICT_MULT_METRICS['plume_dimensions'] = 'Aquifer Plume Dimensions'
TITLE_DICT_MULT_METRICS['plume_areas'] = 'Reservoir Plume Areas'
TITLE_DICT_MULT_METRICS['plume_areas'] = 'Change in Reservoir Plume Areas'
TITLE_DICT_MULT_METRICS['plume_mobility'] = 'Reservoir Plume Velocity'
TITLE_DICT_MULT_METRICS['plume_mobility_angles'] = 'Direction of Reservoir Plume Velocity'
TITLE_DICT_MULT_METRICS['plume_spreading'] = 'Reservoir Plume Dispersion'
TITLE_DICT_MULT_METRICS['plume_spreading_angles'] = 'Direction of Reservoir Plume Dispersion'
TITLE_DICT_MULT_METRICS['SALSA_well_leakage_rates'] = 'Well Leakage Rates'
TITLE_DICT_MULT_METRICS['SALSA_well_leakage_volumes'] = 'Well Leakage Volumes'
TITLE_DICT_MULT_METRICS['SALSA_diffuse_leakage_rates'] = 'Diffuse Leakage Rates'
TITLE_DICT_MULT_METRICS['SALSA_diffuse_leakage_volumes'] = 'Diffuse Leakage Volumes'
TITLE_DICT_MULT_METRICS['SALSA_hydraulic_head'] = 'Hydraulic Head'
TITLE_DICT_MULT_METRICS['SALSA_pressure'] = 'Pressure'

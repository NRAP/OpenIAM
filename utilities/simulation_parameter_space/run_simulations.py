# -*- coding: utf-8 -*-
"""
Reads the file simulation_files.csv and runs all control files and scripts 
included in the file.

The file simulation_files.csv, parameter_sets.csv, and simulation_sets.csv are 
meant to help the user organize the suite of simulations being examined. The 
simulations in each simulation set are meant to have similar setups. Within 
each simulation set, different parameter sets are meant to keep the same setup 
but vary how models are parameterized. 

For example, simulation set 1 might consider legacy wells that are closer to 
the injection sites while simulation set 2 might consider legacy wells that are 
farther from the injection site. Both simulation sets could then have parameter 
sets 1 and 2, where parameter set 1 has higher permeabilities for legacy wells 
and parameter set 2 has lower permeabilities for legacy wells. The parameter 
sets would not change legacy well locations, only their permeabilities.

Specifying all of the simulation files in simulation_files.csv and describing 
how the simulations fit within the parameter sets (parameter_sets.csv) and 
simulation sets (simulation_sets.csv) is meant to help the user organize their 
suite of simulations. This organization can also help if one person needs to 
take over the suite of models set up by another person.
"""
import sys
import os
import logging
from datetime import datetime
import shutil
import pandas as pd

try:
    from openiam.components.iam_base_classes import IAM_DIR
except:
    err_msg = ('Could not import from NRAP-Open-IAM classes, ensure the ' 
               + 'NRAP-Open-IAM environment has been set up and activated. ' 
               + 'See the NRAP-Open-IAM user guide.')
    
    raise ImportError(err_msg)


def read_sim_files(sim_files_dir, sim_files_name):
    """
    Function attempts to simulation file information from a .csv file. If the 
    .csv file is found, the function returns the information as a pandas DataFrame.
    
    :param sim_files_dir: Directory in which the sim_files_name .csv file is located.
    
    :param sim_files_name: Name of the .csv file containing files (.yaml or .py) 
        to run.
    """
    file_path = os.path.join(sim_files_dir, sim_files_name)
    
    data = None
    if not os.path.exists(file_path):
        err_msg = ''.join([
            'In the code run_simulations.py, the .csv file listing simulation ', 
            f'files was given as {file_path}. This file did not exist, however, ', 
            'so the simulations could not be run. Check your input.'])
        logging.error(err_msg)
        raise FileNotFoundError(err_msg)
    else:
        data = pd.read_csv(
            file_path, delimiter=",", dtype='str', comment='#')
    
    return data


def check_file_dir(file, file_dir):
    """
    Checks if a directory (file_dir) exists. If not, attempts to set the directory 
    relative to the main directory of NRAP-Open-IAM (IAM_DIR). If that adjusted 
    directory does not exist, an error message is logged and the file_dir is 
    returned as None.
    """
    file_path = os.path.join(file_dir, file)
    if not os.path.exists(file_path):
        file_dir_updated = os.path.join(IAM_DIR, file_dir)
        file_path_updated = os.path.join(file_dir_updated, file)
        
        if not os.path.exists(file_path_updated):
            err_msg = ''.join([
                f'In the file run_simulations.py, the simulation file {file} ', 
                f'was specified as being located in the directory {file_dir}. ', 
                'This file could not be found. The code also checked if the ', 
                'directory provided was relative to the main directory of the ', 
                f'NRAP-Open-IAM installation ({file_dir_updated}), but the ', 
                'file could not be found with that approach either. The file ', 
                f'{file} will not be used, check your input.'])
            logging.error(err_msg)
            
            file_dir = None
        else:
            file_dir = file_dir_updated
    
    return file_dir


def run_simulation_files(sim_files_dir, sim_files_name, sim_set_file_name, 
                         par_set_file_name, logging_folder_dir=None, 
                         logging_file_name='results_log.log', log_level='Debug'):
    """
    This function reads a .csv file describing the simulation set and runs the 
    files specified. Colntrol files (.yaml) and script files (.py) will be run 
    separately
    
    :param sim_files_dir: directory where the sim_files_name, sim_set_file_name, 
        and par_set_file_name .csv files are located.
        
    :param sim_files_name: name of the .csv file containing information about 
        the files to run.
    
    :param sim_set_file_name: name of the .csv file containing information about 
        the simulations set(s)
     
    :param par_set_file_name: name of the .csv file containing information about 
        the parameter set(s)
    
    :param logging_folder_dir: The name of the folder where a logging file 
        will be made.
    
    :param logging_file_name: name of the .log file that will be created.
    
    :param log_level: logging level to use - 'All', 'Debug', 'Info', 'Warning', 
        'Error', or 'Critical'.
    """
    if logging_folder_dir is None:
        logging_folder_dir = os.path.join(sim_files_dir, 'logging_files')
        
        if not os.path.exists(logging_folder_dir):
            os.mkdir(logging_folder_dir)
        
        start_time = datetime.now()
        now = start_time.strftime('%Y-%m-%d_%H.%M.%S')
        
        logging_folder_dir = os.path.join(logging_folder_dir, 'simulations_{}'.format(now))
        
    if not os.path.exists(logging_folder_dir):
        os.mkdir(logging_folder_dir)
    
    set_or_par_file_warning = ''.join([
        'In the function run_simulation_files(), the {} variable, ', 
        'which specifies the .csv file containing information about the ', 
        '{}, was given as {} in the directory ', f'{sim_files_dir}. This ', 
        'file could not be found, so the simulation set information could not ', 
        f'be copied into the output folder {logging_folder_dir}.'])
    
    files_to_copy = [sim_files_name, sim_set_file_name, par_set_file_name]
    var_names_for_copying = ['sim_files_name', 'sim_set_file_name', 'par_set_file_name']
    var_types_for_copying = ['simulation files', 'simulation set', 'parameter set']
    for ind, (file, var_name, var_type) in enumerate(zip(
            files_to_copy, var_names_for_copying, var_types_for_copying)):
        file_path = os.path.join(sim_files_dir, file)
        
        if not os.path.exists(file_path):
            warning_msg = set_or_par_file_warning.format(
                var_name, var_type, file)
            logging.warning(warning_msg)
        else:
            new_file_path = os.path.join(logging_folder_dir, file)
            shutil.copy2(file_path, new_file_path)
            logging.info(f'Copied the {var_type} file from {file_path} to {new_file_path}.')
    
    try:
        data = read_sim_files(sim_files_dir, sim_files_name)
    except Exception as e:
        err_msg = ''.join([
            'In the code run_simulations.py, an error occured during the function ', 
            'run_simulation_files(). The .csv file containing simulation file ', 
            'information (names, directories, file types) was given as ', 
            f'{sim_files_name} in the directory {sim_files_dir}. Data could not ', 
            f'be read from that file. The error message was: \n{e}'])
        logging.error(err_msg)
        raise ValueError(err_msg)
    
    file_not_found_msg = ''.join([
        'In the code run_simulations.py, an error occured during the function ', 
        'run_simulation_files(). The .csv file containing simulation file ', 
        'information (names, directories, file types) was given as ', 
        f'{sim_files_name} in the directory {sim_files_dir}. This file did not ', 
        'include the required field {}. Check your input.'])
    
    required_fields = ['FileName', 'Directory']
    
    for field in required_fields:
        if not field in data:
            logging.error(file_not_found_msg.format(field))
            raise KeyError(file_not_found_msg.format(field))
    
    try:
        sys.path.append(os.path.join(
            IAM_DIR, 'utilities', 'file_search_and_run', 'Control_Files'))
        from run_CFs_from_csv_file import process_control_files
        
        sys.path.append(os.path.join(
            IAM_DIR, 'utilities', 'file_search_and_run', 'scripts'))
        from run_scripts_from_csv_file import process_script_files
    except:
        err_msg = ''.join([
            'In the script /utilities/simulation_parameter_space/run_simulations.py, ', 
            'there was an error while importing functions from /utilities/file_search_and_run/. ', 
            'The NRAP-Open-IAM tool may not have been installed correctly.'])
        logging.error(err_msg)
    
    
    control_files = []
    control_files_dirs = []
    scripts = []
    scripts_dirs = []
    
    for ind, (file, file_dir) in enumerate(zip(data['FileName'], data['Directory'])):
        file_dir = check_file_dir(file, file_dir)
        
        if file_dir is not None:
            if '.yaml' in file:
                # File is a control file
                control_files.append(file)
                control_files_dirs.append(file_dir)
            elif '.py' in file:
                # File is a script
                scripts.append(file)
                scripts_dirs.append(file_dir)
            else:
                err_msg = ''.join([
                    f'In the script run_simulations.py, the file {file} did not ', 
                    'have the expected format. Files are expected to be either ', 
                    f'.py scripts or .yaml control files. The file {file} will ', 
                    'not be run. Check your input.'])
                logging.error(err_msg)
    
    if len(control_files) > 0:
        process_control_files(control_files, control_files_dirs, set_up_logging=True, 
                              logging_folder_dir=logging_folder_dir, 
                              logging_file_name=logging_file_name, 
                              log_level=log_level)
    
    if len(scripts) > 0:
        process_script_files(scripts, scripts_dirs, set_up_logging=True, 
                             logging_folder_dir=logging_folder_dir, 
                             logging_file_name=logging_file_name, 
                             log_level=log_level)


if __name__ == "__main__":
    sim_files_dir = os.path.join(IAM_DIR, 'utilities', 'simulation_parameter_space')
    sim_files_name = 'simulation_files.csv'
    sim_set_file_name = 'simulation_sets.csv'
    par_set_file_name = 'parameter_sets.csv'
    
    try:
        run_simulation_files(
            sim_files_dir, sim_files_name, sim_set_file_name, par_set_file_name)
    except Exception as e:
        err_msg = ''.join([
            'In the script run_simulations.py, there was an error while ', 
            'attempting to use the funciton run_simulation_files(). The error ', 
            f'message was: \n{e}'])
        logging.error(err_msg)
        raise ValueError(err_msg)

.. include:: ../../common/replace_math.rst

#############################################
NRAP-Open-IAM Quality Assurance Documentation
#############################################

.. toctree::
   :maxdepth: 2
   :caption: Contents:

This is the documentation of the Quality Assurance (QA) for the NRAP-Open-IAM.
The NRAP-Open-IAM is an open-source framework for assessing risks associated
with geologic carbon storage (GCS).

NRAP-Open-IAM evaluates GCS risk using an integrated assessment modeling
approach, where models representing GCS components (e.g., reservoir, wellbore,
shallow aquifer, atmosphere) can be linked together into a complete GCS system model.
This document provides details of QA for individual components,
coupled components, benchmark tests, and describes the process whereby
NRAP-Open-IAM maintains QA during development.

.. include:: qaqc_dev.rst

.. include:: components.rst

.. .. include:: benchmarks.rst

.. include:: testsuite.rst

Contributors
============

During the Phase II and/or Phase III of the NRAP the following researchers contributed
to the development of NRAP-Open-IAM (listed in alphabetical order with affiliation
at the time of active contribution):

    Diana Bacon (Pacific Northwest National Laboratory)

    Seunghwan Baek (Pacific Northwest National Laboratory)

    Pramod Bhuvankar (Lawrence Berkeley National Laboratory)

    Suzanne (Michelle) Bourret (Los Alamos National Laboratory)

    Julia De Toledo Camargo (Pacific Northwest National Laboratory)

    Bailian Chen (Los Alamos National Laboratory)

    Abdullah Cihan (Lawrence Berkeley National Laboratory)

    Dylan Harp (Los Alamos National Laboratory)

    Paul Holcomb (National Energy Technology Laboratory)

    Jaisree Iyer (Lawrence Livermore National Laboratory)

    Elizabeth Keating (Los Alamos National Laboratory)

    Seth King (National Energy Technology Laboratory)

    Greg Lackey (National Energy Technology Laboratory)

    Ernest Lindner (National Energy Technology Laboratory)

    Kayyum Mansoor (Lawrence Livermore National Laboratory)

    Mohamed Mehana (Los Alamos National Laboratory)

    Saro Meguerdijian (Los Alamos National Laboratory)

    Nathaniel Mitchell (National Energy Technology Laboratory)

    Omotayo Omosebi (Lawrence Berkeley National Laboratory)

    Shaparak Salek (National Energy Technology Laboratory)

    Veronika Vasylkivska (National Energy Technology Laboratory)

    Ya-Mei Yang (National Energy Technology Laboratory)

    Yingqi Zhang (Lawrence Berkeley National Laboratory)

.. Indices and tables
.. ==================

* :ref:`genindex`
.. * :ref:`modindex`
* :ref:`search`

# OpenIAM Control File Simulation 2a
#
# This example is used to demonstrate the codes in the \utilities\simulation_parameter_space 
# folder (where the \utilities folder is located in the main directory of the NRAP-Open-IAM 
# installation). The .csv files there are meant to help organize a set of simulations. Furthermore, 
# the file run_simulations.py can be used to run all of the simulations, one after another. This 
# feature can be useful if the user needs to evaluate many simulations. For more information, see 
# the "Managing Simulations" section of the user guide.
#
# Although the file run_simulations.py will run the files, the output of a control file will still 
# be placed in the OutputDirectory entry in the ModelParams section of the control file. Like other 
# control files, a logging file will be placed in the output folder. The run_simulations.py file 
# will, however, create an overall logging file in the \utilities\simulation_parameter_space\logging_files 
# folder. This overall logging file will contain information about all of the simulations assessed. 
# The files simulation_files.csv, simulation_sets.csv, and parameter_sets.csv will also be coppied 
# into the folder created in \utilities\simulation_parameter_space\logging_files. These files are 
# meant to help the user manage the suite of simulations.
#
# This simulation is from simulation set 2, parameter set 1.
#
# Simulation set 2 has 10 leaking wells randomly placed within an area (x values from -1.414 km to 
# 1.414 km and y values of -1.414 km and 1.414 km, for a maximum potential distance from the injection 
# well of 2 km). Simulation set 2 also is a stochastic simulation ("type: lhs" for Latin Hypercube Sampling) 
# with 50 realizations ("siz: 50"). Parameter set 1 of simulation set 2 has randomly varying logResPerm, 
# injRate, and logWellPerm values.
#
# Because the CementedWellbore section includes "AccumulateLeakage: True," the cumulative masses of brine 
# and CO2 (e.g., mass_brine_aquifer1 and mass_CO2_aquifer 1) will be saved and displayed in figures.
#
# To run this file, use the command (\ for Windows / for Mac or Linux):
#  python ../../src/openiam/components/openiam_cf.py --file ControlFile_sim2a.yaml
#
# Last Modified: November, 2024
#-------------------------------------------------
ModelParams:
    EndTime: 25
    TimeStep: 1.0
    Analysis:
        type: lhs
        siz: 50
    Components: [AnalyticalReservoir1, CementedWellbore1]
    OutputDirectory: output/output_sim2a_{datetime}
    OutputType: 1
    Logging: Error
    GenerateLocationsFiles: True
    GenerateOutputFiles: True
    GenerateCombOutputFile: False
#-------------------------------------------------
Stratigraphy:
    numberOfShaleLayers:
        vary: False
        value: 3
    shale1Thickness:
        vary: False
        value: 525.0
    shale2Thickness:
        vary: False
        value: 475.0
    shale3Thickness:
        vary: False
        value: 11.2
    aquifer1Thickness:
        vary: False
        value: 22.4
    aquifer2Thickness:
        vary: False
        value: 19.2
    reservoirThickness:
        vary: False
        value: 51.2
#-------------------------------------------------
AnalyticalReservoir1:
    Type: AnalyticalReservoir
    InjectionWell:
        coordx: 0
        coordy: 0
    Parameters:
        logResPerm:
            min: -14.0
            max: -13.0
            value: -13.5
        reservporPorosity:
            vary: False
            value: 0.13
        reservoirRadius:
            vary: False
            value: 5100
        brineDensity:
            vary: False
            value: 1060
        CO2Density:
            vary: False
            value: 479
        brineViscosity:
            vary: False
            value: 2.535e-4
        CO2Viscosity:
            vary: False
            value: 3.95e-5
        brineResSaturation:
            vary: False
            value: 0.15
        brineCompressibility:
            vary: False
            value: 4.5e-12
        injRate:
            min: 0.1
            max: 0.5
            value: 0.3
    Outputs: [pressure, CO2saturation]
#-------------------------------------------------
CementedWellbore1:
    Type: CementedWellbore
    Connection: AnalyticalReservoir1
    Number: 10
    RandomLocDomain:
        xmin: -1414.21
        xmax: 1414.21
        ymin: -1414.21
        ymax: 1414.21
    AccumulateLeakage: True
    Parameters:
        logWellPerm:
            min: -13.0
            max: -12.0
            value: -12.5
        logThiefPerm:
            vary: False
            value: -12.5
        wellRadius:
            vary: False
            value: 0.125
    Outputs: [CO2_aquifer1, CO2_aquifer2, CO2_atm,
              brine_aquifer1, brine_aquifer2, brine_atm]
#-------------------------------------------------
Plots:
    CO2_Leakage_Rates_Aq1:
        TimeSeries: [CO2_aquifer1]
        FigureSize: [15, 15]
    CO2_Leakage_Rates_Aq2:
        TimeSeries: [CO2_aquifer2]
        FigureSize: [15, 15]
    Brine_Leakage_Rates_Aq1:
        TimeSeries: [brine_aquifer1]
        FigureSize: [15, 15]
    Brine_Leakage_Rates_Aq2:
        TimeSeries: [brine_aquifer2]
        FigureSize: [15, 15]
    CO2_Leakage_Mass_Aq1:
        TimeSeries: [mass_CO2_aquifer1]
        FigureSize: [15, 15]
    CO2_Leakage_Mass_Aq2:
        TimeSeries: [mass_CO2_aquifer2]
        FigureSize: [15, 15]
    Brine_Leakage_Mass_Aq1:
        TimeSeries: [mass_brine_aquifer1]
        FigureSize: [15, 15]
    Brine_Leakage_Mass_Aq2:
        TimeSeries: [mass_brine_aquifer2]
        FigureSize: [15, 15]
    Pressures:
        TimeSeries: [pressure]
        FigureSize: [15, 15]
    CO2_Saturations:
        TimeSeries: [CO2saturation]
        FigureSize: [15, 15]
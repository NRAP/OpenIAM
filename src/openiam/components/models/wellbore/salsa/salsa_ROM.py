"""
The module contains the solution class for the SALSA component.

Author: Nate Mitchell
Date: 02/08/2024
"""
import os
import logging
import csv
import sys
import numpy as np

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# In SALSA, model type can be 1 (just focsed flow), 2 (just diffuse flow), or
# 3 (focused and diffuse flow). For the Salsython version, it has to be set to
# 3.
FIXED_MODEL_TYPE = 3

# This message is saved to a .txt file in the SALSA folder created automatically
README_MSG = [
    'In the SALSA output files contained here, the length (L) and time (T) '
    + 'units are meters and days, respectively.',
    'The file HEAD_AQ contains hydraulic head values in meters for each '
    + 'aquifer at each of the aquiferCoordx and aquiferCoordy locations. '
    + 'The left column is time (days) and the other columns correspond with each aquifer: '
    + 'column A01 for aquifer 1 is just to the right of the time column, '
    + 'then column A02 for aquifer 2, etc.',
    'The file HEAD_AQT contains hydraulic head values in meters for each '
    + 'aquitard (shale) point at each of the shaleCoordx and shaleCoordy locations. '
    + 'The depth (m) of each shale vertical point is given on the left, '
    + 'while the hydraulic head value is on the right. These columns are given '
    + 'for each time in days (e.g., "ZONE T ="   3652.5000000000000      " for 3652.5 days).',
    'The file FLOW_AQT contains diffuse leakage rates in m^3/day ("FLOW RATE") '
    + 'and cumulative leakage volumes in m^3 ("CUMU. FLOW RATE"). The '
    + 'column on the left is time (days). The column just to the right of the '
    + 'time column has the results for aquifer 1 (A01), the column to the '
    + 'right of that has the results for aquifer 2 (A02), etc.',
    'The file FLOW_LW contains the focused leakage rates ("LEAKAGE RATE", m^3/day) '
    + 'and leakage volumes ("CUMULATIVE LEAKAGE", m^3) from all leaking wells '
    + '(i.e., all the well''s leakage combined). The column on the left is time (days), '
    + 'the column to the right of that has results for aquifer 1 (A01), '
    + 'the column to the right of that has results for aquifer 2 (A02), etc.',
    'The file FLOW_LW_EACH is similar to FLOW_LW, except FLOW_LW_EACH shows '
    + 'the results for each leaking well individually '
    + '(e.g., "ZONE T ="LEAKY WELL-    1; X,Y= ..."). '
    + 'Each leaking well location corresponds to one of the x- and y-'
    + 'coordinates given with leakingWellCoordx and leakingWellCoordy.',
    'The file CONTOUR contains hydraulic head values over space for each aquifer '
    + 'specified with the aquiferNamesContourPlots entry (if not entered, '
    + 'the default approach is to include all aquifers). The column on the '
    + 'left contains x-values, the column in the middle contains y-values, '
    + 'and the column on the right contains hydraulic head values. This '
    + 'format is repeated for each aquifer and each time step.'
    ]


class Parameters():
    """ Parameters class for the SALSA ROM."""
    def __init__(self):
        """ Create an instance of the Parameters class. """
        # Time array
        self.time_array = None

        # Analysis options
        self.stochastic = None
        self.realizations = None

        # Input parameters
        self.bottomLayerType = None
        self.topLayerType = None
        self.numberOfShaleLayers = None
        self.numberOfAquiferLayers = None
        self.shaleThickness = None
        self.aquiferThickness = None
        self.shaleVertK = None
        self.aquiferHorizK = None
        self.aquiferANSR = None
        self.shaleSS = None
        self.aquiferSS = None
        self.aquiferFluidDensity = None
        self.aquiferHead = None
        self.shaleInitialGamma = None
        self.shaleGamma = None
        self.aquiferGamma = None
        self.bottomBoundaryCond = None
        self.bottomBoundaryHead = None
        self.topBoundaryCond = None
        self.topBoundaryHead = None
        self.numberOfLaplaceTerms = None
        self.flagMesh = None
        self.xCent = None
        self.yCent = None
        self.xMax = None
        self.yMax = None
        self.gridExp = None
        self.numberOfNodesXDir = None
        self.numberOfNodesYDir = None
        self.numberOfRadialGrids = None
        self.radialZoneRadius = None
        self.radialGridExp = None
        self.numberOfVerticalPoints = None
        self.activeWellRadii = None
        self.activeWellQ = None
        self.leakingWellAqK = None
        self.leakingWellShaleRadii = None
        self.leakingWellShaleK = None
        self.leakingWellAqStat = None
        self.leakingWellShaleStat = None
        self.leakingWellQ = None
        self.leakingWellQAq = None
        self.aquiferNamesContourPlots = None
        self.numberOfAquiferNames = None

        # These are taken from the keyword arguments of the SALSA component
        self.periodEndTimes = None
        self.numberOfPeriods = None
        self.activeWellCoordx = None
        self.activeWellCoordy = None
        self.numberOfActiveWells = None
        self.leakingWellCoordx = None
        self.leakingWellCoordy = None
        self.numberOfLeakingWells = None
        self.aquiferCoordx = None
        self.aquiferCoordy = None
        self.numberOfHeadLocs = None
        self.shaleCoordx = None
        self.shaleCoordy = None
        self.numberOfHeadProfiles = None
        self.contourPlotCoordx = None
        self.contourPlotCoordy = None
        self.numberOfContourPlotNodes = None
        
        # These are not passed to salsa_wrap(), but they are used by the 
        # simulation_model() method of the SALSA component.
        self.topBoundaryPressure = None
        self.topBoundaryFluidDensity = None
        self.waterTableDepth = None
        self.specifyDatum = None
        self.datumDepth = None


class Solution():
    """ Solution class for the SALSA ROM."""
    def __init__(self, parameters):
        """ Create an instance of the Solution class."""

        # Setup the parameters that define a solution
        self.parameters = Parameters()
        self.parameters = parameters

        # Reserve a space for the solution pieces
        self.CO2LeakageRates = None
        self.brineLeakageRates = None
        self.g = 9.8  # acceleration due to gravity

        # Reserve space for instance attributes
        self.nSL = None
        self.reservoirBottom = None
        self.depth = None

        self.interface = None
        self.thicknessH = None
        self.initialPressure = None

        self.CO2Volume = None
        self.CO2SaturationAq = None

        self.CO2MobilityAq = None
        self.brineMobilityAq = None

        self.CO2MobilityShale = None
        self.brineMobilityShale = None
        self.effectiveMobility = None

        self.CO2Q = None
        self.brineQ = None
        self.Lambda = None

    def find(self, output_dir):
        """ Find solution of the ROM corresponding to the provided parameters."""
        from sys import platform as sys_platform

        if sys_platform == "win32":
            # For Windows
            from .Libraries.Windows_x86_64 import salsa2 as salsa
        elif sys_platform in ['linux', 'linux2']:
            # For Linux
            from .Libraries.Linux_x86_64 import salsa2 as salsa
        else:
            # For MacOS
            import platform

            processor = platform.processor()

            if processor == 'arm':
                from .Libraries.MacOS_ARM_Apple_M1_chip import salsa2 as salsa
            else:
                # For MacOS
                from .Libraries.MacOS_x86_64 import salsa2 as salsa

        # Make a SALSA folder in the output folder, change directory to it
        original_dir = os.getcwd()

        # The output directory for the entire simulation
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)

        SALSA_output_dir = os.path.join(output_dir, 'SALSA_Output')
        if not os.path.exists(SALSA_output_dir):
            os.mkdir(SALSA_output_dir)

        # The folder specifically made for SALSA output from a deterministic simulation
        folder_name = 'realization_0'

        if self.parameters.stochastic and self.parameters.realizations is not None:
            # For a stochastic simulation, the realizations range from 1 to the 
            # total number of realizations
            continue_test = True
            for real in range(self.parameters.realizations):
                if continue_test:
                    folder_name = 'realization_{:.0f}'.format(real + 1)
                    SALSA_output_dir = os.path.join(output_dir, 'SALSA_Output', folder_name)

                    # If this folder does not exist yet but the previous folders
                    # did, then this is the correct realization number.
                    if not os.path.exists(SALSA_output_dir):
                        continue_test = False

        elif self.parameters.stochastic and self.parameters.realizations is None:
            warning_msg = ''.join([
                'A SALSA component was set up to run a stochastic simulation ',
                '(salsa.stochastic was True, for a component named salsa), ',
                'but the component was not given the number of realizations ',
                'used (salsa.realizations was left as None). Because the ',
                'number of realizations was not given, the output in the ',
                'SALSA_Output folder will be rewritten by each realization.'])

            logging.warning(warning_msg)

        SALSA_output_dir = os.path.join(output_dir, 'SALSA_Output', folder_name)

        if not os.path.exists(SALSA_output_dir):
            os.mkdir(SALSA_output_dir)

        # Change to the SALSA output directory
        os.chdir(SALSA_output_dir)

        def salsa_wrap(dcy):
            """
            Calls and runs the SALSA model.
            """
            [dcy['haqt'], dcy['saqf'], dcy['faqt'], dcy['cfaqt'], dcy['qctvt'],
        	dcy['cfct'], dcy['fcte_ind'], dcy['chead'], dcy['cxout'],
            dcy['cyout']] = salsa.sals2(
        #------------All variables-----------------------------#
        		dcy['mtype'], dcy['bl'], dcy['tl'], dcy['baq'],
        		dcy['hconx'], dcy['ansr'], dcy['ss'], dcy['rho'], dcy['hi'],
        		dcy['gamma'], dcy['baqt'], dcy['hconp'], dcy['ssp'], dcy['gamma0'],
        		dcy['gammat'], dcy['botb'], dcy['topb'], dcy['hbot'], dcy['htop'],
        		dcy['dt1'], dcy['x1'], dcy['y1'],
        		dcy['riw'], dcy['q'], dcy['x2'], dcy['y2'], dcy['rw'],
        		dcy['kww'], dcy['rwa'], dcy['kw'], dcy['stata'], dcy['staca'],
        		dcy['slw'], dcy['aql'], dcy['qlw'], dcy['nint'], dcy['dt3'],
        		dcy['xba'], dcy['yba'], dcy['xaq'],
        		dcy['yaq'], dcy['flag_mesh'], dcy['xcent'], dcy['ycent'],
        		dcy['xmax'], dcy['ymax'], dcy['exp_plot'], dcy['ni'],
        		dcy['nj'], dcy['ncirc'], dcy['rmax'], dcy['expr'],
        #-------------------output parameters-------------------#
        		dcy['haqt'], dcy['saqf'], dcy['faqt'], dcy['cfaqt'], dcy['qctvt'],
        		dcy['cfct'], dcy['fcte_ind'], dcy['chead'],
        		dcy['cxout'], dcy['cyout'],
        #---------------Array sizes-(In proper order)-------------#
        		dcy['naqp'], dcy['nabv'], dcy['nxyz'],
        		dcy['naq'], dcy['naqt'], dcy['nlw'], dcy['niw'],
        		dcy['nt'], dcy['nb'], dcy['nab'], dcy['nper'])

            return dcy

        direc = dict(
            mtype=FIXED_MODEL_TYPE, bl=self.parameters.bottomLayerType,
            tl=self.parameters.topLayerType, naq=self.parameters.numberOfAquiferLayers,
            baq=self.parameters.aquiferThickness, hconx=self.parameters.aquiferHorizK,
            ansr=self.parameters.aquiferANSR, ss=self.parameters.aquiferSS,
            rho=self.parameters.aquiferFluidDensity, hi=self.parameters.aquiferHead,
            gamma=self.parameters.aquiferGamma, naqt=self.parameters.numberOfShaleLayers,
            baqt=self.parameters.shaleThickness, hconp=self.parameters.shaleVertK,
            ssp=self.parameters.shaleSS, gamma0=self.parameters.shaleInitialGamma,
            gammat=self.parameters.shaleGamma, botb=self.parameters.bottomBoundaryCond,
            topb=self.parameters.topBoundaryCond, hbot=self.parameters.bottomBoundaryHead,
            htop=self.parameters.topBoundaryHead, niw=self.parameters.numberOfActiveWells,
            nper=self.parameters.numberOfPeriods, dt1=self.parameters.periodEndTimes,
            x1=self.parameters.activeWellCoordx, y1=self.parameters.activeWellCoordy,
            riw=self.parameters.activeWellRadii, q=self.parameters.activeWellQ,
            nlw=self.parameters.numberOfLeakingWells, x2=self.parameters.leakingWellCoordx,
            y2=self.parameters.leakingWellCoordy, rw=self.parameters.leakingWellAqRadii,
            kww=self.parameters.leakingWellAqK, rwa=self.parameters.leakingWellShaleRadii,
            kw=self.parameters.leakingWellShaleK, stata=self.parameters.leakingWellShaleStat,
            staca=self.parameters.leakingWellAqStat, slw=self.parameters.leakingWellQAq,
            qlw=self.parameters.leakingWellQ,  nint=self.parameters.numberOfLaplaceTerms,
            nt=len(self.parameters.time_array), dt3=self.parameters.time_array,
            flag_mesh=self.parameters.flagMesh, xcent=self.parameters.xCent,
            ycent=self.parameters.yCent, xmax=self.parameters.xMax,
            ymax=self.parameters.yMax, exp_plot=self.parameters.gridExp,
            ni=self.parameters.numberOfNodesXDir, nj=self.parameters.numberOfNodesYDir,
            ncirc=self.parameters.numberOfRadialGrids, rmax=self.parameters.radialZoneRadius,
            expr=self.parameters.radialGridExp, naqp=self.parameters.numberOfAquiferNames,
            aql=self.parameters.aquiferNamesContourPlots, nb=self.parameters.numberOfHeadLocs,
            xba=self.parameters.aquiferCoordx, yba=self.parameters.aquiferCoordy,
            nab=self.parameters.numberOfHeadProfiles, nabv=self.parameters.numberOfVerticalPoints,
            xaq=self.parameters.shaleCoordx, yaq=self.parameters.shaleCoordy,
            nxyz=self.parameters.numberOfContourPlotNodes,
            x=self.parameters.contourPlotCoordx, y=self.parameters.contourPlotCoordy,
        # Outputs ------------------------------------------------------------#
            haqt=np.zeros((len(self.parameters.time_array),
                           self.parameters.numberOfVerticalPoints + 1,
                           self.parameters.numberOfShaleLayers,
                           self.parameters.numberOfHeadProfiles)),
            saqf=np.zeros((len(self.parameters.time_array),
                           self.parameters.numberOfHeadProfiles,
                           self.parameters.numberOfAquiferLayers)),
            faqt=np.zeros((len(self.parameters.time_array),
                           self.parameters.numberOfAquiferLayers * 2)),
            cfaqt=np.zeros((len(self.parameters.time_array),
                            self.parameters.numberOfAquiferLayers * 2)),
            qctvt=np.zeros((len(self.parameters.time_array),
                            self.parameters.numberOfAquiferLayers)),
            cfct=np.zeros((len(self.parameters.time_array),
                           self.parameters.numberOfAquiferLayers)),
            fcte_ind=np.zeros((len(self.parameters.time_array),
                               self.parameters.numberOfLeakingWells,
                               self.parameters.numberOfAquiferLayers)),
            chead=np.zeros((len(self.parameters.time_array),
                            self.parameters.numberOfAquiferNames,
                            self.parameters.numberOfContourPlotNodes)),
            cxout=np.zeros((self.parameters.numberOfContourPlotNodes)),
            cyout=np.zeros((self.parameters.numberOfContourPlotNodes)))

        direc = salsa_wrap(direc)

        readme_file = 'SALSA_OUTPUT_README.txt'

        with open(readme_file, 'w', newline='') as f:
            writer = csv.writer(f)
            for row in README_MSG:
                writer.writerow([row])
        f.close()

        # Go back to the original directory
        os.chdir(original_dir)

        # Store the output
        self.haqt = direc['haqt']

        self.saqf = direc['saqf']

        self.faqt = direc['faqt']

        self.cfaqt = direc['cfaqt']

        self.qctvt = direc['qctvt']

        self.cfct = direc['cfct']

        self.fcte_ind = direc['fcte_ind']

        self.chead = direc['chead']

        self.cxout = direc['cxout']

        self.cyout = direc['cyout']


if __name__ == "__main__":
    pass

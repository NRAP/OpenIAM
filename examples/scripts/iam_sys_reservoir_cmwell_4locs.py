'''
Here, the system model contains two types of linked component models: the Analytical 
Reservoir model and the Cemented Wellbore. The reservoir model provides pressures 
and CO2 saturations to the wellbore model, which then calculates the leakage rates 
of brine and CO2 into two aquifers overlying the reservoir as well as the atmosphere.

Results are evaluated at four locations. The first two locations have preset 
coordinates, with [x, y] values of [100 m, 100 m] and [540 m, 630 m]. The third 
and fourth locations have randomly generated x and y coordinates.

This example was created to have a setup similar to control file example 1a 
(ControlFile_ex1a.yaml).

Examples of run:
$ python iam_sys_reservoir_cmwell_4locs.py
'''

import matplotlib.pyplot as plt
import numpy as np

from openiam.components.iam_base_classes import SystemModel
from openiam.components.analytical_reservoir_component import AnalyticalReservoir
from openiam.components.cemented_wellbore_component import CementedWellbore
from openiam.matk import pyDOE

if __name__=='__main__':

    # Create system model
    num_years = 10.
    time_array = 365.25 * np.arange(0.0, num_years+1)
    sm_model_kwargs = {'time_array': time_array}   # time is given in days
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    aress = []
    cws = []

    # Pre-determined x and y coordinates
    set_well_x_coords = [[100, 100], [540, 630]]

    # x and y boundaries for the randomly placed well area
    rand_x_min = 150.
    rand_x_max = 250.
    rand_y_min = 200.
    rand_y_max = 300.

    num_rand_wells = 2 # 2 random wells plus the two above gives 4 total wells
    # Well locations
    rand_well_fracs = pyDOE.lhs(2,samples=num_rand_wells)
    rand_well_coords = rand_well_fracs.copy()

    # Set the random x values
    rand_well_coords[:, 0] = rand_x_min + (
        rand_well_coords[:, 0] * (rand_x_max - rand_x_min))

    # Set the random y values
    rand_well_coords[:, 1] = rand_y_min + (
        rand_well_coords[:, 1] * (rand_y_max - rand_y_min))
    
    well_xys = np.array([
        set_well_x_coords[0], set_well_x_coords[1], 
        rand_well_coords[0].tolist(), rand_well_coords[1].tolist()])

    print('Well [x, y] coordnates (m): ', '\n', well_xys)
    
    for i,crds in enumerate(well_xys):
        # Add reservoir components
        # Note: This setup is identical to the Control File 1a
        aress.append(sm.add_component_model_object(
            AnalyticalReservoir(name='ares'+str(i), parent=sm,
                                injX=10., injY=20., locX=crds[0], locY=crds[1])))

        # Stratigraphy parameters that get added to all created components
        aress[-1].add_par('numberOfShaleLayers', value=3, vary=False)
        aress[-1].add_par('shale1Thickness', value=525.0, vary=False)
        aress[-1].add_par('shale2Thickness', value=475.0, vary=False)
        aress[-1].add_par('shale3Thickness', value=11.2, vary=False)
        aress[-1].add_par('aquifer1Thickness', value=22.4, vary=False)
        aress[-1].add_par('aquifer2Thickness', value=19.2, vary=False)
        aress[-1].add_par('reservoirThickness', value=51.2, vary=False)
        
        #reservoir component model parameters
        aress[-1].add_par('injRate', value=0.1, vary=False)
        aress[-1].add_par('reservoirRadius', value=5000, vary=False)
        aress[-1].add_par('brineResSaturation', value=0.15, vary=False)
        aress[-1].add_par('logResPerm', value=-12.75, vary=False)
        
        # Add observations of reservoir component model to be used by the next component
        aress[-1].add_obs_to_be_linked('pressure')
        aress[-1].add_obs_to_be_linked('CO2saturation')

        # Add observations of reservoir component model
        aress[-1].add_obs('pressure')
        aress[-1].add_obs('CO2saturation')
        aress[-1].add_obs('mass_CO2_reservoir')

        # create cemented wellbore component
        cws.append(sm.add_component_model_object(
            CementedWellbore(name='cw'+str(i), parent=sm)))

        #deterministic parameter of cemented wellbore component
        cws[-1].add_par('logWellPerm', value=-13., vary=False)

        #composite parameters that are implicitly added to the cemented wellbore component
        #in the Control script
        cws[-1].add_composite_par(
            'wellDepth', expr = aress[-1].deterministic_pars['shale1Thickness'].name 
            + '+' + aress[-1].deterministic_pars['shale2Thickness'].name 
            + '+' + aress[-1].deterministic_pars['shale3Thickness'].name 
            + '+' + aress[-1].deterministic_pars['aquifer1Thickness'].name 
            + '+' + aress[-1].deterministic_pars['aquifer2Thickness'].name)
        cws[-1].add_composite_par(
            'depthRatio', expr = '(' + aress[-1].deterministic_pars['shale2Thickness'].name 
            + '+' + aress[-1].deterministic_pars['shale3Thickness'].name
            + '+' + aress[-1].deterministic_pars['aquifer2Thickness'].name
            + '+' + aress[-1].deterministic_pars['aquifer1Thickness'].name 
            + '/2)' + '/' + cws[-1].composite_pars['wellDepth'].name)

        # Add keyword arguments linked to the output provided by reservoir model
        cws[-1].add_kwarg_linked_to_obs('pressure', aress[-1].linkobs['pressure'])
        cws[-1].add_kwarg_linked_to_obs('CO2saturation', aress[-1].linkobs['CO2saturation'])
        cws[-1].add_obs('brine_aquifer1')
        cws[-1].add_obs('CO2_aquifer1')
        cws[-1].add_obs('brine_aquifer2')
        cws[-1].add_obs('CO2_aquifer2')
        cws[-1].add_obs('CO2_atm')
        cws[-1].add_obs('brine_atm')

    # Run forward simulation
    sm.forward()

    # Adjust the way plots will be formatted
    font = {'family': 'Arial',
            'weight': 'normal',
            'size': 10}
    plt.rc('font', **font)

    fig1, axs1 = plt.subplots(nrows=2,ncols=2, figsize=(10, 8))

    fig2, axs2 = plt.subplots(nrows=2,ncols=2, figsize=(10, 8))

    fig3, axs3 = plt.subplots(nrows=2,ncols=2, figsize=(10, 8))

    fig4, axs4 = plt.subplots(nrows=2,ncols=2, figsize=(10, 8))

    # Print and plot some of the observations
    for i, cw in enumerate(cws):
        ares = aress[i]

        # Collect observations
        pressure = sm.collect_observations_as_time_series(ares, 'pressure')
        CO2saturation = sm.collect_observations_as_time_series(ares, 'CO2saturation')

        CO2_leakrates_aq1 = sm.collect_observations_as_time_series(cw, 'CO2_aquifer1')
        CO2_leakrates_aq2 = sm.collect_observations_as_time_series(cw, 'CO2_aquifer2')
        brine_leakrates_aq1 = sm.collect_observations_as_time_series(cw, 'brine_aquifer1')
        brine_leakrates_aq2 = sm.collect_observations_as_time_series(cw, 'brine_aquifer2')

        # Print results: CO2 and brine leakage rates and pressure/saturation at the wellbore
        print('------------------------------------------------------------------')
        print('CO2 leakage rates to aquifer 1 at wellbore {}:'.format(i+1), CO2_leakrates_aq1, sep='\n')
        print('CO2 leakage rates to aquifer 2 at wellbore {}:'.format(i+1), CO2_leakrates_aq2, sep='\n')
        print('Brine leakage rates to aquifer 1 at wellbore {}:'.format(i+1), brine_leakrates_aq1, sep='\n')
        print('Brine leakage rates to aquifer 2 at wellbore {}:'.format(i+1), brine_leakrates_aq2, sep='\n')
        print('Pressure at wellbore {}:'.format(i+1), sm.collect_observations_as_time_series(ares,'pressure'), sep='\n')
        print('CO2 saturation at wellbore {}:'.format(i+1), sm.collect_observations_as_time_series(ares, 'CO2saturation'), sep='\n')

        # Plot CO2 and brine leakage rates along the wellbore
        if (i > 1) :
            y_ax = 1
        else:
            y_ax = 0

        axs1[y_ax,i%2].plot(sm.time_array/365.25, CO2_leakrates_aq1, color='#000055',
                 linewidth=2, label="aquifer 1")
        axs1[y_ax,i%2].plot(sm.time_array/365.25, CO2_leakrates_aq2, color='#FF0066',
                 linewidth=2, label="aquifer 2")
        axs1[y_ax,i%2].legend()
        axs1[y_ax,i%2].set_xlabel('Time, t [years]', fontweight='bold', fontsize=12)
        axs1[y_ax,i%2].set_ylabel('Leakage rates, q [kg/s]', fontweight='bold', fontsize=12)
        axs1[y_ax,i%2].set_title(r'Leakage of CO$_2$ at Wellbore {}'.format(i+1), 
                                 fontweight='bold', fontsize=12)
        axs1[y_ax,i%2].ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)

        fig1.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9,
                             wspace=0.25, hspace=0.33)

        axs2[y_ax,i%2].plot(sm.time_array/365.25, brine_leakrates_aq1, color='#000055',
                 linewidth=2, label="aquifer 1")
        axs2[y_ax,i%2].plot(sm.time_array/365.25, brine_leakrates_aq2, color='#FF0066',
                 linewidth=2, label="aquifer 2")
        axs2[y_ax,i%2].legend()
        axs2[y_ax,i%2].set_xlabel('Time, t [years]', fontweight='bold', fontsize=12)
        axs2[y_ax,i%2].set_ylabel('Leakage rates, q [kg/s]', fontweight='bold', fontsize=12)
        axs2[y_ax,i%2].set_title(r'Leakage of Brine at Wellbore {}'.format(i+1), 
                                 fontweight='bold', fontsize=12)
        axs2[y_ax,i%2].ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)

        fig2.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9,
                             wspace=0.25, hspace=0.33)

        axs3[y_ax,i%2].plot(sm.time_array/365.25, pressure, color='C0',
                 linewidth=2)
        axs3[y_ax,i%2].set_xlabel('Time, t [years]', fontweight='bold', fontsize=12)
        axs3[y_ax,i%2].set_ylabel('Pressure [Pa]', fontweight='bold', fontsize=12)
        axs3[y_ax,i%2].set_title(r'Reservoir Pressures at Wellbore {}'.format(i+1), 
                                 fontweight='bold', fontsize=12)
        axs3[y_ax,i%2].ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)

        fig3.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9,
                             wspace=0.25, hspace=0.33)

        axs4[y_ax,i%2].plot(sm.time_array/365.25, CO2saturation, color='C3',
                 linewidth=2)
        axs4[y_ax,i%2].set_xlabel('Time, t [years]', fontweight='bold', fontsize=12)
        axs4[y_ax,i%2].set_ylabel('CO$_2$ Saturation [-]', fontweight='bold', fontsize=12)
        axs4[y_ax,i%2].set_title(r'Reservoir CO$_2$ Saturations at Wellbore {}'.format(i+1), 
                                 fontweight='bold', fontsize=12)
        axs4[y_ax,i%2].ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)

        fig4.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9,
                             wspace=0.25, hspace=0.33)

    plt.show()

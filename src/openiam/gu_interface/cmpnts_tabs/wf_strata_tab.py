"""
Module contains several methods needed for creating tab (page) in GUI
for MultisegmentedWellbore component. Methods read and write dictionaries
needed for control file interface yaml files.
"""

import os
import sys
from re import split

import tkinter as tk
from tkinter import ttk
from tkinter import (StringVar, DoubleVar, IntVar, BooleanVar)

import Pmw

from openiam.components.iam_base_classes import IAM_DIR
from openiam.gu_interface.dictionarydata import componentVars, componentChoices
from openiam.gu_interface.dictionarydata import (connectionsDictionary,
                                                 componentTypeDictionary)
from openiam.gu_interface.dictionarydata import DISTRIBUTION_OPTIONS
from openiam.gu_interface.dictionarydata import connections

from openiam.gu_interface.dictionarydata import LABEL_FONT
from openiam.gu_interface.dictionarydata import (
    STRATA_PARAMETER_LABEL_WIDTH, DISTRIBUTION_MENU_WIDTH,
    DISTRIBUTION_ARG_LABEL_WIDTH, DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
    OUTPUT_LABEL_WIDTH1, PARAMETER_FRAME_PADX, CB_PADX)


STRATA_PARAMETERS = ['numberOfShaleLayers', 'datumPressure', 'reservoirThickness']

# Set Multisegmented Wellbore parameter names and value, min, max, second value, mean, std
STRATA_PARAMETER_VALUES = {
    'shale': [100, 1, 100, 120, 200, 25, 1, 1600],
    'aquifer': [75, 1, 100, 100, 75, 10, 1, 1600],
    'reservoir': [50, 1, 100, 75, 50, 10, 1, 1600]}


def convert_tab_vars():
    componentVars['strata']['Params'] = componentVars['wf_strata']['wf_Params']

def deconvert_tab_vars():
    componentVars['wf_strata']['wf_Params'] = componentVars['strata']['Params']

def add_widgets(controller, tab, toolTip):
    """ Add widgets to the Stratigraphy tab."""

    # Setup stratigraphy related global variables
    componentVars['wf_strata']['wf_Params'] = {}
    componentVars['wf_strata']['wf_Params']['numberOfShaleLayers'] = IntVar()
    counts = list(range(3, 31))
    componentVars['wf_strata']['wf_Params']['numberOfShaleLayers'].set(counts[0])

    componentVars['wf_strata']['wf_Params']['datumPressure'] = DoubleVar()
    componentVars['wf_strata']['wf_Params']['datumPressure'].set(101325)

    layers_par_names = [
        'shale3Thickness', 'aquifer2Thickness', 'shale2Thickness',
        'aquifer1Thickness', 'shale1Thickness', 'reservoirThickness']

    for par_name in layers_par_names:
        short_par_name = get_par_short_name(par_name)

        componentVars['wf_strata']['wf_Params'][par_name] = {}
        for key in ['distribution', 'values', 'weights']:
            componentVars['wf_strata']['wf_Params'][par_name][key] = StringVar()

        componentVars['wf_strata']['wf_Params'][par_name]['distribution'].set(
            DISTRIBUTION_OPTIONS[0])
        componentVars['wf_strata']['wf_Params'][par_name]['weights'].set('0.5, 0.5')
        componentVars['wf_strata']['wf_Params'][par_name]['values'].set(
            '{}, {}'.format(STRATA_PARAMETER_VALUES[short_par_name][0],
                            STRATA_PARAMETER_VALUES[short_par_name][3]))

        for key in ['value', 'min', 'max', 'mode', 'mean', 'std']:
            componentVars['wf_strata']['wf_Params'][par_name][key] = DoubleVar()

        componentVars['wf_strata']['wf_Params'][par_name]['value'].set(
            STRATA_PARAMETER_VALUES[short_par_name][0])
        componentVars['wf_strata']['wf_Params'][par_name]['min'].set(
            STRATA_PARAMETER_VALUES[short_par_name][1])
        componentVars['wf_strata']['wf_Params'][par_name]['max'].set(
            STRATA_PARAMETER_VALUES[short_par_name][2])
        componentVars['wf_strata']['wf_Params'][par_name]['mode'].set(
            STRATA_PARAMETER_VALUES[short_par_name][0])
        componentVars['wf_strata']['wf_Params'][par_name]['mean'].set(
            STRATA_PARAMETER_VALUES[short_par_name][4])
        componentVars['wf_strata']['wf_Params'][par_name]['std'].set(
            STRATA_PARAMETER_VALUES[short_par_name][5])

    # Stratigraphy tab label
    strata_label = ttk.Label(
        tab, text="Stratigraphy", font=LABEL_FONT)
    strata_label.grid(row=0, column=0, sticky='w', pady=(5, 10))

    # Parameters frames
    par_frames = {}
    par_frames['wf_numberOfShaleLayers'] = tk.Frame(tab)
    par_frames['wf_numberOfShaleLayers'].grid(row=1, column=0, sticky='w',
                                           padx=PARAMETER_FRAME_PADX)

    par_label = ttk.Label(par_frames['wf_numberOfShaleLayers'],
                          width=STRATA_PARAMETER_LABEL_WIDTH,
                          text="Number of shale layers:")
    val_menu = tk.OptionMenu(
        par_frames['wf_numberOfShaleLayers'],
        componentVars['wf_strata']['wf_Params']['numberOfShaleLayers'], *counts,
        command=lambda n: add_stratigraphy_layers(n, controller))
    val_menu.config(width=DISTRIBUTION_MENU_WIDTH)
    par_label.grid(row=0, column=0, sticky='w', padx=5)
    val_menu.grid(row=0, column=1, padx=5)
    toolTip.bind(val_menu,
                 'Select the total number of shale layers for simulation.')

    hint_frame = tk.Frame(tab)
    hint_frame.grid(row=2, column=0, sticky='w', pady=10)
    controller.strata_layers_image = tk.PhotoImage(
        file=os.path.join(IAM_DIR, 'src', 'openiam', 'gu_interface',
                          'images', 'ShaleLayers.gif'))
    options = [controller.strata_layers_image]
    hint_menu_var = StringVar()
    hint_menu_var.set('Stratigraphy layers')
    hint_menu = tk.OptionMenu(hint_frame, hint_menu_var, *options)
    hint_menu.grid(row=0, column=0, sticky='w', padx=5)
    menu = hint_menu['menu']
    menu.delete("0", tk.END)
    menu.add_command(image=controller.strata_layers_image)
    toolTip.bind(hint_menu,
                 'Click to check the stratigraphy layers enumeration.')

    # Surface pressure frame
    par_frames['wf_datumPressure'] = tk.Frame(tab)
    par_frames['wf_datumPressure'].grid(row=3, column=0, sticky='w', pady=5,
                                     padx=PARAMETER_FRAME_PADX)

    par_label = ttk.Label(par_frames['wf_datumPressure'],
                          width=STRATA_PARAMETER_LABEL_WIDTH,
                          text="Land surface pressure [Pa]:")
    val_field = tk.Entry(
        par_frames['wf_datumPressure'],
        textvariable=componentVars['wf_strata']['wf_Params']['datumPressure'])
    val_field.config(width=DISTRIBUTION_ARG_TEXTFIELD_WIDTH)
    par_label.grid(row=0, column=0, sticky='w', padx=5)
    val_field.grid(row=0, column=1, padx=5)
    toolTip.bind(val_field,
                 'Enter pressure at the top of the uppermost shale layer.')

    # Create frames for other parameters
    for ind, par_name in enumerate(layers_par_names):
        short_par_name = get_par_short_name(par_name)
        par_frames["wf_" + par_name] = tk.Frame(tab)
        par_frames["wf_" + par_name].grid(row=ind+4, column=0, sticky='w',
                                  padx=PARAMETER_FRAME_PADX)

        controller.setup_parameter_frame(
            par_frames["wf_" + par_name], par_name,
            {'lower_bound': STRATA_PARAMETER_VALUES[short_par_name][6],
             'upper_bound': STRATA_PARAMETER_VALUES[short_par_name][7]},
            get_par_label_text(par_name),
            get_tooltip_part(par_name),
            STRATA_PARAMETER_LABEL_WIDTH, DISTRIBUTION_MENU_WIDTH,
            DISTRIBUTION_ARG_LABEL_WIDTH, DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
            DISTRIBUTION_OPTIONS, componentVars['wf_strata']['wf_Params'][par_name],
            'strata', toolTip)

    controller.wf_strata_par_frames = par_frames
    controller.wf_strata_tab = tab

def add_stratigraphy_layers(num_shale_layers, controller):
    """
    Add the number of shale and aquifer layers based on user selection.
    """
    tool_tip = Pmw.Balloon(controller)
    if num_shale_layers >= 7:
        controller.wf_strata_scanv.config(
            scrollregion=(0, 0, 0, 550+(num_shale_layers-6)*63))
    else:
        controller.wf_strata_scanv.config(
            scrollregion=(0, 0, 0, 0))

    par_list = list(controller.wf_strata_par_frames.keys())
    for par_nm in par_list:
        # If parameter describes thickness of shale or aquifer layer
        if 'Thickness' in par_nm:
            for widget in controller.wf_strata_par_frames[par_nm].winfo_children():
                # Delete all widgets from parameter (aquifer or shale thickness) frame
                tool_tip.unbind(widget)
                widget.destroy()
            frame = controller.wf_strata_par_frames.pop(par_nm)
            frame.destroy()

    new_layers_par_names = [
        'shale{}Thickness'.format(ind) for ind in range(4, num_shale_layers+1)] + [
            'aquifer{}Thickness'.format(ind) for ind in range(3, num_shale_layers)]

    # Create variables for (possibly) new parameters
    for par_name in new_layers_par_names:
        short_par_name = get_par_short_name(par_name)

        componentVars['wf_strata']['wf_Params'][par_name] = {}
        for key in ['distribution', 'values', 'weights']:
            componentVars['wf_strata']['wf_Params'][par_name][key] = StringVar()

        componentVars['wf_strata']['wf_Params'][par_name]['distribution'].set(
            DISTRIBUTION_OPTIONS[0])
        componentVars['wf_strata']['wf_Params'][par_name]['weights'].set('0.5, 0.5')
        componentVars['wf_strata']['wf_Params'][par_name]['values'].set(
            '{}, {}'.format(STRATA_PARAMETER_VALUES[short_par_name][0],
                            STRATA_PARAMETER_VALUES[short_par_name][3]))

        for key in ['value', 'min', 'max', 'mode', 'mean', 'std']:
            componentVars['wf_strata']['wf_Params'][par_name][key] = DoubleVar()

        componentVars['wf_strata']['wf_Params'][par_name]['value'].set(
            STRATA_PARAMETER_VALUES[short_par_name][0])
        componentVars['wf_strata']['wf_Params'][par_name]['min'].set(
            STRATA_PARAMETER_VALUES[short_par_name][1])
        componentVars['wf_strata']['wf_Params'][par_name]['max'].set(
            STRATA_PARAMETER_VALUES[short_par_name][2])
        componentVars['wf_strata']['wf_Params'][par_name]['mode'].set(
            STRATA_PARAMETER_VALUES[short_par_name][0])
        componentVars['wf_strata']['wf_Params'][par_name]['mean'].set(
            STRATA_PARAMETER_VALUES[short_par_name][4])
        componentVars['wf_strata']['wf_Params'][par_name]['std'].set(
            STRATA_PARAMETER_VALUES[short_par_name][5])

    # Define names of layers parameters
    layers_par_names = ['shale{}Thickness'.format(num_shale_layers)]
    for ind in range(1, num_shale_layers):
        layers_par_names = layers_par_names + [
            'aquifer{}Thickness'.format(num_shale_layers-ind),
            'shale{}Thickness'.format(num_shale_layers-ind)]
    layers_par_names = layers_par_names + ['reservoirThickness']

    # Create frames for other parameters
    for ind, par_name in enumerate(layers_par_names):
        short_par_name = get_par_short_name(par_name)
        controller.wf_strata_par_frames["wf_" + par_name] = tk.Frame(controller.wf_strata_tab)
        controller.wf_strata_par_frames["wf_" + par_name].grid(
            row=ind+4, column=0, sticky='w', padx=PARAMETER_FRAME_PADX)

        controller.setup_parameter_frame(
            controller.wf_strata_par_frames["wf_" + par_name], par_name,
            {'lower_bound': STRATA_PARAMETER_VALUES[short_par_name][6],
             'upper_bound': STRATA_PARAMETER_VALUES[short_par_name][7]},
            get_par_label_text(par_name),
            get_tooltip_part(par_name),
            STRATA_PARAMETER_LABEL_WIDTH, DISTRIBUTION_MENU_WIDTH,
            DISTRIBUTION_ARG_LABEL_WIDTH, DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
            DISTRIBUTION_OPTIONS, componentVars['wf_strata']['wf_Params'][par_name],
            'strata', tool_tip)

def get_par_label_text(name):
    if 'aquifer' in name:
        ind = split('Thickness', name)[0][7:]
        return 'Aquifer {} thickness [m]:'.format(ind)
    elif 'shale' in name:
        ind = split('Thickness', name)[0][5:]
        return 'Shale {} thickness [m]:'.format(ind)
    else:
        return 'Reservoir thickness [m]:'

def get_tooltip_part(name):
    if 'aquifer' in name:
        return 'aquifer thickness'
    elif 'shale' in name:
        return 'shale thickness'
    else:
        return 'reservoir thickness'

def get_par_short_name(name):
    if 'aquifer' in name:
        return 'aquifer'
    elif 'shale' in name:
        return 'shale'
    else:
        return 'reservoir'

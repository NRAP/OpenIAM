# OpenIAM Control File Example 2b
#
# This example uses pressures and CO2 saturations from an AnalyticalReservoir 
# component to drive the leakage rate calculations of a MultisegmentedWellbore 
# component. Furthermore, this example demonstrates how the user can control 
# the way that these components calculate the initial pressures of each unit. 
# Both components accept the parameters shalePressureGrad and aquiferPressureGrad. 
# These parameters control the rate at which pressure increases with depth across 
# each unit (e.g., shale2PressureGrad or aquifer1PressureGrad). The datumPressure 
# parameter (linked to the Stratigraphy component) sets the pressure at the 
# surface. The AnalyticalReservoir and MultisegmentedWellbore components 
# calculate the initial pressures at the bottom of each aquifer as well as the 
# top and bottom of the reservoir.
#
# Being able to control the initial pressures is very important, because it impacts 
# how the Multisegmented Wellbore responds to reservoir data. For example, if reservoir 
# simulation data were imported using the Lookup Table Reservoir, the initial pressures 
# at the top of the reservoir in those simulations needs to be consistent with the 
# initial pressures calculated by the Multisegmented Wellbore component. Otherwise, if 
# the Multisegmented Wellbore calculated a higher initial pressure than the value supplied, 
# the wellbore could calculate negative leakage (implying fluid moves from the overlying 
# aquifers and down to the reservoir). Such behaviors, if unintended, can be prevented by 
# controlling the way the Multisegmented Wellbore calculates initial pressures.
#
# The approach described above is used to set the initial pressures at the depths 
# considered, unless the user provides aquiferInitialPressure or reservoirInitialPressure 
# parameter values. These parameters can be used to force the component to set the 
# initial pressure at the bottom of an aquifer (e.g., aquifer1InitialPressure for aquifer 1) 
# or the top of the reservoir (reservoirInitialPressure) to a specific value. This functionality 
# can be important if the user needs to ensure that the component matches the setup of 
# another component it is connected to (e.g., a MultisegmentedWellbore connected to 
# a LookupTableReservoir). Setting specific pressures must be done with caution, however, 
# as strange behaviors can arise if the user provides inappropriate inputs. Furthermore, 
# the user should ensure that the pressures used are not unrealistic for the depths and 
# conditions used (e.g., having a pressure so high that the unit would likely be fractured, 
# violating the assumptions of the models used).
#
# Because the entries 'SaveInitialConditions: True' are included for the AnalyticalReservoir and 
# MultisegmentedWellbore, both components will save .csv files containing the initial pressures 
# used for each depth considered. The files are saved to the folder 'csv_files' in the output 
# directory specified. Specifically, the MultisegmentedWellbore files are saved in a folder 
# called 'MultisegmentedWellbore_Initial_Pressures' in the 'csv_files' directory, while the 
# AnalyticalReservoir files are saved in a folder called 'AnalyticalReservoir_Initial_Pressures'. 
# If the 'SaveInitialConditions' option is not provided, the files are not saved. The file names will 
# start with the name of the component (including location designations, like '_000' for location 
# 0) and end with '_initial_pressures.csv'. For example, the .csv file saved by the 
# AnalyticalReservoir in this example will be 'AnalyticalReservoir1_000_initial_pressures.csv'.
#
# If using connected AnalyticalReservoir and MultisegmentedWellbore components, the user 
# should ensure that both components have the same treatment of pressures. For example, 
# it would be a problem if the AnalyticalReservoir was set up to use a certain pressure 
# at the top of the reservoir but the MultisegmentedWellbore used a different pressure for 
# the top of the reservoir.
#
# The MultisegmentedWellbore is also given the entry 'NoNegativeLeakage: True'. When 'NoNegativeLeakage' 
# is given as True, any negative leakage rates produced by the component will be set to 0 kg/s. 
# Negative leakage (i.e., fluid leaving the unit rather than entering it) can occur if, for example, 
# another unit is under-pressured. In that case, fluid may leave other units, travel through the wellbore, 
# and enter the under-presussured unit. The user may want to prevent negative leakage rates, however, 
# if the behavior does not match the intended setup. If 'NoNegativeLeakage' is not provided as True, 
# the default approach is to allow negative leakage rates.
#
# To run this file, use the command (\ for Windows / for Mac or Linux):
#  python ../../src/openiam/components/openiam_cf.py --file ControlFile_ex2b.yaml
#
# Last Modified: June, 2023
#-------------------------------------------------
ModelParams:
    EndTime: 10
    TimeStep: 1.0
    Analysis: forward
    Components: [AnalyticalReservoir1, msw1]
    OutputDirectory: output/output_ex2b_{datetime}
    OutputType: 1  # 0 - row-wise; 1 - column-wise
    GenerateLocationsFiles: False
    GenerateOutputFiles: True
    GenerateCombOutputFile: False
    GenerateStatFiles: True
    Logging: Warning
#-------------------------------------------------
Stratigraphy:
    numberOfShaleLayers: 3
    reservoirThickness: 50
    shale1Thickness: 40.0
    aquifer1Thickness: 45.0
    shale2Thickness: 250.0
    aquifer2Thickness: 45.0
    shale3Thickness: 250
    datumPressure: 101325
#-------------------------------------------------
# AnalyticalReservoir1 is a user defined name for component
# the type AnalyticalReservoir is the ROM model name
#-------------------------------------------------
AnalyticalReservoir1:
    Type: AnalyticalReservoir
    SaveInitialConditions: True
    Parameters:
        injRate: 3
        logResPerm: -12.5
        reservoirRadius: 2500
        brineResSaturation: 0.1
        brineDensity: 1195
        reservoirPressureGrad: 1.1722e+4
        shale1PressureGrad: 1.0771e+4
        aquifer1PressureGrad: 1.0771e+4
        shale2PressureGrad: 1.0282e+4
        aquifer2PressureGrad: 9.792e+3
        # shale3PressureGrad will use the default value of 9.792e+3
        # datumPressure will be set to the input given to the Stratigraphy component
        # The AnalyticalReservoir does not receive shaleInitialPressure or aquiferInitialPressure parameters
        # If reservoirInitialPressure is provided, then the shalePressureGrad and aquiferPressureGrad parameters have no impact on the pressure and CO2saturation outputs
        # reservoirInitialPressure: 6.476e+6      # initial pressure at the top of the reservoir - if provided, can force the component to use a specific pressure
    Outputs: [pressure,
              CO2saturation]
#-------------------------------------------------
msw1:
    Type: MultisegmentedWellbore
    Locations:
        coordx: [100]
        coordy: [100]
    Connection: AnalyticalReservoir1
    AccumulateLeakage: True
    SaveInitialConditions: True
    NoNegativeLeakage: True
    Parameters:
        logWellPerm: -13.5
        brineDensity: 1195
        reservoirPressureGrad: 1.1722e+4
        shale1PressureGrad: 1.0771e+4
        aquifer1PressureGrad: 1.0771e+4
        shale2PressureGrad: 1.0282e+4
        aquifer2PressureGrad: 9.792e+3
        # shale3PressureGrad will use the default value of 9.792e+3
        # datumPressure will be set to the input given to the Stratigraphy component
        # The initial pressures will be calculated as the values shown below, but the user could enforce different values
        # shale3InitialPressure: 2.549e+6         # initial pressure at the bottom of shale 3 - if provided, can force the component to use a specific pressure
        # aquifer2InitialPressure: 2.990e+6       # initial pressure at the bottom of aquifer 2 - if provided, can force the component to use a specific pressure
        # shale2InitialPressure: 5.560e+6 Pa      # initial pressure at the bottom of shale 2 - if provided, can force the component to use a specific pressure
        # aquifer1InitialPressure: 6.045e+6e+06   # initial pressure at the bottom of aquifer 1 - if provided, can force the component to use a specific pressure
        # Do not need to use shale1InitialPressure - the bottom of shale 1 is the same as the top of the reservoir, so the pressure there is set by reservoirInitialPressure
        # reservoirInitialPressure: 6.476e+6      # initial pressure at the top of the reservoir - if provided, can force the component to use a specific pressure
    Outputs: [CO2_aquifer1, brine_aquifer1, 
              CO2_aquifer2, brine_aquifer2]
#-------------------------------------------------
Plots:
    pressure:
        TimeSeries: [pressure]
    CO2saturation:
        TimeSeries: [CO2saturation]
    CO2_Leakage_Rates:
        TimeSeries: [CO2_aquifer1, CO2_aquifer2]
    Brine_Leakage_Rates:
        TimeSeries: [brine_aquifer1, brine_aquifer2]
    CO2_Leakage_Mass:
        TimeSeries: [mass_CO2_aquifer1, mass_CO2_aquifer2]
    Brine_Leakage_Mass:
        TimeSeries: [mass_brine_aquifer1, mass_brine_aquifer2]

import os
import sys
import logging

try:
    from openiam.components.atmRom_component import AtmosphericROM
    import openiam.visualization as iam_vis
    from openiam.components.iam_base_classes import IAM_DIR
except ImportError as err:
    print('Unable to load NRAP-Open-IAM module: {}'.format(err))


PLOT_TYPES = [
    'TimeSeries', 'TimeSeriesStats', 'TimeSeriesAndStats', 'AtmPlumeSingle', 
    'AtmPlumeEnsemble', 'StratigraphicColumn', 'Stratigraphy', 'AoR', 'TTFD', 
    'GriddedRadialMetric', 'GriddedMetric', 'Bowtie', 'SalsaProfile', 
    'SalsaTimeSeries', 'SalsaContourPlot', 'SalsaLeakageAoR']

FIGURE_SIZES = {
    'TimeSeries': (13, 8), 'TimeSeriesStats': (13, 8), 'TimeSeriesAndStats': (13, 8), 
    'AtmPlumeSingle': (15, 10), 'AtmPlumeEnsemble': (15, 10), 
    'StratigraphicColumn': (6, 10), 'Stratigraphy': (12, 10), 'AoR': (10, 8), 
    'TTFD': (10, 8), 'GriddedRadialMetric': (10, 8), 'GriddedMetric': (10, 8), 
    'Bowtie': (15, 10), 'SalsaProfile': (10, 12), 'SalsaTimeSeries': (10, 12), 
    'SalsaContourPlot': (10, 8), 'SalsaLeakageAoR': (10, 8)}

TIME_SERIES_PLOT_TYPES = {
    'TimeSeries': ['real'], 'TimeSeriesStats': ['stats'], 
    'TimeSeriesAndStats': ['real', 'stats']}

SALSA_PLOTS = ['SalsaProfile', 'SalsaTimeSeries', 'SalsaContourPlot', 
               'SalsaLeakageAoR']

# PLOT_TYPE_ONE plots all have their options indented under the plot name.
PLOT_TYPE_ONE = ['TimeSeries', 'TimeSeriesStats', 'TimeSeriesAndStats', 'AoR']

# PLOT_TYPE_TWO plots all have their options indented under the plot type, which 
# is then indented under the plot name.
PLOT_TYPE_TWO = ['AtmPlumeSingle', 'AtmPlumeEnsemble', 'StratigraphicColumn', 
                 'Stratigraphy', 'TTFD', 'GriddedRadialMetric', 'GriddedMetric', 
                 'Bowtie', 'SalsaProfile', 'SalsaTimeSeries', 'SalsaContourPlot', 
                 'SalsaLeakageAoR']


def process_plots(yaml_data, model_data, sm, s, output_list, analysis,
                  time_array, components, locations):
    """
    Analyze control file setup related to the plots setup to determine
    what kind of plots need to be produced.
    """
    output_dir = os.path.join(IAM_DIR, model_data['OutputDirectory'])

    plots = yaml_data['Plots']
    for p in plots:
        # Figure name
        save_filename = plots[p].get('savefig', p)  # plot name p is default file name
        save_filename = os.path.join(output_dir, save_filename)

        current_plot_type, plot_type_found = get_plot_type(plots[p], p)
        
        if plot_type_found:
            # Title of plot
            title = get_title(current_plot_type, plots[p], p)
            
            # Figure size
            figsize, plots[p] = get_fig_size(current_plot_type, plots[p], p)

            if 'Data' in plots[p]:
                # Keep Data keyword for backward compatibility
                plots[p]['TimeSeries'] = plots[p]['Data']
            
            if current_plot_type in ['TimeSeries', 'TimeSeriesStats', 'TimeSeriesAndStats']:
                subplot_check = False
                if 'Subplot' in plots[p]:
                    if isinstance(plots[p]['Subplot'], dict):
                        subplot = plots[p].get('Subplot', {'use': True})
                        subplot_check = True
                elif 'subplot' in plots[p]:
                    if isinstance(plots[p]['subplot'], dict):
                        subplot = plots[p].get('subplot', {'use': True})
                        subplot_check = True

                if not subplot_check:
                    subplot = {'use': True}

                # This is here in case the subplot entry was given without 'use'
                if 'use' not in subplot:
                    subplot['use'] = True
                
                iam_vis.time_series_plot(
                    plots[p][current_plot_type], sm, s, plots[p], output_list, name=p, 
                    analysis=analysis, savefig=save_filename, title=title, 
                    subplot=subplot, plot_type=TIME_SERIES_PLOT_TYPES[current_plot_type],
                    figsize=figsize)

            if current_plot_type == 'AtmPlumeSingle':
                satm = find_atm_comp(components)

                iam_vis.map_plume_plot_single(plots[p], p, sm, s, satm, time_array,
                                              output_dir, analysis=analysis,
                                              figsize=figsize)

            if current_plot_type == 'AtmPlumeEnsemble':
                satm = find_atm_comp(components)

                iam_vis.map_plume_plot_ensemble(plots[p], p, sm, s, satm, time_array,
                                                output_dir, analysis=analysis,
                                                figsize=figsize)

            if current_plot_type == 'StratigraphicColumn':
                iam_vis.stratigraphic_column(
                    yaml_data, sm, name=p, savefig=save_filename, title=title,
                    figsize=figsize)

            if current_plot_type == 'Stratigraphy':
                iam_vis.stratigraphy_plot(
                    yaml_data, model_data, sm, name=p, savefig=save_filename,
                    title=title, figsize=figsize)

            if current_plot_type == 'AoR':
                iam_vis.area_of_review_plot(
                    yaml_data, model_data, plots[p]['AoR'], sm, s, output_list,
                    locations, name=p, analysis=analysis, savefig=save_filename,
                    title=title, figsize=figsize, save_results=True)

            if current_plot_type == 'TTFD':
                iam_vis.ttfd_plot(
                    yaml_data, model_data, sm, s, output_dir, name=p, analysis=analysis,
                    figsize=figsize, genfontsize=12, axislabelfontsize=14, 
                    titlefontsize=14, boldlabels=True)

            if current_plot_type == 'GriddedRadialMetric':
                iam_vis.gridded_radial_metric_plot(
                    yaml_data, sm, output_dir, name=p, analysis=analysis, 
                    savefig=save_filename, figsize=figsize, genfontsize=12, 
                    axislabelfontsize=14, titlefontsize=14, boldlabels=True)

            if current_plot_type == 'GriddedMetric':
                iam_vis.gridded_metric_plot(
                    yaml_data, model_data, sm, s, output_dir, name=p, analysis=analysis, 
                    savefig=save_filename, figsize=figsize, genfontsize=12, 
                    axislabelfontsize=14, titlefontsize=14, boldlabels=True)
            
            if current_plot_type == 'Bowtie':
                iam_vis.bowtie_plot(
                    yaml_data, model_data, sm, s, output_list, locations, output_dir, 
                    name=p, analysis=analysis, figsize=figsize, genfontsize=12, 
                    axislabelfontsize=14, titlefontsize=14, boldlabels=True)
            
            if current_plot_type in SALSA_PLOTS:
                iam_vis.salsa_plots(
                    sm, s, output_dir, yaml_data['Plots'][p], model_data, 
                    yaml_data, name=p, analysis=analysis, title=title, 
                    plot_type=current_plot_type, figsize=figsize)


def find_atm_comp(components):
    """ Return AtmosphericROM component if it was added to the system model."""

    for comp in components[::-1]:
        if isinstance(comp, AtmosphericROM):
            atm_comp = comp
            break
    else:
        atm_comp = None
        logging.warning('Unable to find Atmospheric ROM for plume plots')

    return atm_comp


def get_plot_type(plot_yaml_input, name):
    """
    Returns the plot type contained in the current plot entry.
    """
    current_plot_type = None
    continue_check = True
    for plot_type in PLOT_TYPES:
        if plot_type in plot_yaml_input and continue_check:
            current_plot_type = plot_type
            continue_check = False
    
    plot_type_found = True
    if not current_plot_type in PLOT_TYPES:
        err_msg = ''.join([
            'The plot {} was not given one of the expected plot types ({}), ', 
            'and the plot will not be made. Check your input.']).format(
                name, current_plot_type, PLOT_TYPES)
        logging.error(err_msg)
        
        plot_type_found = False
    
    return current_plot_type, plot_type_found


def get_fig_size(current_plot_type, plot_yaml_input, name):
    """
    The position of the FigureSize entry depends on the plot type. TimeSeries,
    TimeSeriesStats, TimeSeriesAndStats, and AoR figures have the FigureSize indented
    under the plot name. The other plot types that accept the FigureSize entry
    have it indented under the plot type (which is in turn
    indented under the plot name).
    """
    figsize_warning_msg = ''.join([
        'The "FigureSize" input ({}) provided for the plot {} could ', 
        'not be converted into a tuple. The input will not be used.'])
    
    figsize = None
    
    if current_plot_type in PLOT_TYPE_ONE:
        if isinstance(plot_yaml_input, dict):
            if 'FigureSize' in plot_yaml_input:
                figsize = plot_yaml_input['FigureSize']
    
    elif current_plot_type in PLOT_TYPE_TWO:
        if isinstance(plot_yaml_input[current_plot_type], dict):
            if 'FigureSize' in plot_yaml_input[current_plot_type]:
                figsize = plot_yaml_input[current_plot_type]['FigureSize']
    
    if figsize is not None:
        try:
            figsize = tuple(figsize)
        except:
            warning_msg = figsize_warning_msg.format(figsize, name)
            logging.warning(warning_msg)
            
            figsize = None
    
    if figsize is None and current_plot_type in FIGURE_SIZES:
        figsize = FIGURE_SIZES[current_plot_type]
    
    # Adding the value for the 'figsize' key
    if current_plot_type in PLOT_TYPE_ONE:
        if isinstance(plot_yaml_input, dict):
            plot_yaml_input['figsize'] = figsize
    elif current_plot_type in PLOT_TYPE_TWO:
        if isinstance(plot_yaml_input[current_plot_type], dict):
            plot_yaml_input[current_plot_type]['figsize'] = figsize
    
    return figsize, plot_yaml_input


def get_title(current_plot_type, plot_yaml_input, name):
    """
    The position of the Title entry depends on the plot type. TimeSeries,
    TimeSeriesStats, TimeSeriesAndStats, and AoR figures have the Title indented
    under the plot name. The other plot types that accept the Title entry
    (not TTFD, GriddedMetric, GriddedRadialMetric, AtmPlumeSIngle, or
    AtmPlumeEnsemble) have it indented under the plot type (which is in turn
    indented under the plot name).
    """
    title = None
    
    if current_plot_type in PLOT_TYPE_ONE:
        if isinstance(plot_yaml_input, dict):
            if 'Title' in plot_yaml_input:
                title = plot_yaml_input['Title']
    
    elif current_plot_type in PLOT_TYPE_TWO:
        if isinstance(plot_yaml_input[current_plot_type], dict):
            if 'Title' in plot_yaml_input[current_plot_type]:
                title = plot_yaml_input[current_plot_type]['Title']
    
    if not isinstance(title, str) and title is not None:
        warning_msg = ''.join([
            'For the {} plot {}, the "Title" entry was provided as {}. ', 
            'This input was not a string, so it will not be used as the ', 
            'figure title.']).format(current_plot_type, name, title)
        logging.warning(warning_msg)
        
        title = None
    
    return title

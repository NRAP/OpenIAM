'''
Here, the system model contains two linked component models: an Analytical 
Reservoir model and a Multisegmented Wellbore. The reservoir model provides pressures 
and CO2 saturations to the wellbore model, which then calculates the leakage rates 
of brine and CO2 into two aquifers overlying the reservoir.

This example demonstrates the use of the 'shalePressureGrad', 'aquiferPressureGrad', 
and 'reservoirPressureGrad' parameters of the Analytical Reservoir Multisegmented 
Wellbore components. Those parameters control how the models calculate the  
initial pressures across the subsurface.

For example, the 'shalePressureGrad' 'aquiferPressureGrad' parameters can be 
given with specific unit numbers (e.g., 'shale1PressureGrad' and 'aquifer2PressureGrad'
for shale 1 and aquifer 2). The parameter values are the rates (in Pa/m) at which 
pressure increases across the specified unit. If a unit number is not included 
in the parameter name ('aquiferPressureGrad'), then the pressure gradient will 
apply to all aquifers that were not given specific values.

These parameters are important, because they can help the user ensure that the 
wellbore model can work with the reservoir data used. For example, if the 
reservoir conditions are taken from a separate reservoir simulator, then the 
Multisegmented Wellbore should be set up so that the initial pressures it  
calculates at the base of each unit is consistent with the reservoir simulation. 
Otherwise, if the Multisegmented Wellbore is given pressures lower than what it 
calculates as the initial reservoir pressure, then the wellbore model can produce 
negative leakage rates (e.g., fluid flowing down from a unit, into another 
aquifer or the reservoir).

This example was created to have a setup similar to control file example 2b 
(ControlFile_ex2b.yaml).

Examples of run:
$ python iam_sys_reservoir_mswell_set_pressure_grads.py
'''

import matplotlib.pyplot as plt
import numpy as np

from openiam.components.iam_base_classes import SystemModel
from openiam.components.analytical_reservoir_component import AnalyticalReservoir
from openiam.components.multisegmented_wellbore_component import MultisegmentedWellbore


if __name__=='__main__':
    #All parameters are from ControlFile_ex2b.yaml
    # Create system model
    num_years = 10.
    time_array = 365.25 * np.arange(0.0, num_years+1)
    sm_model_kwargs = {'time_array': time_array}   # time is given in days
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # Add reservoir component
    ares = sm.add_component_model_object(AnalyticalReservoir(name='ares', parent=sm, locX=100., locY=100.))

    
    #Stratigraphy Parameters
    ares.add_par('numberOfShaleLayers', value=3, vary=False)
    ares.add_par('reservoirThickness', value=50.0, vary=False)
    ares.add_par('shale1Thickness', value=40.0, vary=False)
    ares.add_par('aquifer1Thickness', value=45.0, vary=False)
    ares.add_par('shale2Thickness', value=250.0, vary=False)
    ares.add_par('aquifer2Thickness', value=45.0, vary=False)
    ares.add_par('shale3Thickness', value=250.0, vary=False)
    ares.add_par('datumPressure', value=101325, vary=False)
    ares.add_par('injRate', value=3, vary=False)
    
    # Add parameters of Analytical reservoir component model
    ares.add_par('logResPerm', value=-12.5, vary=False)
    ares.add_par('reservoirRadius', value=2500, vary=False)
    ares.add_par('brineResSaturation', value=0.1, vary=False)
    ares.add_par('brineDensity', value=1195, vary=False)
    ares.add_par('reservoirPressureGrad', value= 1.1722e+4, vary=False)
    ares.add_par('shale1PressureGrad', value=1.0771e+4, vary=False)
    ares.add_par('aquifer1PressureGrad', value=1.0771e+4, vary=False)
    ares.add_par('shale2PressureGrad', value=1.0282e+4, vary=False)
    ares.add_par('aquifer2PressureGrad', value=9.792e+3, vary=False)
    
    # Add observations of reservoir component model
    ares.add_obs('pressure')
    ares.add_obs('CO2saturation')
    ares.add_obs_to_be_linked('pressure')
    ares.add_obs_to_be_linked('CO2saturation')

    # Add multisegmented wellbore component
    msw = sm.add_component_model_object(MultisegmentedWellbore(name='msw', parent=sm))

    # Add parameters of multisegmented wellbore component
    msw.add_par('logWellPerm', value=-13.5, vary=False)
    msw.add_par('brineDensity', value=1195, vary=False)
    msw.add_par('reservoirPressureGrad', value=1.1722e+4, vary=False)
    msw.add_par('shale1PressureGrad', value=1.0771e+4, vary=False)
    msw.add_par('aquifer1PressureGrad', value=1.0771e+4, vary=False)
    msw.add_par('shale2PressureGrad', value=1.0282e+4, vary=False)
    msw.add_par('aquifer2PressureGrad', value=9.792e+3, vary=False)
    
    #Stratigraphy parameters that need to be duplicated
    msw.add_par('numberOfShaleLayers', value=3, vary=False)
    msw.add_par('shale1Thickness', value=40.0, vary=False)
    msw.add_par('shale2Thickness', value=250.0, vary=False)
    msw.add_par('shale3Thickness', value=250.0, vary=False)
    msw.add_par('aquifer1Thickness', value=45.0, vary=False)
    msw.add_par('aquifer2Thickness', value=45.0, vary=False)
    msw.add_par('reservoirThickness', value=50.0, vary=False)
    msw.add_par('datumPressure', value=101325, vary=False)

    # Add keyword arguments of the multisegmented wellbore component model
    msw.add_kwarg_linked_to_obs('pressure', ares.linkobs['pressure'])
    msw.add_kwarg_linked_to_obs('CO2saturation', ares.linkobs['CO2saturation'])

    # Add observations of the multisegmented wellbore component
    msw.add_obs('CO2_aquifer1')
    msw.add_obs('CO2_aquifer2')
    msw.add_obs('CO2_atm')
    msw.add_obs('brine_aquifer1')
    msw.add_obs('brine_aquifer2')

    # Run forward simulation
    sm.forward()

    # Collect observations
    pressures = sm.collect_observations_as_time_series(ares, 'pressure')
    CO2saturations = sm.collect_observations_as_time_series(ares, 'CO2saturation')

    CO2_leakrates_aq1 = sm.collect_observations_as_time_series(msw, 'CO2_aquifer1')
    CO2_leakrates_aq2 = sm.collect_observations_as_time_series(msw, 'CO2_aquifer2')

    brine_leakrates_aq1 = sm.collect_observations_as_time_series(msw, 'brine_aquifer1')
    brine_leakrates_aq2 = sm.collect_observations_as_time_series(msw, 'brine_aquifer2')

    font = {'family': 'Arial',
            'weight': 'normal',
            'size': 10}
    plt.rc('font', **font)

    # Plot reservoir conditions at the base of the wellbore
    plt.figure(1)
    ax = plt.gca()
    ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
    ax.plot(sm.time_array/365.25, pressures, color='C0', linewidth=2)
    ax.set_xlabel('Time [years]', fontsize=12, fontweight='bold')
    ax.set_ylabel('Pressure [Pa]', fontsize=12, fontweight='bold')
    ax.set_title(r'Reservoir Pressures at the Base of the Wellbore', 
                 fontsize=12, fontweight='bold')
    
    plt.figure(2)
    ax = plt.gca()
    ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
    ax.plot(sm.time_array/365.25, CO2saturations, color='C3', linewidth=2)
    ax.set_xlabel('Time [years]', fontsize=12, fontweight='bold')
    ax.set_ylabel('CO$_2$ Saturation  [-]', fontsize=12, fontweight='bold')
    ax.set_title(r'CO$_2$ Saturation at the Base of the Wellbore', 
                 fontsize=12, fontweight='bold')

    # Plot CO2 leakage rates along the wellbore
    plt.figure(3)
    ax = plt.gca()
    ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
    ax.plot(sm.time_array/365.25, CO2_leakrates_aq1, color='#000055',
             linewidth=2, label="aquifer 1")
    ax.plot(sm.time_array/365.25, CO2_leakrates_aq2, color='#FF0066',
             linewidth=2, label="aquifer 2")
    plt.legend()
    ax.set_xlabel('Time [years]', fontsize=12, fontweight='bold')
    ax.set_ylabel('Leakage rates [kg/s]', fontsize=12, fontweight='bold')
    ax.set_title(r'Leakage of CO$_2$ to aquifer 1 and aquifer 2', 
                 fontsize=12, fontweight='bold')

    # Plot Brine leakage rates along the wellbore
    plt.figure(4)
    ax = plt.gca()
    ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
    ax.plot(sm.time_array/365.25, brine_leakrates_aq1, color='#000055',
             linewidth=2, label="aquifer 1")
    ax.plot(sm.time_array/365.25, brine_leakrates_aq2, color='#FF0066',
             linewidth=2, label="aquifer 2")
    plt.legend()
    ax.set_xlabel('Time [years]', fontsize=12, fontweight='bold')
    ax.set_ylabel('Leakage rates [kg/s]', fontsize=12, fontweight='bold')
    ax.set_title(r'Leakage of brine to aquifer 1 and aquifer 2', 
                 fontsize=12, fontweight='bold')

    plt.show()

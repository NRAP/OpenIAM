.. deep_alluvium_aquifer:
.. include:: ../../common/replace_math.rst

Deep Alluvium Aquifer Model
===========================

Class documentation
--------------------
.. autoclass:: openiam.base.DeepAlluviumAquifer

Unittests
----------
.. autoclass:: iam_test.Tests
   :members: test_deep_alluvium_aquifer
   :noindex:

.. bibliography:: ../../bibliography/project.bib
    :filter: docname in docnames

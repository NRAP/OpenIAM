The files contained here are meant to be used to organize a suite of simulations. 
Such record keeping can help if, for example, one person needs to take over the 
simulations set up by another person. 

The user can use the spreadsheet simulation_files.csv to:
- List all of the files for the simulations being assessed.
- Specify which simulation set and parameter set each simulation belongs to.
- Provide a description of each simulation file and how it fits into the larger 
  suite of simulations.

The user can use the spreadsheet simulation_sets.csv to:
- List and describe each set of simulations and how each set fits into the larger 
  suite of models.

The user can use the spreadsheet parameter_sets.csv to:
- List and describe each set of parameter sets and how each set fits into the larger 
  suite of models.

These spreadsheets initially have entries that are only meant to demonstrate the 
usage of these files. The user should replace the entries in the spreadsheets with 
their own data.

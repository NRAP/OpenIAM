# -*- coding: utf-8 -*-
"""
This script scans through all .yaml files in the IAM_DIR/examples/Control_Files/ 
directory, where IAM_DIR is the main directory of the NRAP-Open-IAM installation. 
It then finds all .yaml control files that meet the criteria specified with the 
component type, plot type, and analysis type specified by the user through text 
entry. If the user specifies to run the files (also through text entry), it will 
run the files meeting the search criteria. The user can specify whether to run 
only files that meet all of the search criteria or files that meet any of the 
search criteria (not recommended).
----------------------------------------------------------------------------
Instructions:
- Make sure the .yaml files to process are in the directory IAM_DIR/examples/Control_Files, 
  where IAM_DIR is the main directory of the NRAP-Open-IAM installation.
- Follow the prompts to enter the component type, plot type, analysis type, 
  the option to run files, and the selection of files to run (files that meet 
  any of the search criteria or only files that meet all of the search criteria).
- Run the script by running "python user_lookup_run_CFs.py" from your command 
  line or IDE.
"""
import os
import yaml
import csv
import subprocess
import logging
from datetime import datetime
import re

try:
    from openiam.components.iam_base_classes import IAM_DIR
except:
    err_msg = ('Could not import from NRAP-Open-IAM classes, ensure the ' 
               + 'NRAP-Open-IAM environment has been set up and activated. ' 
               + 'See the NRAP-Open-IAM user guide.')
    
    raise ImportError(err_msg)


#----- Inputs ----------------------------------------------------------------#
output_file_name = 'user_lookup_csv_criteria.csv'


#----- Processing ------------------------------------------------------------#
def sort_selected_files(selected_files): 
    """ 
    Sorts a list of files.
    """ 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)] 
    
    return sorted(selected_files, key=alphanum_key)

# Directory containing the scripts for searching for control files
file_search_dir = os.path.join(IAM_DIR, 'utilities', 'file_search_and_run', 'Control_Files')

logging_folder = os.path.join(file_search_dir, 'logging_files')

if not os.path.exists(logging_folder):
    os.mkdir(logging_folder)

# Define the output folder for the log file path. If changed, update the 
# .gitignore file.
start_time = datetime.now()
now = start_time.strftime('%Y-%m-%d_%H.%M.%S')

# Define the output folder. If changed, update the .gitignore file.
output_dir = os.path.abspath(os.path.join(
    logging_folder, 'user_lookup_run_CFs_{}'.format(now)))

os.makedirs(output_dir, exist_ok=True)

# .log file containing messages
log_file_name = 'results_log.log'

log_file_path = os.path.join(output_dir, log_file_name)

log_level = 'DEBUG'

logger = logging.getLogger('')
# Remove default existing handlers
logger.handlers.clear()
logger.setLevel(log_level)
# logging formatter for log files with more details
log_formatter1 = logging.Formatter(
    fmt='%(levelname)s %(module)s.%(funcName)s: %(message)s',
    datefmt='%m-%d %H:%M')
# logging formatter for console output
log_formatter2 = logging.Formatter(
    fmt='%(levelname)-8s %(message)s',
    datefmt='%m-%d %H:%M')

# Setup logging to log file
file_handler = logging.FileHandler(filename=log_file_path, mode='w')
file_handler.setLevel(log_level)
file_handler.setFormatter(log_formatter1)
logger.addHandler(file_handler)

# Setup logging to console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(log_formatter2)
logger.addHandler(console)


def get_files_that_meet_all_criteria(component_type, plot_type, analysis_type, 
                                     files_with_component_type, files_with_plot_type, 
                                     files_with_analysis_type):
    """
    Returns a list of all control files that meet the defined criteria. If one 
    of the criteria (component_type, plot_type, and analysis_type) was left as 
    '', then it is not considered.
    """
    files_meeting_all_criteria = []
    
    if component_type != '' and plot_type != '':
        files_meeting_comp_and_plot = list(
            set(files_with_component_type).intersection(files_with_plot_type))
        
        if analysis_type != '':
            files_meeting_all_criteria = list(
                set(files_meeting_comp_and_plot).intersection(files_with_analysis_type))
        else:
            files_meeting_all_criteria = files_meeting_comp_and_plot.copy()
    
    elif component_type == '' and plot_type != '':
        if analysis_type != '':
            files_meeting_all_criteria = list(
                set(files_with_plot_type).intersection(files_with_analysis_type))
        else:
            files_meeting_all_criteria = files_with_plot_type.copy()
        
    elif component_type == '' and plot_type == '':
        if analysis_type != '':
            files_meeting_all_criteria = files_with_analysis_type.copy()
        else:
            files_meeting_all_criteria = []
        
    elif component_type != '' and plot_type == '':
        if analysis_type != '':
            files_meeting_all_criteria = list(
                set(files_with_component_type).intersection(files_with_analysis_type))
        else:
            files_meeting_all_criteria = files_with_component_type.copy()
    
    try:
        files_meeting_all_criteria = sort_selected_files(files_meeting_all_criteria)
    except:
        pass
    
    return files_meeting_all_criteria


def run_yaml_example(file_path):
    """
    This function runs the script with the given .yaml file as an argument.

    :param file_path: The path to the .yaml file to be processed.
    """
    # Create the path to the Python script that runs the control files
    run_file = os.path.abspath(os.path.join(
        IAM_DIR, 'src', 'openiam', 'components', 'openiam_cf.py'))
    command = ['python', run_file, '--file', file_path]
    
    msg = f"\nRunning {os.path.basename(file_path)}.\n" 
    logging.info(msg)
    
    try:
        # Run the command and capture the output
        result = subprocess.run(command, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        
        msg = f"\nProcessed {os.path.basename(file_path)} successfully. Output: \n{result.stdout}\n"
        logging.info(msg)
        
    except subprocess.CalledProcessError as e:
        # Log any errors encountered during the subprocess execution
        err_msg = f"\nFailed to process {os.path.basename(file_path)}. Error: \n{e.stderr}\n"
        logging.error(err_msg)


def main():
    """
    Main function to interact with the user, search for .yaml files based on criteria, 
    and process the matching files.
    """
    msg = '\nThis code asks the user to specify a component type, plot type, and analysis type. It then specifies which control files fit those criteria, and runs the files if that option is enabled.\n'
    logging.info(msg)
    
    msg = "\nEnter 'q' to exit at any point.\n"
    logging.info(msg)
    
    # Get criteria from user input
    msg = "\nEnter 'True' to run files meeting the criteria. Enter 'False' to prevent files from running (while still getting the .csv output). Enter 'q' to quit: \n"
    run_files = input(msg).strip()
    
    logging.info(msg + run_files + '\n')
    if run_files.lower() == 'q':
        return
    
    if run_files in ['False', 'false', '0']:
        run_files = False
    elif run_files in ['True', 'true', '1']:
        run_files = True
    else:
        err_msg = f'\nThe run_files variable can only be set to "True" or "False", but it was given as {run_files}. Using default setting of "False".\n'
        logging.error(err_msg)
        
        run_files = False
    
    run_selection = 'All'
    if run_files:
        msg = "\nEnter 'All' to only run files meeting all of the criteria specified. Enter 'Any' to run files meeting any of the criteria specified. Enter 'q' to quit: \n"
        run_selection = input(msg).strip()
        
        logging.info(msg + run_selection + '\n')
        if run_selection.lower() == 'q':
            return
        
        if run_selection in ['all', 'ALL']:
            run_selection = 'All'
        
        if run_selection in ['any', 'ANY']:
            run_selection = 'Any'
        
        if run_selection not in ['All', 'Any']:
            err_msg = f'\nThe run_selection variable can only be set to "Any" or "All", but it was given as {run_selection}. Using default setting of "All".\n'
            logging.error(err_msg)
            
            run_selection = 'All'
    
    msg = "\nEnter a component type, or enter 'pass' to not select by component type. Enter 'q' to quit: \n"
    component_type = input(msg).strip()
    
    logging.info(msg + component_type  + '\n')
    if component_type.lower() == 'q':
        return
    
    if component_type in ['pass', 'Pass', 'PASS']:
        component_type = ''
    
    msg = "\nEnter a plot type, or enter 'pass' to not select by plot type. Enter 'q' to quit: \n"
    plot_type = input(msg).strip()
    
    logging.info(msg + plot_type  + '\n')
    if plot_type.lower() == 'q':
        return
    
    if plot_type in ['pass', 'Pass', 'PASS']:
        plot_type = ''
    
    msg = "\nEnter an analysis type ('forward', 'lhs', or 'parstudy'), or enter 'pass' to not select by analysis type. Enter 'q' to quit: \n"
    analysis_type = input(msg).strip()
    
    logging.info(msg + analysis_type  + '\n')
    if analysis_type.lower() == 'q':
        return
    
    if analysis_type in ['pass', 'Pass', 'PASS']:
        analysis_type = ''
    
    if analysis_type not in ['forward', 'lhs', 'parstudy', '']:
        err_msg = f"\nThe analysis_type entered ({analysis_type}) was not one of the values allowed (forward, lhs, parstudy, or ''). The default of forward will be used.\n"
        logging.error(err_msg)
        analysis_type = 'forward'
    
    # Define the directory containing control files
    control_files_directory = os.path.abspath(os.path.join(
        IAM_DIR, 'examples', 'Control_Files'))
    
    yaml_files = [os.path.join(control_files_directory, f) for f in os.listdir(
        control_files_directory) if f.endswith('.yaml')]

    files_with_component_type = []
    files_with_plot_type = []
    files_with_analysis_type = []

    for yaml_file_path in yaml_files:
        with open(yaml_file_path, 'r') as file:
            try:
                yaml_data = yaml.load(file, Loader=yaml.SafeLoader)
                yaml_filename = os.path.basename(yaml_file_path)

                # Check for component type
                if any('Type' in data and data['Type'] == component_type for data in yaml_data.values()):
                    files_with_component_type.append(yaml_filename)

                # Check for plot type
                if 'Plots' in yaml_data:
                    for plot_name, plot_details in yaml_data['Plots'].items():
                        if plot_type in plot_details:  # Directly check if plot_type is a key
                            files_with_plot_type.append(os.path.basename(yaml_file_path))
                            break  # Assuming one plot type match is sufficient

                # Check for analysis type
                if 'ModelParams' in yaml_data:
                    model_params = yaml_data.get('ModelParams', {})
                    analysis_info = model_params.get('Analysis', {})
                    
                    analysis_type_found = analysis_info if isinstance(
                        analysis_info, str) else analysis_info.get('type', '')
                    
                    if analysis_type_found == analysis_type:
                        files_with_analysis_type.append(yaml_filename)

            except yaml.YAMLError as exc:
                err_msg = f"\nError reading {yaml_file_path}: \n{exc}\n"
                logging.error(err_msg)
        
        file.close()
    
    try:
        files_with_component_type = sort_selected_files(files_with_component_type)
    except:
        pass

    try:
        files_with_plot_type = sort_selected_files(files_with_plot_type)
    except:
        pass

    try:
        files_with_analysis_type = sort_selected_files(files_with_analysis_type)
    except:
        pass
    
    files_meeting_all_criteria = get_files_that_meet_all_criteria(
        component_type, plot_type, analysis_type, files_with_component_type, 
        files_with_plot_type, files_with_analysis_type)
    
    output_text_all_files = f'\nControl files meeting all specified criteria: \n{files_meeting_all_criteria}\n'
    logging.info(output_text_all_files)
    
    # Save the results to a .csv file
    csv_file_path = os.path.join(output_dir, output_file_name)
    
    with open(csv_file_path, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([f'Files with "{component_type}" Component Type', 
                         f'Files with "{plot_type}" Plot Type', 
                         f'Files with "{analysis_type}" Analysis Type', 
                         'Files Meeting All Defined Criteria'])
        
        max_len = max(len(files_with_component_type), len(files_with_plot_type), 
                      len(files_with_analysis_type))
        
        for i in range(max_len):
            row = [
                files_with_component_type[i] if i < len(files_with_component_type) else '',
                files_with_plot_type[i] if i < len(files_with_plot_type) else '',
                files_with_analysis_type[i] if i < len(files_with_analysis_type) else '', 
                files_meeting_all_criteria[i] if i < len(files_meeting_all_criteria) else ''
                ]
            writer.writerow(row)
    file.close()
          
    msg = f'\nSearch results saved to the file {csv_file_path}.\n'
    logging.info(msg)
    
    # Process all the files that met the criteria using the external Python script
    if run_files:
        if run_selection == 'Any':
            selected_files = list(set(
                files_with_component_type + files_with_plot_type + files_with_analysis_type))
            
        elif run_selection == 'All':
            selected_files = files_meeting_all_criteria.copy()
            
        else:
            err_msg = f'\nThe run_selection variable can only be set to "Any" or "All", but it was given as {run_selection}. Using default setting of "All".\n'
            logging.error(err_msg)
            
            selected_files = files_meeting_all_criteria.copy()
        
        # Process all the files that met the criteria using the external Python script!
        for file_name in selected_files:
            full_path = os.path.join(control_files_directory, file_name)
            run_yaml_example(full_path)
        
        msg = '\nAll matched files have been processed.\n'
        logging.info(msg)
    
    msg = f'\nA .log file has been saved to {output_dir}.\n'
    logging.info(msg)
    
    # Shutdown logging so the logging file can be deleted afterwards, if desired
    logging.shutdown()


if __name__ == "__main__":
    main()

# OpenIAM Control File Example 65b
#
# This example is similar to control file example 65a, in that it demonstrates the 
# use of the SalsaLeakageAoR plot type with the SALSA component. This example is identical 
# to that example, except this example includes the parameter entry 'leakingWellAllStatAquifer2: 0'. 
# The parameter 'leakingWell#StatAquifer#' is used to specify if a leaking well ('leakingWell#') 
# is screened (1) or unscreended (0) across an aquifer ('Aquifer#'). Because the word 'All' 
# is used in place of specific well and aquifer names, the entry 'leakingWellAllStatAquifer2' 
# applies to all leaking wells and all aquifers. Aquifer 2 is made to be unscreened along all 
# leaking wells.
#
# The SalsaLeakageAoR plot uses the input 'ThresholdVolume', which is set to 1 m^3. Due to the numerical 
# approaches used in SALSA, values that should be zero can be rounded to very small values (e.g., 1.0e-9 
# instead of 0). Volumes that are not above the ThresholdVolume will not be shown. If the 'ThresholdVolume' 
# input is not provided, the default threshold volume is 0.1 m^3. 
#
# Alternative, the user might want to set a specific threshold volume in order to focus on impactful leakage. 
# For example, if 0.25 m^3 of fluid exits a wellbore, the impact from that amount of leakage may be too small 
# to detect.
#
# This example also demonstrates the use of the TimeList entry for the SalsaLeakageAoR plot type. The TimeList 
# entry is given as a list of years (e.g., 'TimeList: [10, 40, 80, 90, 100]' for 10, 40, 80, 90, and 100 years). 
# The TimeList entries used here will highlight the increases in leakage until year 80; injection stops at year 
# 80, and leakage conditions will change once injection stops. Separate figures will be made showing the 
# results at the times specified.
#
# Note that leaking wells far from the injection sites will have leakage that enters aquifer 1. This leakage 
# into aquifer 1 (the storage reservoir) occurs because aquifer 1 is under-pressurized relative to a hydrostatic 
# curve. As a result, fluid will naturally flow from aquifer 3 into aquifer 1 in locations where the pressure 
# of aquifer 1 is not sufficiently increased by the injection operation. Even though the pressure in aquifer 1 will be 
# higher in these locations that the pressure in aquifer 3 at the same x and y coordinates, aquifer 1 will have a 
# lower total hydraulic head (aquifer 3 will have a higher elevation head; see the discussion of hydraulic head in 
# the Equations section).
#
# The flow into aquifer 1 is also why aquifer 3 will have negative leakage volumes. SALSA allows aqufiers 
# and leaking wells to have dynamic feedback, but this feedback also means that unexpected leakage can occur in 
# situations like this. Typically, however, the user would be more concerned when fluid flows into an aquifer 
# overlying the reservoir, rather than into the reservoir from an overlying aquifer.
#
# To run this file, use the command (\ for Windows / for Mac or Linux):
#  python ../../src/openiam/components/openiam_cf.py --file ControlFile_ex65b.yaml
#
# Last Modified: June, 2024
#-------------------------------------------------
ModelParams:
    # Time is in years
    EndTime: 160
    TimeStep: 10.0
    Analysis: forward
    Components: [SALSA]
    OutputDirectory: output/output_ex65b_{datetime}
    Logging: Warning
#-------------------------------------------------
Stratigraphy:
    # The SALSA component does not connect with a stratigraphy component, so 
    # any information entered here does not impact the results from SALSA.
    numberOfShaleLayers:
        vary: False
        value: 3
    shale1Thickness:
        value: 250.0
        vary: False
    shale2Thickness:
        value: 250.0
        vary: False
    shale3Thickness:
        value: 250.0
        vary: False
    aquifer1Thickness:
        value: 100.0
        vary: False
    aquifer2Thickness:
        value: 100.0
        vary: False
    reservoirThickness:
        value: 50.0
        vary: False
#-------------------------------------------------
SALSA:
    Type: SALSA
    InjectionWell:
        #---- keyword arguments related to active wells ----#
        periodEndTimes: [80, 160]
        # Four injection wells arranged in a square formation
        activeWellCoordx: [1000, -1000, -1000, 1000]
        activeWellCoordy: [1000, 1000, -1000, -1000]
    LeakingWell:
        #---- keyword arguments related to leaking wells ----#
        grid:
            xmin: -30000
            xmax: 30000
            xsize: 9
            ymin: -30000
            ymax: 30000
            ysize: 9
    OutputLocations:
        #---- keyword arguments related to hydraulic head output ----#
        aquiferCoordx: [0]               # Here, not focusing on the hydraulic head and pressure outputs for 
        aquiferCoordy: [0]               # shales or aquifers, so the coordinates used here are not important.
        shaleCoordx: [0]
        shaleCoordy: [0]
    ContourPlot:
        aquiferNamesContourPlots: [1]
    Parameters:
        #---- Stratigraphy parameters ----#
        numberOfShaleLayers: 3
        bottomLayerType: 0
        topLayerType: 1
        shaleAllThickness: 150
        shaleAllLogVertK: -12.0
        shaleAllLogSS: -6.0
        shaleAllInitialGamma: 0
        shaleAllGamma: 0
        aquiferAllThickness: 50
        aquiferAllLogHorizK: -4.0
        aquiferAllANSR: 1
        aquiferAllLogSS: -5.0
        aquiferAllFluidDensity: 1000
        aquiferAllHead: 0               # All aquifers except aquifer 1 will be at hydrostatic pressure
        aquifer1Head: -25               # aquifer 1 is initially under-pressurized
        aquiferAllGamma: 0.0
        #---- Boundary conditions -------#
        bottomBoundaryCond: 1
        bottomBoundaryHead: 0.0
        topBoundaryCond: 1
        topBoundaryHead: 0.0
        topBoundaryPressure: 101325
        #---- Active well parameters ----#
        # Injection period 1
        activeWell1QAquifer1Period1: 0.1
        activeWell2QAquifer1Period1: 0.1
        activeWell3QAquifer1Period1: 0.1
        activeWell4QAquifer1Period1: 0.1
        activeWell1QAquifer2Period1: 0.0
        activeWell2QAquifer2Period1: 0.0
        activeWell3QAquifer2Period1: 0.0
        activeWell4QAquifer2Period1: 0.0
        activeWell1QAquifer3Period1: 0.0
        activeWell2QAquifer3Period1: 0.0
        activeWell3QAquifer3Period1: 0.0
        activeWell4QAquifer3Period1: 0.0
        # Injection period 2
        activeWell1QAquifer1Period2: 0.0
        activeWell2QAquifer1Period2: 0.0
        activeWell3QAquifer1Period2: 0.0
        activeWell4QAquifer1Period2: 0.0
        activeWell1QAquifer2Period2: 0.0
        activeWell2QAquifer2Period2: 0.0
        activeWell3QAquifer2Period2: 0.0
        activeWell4QAquifer2Period2: 0.0
        activeWell1QAquifer3Period2: 0.0
        activeWell2QAquifer3Period2: 0.0
        activeWell3QAquifer3Period2: 0.0
        activeWell4QAquifer3Period2: 0.0
        activeWellAllRadiusAquiferAll: 0.1
        #---- Leaking well parameters ----#
        leakingWellAllLogKAquiferAll: -8.0
        leakingWellAllRadiusAquiferAll: 0.05
        leakingWellAllStatAquiferAll: 1
        leakingWellAllLogKShaleAll: -8.0
        leakingWellAllRadiusShaleAll: 0.05
        leakingWellAllStatAquifer2: 0         # This parameter specifies that the leaking wells are not screened across aquifer 2
        #---- Miscellaneous parameters ----#
        numberOfLaplaceTerms: 8
        xCent: 0.0
        yCent: 0.0
        xMax: 10000.0
        yMax: 10000.0
        gridExp: 1.0
        numberOfNodesXDir: 3
        numberOfNodesYDir: 3
        numberOfRadialGrids: 2
        radialZoneRadius: 100.0
        radialGridExp: 1.2
        numberOfVerticalPoints: 10
    Outputs: [profileDepths, allPressureProfilesShales, 
              wellAllLeakageVolumeAquiferAll]
#-------------------------------------------------
Plots:
    Well_Leakage_Aquifer1:
        TimeSeries: [well1LeakageVolumeAquifer1, well2LeakageVolumeAquifer1, 
                     well3LeakageVolumeAquifer1, well4LeakageVolumeAquifer1, 
                     well5LeakageVolumeAquifer1, well6LeakageVolumeAquifer1, 
                     well7LeakageVolumeAquifer1, well8LeakageVolumeAquifer1, 
                     well9LeakageVolumeAquifer1, well10LeakageVolumeAquifer1, 
                     well11LeakageVolumeAquifer1, well12LeakageVolumeAquifer1, 
                     well13LeakageVolumeAquifer1, well14LeakageVolumeAquifer1, 
                     well15LeakageVolumeAquifer1, well16LeakageVolumeAquifer1, 
                     well17LeakageVolumeAquifer1, well18LeakageVolumeAquifer1, 
                     well19LeakageVolumeAquifer1, well20LeakageVolumeAquifer1, 
                     well21LeakageVolumeAquifer1, well22LeakageVolumeAquifer1, 
                     well23LeakageVolumeAquifer1, well24LeakageVolumeAquifer1, 
                     well25LeakageVolumeAquifer1, well26LeakageVolumeAquifer1, 
                     well27LeakageVolumeAquifer1, well28LeakageVolumeAquifer1, 
                     well29LeakageVolumeAquifer1, well30LeakageVolumeAquifer1, 
                     well31LeakageVolumeAquifer1, well32LeakageVolumeAquifer1, 
                     well33LeakageVolumeAquifer1, well34LeakageVolumeAquifer1, 
                     well35LeakageVolumeAquifer1, well36LeakageVolumeAquifer1, 
                     well37LeakageVolumeAquifer1, well38LeakageVolumeAquifer1, 
                     well39LeakageVolumeAquifer1, well40LeakageVolumeAquifer1, 
                     well41LeakageVolumeAquifer1, well42LeakageVolumeAquifer1, 
                     well43LeakageVolumeAquifer1, well44LeakageVolumeAquifer1, 
                     well45LeakageVolumeAquifer1, well46LeakageVolumeAquifer1, 
                     well47LeakageVolumeAquifer1, well48LeakageVolumeAquifer1, 
                     well49LeakageVolumeAquifer1, well50LeakageVolumeAquifer1, 
                     well51LeakageVolumeAquifer1, well52LeakageVolumeAquifer1, 
                     well53LeakageVolumeAquifer1, well54LeakageVolumeAquifer1, 
                     well55LeakageVolumeAquifer1, well56LeakageVolumeAquifer1, 
                     well57LeakageVolumeAquifer1, well58LeakageVolumeAquifer1, 
                     well59LeakageVolumeAquifer1, well60LeakageVolumeAquifer1, 
                     well61LeakageVolumeAquifer1, well62LeakageVolumeAquifer1, 
                     well63LeakageVolumeAquifer1, well64LeakageVolumeAquifer1, 
                     well65LeakageVolumeAquifer1, well66LeakageVolumeAquifer1, 
                     well67LeakageVolumeAquifer1, well68LeakageVolumeAquifer1, 
                     well69LeakageVolumeAquifer1, well70LeakageVolumeAquifer1, 
                     well71LeakageVolumeAquifer1, well72LeakageVolumeAquifer1, 
                     well73LeakageVolumeAquifer1, well74LeakageVolumeAquifer1, 
                     well75LeakageVolumeAquifer1, well76LeakageVolumeAquifer1, 
                     well77LeakageVolumeAquifer1, well78LeakageVolumeAquifer1, 
                     well79LeakageVolumeAquifer1, well80LeakageVolumeAquifer1, 
                     well81LeakageVolumeAquifer1]
        Title: Leakage Into Aquifer 1
        Subplot:
            Use: False
    Well_Leakage_Aquifer2:
        TimeSeries: [well1LeakageVolumeAquifer2, well2LeakageVolumeAquifer2, 
                     well3LeakageVolumeAquifer2, well4LeakageVolumeAquifer2, 
                     well5LeakageVolumeAquifer2, well6LeakageVolumeAquifer2, 
                     well7LeakageVolumeAquifer2, well8LeakageVolumeAquifer2, 
                     well9LeakageVolumeAquifer2, well10LeakageVolumeAquifer2, 
                     well11LeakageVolumeAquifer2, well12LeakageVolumeAquifer2, 
                     well13LeakageVolumeAquifer2, well14LeakageVolumeAquifer2, 
                     well15LeakageVolumeAquifer2, well16LeakageVolumeAquifer2, 
                     well17LeakageVolumeAquifer2, well18LeakageVolumeAquifer2, 
                     well19LeakageVolumeAquifer2, well20LeakageVolumeAquifer2, 
                     well21LeakageVolumeAquifer2, well22LeakageVolumeAquifer2, 
                     well23LeakageVolumeAquifer2, well24LeakageVolumeAquifer2, 
                     well25LeakageVolumeAquifer2, well26LeakageVolumeAquifer2, 
                     well27LeakageVolumeAquifer2, well28LeakageVolumeAquifer2, 
                     well29LeakageVolumeAquifer2, well30LeakageVolumeAquifer2, 
                     well31LeakageVolumeAquifer2, well32LeakageVolumeAquifer2, 
                     well33LeakageVolumeAquifer2, well34LeakageVolumeAquifer2, 
                     well35LeakageVolumeAquifer2, well36LeakageVolumeAquifer2, 
                     well37LeakageVolumeAquifer2, well38LeakageVolumeAquifer2, 
                     well39LeakageVolumeAquifer2, well40LeakageVolumeAquifer2, 
                     well41LeakageVolumeAquifer2, well42LeakageVolumeAquifer2, 
                     well43LeakageVolumeAquifer2, well44LeakageVolumeAquifer2, 
                     well45LeakageVolumeAquifer2, well46LeakageVolumeAquifer2, 
                     well47LeakageVolumeAquifer2, well48LeakageVolumeAquifer2, 
                     well49LeakageVolumeAquifer2, well50LeakageVolumeAquifer2, 
                     well51LeakageVolumeAquifer2, well52LeakageVolumeAquifer2, 
                     well53LeakageVolumeAquifer2, well54LeakageVolumeAquifer2, 
                     well55LeakageVolumeAquifer2, well56LeakageVolumeAquifer2, 
                     well57LeakageVolumeAquifer2, well58LeakageVolumeAquifer2, 
                     well59LeakageVolumeAquifer2, well60LeakageVolumeAquifer2, 
                     well61LeakageVolumeAquifer2, well62LeakageVolumeAquifer2, 
                     well63LeakageVolumeAquifer2, well64LeakageVolumeAquifer2, 
                     well65LeakageVolumeAquifer2, well66LeakageVolumeAquifer2, 
                     well67LeakageVolumeAquifer2, well68LeakageVolumeAquifer2, 
                     well69LeakageVolumeAquifer2, well70LeakageVolumeAquifer2, 
                     well71LeakageVolumeAquifer2, well72LeakageVolumeAquifer2, 
                     well73LeakageVolumeAquifer2, well74LeakageVolumeAquifer2, 
                     well75LeakageVolumeAquifer2, well76LeakageVolumeAquifer2, 
                     well77LeakageVolumeAquifer2, well78LeakageVolumeAquifer2, 
                     well79LeakageVolumeAquifer2, well80LeakageVolumeAquifer2, 
                     well81LeakageVolumeAquifer2]
        Title: Leakage Into Aquifer 2
        Subplot:
            Use: False
    Well_Leakage_Aquifer3:
        TimeSeries: [well1LeakageVolumeAquifer3, well2LeakageVolumeAquifer3, 
                     well3LeakageVolumeAquifer3, well4LeakageVolumeAquifer3, 
                     well5LeakageVolumeAquifer3, well6LeakageVolumeAquifer3, 
                     well7LeakageVolumeAquifer3, well8LeakageVolumeAquifer3, 
                     well9LeakageVolumeAquifer3, well10LeakageVolumeAquifer3, 
                     well11LeakageVolumeAquifer3, well12LeakageVolumeAquifer3, 
                     well13LeakageVolumeAquifer3, well14LeakageVolumeAquifer3, 
                     well15LeakageVolumeAquifer3, well16LeakageVolumeAquifer3, 
                     well17LeakageVolumeAquifer3, well18LeakageVolumeAquifer3, 
                     well19LeakageVolumeAquifer3, well20LeakageVolumeAquifer3, 
                     well21LeakageVolumeAquifer3, well22LeakageVolumeAquifer3, 
                     well23LeakageVolumeAquifer3, well24LeakageVolumeAquifer3, 
                     well25LeakageVolumeAquifer3, well26LeakageVolumeAquifer3, 
                     well27LeakageVolumeAquifer3, well28LeakageVolumeAquifer3, 
                     well29LeakageVolumeAquifer3, well30LeakageVolumeAquifer3, 
                     well31LeakageVolumeAquifer3, well32LeakageVolumeAquifer3, 
                     well33LeakageVolumeAquifer3, well34LeakageVolumeAquifer3, 
                     well35LeakageVolumeAquifer3, well36LeakageVolumeAquifer3, 
                     well37LeakageVolumeAquifer3, well38LeakageVolumeAquifer3, 
                     well39LeakageVolumeAquifer3, well40LeakageVolumeAquifer3, 
                     well41LeakageVolumeAquifer3, well42LeakageVolumeAquifer3, 
                     well43LeakageVolumeAquifer3, well44LeakageVolumeAquifer3, 
                     well45LeakageVolumeAquifer3, well46LeakageVolumeAquifer3, 
                     well47LeakageVolumeAquifer3, well48LeakageVolumeAquifer3, 
                     well49LeakageVolumeAquifer3, well50LeakageVolumeAquifer3, 
                     well51LeakageVolumeAquifer3, well52LeakageVolumeAquifer3, 
                     well53LeakageVolumeAquifer3, well54LeakageVolumeAquifer3, 
                     well55LeakageVolumeAquifer3, well56LeakageVolumeAquifer3, 
                     well57LeakageVolumeAquifer3, well58LeakageVolumeAquifer3, 
                     well59LeakageVolumeAquifer3, well60LeakageVolumeAquifer3, 
                     well61LeakageVolumeAquifer3, well62LeakageVolumeAquifer3, 
                     well63LeakageVolumeAquifer3, well64LeakageVolumeAquifer3, 
                     well65LeakageVolumeAquifer3, well66LeakageVolumeAquifer3, 
                     well67LeakageVolumeAquifer3, well68LeakageVolumeAquifer3, 
                     well69LeakageVolumeAquifer3, well70LeakageVolumeAquifer3, 
                     well71LeakageVolumeAquifer3, well72LeakageVolumeAquifer3, 
                     well73LeakageVolumeAquifer3, well74LeakageVolumeAquifer3, 
                     well75LeakageVolumeAquifer3, well76LeakageVolumeAquifer3, 
                     well77LeakageVolumeAquifer3, well78LeakageVolumeAquifer3, 
                     well79LeakageVolumeAquifer3, well80LeakageVolumeAquifer3, 
                     well81LeakageVolumeAquifer3]
        Title: Leakage Into Aquifer 3
        Subplot:
            Use: False
    Pressure_Profile:
        SalsaProfile:                          # This plot requires the profileDepths and allPressureProfilesShales outputs
            FigureDPI: 300
            FigureSize: [11, 13]               # The default figure size is [10, 12]
            SaveCSVFiles: True
            MetricType: pressure
    Well_Leakage_AoR_Plot.tiff:
        SalsaLeakageAoR:                       # Requires the well#LeakageVolumeAquifer# outputs for all leaking wells and all aquifers included in AquiferNameList
            ThresholdVolume: 1.0
            AquiferNameList: [2, 3]            # Specifies the aquifers that will be examined for well leakage volumes. If not provided, only the highest aquifer will be used.
            FigureDPI: 300                     # default is 100
            SaveCSVFiles: True                 # default is True
            PlotInjectionSites: True           # default is True
            PlotWellbores: True                # default is True
            TimeList: [10, 40, 80, 90, 100]    # Optional entry used to assess results at specific times. Each value in the list is a time, in years.
            Title: SALSA AoR Analysis
            FigureSize: [11, 9]                # The default figure size is [10, 8]
            SpecifyXandYLims:
                xLims: [-40000, 40000]         # Axis limits for the easting (x) and northing (y) distances in meters.
                yLims: [-40000, 40000]


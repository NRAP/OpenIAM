'''
This example illustrates a system model containing four Theis reservoir models,
each with different, time-varying injection rates. The system model is run
for an array of time points. Pressure changes predicted by each reservoir
model are added to calculate resulting pressure at the observation location.
This script is equivalent to the script iam_sys_theis_4inj.py with a single
reservoir component.

Examples of run:
$ python iam_sys_theis_4inj_wells.py
'''

import numpy as np

from openiam.components.iam_base_classes import SystemModel
from openiam.components.theis_reservoir_component import TheisReservoir


if __name__=='__main__':

    # Create system model
    time_array = np.array([0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11.])
    sm_model_kwargs = {'time_array': time_array}  # time is given in days
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # Specify x, y coordinates of four wells
    wx = [739, 584, 287, 519]
    wy = [423, 822, 333, 856]
    num_wells = len(wx)

    # Specify injection rates over time for four wells
    init_rate = np.array([5.0e-3, 6.0e-3, 7.0e-3, 8.0e-3, 0, 0, 0, 0, 0, 0, 0, 0])
    rates = np.zeros((4, len(init_rate)))
    rates[0, :] = init_rate
    rates[1, :] = -init_rate
    rates[2, :] = 2*init_rate
    rates[3, :] = -2*init_rate

    # Create a single Theis reservoir component model
    # Add reservoir component
    tres = sm.add_component_model_object(
        TheisReservoir(name='tres', parent=sm,
                       injX=wx, injY=wy,
                       locX=500, locY=500,
                       injTimes=time_array, injRates=rates))

    # Set input parameters
    tres.add_par('initialPressure', value=1.0e6)     # Pa
    tres.add_par('reservoirThickness', value=30)    # m
    tres.add_par('logResPerm', value=-10.69897)     # m^2
    tres.add_par('reservoirPorosity', value=.2)
    tres.add_par('brineDensity', value=1000)        # kg/m^3
    tres.add_par('brineViscosity', value=2.535e-3)  # Pa*s
    tres.add_par('poreCompressibility', value=2.46e-9)  # 1/Pa
    tres.add_par('brineCompressibility', value=4.4e-10) # 1/Pa

    # Add observations of reservoir component model
    tres.add_obs('pressure')

    # Run system model using current values of its parameters
    sm.forward()

    # add pressure changes from all four wells
    pressure = sm.collect_observations_as_time_series(tres, 'pressure')

    # print results
    for j, time in enumerate(time_array):
        print(rates[0][j], rates[1][j], rates[2][j], rates[3][j], pressure[j])

    # Expected results
    # 0.005 -0.005 0.01 -0.01 1000000.0
    # 0.006 -0.006 0.012 -0.012 1002565.8520622553
    # 0.007 -0.007 0.014 -0.014 1003193.1278905736
    # 0.008 -0.008 0.016 -0.016 1003768.3055677222
    # 0.0 -0.0 0.0 -0.0 1004331.948229658
    # 0.0 -0.0 0.0 -0.0 1000273.1657167267
    # 0.0 -0.0 0.0 -0.0 1000112.7920014161
    # 0.0 -0.0 0.0 -0.0 1000063.7805329191
    # 0.0 -0.0 0.0 -0.0 1000041.523371836
    # 0.0 -0.0 0.0 -0.0 1000029.3501516429
    # 0.0 -0.0 0.0 -0.0 1000021.9100495816
    # 0.0 -0.0 0.0 -0.0 1000017.0095884056

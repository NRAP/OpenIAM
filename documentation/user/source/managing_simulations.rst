.. managing_simulations: 

Managing Simulations
====================

To thoroughly explore the leakage risks at a geologic carbon storage site, a user of 
NRAP-Open-IAM may need to run many simulations in a systematic manner. Many simulations 
can lead to complicated considerations, like different scenarios (e.g., changing injection 
well or leaking well locations) and different parameterizations (e.g., different injection 
rates and leaking well permeabilities). Manually running each file can take a long time. 
Furthermore, managing these simulations can be challenging if (1) multiple users are 
cooperatively handling them or (2) one person manages the simulations for some time, but 
then another person has to take over the simulations.

Organizing A Suite of Simulations
---------------------------------

To help with such challenges, the files in the directory *utilities/simulation_parameter_space* 
can be used. Here the *utilities/* folder is within the main directory of the NRAP-Open-IAM 
installation. There are three *.csv* files in the *utilities/simulation_parameter_space* 
directory: *simulation_files.csv*, *simulation_sets.csv*, and *parameter_sets.csv*.

The file *simulation_files.csv* is a spreadsheet with the following columns: ``FileName``, 
``Directory``, ``SimulationSet``, ``ParameterSet``, and ``Description``. The ``FileName`` 
column is meant to contain a list of file names that either end with *.py* (script files) 
or *.yaml* (control files). The file names must include the extension *.py* or *.yaml*. 
These files are the simulations making up the user's suite of simulations. The 
``Directory`` column specifies where each of the files in the ``FileName`` field are 
located. Each directory should be given relative to the main directory of NRAP-Open-IAM 
(e.g., a directory could be given as *examples/Control_Files*). The ``SimulationSet`` 
column indicates which simulation set each file corresponds with. Here, each simulation 
set is meant to have a similar setup. For example, simulation set 1 could have leaking 
wells that are closer to the injection sites while simulation 2 could have leaking wells 
that are farther from the injection sites. The ``ParameterSet`` column indicates which parameter 
set each file corresponds with. Here, a parameter set should not change the general setup of 
a simulation set. For example, changing the parameter set should not change the locations 
of injection and leaking wells or add different components. Instead, changing the parameter 
set should only involve changes to parameter values like injection rates and permeabilities. Each 
simulation set can have multiple parameter sets that are used to explore the impact of 
different parameters. Finally, the ``Description`` column is used to describe the simulation 
and how it fits into the larger suite of simulations. The user should write the description in 
a way that another person could easily understand what the simulation does and how it is different 
from the other simulations.

The file *simulation_sets.csv* has two columns: ``SimulationSet`` and ``Description``. This file 
is meant to list all of the simulation sets and describe how each set is different from the rest.

The file *parameter_sets.csv* also has two columns: ``ParameterSet`` and ``Description``. This file 
is meant to list all of the parameter sets and summarize how each set is different from the rest. 

The *simulation_files.csv*, *simulation_sets.csv*, and *parameter_sets.csv* files are initially 
set up with example control files, which are all located in the folder 
*utilities/examples/Control_Files/examples_for_sim_par_space_utility*. These examples are only 
meant to demonstrate how these files should be used, however, and the user should replace the entries 
in these *.csv* files with their own.

Running A Suite of Simulations
------------------------------

While these files can be used to organize a suite of simulations, the user can also use the file 
*run_simulations.py* to run all of the files at once. Before using this file, ensure that the *.csv* 
files described above are fully set up.

To run all of the simulation, open a command prompt and navigate to the directory 
*utilities/simulation_parameter_space*. Then, ensure that the NRAP-Open-IAM environment has been 
activated. For more details regarding navigation in a command prompt or activating an environment, 
see the installation instructions near the beginning of this document.

Once the command prompt is located in the directory *utilities/simulation_parameter_space* and the 
NRAP-Open-IAM environment has been activated, enter the following command and hit enter::

    python run_simulations.py

This command will run all of the simulations entered in *simulation_files.csv*. It will also created several folders; 
the folder *logging_files* will be created in *utilities/simulation_parameter_space*, and another folder called 
*simulations_DATETIME* will be created in the *logging_files* folder (where *DATETIME* will be time formatted 
as year-month-day-hour.minute.second). This folder will contain copies of the *simulation_files.csv*, 
*simulation_sets.csv*, and *parameter_sets.csv* files, as they were when the simulation began. It will also contain 
a logging file, ``results_log.log``. This logging file will include any messages logged during the simulation 
(e.g., messages saying where output was saved or describing errors that occurred).

The output from the each of the simulations will be handled as specified within the simulation file. For example, 
one of the files specified in the *simulation_files.csv* spreadsheet might be *ControlFile_sim1a.yaml*, and 
the output from that simulation will be saved in the directory entered for ``OutputDirectory`` in the 
``ModelParams`` section of the control file. If the *run_simulations.py* file is used to run scripts, then 
any output to be saved must be handled within the script itself.

Although the *simulation_sets.csv* and *parameter_sets.csv* files will be copied into the folder created, these 
spreadsheets are only used to provide context for the simulations and the user must ensure that the information in the 
files is accurate.

This functionality is meant to aid in the management of a large number of simulations and allow the user to run 
simulations repeatedly, without the need for manual commands at the start of each simulation.

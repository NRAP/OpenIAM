# -*- coding: utf-8 -*-
"""
This script reads a list of .yaml control files from the control_files_to_run.csv 
file in this directory. The code then looks for the files in the control files 
directory (IAM_DIR/examples/Control_Files/, where IAM_DIR is the main directory 
of the NRAP-Open-IAM installation) and runs the files.
----------------------------------------------------------------------------
Instructions:
- Ensure the control_files_to_run.csv file is present in the current directory.
- The control_files_to_run.csv file should have one column that contains the names of 
  all .yaml files to run (e.g., 'ControlFile_ex1a.yaml', 'ControlFile_ex1b.yaml', ...)
- Make sure the .yaml files to process are in the directory IAM_DIR/examples/Control_Files, 
  where IAM_DIR is the main directory of the NRAP-Open-IAM installation.
- Run the script by running "python run_CFs_from_csv_file.py" from your 
  command line or IDE.
"""
import os
import csv
import subprocess
import logging
from datetime import datetime

try:
    from openiam.components.iam_base_classes import IAM_DIR
except:
    err_msg = ('Could not import from NRAP-Open-IAM classes, ensure the ' 
               + 'NRAP-Open-IAM environment has been set up and activated. ' 
               + 'See the NRAP-Open-IAM user guide.')
    
    raise ImportError(err_msg)


def set_up_logging_file(logging_folder_dir, logging_file_name, log_level, 
                        default_log_level='Debug'):
    """
    Sets up the logging file. Returns the path to the .log file.
    
    :param logging_output_folder: The name of the folder where a logging file 
        will be made.
    
    :param logging_file_name: The name of the logging file
    
    :param log_level: logging level to use - 'All', 'Debug', 'Info', 'Warning', 
        'Error', or 'Critical'
    
    :param default_log_level: If the log_level provided is not an acceptable 
        value, it will automatically be set to default_log_level.
    """
    log_dict = {'All': logging.NOTSET,
                'Debug': logging.DEBUG,
                'Info': logging.INFO,
                'Warning': logging.WARNING,
                'Error': logging.ERROR,
                'Critical': logging.CRITICAL}
    
    if log_level in log_dict:
        log_level = log_dict[log_level]
    else:
        warning_msg = ''.join([
            f'The logging level was specified as {log_level}, but this input ', 
            'did not match one of the expected values ({list(log_dict.keys())}). ', 
            'The logging level will be set to {default_log_level}.'])
        logging.warning(warning_msg)
        
        log_level = log_dict[default_log_level]
    
    # .log file containing messages
    log_file_path = os.path.join(logging_folder_dir, logging_file_name)
    
    logger = logging.getLogger('')
    # Remove default existing handlers
    logger.handlers.clear()
    logger.setLevel(log_level)
    # logging formatter for log files with more details
    log_formatter1 = logging.Formatter(
        fmt='%(levelname)s %(module)s.%(funcName)s: %(message)s',
        datefmt='%m-%d %H:%M')
    # logging formatter for console output
    log_formatter2 = logging.Formatter(
        fmt='%(levelname)-8s %(message)s',
        datefmt='%m-%d %H:%M')

    # Setup logging to log file
    file_handler = logging.FileHandler(filename=log_file_path, mode='w')
    file_handler.setLevel(log_level)
    file_handler.setFormatter(log_formatter1)
    logger.addHandler(file_handler)

    # Setup logging to console
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    console.setFormatter(log_formatter2)
    logger.addHandler(console)
    
    return log_file_path


def run_yaml_example(file_path):
    """
    This function runs a specified Python script with the given .yaml file as an argument.
    
    :param file_path: The path to the .yaml file to be processed.
    """
    # Create the path to the Python script that runs the control files
    run_file = os.path.join(IAM_DIR, 'src', 'openiam', 'components', 'openiam_cf.py')
    command = ['python', run_file, '--file', file_path]
    
    msg = f"\nRunning {os.path.basename(file_path)}.\n" 
    logging.info(msg)
    
    try:
        result = subprocess.run(command, check=True, stdout=subprocess.PIPE, 
                                stderr=subprocess.PIPE, text=True)
        
        msg = f"\nProcessed {os.path.basename(file_path)} successfully. Output: \n{result.stdout}\n"
        logging.info(msg)
        
    except subprocess.CalledProcessError as e:
        # Log any errors encountered during the subprocess execution
        err_msg = f"\nFailed to process {os.path.basename(file_path)}. Error: \n{e.stderr}\n"
        logging.error(err_msg)


def get_list_from_csv(csv_file_path):
    """
    This function reads a .csv file containing filenames or directories and 
    returns the values as a list.
    
    :param csv_file_path: The path to the .csv file containing the inputs.
    """
    files = []
    # Read the .csv file
    with open(csv_file_path, newline='') as csvfile:
        csvreader = csv.reader(csvfile)
        
        for row in csvreader:
            file_name = row[0]  # Assuming filenames are in the first column
            
            # If the first character is '#', do not store the input
            if file_name[0] != '#':
                files.append(file_name)
    
    return files


def process_control_files(files, file_dirs, set_up_logging=False, log_file_path=None, 
                         logging_folder_dir=None, logging_file_name='results_log.log', 
                         log_level='DEBUG'):
    """
    This function takes a list of .yaml file names and processes each file 
    using the run_yaml_example function.
    
    :param files: List of file names for the .yaml files to process.
    
    :param file_dirs: list of directories for each file in files
    
    :param set_up_logging: option to set up a logging file (True) or not (False)
    
    :param log_file_path: The path to the logging file, used if the logging file 
        has already been set up (and set_up_logging is False)
    
    :param logging_folder_dir: The name of the folder where a logging file 
        will be made.
    
    :param logging_file_name: name of the .log file created
    
    :param log_level: logging level to use - 'All', 'Debug', 'Info', 'Warning', 
        'Error', or 'Critical'
    """
    # The .log file may be set up before process_script_files() is used, or it 
    # can be set up during the function.
    if (set_up_logging and logging_folder_dir is not None 
        and logging_file_name is not None and log_level is not None):
        log_file_path = set_up_logging_file(
            logging_folder_dir, logging_file_name, log_level)
    
    for ind, (file_name, file_dir) in enumerate(zip(files, file_dirs)):
        full_path = os.path.join(file_dir, file_name)
        
        if os.path.exists(full_path):
            run_yaml_example(full_path)
        else:
            err_msg = f'\nThe file "{full_path}" does not exist. Check your input.\n'
            logging.error(err_msg)
    
    msg = f'\nA logging file has been saved ({log_file_path}) and all matched files have been processed.\n'
    logging.info(msg)
    
    # Shutdown logging so the logging file can be deleted afterwards, if desired
    logging.shutdown()


def run_cfs_from_csv_file(file_search_dir, file_name, control_files_directory, 
                          logging_output_folder, logging_file_name='results_log.log', 
                          log_level='Debug'):
    """
    Function reads a .csv containing a list of .yaml control files. It then 
    searches for all of the control files in a certain directory (control_files_directory). 
    If a file is found, it is run - the output from that control file will be 
    saved to the output directory specified within the control file. A logging 
    file is made to record the messages displayed during the run. This function 
    is meant to allow a user to run many control files, one after another.
    
    :param file_search_dir: The directory containing the .csv file containing 
        a list of control files to search for and run.
    
    :param file_name: The name of the .csv file containing a list of control 
        files to search for and run.
    
    :param control_files_directory: The directory or a list of directories in 
        which to search for the control files. If given as a list of directories, 
    
    :param logging_output_folder: The name of the folder where a logging file 
        will be made.
        
    :param logging_file_name: name of the .log file
        
    :param log_level: logging level to use - 'All', 'Debug', 'Info', 'Warning', 
        'Error', or 'Critical'
    """
    # Create the path to the folder logging_output_folder
    logging_folder_dir = os.path.join(file_search_dir, logging_output_folder)

    # Ensure the directory exists
    if not os.path.exists(logging_folder_dir):
        os.makedirs(logging_folder_dir)
    
    log_file_path = set_up_logging_file(
        logging_folder_dir, logging_file_name, log_level)
    
    # Create the full path to the input .csv file
    input_csv_file_path = os.path.join(file_search_dir, file_name)
    
    # Check if the .csv file exists in the output directory
    if not os.path.exists(input_csv_file_path):
        err_msg = ''.join([
            'In the code run_CFs_from_csv_file.py, an error occurred ', 
            'during the function run_cfs_from_csv_file(). The file ', 
            f'{file_name} not found in {file_search_dir}. Check your input.'])
        logging.error(err_msg)
        raise FileNotFoundError(err_msg)
    
    try:
        files = get_list_from_csv(input_csv_file_path)
    except Exception as e:
        err_msg = ''.join([
            'In the code run_CFs_from_csv_file.py, an error occurred while ', 
            f'processing the input .csv file {file_name} in the directory ', 
            f'{file_search_dir}. The error message was: \n{e}'])
        logging.error(err_msg)
    
    if not isinstance(control_files_directory, list):
        file_dirs = [control_files_directory] * len(files)
    else:
        file_dirs = control_files_directory.copy()
        if len(file_dirs) != len(files):
            err_msg = ''.join([
                'In the code run_CFs_from_csv_file.py, an error occurred ', 
                'during the run_cfs_from_csv_file() function. The ', 
                'control_files_directory variable was given as a list ', 
                'of directories, but the length of this list ({len(control_files_directory)}) ', 
                'did not match the length of the files list ({len(files)}). ', 
                'The user can provide a single directory, or one directory ', 
                'for each control file.'])
            logging.error(err_msg)
            raise ValueError(err_msg)
    
    try:
        # Call the main function with the .csv file path
        process_control_files(files, file_dirs, log_file_path=log_file_path)
    except Exception as e:
        err_msg = ''.join([
            'In the code run_CFs_from_csv_file.py, an error occurred while ', 
            f'processing the input .csv file {file_name} in the directory ', 
            f'{file_search_dir}. The error message was: \n{e}'])
        logging.error(err_msg)


if __name__ == "__main__":  
    # Demonstrates how to use the run_cfs_from_csv_file() function.
    #----- Inputs ---------#
    # Results will be placed in a folder with this name. If changed, update the 
    # .gitignore file in this directory
    start_time = datetime.now()
    now = start_time.strftime('%Y-%m-%d_%H.%M.%S')
    
    logging_output_folder = os.path.join(
        IAM_DIR, 'utilities', 'file_search_and_run', 'Control_Files', 'logging_files')
    
    if not os.path.exists(logging_output_folder):
        os.mkdir(logging_output_folder)
    
    logging_output_folder = os.path.join(
        logging_output_folder, 'run_CFs_from_csv_{}'.format(now))

    # The name of the .csv file specifying which .yaml control files to run.
    file_name = 'control_files_to_run.csv'

    #----- Processing -----#
    # Define the directory containing the .yaml files, can be given as one 
    # directory or as a list containing a directory for each control file
    control_files_directory = os.path.join(IAM_DIR, 'examples', 'Control_Files')

    # Directory containing the scripts for searching for control files
    file_search_dir = os.path.join(
        IAM_DIR, 'utilities', 'file_search_and_run', 'Control_Files')
    
    run_cfs_from_csv_file(file_search_dir, file_name, control_files_directory, 
                          logging_output_folder)

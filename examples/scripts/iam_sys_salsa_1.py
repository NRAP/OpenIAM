# -*- coding: utf-8 -*-
'''
This example uses the SALSA component to model hydraulic head and leakage 
dynamics across alternating shale and aquifer layers. The model takes input 
parameters for the stratigraphic layers (e.g., number of layers, thicknesses, 
and hydraulic conductivity), injection wells (e.g., injection rates over time), 
and leaking wells (e.g., well permeabilities). 

Here, aquifer 1 is the reservoir, while aquifer 2 is a unit overlying the reservoir. 
There is a shale unit (shale 1) below aquifer 1 because the bottomLayerType parameter 
is set to 0. There is another shale (shale 2) between aquifer 1 and aquifer 2, 
and then there is a shale (shale 3) above aquifer 2 because the topLayerType 
parameter is set to 0.

Aquifer 1 is a storage reservoir because it is given injection rate parameters. 
With SALSA, any aquifer can be given injection or extraction rates.

THis simulation uses one injection well and one leaking well. The simulation 
produces outputs such as leakage rates, cumulative volumes, and hydraulic head 
variations at specific observation points. Results are visualized using 
specialized plot types, including time series, head profiles, and contour plots, 
to provide insights into subsurface fluid behavior over time.

This script was created to have the same setup as the control file 
`ControlFile_ex62a.yaml`.

Example of run:
$ python iam_sys_salsa_1.py
'''

import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, ticker
from datetime import datetime
from openiam.components.iam_base_classes import SystemModel
from openiam.components.salsa_component import SALSA

# TF_ENABLE_ONEDNN_OPTS=0

# Plotting options
FIGSIZE = (11, 8)
GENFONTSIZE = 10
FONTSIZE = 12
LGNFONTSIZE = 8
LINEWIDTH = 2
LINESTYLES = ['-', '--', ':', '-.', (0, (1, 10)), (5, (10, 3)), (0, (3, 10, 1, 10))]
HANDLELENGTH = 7
DPI = 300

FONT = 'Arial'

font = {'family': FONT,
        'weight': 'normal',
        'size': GENFONTSIZE}
plt.rc('font', **font)

def main():
    # Define output directory
    output_dir = os.path.join(os.getcwd(), '..', '..', 'Output')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    now = datetime.now().strftime('%Y-%m-%d_%H.%M.%S')
    output_dir = os.path.join(output_dir, f'iam_sys_salsa_1_{now}')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    csv_dir = os.path.join(output_dir, 'csv_files')
    if not os.path.exists(csv_dir):
        os.makedirs(csv_dir)

    # Define parameters
    num_years = 100
    dt = 10.0
    time_array = 365.25 * np.arange(0, num_years + dt, dt)

    # Number of aquifers is calculated as (numberOfShaleLayers + bottomLayerType + topLayerType - 1)
    numberOfShaleLayers = 3
    bottomLayerType = 0
    topLayerType = 0
    
    # Calculated as (numberOfShaleLayers + bottomLayerType + topLayerType - 1)
    numberOfAquiferLayers = 2

    # Boundary conditions at the bottom and top boundaries
    bottomBoundaryCond = 1      # 0 for fixed-head boundary, 1 for no-flow boundary
    bottomBoundaryHead = 0.0    # hydraulic head in meters, used if bottomBoundaryCond is 0
    topBoundaryCond = 0         # 0 for fixed-head boundary, 1 for no-flow boundary
    topBoundaryHead = 0.0       # hydraulic head in meters, used if topBoundaryCond is 0

    # Options for the solution, see the SALSA documentation in the user guide
    numberOfLaplaceTerms = 8
    xCent = 0
    yCent = 0
    xMax = 3000
    yMax = 3000
    gridExp = 1.0
    numberOfNodesXDir = 40
    numberOfNodesYDir = 40
    numberOfRadialGrids = 6
    radialZoneRadius = 50.0
    radialGridExp = 1.2
    numberOfVerticalPoints = 10

    # Aquifer properties
    aquifer1Thickness = 50.0      # meters
    aquifer1LogHorizK = -6.462    # log10 m/s
    aquifer1ANSR = 1.0
    aquifer1LogSS = -5.675
    aquifer1FluidDensity = 1000.0 # kg/(m^3)
    aquifer1Head = 70.6334 
    aquifer1Gamma = 0.0
    
    aquifer2Thickness = 50.0      # meters
    aquifer2LogHorizK = -6.462    # log10 m/s
    aquifer2ANSR = 1.0
    aquifer2LogSS = -5.675
    aquifer2FluidDensity = 1000.0 # kg/(m^3)
    aquifer2Head = 36.4788
    aquifer2Gamma = 0.0

    # Shale properties
    shale1Thickness = 186.0     # meters
    shale1LogVertK = -10.994    # log10 m/s
    shale1LogSS = -5.269
    shale1InitialGamma = 0
    shale1Gamma = 0
    
    shale2Thickness = 186.0     # meters
    shale2LogVertK = -10.994    # log10 m/s
    shale2LogSS = -5.269
    shale2InitialGamma = 0
    shale2Gamma = 0
    
    shale3Thickness = 186.0     # meters
    shale3LogVertK = -10.994    # log10 m/s
    shale3LogSS = -5.269
    shale3InitialGamma = 0
    shale3Gamma = 0

    # Active (injection / extraction) well properties
    # Lists of coordinates (easting x and northing y) in meters
    activeWellCoordx = [0.0]
    activeWellCoordy = [0.0]
    
    # Radius (meters) across each aquifer
    active_well1_radius_aq1 = 0.1
    active_well1_radius_aq2 = 0.1 
    
    # Injection rates in (m^3)/s for each aquifer and each injection period
    aquifer1_injection_rate_period1_cms = 0.00966 
    aquifer1_injection_rate_period2_cms = 0
    aquifer2_injection_rate_period1_cms = 0
    aquifer2_injection_rate_period2_cms = 0
    
    # List of the times at which injection rates change. Times given in years here. 
    # In this example, injection stops at 50 years and the post injection period 
    # lasts until 100 years. 
    periodEndTimes = [50, 100]
    
    # Inputs for the leaking wells
    # Lists of coordinates (easting x and northing y) in meters
    leakingWellCoordx = [2000]
    leakingWellCoordy = [0]
    
    # Log. of hydraulic conductivity (log10 m/s) across each aquifer and shale, 
    # so 10^(-5.5) m/s
    leaking1WellLogKAq1 = -5.5
    leaking1WellLogKAq2 = -5.5
    leakingWell1LogKShale1 = -5.5
    leakingWell1LogKShaleK2 = -5.5
    leakingWell1LogKShaleK3 = -5.5
    
    # Leaking well radius (m) across each aquifer and shale
    leakingWell_radius = 0.15
    leaking1WellRadiusAq1 = leakingWell_radius
    leaking1WellRadiusAq2 = leakingWell_radius
    leakingWell1RadiusShale1 = leakingWell_radius
    leakingWell1RadiusShale2 = leakingWell_radius
    leakingWell1RadiusShale3 = leakingWell_radius
    
    # Leaking well status across aquifers 1 and 2 (0 for cased, 1 for screened - 
    # leakage can only enter an aquifer from the leaking well when it is screened)
    leakingWell1StatAq1 = 1
    leakingWell1StatAq2 = 1
    
    # Output coordinates (in meters, easting x and northing y) for hydraulic head outputs
    aquiferCoordx = [1990.0]
    aquiferCoordy = [0]
    shaleCoordx = [1990.0]
    shaleCoordy = [0]
    
    # The aquifers to show in the contour plots
    aquiferNamesContourPlots = [1, 2]

    # Create system model
    sm = SystemModel(model_kwargs={'time_array': time_array})
    
    comp_name = 'salsa'
    salsa = sm.add_component_model_object(SALSA(
        name=comp_name, parent=sm, periodEndTimes=periodEndTimes,
        activeWellCoordx=activeWellCoordx, activeWellCoordy=activeWellCoordy,
        leakingWellCoordx=leakingWellCoordx, leakingWellCoordy=leakingWellCoordy,
        aquiferCoordx=aquiferCoordx, aquiferCoordy=aquiferCoordy,
        shaleCoordx=shaleCoordx, shaleCoordy=shaleCoordy,
        aquiferNamesContourPlots=aquiferNamesContourPlots, output_dir=output_dir))

    # Add parameters of the SALSA component
    salsa.add_par('bottomLayerType', value=bottomLayerType, vary=False)
    salsa.add_par('topLayerType', value=topLayerType, vary=False)
    salsa.add_par('bottomBoundaryCond', value=bottomBoundaryCond, vary=False)
    salsa.add_par('bottomBoundaryHead', value=bottomBoundaryHead, vary=False)
    salsa.add_par('topBoundaryCond', value=topBoundaryCond, vary=False)
    salsa.add_par('topBoundaryHead', value=topBoundaryHead, vary=False)
    salsa.add_par('numberOfLaplaceTerms', value=numberOfLaplaceTerms, vary=False)
    salsa.add_par('xCent', value=xCent, vary=False)
    salsa.add_par('yCent', value=yCent, vary=False)
    salsa.add_par('xMax', value=xMax, vary=False)
    salsa.add_par('yMax', value=yMax, vary=False)
    salsa.add_par('gridExp', value=gridExp, vary=False)
    salsa.add_par('numberOfNodesXDir', value=numberOfNodesXDir, vary=False)
    salsa.add_par('numberOfNodesYDir', value=numberOfNodesYDir, vary=False)
    salsa.add_par('numberOfRadialGrids', value=numberOfRadialGrids, vary=False)
    salsa.add_par('radialZoneRadius', value=radialZoneRadius, vary=False)
    salsa.add_par('radialGridExp', value=radialGridExp, vary=False)
    salsa.add_par('numberOfVerticalPoints', value=numberOfVerticalPoints, vary=False)

    # Shale properties
    salsa.add_par('numberOfShaleLayers', value=numberOfShaleLayers, vary=False)
    salsa.add_par('shale1Thickness', value=shale1Thickness, vary=False)
    salsa.add_par('shale1LogVertK', value=shale1LogVertK, vary=False)
    salsa.add_par('shale1LogSS', value=shale1LogSS, vary=False)
    salsa.add_par('shale1InitialGamma', value=shale1InitialGamma, vary=False)
    salsa.add_par('shale1Gamma', value=shale1Gamma, vary=False)

    salsa.add_par('shale2Thickness', value=shale2Thickness, vary=False)
    salsa.add_par('shale2LogVertK', value=shale2LogVertK, vary=False)
    salsa.add_par('shale2LogSS', value=shale2LogSS, vary=False)
    salsa.add_par('shale2InitialGamma', value=shale2InitialGamma, vary=False)
    salsa.add_par('shale2Gamma', value=shale2Gamma, vary=False)

    salsa.add_par('shale3Thickness', value=shale3Thickness, vary=False)
    salsa.add_par('shale3LogVertK', value=shale3LogVertK, vary=False)
    salsa.add_par('shale3LogSS', value=shale3LogSS, vary=False)
    salsa.add_par('shale3InitialGamma', value=shale3InitialGamma, vary=False)
    salsa.add_par('shale3Gamma', value=shale3Gamma, vary=False)

    # Aquifer properties
    salsa.add_par('aquifer1Thickness', value=aquifer1Thickness, vary=False)
    salsa.add_par('aquifer1LogHorizK', value=aquifer1LogHorizK, vary=False)
    salsa.add_par('aquifer1ANSR', value=aquifer1ANSR, vary=False)
    salsa.add_par('aquifer1LogSS', value=aquifer1LogSS, vary=False)
    salsa.add_par('aquifer1FluidDensity', value=aquifer1FluidDensity, vary=False)
    salsa.add_par('aquifer1Head', value=aquifer1Head, vary=False)
    salsa.add_par('aquifer1Gamma', value=aquifer1Gamma, vary=False)

    salsa.add_par('aquifer2Thickness', value=aquifer2Thickness, vary=False)
    salsa.add_par('aquifer2LogHorizK', value=aquifer2LogHorizK, vary=False)
    salsa.add_par('aquifer2ANSR', value=aquifer2ANSR, vary=False)
    salsa.add_par('aquifer2LogSS', value=aquifer2LogSS, vary=False)
    salsa.add_par('aquifer2FluidDensity', value=aquifer2FluidDensity, vary=False)
    salsa.add_par('aquifer2Head', value=aquifer2Head, vary=False)
    salsa.add_par('aquifer2Gamma', value=aquifer2Gamma, vary=False)

    # Well properties
    salsa.add_par('activeWell1RadiusAquifer1', value=active_well1_radius_aq1, vary=False)
    salsa.add_par('activeWell1RadiusAquifer2', value=active_well1_radius_aq2, vary=False)
    
    # Injection rates (m^3/s) for each aquifer and each period
    salsa.add_par('activeWell1QAquifer1Period1', value=aquifer1_injection_rate_period1_cms, vary=False)
    salsa.add_par('activeWell1QAquifer1Period2', value=aquifer1_injection_rate_period2_cms, vary=False)
    salsa.add_par('activeWell1QAquifer2Period1', value=aquifer2_injection_rate_period1_cms, vary=False)
    salsa.add_par('activeWell1QAquifer2Period2', value=aquifer2_injection_rate_period2_cms, vary=False)

    # Leaking Well properties
    salsa.add_par('leakingWell1LogKAquifer1', value=leaking1WellLogKAq1, vary=False)
    salsa.add_par('leakingWell1LogKAquifer2', value=leaking1WellLogKAq2, vary=False)
    salsa.add_par('leakingWell1RadiusAquifer1', value=leaking1WellRadiusAq1, vary=False)
    salsa.add_par('leakingWell1RadiusAquifer2', value=leaking1WellRadiusAq2, vary=False)
    salsa.add_par('leakingWell1StatAquifer1', value=leakingWell1StatAq1, vary=False)
    salsa.add_par('leakingWell1StatAquifer2', value=leakingWell1StatAq2, vary=False)
    salsa.add_par('leakingWell1LogKShale1', value=leakingWell1LogKShale1, vary=False)
    salsa.add_par('leakingWell1LogKShale2', value=leakingWell1LogKShaleK2, vary=False)
    salsa.add_par('leakingWell1LogKShale3', value=leakingWell1LogKShaleK3, vary=False)
    salsa.add_par('leakingWell1RadiusShale1', value=leakingWell1RadiusShale1, vary=False)
    salsa.add_par('leakingWell1RadiusShale2', value=leakingWell1RadiusShale2, vary=False)
    salsa.add_par('leakingWell1RadiusShale3', value=leakingWell1RadiusShale3, vary=False)

    # Add observations
    observations = [
        'headLoc1Aquifer1', 'headLoc1Aquifer2', 
        'pressureLoc1Aquifer1', 'pressureLoc1Aquifer2',
        'diffuseLeakageRateBottomAquifer1', 'diffuseLeakageRateBottomAquifer2',
        'diffuseLeakageVolumeBottomAquifer1', 'diffuseLeakageVolumeBottomAquifer2',
        'diffuseLeakageRateTopAquifer1', 'diffuseLeakageRateTopAquifer2',
        'diffuseLeakageVolumeTopAquifer1', 'diffuseLeakageVolumeTopAquifer2',
    ]
    
    # Get lists for the combinations of leaking wells and aquifer numbers
    well_nums = list(range(1, len(leakingWellCoordx) + 1)) * numberOfAquiferLayers
    aq_nums = list(range(1, numberOfAquiferLayers + 1)) * len(well_nums)
    
    # Leakage rates and volumes from each individual leaking well into each aquifer
    indiv_well_lr_obs = [f'well{wellRef}LeakageRateAquifer{aqNum}' 
                         for (wellRef, aqNum) in zip(well_nums, aq_nums)]
    
    indiv_well_lv_obs = [f'well{wellRef}LeakageVolumeAquifer{aqNum}' 
                         for (wellRef, aqNum) in zip(well_nums, aq_nums)]
    
    observations += indiv_well_lr_obs
    observations += indiv_well_lv_obs
    
    # Leakage rates and volumes from all leaking well into each aquifer
    total_well_lr_obs = [f'wellLeakageRateAquifer{aqNum}' 
                         for aqNum in range(1, numberOfAquiferLayers + 1)]
    
    total_well_lv_obs = [f'wellLeakageVolumeAquifer{aqNum}' 
                         for aqNum in range(1, numberOfAquiferLayers + 1)]
    
    observations += total_well_lr_obs
    observations += total_well_lv_obs

    numberOfProfiles = 1
    for profileRef in range(1, numberOfProfiles + 1):
        for vert_point in range(1, numberOfVerticalPoints + 1):
            for shaleRef in range(1, numberOfShaleLayers + 1):
                observations.append(f'headProfile{profileRef}VertPoint{vert_point}Shale{shaleRef}')
                observations.append(f'pressureProfile{profileRef}VertPoint{vert_point}Shale{shaleRef}')

    SALSA_GRID_OBSERVATIONS = [
       'profileDepths', 'allHeadProfilesShales', 'contourPlotAquiferHead', 
       'allPressureProfilesShales', 'contourPlotAquiferPressure',
       'contourPlotCoordx', 'contourPlotCoordy']
    
    observations.extend(SALSA_GRID_OBSERVATIONS)
    
    for obs in observations:
        if obs in SALSA_GRID_OBSERVATIONS:
            salsa.add_grid_obs(
                obs, constr_type='matrix', output_dir=output_dir)
        else:
            salsa.add_obs(obs)

    sm.forward()

    ##############
    # Outputs    #
    ##############
    output_data = {}
    for obs in observations:

        if not obs in SALSA_GRID_OBSERVATIONS:
            try:
                data = sm.collect_observations_as_time_series(salsa, obs)
                output_data[obs] = data
                output_file = os.path.join(csv_dir, f'{obs}.csv')
                np.savetxt(output_file, np.column_stack((time_array, data)), 
                           delimiter=',', header=f'Time,{obs}', comments='')
            except Exception as e:
                print(f"Error fetching or saving data for observation {obs}: {e}")
        else:
            output_data[obs] = 'SAVED_NPZ_FILE'

    ##############
    # Plotting   #
    ##############
    plt.close('all')
    plot_configurations = {
        'Diffuse_Leakage_Rates': [
            'diffuseLeakageRateBottomAquifer1', 'diffuseLeakageRateTopAquifer1',
            'diffuseLeakageRateBottomAquifer2', 'diffuseLeakageRateTopAquifer2'
        ],
        'Diffuse_Leakage_Volumes': [
            'diffuseLeakageVolumeBottomAquifer1', 'diffuseLeakageVolumeTopAquifer1',
            'diffuseLeakageVolumeBottomAquifer2', 'diffuseLeakageVolumeTopAquifer2'
        ],
        'Total_Well_Leakage_Volumes': total_well_lv_obs,
        'Total_Well_Leakage_Rates': total_well_lr_obs,
        'Individual_Well_Leakage_Volumes': indiv_well_lv_obs,
        'Individual_Well_Leakage_Rates': indiv_well_lr_obs,
    }
    
    for plot_name, obs_list in plot_configurations.items():
        y_label_addition = ''
        if 'Rate' in plot_name:
            y_label_addition = ' (m$^3$/s)'
        elif 'Volume' in plot_name:
            y_label_addition = ' (m$^3$)'
        
        plt.figure(figsize=FIGSIZE, dpi=DPI)
        
        lstyle_ref = 0
        for obs in obs_list:
            if obs in output_data:
                plt.plot(time_array / 365.25, output_data[obs], label=obs, 
                         linewidth=LINEWIDTH, linestyle=LINESTYLES[lstyle_ref])
                
                lstyle_ref += 1 
                if lstyle_ref >= len(LINESTYLES):
                    lstyle_ref = 0
                
        plt.xlabel('Time (years)', fontsize=FONTSIZE, fontweight='bold')
        plt.ylabel(plot_name.replace('_', ' ') + y_label_addition, 
                   fontsize=FONTSIZE, fontweight='bold')
        
        ax = plt.gca()
        ax.ticklabel_format(style='sci', axis='y',scilimits=(0, 0), useMathText=True)
        
        plt.title(f'{plot_name.replace("_", " ")} Over Time', 
                  fontsize=FONTSIZE, fontweight='bold')
        plt.legend(fontsize=LGNFONTSIZE, handlelength=HANDLELENGTH)
        
        plt.savefig(os.path.join(output_dir, f'{plot_name}.png'))
        plt.show()

    plot_specific_observations(output_data, time_array, output_dir, comp_name, 
                               aquiferNamesContourPlots, activeWellCoordx, 
                               activeWellCoordy, leakingWellCoordx, 
                               leakingWellCoordy, aquiferCoordx, aquiferCoordy, 
                               numberOfShaleLayers, numberOfAquiferLayers)

def plot_specific_observations(output_data, time_array, output_dir, comp_name, 
                               aquiferNamesContourPlots, activeWellCoordx, 
                               activeWellCoordy, leakingWellCoordx, 
                               leakingWellCoordy, aquiferCoordx, aquiferCoordy, 
                               numberOfShaleLayers, numberOfAquiferLayers,
                               cmap='Spectral_r'):
    def make_plot(plotInd, obs_list, metric_label, metric_units, unit_type, 
                  figsize=FIGSIZE, diff_subplots=False, max_subplots=None):
        plt.figure(plotInd, figsize=figsize, dpi=DPI)
        
        plotInd += 1
        
        # Excludes the 's' at the end
        unit_type_v2 = unit_type[:-1]
        
        if diff_subplots:
            if max_subplots is None:
                diff_subplots = False
                print(
                    'In the function plot_specific_observations(), cannot use ', 
                    + 'different subplots (diff_subplots = True) unless the max_subplots ',  
                    + 'keyword argument is provided.')
        else:
            ax = plt.gca()
        
        min_value = 9.99e+99
        max_value = -9.99e+99
        lstyle_ref = 0
        for obs in obs_list:
            if obs in output_data:
                if diff_subplots:
                    unitNum = int(obs[obs.index(unit_type_v2) + len(unit_type_v2)])
                    
                    ax = plt.subplot(max_subplots, 1, unitNum)
                
                ax.plot(time_array / 365.25, output_data[obs], label=obs, 
                         linewidth=LINEWIDTH, linestyle=LINESTYLES[lstyle_ref])
                
                lstyle_ref += 1 
                if lstyle_ref >= len(LINESTYLES):
                    lstyle_ref = 0
                
                if np.min(output_data[obs]) < min_value:
                    min_value = np.min(output_data[obs])
                
                if np.max(output_data[obs]) > max_value:
                    max_value = np.max(output_data[obs])
        
        if diff_subplots:
            for subplotRef in range(1, max_subplots + 1):
                ax = plt.subplot(max_subplots, 1, subplotRef)
                ax.set_title(f'{unit_type_v2} {subplotRef}', fontsize=FONTSIZE, 
                             fontweight='bold')
                
                if subplotRef == max_subplots:
                    ax.set_xlabel('Time (years)', fontsize=FONTSIZE, fontweight='bold')
                    
                ax.set_ylabel(f'{metric_label} ({metric_units})', fontsize=FONTSIZE, fontweight='bold')
                ax.ticklabel_format(style='sci', axis='y', scilimits=(0, 0), useMathText=True)
                
                ax.set_ylim([min_value, max_value])
                
                # Not using a legend here, space is limited in the subplot
                # ax.legend(fontsize=LGNFONTSIZE, handlelength=HANDLELENGTH)
        else:
            ax.set_xlabel('Time (years)', fontsize=FONTSIZE, fontweight='bold')
            ax.set_ylabel(f'{metric_label} in {unit_type} ({metric_units})', fontsize=FONTSIZE, fontweight='bold')
            
            ax.ticklabel_format(style='sci', axis='y', scilimits=(0, 0), useMathText=True)
            
            ax.set_title(f'Salsa {metric_label} Output in {unit_type}', fontsize=FONTSIZE, fontweight='bold')
            
            ax.legend(fontsize=LGNFONTSIZE, handlelength=HANDLELENGTH)
        
        title_addition = 'Head'
        if metric_label == 'Pressure':
            title_addition = metric_label
        
        plt.savefig(os.path.join(output_dir, f'Salsa{title_addition}TimeSeries_{unit_type}.png'))
                    
        return plotInd
    
    plotInd = 1  # Start plot index at 1
    
    aq_ref_list = list(range(numberOfAquiferLayers)) * len(aquiferCoordx)
    loc_list = list(range(len(aquiferCoordx))) * numberOfAquiferLayers
    
    metrics = [[f'headLoc{locRef + 1}Aquifer{aqRef + 1}' 
                for (locRef, aqRef) in zip(loc_list, aq_ref_list)], 
               [f'pressureLoc{locRef + 1}Aquifer{aqRef + 1}' 
                for (locRef, aqRef) in zip(loc_list, aq_ref_list)], 
               ]
    metric_labels = ['Hydraulic Head', 'Pressure']
    metric_units = ['m', 'Pa']
    for ind, (metric, metric_label, metric_unit) in enumerate(zip(
            metrics, metric_labels, metric_units)):
        try:
            plotInd = make_plot(
                plotInd, metric, metric_label, metric_unit, 'Aquifers')
        except Exception as e:
            print(f"Error plotting Salsa Vertical Profile: {e}")
    
    # Get the head and pressure profile output for aquitards
    aquitard_head_obs_list = []
    aquitard_pressure_obs_list = []
    for obs in output_data.keys():
        if 'headProfile' in obs:
            aquitard_head_obs_list.append(obs)
        
        if 'pressureProfile' in obs:
            aquitard_pressure_obs_list.append(obs)
    
    aquitard_obs_list = [aquitard_head_obs_list, aquitard_pressure_obs_list]
    
    height_shales = 12 * (numberOfShaleLayers / 3)
    figsize_shales = (10, height_shales)
    
    for ind, (metric, metric_label, metric_unit) in enumerate(zip(
            aquitard_obs_list, metric_labels, metric_units)):
        try:
            plotInd = make_plot(
                plotInd, metric, metric_label, metric_unit, 'Shales', 
                figsize=figsize_shales, diff_subplots=True, max_subplots=numberOfShaleLayers)
        except Exception as e:
            print(f"Error plotting Salsa Vertical Profile: {e}")
    
    def fmt(x, pos):
        a, b = '{:.2e}'.format(x).split('e')
        b = int(b)
        return r'${} \times 10^{{{}}}$'.format(a, b)
    
    def get_val_str(val):
        a, b = '{:.2e}'.format(np.min(val)).split('e')
        b = int(b)
        val_str = r'${}\times10^{{{}}}$'.format(a, b)
        
        return val_str

    # Contour plots
    fail_check = False
    try:
        file_name = '{}_{}_sim_0_time_0.npz'.format(comp_name, 'contourPlotCoordx')
        data = np.load(os.path.join(output_dir, file_name))
        contourPlotCoordx = data['data']
        data.close()
        
        file_name = '{}_{}_sim_0_time_0.npz'.format(comp_name, 'contourPlotCoordy')
        data = np.load(os.path.join(output_dir, file_name))
        contourPlotCoordy = data['data']
        data.close()
    except:
        print("Missing contour plot data.")
        fail_check = True
    
    def make_contour_plot(plotInd, metric, metric_label, metric_unit, 
                          contourPlotCoordx, contourPlotCoordy, colormap, 
                          metric_loc='Bottom'):
        # Do not use 'Bottom' when getting the bottom pressure, only 'Mid' or 
        # 'Top' for middle and top.
        metric_loc_str = metric_loc
        if metric_loc == 'Bottom':
            metric_loc_str = ''
        
        metric_name_for_npz = f'contourPlot{metric_loc_str}Aquifer{metric}'
        
        # In a stochastic simulation the index 'sim_0' would be varied for 
        # different realizations (e.g., 'sim_1', 'sim_2', ... 'sim_N' where 
        # N is the number of realizations).
        file_name = '{}_{}_sim_0_time_0.npz'.format(comp_name, metric_name_for_npz)
        file_path = os.path.join(output_dir, file_name)
        
        if os.path.exists(file_path):
            data = np.load(file_path)
            contourPlotAquiferMetric = data['data']
            data.close()
            
            for aquInd, aquName in enumerate(aquiferNamesContourPlots):
                min_val = np.min(contourPlotAquiferMetric[:, aquInd, :])
                max_val = np.max(contourPlotAquiferMetric[:, aquInd, :])
                values_for_cbar = np.linspace(min_val, max_val, 200)
                
                for timeInd, time in enumerate(time_array):
                    plt.figure(plotInd, figsize=FIGSIZE, dpi=DPI)
                    plotInd += 1
                    
                    values = contourPlotAquiferMetric[timeInd, aquInd, :]

                    plt.tricontourf(contourPlotCoordx / 1000, contourPlotCoordy / 1000, 
                                    values, cmap=colormap, levels=values_for_cbar, 
                                    locator=ticker.MaxNLocator(
                                        nbins=100, prune='lower'))
                    
                    for ind, (injX, injY) in enumerate(zip(
                            activeWellCoordx, activeWellCoordy)):
                        if ind == 0:
                            plt.plot(injX / 1000, injY / 1000, marker='s', color='k', 
                                     markerfacecolor='none', linestyle='none', 
                                     linewidth=LINEWIDTH, label='Injection Well')
                        else:
                            plt.plot(injX / 1000, injY / 1000, marker='s', color='k', 
                                     markerfacecolor='none', linestyle='none', 
                                     linewidth=LINEWIDTH)
                    
                    for ind, (leakX, leakY) in enumerate(zip(
                            leakingWellCoordx, leakingWellCoordy)):
                        if ind == 0:
                            plt.plot(leakX / 1000, leakY / 1000, marker='o', color='k', 
                                     markerfacecolor='none', linestyle='none', 
                                     linewidth=LINEWIDTH, label='Leaking Well')
                        else:
                            plt.plot(leakX / 1000, leakY / 1000, marker='o', color='k', 
                                     markerfacecolor='none', linestyle='none', 
                                     linewidth=LINEWIDTH)
                    
                    plt.xlabel('X (km)', fontsize=FONTSIZE, fontweight='bold')
                    plt.ylabel('Y (km)', fontsize=FONTSIZE, fontweight='bold')
                    
                    min_val_str = get_val_str(np.min(values))
                    max_val_str = get_val_str(np.max(values))
                    
                    if np.min(values) == np.max(values):
                        range_str = f', {metric_label} Value: {min_val_str} {metric_unit}'
                    else:
                        range_str = f', {metric_label} Range: {min_val_str} {metric_unit} to {max_val_str} {metric_unit}'
                    
                    # Used to specify if the pressure is taken from the bottom, 
                    # middle, or top of the aquifer.
                    metric_loc_str = ''
                    if metric_loc != '':
                        if metric_loc == 'Mid':
                            metric_loc_str = 'Middle of '
                        else:
                            metric_loc_str = metric_loc + ' of '
                    
                    plt.title(f'{metric_loc_str}Aquifer {aquName}, t = {time / 365.25} years' + range_str, 
                              fontsize=FONTSIZE, fontweight='bold')
                    
                    ax = plt.gca()
                    cbar = plt.colorbar(cm.ScalarMappable(cmap=colormap), ax=ax, 
                                        values=values_for_cbar, 
                                        format=ticker.FuncFormatter(fmt))
                    cbar.set_label(f'{metric_label} ({metric_unit})', rotation=90, 
                                   fontsize=FONTSIZE, fontweight='bold')
                    tick_locator = ticker.MaxNLocator(nbins=5)
                    cbar.locator = tick_locator
                    cbar.update_ticks()
                    
                    plt.legend(fontsize=LGNFONTSIZE)
                    
                    plt.savefig(os.path.join(
                        output_dir, f'SalsaContourPlot_Aquifer{aquName}_{metric_loc}{metric}_timeInd{timeInd}.png'), 
                        dpi=DPI)
        else:
            print(f'File required for a contour plot is not present: {file_path}')
        
        return plotInd
    
    if not fail_check:
        # Used to specify if the pressure should be taken as the top ('Top'), 
        # bottom ('Bottom'), or middle ('Mid') of the aquifer. Set to '' for Head.
        aq_locations = ['', 'Bottom']
        
        metrics = ['Head', 'Pressure']
        metric_labels = ['Hydraulic Head', 'Pressure']
        metric_units = ['m', 'Pa']
        # Have different colormaps for the metrics, so the plots can be distinguished more easily
        colormaps = ['Spectral_r', 'RdYlBu_r']
        
        for ind, (metric, metric_loc, metric_label, metric_unit, colormap) in enumerate(zip(
                metrics, aq_locations, metric_labels, metric_units, colormaps)):
            try:
                plotInd = make_contour_plot(
                    plotInd, metric, metric_label, metric_unit, 
                    contourPlotCoordx, contourPlotCoordy, colormap=colormap, 
                    metric_loc=metric_loc)
            except Exception as e:
                print(f"Error plotting Salsa Contour Plot: {e}")
    
    plt.show()

if __name__ == '__main__':
    main()
